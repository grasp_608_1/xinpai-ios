//
//  XPCommonMacros.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#ifndef XPCommonMacros_h
#define XPCommonMacros_h

#pragma mark -转化为空 防崩溃
#define TO_STR(obj) ((obj==nil||obj==NULL||[[NSString stringWithFormat:@"%@",obj] isEqualToString:@"<null>"])?@"":[NSString stringWithFormat:@"%@",obj])
#define stringIsEmpty(str) ([str isKindOfClass:[NSNull class]] || str == nil || [[NSString stringWithFormat:@"%@",str] isEqualToString:@"<null>"] || [str length] < 1 ? YES : NO)
#define arrayIsEmpty(array) (array == nil || [array isKindOfClass:[NSNull class]] || array.count == 0)
#define dicIsEmpty(dic) (dic == nil || [dic isKindOfClass:[NSNull class]] || dic.allKeys == 0)

#pragma mark -控制按钮交互最低间隔时间1s
#define buttonCanUseAfterOneSec(sender) sender.userInteractionEnabled = NO;\
dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{\
    sender.userInteractionEnabled = YES;\
});

#define phonePredicate  @"^1[3456789][0-9]{9}"
#define codePredicate   @"^[0-9]{6}"

#define MidX(v)                 CGRectGetMidX((v).frame)
#define MidY(v)                 CGRectGetMidY((v).frame)
#define MaxX(v)                 CGRectGetMaxX((v).frame)
#define MaxY(v)                 CGRectGetMaxY((v).frame)

#pragma mark-UI常量
#define SCREEN_WIDTH            ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT           ([UIScreen mainScreen].bounds.size.height)
#define k_keyWindow             [UIApplication sharedApplication].windows.firstObject
// Nav高
#define kNavBarHeight 44.0
// 状态栏高
#define kStatusBarHeight [XPCommonTool getStatusBarHight]
// 顶部导航和状态栏高度
#define kTopHeight (kStatusBarHeight + kNavBarHeight)

// tabBar高
#define kTabBarHeight ([UIApplication sharedApplication].windows.firstObject.windowScene.statusBarManager.statusBarFrame.size.height>20?83:49)
// 顶部导航和状态栏高度+iPhoneX底部高度(适配iPhoneX)
#define kGSMarginHeight ([[UIApplication sharedApplication] statusBarFrame].size.height>20?(kTopHeight+34.f):kTopHeight)

// iPhone 底部高度 ( iPhone X:34,其他为 0)
#define kBottomHeight ([UIApplication sharedApplication].windows.firstObject.windowScene.statusBarManager.statusBarFrame.size.height>20?34:0)

#define IS_IPHONEX \
({BOOL isPhoneX = NO;\
if (@available(iOS 11.0, *)) {\
isPhoneX = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0.0;\
}\
(isPhoneX);})

//App 颜色
#define RandColor  RGBCOLOR(arc4random_uniform(255), arc4random_uniform(255), arc4random_uniform(255))
#define app_main_color UIColorFromRGB(0x4C49E7)
#define app_less_color UIColorFromRGB(0x9B99DF)
#define app_font_color UIColorFromRGB(0x3A3C41)
#define app_font_color_8A UIColorFromRGB(0x8A8A8A)
#define app_font_color_FF4A4A  UIColorFromRGB(0xFF4A4A )
//[UIColor colorWithHexString:@"#FAB2B7"]
// 颜色(RGB)
#define RGBCOLOR(r, g, b)       [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r, g, b, a)   [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]
// RGB颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue)\
                                \
                                [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
                                                green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
                                                 blue:((float)(rgbValue & 0xFF))/255.0 \
                                                alpha:1.0]

#pragma mark -  字体区

//#define BOLDSYSTEMFONT(FONTSIZE)[UIFont boldSystemFontOfSize:FONTSIZE]
//#define SYSTEMFONT(FONTSIZE)    [UIFont systemFontOfSize:FONTSIZE]
#define kFontRegular(fontSize) [UIFont fontWithName:@"PingFangSC-Regular" size: fontSize]
#define kFontMedium(fontSize) [UIFont fontWithName:@"PingFangSC-Medium" size: fontSize]
#define kFontBold(fontSize) [UIFont fontWithName:@"PingFangSC-Semibold" size: fontSize]

#define XPWeakSelf __weak typeof(self) weakSelf = self;
#pragma mark -Notification

#define Notification_user_should_login @"Notification_user_should_log"
#define Notification_user_login_success @"Notification_user_login_success"
#define Notification_user_login_out @"Notification_user_login_out"
#define Notification_user_should_Authentication @"Notification_user_should_Authentication"
#define Notification_user_choose_adress @"Notification_user_choose_adress"
#define Notification_user_choose_bankcard @"Notification_user_choose_bankcard"


#define Notification_homepage_fresh @"Notification_homepage_fresh"
#define Notification_shopList_fresh @"Notification_shopList_fresh"

#define XP_wechat_appKey  @"wx4e65168bf0708da2"
#define XP_wechat_appSecret @"8f96c6f19e2414ee2b12d5aae4529906"
#define XP_wechat_universalLink @"https://www.new-auction.com/app/xinpai/"
#define XP_UM_appKey @"6673d82dcac2a664de511fd3"
#define XP_UM_appKey_debug @"67514e357e5e6a4eebae1af4"
#define ZW_UM_appKey @"67514c147e5e6a4eebae19db"
#define ZW_UM_appKey_debug @"67514c678f232a05f1c957b0"
#define ZW_wechat_appKey  @"wx2d3309d53fa1dddb"
#define ZW_wechat_appSecret @"26a8c22870a961e4d6d0f019f4ccb304"
#define ZW_wechat_universalLink @"https://www.new-auction.com/app/zaowu/"

#endif /* XPCommonMacros_h */
