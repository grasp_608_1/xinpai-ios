//
//  UIButton+parameter.h
//  TOEFLNEWAPP
//
//  Created by Liu on 2021/1/27.
//  Copyright © 2021 学为贵. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (parameter)

@property (nonatomic, copy) NSDictionary *attachDic;
//
@property (nonatomic, copy) NSArray *attachArr;

//@property (nonatomic, copy) NSSet

@property (nonatomic, copy) NSString *attachString;


@end

NS_ASSUME_NONNULL_END
