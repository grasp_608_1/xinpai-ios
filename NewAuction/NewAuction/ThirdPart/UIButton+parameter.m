//
//  UIButton+parameter.m
//  TOEFLNEWAPP
//
//  Created by Liu on 2021/1/27.
//  Copyright © 2021 学为贵. All rights reserved.
//

#import "UIButton+parameter.h"

static char attachStringKey;
static char attachDicKey;
static char attachArrKey;

@implementation UIButton (parameter)


-(NSString *)attachString{
    return objc_getAssociatedObject(self, &attachStringKey);
}
- (void)setAttachString:(NSString *)attachString{
    objc_setAssociatedObject(self, &attachStringKey, [NSString stringWithFormat:@"%@",attachString], OBJC_ASSOCIATION_COPY);
}


-(NSArray *)attachArr{
    return objc_getAssociatedObject(self, &attachArrKey);
}

- (void)setAttachArr:(NSArray *)attachArr{
    objc_setAssociatedObject(self, &attachArrKey, attachArr, OBJC_ASSOCIATION_COPY);
}
-(NSDictionary *)attachDic{
    return objc_getAssociatedObject(self, &attachDicKey);
}

-(void)setAttachDic:(NSDictionary *)attachDic{
    objc_setAssociatedObject(self, &attachDicKey, attachDic, OBJC_ASSOCIATION_COPY);
}

@end
