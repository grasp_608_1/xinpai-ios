//
//  CHTCollectionViewWaterfallHeader.m
//  Demo
//
//  Created by Neil Kimmett on 21/10/2013.
//  Copyright (c) 2013 Nelson. All rights reserved.
//

#import "CHTCollectionViewWaterfallHeader.h"

@interface CHTCollectionViewWaterfallHeader ()

@end

@implementation CHTCollectionViewWaterfallHeader

#pragma mark - Accessors
- (id)initWithFrame:(CGRect)frame {
  if (self = [super initWithFrame:frame]) {
//    self.backgroundColor = [UIColor redColor];
      [self setupViews];
  }
  return self;
}
- (void)setupViews{
    UIImageView *titleImageView = [[UIImageView alloc] init];
    titleImageView.frame = CGRectMake(self.mj_w/2 - 58,12, 116, 24);
    self.titleImageView = titleImageView;
    [self addSubview:titleImageView];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(14);
    titleLabel.textColor = UIColorFromRGB(0xFF4A4A);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.frame = CGRectMake(0, 0, self.mj_w, self.mj_h);
    self.titleLabel = titleLabel;
    [self addSubview:titleLabel];
}


@end
