//
//  XPProductPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import "XPProductPopView.h"
#import "XPDefaultLabelCell.h"

@interface XPProductPopView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, copy) NSArray *dataArray;

@property (nonatomic, assign) CGPoint point;

@end

@implementation XPProductPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.alpha = 0;
    self.backgroundColor = [UIColor clearColor];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces = NO;
    [self addSubview:self.tableView];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPDefaultLabelCell *cell = [XPDefaultLabelCell cellWithTableView:tableView];
    [cell cellWithTitle:self.dataArray[indexPath.row]];
    return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.selectBlock) {
        self.selectBlock(indexPath.row,self.dataArray[indexPath.row]);
        [self dismiss];
    }
}


- (void)configWithTitleArray:(NSArray *)titleArray{
    self.dataArray = titleArray;
    
    [self.tableView reloadData];
    
}
- (void)showToPoint:(CGPoint)point titleArray:(NSArray *)titleArray{
    self.alpha = 1;
    self.point = point;
    [self configWithTitleArray:titleArray];
    self.tableView.frame = CGRectMake(point.x, point.y, 120, 0);
    [UIView animateWithDuration:0.2 animations:^{
        CGFloat maxH = titleArray.count * 40;
        if (maxH > self.mj_h * 0.7) {
            maxH = self.mj_h * 0.7;
        }
        self.tableView.frame = CGRectMake(point.x, point.y, 120,maxH );
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.2 animations:^{
//        self.tableView.frame = CGRectZero;
        self.tableView.frame = CGRectMake(self.point.x, self.point.y, 120, 0);
    } completion:^(BOOL finished) {
        self.alpha = 0;
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self dismiss];
}

@end
