//
//  XPProductDetailView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import "XPProductDetailView.h"
#import "XPSelectItemView.h"

@interface XPProductDetailView ()

@end


@implementation XPProductDetailView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.backgroundColor = UIColor.whiteColor;
}
- (void)configWithtitleArray:(NSArray *)array valueArray:(NSArray *)valueArray {
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    
    for (int i = 0; i<array.count; i++) {
        
        UIView *bgView = [UIView new];
        bgView.frame = CGRectMake(16, 32*i, self.mj_w - 32, 32);
        bgView.backgroundColor = i%2 == 0? UIColor.whiteColor:UIColorFromRGB(0xF9F9F9);
        [self addSubview:bgView];
        
        UILabel *leftLabel = [UILabel new];
        leftLabel.font = kFontRegular(14);
        leftLabel.textColor = UIColorFromRGB(0x8A8A8A);
        leftLabel.frame = CGRectMake(14, 0, 80, bgView.mj_h);
        [bgView addSubview:leftLabel];
        
        UILabel *rightLabel = [UILabel new];
        rightLabel.font = kFontRegular(14);
        rightLabel.textColor = UIColorFromRGB(0x3A3C41);
        rightLabel.textAlignment = NSTextAlignmentRight;
        rightLabel.frame = CGRectMake(MaxX(leftLabel), 0, bgView.mj_w - 28 - leftLabel.mj_w, bgView.mj_h);
        [bgView addSubview:rightLabel];
        
        leftLabel.text = array[i];
        rightLabel.text = valueArray[i];
       
    }
}



@end
