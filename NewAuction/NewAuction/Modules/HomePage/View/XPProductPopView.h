//
//  XPProductPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPProductPopView : UIView

//- (void)configWithTitleArray:(NSArray *)titleArray;

@property (nonatomic, copy)void(^selectBlock)(NSInteger index,NSString *content);

- (void)showToPoint:(CGPoint)point titleArray:(NSArray *)titleArray;

- (void)dismiss;
@end

NS_ASSUME_NONNULL_END
