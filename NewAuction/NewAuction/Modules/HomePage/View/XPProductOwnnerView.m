//
//  XPProductOwnnerView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import "XPProductOwnnerView.h"

@implementation XPProductOwnnerView


-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.backgroundColor = UIColor.whiteColor;
}

- (CGFloat)configWithTitle:(NSString *)titleStr listArray:(NSArray *)array {
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat maxY = 0;
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(14);
    titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel.frame = CGRectMake(30, 8, self.mj_w - 30 -56, 24);
    titleLabel.text = titleStr;
//    [self addSubview:titleLabel];
    
//    UIView *sepLineView = [[UIView alloc]init];
//    sepLineView.backgroundColor = UIColorFromRGB(0xDEDEDE);
//    sepLineView.frame = CGRectMake(16, MaxY(titleLabel) + 8, self.mj_w - 32, 1);
//    
//    [self addSubview:sepLineView];
    
    
    CGFloat labelW = 60;
    
    for (int i = 0; i<array.count; i++) {
        
        UILabel *leftLabel = [UILabel new];
        leftLabel.font = kFontRegular(14);
        leftLabel.textColor = UIColorFromRGB(0x8A8A8A);
        leftLabel.frame = CGRectMake(30,i*32,labelW, 32);
        if (i == 0) {
            leftLabel.text = @"上拍方";
        }else if (i == 1) {
            leftLabel.text = @"联系人";
        }else {
            leftLabel.text = @"联系电话";
        }
        
        
        [self addSubview:leftLabel];
        
        UILabel *rightLabel = [UILabel new];
        rightLabel.font = kFontRegular(14);
        rightLabel.textColor = UIColorFromRGB(0x3A3C41);
        rightLabel.textAlignment = NSTextAlignmentRight;
        rightLabel.frame = CGRectMake(30 + labelW + 5, leftLabel.mj_y, self.mj_w - 60 - labelW + 5, 32);
        rightLabel.text = TO_STR(array[i]);
        [self addSubview:rightLabel];
        
        if (i== array.count - 1) {
            maxY = MaxY(rightLabel);
        }
    }
    return maxY + 8;
}

@end
