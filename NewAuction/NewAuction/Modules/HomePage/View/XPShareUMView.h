//
//  XPShareUMView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShareUMView : UIView

@property (nonatomic, copy) void(^actionBlock)(NSInteger index);

- (void)show;


@end

NS_ASSUME_NONNULL_END
