//
//  XPShareUMView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/3.
//

#import "XPShareUMView.h"

@interface XPShareUMView ()

@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UIView *bgView;
@end

static NSTimeInterval xpShareDuring = 0.4;

@implementation XPShareUMView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    UIView *bgView = [[UIView alloc] initWithFrame:self.bounds];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
    [bgView addGestureRecognizer:tap];
    self.bgView = bgView;
    [self addSubview:bgView];
    
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.layer.masksToBounds = YES;
    mainView.frame = CGRectMake(0,self.mj_h, self.mj_w , 210 + kBottomHeight);
    [XPCommonTool cornerRadius:mainView andType:1 radius:20];
    self.mainView = mainView;
    [self addSubview:mainView];

    CGFloat buttonW = 64;
    CGFloat margin = (self.mainView.mj_w - buttonW * 2)/3.;
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.frame = CGRectMake(margin,30,buttonW,buttonW);
    [button1 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button1 setImage:[UIImage imageNamed:@"xp_wechat_friend"] forState:UIControlStateNormal];
    button1.tag = 88;
    [self.mainView addSubview:button1];
    
    UILabel *titleLabel1 = [UILabel new];
    titleLabel1.font = kFontRegular(14);
    titleLabel1.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel1.textAlignment = NSTextAlignmentCenter;
    titleLabel1.frame = CGRectMake(button1.mj_x, MaxY(button1) + 6, button1.mj_w, 20);
    titleLabel1.text = @"微信好友";
    [self.mainView addSubview:titleLabel1];
    
    
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.frame = CGRectMake(margin * 2 + buttonW,button1.mj_y,buttonW,buttonW);
    [button2 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:button2];
    [button2 setImage:[UIImage imageNamed:@"xp_wechat_circle"] forState:UIControlStateNormal];
    button2.tag = 99;
    
    UILabel *titleLabel2 = [UILabel new];
    titleLabel2.font = kFontRegular(14);
    titleLabel2.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel2.textAlignment = NSTextAlignmentCenter;
    titleLabel2.frame = CGRectMake(button2.mj_x, MaxY(button2) + 6, button2.mj_w, 20);
    titleLabel2.text = @"朋友圈";
    [self.mainView addSubview:titleLabel2];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(0,self.mainView.mj_h - kBottomHeight - 48, self.mainView.mj_w, 48);
    [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    cancelButton.titleLabel.font = kFontMedium(14);
    [cancelButton setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:cancelButton];
}

- (void)show{
    [UIView animateWithDuration:xpShareDuring animations:^{
        self.mainView.frame = CGRectMake(0,self.mj_h - 210 - kBottomHeight, self.mj_w , 210 + kBottomHeight);
    }];
}

- (void)buttonClicked:(UIButton *)sender{
    buttonCanUseAfterOneSec(sender);
    
    if (sender.tag == 88) {
        if (self.actionBlock) {
            self.actionBlock(1);
        }
    }else {
        if (self.actionBlock) {
            self.actionBlock(2);
        }
    }
    [self animationDismiss];
}

- (void)tap {
    buttonCanUseAfterOneSec(self.bgView);
    [self animationDismiss];
}

- (void)cancelButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self animationDismiss];
}

- (void)animationDismiss {
    [UIView animateWithDuration:xpShareDuring animations:^{
        self.mainView.frame = CGRectMake(0,self.mj_h, self.mj_w , 210 + kBottomHeight);
        self.bgView.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
