//
//  XPSearchDefaultView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/4.
//

#import "XPSearchDefaultView.h"

@interface XPSearchDefaultView ()<UITextFieldDelegate>

@property (nonatomic, strong) UIButton *searchButton;

@property (nonatomic, strong) UITextField *contentTextField;
//recoard textField status
@property (nonatomic, assign) BOOL isActive;

@end

@implementation XPSearchDefaultView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
-(void)updateFrame:(CGRect)frame{
    self.frame = frame;
    self.searchButton.frame = CGRectMake(self.mj_w - 32 - 16, 0, 32, self.mj_h);
}
- (void)setupView {
    
    self.layer.cornerRadius = self.mj_h/2.;
    self.backgroundColor = UIColor.whiteColor;
    
    UIImageView *imageView = [UIImageView new];
    imageView.image = [UIImage imageNamed:@"xp_search_black"];
    imageView.frame = CGRectMake(16, 8, 24, 24);
    self.searchIcon = imageView;
    [self addSubview:imageView];
    
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
    searchButton.titleLabel.font = kFontRegular(16);
    [searchButton setTitle:@"搜索" forState:UIControlStateNormal];
    searchButton.frame = CGRectMake(self.mj_w - 32 - 16, 0, 32, self.mj_h);
    [searchButton addTarget:self action:@selector(searchButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.searchButton = searchButton;
    [self addSubview:searchButton];
    
    UITextField *contentTextField = [[UITextField alloc] init];
    contentTextField.font = kFontRegular(14);
    contentTextField.textColor = app_font_color;
    contentTextField.frame = CGRectMake(MaxX(imageView) + 16, 0, searchButton.mj_x - MaxX(imageView) - 16 , self.mj_h);
    contentTextField.delegate = self;
    self.contentTextField = contentTextField;
    self.contentTextField.userInteractionEnabled = NO;
    [self addSubview:contentTextField];
}

- (void)searchButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.searchBlock) {
        self.searchBlock(self.contentTextField.text.length == 0? @"" :self.contentTextField.text);
    }
}

-(void)setCanSearch:(BOOL)canSearch {
    self.contentTextField.userInteractionEnabled = canSearch;
}

#pragma mark -- UItextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (self.searchStateBlock) {
        self.searchStateBlock(YES);
    }

        
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (self.searchStateBlock) {
        self.searchStateBlock(NO);
    }
}
-(void)setDefaultSearchStr:(NSString *)defaultSearchStr {
    self.contentTextField.text = defaultSearchStr;
}
@end
