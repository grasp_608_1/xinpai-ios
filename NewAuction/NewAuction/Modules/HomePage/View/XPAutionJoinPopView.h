//
//  XPAutionJoinPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPAutionJoinPopView : UIView

- (void)showWithPrice:(NSNumber *)price maxCount:(NSInteger)maxCount enterType:(NSInteger)enterType imageUrl:(NSString *)url title:(NSString *)title userMoney:(NSNumber *)money;

@property (nonatomic, copy) void(^ActionBlock)(NSInteger num,NSString *extraMoney);
@end

NS_ASSUME_NONNULL_END
