//
//  XPFavoriteButton.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/15.
//

#import "XPFavoriteButton.h"

@implementation XPFavoriteButton

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    
    
    
}

- (void)creatDIYButtonWithImageName:(NSString *)imageName buttonW:(CGFloat)buttonW title:(NSString *)title TitleColor:(UIColor *)color {
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageNamed:imageName];
    imageView.frame = CGRectMake(0, 3, self.mj_h - 6, self.mj_h - 6);
    self.fImageView = imageView;
    [self addSubview:imageView];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontRegular(12);
    titleLabel.textColor = color;
    titleLabel.frame = CGRectMake(MaxX(imageView) + 2, 0, 40, buttonW);
    titleLabel.text = title;
    self.fLabel = titleLabel;
    [self addSubview:titleLabel];
}

@end
