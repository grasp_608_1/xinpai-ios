//
//  XPProductDetailView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPProductDetailView : UIView

- (void)configWithtitleArray:(NSArray *)array valueArray:(NSArray *)valueArray;


@end

NS_ASSUME_NONNULL_END
