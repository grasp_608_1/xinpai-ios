//
//  XPMainTopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/4.
//

#import "XPMainTopView.h"
#import "XPLunboView.h"
#import "UIButton+WebCache.h"


@interface XPMainTopView ()<XPLunboViewDelegate>
//轮播图
@property (nonatomic, strong) XPLunboView *topView;

@property (nonatomic, strong) UIScrollView *selectView;

@property (nonatomic, copy)   NSArray *itemImageArray;

//当前选中了第几个分类
@property (nonatomic, assign) NSInteger currentIndex;

@end

@implementation XPMainTopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    CGFloat topImageH = 324*SCREEN_WIDTH/375.;
    self.currentIndex = 0;
    
    self.topView = [[XPLunboView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, topImageH)];
    self.topView.layer.shadowColor = RGBACOLOR(64, 61, 78, 0.1).CGColor;
    self.topView.layer.shadowOffset = CGSizeMake(0, 4);
    self.topView.layer.shadowOpacity = 1;
    self.topView.layer.shadowRadius = 4;
    self.topView.cornerRadius = 0;
    self.topView.lunDelegate = self;
    [self addSubview:self.topView];
    
    self.selectView = [UIScrollView new];
    self.selectView.bounces = NO;
    self.selectView.showsHorizontalScrollIndicator = NO;
    self.selectView.frame = CGRectMake(0, MaxY(self.topView), self.mj_w, 60);
    [self addSubview:self.selectView];
    
}

- (CGFloat)viewWithData:(NSDictionary *)dataDict{
    
    [self.topView setLunBoArray:dataDict[@"auctionRotationVOs"]];
    
    [self.selectView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    self.itemImageArray = dataDict[@"auctionSubjectVOs"];
    
    CGFloat imageW = 0;
    if (self.itemImageArray.count < 4) {
        imageW = self.mj_w/self.itemImageArray.count;
    }else{
        imageW = self.mj_w/3. - 10;
    }
    
    self.selectView.contentSize = CGSizeMake(imageW *self.itemImageArray.count, self.selectView.mj_h);
    for (int i = 0; i<self.itemImageArray.count; i++) {

        NSDictionary *dic = self.itemImageArray[i];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//        btn.backgroundColor = RandColor;
        [self.selectView addSubview:btn];
        btn.tag = i;
        [btn addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn sd_setImageWithURL:[NSURL URLWithString:dic[@"pic"]] forState:UIControlStateNormal];
        [btn sd_setImageWithURL:[NSURL URLWithString:dic[@"picBack"]] forState:UIControlStateSelected];
        btn.frame = CGRectMake(imageW*i, 0, imageW, 60);
    }
    
    if (self.itemImageArray.count <= self.currentIndex) {
        self.currentIndex = 0;
    }
    for (UIView * view in self.selectView.subviews) {
        
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            if (btn.tag == self.currentIndex) {
                [self itemButtonClicked:btn];
                break;
            }
        }
    }
    
    
    return 324*SCREEN_WIDTH/375. + 60;
}

#pragma mark  -- banner delegate
-(void)tapImageClick:(NSDictionary *)dic{
    if (self.bannerSelectBlock) {
        self.bannerSelectBlock(dic);
    }
}

#pragma mark -- item area clicked
- (void)itemButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
   //数据切换
    for (UIView * view in self.selectView.subviews) {
        
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            if (btn == sender) {
                self.currentIndex = sender.tag;
                btn.selected = YES;
                if (self.itemSelectBlock) {
                    self.itemSelectBlock(self.itemImageArray[sender.tag],sender.tag);
                }
            }else {
                btn.selected = NO;
            }
        }
        
    }
}


@end
