//
//  XPDefaultLabelCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPDefaultLabelCell : UITableViewCell

@property (nonatomic, strong) UIView *sepView;


+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (void)cellWithTitle:(NSString *)titleStr;

@end

NS_ASSUME_NONNULL_END
