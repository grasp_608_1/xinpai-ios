//
//  XPFilterView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPFilterView : UIView
@property (nonatomic, copy) void(^tapBlock)(NSInteger index,UIButton *sender);

@property (nonatomic, copy) NSArray <UIButton *>*buttonArray;
//只有选中具体分类后,才会更新其他按钮的状态
- (void)delayUpdateFliterUI;
//重置UI,默认选中第一个
- (void)resetUI;

@end

NS_ASSUME_NONNULL_END
