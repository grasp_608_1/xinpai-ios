//
//  XPSearchDefaultView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/4.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPSearchDefaultView : UIButton
//是否是输入状态  默认 no,结束输入返回no,输入状态yes
@property (nonatomic, copy) void(^searchStateBlock)(BOOL state);
//点击搜索回调
@property (nonatomic, copy) void(^searchBlock)(NSString *keyStr);

@property (nonatomic, strong) UIImageView *searchIcon;
//default is no
@property (nonatomic, assign) BOOL canSearch;
//默认搜索文字
@property (nonatomic, copy) NSString *defaultSearchStr;
//更新UI
-(void)updateFrame:(CGRect)frame;
@end

NS_ASSUME_NONNULL_END
