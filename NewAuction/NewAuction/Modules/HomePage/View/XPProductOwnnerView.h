//
//  XPProductOwnnerView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPProductOwnnerView : UIView

- (CGFloat)configWithTitle:(NSString *)titleStr listArray:(NSArray *)array;

@end

NS_ASSUME_NONNULL_END
