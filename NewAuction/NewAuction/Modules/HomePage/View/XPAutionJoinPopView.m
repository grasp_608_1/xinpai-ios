//
//  XPAutionJoinPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/15.
//

#import "XPAutionJoinPopView.h"
#import "XPAutionPopView.h"
#import "XPHitButton.h"
#import "XPNoMenuTextField.h"
@interface XPAutionJoinPopView ()<UITextFieldDelegate>
//产品图片
@property (nonatomic, strong) UIImageView *pImageView;
//产品名称
@property (nonatomic, strong) UILabel *pTitleLabel;

@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;
@property (nonatomic, strong) UILabel *label3;
@property (nonatomic, strong) UILabel *label4;

@property (nonatomic, strong) XPNoMenuTextField *numberTextField;

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UIView *countView;

@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UIButton *mButton;//减法
@property (nonatomic, strong) UIButton *addButton;//加法
@property (nonatomic, strong) UIButton *allButton;//全部
//单个价格
@property (nonatomic, assign) CGFloat price;
//最大数量
@property (nonatomic, assign) NSInteger max;
//当前数量
@property (nonatomic, assign) NSInteger count;
//账户余额
@property (nonatomic, assign) CGFloat userMoney;
@property (nonatomic, copy) NSString *needMoney;
@end

@implementation XPAutionJoinPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}



- (void)setupView{
    
    self.backgroundColor = UIColor.clearColor;
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    [self addSubview:bgView];
    
    
    CGFloat mianH = 220 + 40 + 8 + kBottomHeight;
    
    self.mainView = [UIView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h - mianH, SCREEN_WIDTH, mianH);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mainView.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(leftButtonClcked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    
    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(12, 60, 138, 138);
    self.pImageView.backgroundColor = RandColor;
    self.pImageView.layer.cornerRadius = 4;
    self.pImageView.layer.masksToBounds = YES;
    [self.mainView addSubview:_pImageView];
    
    CGFloat LabelX = MaxX(self.pImageView) + 12;
    CGFloat LabelW = self.mainView.mj_w - LabelX  - 12;
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontMedium(14);
    self.pTitleLabel.textColor = app_font_color;
    self.pTitleLabel.frame = CGRectMake(12, 12, self.mainView.mj_w - 12 - 12 - 24 - 5, 24);
    [self.mainView addSubview:_pTitleLabel];
    
    self.label1 = [UILabel new];
    self.label1.textColor = UIColorFromRGB(0x8A8A8A);
    self.label1.font = kFontRegular(14);
    self.label1.frame = CGRectMake(LabelX, self.pImageView.mj_y, LabelW, 24);
    [self.mainView addSubview:self.label1];
    
    
    self.label2 = [UILabel new];
    self.label2.font = kFontRegular(14);
    self.label2.textColor = UIColorFromRGB(0x8A8A8A);
    self.label2.frame = CGRectMake(LabelX, MaxY(self.label1) + 4, self.label1.mj_w, 24);
    [self.mainView addSubview:self.label2];
    
    
    self.label3 = [UILabel new];
    self.label3.textColor = UIColorFromRGB(0x8A8A8A);
    self.label3.font = kFontRegular(14);
    self.label3.frame = CGRectMake(LabelX, MaxY(self.label2) + 4, self.label1.mj_w, 24);
    [self.mainView addSubview:self.label3];
    
    self.label4 = [UILabel new];
    self.label4.textColor = UIColorFromRGB(0x8A8A8A);
    self.label4.font = kFontRegular(14);
    self.label4.frame = CGRectMake(LabelX, MaxY(self.label3) + 4, self.label1.mj_w, 24);
    [self.mainView addSubview:self.label4];
    
    CGFloat btnW = (self.mj_w - 15 - 20 *2)/2.;
    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,_mainView.mj_h - 40 - 8 - kBottomHeight, self.mj_w, 40);
    [self.mainView addSubview:bottomView];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setTitle:@"取消" forState:UIControlStateNormal];
    [leftButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    [leftButton setBackgroundColor:RGBACOLOR(110, 107, 255, 0.15)];
    leftButton.frame = CGRectMake(20, 4, btnW, 40);
    leftButton.layer.cornerRadius = 20;
    [leftButton addTarget:self action:@selector(leftButtonClcked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:leftButton];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [rightButton setBackgroundColor:UIColorFromRGB(0x6E6BFF)];
    rightButton.frame = CGRectMake(bottomView.center.x + 7.5, 4, btnW, 40);
    rightButton.layer.cornerRadius = 20;
    rightButton.layer.masksToBounds = YES;
    self.rightButton = rightButton;
    [rightButton addTarget:self action:@selector(rightButtonClcked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:rightButton];
    [rightButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :rightButton colors:
                                       @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                         (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    
    UILabel *label5 = [UILabel new];
    label5.textColor = UIColorFromRGB(0x8A8A8A);
    label5.font = kFontRegular(14);
    label5.frame = CGRectMake(LabelX, MaxY(self.label4) + 4, 28, 24);
    label5.text = @"数量";
    [self.mainView addSubview:label5];
    
    UIView *countView = [UIView new];
    countView.frame = CGRectMake(MaxX(label5) + 12, label5.mj_y, self.label1.mj_w, 24);
    self.countView = countView;
    [_mainView addSubview:countView];
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.backgroundColor = [UIColor whiteColor];
    [button1 addTarget:self action:@selector(mButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button1 setImage:[UIImage imageNamed:@"Group_99"] forState:UIControlStateNormal];
    button1.frame = CGRectMake(0, 0, 24, 24);
    self.mButton = button1;
    [countView addSubview:button1];
    
    self.numberTextField = [XPNoMenuTextField new];
    self.numberTextField.textColor = UIColorFromRGB(0x3A3C41);
    self.numberTextField.font = kFontMedium(16);
    self.numberTextField.frame = CGRectMake(MaxX(button1) + 4, 0, 28, 24);
    self.numberTextField.text = @"1";
    self.numberTextField.delegate = self;
    self.numberTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.numberTextField.textAlignment = NSTextAlignmentCenter;
    self.numberTextField.layer.cornerRadius = 4;
    self.numberTextField.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [countView addSubview:self.numberTextField];
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.backgroundColor = [UIColor whiteColor];
    [button2 addTarget:self action:@selector(aButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button2 setImage:[UIImage imageNamed:@"Group_100"] forState:UIControlStateNormal];
    button2.frame = CGRectMake(MaxX(self.numberTextField) + 3, 0, 24, 24);
    self.addButton = button2;
    [countView addSubview:button2];
    
    
    UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button3 addTarget:self action:@selector(allButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button3 setTitle:@"全部" forState:UIControlStateNormal];
    [button3 setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    button3.titleLabel.font = kFontRegular(14);
    button3.frame = CGRectMake(MaxX(button2) + 16, 0, 30, 24);
    self.allButton = button3;
    [countView addSubview:button3];
    
    self.count = 1;
}

- (void)mButtonClicked:(UIButton *)sender {
//    buttonCanUseAfterOneSec(sender);
    
    if (self.count <= 0) {
        self.numberTextField.text = @"0";
        self.count = 0;
    }else {
        self.count --;
        
        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.count];
    }
    [self showTotalPrice];
    [self showNeedPayMoney];
    [self layoutCountView];
}
////当前最大可参拍数量为0
- (void)aButtonClicked:(UIButton *)sender {
    
    if (self.max == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"当前最大可参拍数量为0" ToView:[UIApplication sharedApplication].keyWindow];
    }
    
    if (self.count >= self.max) {
        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.max];
        self.count = self.max;
    }else {
        self.count ++;
        
        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.count];
    }
    [self showTotalPrice];
    [self showNeedPayMoney];
    [self layoutCountView];
}

- (void)allButtonClicked:(UIButton *)sender {
    
    self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.max];
    self.count = self.max;
    [self showTotalPrice];
    [self showNeedPayMoney];
    [self layoutCountView];
}

- (void)layoutCountView {
    CGFloat numW = [self.numberTextField.text sizeWithAttributes:@{NSFontAttributeName:self.numberTextField.font}].width;
    if (numW <10) {
        numW = 10;
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.numberTextField.frame = CGRectMake(MaxX(self.mButton) + 4, 0, 20 + numW, 24);
        self.addButton.frame = CGRectMake(MaxX(self.numberTextField) + 4, 0, 24, 24);
        self.allButton.frame = CGRectMake(MaxX(self.addButton) + 17, 0, 30, 24);
    }];
}


- (void)showWithPrice:(NSNumber *)price maxCount:(NSInteger)maxCount enterType:(NSInteger)enterType imageUrl:(NSString *)url title:(NSString *)title userMoney:(NSNumber *)money{
    self.price = price.doubleValue * 1000;
    self.userMoney = money.doubleValue * 1000;
    self.max = maxCount;
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    self.pTitleLabel.text = title;
    
    self.countView.hidden = NO;
    [self.rightButton setTitle:@"参拍" forState:UIControlStateNormal];
    
    self.label1.text = [NSString stringWithFormat:@"拍品库存 %zd",maxCount];
    if (maxCount == 0) {
        self.count = 0;
        self.numberTextField.text = @"0";
    }else {
        self.count = 1;
        self.numberTextField.text = @"1";
    }
    NSString *totalMoneyStr = [XPMoneyFormatTool formatMoneyWithNum:money];
    NSString *totalMoney = [NSString stringWithFormat:@"保证金 ￥%@",totalMoneyStr];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalMoney];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:app_font_color} range:NSMakeRange(4, totalMoneyStr.length + 1)];
    self.label2.attributedText = attr;
    
    
    [self showTotalPrice];
    [self showNeedPayMoney];
    [self layoutCountView];
}

- (void)showTotalPrice {
    
    double totalMoneyf = self.price * self.count;
    NSString *totalMoneyStr = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithDouble:totalMoneyf/1000.]]];
    NSString *totalMoney = [NSString stringWithFormat:@"拍品总价 ￥%@",totalMoneyStr];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalMoney];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(5, totalMoneyStr.length + 1)];
    
    self.label3.attributedText = attr;
}

- (void)showNeedPayMoney {
    
    double totalMoneyf = self.price * self.count - self.userMoney;
    
    NSNumber *extraNumber = [NSNumber numberWithDouble:totalMoneyf/1000.];
    NSString *extraStr = [XPMoneyFormatTool formatMoneyWithNum:extraNumber];
    
    if (extraStr.doubleValue < 0) {
        totalMoneyf = 0;
    }
    
    NSString *totalMoneyStr = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithDouble:totalMoneyf/1000.]]];
    self.needMoney = totalMoneyStr;
    
    NSString *totalMoney = [NSString stringWithFormat:@"需补差价 ￥%@",totalMoneyStr];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalMoney];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(5, totalMoneyStr.length + 1)];
    
    self.label4.attributedText = attr;
}

- (void)leftButtonClcked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    
    [self removeFromSuperview];
}

- (void)rightButtonClcked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.ActionBlock) {
        self.ActionBlock(self.count,self.needMoney);
    }
}

#pragma mark --- textfieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == self.numberTextField) {
        if (self.numberTextField.text.length == 0) {
            self.count = 0;
            self.numberTextField.text = @"0";
            
            [self showTotalPrice];
            [self showNeedPayMoney];
            [self layoutCountView];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.numberTextField) {
        
        //限制只能输入：1234567890.
        NSCharacterSet * characterSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
        NSString * filtered = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        if (![string isEqualToString:filtered]) {
            return NO;
        }
        
        //首位是0
        if ([self.numberTextField.text isEqualToString:@"0"]) {
            if ([string isEqual:@"0"]) {
                return NO;
            }else{
                self.numberTextField.text = @"";
                return YES;
            }
        }
        
        // 获取当前文本框中的文本，包括用户输入的新字符
        NSString *currentText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        // 如果当前文本长度超过6位，则不允许输入更多字符
        NSInteger inputNum = [currentText integerValue];
        
        if (inputNum > self.max) {
            self.count = self.max;
            self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.count];
            
            [self showTotalPrice];
            [self showNeedPayMoney];
            [self layoutCountView];
            
            return NO;
        }else {
            self.count = inputNum;
            [self showTotalPrice];
            [self showNeedPayMoney];
            [self layoutCountView];
            return YES;
        }
    }
    // 允许输入
    return YES;
}



@end
