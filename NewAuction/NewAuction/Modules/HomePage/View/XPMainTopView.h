//
//  XPMainTopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/4.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPMainTopView : UIView
//轮播图点击
@property (nonatomic, copy) void(^bannerSelectBlock)(NSDictionary *dic);
//区域切换
@property (nonatomic, copy) void(^itemSelectBlock)(NSDictionary *dic, NSInteger index);
//数据源
- (CGFloat)viewWithData:(NSDictionary *)dataDict;

@end

NS_ASSUME_NONNULL_END
