//
//  XPProductImageView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPProductImageView : UIView
//单图同步获取图片高度
-(CGFloat)configWithImageUrl:(NSString *)url;

//单图异步获取图片高度
- (void)asynGetAsynNetImageSizeWithUrl:(NSString *)url compeletion:(void(^)(CGSize imageSize, UIImage *image))handler;
- (CGFloat)configWithLocalImage:(UIImage *)image Title:(NSString *)title;

//多图异步
- (CGFloat)configPicturesWithImageArray:(NSArray *)imagesArray Title:(NSString *)title;
//多图异步 不同样式title居左
- (CGFloat)configPicturesWithImageArray:(NSArray *)imagesArray Title:(NSString *)title skuName:(NSString *)skuNamel;

@end

NS_ASSUME_NONNULL_END
