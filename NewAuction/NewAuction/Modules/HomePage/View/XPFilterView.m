//
//  XPFilterView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import "XPFilterView.h"

@interface XPFilterView ()

@property (nonatomic, assign)NSInteger lastIndex;

@end

@implementation XPFilterView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    self.lastIndex = 1;
    
    NSArray *titleArray = @[@"推荐",@"销量",@"价格",@"全部"];
    
    CGFloat margin = 12;
    CGFloat btnW = (self.mj_w - 48 - (titleArray.count - 1) * 12)/titleArray.count;
    CGFloat btnH = self.mj_h;
    
    NSMutableArray *tempArray = [NSMutableArray array];
    
    for (int i = 0; i<titleArray.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:titleArray[i] forState:UIControlStateNormal];
        [self addSubview:btn];
        [tempArray addObject:btn];
        
        btn.frame = CGRectMake(24 + (margin + btnW) * i, 0, btnW, btnH);
        btn.tag = i + 1;
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if (i == 0) {
            [btn setImage:[UIImage imageNamed:@"Group_126"] forState:UIControlStateNormal];
            [btn setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontMedium(15);
            [btn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -(btnH + 30))];
            [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -(btnH ), 0, 0)];
            
        }else if (i == 2){
            [btn setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontRegular(14);
            [btn setImage:[UIImage imageNamed:@"Group_127"] forState:UIControlStateNormal];
            [btn setImage:[UIImage imageNamed:@"Group_128"] forState:UIControlStateSelected];
            [btn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -(btnH + 30))];
            [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -(btnH ), 0, 0)];
        }else if (i == 3){
            [btn setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontRegular(15);
            [btn setImage:[UIImage imageNamed:@"xp_filter"] forState:UIControlStateNormal];
            [btn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 2)];
            [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 2, 0, 0)];
        }
        
        
        else {
            [btn setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontRegular(14);
        }
    }
    self.buttonArray = tempArray.copy;
  
}

- (void)btnClicked:(UIButton *)sender {
    
    if (sender.tag == 4) {
        
        if (self.tapBlock) {
            self.tapBlock(sender.tag,sender);
        }
        return;
//        sender.selected = !sender.selected;
    }
    
    if (_lastIndex == 3 && sender.tag == 3) {
        sender.selected = !sender.selected;
        
        
    }else if (sender.tag == 3 && _lastIndex != 3){
        sender.selected = NO;
    }

    
    [self updateSeleteUIWithSender:sender];
    
    if (self.tapBlock) {
        self.tapBlock(sender.tag,sender);
    }
    
}

- (void)delayUpdateFliterUI{
    
    UIButton *fliterButton = [self.buttonArray objectAtIndex:3];
//    [self updateSeleteUIWithSender:fliterButton];
    fliterButton.selected = !fliterButton.selected;
}

- (void)updateSeleteUIWithSender:(UIButton *)sender {
    _lastIndex = sender.tag;
    
    for (UIView *btn  in self.subviews) {
        
        if ([btn isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)btn;
            if (btn.tag == 4) {
                continue;
            }
            if (sender.tag == button.tag) {
                [button setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
                button.titleLabel.font = kFontMedium(15);
            }else {
                [button setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
                button.titleLabel.font = kFontRegular(14);
            }
            
        }
    }
}


-(void)resetUI{
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self setupView];
    self.lastIndex = 1;
}

@end
