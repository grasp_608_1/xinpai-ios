//
//  XPProducPriceListView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import "XPProducPriceListView.h"

@implementation XPProducPriceListView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.backgroundColor = UIColor.whiteColor;
    self.clipsToBounds = YES;
    self.bounces = NO;
    self.showsVerticalScrollIndicator = NO;
}

- (CGFloat)configWithTitle:(NSString *)titleStr listArray:(NSArray *)array {
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat maxY = 0;
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(14);
    titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel.frame = CGRectMake(30, 8, self.mj_w - 30 -56, 24);
    titleLabel.text = titleStr;
//    [self addSubview:titleLabel];
    
    UILabel *priceLable = [UILabel new];
    priceLable.font = kFontMedium(14);
    priceLable.textColor = UIColorFromRGB(0x3A3C41);
    priceLable.textAlignment = NSTextAlignmentRight;
    priceLable.frame = CGRectMake(self.mj_w - 30 - 56, 8, 56, 24);
    priceLable.text = @"历史价格";

    CGFloat labelW = self.mj_w/2. - 30;
    for (int i = 0; i<array.count; i++) {
        NSDictionary *dic = array[i];
        
        UILabel *leftLabel = [UILabel new];
        leftLabel.font = kFontRegular(14);
        leftLabel.textColor = UIColorFromRGB(0x3A3C41);
        leftLabel.frame = CGRectMake(30,i*32,labelW, 32);
        leftLabel.text = dic[@"windupDate"] ?: @"";
        [self addSubview:leftLabel];
        
        UILabel *rightLabel = [UILabel new];
        rightLabel.font = kFontMedium(14);
        rightLabel.textColor = UIColorFromRGB(0x6E6BFF);
        rightLabel.textAlignment = NSTextAlignmentRight;
        rightLabel.frame = CGRectMake(self.mj_w/2., leftLabel.mj_y, labelW, 32);
        rightLabel.text = dic[@"windupPrice"] ?[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:dic[@"windupPrice"]]]: @"";
        [self addSubview:rightLabel];
        
        if (i== array.count - 1) {
            maxY = MaxY(rightLabel);
        }
    }
    if (array.count == 0) {
        maxY = 32;
        self.contentSize = CGSizeMake(self.mj_w, maxY);
    }else if (array.count<= 10){
        maxY = array.count * 32;
        self.contentSize = CGSizeMake(self.mj_w, maxY);
    }else {
        maxY = 32 * 10;
        self.contentSize = CGSizeMake(self.mj_w, array.count * 32);
    }
    
    return maxY + 8;
}


@end
