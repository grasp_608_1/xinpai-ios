//
//  XPLunboView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/23.
//

#import "XPLunboView.h"
#import <WebKit/WebKit.h>

@implementation XPLunboView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.cornerRadius = 20;
        [self createView];
    }
    return self;
}

- (void)createView
{
    self.imageArray = [NSMutableArray array];
    self.pagingEnabled = true;
    self.showsHorizontalScrollIndicator = false;
    self.delegate = self;
    self.isTimer = NO;
    
}

- (void)setLunBoArray:(NSArray *)lunBoArray
{
    if (_lunBoArray != lunBoArray) {
        _lunBoArray = lunBoArray;
        if (self.imageArray.count == lunBoArray.count + 2) {
            for (int i = 0; i < lunBoArray.count + 2; i ++) {
                UIImageView *imageView = self.imageArray[i];
                NSDictionary *dic = [self getDicWithIndex:i];
                [imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"pic"]]placeholderImage:self.placeholderImage];
            }
        }  else if (lunBoArray.count == 1)
        {
            self.contentSize = CGSizeMake(self.frame.size.width, 0);
            for (UIImageView *imageView in self.imageArray) {
                [imageView removeFromSuperview];
            }
            [self.imageArray removeAllObjects];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
            imageView.userInteractionEnabled = YES;
            imageView.tag = 2000;
            imageView.layer.cornerRadius = self.cornerRadius;
            imageView.layer.masksToBounds = YES;
            NSDictionary *dic = [_lunBoArray firstObject];
            [imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"pic"]]placeholderImage:self.placeholderImage];
            [self addSubview:imageView];
            [self.imageArray addObject:imageView];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageClick:)];
            [imageView addGestureRecognizer:tap];
            
        }  else if (lunBoArray.count)
        {
            self.contentSize = CGSizeMake(self.frame.size.width * (lunBoArray.count + 2), 0);
            for (UIImageView *imageView in self.imageArray) {
                [imageView removeFromSuperview];
            }
            [self.imageArray removeAllObjects];
            for (int i = 0; i < lunBoArray.count + 2; i ++) {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)];
                imageView.userInteractionEnabled = YES;
                imageView.tag = 2000 + i;
                imageView.layer.cornerRadius = self.cornerRadius;
                imageView.layer.masksToBounds = YES;
                NSDictionary *dic = [self getDicWithIndex:i];
                [imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"pic"]]placeholderImage:self.placeholderImage];
                [self addSubview:imageView];
                [self.imageArray addObject:imageView];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageClick:)];
                [imageView addGestureRecognizer:tap];
            }
        }
        if (lunBoArray.count > 1) {
            self.scrollEnabled = YES;
            [self setContentOffset:CGPointMake(self.frame.size.width, 0) animated:NO];
            [self addScrolTimer];
            self.isTimer = YES;
        } else
        {
            self.scrollEnabled = NO;
            [self setContentOffset:CGPointMake(0, 0) animated:NO];
            [self removeScrolTimer];
            self.isTimer = NO;
        }
    }
}

- (void)disappearCurrView
{
    if (self.lunBoArray.count > 1 && self.isTimer) {
        self.isTimer = NO;
    }
}

- (void)imageClick:(UITapGestureRecognizer *)tap
{
    buttonCanUseAfterOneSec(tap.view)
    UIImageView *imageView = (UIImageView*)tap.view;
    NSDictionary *dic;
    if (self.imageArray.count == 1) {
        dic = [_lunBoArray firstObject];
    } else
    {
        dic = [self getDicWithIndex:imageView.tag - 2000];
    }
    if (self.lunDelegate) {
        [self.lunDelegate tapImageClick:dic];
    } else {
        if (self.callBlock) {
            self.callBlock(dic);
        }
        
    }
    
}

- (NSDictionary*)getDicWithIndex:(NSInteger)i
{
    NSDictionary *dic;
    if (i == 0) {
        dic  = [_lunBoArray lastObject];
    } else if (i == _lunBoArray.count + 1)
    {
        dic  = [_lunBoArray firstObject];
    } else
    {
        dic = _lunBoArray[i - 1];
    }
    return dic;
}

- (void)runTimePage
{
    NSInteger index = self.contentOffset.x/self.frame.size.width;
    if (index == 0) {
        index = _lunBoArray.count;
        self.contentOffset = CGPointMake(index*self.frame.size.width, 0);
    }else if (index == _lunBoArray.count+1)
    {
        index = 1;
        self.contentOffset = CGPointMake(index*self.frame.size.width, 0);
    }
    index++;
    [self setContentOffset:CGPointMake(index*self.frame.size.width, 0) animated:true];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self removeScrolTimer];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self addScrolTimer];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger index = scrollView.contentOffset.x/self.frame.size.width;
    if (index == 0 && _lunBoArray.count > 1) {
        index = _lunBoArray.count;
        self.contentOffset = CGPointMake(index*self.frame.size.width, 0);
    }else if(index == _lunBoArray.count+1)
    {
        index = 1;
        self.contentOffset = CGPointMake(index*self.frame.size.width, 0);
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (!self.isTimer) {
        [self removeScrolTimer];
    }
}

- (void)addScrolTimer
{
    if (self.forbidScroll) {
        return;
    }
    [self.lunboTimer invalidate];
    self.lunboTimer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(runTimePage) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.lunboTimer forMode:NSRunLoopCommonModes];
    
}

- (void)removeScrolTimer
{
    [self.lunboTimer invalidate];
}

-(void)setStaticLuboArray:(NSArray *)staticLuboArray {

    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.contentOffset = CGPointMake(0, 0);
    self.contentSize = CGSizeMake(self.frame.size.width * staticLuboArray.count, 0);
    
    for (int i = 0; i < staticLuboArray.count; i ++) {
        NSDictionary *dic = staticLuboArray[i];
        if (dic[@"modelStatus"] ) {
            NSInteger modelType = [dic[@"modelStatus"] intValue];
    
            if (modelType == 0) {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)];
                imageView.userInteractionEnabled = YES;
                imageView.tag = 2000 + i;
                imageView.layer.cornerRadius = self.cornerRadius;
                imageView.layer.masksToBounds = YES;
                
                [imageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(dic[@"pic"])]placeholderImage:self.placeholderImage];
                [self addSubview:imageView];
                
            }else if (modelType == 1){
                WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(i * self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)];
                [self addSubview:webView];
                [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:TO_STR(dic[@"modelUrl"])]]];
            }else {
                
            }
//            
        }else {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)];
            imageView.userInteractionEnabled = YES;
            imageView.tag = 2000 + i;
            imageView.layer.cornerRadius = self.cornerRadius;
            imageView.layer.masksToBounds = YES;
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(dic[@"pic"]) ]placeholderImage:self.placeholderImage];
            [self addSubview:imageView];
        }
        
    }
}


@end
