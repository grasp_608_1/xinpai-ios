//
//  XPShopCollectionViewCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/25.
//

#import <UIKit/UIKit.h>
#import "XPShopModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCollectionViewCell : UICollectionViewCell
//商品图片
@property (nonatomic, strong) UIImageView *imageView;
//商品名称
@property (nonatomic, strong) UILabel *titleLabel;
//商品价格
@property (nonatomic, strong) UILabel *priceLabel;
//倒计时
@property (nonatomic, strong) UILabel *timeLabel;
//倒计时时间
@property (nonatomic, strong) UILabel *timeCount;

@property (nonatomic, strong) XPShopModel *model;

@end

NS_ASSUME_NONNULL_END
