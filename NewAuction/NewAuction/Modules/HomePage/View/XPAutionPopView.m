//
//  XPAutionPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/18.
//

#import "XPAutionPopView.h"
#import "XPAutionPopView.h"
#import "XPHitButton.h"
@interface XPAutionPopView ()


//产品图片
@property (nonatomic, strong) UIImageView *pImageView;
//产品名称
@property (nonatomic, strong) UILabel *pTitleLabel;

@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;
@property (nonatomic, strong) UILabel *label3;
@property (nonatomic, strong) UILabel *label4;

@property (nonatomic, strong) UILabel *numberLabel;

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UIView *countView;

@property (nonatomic, strong) UIButton *rightButton;
//单个价格
@property (nonatomic, assign) CGFloat price;
//最大数量
@property (nonatomic, assign) NSInteger max;
//当前数量
@property (nonatomic, assign) NSInteger count;
@end

@implementation XPAutionPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}



- (void)setupView{
    
    self.backgroundColor = UIColor.clearColor;
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    [self addSubview:bgView];
    
    
    CGFloat mianH = 220 + 40 + 8 + kBottomHeight;
    
    self.mainView = [UIView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h - mianH, SCREEN_WIDTH, mianH);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mainView.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(leftButtonClcked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    
    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(12, 60, 138, 138);
    self.pImageView.backgroundColor = RandColor;
    self.pImageView.layer.cornerRadius = 4;
    self.pImageView.layer.masksToBounds = YES;
    [self.mainView addSubview:_pImageView];
    
    CGFloat LabelX = MaxX(self.pImageView) + 12;
    CGFloat LabelW = self.mainView.mj_w - LabelX  - 12;
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontMedium(14);
    self.pTitleLabel.textColor = app_font_color;
    self.pTitleLabel.frame = CGRectMake(12, 12, self.mainView.mj_w - 12 - 12 - 24 - 5, 24);
    [self.mainView addSubview:_pTitleLabel];
    
    self.label1 = [UILabel new];
    self.label1.textColor = UIColorFromRGB(0x8A8A8A);
    self.label1.font = kFontRegular(14);
    self.label1.frame = CGRectMake(LabelX, self.pImageView.mj_y, LabelW, 24);
    [self.mainView addSubview:self.label1];
    
    
    self.label2 = [UILabel new];
    self.label2.font = kFontRegular(14);
    self.label2.textColor = UIColorFromRGB(0x8A8A8A);
    self.label2.frame = CGRectMake(LabelX, MaxY(self.label1) + 6, self.label1.mj_w, 24);
    [self.mainView addSubview:self.label2];
    
    
    self.label3 = [UILabel new];
    self.label3.textColor = UIColorFromRGB(0x8A8A8A);
    self.label3.font = kFontRegular(14);
    self.label3.frame = CGRectMake(LabelX, MaxY(self.label2) + 9, self.label1.mj_w, 24);
    self.label3.text = @"委拍数量";
    [self.mainView addSubview:self.label3];
    
    self.label4 = [UILabel new];
    self.label4.textColor = UIColorFromRGB(0x3A3C41);
    self.label4.font = kFontMedium(16);
    self.label4.frame = CGRectMake(MaxX(self.label3) + 12, MaxY(self.label2) + 9, 150, 24);
    self.label4.text = @"99";
    [self.mainView addSubview:self.label4];
    
    CGFloat btnW = (self.mj_w - 15 - 20 *2)/2.;
    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,_mainView.mj_h - 40 - 8 - kBottomHeight, self.mj_w, 40);
    [self.mainView addSubview:bottomView];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setTitle:@"取消" forState:UIControlStateNormal];
    [leftButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    [leftButton setBackgroundColor:RGBACOLOR(110, 107, 255, 0.15)];
    leftButton.frame = CGRectMake(20, 4, btnW, 40);
    leftButton.layer.cornerRadius = 20;
    [leftButton addTarget:self action:@selector(leftButtonClcked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:leftButton];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [rightButton setBackgroundColor:UIColorFromRGB(0x6E6BFF)];
    rightButton.frame = CGRectMake(bottomView.center.x + 7.5, 4, btnW, 40);
    rightButton.layer.cornerRadius = 20;
    rightButton.layer.masksToBounds = YES;
    self.rightButton = rightButton;
    [rightButton addTarget:self action:@selector(rightButtonClcked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:rightButton];
    [rightButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :rightButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    UIView *countView = [UIView new];
    countView.frame = CGRectMake(MaxX(_label3) + 12, _label3.mj_y, _mainView.mj_w - MaxX(_label3) - 12 -10, 24);
    self.countView = countView;
    [_mainView addSubview:countView];

}


- (void)showWithPrice:(NSNumber *)price 
           totalPrice:(NSNumber *)totalPrice
                count:(NSInteger)count
             imageUrl:(NSString *)url
                title:(NSString *)title
{
    
    self.price = price ? price.doubleValue : 0;
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    self.pTitleLabel.text = title;
    self.count = count;
    self.countView.hidden = YES;
    self.label4.hidden = YES;
    [self.rightButton setTitle:@"委拍" forState:UIControlStateNormal];
    
    
    NSString *countStr = [NSString stringWithFormat:@"委拍数量 %zd",count];
    NSMutableAttributedString *attr1 = [[NSMutableAttributedString alloc] initWithString:countStr];
    [attr1 addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:app_font_color} range:NSMakeRange(5, [NSString stringWithFormat:@"%zd",count].length)];
    self.label1.attributedText = attr1;
    
//    NSString *tempPriceStr = [XPMoneyFormatTool formatMoneyWithNum:price] ?: @"0";
//    NSString *priceStr = [NSString stringWithFormat:@"委拍单价 %@￥",tempPriceStr];
//    NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc] initWithString:priceStr];
//    [attr2 addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:app_font_color} range:NSMakeRange(5, tempPriceStr.length + 1)];
//    self.label2.attributedText = attr2;
    
    NSString *tempTotalPriceStr = [XPMoneyFormatTool formatMoneyWithNum:totalPrice];
    NSString *totalPriceStr = [NSString stringWithFormat:@"委拍总价 ￥%@",tempTotalPriceStr];
    NSMutableAttributedString *attr3 = [[NSMutableAttributedString alloc] initWithString:totalPriceStr];
    [attr3 addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(5, tempTotalPriceStr.length + 1)];
    self.label2.attributedText = attr3;
    self.label3.text = @"";
//    self.label3.attributedText = attr3;
    
}

- (void)showTotalPrice {
    CGFloat totalMoney = self.price * self.count;
    
    self.label2.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:totalMoney]]];
}

- (void)leftButtonClcked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    
    [self removeFromSuperview];
}

- (void)rightButtonClcked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.ActionBlock) {
        self.ActionBlock(self.count);
    }
}

@end
