//
//  XPFavoriteButton.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPFavoriteButton : UIButton

@property (nonatomic, strong) UIImageView *fImageView;
@property (nonatomic, strong) UILabel *fLabel;


- (void)creatDIYButtonWithImageName:(NSString *)imageName buttonW:(CGFloat)buttonW title:(NSString *)title TitleColor:(UIColor *)color;
@end

NS_ASSUME_NONNULL_END
