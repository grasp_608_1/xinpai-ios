//
//  XPDefaultLabelCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import "XPDefaultLabelCell.h"

@interface XPDefaultLabelCell ()

@property (nonatomic, strong) UILabel *label;

@end

@implementation XPDefaultLabelCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPDefaultLabelCell";
    XPDefaultLabelCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPDefaultLabelCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    self.clipsToBounds = YES;
    //分割线1
    UIView *sepView = [UIView new];
    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.sepView = sepView;
    self.sepView.frame = CGRectMake(10, 39, SCREEN_WIDTH - 20, 1);
    [self.contentView addSubview:sepView];
    
}

- (void)cellWithTitle:(NSString *)titleStr {
    
    self.textLabel.text = titleStr;
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    self.textLabel.font = kFontRegular(14);
    self.textLabel.textColor = UIColorFromRGB(0x3A3C41);

    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
