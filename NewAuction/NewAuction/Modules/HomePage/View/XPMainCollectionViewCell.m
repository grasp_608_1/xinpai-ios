//
//  XPMainCollectionViewCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/11.
//

#import "XPMainCollectionViewCell.h"

@interface XPMainCollectionViewCell ()

@property (nonatomic, strong)  XPWeakTimer *timer;

@property (nonatomic, assign) NSInteger currentSconds;

@end

@implementation XPMainCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
//        self.backgroundColor = UIColorFromRGB(0xF7F4FF);
        self.backgroundColor = UIColor.whiteColor;
        self.layer.cornerRadius = 4;
        self.clipsToBounds = YES;
        [self  setupViews];
        
    }
    
    return self;
}

-(void)setupViews
{
    CGFloat margin = 10;
    CGFloat cellW = self.bounds.size.width;
    self.imageView = [[UIImageView alloc] init];
    self.imageView.frame = CGRectMake(0, 0,cellW ,cellW);
    self.imageView.image = [UIImage imageNamed:@"xp_pd_default"];
    [self addSubview:self.imageView];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.frame = CGRectMake(10, MaxY(self.imageView) + 8, cellW - 2 * margin, 18);
    self.titleLabel.font = kFontMedium(14);
    self.titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    [self addSubview:self.titleLabel];
    
    self.priceLabel = [UILabel new];
    self.priceLabel.frame = CGRectMake(10,MaxY(self.titleLabel) + 6, cellW - 2 * 6, 22);
    self.priceLabel.font = kFontMedium(18);
    self.priceLabel.textColor = UIColorFromRGB(0xFF4A4A);
    [self addSubview:self.priceLabel];

    self.timeLabel = [UILabel new];
    self.timeLabel.frame = CGRectMake(10,MaxY(self.priceLabel) + 6, 72, 18);
    self.timeLabel.font = kFontRegular(12);
    self.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
    [self addSubview:self.timeLabel];
    
    self.timeCount = [UILabel new];
    self.timeCount.frame = CGRectMake(MaxX(self.timeLabel),MaxY(self.priceLabel) + 6, cellW - MaxX(self.timeLabel) -10, 18);
    self.timeCount.font = kFontMedium(14);
    self.timeCount.textColor = UIColorFromRGB(0x6E6BFF);
    [self addSubview:self.timeCount];
    
    _timer = [XPWeakTimer shareTimer];
}

-(void)cellWithModel:(XPProductModel *)model passedTime:(NSInteger)passedTime {
    [_timer stopTimer];
    self.model = model;
    self.titleLabel.text = model.name;
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.productPicSubject]] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    NSString *priceStr;
    if (model.isAuction.intValue == 1) {
        [self.timer stopTimer];
        
        self.timeCount.text = @"";
        
        self.timeLabel.text = @"开拍进行中";
        self.timeLabel.font = kFontMedium(12);
        self.timeLabel.textColor = UIColorFromRGB(0x6E6BFF);
        priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceWindupLatest]];
        
    }else if (model.isAuction.intValue == 2) {
        [self.timer stopTimer];
        
        self.timeCount.text = @"";
        
        self.timeLabel.text = @"敬请期待";
        self.timeLabel.font = kFontMedium(12);
        self.timeLabel.textColor = UIColorFromRGB(0x6E6BFF);
        priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceSaleSubject]];
    }else {
        
        self.timeLabel.font = kFontRegular(12);
        self.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
        self.timeLabel.text = @"开拍倒计时:";
        priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceCostSubject]];
        
        //计算时间差
        _currentSconds = (model.startTimeLong.integerValue - model.currentTimeLong.integerValue)/1000. - passedTime;
                
        NSString *hour = [self getHour];
        NSString *min = [self getMM];
        NSString *second = [self getSS];


        if (_currentSconds < 0) {
            self.timeCount.text = @"00:00:00";
        }else {
            self.timeCount.text = [NSString stringWithFormat:@"%@:%@:%@",hour,min,second];
            
            [_timer startTimerWithTime:1 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
        }
        priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceWindupLatest]];
    }
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(12)} range:NSMakeRange(0, 1)];
    
    self.priceLabel.attributedText = attr;
}

-(void)refreshTimer{//活动倒计时刷新
    
    if (self.currentSconds <=0) {//刷新首页
        [self.timer stopTimer];
        [[NSNotificationCenter defaultCenter] postNotificationName:Notification_homepage_fresh object:nil userInfo:@{@"status":@"0"}];
    }else{
        self.currentSconds --;
        NSString *hour = [self getHour];
        NSString *min = [self getMM];
        NSString *second = [self getSS];

        self.timeCount.text = [NSString stringWithFormat:@"%@:%@:%@",hour,min,second];
                
    }
    
}
- (NSString *)getHour{
    return [NSString stringWithFormat:@"%02ld",self.currentSconds/3600];
}
- (NSString *)getMM{
    return [NSString stringWithFormat:@"%02ld",(self.currentSconds%3600)/60];
}
- (NSString *)getSS{
    return [NSString stringWithFormat:@"%02ld",self.currentSconds%60];
}

@end
