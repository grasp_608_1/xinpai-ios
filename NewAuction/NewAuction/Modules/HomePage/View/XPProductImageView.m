//
//  XPProductImageView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import "XPProductImageView.h"
#import <ImageIO/ImageIO.h>

@implementation XPProductImageView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
        self.backgroundColor = UIColor.whiteColor;
    }
    return self;
}
- (void)setupView {
    
}

-(CGFloat)configWithImageUrl:(NSString *)url {
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGSize imageSize = [self getNetImageSizeWithUrl:url];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(14);
    titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel.frame = CGRectMake(30, 8, self.mj_w - 60, 24);
    titleLabel.text = @"拍品介绍";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLabel];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    
    CGFloat imageViewW = self.mj_w - 32;
    CGFloat imageH = 0;
    if (imageSize.height != 0) {
        imageH = imageSize.height *imageViewW/imageSize.width;
    }
    
    [self addSubview:imageView];
    imageView.frame = CGRectMake(16, MaxY(titleLabel) + 8, imageViewW, imageH);
    
    
    [imageView sd_setImageWithURL:[NSURL URLWithString:url]];
    
    CGFloat maxY = MaxY(imageView) + 12;
    
    return maxY;
}
//同步获取
- (CGSize)getNetImageSizeWithUrl:(NSString *)url {
    //@"http://www.myimageurl.com/image.png"
    NSMutableString *imageURL = [[NSMutableString alloc] initWithString:url];
    
    CGImageSourceRef source = CGImageSourceCreateWithURL((CFURLRef)[NSURL URLWithString:imageURL], NULL);
    NSDictionary* imageHeader = (__bridge NSDictionary*) CGImageSourceCopyPropertiesAtIndex(source, 0, NULL);
    
//    NSLog(@"Image header %@",imageHeader);
    CGFloat pixelHeight = [[imageHeader objectForKey:@"PixelHeight"] floatValue];
    CGFloat pixelWidth  = [[imageHeader objectForKey:@"PixelWidth"] floatValue];
    CGSize size = CGSizeMake(pixelWidth, pixelHeight);
   
    return size;
}

//单图同步获取
- (CGFloat)configWithLocalImage:(UIImage *)image Title:(NSString *)title{
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGSize imageSize = image.size;
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(14);
    titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel.frame = CGRectMake(30, 8, self.mj_w - 60, 24);
    titleLabel.text = title;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLabel];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    
    CGFloat imageViewW = self.mj_w - 32;
    CGFloat imageH = 0;
    if (imageSize.height != 0) {
        imageH = imageSize.height *imageViewW/imageSize.width;
    }
    
    [self addSubview:imageView];
    imageView.image = image;
    
    if (title.length == 0) {
        titleLabel.hidden = YES;
        imageView.frame = CGRectMake(16,0, imageViewW, imageH);
    }else {
        titleLabel.hidden = NO;
        imageView.frame = CGRectMake(16, MaxY(titleLabel) + 8, imageViewW, imageH);
    }
    
    CGFloat maxY = MaxY(imageView);
    
    return maxY;
}


//多图异步
- (CGFloat)configPicturesWithImageArray:(NSArray *)imagesArray Title:(NSString *)title{
    CGFloat maxY = 0;
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (title.length != 0) {
        UILabel *titleLabel = [UILabel new];
        titleLabel.font = kFontMedium(14);
        titleLabel.textColor = UIColorFromRGB(0x3A3C41);
        titleLabel.frame = CGRectMake(30, 8, self.mj_w - 60, 24);
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:titleLabel];
        maxY = MaxY(titleLabel) + 8;
    }
    
    for (int i = 0; i< imagesArray.count; i++) {
        NSDictionary *dic = imagesArray[i];
        CGFloat imageOriginW = [dic[@"width"] floatValue];
        CGFloat imageOriginH = [dic[@"height"] floatValue];
        
        CGFloat imageW = self.mj_w - 32;
        CGFloat imageH = 0;
        if (imageOriginH != 0) {
            imageH = imageOriginH *imageW/imageOriginW;
        }
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.frame = CGRectMake(16, maxY, imageW, imageH);
        [self addSubview:imageView];
        
        [imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"introImage"]]];
        maxY = maxY + imageH;
    }
    
    return maxY + 12;
}

//多图异步
- (CGFloat)configPicturesWithImageArray:(NSArray *)imagesArray Title:(NSString *)title skuName:(NSString *)skuName{
    CGFloat maxY = 0;
    
    if (title.length != 0) {
        UILabel *titleLabel = [UILabel new];
        titleLabel.font = kFontMedium(14);
        titleLabel.textColor = UIColorFromRGB(0x3A3C41);
        titleLabel.frame = CGRectMake(16, 8, self.mj_w - 60, 24);
        titleLabel.text = title;
        [self addSubview:titleLabel];
        maxY = MaxY(titleLabel) + 8;
    }
    
    if (skuName.length != 0) {
        UILabel *skuNameLabel = [UILabel new];
        skuNameLabel.font = kFontMedium(14);
        skuNameLabel.numberOfLines = NO;
        skuNameLabel.textColor = UIColorFromRGB(0x3A3C41);
        skuNameLabel.frame = CGRectMake(16, maxY, self.mj_w - 32, 0);
        CGFloat skuH = [skuName boundingRectWithSize:CGSizeMake(skuNameLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:skuNameLabel.font} context:nil].size.height;
        skuNameLabel.frame = CGRectMake(16, maxY, self.mj_w - 32, skuH);
        skuNameLabel.text = skuName;
        [self addSubview:skuNameLabel];
        maxY = MaxY(skuNameLabel) + 8;
    }
    
    for (int i = 0; i< imagesArray.count; i++) {
        NSDictionary *dic = imagesArray[i];
        CGFloat imageOriginW = [dic[@"width"] floatValue];
        CGFloat imageOriginH = [dic[@"height"] floatValue];
        
        CGFloat imageW = self.mj_w - 32;
        CGFloat imageH = 0;
        if (imageOriginH != 0) {
            imageH = imageOriginH *imageW/imageOriginW;
        }
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.frame = CGRectMake(16, maxY, imageW, imageH);
        [self addSubview:imageView];
        
        [imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"introImage"]]];
        maxY = maxY + imageH;
    }
    
    return maxY + 12;
}



@end
