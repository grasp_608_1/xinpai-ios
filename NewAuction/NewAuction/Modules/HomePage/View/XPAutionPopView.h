//
//  XPAutionPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPAutionPopView : UIView

/// 委拍信息
/// - Parameters:
///   - price: 单价
///   - totalPrice: 总价
///   - count: 委拍数量
///   - url: 商品图片地址
///   - title: 标题
- (void)showWithPrice:(NSNumber *)price
           totalPrice:(NSNumber *)totalPrice
                count:(NSInteger)count
             imageUrl:(NSString *)url
                title:(NSString *)title;

//委拍按钮点击
@property (nonatomic, copy) void(^ActionBlock)(NSInteger num);

@end

NS_ASSUME_NONNULL_END
