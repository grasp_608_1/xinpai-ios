//
//  XPLunboView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol XPLunboViewDelegate <NSObject>

- (void)tapImageClick:(NSDictionary*)dic;

@end

@interface XPLunboView : UIScrollView<UIScrollViewDelegate>

@property (nonatomic, strong) NSTimer *lunboTimer;

@property (nonatomic, strong) NSArray *lunBoArray;

@property (nonatomic, strong) NSMutableArray *imageArray;

@property (nonatomic, assign) BOOL isTimer;

@property (nonatomic, weak) id<XPLunboViewDelegate> lunDelegate;

@property (nonatomic, strong) UIImage *placeholderImage;

@property (nonatomic, assign) CGFloat cornerRadius;

@property (nonatomic, copy) void (^callBlock)(NSDictionary *dic);
//关闭自动轮播
@property (nonatomic, assign) BOOL forbidScroll;
//禁止轮播和自动滑动
@property (nonatomic, strong) NSArray *staticLuboArray;

- (void)disappearCurrView;


@end

NS_ASSUME_NONNULL_END
