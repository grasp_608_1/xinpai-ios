//
//  XPProductModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPProductModel : NSObject
//商品名
@property (nonatomic, copy) NSString *name;
//拍品图片地址
@property (nonatomic, copy) NSString *productPicSubject;
//商品图片地址
@property (nonatomic, copy) NSString *image;
//开始时间
@property (nonatomic, copy) NSString *timeStart;
//结束时间
@property (nonatomic, copy) NSString *timeEnd;
//是否在开拍中！：开拍中，0未开拍
@property (nonatomic, assign) NSNumber *isAuction;
//价格
@property (nonatomic, strong) NSNumber *priceWindupLatest;
//专题商品价
@property (nonatomic, strong) NSNumber *priceCostSubject;
//专题商品价
@property (nonatomic, strong) NSNumber *priceSaleSubject;
//商品ID
@property (nonatomic, strong) NSNumber *productId;
//商品skuID
@property (nonatomic, strong) NSNumber *productSkuId;
//拍品id
@property (nonatomic, strong) NSNumber *auctionItemId;
//拍品室ID
@property (nonatomic, strong) NSNumber *roomId;

@property (nonatomic, strong) NSNumber *auctionItemsId;
//开始拍卖时间
@property (nonatomic, strong) NSNumber *startTimeLong;
//当前服务器时间
@property (nonatomic, strong) NSNumber *currentTimeLong;
//结束拍卖时间
@property (nonatomic, strong) NSNumber *endTimeLong;




@end

NS_ASSUME_NONNULL_END
