//
//  XPAutionSubjectModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPAutionSubjectModel : NSObject

@property (nonatomic, copy) NSString *name;
//图片地址
@property (nonatomic, copy) NSString *pic;

@property (nonatomic, copy) NSString *picBack;
//列表
//@property (nonatomic, copy) NSArray *auctionSubjectContainProductVOs;
//二级分类列表
@property (nonatomic, copy) NSArray *mallSubjectProductVOs;

@end

NS_ASSUME_NONNULL_END
