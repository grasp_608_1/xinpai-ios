//
//  XPShopModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopModel : NSObject

@property (nonatomic, copy) NSString *introImage;
@property (nonatomic, copy) NSString *stockUnit;
@property (nonatomic, strong) NSString *spec;
@property (nonatomic, copy) NSArray *imageList;

@property (nonatomic, copy) NSString *pic;//商品图片
@property (nonatomic, copy) NSString *productName;//商品名称
@property (nonatomic, copy) NSString *skuName;//商品名称
@property (nonatomic, copy) NSString *attributeValues;//商品属性值拼接串
@property (nonatomic, copy) NSString *activityNote;//活动标签
@property (nonatomic, copy) NSString *freightNote;//邮费标签

@property (nonatomic, strong) NSNumber *productId;//商品id
@property (nonatomic, strong) NSNumber *productSkuId;//商品skuid
@property (nonatomic, strong) NSNumber *commentNum;//评论数
@property (nonatomic, strong) NSNumber *commentRate;//好评率
@property (nonatomic, strong) NSNumber *priceMarket;//市场价
@property (nonatomic, strong) NSNumber *priceCost;//销售价
@property (nonatomic, strong) NSNumber *priceSale;//销售价
@property (nonatomic, strong) NSNumber *pricePoint;//兑换所需积分
@property (nonatomic, strong) NSNumber *modelStatus;//商品类型 0普通商品 1 造物商品
@property (nonatomic, strong) NSNumber *pointStatus;//商品类型 0其他商品 1 积分商品
@property (nonatomic, strong) NSNumber *groupStatus;//拼团
@property (nonatomic, strong) NSNumber *presaleStatus;//预售

@property (nonatomic, strong) NSNumber *isShow;
@property (nonatomic, strong) NSNumber *merchantId;
@property (nonatomic, strong) NSNumber *postageBase;
@property (nonatomic, strong) NSNumber *isDel;
@property (nonatomic, strong) NSNumber *postageStep;
@property (nonatomic, strong) NSNumber *stock;
@property (nonatomic, strong) NSNumber *isPostage;
@property (nonatomic, strong) NSNumber *sales;//销量
@property (nonatomic, strong) NSNumber *sale;//推荐列表销量
@property (nonatomic, strong) NSNumber *auditStatus;
@property (nonatomic, strong) NSNumber *categoryId;

@end

NS_ASSUME_NONNULL_END
