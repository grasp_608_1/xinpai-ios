//
//  XPProductModel.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/11.
//

#import "XPProductModel.h"
#import "MJExtension.h"

@implementation XPProductModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"productId":@"id"};
}

@end
