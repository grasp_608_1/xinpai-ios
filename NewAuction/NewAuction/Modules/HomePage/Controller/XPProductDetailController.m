//
//  XPProductDetailController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import "XPProductDetailController.h"
#import "XPSelectItemView.h"
#import "XPProductDetailView.h"
#import "XPProducPriceListView.h"
#import "XPProductOwnnerView.h"
#import "XPProductImageView.h"
#import "XPFavoriteButton.h"
#import "XPCalendarEventManager.h"
#import "XPAutionPopView.h"
#import "XPAutionJoinPopView.h"
#import "XPWeakTimer.h"
#import "FormatTimerView.h"
#import "XPShareUMView.h"
#import "XPUMShareTool.h"
#import "XPPaymentView.h"
#import "XPIpTool.h"
#import "XPBankModel.h"

@interface XPProductDetailController ()

@property (nonatomic, strong)UIScrollView *mainView;

@property (nonatomic, copy) NSArray *productInfoArray;
@property (nonatomic, copy) NSArray *productInfoDetailArray;

@property (nonatomic, strong) XPProductDetailView *productInfo;
@property (nonatomic, strong) XPProducPriceListView *priceListView;
@property (nonatomic, strong) XPProductOwnnerView *ownnerView;
@property (nonatomic, strong) XPSelectItemView *selectView;

@property (nonatomic, strong) UIImageView *pImageView;
//最新落锤价
@property (nonatomic, strong) UILabel *priceLabel;
//上涨价格
@property (nonatomic, strong) UILabel *addPriceLabel;
@property (nonatomic, strong) UILabel *endTimeLabel;

@property (nonatomic, strong) XPFavoriteButton *favButton;

@property (nonatomic, strong)  UIButton *alertButton;

@property (nonatomic, strong) XPProductImageView *longImageView;

@property (nonatomic, assign) CGFloat imageH;
//拍品相关信息
@property (nonatomic, copy) NSDictionary *auctionDetailDic;
//上拍方
@property (nonatomic, copy) NSDictionary *auctionOwnnerDic;
//历史价格
@property (nonatomic, copy) NSArray *historyArray;
//拍卖室
@property (nonatomic, copy) NSDictionary *auctionRoomDic;
//商品表
@property (nonatomic, copy) NSDictionary *auctionProductDic;

@property (nonatomic, copy) NSDictionary *mainDic;

@property (nonatomic, assign) NSInteger currentIndex;
//开拍倒计时状态 1拍卖中/2等待开拍
@property (nonatomic, strong) NSNumber *countdownStatus;

@property (nonatomic, strong) XPAutionPopView *pop;
@property (nonatomic, strong) XPAutionJoinPopView *joinPop;

@property (nonatomic, strong)  XPWeakTimer *timer;

@property (nonatomic, assign) NSInteger currentSconds;

@property (nonatomic, strong) UILabel *countTimeLabel;

@property (nonatomic, strong) FormatTimerView *formatetimeView;
//选择银行卡后选中的银行信息
@property (nonatomic, strong) XPBankModel *bankModel;
//参拍数量
@property (nonatomic, assign) NSInteger jointotalCount;
//需补差价
@property (nonatomic, copy) NSString *extraMoneyValue;
//汇付支付支付id
@property (nonatomic, strong) NSString *paymentId;
//汇付支付订单号
@property (nonatomic, copy) NSString *orderId;
//支付
@property (nonatomic, strong)XPPaymentView *payView;
//用来参拍的账户余额
@property (nonatomic, strong) NSNumber *userMoney;

//验证码
@property (nonatomic, copy) NSString *codeStr;
//补差价订单号
@property (nonatomic, copy) NSString *sendyPayOrder;
//是否要查询支付结果
@property (nonatomic, assign) BOOL isPayShow;

@end

@implementation XPProductDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.currentIndex = 1;
    [self initData];
    [self setupUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)initData {
    self.productInfoArray = @[@"专场名称",@"数量",@"拍室号",@"付款时间",@"参数",@"起始价",@"拍价递增比",@"提货价",@"市场价"];
    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.isPayShow) {
        [self queryOrderStatus];
    }else{
        [self getData];
    }
}

-(void)queryOrderStatus{
    if (self.isPayShow) {
        self.isPayShow = NO;
        [[XPPaymentManager shareInstance] orderPayResultQueryWithType:SendayPayRechargePart orderId:self.sendyPayOrder controller:self requestResultBlock:^(XPRequestResult * _Nonnull data) {
            if (data.isSucess) {
                [[XPShowTipsTool shareInstance] showMsg:@"参拍中，可前往拍卖订单查看" ToView:k_keyWindow];
                [self getData];
            }else {
                if (data.code.intValue == 10000) {//支付查询结束,未完成支付
                    
                }else {//失败
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
                }
            }
        } shouldQuery:YES];
    }
}

- (void)applicationWillEnterForeground:(NSNotification *)noti {
    if ([self.navigationController.viewControllers lastObject] == self) {
        [self queryOrderStatus];
    }
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"商品详情";
    [self.view addSubview:self.navBarView];
}
- (void)setupUI {
    UIScrollView *mainView = [[UIScrollView alloc] init];
    mainView.frame = CGRectMake(0, kTopHeight, self.view.mj_w, SCREEN_HEIGHT - kTopHeight - 48 - self.safeBottom);
    mainView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    mainView.showsVerticalScrollIndicator = NO;
    mainView.bounces = NO;
    self.mainView = mainView;
    [self.view addSubview:mainView];
    
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = UIColor.whiteColor;
    topView.frame = CGRectMake(0, 10, SCREEN_WIDTH , 140);
    [self.mainView addSubview:topView];
    
    UIImageView *pImageView = [[UIImageView alloc] init];
    pImageView.backgroundColor = UIColor.whiteColor;
    pImageView.frame = CGRectMake(16, 0, topView.mj_h, topView.mj_h);
    self.pImageView = pImageView;
    [topView addSubview:pImageView];
    
    UILabel *priceLabelDes = [UILabel new];
    priceLabelDes.font = kFontRegular(12);
    priceLabelDes.textColor = UIColorFromRGB(0x8A8A8A);
    priceLabelDes.text = @"最新落槌价";
    priceLabelDes.frame = CGRectMake(MaxX(pImageView) + 6, 8, 65, 24);
    [topView addSubview:priceLabelDes];
    
    UILabel *priceLabel = [UILabel new];
    priceLabel.font = kFontMedium(20);
    priceLabel.textColor = UIColorFromRGB(0xFF4A4A);
    self.priceLabel = priceLabel;
    priceLabel.frame = CGRectMake(MaxX(priceLabelDes) + 6, 8, topView.mj_w - 6 - MaxX(priceLabelDes) - 2, 24);
    [topView addSubview:priceLabel];
    
    UIImageView *trendImageView = [[UIImageView alloc] init];
    trendImageView.image = [UIImage imageNamed:@"xp_trend"];
    trendImageView.frame = CGRectMake(priceLabel.mj_x + 4 , MaxY(priceLabel) + 6, 16, 16);
    [topView addSubview:trendImageView];
    
    UILabel *addPriceLabel = [UILabel new];
    addPriceLabel.font = kFontMedium(12);
    addPriceLabel.textColor = UIColorFromRGB(0x3A3C41);
    addPriceLabel.frame = CGRectMake(MaxX(trendImageView) + 6, MaxY(priceLabel) + 2, topView.mj_w - 6 - MaxX(trendImageView) - 2, 24);
    self.addPriceLabel = addPriceLabel;
    [topView addSubview:addPriceLabel];
    
    UILabel *endTimeLabelDes = [UILabel new];
    endTimeLabelDes.font = kFontRegular(12);
    endTimeLabelDes.textColor = UIColorFromRGB(0x8A8A8A);
    endTimeLabelDes.text = @"结束时间";
    endTimeLabelDes.frame = CGRectMake(MaxX(pImageView) + 6, MaxY(addPriceLabel), 65, 24);
    [topView addSubview:endTimeLabelDes];
    
    UILabel *endTimeLabel = [UILabel new];
    endTimeLabel.font = kFontMedium(12);
    endTimeLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.endTimeLabel = endTimeLabel;
    endTimeLabel.frame = CGRectMake(MaxX(endTimeLabelDes) + 6, MaxY(addPriceLabel), topView.mj_w - 6 - MaxX(endTimeLabelDes) - 2, 24);
    [topView addSubview:endTimeLabel];
    
    UILabel *countTimeLabelDes = [UILabel new];
    countTimeLabelDes.font = kFontRegular(12);
    countTimeLabelDes.textColor = UIColorFromRGB(0x8A8A8A);
    countTimeLabelDes.text = @"开拍倒计时";
    countTimeLabelDes.frame = CGRectMake(MaxX(pImageView) + 6, MaxY(endTimeLabel), 65, 24);
    [topView addSubview:countTimeLabelDes];
    
    UILabel *countTimeLabel = [UILabel new];
    countTimeLabel.font = kFontMedium(12);
    countTimeLabel.textColor = UIColorFromRGB(0x3A3C41);
    countTimeLabel.text = @"开拍进行中";
    countTimeLabel.frame = CGRectMake(MaxX(countTimeLabelDes) +6, MaxY(endTimeLabel), 80, 24);
    countTimeLabel.hidden = YES;
    self.countTimeLabel = countTimeLabel;
    [topView addSubview:countTimeLabel];
    
    _formatetimeView = [[FormatTimerView alloc] initWithFrame:CGRectMake(self.countTimeLabel.mj_x, self.countTimeLabel.mj_y + 4, self.countTimeLabel.mj_w, self.countTimeLabel.mj_h)];
    _formatetimeView.hidden = YES;
    [topView addSubview:_formatetimeView];
    
    
    XPFavoriteButton *favButton = [[XPFavoriteButton alloc] initWithFrame:CGRectMake(MaxX(pImageView) + 6, topView.mj_h - 30, 60, 24)];
    [favButton creatDIYButtonWithImageName:@"xp_product_uncollect" buttonW:24 title:@"收藏" TitleColor:UIColorFromRGB(0x8A8A8A)];
    self.favButton = favButton;
    [topView addSubview:favButton];
    
    UIButton *shareButton = [self creatDIYButtonWithImageName:@"xp_share" buttonW:24 title:@"分享" TitleColor:UIColorFromRGB(0x8A8A8A)];
    shareButton.frame = CGRectMake(MaxX(favButton) + 6, favButton.mj_y, 60, 24);
    [topView addSubview:shareButton];
    
    UIButton *alertButton = [self creatDIYButtonWithImageName:@"xp_tips" buttonW:24 title:@"提醒我" TitleColor:UIColorFromRGB(0x6E6BFF)];
    alertButton.frame = CGRectMake(MaxX(shareButton) + 6, favButton.mj_y, 60, 24);
    self.alertButton = alertButton;
    [topView addSubview:alertButton];
    
    [alertButton addTarget:self action:@selector(alertButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [favButton addTarget:self action:@selector(favButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    XPSelectItemView *selectItemView = [[XPSelectItemView alloc] initWithFrame:CGRectMake(0, MaxY(topView) + 10, SCREEN_WIDTH, 40)];
    [selectItemView configWithtitleArray:@[@"拍卖详情",@"上拍方",@"历史买价"]];
    [mainView addSubview:selectItemView];
    [selectItemView hiddenLine];
    [selectItemView showDownSepLine];
    self.selectView = selectItemView;
    selectItemView.backgroundColor = UIColor.whiteColor;
    XPWeakSelf
    [selectItemView setTapBlock:^(NSInteger index) {
        [weakSelf selectIndexWithIndex:index];
    }];
    
    XPProductDetailView *productInfo = [[XPProductDetailView alloc] initWithFrame:CGRectMake(0, MaxY(selectItemView), mainView.mj_w, 296)];
    productInfo.hidden = NO;
    self.productInfo = productInfo;
    [mainView addSubview:productInfo];
    
    XPProducPriceListView *priceListView = [[XPProducPriceListView alloc] initWithFrame:CGRectMake(0, MaxY(selectItemView),SCREEN_WIDTH , 0)];

    priceListView.hidden = YES;
    self.priceListView = priceListView;
    [mainView addSubview:priceListView];
    
    
    
    XPProductOwnnerView *priceOwnnerView = [[XPProductOwnnerView alloc] initWithFrame:CGRectMake(0, MaxY(selectItemView),SCREEN_WIDTH , 100)];
    
    priceOwnnerView.hidden = YES;
    self.ownnerView = priceOwnnerView;
    [mainView addSubview:priceOwnnerView];
    
    XPProductImageView *longImageView = [[XPProductImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0)];
    longImageView.frame = CGRectMake(0, MaxY(self.productInfo) + 10, SCREEN_WIDTH, 0);
    self.longImageView = longImageView;
    self.imageH = 0;
    [mainView addSubview:longImageView];
    
    self.mainView.contentSize = CGSizeMake(SCREEN_WIDTH, MaxY(self.longImageView));
    
    CGFloat buttonW = (self.view.mj_w - 60)/2;
    
    UIButton *joinButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [joinButton setTitle:@"委拍" forState:UIControlStateNormal];
    [joinButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    joinButton.titleLabel.font = kFontRegular(14);
    joinButton.layer.cornerRadius = 20;
    joinButton.backgroundColor = RGBACOLOR(110, 107, 255, 0.15);
    joinButton.frame = CGRectMake(20, SCREEN_HEIGHT - 40 - self.safeBottom - 4, buttonW, 40);
    [joinButton addTarget:self action:@selector(delegateButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:joinButton];
    
    UIButton *delegateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [delegateButton setTitle:@"参拍" forState:UIControlStateNormal];
    [delegateButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    delegateButton.titleLabel.font = kFontRegular(14);
    delegateButton.layer.cornerRadius = 20;
    delegateButton.layer.masksToBounds = YES;
    delegateButton.frame = CGRectMake(buttonW + 40, SCREEN_HEIGHT - 40 - self.safeBottom - 4, buttonW, 40);
    [delegateButton addTarget:self action:@selector(joinButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:delegateButton];
    
    [delegateButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :delegateButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    _timer = [XPWeakTimer shareTimer];
}

- (UIButton *)creatDIYButtonWithImageName:(NSString *)imageName buttonW:(CGFloat)buttonW title:(NSString *)title TitleColor:(UIColor *)color{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(10, 1, buttonW, buttonW);
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageNamed:imageName];
    imageView.frame = CGRectMake(0, 3, button.mj_w - 6, button.mj_w - 6);
    [button addSubview:imageView];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontRegular(12);
    titleLabel.textColor = color;
    titleLabel.frame = CGRectMake(MaxX(imageView) + 2, 0, 40, buttonW);
    titleLabel.text = title;
    [button addSubview:titleLabel];
    return button;
}

- (void)selectIndexWithIndex:(NSInteger)index {
    self.currentIndex = index;
    //计算布局
    if (index == 1) {
        self.productInfo.hidden = NO;
        self.priceListView.hidden = YES;
        self.ownnerView.hidden = YES;
        self.longImageView.frame = CGRectMake(0, MaxY(self.productInfo) + 10, SCREEN_WIDTH, self.imageH);
        self.mainView.contentSize = CGSizeMake(SCREEN_WIDTH, MaxY(self.longImageView));
    }else if (index == 2) {
        self.productInfo.hidden = YES;
        self.priceListView.hidden = YES;
        self.ownnerView.hidden = NO;
        self.longImageView.frame = CGRectMake(0, MaxY(self.ownnerView) + 10, SCREEN_WIDTH, self.imageH);
        self.mainView.contentSize = CGSizeMake(SCREEN_WIDTH, MaxY(self.longImageView));
    }else if (index == 3) {
        self.productInfo.hidden = YES;
        self.priceListView.hidden = NO;
        self.ownnerView.hidden = YES;
        self.longImageView.frame = CGRectMake(0, MaxY(self.priceListView) + 10, SCREEN_WIDTH, self.imageH);
        self.mainView.contentSize = CGSizeMake(SCREEN_WIDTH, MaxY(self.longImageView));
    }
}

#pragma mark -- 参拍
- (void)joinButtonClicked:(UIButton *)sender {
    
    if (![[XPVerifyTool shareInstance] checkLogin]) {
        [[XPVerifyTool shareInstance] userShouldLoginFirst];
    }else if (![[XPVerifyTool shareInstance] checkUserInventCode]){
        [self showInventAlert];
    }else if(![[XPVerifyTool shareInstance] checkUserAuthentication]){
        [[XPVerifyTool shareInstance] userShouldAuthentication];
    }else {
        sender.userInteractionEnabled = NO;
        if (self.countdownStatus.intValue == 2) {
            [[XPShowTipsTool shareInstance] showMsg:@"暂未到开拍时间" ToView:self.view];
            sender.userInteractionEnabled = YES;
            return;
        }else {
            
            NSString *urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host, kVERIFY_Get_joinNum];
            
            XPWeakSelf
            [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:@{@"id":self.auctionItemId} requestResultBlock:^(XPRequestResult * _Nonnull data) {
                sender.userInteractionEnabled = YES;
                if (data.isSucess) {
                    
                    XPAutionJoinPopView *pop = [[XPAutionJoinPopView alloc] initWithFrame:self.view.bounds];
                    [self.view addSubview:pop];
                    NSNumber *maxNumber = data.userData[@"count"];
//                    self.auctionDetailDic[@"itemAmount"];
                    self.userMoney = data.userData[@"countPrice"];
                    [pop showWithPrice:self.auctionDetailDic[@"priceWindupLatest"] maxCount:maxNumber.integerValue enterType:1 imageUrl:[NSString stringWithFormat:@"%@",self.auctionProductDic[@"image"]] title:self.auctionProductDic[@"name"] userMoney:self.userMoney];
                    [pop setActionBlock:^(NSInteger num, NSString * _Nonnull extraMoney) {
                        [weakSelf joinActionWithCount:num extraMoney:extraMoney];
                    }];
                    self.joinPop = pop;
                    
                }else {
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
                }
                
            }];
        }
    }
    
}
- (void)joinActionWithCount:(NSInteger)num extraMoney:(NSString *)extraMoney{
    self.jointotalCount = num;
    self.extraMoneyValue = extraMoney;
    if (num == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"拍品数量需大于0" ToView:[UIApplication sharedApplication].keyWindow];
        return;
    }
    
    [self.joinPop removeFromSuperview];
    
    if (extraMoney.floatValue > 0) {//需要补余额
        [self showPayView];
    }else {
        //余额充足,直接参拍
        [self postJoinAutionDirectly:YES];
    }
    
}

- (void)showPayView{

    XPPaymentView *payView = [[XPPaymentView alloc] initWithFrame:self.view.bounds];
    self.payView = payView;
    [self.view addSubview:payView];
    
    [payView viewWithNeedMoney:[NSNumber numberWithFloat:self.extraMoneyValue.floatValue] accountMoney:@(0) type:XPPaymentViewTypeJoin];
    
    XPWeakSelf
    [payView setNextBlock:^(NSInteger status, NSString * _Nonnull accountAmount) {
        [weakSelf orderPayWithPayType:status];
    }];
}
//三方支付支付
- (void)orderPayWithPayType:(NSInteger)payType{

    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_join_product];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"cashAmount"] = self.extraMoneyValue;
    paraM[@"amountPar"] = [NSNumber numberWithFloat:self.jointotalCount];
    paraM[@"auctionItemsId"] = self.auctionItemId;
    paraM[@"payType"] = @(payType);
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            if(payType == 1){
                [[XPPaymentManager shareInstance] showAlipayWithDic:data.userData viewController:self];
                self.isPayShow = YES;
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if (payType == 2) {//微信支付
                self.isPayShow = YES;
                [[XPPaymentManager shareInstance] showWeChatPayWithDic:data.userData viewController:self];
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if (payType == 11){
                if ([data.userData isKindOfClass:[NSDictionary class]]  && data.userData[@"fastPayUrl"]) {
                    self.isPayShow = YES;
                    self.sendyPayOrder = data.userData[@"orderNo"];
                    [[XPPaymentManager shareInstance] showPayViewWithUrl:data.userData[@"fastPayUrl"] viewController:self];
                }else{
                    [[XPShowTipsTool shareInstance] showMsg:@"服务器内部错误,请稍后重试" ToView:k_keyWindow];
                }
            }else if(data.code.intValue == 1){
                [self showInventAlert];
            }
        }else if(data.code.intValue == 1){
            [self showInventAlert];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
}




- (void)postJoinAutionDirectly:(BOOL)directly{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host, kVERIFY_join_product];
    NSDictionary *paraM = @{@"amountPar":@(self.jointotalCount),
                            @"auctionItemsId":self.auctionItemId,
                            @"payType":@(10)
    };
    
    if (directly) {
        [self.joinPop removeFromSuperview];
    }
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
            [self performSelector:@selector(getData) withObject:nil afterDelay:1];
            
        }else if(data.code.intValue == 1){
            [self showInventAlert];
        }else {
            [[XPShowTipsTool shareInstance] showLongDuringMsg:data.msg ToView:k_keyWindow];
        }
        
    }];
}

- (void)inputInventCodeWithNum:(NSString *)str {

    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_user_UpdateInventCode];
    NSDictionary *paraM = @{@"supInvCode":str};

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            XPLocalStorage *storage = [XPLocalStorage new];
            [storage writeUserSubInventCode:str];
            [[XPShowTipsTool shareInstance] showMsg:@"您已可以使用参拍功能" ToView:k_keyWindow];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
}
//弹出邀请码匡
- (void)showInventAlert {
        XPWeakSelf
        self.xpAlertView.autoDissmiss = NO;
        [self.xpAlertView configWithTitle:@"填写邀请码" desCriptionStr:@"" placeHolderStr:@"请填写邀请码" leftAction:^(NSString * _Nullable extra) {
            self.xpAlertView.autoDissmiss = YES;
            
        } rightAction:^(NSString * _Nullable extra) {
            if (extra.length == 0) {
                [[XPShowTipsTool shareInstance] showMsg:@"请输入邀请码" ToView:k_keyWindow];
            }else if(extra.length != 6){
                [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的邀请码" ToView:k_keyWindow];
            }else {
                [weakSelf inputInventCodeWithNum:extra];
                weakSelf.xpAlertView.autoDissmiss = YES;
                [weakSelf.xpAlertView dismiss];
            }
        } alertType:XPAlertTypeInput];
}

#pragma mark -- 委拍
- (void)delegateActionWithCount:(NSInteger)num {
    if (num == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"当前无可委拍拍品" ToView:[UIApplication sharedApplication].keyWindow];
        return;
    }
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host, kVERIFY_delegate_product];
    
    NSDictionary *paraM = @{@"number":@(num),
                            @"auctionItemId":self.auctionItemId};
    
    
    [self.pop removeFromSuperview];
    [self startLoadingGif];
    
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            [[XPShowTipsTool shareInstance] showMsg:@"委拍成功" ToView:k_keyWindow];
            
            [self performSelector:@selector(getData) withObject:nil afterDelay:1];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:[UIApplication sharedApplication].keyWindow];
        }
        
    }];
}
- (void)delegateButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender)
    
    if (![[XPVerifyTool shareInstance] checkLogin]) {
        [[XPVerifyTool shareInstance] userShouldLoginFirst];
    }else {
        sender.userInteractionEnabled = NO;
            NSString *urlStr = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host, kVERIFY_Get_DelegateNum,self.auctionItemId];
            
            XPWeakSelf
            [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
                sender.userInteractionEnabled = YES;
                if (data.isSucess) {
                    
                    XPAutionPopView *pop = [[XPAutionPopView alloc] initWithFrame:self.view.bounds];
                    [k_keyWindow addSubview:pop];
                    NSNumber *maxNumber = data.userData[@"sum"];;
                    NSNumber *price = data.userData[@"singlePrice"];
                    NSNumber *totalPrice = data.userData[@"countPrice"];
                    [pop showWithPrice:price 
                            totalPrice:totalPrice
                                 count:maxNumber.intValue
                              imageUrl:self.auctionProductDic[@"image"]
                                 title:weakSelf.auctionProductDic[@"name"]];

                    [pop setActionBlock:^(NSInteger num) {
                        [weakSelf delegateActionWithCount:num];
                    }];
                    weakSelf.pop = pop;
                    
                }else {
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
                }
                
            }];
        
    }
}

-(void)refreshminTimer{//直播课倒计时刷新
    if (self.currentSconds <= 0) {//刷新页面
        [self getData];
    }else{
        self.currentSconds --;
        self.formatetimeView.seconds = self.currentSconds;
    }
    
}

#pragma mark -- 收藏相关
- (void)favButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender)
    
    if (![[XPVerifyTool shareInstance] checkLogin]) {
        [[XPVerifyTool shareInstance] userShouldLoginFirst];
    }else {
        if (self.favButton.selected == YES) {
            [self deleteFavoriteProduct];
        }else {
            [self addFavoriteProduct];
        }
    }
    
}
//添加收藏
- (void)addFavoriteProduct {
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kVERIFY_Get_fav_add,self.auctionItemId];

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:nil requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.favButton.fImageView.image = [UIImage imageNamed:@"xp_product_collected"];
            self.favButton.fLabel.text = @"已收藏";
            self.favButton.fLabel.textColor = UIColorFromRGB(0x6E6BFF);
            self.favButton.selected = YES;
            [[XPShowTipsTool shareInstance] showMsg:@"收藏成功" ToView:self.view];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
        
    }];
}
//取消收藏
- (void)deleteFavoriteProduct {
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kVERIFY_Get_fav_delete,self.auctionItemId];

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.favButton.fImageView.image = [UIImage imageNamed:@"xp_product_uncollect"];
            self.favButton.fLabel.text = @"收藏";
            self.favButton.fLabel.textColor = UIColorFromRGB(0x8A8A8A);
            self.favButton.selected = NO;
            [[XPShowTipsTool shareInstance] showMsg:@"已取消收藏" ToView:self.view];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

#pragma mark -- 分享相关
- (void)shareButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
                            
    bool installWechat=  [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession];
    
    if (!installWechat) {
        [[XPShowTipsTool shareInstance] showMsg:@"未安装微信,请先安装微信" ToView:self.view];
        return;
    }
    
    [self getShareUrl];
    
}
- (void)getShareUrl {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_product_share];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:nil requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            //分享
            NSDictionary *shareDic = data.userData;
            if (!shareDic || shareDic.allKeys.count == 0) {
                //后台数据不对,暂不拉起
            }else {
                [self shareWithdata:shareDic];
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
//使用友盟sdk
- (void)shareWithdata:(NSDictionary *)shareDic {
    
    XPShareUMView *shareView = [[XPShareUMView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
    [[UIApplication sharedApplication].keyWindow addSubview:shareView];
    [shareView show];
    
    XPWeakSelf
    [shareView setActionBlock:^(NSInteger index) {
        if (index == 1) {//分享好友
            
            [weakSelf shareWithPlatform:UMSocialPlatformType_WechatSession shareWithdata:shareDic];
        }else if (index == 2){
            [weakSelf shareWithPlatform:UMSocialPlatformType_WechatTimeLine shareWithdata:shareDic];
        }
        
    }];
}

- (void)shareWithPlatform:(UMSocialPlatformType)platformType shareWithdata:(NSDictionary *)shareDic{
    
    [[XPUMShareTool shareInstance] shareWebUrlWithPlatform:platformType title:shareDic[@"title"]?: @"分享"
                                                     descr:shareDic[@"content"]?:@"新拍分享链接"
                                              webUrlString:shareDic[@"shareAddress"]?:@""
                                     currentViewController:self
                                                completion:^(id result, NSError *error) {
        
        if(error){
        NSLog(@"************Share fail with error %@*********",error);
    //        [[XPShowTipsTool shareInstance] showMsg:@"分享失败" ToView:self.view];
        }else{
        NSLog(@"response data is %@",result);
    //        [[XPShowTipsTool shareInstance] showMsg:@"分享成功" ToView:self.view];
        }
    }];
}


#pragma mark -- 日历提醒
- (void)alertButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    NSString *startStampStr = [self getTimeStampWithTime:self.mainDic[@"lastSessionTime"] dataFormate:@"YYYY-MM-dd HH:mm:ss"];
    
    NSString *nowStampStr = [self getLocalNowStampStr];
    long diff = startStampStr.longLongValue - nowStampStr.longLongValue;
    
    if (self.countdownStatus.intValue == 1) {
        [[XPShowTipsTool shareInstance] showMsg:@"拍品已经开拍!" ToView:self.view];
    }else if (diff <= 120000){
        [[XPShowTipsTool shareInstance] showMsg:@"距离开拍时间小于两分钟!" ToView:self.view];
    }else {
        //判断日历中是否已存在本次提醒
        
        NSString *calendarId = [NSString stringWithFormat:@"calendar_%@_%@",self.auctionItemId,startStampStr];
        
        if ([[XPCalendarEventManager sharedInstance]checkCalendarIsExistWithId:calendarId]) {
            [[XPShowTipsTool shareInstance] showMsg:@"当前日历已存在,请勿重复添加!" ToView:self.view];
        }else {
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *startDate = [formatter dateFromString:self.mainDic[@"lastSessionTime"]];
            NSDate *endDate = [formatter dateFromString:self.mainDic[@"lastSessionTimeEnd"]];
            NSString *locationStr = [NSString stringWithFormat:@"距离 %@ 开拍还有两分钟哦~",self.auctionProductDic[@"name"]];
            
            [[XPCalendarEventManager sharedInstance] createEventCalendarTitle:@"开拍提醒" location:locationStr startDate:startDate endDate:endDate allDay:NO endTimeoffset:-120 calendarId:calendarId];
        }
    }
}

#pragma mark -- 页面数据源
- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kProduct_detail];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] =[NSString stringWithFormat:@"%@",self.auctionItemId];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.mainDic = data.userData;
            self.auctionDetailDic = data.userData[@"auctionDetailItemsVo"];//拍品表
            self.historyArray =  data.userData[@"auctionDetailPriceRoomVo"];//历史价
            self.auctionOwnnerDic = data.userData[@"auctionDetailMerchantVo"];//商家信息
            self.auctionRoomDic = data.userData[@"auctionDetailRoomVo"];//拍卖师表
            self.auctionProductDic = data.userData[@"auctionDetailProductVo"];
            self.countdownStatus = data.userData[@"countdownStatus"];
            [self refreshData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)refreshData {
    
    [self.timer stopTimer];
    NSString *introStr = self.auctionProductDic[@"introImage"] ?:@"";
    NSString *productStr = self.auctionProductDic[@"image"] ?:@"";
    NSArray *imageIntroArray = self.auctionProductDic[@"imageList"];
    
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:productStr] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    NSString *priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.auctionDetailDic[@"priceWindupLatest"]?:@(0)]];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(12)} range:NSMakeRange(0, 1)];
    
    self.priceLabel.attributedText = attr;
    self.addPriceLabel.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.mainDic[@"email"]?:@(0)]];
    self.endTimeLabel.text = [NSString stringWithFormat:@"%@",self.auctionDetailDic[@"endTime"]?[self formateTimeWithTimeString:self.auctionDetailDic[@"endTime"]]:@""];
    //开拍倒计时
    
    NSNumber *countDownStatus = self.mainDic[@"countdownStatus"];
    if (countDownStatus.integerValue == 1) {//开拍中
        self.countTimeLabel.hidden = NO;
        self.formatetimeView.hidden = YES;
    }else {
        self.countTimeLabel.hidden = YES;
        self.formatetimeView.hidden = NO;
        //获取时间差
        NSString *now = [self getTimeStampWithTime:self.mainDic[@"now"] dataFormate:@"YYYY-MM-dd HH:mm:ss"];
        NSString *start = [self getTimeStampWithTime:self.mainDic[@"lastSessionTime"] dataFormate:@"YYYY-MM-dd HH:mm:ss"];

        NSInteger timeCompare = start.integerValue - now.integerValue;
        if (timeCompare > 0 && self.countdownStatus.intValue == 2) {
            self.currentSconds = timeCompare/1000.;
            self.formatetimeView.seconds = self.currentSconds;
            //开始倒计时
            [self.timer startTimerWithTime:1 target:self selector:@selector(refreshminTimer) userInfo:nil repeats:YES];
        }
        
    }
    
//    @[@"专场名称",@"数量",@"拍室号",@"付款时间",@"参数",@"起始价",@"拍价递增比",@"提货价",@"市场价"];
    //专场名称
    NSString *name = self.auctionRoomDic[@"name"]?:@"";
    //数量
    NSString *countNum = self.auctionDetailDic[@"itemAmount"] ?[NSString stringWithFormat:@"%@",self.auctionDetailDic[@"itemAmount"]]: @"";
    //拍室号
    NSString *roomID = self.auctionRoomDic[@"roomNo"] ?: @"";
    //付款时间
    NSNumber *payTimeStatus = self.auctionDetailDic[@"payTime"];
    NSString *payTime = payTimeStatus.integerValue == 0 ? @"竞拍成功，落槌即付" :@"暂无";
    //参数
    NSString *productSpec = self.auctionProductDic[@"spec"] ?: @"";
    //起始价
    NSString *startPrice = self.auctionDetailDic[@"priceStart"] ?[NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:self.auctionDetailDic[@"priceStart"]]]: @"";
    //拍价递增比
    NSNumber *priceIncrStr = self.auctionDetailDic[@"priceIncr"] ?self.auctionDetailDic[@"priceIncr"] :@(0);
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.minimumFractionDigits = 0;
    formatter.maximumFractionDigits = 2;
        
    priceIncrStr = [NSNumber numberWithFloat:[priceIncrStr floatValue] * 100.];
    NSString *priceIncr = [NSString stringWithFormat:@"%@%%",[formatter stringFromNumber:priceIncrStr]];
    //提货价
    NSString *endPrice =    self.auctionDetailDic[@"pricePick"] ?[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.auctionDetailDic[@"pricePick"]]]: @"";
    //市场价
    NSString *marketPrice =    self.auctionProductDic[@"priceMarket"] ?[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.auctionProductDic[@"priceMarket"]]]: @"";
    
    NSArray *tempArray = @[name,countNum,roomID,payTime,productSpec,startPrice,priceIncr,endPrice,marketPrice];
    NSMutableArray *muArray = [NSMutableArray array];
    for (int i = 0; i<tempArray.count; i++) {
        [muArray addObject:[NSString stringWithFormat:@"%@",tempArray[i]]];
    }
    self.productInfoDetailArray = [muArray copy];
    
    [self.productInfo configWithtitleArray:self.productInfoArray valueArray:self.productInfoDetailArray];
    
    CGFloat priceHeight = [self.priceListView configWithTitle:self.auctionProductDic[@"name"]?:@"" listArray:self.historyArray];
    
    NSString *ownName = self.auctionOwnnerDic[@"companyName"] ?:@"";
    NSString *ownLeader = self.auctionOwnnerDic[@"leader"] ?:@"";
    NSString *ownPhone = self.auctionOwnnerDic[@"phone"] ?:@"";
    NSArray *ownDicArray = @[ownName,ownLeader,ownPhone];
    
    CGFloat ownHeight =  [self.ownnerView configWithTitle:@"上拍方" listArray:ownDicArray];
    
    self.priceListView.frame = CGRectMake(0, MaxY(self.selectView),SCREEN_WIDTH , priceHeight);
    self.ownnerView.frame = CGRectMake(0, MaxY(self.selectView),SCREEN_WIDTH , ownHeight);
    
    [self selectIndexWithIndex:self.currentIndex];
    
    NSNumber *favoriteStatus = self.mainDic[@"favoriteStatus"];
    //收藏按钮
    if (favoriteStatus.integerValue == 0) {
        self.favButton.fImageView.image = [UIImage imageNamed:@"xp_product_uncollect"];
        self.favButton.fLabel.text = @"收藏";
        self.favButton.fLabel.textColor = UIColorFromRGB(0x8A8A8A);
        self.favButton.selected = NO;
    }else {
        self.favButton.fImageView.image = [UIImage imageNamed:@"xp_product_collected"];
        self.favButton.fLabel.text = @"已收藏";
        self.favButton.fLabel.textColor = UIColorFromRGB(0x6E6BFF);
        self.favButton.selected = YES;
    }
    
    NSString *startStampStr = [self getTimeStampWithTime:self.mainDic[@"lastSessionTime"] dataFormate:@"YYYY-MM-dd HH:mm:ss"];
    
    NSString *nowStampStr = [self getTimeStampWithTime:self.mainDic[@"now"] dataFormate:@"YYYY-MM-dd HH:mm:ss"];
    long diff = startStampStr.longLongValue - nowStampStr.longLongValue;
    
    if (self.countdownStatus.intValue == 1) {
        self.alertButton.hidden = YES;
    }else if (diff <= 120000){
        self.alertButton.hidden = YES;
    }else {
        self.alertButton.hidden = NO;
    }
    
    self.imageH = [self.longImageView configPicturesWithImageArray:imageIntroArray Title:@"拍品介绍" skuName:TO_STR(self.auctionProductDic[@"name"])];
    CGFloat imageY = 0;
    if (self.currentIndex == 1) {
        imageY = MaxY(self.productInfo) + 10;
    }else if (self.currentIndex == 2){
        imageY = MaxY(self.ownnerView) + 10;
    }else{
        imageY = MaxY(self.priceListView) + 10;
    }
    self.longImageView.frame = CGRectMake(0,imageY, SCREEN_WIDTH, self.imageH);
    self.mainView.contentSize = CGSizeMake(SCREEN_WIDTH, MaxY(self.longImageView));
}

- (void)rechargeMoneyQuery{
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kVERIFY_join_extraPay_comfirmQuery,self.paymentId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        
        if (data.isSucess) {
            if (data.userData) {
                [self stopLoadingGif];
                [[XPShowTipsTool shareInstance] showLongDuringMsg:@"参拍中，请稍后查看参拍结果" ToView:k_keyWindow];
                [self performSelector:@selector(getData) withObject:nil afterDelay:1];
            }
        }else if(data.code.intValue == 10000){//pengding
            [self performSelector:@selector(rechargeMoneyQuery) withObject:nil afterDelay:2.0];
         }else {
            [self stopLoadingGif];
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

#pragma mark -- 其他
- (NSString *)getTimeStampWithTime:(NSString *)time dataFormate:(NSString *)dataFormateStr {
    
    if (![time isKindOfClass:[NSString class]] || time.length == 0) {
        return @"0";
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:dataFormateStr]; //设定时间的格式
    NSDate *tempDate = [dateFormatter dateFromString:time];//将字符串转换为时间对象
    NSString *timeStr = [NSString stringWithFormat:@"%ld", (long)[tempDate timeIntervalSince1970]*1000];
    return timeStr;
}

- (NSString *)getLocalNowStampStr {
    NSDate *tempDate = [NSDate now];//将字符串转换为时间对象
    NSString *timeStr = [NSString stringWithFormat:@"%ld", (long)[tempDate timeIntervalSince1970]*1000];
    return timeStr;
}

- (NSString *)formateTimeWithTimeString:(NSString *)timeStr {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:timeStr];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *targetDateString = [dateFormatter stringFromDate:date];
   
    return targetDateString;
}

-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
