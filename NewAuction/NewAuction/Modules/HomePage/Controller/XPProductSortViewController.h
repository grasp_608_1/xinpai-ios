//
//  XPProductSortViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPProductSortViewController : XPBaseViewController
// 0 显示标题 不显示搜索 显示显示分类  1 显示搜索 不显示标题 不显示筛选 2 显示标题 不显示搜索 不显示分类
@property (nonatomic, assign) NSInteger entryType;

@end

NS_ASSUME_NONNULL_END
