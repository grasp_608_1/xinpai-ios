//
//  XPProductDetailController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import "XPBaseViewController.h"
#import "XPProductModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPProductDetailController : XPBaseViewController

@property (nonatomic, strong) NSNumber *auctionItemId;

@end

NS_ASSUME_NONNULL_END
