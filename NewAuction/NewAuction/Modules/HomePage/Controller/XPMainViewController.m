//
//  XPMainViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "XPMainViewController.h"
#import "XPNavigationController.h"
#import "XPMainCollectionViewCell.h"
#import "XPProductDetailController.h"
#import "XPShopingController.h"
#import "XPWeakTimer.h"
#import "XPBaseWebViewViewController.h"
#import "XPProductSortViewController.h"
#import "XPMainTopView.h"
#import "XPSearchDefaultView.h"
#import "XPAutionSubjectModel.h"
#import "XPShopGoodsDetailController.h"

@interface XPMainViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UIScrollView *mainView;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) XPMainTopView *topView;
//列表数据源
@property (nonatomic, strong) NSArray *dataArray;

@property (nonatomic, strong) XPWeakTimer *mainTimer;

@property (nonatomic, assign) NSInteger passedTime;

@end

@implementation XPMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    [self buildCollectionView];
    [self initMainTimer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData) name:Notification_homepage_fresh object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:[UIApplication sharedApplication]];
}

-(void)buildCollectionView{
    CGFloat margin = 13;
    CGFloat itemW = (SCREEN_WIDTH - margin*3)/2.;
    CGFloat itemH = 258;
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(itemW,itemH);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    CGRect rect = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    self.collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
    [self.collectionView registerClass:[XPMainCollectionViewCell class] forCellWithReuseIdentifier:@"XPMainCollectionViewCell"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"collectionCellHeader"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"collectionCellFooter"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = UIColorFromRGB(0xF7F4FF);
    self.collectionView.bounces = NO;
    self.collectionView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.collectionView];
    
    CGFloat topImageH = 324*SCREEN_WIDTH/375.;

    self.topView = [[XPMainTopView alloc] initWithFrame:CGRectMake(0, -topImageH - 60 , self.collectionView.mj_w, topImageH + 60)];
    [self.collectionView addSubview:self.topView];
    [self.topView viewWithData:@{}];
    [self topCallBack];
    self.collectionView.contentInset = UIEdgeInsetsMake(topImageH + 60, 0, 0, 0);
    
}

#pragma mark -- UICollectionView dataSorce
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.dataArray.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    XPAutionSubjectModel *model = self.dataArray[section];
    return model.mallSubjectProductVOs.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat itemW = 170;
    CGFloat itemH = 258;
    CGSize size = CGSizeMake(itemW,itemH);
    
    return size;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    XPMainCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"XPMainCollectionViewCell" forIndexPath:indexPath];
    
    XPAutionSubjectModel *model = self.dataArray[indexPath.section];
    NSArray *subArray = [XPProductModel mj_objectArrayWithKeyValuesArray:model.mallSubjectProductVOs];
    
    XPProductModel *subModel = subArray[indexPath.row];
    
    [cell cellWithModel:subModel passedTime:self.passedTime];
    
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return (SCREEN_WIDTH - 170*2)/3.;
}

//cell的最小列间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, (SCREEN_WIDTH - 170*2)/3., 0, (SCREEN_WIDTH - 170*2)/3.);
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableView = nil;
       //返回段头段尾视图
       if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
           UICollectionReusableView *header=[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"collectionCellHeader" forIndexPath:indexPath];
           [header.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
           
           XPAutionSubjectModel *model = self.dataArray[indexPath.section];
           
           
           //添加头视图的内容
           UIImageView *backGroundBanner = [[UIImageView alloc] init];
           [backGroundBanner sd_setImageWithURL:[NSURL URLWithString:model.pic]];
           backGroundBanner.frame = CGRectMake(0, 0, SCREEN_WIDTH, 52);
           
           [header addSubview:backGroundBanner];
                      
           UILabel *titleLable = [UILabel new];
           titleLable.font = kFontBold(18);
           titleLable.textColor = UIColorFromRGB(0x6E6BFF);
           titleLable.frame = CGRectMake(0, 0, backGroundBanner.mj_w, backGroundBanner.mj_h);
           titleLable.textAlignment = NSTextAlignmentCenter;
           [header addSubview:titleLable];
           
           reusableView = header;
           
           titleLable.text = model.name;
           
           return reusableView;
       }else {
           UICollectionReusableView *footer=[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"collectionCellFooter" forIndexPath:indexPath];
           
           
           [footer.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
           
           UIImageView *lineImageView = [[UIImageView alloc] init];
           lineImageView.image = [UIImage imageNamed:@"xp_bottom_shadow"];
           lineImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 26);
           [footer addSubview:lineImageView];
           
           if (indexPath.section == self.dataArray.count -1 && APPTYPE == 0) {
               UIView *authView = [UIView new];
//               authView.backgroundColor = [UIColor greenColor];
               authView.frame = CGRectMake(0, 26+ 12, SCREEN_WIDTH, 84);
               [footer addSubview:authView];
               
               NSString *authText = @"京公网安备11011502038171号";
               UILabel *authLabel1 = [[UILabel alloc] init];
               authLabel1.font = kFontRegular(12);
               authLabel1.textColor = UIColorFromRGB(0x8A8A8A);
               authLabel1.text = authText;
               [authView addSubview:authLabel1];
               
               UIImageView *authImageView = [[UIImageView alloc] init];
               authImageView.image = [UIImage imageNamed:@"xp_icp"];
               [authView addSubview:authImageView];
               
               CGFloat textLength = [authText sizeWithAttributes:@{NSFontAttributeName:authLabel1.font}].width;
               CGFloat originX = (SCREEN_WIDTH - 16 - textLength - 6)/2.;
               authImageView.frame = CGRectMake(originX, 1, 16, 16);
               authLabel1.frame = CGRectMake(MaxX(authImageView) + 6, 0, textLength, 18);
               
               UILabel *authLabel2 = [[UILabel alloc] init];
               authLabel2.font = kFontRegular(12);
               authLabel2.textColor = UIColorFromRGB(0x8A8A8A);
               authLabel2.text = @"琼ICP备2024019336号-3A";
               authLabel2.textAlignment = NSTextAlignmentCenter;
               authLabel2.frame = CGRectMake(0, MaxY(authLabel1) + 6, SCREEN_WIDTH, 16);
               [authView addSubview:authLabel2];
               
           }
           reusableView = footer;
           
           return reusableView;
       }
    
    return reusableView;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.collectionView.bounds.size.width, 64);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    
    if (section == self.dataArray.count - 1){
        if (APPTYPE == 0) {
            return CGSizeMake(self.collectionView.bounds.size.width, 26 + 12 + 70);
        }else {
            return CGSizeMake(self.collectionView.bounds.size.width, 26 + 12);
        }
        
    }else{
        return CGSizeMake(self.collectionView.bounds.size.width, 26 + 12);
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    XPAutionSubjectModel *model = self.dataArray[indexPath.section];
    NSArray *subArray = [XPProductModel mj_objectArrayWithKeyValuesArray:model.mallSubjectProductVOs];
    XPProductModel *subModel = subArray[indexPath.row];
    if (subModel.isAuction.integerValue == 2) {
        XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
        vc.productSkuId = subModel.productSkuId;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        XPProductDetailController *vc = [XPProductDetailController new];
        vc.auctionItemId = subModel.auctionItemId;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)setupNavBarView {
    if (APPTYPE == 0) {
        self.navBarView = [[XPNavTopView alloc] init];
        self.navBarView.leftButton.frame = CGRectMake(10, self.safeTop + 2, 40, 40);
        [self.navBarView.leftButton setImage:[UIImage imageNamed:@"home_top_cert"] forState:UIControlStateNormal];
        [self.navBarView.leftButton addTarget:self action:@selector(topLeftButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.navBarView];
        
    }else if(APPTYPE == 1){
        self.navBarView = [[XPNavTopView alloc] init];
        [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        [self.navBarView.rightButton setImage:[UIImage imageNamed:@"xp_aution_category"] forState:UIControlStateNormal];
        [self.navBarView.rightButton addTarget:self action:@selector(topRightButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.navBarView];
    }
    
    [self.navBarView.rightButton setImage:[UIImage imageNamed:@"xp_aution_category"] forState:UIControlStateNormal];
    [self.navBarView.rightButton addTarget:self action:@selector(topRightButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"新拍";
}

- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kHome_main_data];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            [self.topView viewWithData:data.userData];
            self.passedTime = 0;
            [self.collectionView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}



-(void)initMainTimer{
    _mainTimer = [XPWeakTimer shareTimer];
    [_mainTimer startTimerWithTime:1 target:self selector:@selector(countPassedTime) userInfo:nil repeats:YES];
    _passedTime = 0;
}
-(void)countPassedTime{
    _passedTime ++;
    if (_passedTime > 60*60*24*7) {
        [self.mainTimer stopTimer];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self getData];
    
}

#pragma mark -- top Click
- (void)topLeftButtonClicked:(UIButton *)sender
{
    buttonCanUseAfterOneSec(sender);
    XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
    vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,kCompany_authen];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)searchButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPProductSortViewController *vc = [XPProductSortViewController new];
    vc.hidesBottomBarWhenPushed = YES;
    vc.entryType = 1;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)topRightButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPProductSortViewController *vc = [XPProductSortViewController new];
    vc.entryType = 2;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)topCallBack {
   
    XPWeakSelf;
    
    [self.topView setBannerSelectBlock:^(NSDictionary * _Nonnull dic) {

        [XPRoutetool entryWithDic:dic];
        
    }];
    
    [self.topView setItemSelectBlock:^(NSDictionary * _Nonnull dic, NSInteger index) {
        weakSelf.dataArray = [XPAutionSubjectModel mj_objectArrayWithKeyValuesArray:dic[@"auctionSubjectContainProductVOs"]];
        
        [weakSelf.collectionView reloadData];
        
    }];
    
    
}
#pragma mark - 后台进入
- (void)applicationWillEnterForeground:(NSNotification *)notification
{
    [self getData];
}
@end
