//
//  XPProductSortViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "XPProductSortViewController.h"
#import "XPSlideItemView.h"
#import "XPProductModel.h"
#import "XPProductSortCell.h"
#import "XPEmptyView.h"
#import "XPProductDetailController.h"
#import "XPWeakTimer.h"
#import "XPSearchDefaultView.h"

@interface XPProductSortViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) XPSlideItemView *selectItemView;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//当前页码
@property (nonatomic, assign) NSInteger page;
//分类
@property (nonatomic, strong) NSMutableArray *nameArray;
@property (nonatomic, strong) NSMutableArray *idArray;
@property (nonatomic, strong) XPEmptyView *emptyView;

@property (nonatomic, strong) XPWeakTimer *mainTimer;

@property (nonatomic, assign) NSInteger passedTime;

@property (nonatomic, copy) NSString *keyStr;

@end

@implementation XPProductSortViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    _dataArray = [NSMutableArray array];
    //默认选中第一个
    self.currentIndex = 1;
    self.page = 1;
    self.keyStr = @"";
    
    [self setupUI];
    [self initMainTimer];
}

- (void)setupUI {
    
    if (self.entryType == 0) {
        XPSlideItemView *selectItemView = [[XPSlideItemView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, 40)];
        selectItemView.backgroundColor = UIColor.whiteColor;
        [self.view addSubview:selectItemView];
        self.selectItemView = selectItemView;
    }
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    if (self.entryType == 0) {
        self.tableView.frame = CGRectMake(0, MaxY(self.selectItemView), SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - 40);
    }else if (self.entryType == 1 || self.entryType == 2){
        self.tableView.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    }
    self.tableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    UIView *tableViewfooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 12)];
    self.tableView.tableFooterView = tableViewfooter;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.mj_w, self.safeBottom)];
    [self.view addSubview:self.tableView];
    XPWeakSelf
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf headRefreshDataBySortID:weakSelf.currentIndex];
    }];

    [self.selectItemView setTapBlock:^(NSInteger index) {
        //网络请求
        weakSelf.currentIndex = index;
        weakSelf.page = 1;
        [weakSelf.dataArray removeAllObjects];
        [weakSelf getListDataBySortID:weakSelf.currentIndex];
        
    }];
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_empty_favorite"] Description:@"该分类没有商品哦~"];
    [self.view addSubview:self.emptyView];
    
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    self.navBarView.backgroundColor = UIColor.whiteColor;
    self.navBarView.titleLabel.text = @"拍品列表";
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.navBarView];
    if (self.entryType == 1) {
        
        XPSearchDefaultView *searchView = [[XPSearchDefaultView alloc] initWithFrame:CGRectMake(52, self.safeTop, SCREEN_WIDTH - 52 - 24, 40)];
        searchView.backgroundColor = UIColorFromRGB(0xF2F2F2);
        searchView.searchIcon.image = [UIImage imageNamed:@"xp_search_black"];
        searchView.canSearch = YES;
        searchView.defaultSearchStr = self.keyStr;
        [self.navBarView addSubview:searchView];
        
        [self.view addSubview:self.navBarView];
        
        XPWeakSelf;
        [searchView setSearchBlock:^(NSString * _Nonnull keyStr) {
            [weakSelf.view endEditing:YES];
            
            weakSelf.dataArray = @[].mutableCopy;
            [weakSelf.tableView reloadData];
            weakSelf.keyStr = keyStr;
            [weakSelf getListDataBySortID:0];
            weakSelf.page = 1;
        }];
        
        [searchView setSearchStateBlock:^(BOOL state) {
            if (state) {
                weakSelf.tableView.hidden = YES;
                weakSelf.emptyView.hidden = YES;
            }else{
                if (weakSelf.dataArray.count > 0) {
                    weakSelf.tableView.hidden = NO;
                    weakSelf.emptyView.hidden = YES;
                }else {
                    weakSelf.tableView.hidden = YES;
                    weakSelf.emptyView.hidden = NO;
                }
            }
        }];
    }
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPProductSortCell *cell = [XPProductSortCell cellWithTableView:tableView];
    cell.cellType = 1;
    [cell cellWithModel:self.dataArray[indexPath.row] passedTime:self.passedTime];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XPProductDetailController *vc = [XPProductDetailController new];
    XPProductModel *model = self.dataArray[indexPath.row];
    vc.auctionItemId = model.productId;
    
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.idArray.count == 0 && (self.entryType == 0)) {
        [self initData];
    }else if (self.entryType == 2){
        [self getListDataBySortID:0];
    }
}

//分类数据
- (void)initData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_shop_sortOne_list];
    
    [self startLoadingGif];
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:nil requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [self getTitleArray:data.userData];
            
            [self.selectItemView configWithtitleArray:self.nameArray];
            
            [self getListDataBySortID:self.currentIndex];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)getListDataBySortID:(NSInteger )num {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Get_sort_product];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    if (self.entryType == 0) {
        paraM[@"categoryId"] = self.idArray[num - 1];
    }else if (self.entryType == 2){
        paraM[@"categoryId"] = @(0);
    }
    else if (self.entryType == 1){
        paraM[@"keywords"] = self.keyStr;
    }
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            self.passedTime = 0;
            
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPProductModel mj_objectArrayWithKeyValuesArray:dataArray];
        
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            [self canShowEmptyViewWithDtaArray:modelArray];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
- (void)headRefreshDataBySortID:(NSInteger )num {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Get_sort_product];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    if (self.entryType == 0) {
        paraM[@"categoryId"] = self.idArray[num - 1];
    }else if (self.entryType == 2){
        paraM[@"categoryId"] = @(0);
    }else if (self.entryType == 1){
        paraM[@"keywords"] = self.keyStr;
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPProductModel mj_objectArrayWithKeyValuesArray:dataArray];
            self.passedTime = 0;
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            [self canShowEmptyViewWithDtaArray:modelArray];
            
        }else {
            self.emptyView.hidden = NO;
            self.tableView.hidden = YES;
            
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)canShowEmptyViewWithDtaArray:(NSArray *)array {
    if (array.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}



- (void)getTitleArray:(NSArray *)array {
    self.nameArray = [NSMutableArray array];
    self.idArray = [NSMutableArray array];
    
    for (int i = 0; i<array.count; i++) {
        NSDictionary *dic = array[i];
        [self.nameArray addObject:dic[@"name"]];
        [self.idArray addObject:dic[@"id"]];
    }
    [self.nameArray insertObject:@"全部" atIndex:0];
    [self.idArray insertObject:@(0) atIndex:0];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Get_sort_product];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    NSNumber *idNum = self.idArray[_currentIndex - 1];
    if (self.entryType == 0) {
        paraM[@"categoryId"] = idNum;
    }else if (self.entryType == 2){
        paraM[@"categoryId"] = @(0);
    }else if (self.entryType == 1){
        paraM[@"keywords"] = self.keyStr;
    }

    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPProductModel mj_objectArrayWithKeyValuesArray:dataArray];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                self.passedTime = 0;
                //将新的服务器时间替换
                XPProductModel *firstModel = modelArray.firstObject;
                for (int i = 0; i<self.dataArray.count; i++) {
                    XPProductModel *model = self.dataArray[i];
                    model.currentTimeLong = firstModel.currentTimeLong;
                }
                [self.dataArray addObjectsFromArray:modelArray];
                [self.tableView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}
//初始化计时器
-(void)initMainTimer{
    _mainTimer = [XPWeakTimer shareTimer];
    [_mainTimer startTimerWithTime:1 target:self selector:@selector(countPassedTime) userInfo:nil repeats:YES];
    _passedTime = 0;
}
-(void)countPassedTime{
    _passedTime ++;
    
    if (_passedTime > 60*60*24*7) {
        [self.mainTimer stopTimer];
    }
    
}
@end
