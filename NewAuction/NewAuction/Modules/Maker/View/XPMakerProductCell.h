//
//  XPMakerProductCell.h
//  NewAuction
//
//  Created by Grasp_L on 2025/1/2.
//

#import <UIKit/UIKit.h>
#import "XPMakerProductModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPMakerProductCell : UITableViewCell
//初始化
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//书卷
- (void)cellWithModel:(XPMakerProductModel *)model;

@end

NS_ASSUME_NONNULL_END
