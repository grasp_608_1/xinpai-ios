//
//  XPMakerTopView.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/2.
//

#import "XPMakerTopView.h"
#import "XPLunboView.h"
#import "XPShopSortItemView.h"

#define makerBannerHeight  156*SCREEN_WIDTH/375.

@interface XPMakerTopView ()<XPLunboViewDelegate>
@property (nonatomic, strong) UIView *mainView;
//轮播图
@property (nonatomic, strong) XPLunboView *topView;
//一级分类
@property (nonatomic, strong) XPShopSortItemView *selectItemView;
//当前分类选中位置
@property (nonatomic, assign) NSInteger currentIndex;
@end


@implementation XPMakerTopView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.backgroundColor = UIColor.whiteColor;

    self.currentIndex = 1;
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(0, 0, self.mj_w, makerBannerHeight + 52);
    mainView.clipsToBounds = YES;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    self.topView = [[XPLunboView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, makerBannerHeight)];
    self.topView.cornerRadius = 0;
    self.topView.lunDelegate = self;
    [self.mainView addSubview:self.topView];
    
    XPShopSortItemView *selectItemView = [[XPShopSortItemView alloc] initWithFrame:CGRectMake(25 + 25 + 50 , MaxY(self.topView) + 12, SCREEN_WIDTH - 16 - 70, 24)];
    selectItemView.itemSpace = 50;
    selectItemView.backgroundColor = UIColor.whiteColor;
    [self.mainView addSubview:selectItemView];
    self.selectItemView = selectItemView;
    CGFloat selectW = [self.selectItemView configWithtitleArray:@[@{@"name":@"作品"},@{@"name":@"创客"}]];
    selectItemView.frame = CGRectMake(SCREEN_WIDTH/2. - selectW/2., MaxY(self.topView) + 12, selectW, 24);
    selectItemView.contentSize = CGSizeMake(selectItemView.mj_w, selectItemView.mj_h);
    selectItemView.bounces = NO;
    XPWeakSelf
    [self.selectItemView setTapBlock:^(NSInteger index) {
        if (weakSelf.selectBlock) {
            weakSelf.selectBlock(index);
        }
    }];
    
}

-(void)viewWithData:(NSDictionary *)dataDict {
    
    NSArray *banerArray = dataDict[@"auctionRotationVOs"];
    [self.topView setLunBoArray:banerArray];
    
}

-(void)tapImageClick:(NSDictionary *)dic {
    if (self.lunboBlock) {
        self.lunboBlock(dic);
    }
}

@end
