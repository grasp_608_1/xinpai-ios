//
//  XPMakerUserCell.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/7.
//

#import "XPMakerUserCell.h"

@interface XPMakerUserCell ()

@property (nonatomic, strong) UIView *mainView;
//图片
@property (nonatomic, strong) UIImageView *iconImageView;
//用户头像
@property (nonatomic, strong) UIImageView *userIcon;
//用户名
@property (nonatomic, strong) UILabel *nameLabel;
//徽章
@property (nonatomic, strong) UIView *badgeView;
//热度
@property (nonatomic, strong) UIView *hotView;
//🔥
@property (nonatomic, strong) UIImageView *hotIcon;
//热度数量
@property (nonatomic, strong) UILabel *hotNumLabel;
//作者签名
@property (nonatomic, strong) UILabel *descLabel;
//作品数量
@property (nonatomic, strong) UILabel *workNumLabel;
//平台获利
@property (nonatomic, strong) UILabel *moneyLabel;

@end


@implementation XPMakerUserCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPMakerUserCell";
    XPMakerUserCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPMakerUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    CGFloat mainW = SCREEN_WIDTH - 24;
    CGFloat ImageW = 160;
    CGFloat LabelW = mainW - 8 - ImageW - 8 - 8;
    CGFloat labelX = ImageW + 16;
    
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 12,mainW,132);
    mainView.layer.cornerRadius = 8;
    mainView.clipsToBounds = YES;
    self.mainView = mainView;
    [self.contentView addSubview:mainView];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.frame = CGRectMake(8,16,ImageW ,100);
    self.iconImageView.layer.cornerRadius = 4;
    self.iconImageView.layer.masksToBounds = YES;
    [mainView addSubview:self.iconImageView];
    
    UIImageView *userIcon = [UIImageView new];
    userIcon.frame = CGRectMake(labelX, self.iconImageView.mj_y + 3,28,28);
    userIcon.layer.cornerRadius = 14;
    userIcon.layer.masksToBounds = YES;
    self.userIcon = userIcon;
    [mainView addSubview:userIcon];
    
    self.nameLabel = [UILabel new];
    self.nameLabel.font = kFontMedium(14);
    self.nameLabel.textColor = app_font_color;
    self.nameLabel.frame = CGRectMake(MaxX(self.userIcon) + 4, self.userIcon.mj_y, LabelW - 24 - 4, self.userIcon.mj_h);
    [mainView addSubview:self.nameLabel];
    
    self.badgeView = [UIView new];
    self.badgeView.layer.cornerRadius = 2;
    self.badgeView.layer.masksToBounds = YES;
    [self.mainView addSubview:self.badgeView];
    
    self.descLabel = [UILabel new];
    self.descLabel.font = kFontRegular(12);
    self.descLabel.textColor = app_font_color_8A;
    self.descLabel.frame = CGRectMake(labelX, MaxY(self.userIcon) + 7, LabelW, 18);
    [mainView addSubview:self.descLabel];
    
    self.workNumLabel = [UILabel new];
    self.workNumLabel.font = kFontMedium(14);
    self.workNumLabel.textColor = app_font_color;
    self.workNumLabel.frame = CGRectMake(labelX, MaxY(self.descLabel) + 4, LabelW, 18);
    [mainView addSubview:self.workNumLabel];
    
    self.moneyLabel = [UILabel new];
    self.moneyLabel.font = kFontMedium(14);
    self.moneyLabel.textColor = app_font_color;
    self.moneyLabel.frame = CGRectMake(labelX, MaxY(self.workNumLabel) + 4, LabelW, 18);
    [mainView addSubview:self.moneyLabel];
    
    self.hotView = [UIView new];
    self.hotView.frame = CGRectMake(100, 78, 100, 18);
    self.hotView.backgroundColor = RGBACOLOR(58, 60, 65, 0.7);
    self.hotView.layer.cornerRadius = 2;
    [self.iconImageView addSubview:self.hotView];
    
    self.hotIcon = [[UIImageView alloc] init];
    self.hotIcon.frame = CGRectMake(4, 3, 12, 12);
    self.hotIcon.image = [UIImage imageNamed:@"xp_circle_fire"];
    [self.hotView addSubview:self.hotIcon];
    
    UILabel *hotNumLabel = [UILabel new];
    hotNumLabel.font = kFontRegular(12);
    hotNumLabel.textColor = UIColor.whiteColor;
    hotNumLabel.frame = CGRectMake(17, 0,100, 16);
    self.hotNumLabel = hotNumLabel;
    [self.hotView addSubview:hotNumLabel];
}

-(void)cellWithModel:(XPMakerProductModel *)model {
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(model.worksPic)] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:model.userHead] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    
    self.nameLabel.text = model.nickname;
        
    self.descLabel.text = model.introduce;
    
    self.workNumLabel.text = [NSString stringWithFormat:@"作品数量: %@",model.worksNum];
    
    self.moneyLabel.text = [NSString stringWithFormat:@"平台获利: %@",[XPMoneyFormatTool formatMoneyWithNum:model.incomeTotal]];
    
    NSString *hotNum = [NSString stringWithFormat:@"%@",model.worksPopularity];
    CGFloat hotW = [hotNum sizeWithAttributes:@{NSFontAttributeName:self.hotNumLabel.font,}].width;
    self.hotNumLabel.frame = CGRectMake(17, 0, hotW, 18);
    self.hotNumLabel.text = hotNum;
    self.hotView.frame = CGRectMake(self.iconImageView.mj_w - 4 - 21 - hotW, self.iconImageView.mj_h - 4 - 18, 21 + hotW, 18);
}

@end
