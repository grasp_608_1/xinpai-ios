//
//  XPMakerProductCell.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/2.
//

#import "XPMakerProductCell.h"
#import "XPColorsSignView.h"

@interface XPMakerProductCell ()

@property (nonatomic, strong) UIView *mainView;
//图片
@property (nonatomic, strong) UIImageView *iconImageView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//用户头像
@property (nonatomic, strong) UIImageView *userIcon;
//用户名
@property (nonatomic, strong) UILabel *nameLabel;
//价格
@property (nonatomic, strong) UILabel *priceLabel;
//标签
@property (nonatomic, strong) XPColorsSignView *signsView;
//热度
@property (nonatomic, strong) UIView *hotView;
//🔥
@property (nonatomic, strong) UIImageView *hotIcon;
//热度数量
@property (nonatomic, strong) UILabel *hotNumLabel;

@end


@implementation XPMakerProductCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPMakerProductCell";
    XPMakerProductCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPMakerProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    CGFloat mainW = SCREEN_WIDTH - 24;
    CGFloat ImageW = 160;
    CGFloat LabelW = mainW - 8 - ImageW - 8 - 8;
    CGFloat labelX = ImageW + 16;
    
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 12,mainW,132);
    mainView.layer.cornerRadius = 8;
    mainView.clipsToBounds = YES;
    self.mainView = mainView;
    [self.contentView addSubview:mainView];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.frame = CGRectMake(8,16,ImageW ,100);
    self.iconImageView.layer.cornerRadius = 4;
    self.iconImageView.layer.masksToBounds = YES;
    [mainView addSubview:self.iconImageView];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = kFontMedium(14);
    self.titleLabel.textColor = app_font_color;
    self.titleLabel.frame = CGRectMake(labelX, 16, LabelW, 20);
    [mainView addSubview:self.titleLabel];
    
    UIImageView *userIcon = [UIImageView new];
    userIcon.frame = CGRectMake(labelX, MaxY(self.titleLabel) + 4,24,24);
    userIcon.layer.cornerRadius = 12;
    userIcon.layer.masksToBounds = YES;
    self.userIcon = userIcon;
    [mainView addSubview:userIcon];
    
    self.nameLabel = [UILabel new];
    self.nameLabel.font = kFontMedium(14);
    self.nameLabel.textColor = app_font_color;
    self.nameLabel.frame = CGRectMake(MaxX(self.userIcon) + 4, self.userIcon.mj_y, LabelW - 24 - 4, self.userIcon.mj_h);
    [mainView addSubview:self.nameLabel];
    
    self.priceLabel = [UILabel new];
    self.priceLabel.font = kFontMedium(14);
    self.priceLabel.textColor = UIColorFromRGB(0xFF4A4A);
    self.priceLabel.frame = CGRectMake(labelX, MaxY(self.userIcon) + 4, LabelW, 20);
    [mainView addSubview:self.priceLabel];
    
    
    self.signsView = [XPColorsSignView new];
    self.signsView.frame = CGRectMake(labelX, MaxY(self.priceLabel) + 4, LabelW, 24);
    self.signsView.layer.masksToBounds = YES;
    [mainView addSubview:self.signsView];
    
    self.hotView = [UIView new];
    self.hotView.frame = CGRectMake(100, 78, 100, 18);
    self.hotView.backgroundColor = RGBACOLOR(58, 60, 65, 0.7);
    self.hotView.layer.cornerRadius = 2;
    [self.iconImageView addSubview:self.hotView];
    
    self.hotIcon = [[UIImageView alloc] init];
    self.hotIcon.frame = CGRectMake(4, 3, 12, 12);
    self.hotIcon.image = [UIImage imageNamed:@"xp_circle_fire"];
    [self.hotView addSubview:self.hotIcon];
    
    UILabel *hotNumLabel = [UILabel new];
    hotNumLabel.font = kFontRegular(12);
    hotNumLabel.textColor = UIColor.whiteColor;
    hotNumLabel.frame = CGRectMake(17, 0,100, 16);
    self.hotNumLabel = hotNumLabel;
    [self.hotView addSubview:hotNumLabel];
}

-(void)cellWithModel:(XPMakerProductModel *)model {
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    self.titleLabel.text = model.name;
    
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:model.userHead] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    
    self.nameLabel.text = model.userNickname;

    NSString *priceString = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceSale]];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"售价:%@",priceString]];
    [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color,NSFontAttributeName:kFontRegular(14)} range:NSMakeRange(0, 3)];
    self.priceLabel.attributedText = attr;
    
    NSArray *titleArray = model.worksSkuLabelVos;
    if (titleArray && titleArray.count > 0) {
        [self.signsView viewWithArray:titleArray];
    }
    
    NSString *hotNum = [NSString stringWithFormat:@"%@",model.popularity];
    CGFloat hotW = [hotNum sizeWithAttributes:@{NSFontAttributeName:self.hotNumLabel.font,}].width;
    self.hotNumLabel.frame = CGRectMake(17, 0, hotW, 18);
    self.hotNumLabel.text = hotNum;
    self.hotView.frame = CGRectMake(self.iconImageView.mj_w - 4 - 21 - hotW, self.iconImageView.mj_h - 4 - 18, 21 + hotW, 18);
    
}


@end
