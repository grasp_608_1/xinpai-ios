//
//  XPMakerUploadCell.h
//  NewAuction
//
//  Created by Grasp_L on 2025/1/6.
//

#import <UIKit/UIKit.h>
#import "XPMakerProductModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPMakerUploadCell : UITableViewCell
//申请售卖
@property (nonatomic, copy) void(^applyBlock)(void);
//点赞分收藏
@property (nonatomic, copy) void(^optionBlock)(NSInteger index);
//初始化
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//书卷
- (CGFloat)cellWithModel:(XPMakerProductModel *)model;

@end

NS_ASSUME_NONNULL_END
