//
//  XPMakerUserCell.h
//  NewAuction
//
//  Created by Grasp_L on 2025/1/7.
//

#import <UIKit/UIKit.h>

#import "XPMakerProductModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPMakerUserCell : UITableViewCell
//初始化
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//书卷
- (void)cellWithModel:(XPMakerProductModel *)model;

@end

NS_ASSUME_NONNULL_END
