//
//  XPColorsSignView.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/6.
//

#import "XPColorsSignView.h"

@implementation XPColorsSignView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
}

- (void)viewWithArray:(NSArray *)array {
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (array.count > 0) {
        CGFloat left = 0;
        for (int i = 0; i<array.count; i++) {
            NSDictionary *dic = array[i];
            if (!dic || [dic isKindOfClass:[NSNull class]]) {
                break;;
            }
            UILabel *label = [[UILabel alloc] init];
            label.textColor = [XPCommonTool colorWithHex:TO_STR(dic[@"fontColour"])];
            label.font = kFontRegular(10);
            label.layer.cornerRadius = 4;
            label.layer.borderColor = label.textColor.CGColor;
            label.layer.borderWidth = 0.8;
            label.textAlignment = NSTextAlignmentCenter;
            label.text = TO_STR(dic[@"name"]);
            CGFloat labelW = [label.text sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
            label.frame = CGRectMake(left, 1, labelW + 4, 18);
            [self addSubview:label];
            left = MaxX(label) + 4;
        }
    }
}

@end
