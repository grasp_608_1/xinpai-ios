//
//  XPColorsSignView.h
//  NewAuction
//
//  Created by Grasp_L on 2025/1/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPColorsSignView : UIView

- (void)viewWithArray:(NSArray *)array;

@end

NS_ASSUME_NONNULL_END
