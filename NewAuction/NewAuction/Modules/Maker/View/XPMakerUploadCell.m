//
//  XPMakerUploadCell.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/6.
//

#import "XPMakerUploadCell.h"
#import "XPColorsSignView.h"
@interface XPMakerUploadCell ()

@property (nonatomic, strong) UIView *mainView;
//图片
@property (nonatomic, strong) UIImageView *iconImageView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//标题
@property (nonatomic, strong) UILabel *descLabel;
//标签
@property (nonatomic, strong) XPColorsSignView *signsView;
//人气
@property (nonatomic, strong) UILabel *hotNumLabel;
//等级
@property (nonatomic, strong) UILabel *LevelLabel;
//具体等级
@property (nonatomic, strong) UIButton *levelButton;
//获奖
@property (nonatomic, strong) UILabel *scoreLabel;
//上传时间
@property (nonatomic, strong) UILabel *TimeLabel;
//售卖状态
@property (nonatomic, strong) UIButton *salesStatusButton;
//点赞  分享 收藏
@property (nonatomic, strong) UIView *activeView;

@property (nonatomic, strong) XPMakerProductModel *model;

@end

@implementation XPMakerUploadCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPMakerUploadCell";
    XPMakerUploadCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPMakerUploadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    CGFloat mainW = SCREEN_WIDTH - 24;
    
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 12,mainW,132);
    mainView.layer.cornerRadius = 8;
    mainView.clipsToBounds = YES;
    self.mainView = mainView;
    [self.contentView addSubview:mainView];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = kFontMedium(16);
    self.titleLabel.textColor = app_font_color;
    self.titleLabel.frame = CGRectMake(12, 16, self.mainView.mj_w - 24, 24);
    [mainView addSubview:self.titleLabel];
    
    self.descLabel = [UILabel new];
    self.descLabel.font = kFontRegular(12);
    self.descLabel.textColor = app_font_color_8A;
    self.descLabel.frame = CGRectMake(12, MaxY(self.titleLabel), self.titleLabel.mj_w, 18);
    [mainView addSubview:self.descLabel];
    
    self.signsView = [XPColorsSignView new];
    self.signsView.layer.masksToBounds = YES;
    [mainView addSubview:self.signsView];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.layer.cornerRadius = 4;
    self.iconImageView.layer.masksToBounds = YES;
    [mainView addSubview:self.iconImageView];
    
    self.hotNumLabel = [UILabel new];
    self.hotNumLabel.font = kFontRegular(14);
    self.hotNumLabel.textColor = app_font_color;
    [mainView addSubview:self.hotNumLabel];
    
    self.LevelLabel = [UILabel new];
    self.LevelLabel.font = kFontRegular(14);
    self.LevelLabel.textColor = app_font_color;
    [mainView addSubview:self.LevelLabel];
    
    self.levelButton = [UIButton new];
    [self.levelButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [self.levelButton setBackgroundImage:[UIImage imageNamed:@"xp_make_levelbg"] forState:UIControlStateNormal];
    self.levelButton.titleLabel.font = kFontMedium(12);
    [mainView addSubview:self.levelButton];
    
    self.scoreLabel = [UILabel new];
    self.scoreLabel.font = kFontRegular(14);
    self.scoreLabel.textColor = app_font_color;
    [mainView addSubview:self.scoreLabel];
    
    self.TimeLabel = [UILabel new];
    self.TimeLabel.font = kFontRegular(14);
    self.TimeLabel.textColor = app_font_color;
    [mainView addSubview:self.TimeLabel];
    
    self.salesStatusButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.salesStatusButton addTarget:self action:@selector(salesStatusButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.salesStatusButton.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.salesStatusButton.titleLabel.font = kFontRegular(14);
    self.salesStatusButton.layer.cornerRadius = 16;
    self.salesStatusButton.layer.masksToBounds = YES;
    [mainView addSubview:self.salesStatusButton];
    
    UIView *activeView = [UIView new];
    self.activeView = activeView;
    [self.mainView addSubview:activeView];
    
}

-(CGFloat)cellWithModel:(XPMakerProductModel *)model {
    
    CGFloat mainW = SCREEN_WIDTH - 24;
    CGFloat ImageW = 160;
    CGFloat LabelW = mainW - 8 - ImageW - 8 - 12;
    CGFloat labelX = ImageW + 8 + 12;
    CGFloat top = MaxY(self.titleLabel);
    
    self.titleLabel.text = model.name;
    top = MaxY(self.titleLabel);
    if (model.introduce && model.introduce.length > 0) {
        self.descLabel.text = model.introduce;
        top = MaxY(self.descLabel);
    }else{
        self.descLabel.text = @"";
    }
    
    [self.signsView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSArray *signsArray = model.worksSkuLabelVos;
    if (signsArray.count > 0) {
        self.signsView.hidden = NO;
        [self.signsView viewWithArray:signsArray];
        self.signsView.frame = CGRectMake(12, top + 4, mainW - 24, 20);
        top = MaxY(self.signsView);
    }else {
        self.signsView.hidden = YES;
    }
    
    self.iconImageView.frame = CGRectMake(12,16 + top,ImageW ,100);
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    top = MaxY(self.iconImageView);
    self.hotNumLabel.text = [NSString stringWithFormat:@"人气:%@",model.popularity];
    self.hotNumLabel.frame = CGRectMake(labelX, self.iconImageView.mj_y + 4, LabelW, 20);
    
    self.LevelLabel.text = @"作品评级:";
    self.LevelLabel.frame = CGRectMake(labelX, MaxY(self.hotNumLabel) + 4, LabelW, 20);
    
    self.levelButton.frame = CGRectMake(labelX + 64, self.LevelLabel.mj_y + 3, 21, 16);
    [self.levelButton setTitle:model.levelName forState:UIControlStateNormal];
   
    if (model.rewardInfo && model.rewardInfo.length > 0) {
        self.scoreLabel.hidden = NO;
        self.scoreLabel.text = [NSString stringWithFormat:@"获奖:%@",model.rewardInfo];
        self.scoreLabel.frame = CGRectMake(labelX, MaxY(self.LevelLabel) + 4, LabelW, 20);
        self.TimeLabel.frame = CGRectMake(labelX, MaxY(self.scoreLabel) + 4, LabelW, 20);
    }else{
        self.scoreLabel.hidden = YES;
        self.TimeLabel.frame = CGRectMake(labelX, MaxY(self.LevelLabel) + 4, LabelW, 20);
    }
    
    self.TimeLabel.text = [NSString stringWithFormat:@"上传日期:%@",model.createTime];
    
    if (model.auditStatus.intValue == 1) {
        [self.salesStatusButton setTitle:[NSString stringWithFormat:@"已售%@",model.orderNum] forState:UIControlStateNormal];
        [self.salesStatusButton setTitleColor:app_font_color_8A forState:UIControlStateNormal];
        [self.salesStatusButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }else {
        [self.salesStatusButton setTitle:@"申请售卖" forState:UIControlStateNormal];
        [self.salesStatusButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        [self.salesStatusButton setBackgroundImage:[UIImage imageNamed:@"xp_maker_joinbg"] forState:UIControlStateNormal];
    }
    
    self.salesStatusButton.frame = CGRectMake(self.mainView.mj_w - 80 - 12, top + 16, 80, 32);
    
    //转发评论点赞
    self.activeView.frame = CGRectMake(12, MaxY(self.iconImageView) + 22, self.mainView.mj_w - 24 - 80 ,18);
    
    NSNumber *likeNum = model.likeNum ?:@(0);
//    NSNumber *shareNum = model.shareNum ?:@(0);
    NSNumber *favoriteNum = model.favoriteNum ?:@(0);
    
    NSArray *activityArray = @[
        @{@"image":@"xp_circle_like",@"num" :likeNum},
        //@{@"image":@"xp_circle_share",@"num" :shareNum},
        @{@"image":@"xp_shopProduct_uncollect",@"num" :favoriteNum},
    ];
    [self.activeView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat right = 0;
    for (int i = 0 ; i< activityArray.count; i++) {
        NSDictionary *dic = activityArray[i];
        NSString *numStr = [NSString stringWithFormat:@"%@",dic[@"num"]];
        CGFloat numW = [numStr sizeWithAttributes:@{NSFontAttributeName:kFontRegular(10)}].width;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(right, 0, numW + 18 + 12, 18);
        button.tag = i;
        [button addTarget:self action:@selector(itemClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.activeView addSubview:button];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 0, 18, 18)];
        imageView.image = [UIImage imageNamed:dic[@"image"]];
        [button addSubview:imageView];
        
        UILabel *label = [UILabel new];
        label.font = kFontRegular(10);
        label.textColor = app_font_color_8A;
        label.textAlignment = NSTextAlignmentRight;
        label.text = numStr;
        label.frame = CGRectMake(0, 2, button.mj_w , 18);
        [button addSubview:label];
        //+ numW + 18 + 12
        right = MaxX(button);
    }
    self.mainView.frame = CGRectMake(12, 12,mainW,MaxY(_activeView) + 22);
    
    return MaxY(self.mainView);
    
}

- (void)salesStatusButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.applyBlock) {
        self.applyBlock();
    }
}

-(void)itemClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.optionBlock) {
        self.optionBlock(sender.tag);
    }
}


@end
