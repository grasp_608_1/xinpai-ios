//
//  XPMakerTopView.h
//  NewAuction
//
//  Created by Grasp_L on 2025/1/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPMakerTopView : UIView
//轮播点击
@property (nonatomic, copy) void(^lunboBlock)(NSDictionary *dic);
//底部标签切换
@property (nonatomic, copy) void(^selectBlock)(NSInteger index);

- (void)viewWithData:(NSDictionary *)dataDict;

@end

NS_ASSUME_NONNULL_END
