//
//  XPMakerProductModel.h
//  NewAuction
//
//  Created by Grasp_L on 2025/1/2.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPMakerProductModel : NSObject

@property (nonatomic, copy) NSString *userNickname;//作品作者昵称
@property (nonatomic, copy) NSString *nickname;//创客作者昵称
@property (nonatomic, copy) NSString *userHead;//头像
@property (nonatomic, copy) NSString *levelName;//作品评级
@property (nonatomic, copy) NSString *pic;//作品图
@property (nonatomic, copy) NSString *worksPic;//创客作品pic
@property (nonatomic, copy) NSString *createTime;//创建时间
@property (nonatomic, copy) NSString *circleLabelVOs;//标签
@property (nonatomic, copy) NSString *name;//商品名称
@property (nonatomic, copy) NSString *introduce;//个人介绍
@property (nonatomic, copy) NSString *rewardInfo;//最近一次获奖信息



@property (nonatomic, strong) NSNumber *priceSale;//销售价
@property (nonatomic, strong) NSNumber *popularity;//热度
@property (nonatomic, strong) NSNumber *worksPopularity;//作品热度
@property (nonatomic, strong) NSNumber *level;//作品等级
@property (nonatomic, strong) NSNumber *userId;//用户id
@property (nonatomic, strong) NSNumber *makerId;//创客id
@property (nonatomic, strong) NSNumber *productSkuId;//skuid
@property (nonatomic, strong) NSNumber *worksNum;//作品数量
@property (nonatomic, strong) NSNumber *incomeTotal;//平台获利
@property (nonatomic, strong) NSNumber *uID;//id
///转商品审核状态 0 app创建 11 文件已上传 12 转商品待审核 1 审核通过 2 审核失败
@property (nonatomic, strong) NSNumber *auditStatus;//申请售卖状态
@property (nonatomic, strong) NSNumber *orderNum;//售卖数量
@property (nonatomic, strong) NSNumber *likeNum;//点赞数量
@property (nonatomic, strong) NSNumber *shareNum;//分享数量
@property (nonatomic, strong) NSNumber *commentNum;//评论数量
@property (nonatomic, strong) NSNumber *favoriteNum;//收藏数量


@property (nonatomic, strong) NSArray *worksSkuLabelVos;//标签

@end

NS_ASSUME_NONNULL_END
