//
//  XPMyMakerUploadController.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/6.
//

#import "XPMyMakerUploadController.h"
#import "XPEmptyView.h"
#import "XPMakerUploadCell.h"
#import "XPMakerProductModel.h"

@interface XPMyMakerUploadController ()<UITableViewDelegate,UITableViewDataSource>
//上传作品
@property (nonatomic, strong) UIButton *topButton;
//顶部上传按钮
@property (nonatomic, strong) UIView *topView;
//列表
@property (nonatomic, strong) UITableView *tableView;
//无数据
@property (nonatomic, strong) XPEmptyView *emptyView;
//当前页码
@property (nonatomic, assign) NSInteger page;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//高度
@property (nonatomic, copy) NSArray *heightArray;


@end

@implementation XPMyMakerUploadController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupViews];
    self.page = 1;
    [self getData];
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.navBarView];
    
    self.navBarView.titleLabel.text = @"我的作品";
    
}
-(void)setupViews{
    
    CGFloat topH = 130;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage imageNamed:@"xp_maker_upload"] forState:UIControlStateNormal];
    [button setTitle:@"上传作品" forState:UIControlStateNormal];
    [button setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    button.titleLabel.font= kFontMedium(16);
    [button addTarget:self action:@selector(uploadButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, topH);
    [self.view addSubview:button];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0,kTopHeight + topH, SCREEN_WIDTH, SCREEN_HEIGHT -  kTopHeight - topH);
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.safeBottom)];
    [self.view addSubview:self.tableView];
     
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, self.tableView.mj_y + 100, emptyW,emptyW + 27)];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无作品"];
    [self.view addSubview:self.emptyView];

    XPWeakSelf
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf headerRefresh];
    }];
    
}


#pragma mark --- tableview delegete dataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPMakerUploadCell *cell = [XPMakerUploadCell cellWithTableView:tableView];
    XPMakerProductModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
   
    XPWeakSelf
    [cell setApplyBlock:^{
        
        [weakSelf.xpAlertView showCancelAlertWithTitle:@"提示您确定要申请售卖该作品吗?" leftAction:^(NSString * _Nullable extra) {
            
        } rightAction:^(NSString * _Nullable extra) {
            [weakSelf applySaleWithModel:model];
        }];
        
    }];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *num = self.heightArray[indexPath.row];
    return num.floatValue;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


//获取cell高度
- (void)getCellHeightArray{
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPMakerUploadCell *cell = XPMakerUploadCell.new;
    for (int i = 0; i<self.dataArray.count; i++) {
        XPMakerProductModel *model = self.dataArray[i];
        CGFloat height = [cell cellWithModel:model];
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    self.heightArray = tempArray.copy;
}

- (void)uploadButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    NSString *urlStr = @"https://3ddesigner.iotbroad.com/#/maker";
    [self.xpAlertView showCancelAlertWithTitle:[NSString stringWithFormat:@"点击确定复制地址:\n%@",urlStr] leftAction:^(NSString * _Nullable extra) {
        
    } rightAction:^(NSString * _Nullable extra) {
        UIPasteboard *pastboard = [UIPasteboard generalPasteboard];
        pastboard.string = urlStr;
        [[XPShowTipsTool shareInstance] showMsg:@"已复制" ToView:k_keyWindow];
    }];
    
}

#pragma mark --- netWork
- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"makerId"] = @(-1);
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    //类型 0 作品 1 创客
    paraM[@"type"] = @(1);
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPMakerProductModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self getCellHeightArray];
            [self.tableView reloadData];
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
- (void)headerRefresh{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"makerId"] = @(-1);
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    //类型 0 作品 1 创客
    paraM[@"type"] = @(1);

    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPMakerProductModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self getCellHeightArray];
            [self.tableView reloadData];
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
- (void)footerRefresh {
    self.page ++;
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"makerId"] = @(-1);
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    //类型 0 作品 1 创客
    paraM[@"type"] = @(1);

    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray ;
            modelArray = [XPMakerProductModel mj_objectArrayWithKeyValuesArray:dataArray];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self getCellHeightArray];
                [self.tableView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

//申请售卖
- (void)applySaleWithModel:(XPMakerProductModel *)model {
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kMaker_work_apply,model.uID];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:@"申请售卖成功，请耐心等待审核" ToView:k_keyWindow];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)canShowEmptyView {
    if (self.dataArray.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
    
}
@end
