//
//  XPMakerMoneyTranferOutController.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/2.
//

#import "XPMakerMoneyTranferOutController.h"
#import "XPMyBankCardController.h"
#import "XPBankModel.h"
#import "XPTrendDetailViewController.h"

@interface XPMakerMoneyTranferOutController ()<UITextFieldDelegate>
//总余额
@property (nonatomic, strong) UILabel *totalLabel;
//银行卡信息
@property (nonatomic, strong) UILabel *targetBankNameLabel;
//手续费
@property (nonatomic, strong) UILabel *percentLabel;
//提现金额输入
@property (nonatomic, strong) UITextField *textField;
//银行信息model
@property (nonatomic, strong) XPBankModel *bankModel;
//个人金额信息
@property (nonatomic, strong) NSDictionary *dataDict;
@end

@implementation XPMakerMoneyTranferOutController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupViews];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(freshBankCard) name:Notification_user_choose_bankcard object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getData];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.navBarView];
    self.navBarView.titleLabel.text = @"我的余额";
    
    [self.navBarView.rightButton setTitle:@"明细" forState:UIControlStateNormal];
    [self.navBarView.rightButton setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
    [self.navBarView.rightButton addTarget:self action:@selector(detailButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.rightButton.titleLabel.font = kFontRegular(16);
}

- (void)setupViews {
    
    UIView *topView = [UIView new];
    topView.backgroundColor = UIColorFromRGB(0x6E6BFF);
    topView.layer.cornerRadius = 8;
    topView.layer.masksToBounds = YES;
    topView.frame = CGRectMake(12, 12 + kTopHeight, SCREEN_WIDTH - 24, 96);
    [self.view addSubview:topView];
    
    UILabel *label1 = [UILabel new];
    label1.font = kFontRegular(14);
    label1.textColor = UIColor.whiteColor;
    label1.frame = CGRectMake(24, 12, 200, 24);
    label1.text = @"余额";
    [topView addSubview:label1];
    
    UILabel *label2 = [UILabel new];
    label2.font = kFontMedium(28);
    label2.textColor = UIColor.whiteColor;
    label2.frame = CGRectMake(24, MaxY(label1) +12, topView.mj_w - 24, 36);
    label2.text = @"0.00";
    self.totalLabel  = label2;
    [topView addSubview:label2];
    
    UIButton *midView = [UIButton new];
    midView.backgroundColor = UIColor.whiteColor;
    midView.layer.cornerRadius = 8;
    midView.layer.masksToBounds = YES;
    midView.frame = CGRectMake(12, MaxY(topView) + 12, SCREEN_WIDTH - 24, 96);
    [midView addTarget:self action:@selector(bankButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:midView];
    
    UILabel *label3 = [UILabel new];
    label3.font = kFontRegular(14);
    label3.textColor = UIColorFromRGB(0x3A3C41);
    label3.frame = CGRectMake(24, 12, 100, 24);
    label3.text = @"提现";
    [midView addSubview:label3];
    
    UILabel *label4 = [UILabel new];
    label4.font = kFontRegular(16);
    label4.textColor = UIColorFromRGB(0x3A3C41);
    label4.frame = CGRectMake(24, MaxY(label3) + 12, midView.mj_w - 24 - 40, 24);
    label4.text = @"选择银行卡";
    self.targetBankNameLabel = label4;
    [midView addSubview:label4];
    
    UIImageView *chooseImageView = [[UIImageView alloc] initWithFrame:CGRectMake(midView.mj_w - 12 - 24, label4.mj_y, 24, 24)];
    chooseImageView.image = [UIImage imageNamed:@"xp_setting_arrow"];
    [midView addSubview:chooseImageView];
    
    UIView *downView = [UIView new];
    downView.backgroundColor = UIColor.whiteColor;
    downView.frame = CGRectMake(12, MaxY(midView) + 12, SCREEN_WIDTH -24, 132);
    downView.layer.cornerRadius = 16;
    downView.layer.masksToBounds = YES;
    [self.view addSubview:downView];
    
    UILabel *label5 = [UILabel new];
    label5.font = kFontRegular(14);
    label5.textColor = UIColorFromRGB(0x3A3C41);
    label5.frame = CGRectMake(24, 12, 60, 24);
    label5.text = @"提现金额";
    [downView addSubview:label5];
    
    UILabel *label6 = [UILabel new];
    label6.font = kFontMedium(24);
    label6.textColor = UIColorFromRGB(0x000000);
    label6.frame = CGRectMake(24, 64, 24, 24);
    label6.text = @"￥";
    [downView addSubview:label6];
    
    UILabel *label7 = [UILabel new];
    label7.font = kFontRegular(14);
    label7.textColor = UIColorFromRGB(0xB8B8B8);
    label7.frame = CGRectMake(MaxX(label5) + 12, 12,downView.mj_w - MaxX(label5) - 24, 24);
    label7.textAlignment = NSTextAlignmentRight;
    label7.text = @"";
    self.percentLabel = label7;
    [downView addSubview:label7];
    
    UITextField *textField = [[UITextField alloc] init];
    textField.font = kFontMedium(32);
    textField.textColor = UIColor.blackColor;
//    textField.placeholder = @"请输入金额";
    textField.keyboardType = UIKeyboardTypeNumberPad;
    textField.delegate = self;
    self.textField = textField;
    textField.keyboardType = UIKeyboardTypeDecimalPad;
    textField.frame = CGRectMake(MaxX(label6), 56, downView.mj_w - 66 - 24 - 50, 40);
    [downView addSubview:textField];
    
    
    UIButton *allButton = [UIButton buttonWithType:UIButtonTypeCustom];
    allButton.frame = CGRectMake(MaxX(textField) + 10,textField.mj_y, 40, 40);
    [allButton setTitle:@"全部" forState:UIControlStateNormal];
    allButton.titleLabel.font = kFontMedium(16);
    [allButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    [allButton addTarget:self action:@selector(allButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [downView addSubview:allButton];
    
    
    UIView *sepView = [UIView new];
    sepView.frame = CGRectMake(24, MaxY(textField) + 12, downView.mj_w - 48, 1);
    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [downView addSubview:sepView];
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(12,SCREEN_HEIGHT - self.safeBottom - 30-50, SCREEN_WIDTH - 2 *12, 50);
    [saveButton setTitle:@"确认转出" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    saveButton.layer.cornerRadius = 25;
    saveButton.clipsToBounds = YES;
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveButton];
    
    
    
    
}

- (void)freshTopData{
    self.totalLabel.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.dataDict[@"availableAmount"]]];
    /*
    NSNumber *percent = self.dataDict[@"withdrawalFees"];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.minimumFractionDigits = 0;
    formatter.maximumFractionDigits = 2;
            
    percent = [NSNumber numberWithFloat:[percent floatValue] * 100];
    NSString *priceIncrStr = [NSString stringWithFormat:@"%@",[formatter stringFromNumber:percent]];
    
    self.percentLabel.text = [NSString stringWithFormat:@"转出时需要额外扣除%@%%手续费",priceIncrStr];
     */
}

- (void)allButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    self.textField.text = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:self.dataDict[@"availableAmount"]]];
    
}


- (void)bankButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPMyBankCardController *vc = [XPMyBankCardController new];
    vc.enterType = 1;
    XPWeakSelf;
    vc.chooseBankBlock = ^(XPBankModel * _Nonnull bankModel) {
        weakSelf.bankModel = bankModel;
        weakSelf.targetBankNameLabel.text = bankModel.bankName;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)freshBankCard {
    self.targetBankNameLabel.text = @"选择银行卡";
    self.bankModel = [XPBankModel new];
}

- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self.view endEditing:YES];
    
    NSString *totalMoney = [XPMoneyFormatTool formatMoneyWithNum:self.dataDict[@"availableAmount"]];
    if (totalMoney.floatValue == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"您还没有余额可以提现" ToView:self.view];
        return;
    }else if (totalMoney.floatValue < self.textField.text.floatValue) {
        [[XPShowTipsTool shareInstance] showMsg:@"转出金额大于账户余额" ToView:self.view];
        return;
    }else if ([self.targetBankNameLabel.text containsString:@"选择银行"]){
        [[XPShowTipsTool shareInstance] showMsg:@"请先选择银行卡" ToView:self.view];
    }else if ([self.textField.text floatValue] == 0){
        [[XPShowTipsTool shareInstance] showMsg:@"请输入金额" ToView:self.view];
    }
    else {
        [self toMoneyStepOne];
    }
}


- (void)detailButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPTrendDetailViewController *vc = [XPTrendDetailViewController new];
    vc.enterType = 13;
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark --- textfield Delegate
//限制只能输入金额
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //限制.后面最多有两位，且不能再输入.
    if ([textField.text rangeOfString:@"."].location != NSNotFound) {
        //有.了 且.后面输入了两位  停止输入
        if (toBeString.length > [toBeString rangeOfString:@"."].location+3) {
            return NO;
        }
        //有.了，不允许再输入.
        if ([string isEqualToString:@"."]) {
            return NO;
        }
    }
    
    //限制首位0，后面只能输入. 或 删除
    if ([textField.text isEqualToString:@"0"]) {
        if (!([string isEqualToString:@"."] || [string isEqualToString:@""])) {
            return NO;
        }
    }
    
    //首位. 前面补全0
    if ([toBeString isEqualToString:@"."]) {
        textField.text = @"0";
        return YES;
    }
    
    //限制只能输入：1234567890.
    NSCharacterSet * characterSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890."] invertedSet];
    NSString * filtered = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}


#pragma mark --- network
- (void)toMoneyStepOne {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_money_exchange];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"withdrawalOfCash"] = self.textField.text;
    paraM[@"bankId"] = self.bankModel.productId;
    paraM[@"withdrawType"] = @(201);
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance]showMsg:@"提交成功,请耐心等待1~3个工作日" ToView:self.view];
            [self performSelector:@selector(goBack) withObject:nil afterDelay:0.7];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//获取个人创客账户信息
-(void)getData{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_money_info];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            self.dataDict = data.userData;
            
            [self freshTopData];
    
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

@end
