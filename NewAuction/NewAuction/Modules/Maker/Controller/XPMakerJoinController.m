//
//  XPMakerJoinController.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/6.
//

#import "XPMakerJoinController.h"
#import "UIImage+Compression.h"
#import "XPOSSUploadTool.h"

@interface XPMakerJoinController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>

@property (nonatomic, strong) UIButton *frontButton;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIImageView *frontImageView;
//邮箱
@property (nonatomic, strong) UITextField *emailfield;
//加入按钮
@property (nonatomic, strong) UIButton *joinButton;
//上传的图片
@property (nonatomic, strong) UIImage *chooseImage;
//图片地址
@property (nonatomic, copy)   NSString *picUrl;

@end

@implementation XPMakerJoinController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupViews];
}
-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.navBarView];
    
    self.navBarView.titleLabel.text = @"加入创客";
}

-(void)setupViews {
    
    UIView *topView = [UIView new];
    topView.backgroundColor = UIColor.whiteColor;
    topView.layer.cornerRadius = 8;
    topView.frame = CGRectMake(12, kTopHeight + 12, self.view.mj_w - 24, 300);
    [self.view addSubview:topView];
    
    UILabel *deslabel = [UILabel new];
    deslabel.frame = CGRectMake(24, 12, 200, 24);
    deslabel.font = kFontRegular(14);
    deslabel.textColor = app_font_color;
    deslabel.text = @"创客资料上传";
    [topView addSubview:deslabel];
    
    CGFloat cardW = topView.mj_w - 24 *2;
    CGFloat cardH = cardW * 186/303.;
    UIView *bgView1 = [UIView new];
    bgView1.layer.cornerRadius = 4;
    bgView1.frame = CGRectMake(24, 48, cardW, cardH);
    [topView addSubview:bgView1];
    
    self.frontImageView = [[UIImageView alloc] initWithFrame:bgView1.bounds];
    self.frontImageView.image = [UIImage imageNamed:@"xp_maker_handFace"];
    [bgView1 addSubview:self.frontImageView];
    
    //身份证正面
    UIButton *frontBtn = [UIButton new];
    frontBtn.frame = bgView1.bounds;
    self.frontButton = frontBtn;
    [frontBtn addTarget:self action:@selector(frontButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bgView1 addSubview:frontBtn];
    
    
    UIView *downView = [UIView new];
    downView.backgroundColor = UIColor.whiteColor;
    downView.layer.cornerRadius = 8;
    downView.frame = CGRectMake(12,  MaxY(topView) + 14, self.view.mj_w - 24, 300);
    [self.view addSubview:downView];
    
    UILabel *label2 = [UILabel new];
    label2.frame = CGRectMake(12, 8, 86, 40);
    label2.font = kFontRegular(14);
    label2.textColor = app_font_color;
    label2.text = @"邮箱";
    [downView addSubview:label2];
    
    self.emailfield = [[UITextField alloc] init];
    self.emailfield.font = kFontRegular(14);
    self.emailfield.textColor = app_font_color;
    self.emailfield.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.emailfield.layer.cornerRadius = 4;
    self.emailfield.placeholder = @"请输入邮箱";
    self.emailfield.keyboardType  = UIKeyboardTypeEmailAddress;
    self.emailfield.frame = CGRectMake(88, 8, downView.mj_w - 88 - 44 , 40);
    self.emailfield.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, self.emailfield.mj_h)];
    self.emailfield.leftViewMode = UITextFieldViewModeAlways;
    [downView addSubview:self.emailfield];
    
    UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [editButton setImage:[UIImage imageNamed:@"xp_text_edit"] forState:UIControlStateNormal];
    [editButton addTarget:self action:@selector(editButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    editButton.frame = CGRectMake(downView.mj_w - 24 - 12, 16, 24, 24);
    [downView addSubview:editButton];
    
    self.joinButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.joinButton.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.joinButton.layer.cornerRadius = 25;
    self.joinButton.layer.masksToBounds = YES;
    self.joinButton.titleLabel.font = kFontRegular(16);
    [self.joinButton setTitle:@"加入创客" forState:UIControlStateNormal];
    [self.joinButton setBackgroundColor:UIColorFromRGB(0x6E6BFF)];
    self.joinButton.frame = CGRectMake(downView.center.x - 91, MaxY(editButton) + 40, 182, 50);
    [self.joinButton addTarget:self action:@selector(joinButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [downView addSubview:self.joinButton];
    
}

-(void)frontButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self showAlertWithMsg:@""];
}

-(void)editButtonClicked:(UIButton *)sender{
    buttonCanUseAfterOneSec(sender);
    [self.emailfield becomeFirstResponder];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark -- 图片上传
//上传图片
-(void)showAlertWithMsg:(NSString *)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       
    }];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openCamera];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openAlbum];
    }];
    [alert addAction:cancel];
    [alert addAction:action];
    [alert addAction:action2];

    [self presentViewController:alert animated:YES completion:nil];
}
- (void)openCamera {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}
- (void)openAlbum {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:true completion:^{
        
    }];
    NSString *type  = [info objectForKey:UIImagePickerControllerMediaType];
    //选择的类型图片
    if ([type isEqualToString:@"public.image"]) {
        //先把图片转成NSData
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        self.chooseImage = [image compressToImage];
    }
    [picker dismissViewControllerAnimated:YES completion:^{
        [self uploadCardWithImage:self.chooseImage];
    }];
  
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)uploadCardWithImage:(UIImage *)iConImage {
    
    iConImage = [iConImage compressToImage];
    NSString *filePath = [[XPOSSUploadTool shareInstance] saveImageToSandbox:iConImage];
    [self startLoadingGif];
    [[XPOSSUploadTool shareInstance] uploadFileWithMediaType:XPMediaTypeMakerFaceHand filePath:filePath progress:^(CGFloat progress) {
        
    } completeBlock:^(BOOL success, NSString * _Nonnull remoteUrlStr) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self stopLoadingGif];
            if (success) {
                //修改用户名
                self.frontImageView.image = self.chooseImage;
                self.picUrl = remoteUrlStr;
            }else{
                [[XPShowTipsTool shareInstance] showMsg:@"上传失败,请稍后重试!" ToView:k_keyWindow];
            }
        });
    }];
}


#pragma mark--- button Action
-(void)joinButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.picUrl.length == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"请上传照片" ToView:k_keyWindow];
    }else if (self.emailfield.text.length == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"请输入邮箱" ToView:k_keyWindow];
    }else if (![self.emailfield.text containsString:@".com"]){
        [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的邮箱" ToView:k_keyWindow];
    }else{
        [self submitApplyRequest];
    }
    
    
}

//获取个人创客账户信息
-(void)submitApplyRequest{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_apply];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"email"] = self.emailfield.text;
    paraM[@"cardHandPic"] = self.picUrl;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:@"提交成功!" ToView:k_keyWindow];
            [self goBack];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}


@end
