//
//  XPMakerViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/2.
//

#import "XPMakerViewController.h"
#import "XPSearchDefaultView.h"
#import "XPEmptyView.h"
#import "XPMakerTopView.h"
#import "XPMakerProductCell.h"
#import "XPMakerUserCell.h"
#import "XPMakerProductModel.h"
#import "XPMakerJoinController.h"
#import "XPShopGoodsDetailController.h"
#import "XPCreationAuthorController.h"

@interface XPMakerViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UIView *topView;
//搜索框
@property (nonatomic, strong) XPSearchDefaultView *searchView;
//加入创客状态按钮
@property (nonatomic, strong) UIButton *statusButton;
//中间bannerVIew
@property (nonatomic, strong) XPMakerTopView *centerView;
//列表
@property (nonatomic, strong) UITableView *tableView;
//无数据
@property (nonatomic, strong) XPEmptyView *emptyView;
//搜索遮罩
@property (nonatomic, strong) UIView *searchBgView;
//我的 /创客
@property (nonatomic, assign) NSInteger currentIndex;
//关键字
@property (nonatomic, copy) NSString *keyWords;
//当前页码
@property (nonatomic, assign) NSInteger page;
//顶部数据
@property (nonatomic, copy)   NSDictionary *topDic;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation XPMakerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentIndex = 1;
    [self setupViews];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getTopData];
    [self getData];
}

-(void)freshTopData {

    NSNumber *auditStatus = self.topDic[@"auditStatus"];
    
// 创客状态 10 未提交 0 待审核 1 审核通过 2 审核拒绝
    if (auditStatus.intValue == 10 || auditStatus.intValue == 2) {
        [self.searchView updateFrame:CGRectMake(16, self.safeTop + 6 ,SCREEN_WIDTH - 16 - 100, 32)];
        [self.statusButton setTitle:@"加入创客" forState:UIControlStateNormal];
        self.statusButton.hidden = NO;
        [self.statusButton setBackgroundImage:[UIImage imageNamed:@"xp_maker_joinbg"] forState:UIControlStateNormal];
        [self.statusButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else if(auditStatus.intValue == 0){
        [self.searchView updateFrame:CGRectMake(16, self.safeTop + 6,SCREEN_WIDTH - 16 - 100, 32)];
        [self.statusButton setTitle:@"审核中" forState:UIControlStateNormal];
        self.statusButton.hidden = NO;
        [self.statusButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [self.statusButton setTitleColor:UIColorFromRGB(0x6E6BFF)forState:UIControlStateNormal];
    }else {
        [self.searchView updateFrame:CGRectMake(16, self.safeTop + 6,SCREEN_WIDTH - 32, 32)];
        self.statusButton.hidden = YES;
    }
    
}

- (void)setupViews {
    UIView *topView = [[UIView alloc]init];
    topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 208 + kTopHeight);
    topView.clipsToBounds = YES;
    topView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.topView = topView;
    [self.view addSubview:topView];
    
    
    XPSearchDefaultView *searchView = [[XPSearchDefaultView alloc] initWithFrame:CGRectMake(16, self.safeTop + 6 ,SCREEN_WIDTH - 16 - 100, 32)];
    searchView.backgroundColor = UIColor.whiteColor;
    searchView.searchIcon.image = [UIImage imageNamed:@"xp_search_black"];
    searchView.canSearch = YES;
    self.searchView = searchView;
    [self.topView addSubview:searchView];

    self.statusButton = [[UIButton alloc] init];
    self.statusButton.layer.cornerRadius = 8;
    [self.statusButton setTitle:@"加入创客" forState:UIControlStateNormal];
    [self.statusButton setBackgroundImage:[UIImage imageNamed:@"xp_maker_joinbg"] forState:UIControlStateNormal];
    self.statusButton.frame = CGRectMake(SCREEN_WIDTH - 80 - 12, self.safeTop + 6, 80, 32);
    self.statusButton.titleLabel.font = kFontRegular(12);
    [self.statusButton addTarget:self action:@selector(statusButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.statusButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [self.view addSubview:self.statusButton];
    
    self.centerView = [[XPMakerTopView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, 208*SCREEN_WIDTH/375.)];
    [self.view addSubview:self.centerView];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, MaxY(self.centerView), SCREEN_WIDTH, SCREEN_HEIGHT - MaxY(self.centerView) - kTabBarHeight );
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 12)];
    [self.view addSubview:self.tableView];
     
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, self.tableView.mj_y + 100, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无数据"];
    [self.view addSubview:self.emptyView];
    
    UIView *searchBgView = [[UIView alloc] init];
    searchBgView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    searchBgView.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT);
    self.searchBgView = searchBgView;
    self.searchBgView.hidden = YES;
    [self.view addSubview:searchBgView];
    
    XPWeakSelf
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf headerRefresh];
    }];
    
    [self callback];
}
- (void)callback{
    XPWeakSelf
    
    [self.centerView setLunboBlock:^(NSDictionary * _Nonnull dic) {
        [XPRoutetool entryWithDic:dic];
    }];
    
    [self.searchView setSearchBlock:^(NSString * _Nonnull keyStr) {
        [weakSelf.view endEditing:YES];
        
        weakSelf.dataArray = @[].mutableCopy;
        weakSelf.keyWords = keyStr;
        weakSelf.page = 1;
        [weakSelf getData];
        
    }];
    
    [self.searchView setSearchStateBlock:^(BOOL state) {
        if (state) {
            weakSelf.tableView.hidden = YES;
            weakSelf.searchBgView.hidden = NO;
            weakSelf.emptyView.hidden = YES;
        }else{
//                weakSelf.topView.hidden = NO;
            weakSelf.searchBgView.hidden = YES;
            [weakSelf canShowEmptyView];
        }
    }];
    
    [self.centerView setSelectBlock:^(NSInteger index) {
        [weakSelf.dataArray removeAllObjects];
        [weakSelf.tableView reloadData];
        
        weakSelf.currentIndex = index;
        [weakSelf getData];
        
    }];
}


#pragma mark --- tableview delegete dataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.currentIndex == 1) {
        XPMakerProductCell *cell = [XPMakerProductCell cellWithTableView:tableView];
        XPMakerProductModel *model = self.dataArray[indexPath.row];
        [cell cellWithModel:model];
        return cell;
    }else{
        XPMakerUserCell *cell = [XPMakerUserCell cellWithTableView:tableView];
        XPMakerProductModel *model = self.dataArray[indexPath.row];
        [cell cellWithModel:model];
        return cell;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count - 1 == indexPath.row) {
        return 132 + 12 + 12;
    }return 132 + 12 ;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    XPMakerProductModel *model = self.dataArray[indexPath.row];
    
    if (self.currentIndex == 1) {
        XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.productSkuId = model.productSkuId;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        
        if (![[XPVerifyTool shareInstance] checkLogin]) {
            [[XPVerifyTool shareInstance] userShouldLoginFirst];
            return;
        }
        
        XPCreationAuthorController *vc = [XPCreationAuthorController new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.authorUserId = model.uID;
        [self.navigationController pushViewController:vc animated:YES];
    }

}


#pragma mark --- netWork
- (void)getData {
    NSString *url = [self getListUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    //类型 0 作品 1 创客
    paraM[@"type"] = @(self.currentIndex - 1);
    paraM[@"keywords"] = self.keyWords;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPMakerProductModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)headerRefresh{
    NSString *url = [self getListUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    //类型 0 作品 1 创客
    paraM[@"type"] = @(self.currentIndex - 1);
    paraM[@"keywords"] = self.keyWords;

    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray;
            modelArray = [XPMakerProductModel mj_objectArrayWithKeyValuesArray:dataArray];
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    NSString *url = [self getListUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    //类型 0 作品 1 创客
    paraM[@"type"] = @(self.currentIndex - 1);
    paraM[@"keywords"] = self.keyWords;

    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray ;
            modelArray = [XPMakerProductModel mj_objectArrayWithKeyValuesArray:dataArray];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self.tableView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

- (void)getTopData {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_home_data];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.topDic = data.userData;
            [self.centerView viewWithData:self.topDic];
            [self freshTopData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

-(void)statusButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    NSNumber *auditStatus = self.topDic[@"auditStatus"];
    // 创客状态 10 未提交 0 待审核 1 审核通过 2 审核拒绝
XPWeakSelf
    if (auditStatus.intValue == 10) {//未加入
        [self jumpToJoinMaker];
    }else if (auditStatus.intValue == 2){//已拒绝
        [self.xpAlertView showCancelAlertWithTitle:[NSString stringWithFormat:@"审核未通过,原因:\n%@\n是否继续提交审核?",self.topDic[@"auditReason"]] leftAction:^(NSString * _Nullable extra) {
            
        } rightAction:^(NSString * _Nullable extra) {
            [weakSelf jumpToJoinMaker];
        }];
    }else{//审核中 已加入等情况
        //不用处理
    }
    
}

-(void)jumpToJoinMaker {
    
    if (![[XPVerifyTool shareInstance] checkLogin]) {
        [[XPVerifyTool shareInstance] userShouldLoginFirst];
    }else if(![[XPVerifyTool shareInstance] checkUserAuthentication]){
        [[XPVerifyTool shareInstance] userShouldAuthentication];
    }else {
        XPMakerJoinController *vc = [[XPMakerJoinController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


-(NSString *)getListUrlPath{
    NSString *url;
    if (self.currentIndex == 1) {
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_list];
    }else{
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_list_my];
    }
    return url;
}

- (void)canShowEmptyView {
    if (self.dataArray.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}
@end
