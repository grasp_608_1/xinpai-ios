//
//  XPMakerBadgeController.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/2.
//

#import "XPMakerBadgeController.h"
#import "UIButton+parameter.h"

@interface XPMakerBadgeController ()
//z一斤
@property (nonatomic, strong) UIView *topView;

@property (nonatomic, strong) UIView *bottomView;

@end

@implementation XPMakerBadgeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    [self setupViews];
    [self getData];
//    [self freshUIWithData:@{}];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.navBarView];
    
    self.navBarView.titleLabel.text = @"我的徽章";
    
}

-(void)freshUIWithData:(NSDictionary *)dataDict{
    
    CGFloat top = kTopHeight;
    NSDictionary *badgeInfo = dataDict[@"badgeInfo"];
    
    [self.topView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (badgeInfo.allKeys.count > 0) {
        UIView *topTitleView = [self createTopTitleViewWithTitle:@"最新获得"];
        [self.topView addSubview:topTitleView];
        UIButton *topSignleButton = [self createBadgeButtonWithDic:badgeInfo];
        topSignleButton.frame = CGRectMake(self.topView.mj_w/2 - topSignleButton.mj_w /2., 56, topSignleButton.mj_w, topSignleButton.mj_h);
        [self.topView addSubview:topSignleButton];
        
        self.topView.frame = CGRectMake(12, top + 12, SCREEN_WIDTH - 24, MaxY(topSignleButton) + 16);
        topTitleView.frame = CGRectMake(self.topView.mj_w/2 - topTitleView.mj_w/2.,16, topTitleView.mj_w, topTitleView.mj_h);
        top = MaxY(self.topView);
    }else {
        self.topView.frame = CGRectZero;
    }
    
    
    
    NSArray *bottomArray = dataDict[@"badges"];
    
    UIView *bottomTitleView = [self createTopTitleViewWithTitle:@"全部徽章"];
    bottomTitleView.frame = CGRectMake(self.bottomView.mj_w/2 - bottomTitleView.mj_w/2.,16, bottomTitleView.mj_w, bottomTitleView.mj_h);
    [self.bottomView addSubview:bottomTitleView];
    
    CGFloat bottomH = 0;
    for (int i = 0; i< bottomArray.count; i++) {
        NSDictionary *tempDic = [bottomArray objectAtIndex:i];
        UIButton *bottomButton = [self createBadgeButtonWithDic:tempDic];
        
        CGFloat itemW = (self.view.mj_w - 24 - 16 - 20) /3.;
        CGFloat itemH = itemW + 4 + 24;
        
        bottomButton.frame = CGRectMake(8 + (itemW + 10) *(i%3), 56 + itemH *(i/3), itemW, itemH);
        [bottomButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        bottomButton.attachDic = tempDic;
        [self.bottomView addSubview:bottomButton];
        if (i == bottomArray.count - 1) {
            bottomH = MaxY(bottomButton);
        }
    }
    
    self.bottomView.frame = CGRectMake(12, top + 12, SCREEN_WIDTH - 24, bottomH + 12);
    
}

- (void)setupViews{
    
    self.topView = [UIView new];
    self.topView.frame = CGRectMake(12, kTopHeight + 12, SCREEN_WIDTH - 24, 200);
    self.topView.layer.cornerRadius = 8;
    self.topView.backgroundColor = UIColor.whiteColor;
    self.topView.clipsToBounds = YES;
    [self.view addSubview:self.topView];
    
    self.bottomView = [UIView new];
    self.bottomView.frame = CGRectMake(12, MaxY(self.topView), SCREEN_WIDTH - 24, 200);
    self.bottomView.layer.cornerRadius = 8;
    self.bottomView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:self.bottomView];
}

-(UIView *)createTopTitleViewWithTitle:(NSString *)titleStr {
    UIView *view = [UIView new];
    
    UIImageView *leftImageView = [[UIImageView alloc] init];
    leftImageView.image = [UIImage imageNamed:@"xp_maker_badge_left"];
    leftImageView.frame = CGRectMake(0, 5, 31, 14);
    [view addSubview:leftImageView];
    
    UIImageView *rightImageView = [[UIImageView alloc] init];
    rightImageView.image = [UIImage imageNamed:@"xp_maker_badge_right"];
    [view addSubview:rightImageView];

    UILabel *label = [UILabel new];
    label.font = kFontRegular(14);
    label.textColor = app_font_color;
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    label.text = titleStr;
    
    CGFloat labelW = [titleStr sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
    label.frame = CGRectMake(39, 0, labelW + 16, 24);
    rightImageView.frame = CGRectMake(MaxX(label) + 8, 5, 31, 14);
    view.frame = CGRectMake(0, 0, MaxX(rightImageView), 24);
    return view;
}

- (UIButton *)createBadgeButtonWithDic:(NSDictionary *)dic{
    
    CGFloat itemW = (self.view.mj_w - 24 - 16 - 20) /3.;
    CGFloat itemH = itemW + 4 + 24;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, itemW, itemH);
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, itemW, itemW)];
    [imageV sd_setImageWithURL:[NSURL URLWithString:dic[@"badgeUrl"]]];
    [button addSubview:imageV];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontRegular(14);
    titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel.frame = CGRectMake(0, MaxY(imageV) + 4, itemW, 24);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = dic[@"badgeName"];
    [button addSubview:titleLabel];
    
    return button;
}

#pragma mark --- netWork
//获取个人徽章
-(void)getData{
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kMaker_badge_my,self.userId];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [self freshUIWithData:data.userData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}


- (void)bottomButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self.xpAlertView showAlertWithTitle:sender.attachDic[@"badgeDesc"] actionBlock:^(NSString * _Nullable extra) {
        
    }];
    
}

@end
