//
//  XPMakerBadgeController.h
//  NewAuction
//
//  Created by Grasp_L on 2025/1/2.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPMakerBadgeController : XPBaseViewController

@property (nonatomic, strong) NSNumber *userId;

@end

NS_ASSUME_NONNULL_END
