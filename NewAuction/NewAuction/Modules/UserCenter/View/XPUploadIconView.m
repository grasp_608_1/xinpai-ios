//
//  XPUploadIconView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/17.
//

#import "XPUploadIconView.h"

@implementation XPUploadIconView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    UIView *backView = [UIView new];
    backView.frame = self.bounds;
    backView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
    [backView addGestureRecognizer:tap];
    backView.userInteractionEnabled = YES;
    [self addSubview:backView];
    
    
    UIView *mainView = [[UIView alloc] init];
    mainView.frame = CGRectMake(0, self.mj_h - 257 - 60 , self.mj_w,257 + 60);
    mainView.backgroundColor = UIColorFromRGB(0xF8F9FD);
    [XPCommonTool cornerRadius:mainView andType:1 radius:10];
    [self addSubview:mainView];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font= kFontMedium(16);
    titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel.backgroundColor = UIColor.whiteColor;
    titleLabel.text = @"修改个人头像";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.frame = CGRectMake(0, 0, self.mj_w, 60);
    self.titleLabel = titleLabel;
    [mainView addSubview:titleLabel];
    
    CGFloat margin = (self.mj_w- 64 *2)/3.;
    
    UIButton *albumBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    albumBtn.frame = CGRectMake(margin, MaxY(titleLabel) + 30, 64, 64);
    albumBtn.backgroundColor = UIColor.whiteColor;
    [albumBtn addTarget:self action:@selector(openAlbum:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:albumBtn];
    
    UIImageView *albumImageView = [UIImageView new];
    albumImageView.frame = CGRectMake(18, 18, 28, 28);
    albumImageView.image = [UIImage imageNamed:@"xp_album_new"];
    [albumBtn addSubview:albumImageView];
    
    UILabel *titleLabel1 = [UILabel new];
    titleLabel1.frame = CGRectMake(albumBtn.mj_x - 10, MaxY(albumBtn), albumBtn.mj_w + 20, 24);
    titleLabel1.text = @"从相册选择";
    titleLabel1.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel1.font = kFontRegular(14);
    titleLabel1.textAlignment = NSTextAlignmentCenter;
    titleLabel1.backgroundColor = UIColor.clearColor;
    [mainView addSubview:titleLabel1];
    
    
    UIButton *cameraBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cameraBtn.backgroundColor = UIColor.whiteColor;
    cameraBtn.frame = CGRectMake(self.mj_w - 64 - margin, MaxY(titleLabel) + 30, 64, 64);
    [cameraBtn addTarget:self action:@selector(openCamera:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:cameraBtn];
    
    UIImageView *cameraImageView = [UIImageView new];
    cameraImageView.frame = CGRectMake(18, 18, 28, 28);
    cameraImageView.image = [UIImage imageNamed:@"xp_camera_new"];
    [cameraBtn addSubview:cameraImageView];
    
    UILabel *titleLabel2 = [UILabel new];
    titleLabel2.frame = CGRectMake(cameraBtn.mj_x - 10, MaxY(albumBtn), albumBtn.mj_w + 20, 24);
    titleLabel2.text = @"拍照";
    titleLabel2.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel2.font = kFontRegular(14);
    titleLabel2.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:titleLabel2];
//    
    
    UIButton *submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [submitButton setTitle:@"完成" forState:UIControlStateNormal];
    [submitButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(submitClicked:) forControlEvents:UIControlEventTouchUpInside];
    submitButton.frame = CGRectMake(12, mainView.mj_h - 50 -30, self.mj_w - 24, 50);
    self.submitButton = submitButton;
    [mainView addSubview:submitButton];
    
    
}

- (void)openCamera:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender)
    if (self.openCamera) {
        self.openCamera();
    }
}

- (void)openAlbum:(UIButton *)sender{
    buttonCanUseAfterOneSec(sender)
    if (self.openAlbum) {
        self.openAlbum();
    }
}

- (void)submitClicked:(UIButton *)sender {
 buttonCanUseAfterOneSec(sender)
    if (self.submitBlock) {
        self.submitBlock();
    }
}
- (void)tap{
    self.hidden = YES;
}

@end
