//
//  XPUserCenterCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/12.
//

#import "XPUserCenterCell.h"

@interface XPUserCenterCell ()

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel *desLabel;

@property (nonatomic, strong) UIImageView *rightArrow;

@end


@implementation XPUserCenterCell
+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPUserCenterCell";
    XPUserCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPUserCenterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xFAFAFA);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    self.mainView = [UIView new];
    self.mainView.backgroundColor = UIColor.whiteColor;
    self.mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 24, 58);
    [self addSubview:self.mainView];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.frame = CGRectMake(22, 16, 24, 24);
    [self.mainView addSubview:self.iconImageView];
    
    self.desLabel = [UILabel new];
    self.desLabel.frame = CGRectMake(MaxX(self.iconImageView)+ 14, 0, 100, self.mainView.bounds.size.height);
    self.desLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.desLabel.font = kFontRegular(16);
    [self.mainView addSubview:self.desLabel];
    
    self.rightArrow = [UIImageView new];
    self.rightArrow.frame = CGRectMake(self.mainView.frame.size.width - 20 - 24, 17, 24, 24);
    self.rightArrow.image = [UIImage imageNamed:@"xp_setting_arrow"];
    [self.mainView addSubview:self.rightArrow];
    
    self.downLine = [[UIView alloc] init];
    self.downLine.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.downLine.frame = CGRectMake(20, self.mainView.frame.size.height - 1, self.mainView.frame.size.width - 20 *2, 1);
    [self.mainView addSubview:self.downLine];
    
    
}

- (void)cellWithtitle:(NSString *)title IconName:(NSString *)iconName{
    self.desLabel.text = title;
    self.iconImageView.image = [UIImage imageNamed:iconName];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
