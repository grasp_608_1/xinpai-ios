//
//  XPProductSortCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/11.
//

#import <UIKit/UIKit.h>
#import "XPProductModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^TapBlock)(void);
@interface XPProductSortCell : UITableViewCell
/// cell 初始化方法
+ (instancetype)cellWithTableView:(UITableView *)tableView;

//1 分类 2 我的收藏
@property (nonatomic, assign) NSInteger cellType;

- (void)cellWithModel:(XPProductModel *)model passedTime:(NSInteger)passedTime;

@property (nonatomic, copy) TapBlock tapBlock;
@end

NS_ASSUME_NONNULL_END
