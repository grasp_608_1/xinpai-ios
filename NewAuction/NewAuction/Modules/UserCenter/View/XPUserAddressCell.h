//
//  XPUserAddressCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/13.
//

#import <UIKit/UIKit.h>
#import "XPAdressModel.h"
#import "XPHitButton.h"
NS_ASSUME_NONNULL_BEGIN

@interface XPUserAddressCell : UITableViewCell

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UILabel *nameLable;

@property (nonatomic, strong) UILabel *adressLabel;

@property (nonatomic, strong) UIView *downLine;

@property (nonatomic, strong) XPHitButton *rightArrow;

//0 默认样式可编辑, 1 只能选中,不能编辑状态
@property (nonatomic, assign) NSInteger cellType;

@property (nonatomic, copy) void(^editBlock)(void);




+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (void)cellWithDic:(XPAdressModel *)model;

@end

NS_ASSUME_NONNULL_END
