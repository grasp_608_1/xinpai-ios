//
//  XPUserLevelView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/8/28.
//

#import "XPUserLevelView.h"

@interface XPUserLevelView ()
//用户角色头像
@property (nonatomic, strong) UIImageView *userRoleIcon;

@end


@implementation XPUserLevelView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.backgroundColor = UIColorFromRGB(0xFCFCFF);
    self.titleLabel.font = kFontMedium(10);
    [self setTitleColor: UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
    self.layer.cornerRadius = 9;
    self.layer.masksToBounds = YES;
    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    self.userRoleIcon = [UIImageView new];
    self.userRoleIcon.frame = CGRectMake(self.mj_w - 3 - 24, -2, 22, 22);
    [self addSubview:self.userRoleIcon];
}

-(void)setUserModel:(XPUserInfoModel *)userModel{
    
    [self setTitle:[NSString stringWithFormat:@"  LV%@ %@",userModel.userType,[XPUserDataTool getUserRole:userModel.userType.integerValue]] forState:UIControlStateNormal];
    
    self.userRoleIcon.image = [XPUserDataTool getUserRoleIcon:userModel.userType.integerValue];
}

@end
