//
//  XPGoodManagerCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/17.
//

#import <UIKit/UIKit.h>
#import "XPAuctionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPGoodManagerCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (void)cellWithModel:(XPAuctionModel *)model;

@property (nonatomic, copy) void(^leftBlock)(void);

@property (nonatomic, copy) void(^rightBlock)(void);

@end

NS_ASSUME_NONNULL_END
