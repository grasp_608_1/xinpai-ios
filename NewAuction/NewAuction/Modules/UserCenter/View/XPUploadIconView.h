//
//  XPUploadIconView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPUploadIconView : UIView

@property (nonatomic, copy) void(^submitBlock)(void);
@property (nonatomic, copy) void(^openAlbum)(void);
@property (nonatomic, copy) void(^openCamera)(void);

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIButton *submitButton;
@end

NS_ASSUME_NONNULL_END
