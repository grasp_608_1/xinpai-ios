//
//  XPBankCardView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/17.
//

#import "XPBankCardCell.h"
#import "XPHitButton.h"

@interface XPBankCardCell ()

@property (nonatomic, strong) UIView *mainView;
@property (nonatomic ,strong) UIImageView *iconImageView;
@property (nonatomic ,strong) UIImageView *bgImageView;

@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;
@property (nonatomic, strong) UILabel *label3;
@property (nonatomic, strong) UILabel *label4;

@property (nonatomic,strong) XPHitButton *rightButton;

@end

@implementation XPBankCardCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPBankCardCell";
    XPBankCardCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPBankCardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xFAFAFA);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    UIView *mainView = [UIView new];
    mainView.layer.cornerRadius = 8;
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 12, SCREEN_WIDTH - 24, 120);
    mainView.layer.cornerRadius = 8;
    mainView.layer.masksToBounds = YES;
    self.mainView = mainView;
    [self.contentView addSubview:mainView];
    
    UIImageView *bgImageView = [UIImageView new];
    bgImageView.frame = self.mainView.bounds;
    self.bgImageView = bgImageView;
    [self.mainView addSubview:bgImageView];
    

    self.label1 = [UILabel new];
    self.label1.textColor = UIColorFromRGB(0x3A3C41);
    self.label1.font = kFontMedium(18);
    self.label1.frame = CGRectMake(16, 16, self.mainView.mj_w - 16 - 90, 24);
    [self.mainView addSubview:self.label1];
    
    self.label2 = [UILabel new];
    self.label2.textColor = RGBACOLOR(58, 60, 65, 0.5);
    self.label2.font = kFontRegular(14);
    self.label2.frame = CGRectMake(16, MaxY(self.label1) + 6, 100, 18);
    self.label2.text = @"储蓄卡";
    [self.mainView addSubview:self.label2];
    
    self.label3 = [UILabel new];
    self.label3.textColor = UIColorFromRGB(0x3A3C41);
    self.label3.font = kFontMedium(28);
    self.label3.textAlignment = NSTextAlignmentRight;
    self.label3.frame = CGRectMake(0, 72, self.mainView.mj_w - 16, 32);
    self.label3.textAlignment = NSTextAlignmentRight;
    [self.mainView addSubview:self.label3];
    
    self.rightButton = [[XPHitButton alloc] init];
    self.rightButton.hitRect = 40;
    self.rightButton.frame = CGRectMake(self.mainView.mj_w - 16 - 70, 16, 70, 28);
    [self.rightButton setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
    [self.rightButton setTitle:@"解绑" forState:UIControlStateNormal];
    self.rightButton.titleLabel.font = kFontRegular(14);
    self.rightButton.layer.cornerRadius = 14;
    self.rightButton.layer.borderColor = UIColorFromRGB(0x3A3C41).CGColor;
    self.rightButton.layer.borderWidth = 1;
    [self.rightButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.rightButton];
    
}

-(void)cellWithModel:(XPBankModel *)model {
    
    NSString *str = model.cardNo;
    if (str.length > 4) {
        str =  [str substringWithRange:NSMakeRange(str.length - 4, 4)];
        
    }
    str = [NSString stringWithFormat:@"**** **** **** %@",str];
    
    self.label1.text = model.bankName;
    self.label3.text = str;
    
    if (self.cellType == 0) {//支付卡
        self.bgImageView.image = [UIImage imageNamed:@"xp_bank_bg1"];
    }else {
        self.bgImageView.image = [UIImage imageNamed:@"xp_bank_bg2"];
    }
    
}

- (void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.rightBlock) {
        self.rightBlock();
    }
    
}


@end
