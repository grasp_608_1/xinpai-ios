//
//  XPUserAddressCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/13.
//

#import "XPUserAddressCell.h"


@interface XPUserAddressCell ()

@property (nonatomic, strong) UILabel *defaultLabel;

@end

@implementation XPUserAddressCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPUserAddressCell";
    XPUserAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPUserAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    self.mainView = [UIView new];
    self.mainView.backgroundColor = UIColor.whiteColor;
    self.mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 24, 86);
    [self.contentView addSubview:self.mainView];
    
    self.nameLable = [UILabel new];
    self.nameLable.textColor = UIColorFromRGB(0x3A3C41);
    self.nameLable.font = kFontMedium(14);
    self.nameLable.frame = CGRectMake(12, 12, self.mainView.mj_w - 24, 20);
    [self.mainView addSubview:self.nameLable];
    
    self.adressLabel = [UILabel new];
    self.adressLabel.frame = CGRectMake(12, MaxY(self.nameLable) + 6, self.mainView.mj_w - 60, 40);
    self.adressLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.adressLabel.font = kFontRegular(12);
    self.adressLabel.numberOfLines = NO;
    [self.mainView addSubview:self.adressLabel];
    
    self.defaultLabel = [UILabel new];
    self.defaultLabel.frame = CGRectMake(MaxX(self.nameLable) + 6, 12, 60, 20);
    self.defaultLabel.textColor = UIColorFromRGB(0x6E6BFF);
    self.defaultLabel.font = kFontRegular(12);
    self.defaultLabel.numberOfLines = NO;
    self.defaultLabel.text = @"默认地址";
    self.defaultLabel.hidden = YES;
    [self.mainView addSubview:self.defaultLabel];
    
    self.rightArrow = [XPHitButton new];
    self.rightArrow.frame = CGRectMake(self.mainView.frame.size.width - 12 - 24, 32, 24, 24);
    [self.rightArrow setImage:[UIImage imageNamed:@"xp_text_edit"] forState:UIControlStateNormal];
    [self.rightArrow addTarget:self action:@selector(editButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.rightArrow.hitRect = 86;
    [self.mainView addSubview:self.rightArrow];
    
    self.downLine = [[UIView alloc] init];
    self.downLine.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.downLine.frame = CGRectMake(12, self.mainView.frame.size.height - 1, self.mainView.frame.size.width - 12 *2, 1);
    [self.mainView addSubview:self.downLine];
    
}

- (void)cellWithDic:(XPAdressModel *)model {
    
    NSString *nameStr = [NSString stringWithFormat:@"%@     %@",model.name,model.phone];
    
    self.nameLable.text = nameStr;
    
    CGFloat labelW = [nameStr sizeWithAttributes:@{NSFontAttributeName:kFontMedium(14)}].width;
    
    self.nameLable.frame = CGRectMake(12, 12, labelW, 20);
    self.defaultLabel.frame = CGRectMake(MaxX(self.nameLable) + 6, 12, 60, 20);
    
    NSString *areaStr = [model.area stringByReplacingOccurrencesOfString:@"," withString:@""];
    self.adressLabel.text =  [NSString stringWithFormat:@"%@ %@",areaStr,model.address];
    if (model.isDefault.intValue == 1) {
        self.defaultLabel.hidden = NO;
    }else {
        self.defaultLabel.hidden = YES;
    }
    
    if (self.cellType == 1) {
        self.rightArrow.userInteractionEnabled = NO;
        if ([model.isSelected isEqualToString:@"1"]) {//选中状态
            [self.rightArrow setImage:[UIImage imageNamed:@"xp_dot_selected"] forState:UIControlStateNormal];
        }else{//未选中状态
            [self.rightArrow setImage:[UIImage imageNamed:@"xp_location"] forState:UIControlStateNormal];
        }
        
    }
    
}

- (void)editButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.editBlock) {
        self.editBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
