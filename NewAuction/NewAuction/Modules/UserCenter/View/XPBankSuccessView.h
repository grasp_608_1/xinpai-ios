//
//  XPBankSuccessView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPBankSuccessView : UIView


@property (nonatomic, copy) void(^successBlock)(void);

@end

NS_ASSUME_NONNULL_END
