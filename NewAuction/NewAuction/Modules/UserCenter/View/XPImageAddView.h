//
//  XPImageAddView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPImageAddView : UIView

/// 区分不同的业务场景 0 用户反馈 1 售后申请时图片上传 2 商品评价
@property (nonatomic, assign) NSInteger type;

@property (nonatomic, copy) void(^changeBlock)(NSArray *imagesArray);

@end

NS_ASSUME_NONNULL_END
