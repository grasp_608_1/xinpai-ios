//
//  XPRechargeTipPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/31.
//

#import "XPRechargeTipPopView.h"
#import "XPDoubleLabelView.h"

@interface XPRechargeTipPopView ()

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *desLabel;

@property (nonatomic, strong) XPDoubleLabelView *label1;
@property (nonatomic, strong) XPDoubleLabelView *label2;
@property (nonatomic, strong) XPDoubleLabelView *label3;
@property (nonatomic, strong) XPDoubleLabelView *label4;

@property (nonatomic, strong) UILabel *tipsLabel;

@property (nonatomic, strong) UIButton *sureButton;

@end


@implementation XPRechargeTipPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    
    UIView *bgView = [[UIView alloc] initWithFrame:self.bounds];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    [self addSubview:bgView];
    
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.layer.cornerRadius = 20;
    mainView.layer.masksToBounds = YES;
    mainView.frame = CGRectMake(40,self.mj_h/2 - 200 , self.mj_w - 80, 0);
    self.mainView = mainView;
    [self addSubview:mainView];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.textColor = app_font_color;
    titleLabel.font = kFontMedium(16);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.frame = CGRectMake(20, 20, mainView.mj_w - 40, 20);
    titleLabel.numberOfLines = NO;
    self.titleLabel = titleLabel;
    [mainView addSubview:titleLabel];
    
    UILabel *desLabel = [[UILabel alloc] init];
    desLabel.textColor = app_font_color;
    desLabel.font = kFontMedium(14);
    desLabel.frame = CGRectMake(24, MaxY(self.titleLabel) + 16, mainView.mj_w -48 , 24);
    desLabel.numberOfLines = NO;
    self.desLabel = desLabel;
    [mainView addSubview:desLabel];
    
    XPDoubleLabelView *label1 = [[XPDoubleLabelView alloc] initWithFrame:CGRectMake(24, MaxY(self.desLabel) + 12, self.mainView.mj_w - 48, 32)];
    label1.layer.borderColor = UIColorFromRGB(0xF2F2F2).CGColor;
    label1.layer.borderWidth = 1;
    [XPCommonTool cornerRadius:label1 andType:1 radius:4];
    self.label1 = label1;
    [mainView addSubview:label1];
    
    XPDoubleLabelView *label2 = [[XPDoubleLabelView alloc] initWithFrame:CGRectMake(24, MaxY(self.label1) - 1, self.mainView.mj_w - 48, 32)];
    label2.layer.borderColor = UIColorFromRGB(0xF2F2F2).CGColor;
    label2.layer.borderWidth = 1;
    [XPCommonTool cornerRadius:label2 andType:2 radius:4];
    self.label2 = label2;
    [mainView addSubview:label2];
    
    XPDoubleLabelView *label3 = [[XPDoubleLabelView alloc] initWithFrame:CGRectMake(24, MaxY(self.label2) - 1, self.mainView.mj_w - 48, 32)];
    label3.layer.borderColor = UIColorFromRGB(0xF2F2F2).CGColor;
    label3.layer.borderWidth = 1;
    [XPCommonTool cornerRadius:label3 andType:2 radius:4];
    self.label3 = label3;
    [mainView addSubview:label3];
    
    XPDoubleLabelView *label4 = [[XPDoubleLabelView alloc] initWithFrame:CGRectMake(24, MaxY(self.label3) - 1, self.mainView.mj_w - 48, 32)];
    label4.layer.borderColor = UIColorFromRGB(0xF2F2F2).CGColor;
    label4.layer.borderWidth = 1;
    [XPCommonTool cornerRadius:label4 andType:2 radius:4];
    self.label4 = label4;
    [mainView addSubview:label4];
    
    UILabel *tipsLabel = [[UILabel alloc] init];
    tipsLabel.textColor = app_font_color_8A;
    tipsLabel.font = kFontRegular(12);
    tipsLabel.frame = CGRectMake(20, MaxY(self.label4) + 8, mainView.mj_w -48 , 24);
    tipsLabel.numberOfLines = NO;
    self.tipsLabel = tipsLabel;
    [mainView addSubview:tipsLabel];
    
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sureButton.frame = CGRectMake(57,MaxY(self.tipsLabel) + 30,self.mainView.mj_w - 114, 32);
    [sureButton setTitle:@"我知道了" forState:UIControlStateNormal];
    sureButton.titleLabel.font = kFontMedium(14);
    [sureButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    sureButton.layer.cornerRadius = 16;
    sureButton.clipsToBounds = YES;
    sureButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
    [sureButton addTarget:self action:@selector(sureButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.sureButton = sureButton;
    [self.mainView addSubview:sureButton];
    [sureButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :sureButton colors:
                                       @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                         (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    self.alpha = 0;
    
}

-(void)viewWithDictionary:(NSDictionary *)dict {
    self.titleLabel.text = @"手续费说明";
    self.desLabel.text = @"支付时,第三方支付平台将收取您的手续费,费率如下：";
    CGFloat desH = [self.desLabel.text boundingRectWithSize:CGSizeMake(self.desLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.desLabel.font} context:nil].size.height;
    self.desLabel.frame = CGRectMake(24, MaxY(self.titleLabel) + 16, self.mainView.mj_w -48 , desH);
    
    self.label1.leftStr = @"对公转账";
    self.label1.rightStr = @"无手续费";
    self.label1.frame = CGRectMake(24, MaxY(self.desLabel) + 12, self.mainView.mj_w - 48, 32);
    
    self.label2.leftStr = @"杉德支付";
    self.label2.rightStr = [NSString stringWithFormat:@"%@%%",dict[@"percentSandyStr"]];
    self.label2.frame = CGRectMake(24, MaxY(self.label1) - 1, self.mainView.mj_w - 48, 32);
    
    self.label3.leftStr = @"微信支付";
    self.label3.rightStr = [NSString stringWithFormat:@"%@%%",dict[@"percentWechatStr"]];
    self.label3.frame = CGRectMake(24, MaxY(self.label2) - 1, self.mainView.mj_w - 48, 32);
    
    self.label4.leftStr = @"支付宝支付";
    self.label4.rightStr = [NSString stringWithFormat:@"%@%%",dict[@"percentAlipayStr"]];
    self.label4.frame = CGRectMake(24, MaxY(self.label3) - 1, self.mainView.mj_w - 48, 32);
    
    self.tipsLabel.text = @"新注册用户享有5000元免手续费额度，有效期1年，超出后需要向第三方支付手续费。免额期间手续费由平台补贴。";
    CGFloat tipH = [self.tipsLabel.text boundingRectWithSize:CGSizeMake(self.tipsLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.tipsLabel.font} context:nil].size.height;

    self.tipsLabel.frame = CGRectMake(24, MaxY(self.label4) + 16, self.mainView.mj_w -48 , tipH);
    
    self.sureButton.frame = CGRectMake(57,MaxY(self.tipsLabel) + 30,self.mainView.mj_w - 114, 32);
    
    CGFloat mainH = MaxY(self.sureButton) + 24;
    self.mainView.frame = CGRectMake(40,self.mj_h/2 - mainH/2. - 50 , self.mj_w - 80, mainH);
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1;
    }];
}

- (void)sureButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
    
}

@end
