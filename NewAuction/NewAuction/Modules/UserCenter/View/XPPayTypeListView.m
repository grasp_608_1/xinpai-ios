//
//  XPPayTypeListView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/31.
//

#import "XPPayTypeListView.h"


@interface XPPayTypeListView ()
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//副标题
@property (nonatomic, strong) UILabel *descLabel;
//支付方式图标
@property (nonatomic, strong) UIImageView *iconImageView;
//提示
@property (nonatomic, strong) XPHitButton *tipButton;


@end

@implementation XPPayTypeListView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    UIView *sepView = [[UIView alloc] initWithFrame:CGRectMake(24, 0, self.mj_w - 24 * 2, 1)];
    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    sepView.hidden = YES;
    self.sepView = sepView;
    [self addSubview:sepView];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.frame = CGRectMake(24, 20, 24, 24);
    [self addSubview:self.iconImageView];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = kFontRegular(16);
    self.titleLabel.textColor = app_font_color;
    self.titleLabel.frame = CGRectMake(57, 12, self.mj_w - 57, 24);
    [self addSubview:self.titleLabel];
    
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = kFontRegular(12);
    self.descLabel.textColor = app_font_color_8A;
    self.descLabel.frame = CGRectMake(self.titleLabel.mj_x, MaxY(self.titleLabel), self.mj_w - 57, 16);
    [self addSubview:self.descLabel];
    
    self.tipButton = [XPHitButton new];
    self.tipButton.hitRect = 30;
    self.tipButton.hidden = YES;
    [self.tipButton setBackgroundImage:[UIImage imageNamed:@"xp_need_help"] forState:UIControlStateNormal];
    [self.tipButton addTarget:self action:@selector(tipButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.tipButton];
    
    self.selectedButton = [XPHitButton new];
    [self.selectedButton setBackgroundImage:[UIImage imageNamed:@"xp_xbox_unselect"] forState:UIControlStateNormal];
    [self.selectedButton setBackgroundImage:[UIImage imageNamed:@"xp_xbox_selected"] forState:UIControlStateSelected];
    [self.selectedButton addTarget:self action:@selector(selectedButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.selectedButton.hitRect = 30;
    self.selectedButton.frame = CGRectMake(self.mj_w - 24 - 24, 20, 24, 24);
    [self addSubview:self.selectedButton];
    
    
}

-(void)viewWithDict:(NSDictionary *)dic showTips:(BOOL)isShow showDesc:(BOOL)showDesc{
    
    self.iconImageView.image = [UIImage imageNamed:dic[@"pic"]];
    self.titleLabel.text = dic[@"title"];
    
    if (showDesc) {
        NSString *tips = dic[@"desc"];
        self.descLabel.text = tips;
        if (isShow) {
            self.tipButton.hidden = NO;
            CGFloat tipW = [tips sizeWithAttributes:@{NSFontAttributeName:self.descLabel.font}].width;
            self.tipButton.frame = CGRectMake(tipW + 2 + self.descLabel.mj_x, self.descLabel.mj_y, 14, 14);
        }
    }else {
        self.titleLabel.frame = CGRectMake(57, 0, self.mj_w - 57, self.mj_h);
    }
      
}

- (void)tipButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.tipBlock) {
        self.tipBlock();
    }
}
- (void)selectedButtonClicked:(UIButton *)sender {
    if (self.selectedBlock) {
        self.selectedBlock(sender.tag);
    }
}

@end
