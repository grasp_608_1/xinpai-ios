//
//  XPUserFansCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/12.
//

#import "XPUserFansCell.h"

@interface XPUserFansCell ()

@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *timeLabel;
//头像
@property (nonatomic, strong) UIImageView *iconImageView;
//等级
@property (nonatomic, strong) UIImageView *levelIcon;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UILabel *profitLabel;

@end

@implementation XPUserFansCell


+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPUserFansCell";
    XPUserFansCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPUserFansCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    CGFloat mainW = SCREEN_WIDTH - 24;
    CGFloat imagWH = 48;
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 12,mainW,72);
    mainView.layer.cornerRadius = 8;
    mainView.clipsToBounds = YES;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    UIImageView *icon = [UIImageView new];
    icon.frame = CGRectMake(12, 12, imagWH,imagWH);
    icon.layer.cornerRadius = imagWH/2;
    icon.layer.masksToBounds = YES;
    self.iconImageView = icon;
    [mainView addSubview:icon];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = kFontMedium(16);
    self.titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    [mainView addSubview:self.titleLabel];
    
    UIImageView *levelIcon = [UIImageView new];
    levelIcon.frame = CGRectMake(12, 12,0,0);
    levelIcon.layer.cornerRadius = 12;
    levelIcon.layer.masksToBounds = YES;
    self.levelIcon = levelIcon;
    [mainView addSubview:levelIcon];
    
    self.phoneLabel = [UILabel new];
    self.phoneLabel.font = kFontRegular(14);
    self.phoneLabel.textColor = UIColorFromRGB(0x8A8A8A);
    [mainView addSubview:self.phoneLabel];
    
    self.profitLabel = [UILabel new];
    self.profitLabel.font = kFontMedium(16);
    self.profitLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.profitLabel.textAlignment = NSTextAlignmentRight;
    
    [mainView addSubview:self.profitLabel];
    
    self.timeLabel = [UILabel new];
    self.timeLabel.font = kFontRegular(12);
    self.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.timeLabel.textAlignment = NSTextAlignmentRight;
    [mainView addSubview:self.timeLabel];
}

-(void)setModel:(XPFansModel *)model{
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.head] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    NSString *profitStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.cashCount]];
    CGFloat profitW = [profitStr sizeWithAttributes:@{NSFontAttributeName: self.profitLabel.font}].width;
    NSString *timeStr = [NSString stringWithFormat:@"注册日期 %@",model.createDate];
    CGFloat timeW = [timeStr sizeWithAttributes:@{NSFontAttributeName:self.timeLabel.font}].width;
    
    self.titleLabel.text = model.nickname;
    self.profitLabel.text = profitStr;
    self.phoneLabel.text = model.phone;
    self.timeLabel.text = timeStr;
    self.levelIcon.image = [XPUserDataTool getUserRoleIcon:model.userType.integerValue];
    
    CGFloat titleW = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}].width;

    self.profitLabel.frame = CGRectMake(self.mainView.mj_w - 12 - profitW,12,profitW, 24);
    self.titleLabel.frame = CGRectMake(72, 12, titleW, 24);
    self.levelIcon.frame = CGRectMake(MaxX(self.titleLabel) + 4, self.titleLabel.mj_y, 24, 24);
    self.timeLabel.frame = CGRectMake(self.mainView.mj_w - timeW - 12, MaxY(self.profitLabel), timeW, 24);
    self.phoneLabel.frame = CGRectMake(self.titleLabel.mj_x, MaxY(self.titleLabel), self.mainView.mj_w - 72 - timeW - 12 - 5, 24);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
