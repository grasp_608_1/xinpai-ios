//
//  XPProductCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/11.
//

#import <UIKit/UIKit.h>
#import "XPAuctionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPProductCell : UITableViewCell

/// cell 初始化方法
+ (instancetype)cellWithTableView:(UITableView *)tableView;

//1 委托参拍商品  2 委托委拍商品  3 仓库参拍 4 仓库待提货 5 仓库已提货
@property (nonatomic, assign) NSInteger cellType;

- (void)cellWithModel:(XPAuctionModel *)model;

@property (nonatomic, copy) void(^leftBlock)(NSString *extra);
@property (nonatomic, copy) void(^rightBlock)(NSString *extra);

@end

NS_ASSUME_NONNULL_END
