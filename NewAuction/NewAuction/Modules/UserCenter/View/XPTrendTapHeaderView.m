//
//  XPTrendTapHeaderView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/26.
//

#import "XPTrendTapHeaderView.h"

@interface XPTrendTapHeaderView ()
//总金额
@property (nonatomic, strong) UILabel *topTotalLabel;
//左侧按钮
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, copy) NSString *leftTitle;
@property (nonatomic, strong) UILabel *leftTotalDescLabel;
@property (nonatomic, strong) UILabel *leftTotalLabel;
//右侧按钮
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UILabel *rightTotalDescLabel;
@property (nonatomic, strong) UILabel *rightTotalLabel;
@property (nonatomic, copy) NSString *rightTitle;

@property (nonatomic, assign) NSInteger index;

@end


@implementation XPTrendTapHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
 
    self.index = -1;
    self.topTotalLabel = [UILabel new];
    self.topTotalLabel.font = kFontMedium(16);
    self.topTotalLabel.textColor = app_font_color;
    self.topTotalLabel.textAlignment = NSTextAlignmentCenter;
    self.topTotalLabel.frame = CGRectMake(0, 4, self.mj_w, 24);
    [self addSubview:self.topTotalLabel];
    
    CGFloat buttonW = 171;
    CGFloat buttonH = 40;
    
    self.leftButton = [[UIButton alloc] init];
    self.leftButton.frame = CGRectMake(self.mj_w/2. - buttonW  + 25 , 40, buttonW, buttonH);
    [self.leftButton setTitleColor:app_font_color_8A forState:UIControlStateNormal];
    self.leftButton.tag = 0;
    self.leftButton.titleLabel.font = kFontRegular(16);
    [self.leftButton setTitleColor:app_font_color_8A forState:UIControlStateNormal];
    [self.leftButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.leftButton];
    
    self.leftTotalDescLabel = [UILabel new];
    self.leftTotalDescLabel.font = kFontRegular(12);
    self.leftTotalDescLabel.textColor = app_font_color_8A;
    self.leftTotalDescLabel.textAlignment = NSTextAlignmentCenter;
    self.leftTotalDescLabel.frame = CGRectMake(0, 2, self.leftButton.mj_w, 18);
    [self.leftButton addSubview:self.leftTotalDescLabel];
    
    self.leftTotalLabel = [UILabel new];
    self.leftTotalLabel.font = kFontMedium(12);
    self.leftTotalLabel.textColor = app_font_color;
    self.leftTotalLabel.textAlignment = NSTextAlignmentCenter;
    self.leftTotalLabel.frame = CGRectMake(0, 20, self.leftButton.mj_w, 18);
    [self.leftButton addSubview:self.leftTotalLabel];
     
    self.rightButton = [[UIButton alloc] init];
    self.rightButton.frame = CGRectMake(self.mj_w/2. - 25 , 40, buttonW, buttonH);
    [self.rightButton setTitleColor:app_font_color_8A forState:UIControlStateNormal];
    self.rightButton.tag = 1;
    self.rightButton.titleLabel.font = kFontRegular(16);
    [self.rightButton setTitleColor:app_font_color_8A forState:UIControlStateNormal];
    [self.rightButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.rightButton];
    
    self.rightTotalDescLabel = [UILabel new];
    self.rightTotalDescLabel.font = kFontRegular(12);
    self.rightTotalDescLabel.textColor = app_font_color_8A;
    self.rightTotalDescLabel.textAlignment = NSTextAlignmentCenter;
    self.rightTotalDescLabel.frame = CGRectMake(0, 2, self.rightButton.mj_w, 18);
    [self.rightButton addSubview:self.rightTotalDescLabel];
    
    self.rightTotalLabel = [UILabel new];
    self.rightTotalLabel.font = kFontMedium(12);
    self.rightTotalLabel.textColor = app_font_color;
    self.rightTotalLabel.textAlignment = NSTextAlignmentCenter;
    self.rightTotalLabel.frame = CGRectMake(0, 20, self.rightButton.mj_w, 18);
    [self.rightButton addSubview:self.rightTotalLabel];

}

-(void)freshWithTopTitle:(NSString *)totalStr
                letTitle:(NSString *)leftTitle
         leftTitleDesStr:(NSString *)leftDesc
            leftAmountTitleStr:(NSString *)leftAmountTitleStr
                rightTitle:(NSString *)rightTitle
       rightTitleDescStr:(NSString *)rigthTitleDescStr
           rightAmountTitle:(NSString *)rightAmountTitleStr {
    
    _leftTitle = leftTitle;
    _rightTitle = rightTitle;
    
    self.topTotalLabel.text = totalStr;
    
    
    self.leftTotalDescLabel.text = leftDesc;
    self.leftTotalLabel.text = leftAmountTitleStr;

    self.rightTotalDescLabel.text = rigthTitleDescStr;
    self.rightTotalLabel.text = rightAmountTitleStr;
    
    if (self.index == -1 || self.index == 0) {
        [self buttonClicked:self.leftButton];
    }else {
        [self buttonClicked:self.rightButton];
    }
    
    
}


-(void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (sender.tag == self.index) {
        return;
    }
    
    if (sender.tag == 0) {
        
        self.leftTotalLabel.hidden = NO;
        self.leftTotalDescLabel.hidden = NO;
        self.rightTotalLabel.hidden = YES;
        self.rightTotalDescLabel.hidden = YES;
        
        [self.leftButton setTitle:@"" forState:UIControlStateNormal];
        [self.rightButton setTitle:self.rightTitle forState:UIControlStateNormal];
       
        [self.leftButton setBackgroundImage:[UIImage imageNamed:@"xp_trend_topSelected"] forState:UIControlStateNormal];
        [self.rightButton setBackgroundImage:[UIImage imageNamed:@"xp_trend_topNormal"] forState:UIControlStateNormal];
        
        [self bringSubviewToFront:self.leftButton];
        
    }else {
        
        self.leftTotalLabel.hidden = YES;
        self.leftTotalDescLabel.hidden = YES;
        self.rightTotalLabel.hidden = NO;
        self.rightTotalDescLabel.hidden = NO;
        
        [self.leftButton setTitle:self.leftTitle forState:UIControlStateNormal];
        [self.rightButton setTitle:@"" forState:UIControlStateNormal];
       
        [self.leftButton setBackgroundImage:[UIImage imageNamed:@"xp_trend_topNormal"] forState:UIControlStateNormal];
        [self.rightButton setBackgroundImage:[UIImage imageNamed:@"xp_trend_topSelected"] forState:UIControlStateNormal];
        [self bringSubviewToFront:self.rightButton];
    }
    
    self.index = sender.tag;
    if (self.itemBlock) {
        self.itemBlock(sender.tag);
    }
}

@end
