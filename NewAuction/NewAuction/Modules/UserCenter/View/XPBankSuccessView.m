//
//  XPBankSuccessView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/17.
//

#import "XPBankSuccessView.h"
#import "NSTimer+JKBlocks.h"

@interface XPBankSuccessView()

@property (nonatomic, strong) NSTimer *timer;
//计时器
@property(nonatomic,strong)NSTimer * oneMinTimer;

@property (nonatomic, strong) UILabel *timeLabel;
@end


@implementation XPBankSuccessView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    UIView *mainView = [UIView new];
    mainView.layer.cornerRadius = 8;
    mainView.backgroundColor = RandColor;
    mainView.frame = CGRectMake(12, kTopHeight + 12, SCREEN_WIDTH - 24, 120);
    [self addSubview:mainView];
    
    UIImageView *sucLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Group_67"]];
    sucLogo.frame = CGRectMake(self.center.x - 20, MaxY(mainView) + 126, 40, 40);
    [self addSubview:sucLogo];
    
    UILabel *desLabel = [UILabel new];
    desLabel.textColor = UIColorFromRGB(0x6E6BFF);
    desLabel.font = kFontRegular(16);
    desLabel.text = @"添加成功";
    desLabel.frame = CGRectMake(0, MaxY(sucLogo) + 14, self.mj_w, 24);
    desLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:desLabel];
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = kFontRegular(14);
    timeLabel.textColor = UIColorFromRGB(0x3A3C41);
    timeLabel.frame = CGRectMake(0, MaxY(desLabel) + 10, self.mj_w, 24);
    timeLabel.textAlignment = NSTextAlignmentCenter;
    timeLabel.text= @"定时返回啊";
    self.timeLabel = timeLabel;
    [self addSubview:timeLabel];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"现在退出" forState:UIControlStateNormal];
    [button setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(12, self.mj_h - 80, self.mj_w - 24, 50);
    [self addSubview:button];
    
    [self countDown];
}

#pragma mark -- 倒计时
-(void)countDown{
    
    XPWeakSelf
    __block int i = 4;
        //一分钟倒计时
    self.oneMinTimer = [NSTimer jk_timerWithTimeInterval:1 block:^{
        if (i >= 0) {
            
            NSString *str = [NSString stringWithFormat:@"在%d秒后自动返回",i];
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:str];
            [attr addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0x6E6BFF),NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(1, 1)];
            
            weakSelf.timeLabel.attributedText = attr;
//            weakSelf.timeLabel.text = str;
            
            i--;
            
        } else{
            [weakSelf.oneMinTimer invalidate];
            weakSelf.oneMinTimer = nil;
            if (self.successBlock) {
                self.successBlock();
            }
            
        }
    } repeats:YES];
    [[NSRunLoop mainRunLoop]addTimer:self.oneMinTimer forMode:NSRunLoopCommonModes];
    [self.oneMinTimer fire];
}

- (void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.successBlock) {
        self.successBlock();
    }
}

@end
