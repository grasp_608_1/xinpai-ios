//
//  XPTrendDetailCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/18.
//

#import "XPTrendDetailCell.h"

@interface XPTrendDetailCell ()

@property (nonatomic ,strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;
@property (nonatomic, strong) UILabel *label3;
@property (nonatomic, strong) UILabel *label4;
@property (nonatomic, strong) UILabel *label5;

@end

@implementation XPTrendDetailCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPTrendDetailCell";
    XPTrendDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPTrendDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViews];
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
    }
    return self;
}

-(void)setupViews{
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 24, 68);
    self.mainView = mainView;
    self.mainView.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:mainView];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.frame = CGRectMake(12, 24, 22, 22);
    [self.mainView addSubview:self.iconImageView];
    
    self.label1 = [UILabel new];
    self.label1.textColor = UIColorFromRGB(0x3A3C41);
    self.label1.font = kFontMedium(14);
    self.label1.frame = CGRectMake(48, 12, 240, 20);
    [self.mainView addSubview:self.label1];
    
    self.label2 = [UILabel new];
    self.label2.textColor = UIColorFromRGB(0xB8B8B8);
    self.label2.font = kFontRegular(14);
    self.label2.frame = CGRectMake(self.label1.mj_x, MaxY(self.label1) + 6, self.mainView.mj_w - MaxX(self.iconImageView) - 24, 20);
    [self.mainView addSubview:self.label2];
    
    
    self.label3 = [UILabel new];
    self.label3.textColor = UIColorFromRGB(0x3A3C41);
    self.label3.font = kFontMedium(14);
    self.label3.textAlignment = NSTextAlignmentRight;
    self.label3.frame = CGRectMake(self.mainView.mj_w - 120 - 12, self.label1.mj_y, 120, 20);
    [self.mainView addSubview:self.label3];
    
    self.label4 = [UILabel new];
    self.label4.textColor = UIColorFromRGB(0xB8B8B8);
    self.label4.font = kFontRegular(14);
    self.label4.textAlignment = NSTextAlignmentRight;
    self.label4.frame = CGRectMake(self.mainView.mj_w/2 - 12, self.label2.mj_y, self.mainView.mj_w/2., 20);
    [self.mainView addSubview:self.label4];
    
    
    self.label5 = [UILabel new];
    self.label5.textColor = UIColorFromRGB(0x3A3C41);
    self.label5.font = kFontMedium(14);
    self.label5.textAlignment = NSTextAlignmentRight;
    self.label5.frame = CGRectMake(self.mainView.mj_w - 120 - 12, 0, 120, self.mainView.mj_h);
    [self.mainView addSubview:self.label5];
    
    
    self.downLine = [[UIView alloc] init];
    self.downLine.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.downLine.frame = CGRectMake(12, self.mainView.frame.size.height - 1, self.mainView.frame.size.width - 12 *2, 1);
    [self.mainView addSubview:self.downLine];
    
}

- (void)cellWithModel:(XPTrendModel *)model image:(UIImage *)iconImage {
    ////1全部账单     2参拍明细       3委拍明细     4收益明细  5盈利明细
    ///6盈利兑换明细   7收益兑换明细 8货款转出明细
    ///9支付货款明细  10我的积分  11充值明细
    /// 12 提现明细
    
    if (self.cellType == 1 ) {
        self.label2.text = model.createTime;
        
        if (model.amount.floatValue > 0) {
            self.label3.text = [NSString stringWithFormat:@"+￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.amount]];
        }else if(model.amount.floatValue < 0){
            self.label3.text = [NSString stringWithFormat:@"-￥%@",[[XPMoneyFormatTool formatMoneyWithNum:model.amount] stringByReplacingOccurrencesOfString:@"-" withString:@""]];
        }else{//等于 0
            self.label3.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.amount]];
        }

        self.label4.text = [NSString stringWithFormat:@"手续费: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.serviceCharge]];
        self.label5.hidden = YES;
        self.label1.text = model.itemTypeName;
        
    }else if (self.cellType == 2) {
        self.label2.text = model.createTime;
        self.label3.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.countPricePar]];
        self.label4.text = [NSString stringWithFormat:@"数量: %@",model.amountParSuccess];
        self.label5.hidden = YES;
        self.label1.text = model.itemName;
    }else if (self.cellType == 3){
        self.label2.text = model.entTime;
        self.label3.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.valueEnt]];
        self.label4.text = [NSString stringWithFormat:@"数量: %@",model.amountEnt];
        self.label5.hidden = YES;
        self.label1.text = model.name;
    }else if(self.cellType == 4){
        //收益明细
        self.label1.text = model.referName;
        self.label2.text = model.createTime;
        self.label3.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.amount maximumFractionDigits:4]];
        self.label4.text = model.itemTypeName;
        self.label5.hidden = YES;
    }
    else if(self.cellType == 5){
        
        self.label1.text = model.itemTypeName;
        self.label2.text = model.createTime;
        self.label5.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.amount]];
        self.label3.hidden = YES;
        self.label4.hidden = YES;
        self.label5.hidden = NO;
        
    }else if(self.cellType == 6 || self.cellType == 7  || self.cellType == 8){
        self.iconImageView.hidden = YES;
        self.label1.frame = CGRectMake(12, 12, 240, 20);
        self.label2.frame = CGRectMake(12, MaxY(self.label1) + 6, self.mainView.mj_w/2. - 12, 20);
        self.label5.hidden = YES;
        
        self.label4.frame = CGRectMake(self.mainView.mj_w/2, self.label2.mj_y, self.mainView.mj_w/2. - 12, 20);
        
        if (self.cellType == 6) {
            self.label1.text = @"收入兑换";
            self.label2.text = model.createTime;
        }else if (self.cellType == 7){
            self.label2.text = model.createTime;
            self.label1.text = @"补贴兑换";
        }else if(self.cellType == 8){
            self.label1.text = @"保证金提现";
            self.label2.text = model.updateTime;
        }else {
            
        }
        self.label1.hidden = NO;
        
        self.label3.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.amount]];
        self.label4.text = [NSString stringWithFormat:@"手续费: %@",[XPMoneyFormatTool formatMoneyWithNum:model.serviceCharge]];
        
    }else if(self.cellType == 9){
        //支付货款明细
            self.label5.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.amount]];
            self.label2.text = model.createTime;
            self.label1.text = model.rechargeNo;
            self.label3.hidden = YES;
            self.label4.hidden = YES;
    }else if(self.cellType == 10){
        //我的积分不会出现在全部账单列表,不用考虑复用问题
        //支付货款明细
        if (model.changeType.intValue == 0) {
            self.label3.text = [NSString stringWithFormat:@"+%@",model.changeCount];
        }else if(model.changeType.intValue == 1){
            self.label3.text = [NSString stringWithFormat:@"%@",model.changeCount];
        }
            self.label2.text = model.createTime;
        self.label1.text = model.sourceTypeName;
            self.label4.hidden = YES;
        self.label1.frame = CGRectMake(12, 12, 240, 20);
        self.label2.frame = CGRectMake(self.label1.mj_x, MaxY(self.label1) + 6, self.mainView.mj_w - 24, 20);
        self.iconImageView.hidden = YES;
    }else if(self.cellType == 11){
        //收益明细
        self.label1.text = model.itemTypeName;
        self.label2.text = model.createTime;
        self.label3.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.amount]];
        self.label4.text = [NSString stringWithFormat:@"手续费: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.serviceCharge]];
        self.label5.hidden = YES;
    }else if(self.cellType == 12){
        //收益明细
        self.label1.text = model.itemTypeName;
        self.label2.text = model.createTime;
        self.label3.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.amount]];
        self.label4.text = [NSString stringWithFormat:@"手续费: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.serviceCharge]];
        self.label5.hidden = YES;
    }else if(self.cellType == 13){
        self.label5.hidden = YES;
        
        self.label1.text = model.withdrawTypeName;
        self.label2.text = model.createTime;
        self.label3.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.amount]];
        self.label4.text = [NSString stringWithFormat:@"手续费: %@",[XPMoneyFormatTool formatMoneyWithNum:model.serviceCharge]];
        
    }
    else
    {
        self.label5.text = [NSString stringWithFormat:@"%@",model.itemType];
        self.label2.text = model.createTime;
        self.label1.text = model.name;
        self.label3.hidden = YES;
        self.label4.hidden = YES;
    }
    
    self.iconImageView.image = iconImage;
    
}

-(NSString *)getScoreStrWithType:(NSNumber *)sourceType {
    
    int status = sourceType.intValue;
    NSString *str = @"";
    switch (status) {
        case 0:
            str = @"系统赠送";
            break;
        case 11:
            str = @"签到";
            break;
        case 21:
            str = @"订单下单花费";
            break;
        case 22:
            str = @"订单下单赠送";
            break;
        case 23:
            str = @"订单用户取消返还";
            break;
        case 24:
            str = @"订单售后完成返还";
            break;
            
        default:
            str = @"未知类型";
            break;
    }
    return str;
}





- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
