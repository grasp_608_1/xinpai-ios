//
//  XPPreferenceSetCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/7.
//

#import "XPPreferenceSetCell.h"

@interface XPPreferenceSetCell ()
@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *desLabel;
@end

@implementation XPPreferenceSetCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPPreferenceSetCell";
    XPPreferenceSetCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPPreferenceSetCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    CGFloat mainW = SCREEN_WIDTH - 24 *2;
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(24, 0,mainW,120);
    mainView.layer.cornerRadius = 8;
    mainView.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    mainView.clipsToBounds = YES;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    UIImageView *icon = [UIImageView new];
    icon.frame = CGRectMake(12, 12, 108,96);
    self.iconImageView = icon;
    [mainView addSubview:icon];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = kFontMedium(18);
    self.titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.titleLabel.frame = CGRectMake(132, 16, self.mainView.mj_w - 132 - 12, 24);
    [mainView addSubview:self.titleLabel];
    
    self.desLabel = [UILabel new];
    self.desLabel.font = kFontRegular(14);
    self.desLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.desLabel.numberOfLines = NO;
    self.desLabel.frame = CGRectMake(132, 48, self.mainView.mj_w - 132 - 12, 0);
    [mainView addSubview:self.desLabel];
}

-(void)setDataDic:(NSDictionary *)dataDic{
    
    self.iconImageView.image = [UIImage imageNamed:dataDic[@"image"]];
    self.titleLabel.text = dataDic[@"title"];
    NSString *desStr =  dataDic[@"description"];
    self.desLabel.text = desStr;
    
    CGFloat desH = [desStr boundingRectWithSize:CGSizeMake(self.desLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.desLabel.font} context:nil].size.height;
    
    self.desLabel.frame = CGRectMake(132, 48, self.mainView.mj_w - 132 - 12, desH);
    
    NSString *statusStr = dataDic[@"status"];
    if ([statusStr isEqualToString:@"1"]) {
        self.mainView.layer.borderWidth = 1;
        self.mainView.backgroundColor = UIColorFromRGB(0xF6F7FF);
    }else {
        self.mainView.layer.borderWidth = 0;
        self.mainView.backgroundColor = UIColor.whiteColor;
    }
    
}

@end
