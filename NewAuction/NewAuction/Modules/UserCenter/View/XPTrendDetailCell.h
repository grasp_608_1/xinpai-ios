//
//  XPTrendDetailCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/18.
//

#import <UIKit/UIKit.h>
#import "XPTrendModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPTrendDetailCell : UITableViewCell

@property (nonatomic, strong)UIView *mainView;

@property (nonatomic, strong) UIView *downLine;

//全部账单 1    参拍明细 2     委拍明细 3    收益明细 4 盈利明细 5
@property (nonatomic, assign) NSInteger cellType;

+ (instancetype)cellWithTableView:(UITableView *)tableView;
- (void)cellWithModel:(XPTrendModel *)model image:(UIImage *)iconImage;

@end

NS_ASSUME_NONNULL_END
