//
//  XPUserSettingCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/12.
//

#import "XPUserSettingCell.h"

@interface XPUserSettingCell()


@end


@implementation XPUserSettingCell


+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPUserSettingCell";
    XPUserSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPUserSettingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xFAFAFA);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    self.mainView = [UIView new];
    self.mainView.backgroundColor = UIColor.whiteColor;
    self.mainView.frame = CGRectMake(20, 0, SCREEN_WIDTH - 40, 50);
    [self addSubview:self.mainView];
    
    self.desLabel = [UILabel new];
    self.desLabel.frame = CGRectMake(20, 0, 120, self.mainView.bounds.size.height);
    self.desLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.desLabel.font = kFontRegular(16);
    [self.mainView addSubview:self.desLabel];
    
    self.rightArrow = [UIImageView new];
    self.rightArrow.frame = CGRectMake(self.mainView.frame.size.width - 20 - 24, 13, 24, 24);
    self.rightArrow.image = [UIImage imageNamed:@"xp_setting_arrow"];
    [self.mainView addSubview:self.rightArrow];
    
    self.extraLabel = [UILabel new];
    self.extraLabel.frame = CGRectMake(self.mainView.frame.size.width - 100 - 20 - 24 -5, 0, 100, self.mainView.frame.size.height);
    self.extraLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.extraLabel.font = kFontRegular(12);
    self.extraLabel.textAlignment = NSTextAlignmentRight;
    [self.mainView addSubview:self.extraLabel];
    
    self.downLine = [[UIView alloc] init];
    self.downLine.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.downLine.frame = CGRectMake(20, self.mainView.frame.size.height - 1, self.mainView.frame.size.width - 20 *2, 1);
    [self.mainView addSubview:self.downLine];
 
}



@end
