//
//  XPPayExtraMoneyView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/15.
//

#import "XPPayExtraMoneyView.h"
#import "NSTimer+JKBlocks.h"
#import "XPBaseViewController.h"
#import "XPHitButton.h"
#import "XPMyBankCardController.h"

@interface XPPayExtraMoneyView ()<UITextFieldDelegate>

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *extraMoneyLabel;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UITextField *codeTextField;

@property (nonatomic,strong)   NSTimer * oneMinTimer;
@property (nonatomic, copy)   NSString *codeUUid;

@property (nonatomic, strong) XPBankModel *bankModel;
@property (nonatomic, strong) UIButton *bankButton;

@end

static NSTimeInterval xpShareDuring = 0.4;

static CGFloat mainHeight = 250;

@implementation XPPayExtraMoneyView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(freshBankCard:) name:Notification_user_choose_bankcard object:nil];
        
    }
    return self;
}
- (void)setupView {
    
    UIView *bgView = [[UIView alloc] initWithFrame:self.bounds];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
//    [bgView addGestureRecognizer:tap];
    self.bgView = bgView;
    [self addSubview:bgView];
    
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.layer.masksToBounds = YES;
    mainView.frame = CGRectMake(0,self.mj_h, self.mj_w , mainHeight + kBottomHeight);
    [XPCommonTool cornerRadius:mainView andType:1 radius:20];
    self.mainView = mainView;
    [self addSubview:mainView];

    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(16);
    titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel.text = @"汇付快捷支付";
    titleLabel.frame = CGRectMake(37, 18, self.mainView.mj_w - 37*2 , 24);
//    [self.mainView addSubview:titleLabel];
    titleLabel.hidden = YES;
    
    XPHitButton *deleteButton = [[XPHitButton alloc] init];
    deleteButton.hitRect = 40;
    [deleteButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    deleteButton.frame = CGRectMake(self.mainView.mj_w - 24 - 12,12, 24, 24);
    [deleteButton addTarget:self action:@selector(tap) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:deleteButton];

    UIView *sepView = [UIView new];
    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    sepView.frame = CGRectMake(0, 60, self.mainView.mj_w, 1);
    [self.mainView addSubview:sepView];
    
    UIImageView *chooseImageView = [[UIImageView alloc] initWithFrame:CGRectMake(37, MaxY(sepView) + 18, 12, 12)];
    chooseImageView.image = [UIImage imageNamed:@"Group_67"];
    [self.mainView addSubview:chooseImageView];
    
    UIButton *bankButton = [UIButton new];
    bankButton.frame = CGRectMake(37 + 12 + 6, MaxY(sepView), self.mainView.mj_w - (37 + 12 + 6), 47);
//    [bankButton addTarget:self action:@selector(bankButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bankButton setTitle:@"杉德支付" forState:UIControlStateNormal];
    bankButton.titleLabel.font = kFontRegular(14);
    [bankButton setTitleColor:app_font_color forState:UIControlStateNormal];
    bankButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.bankButton = bankButton;
    [self.mainView addSubview:bankButton];
    
    UIImageView *arrowImageView = [[UIImageView alloc] init];
    arrowImageView.image = [UIImage imageNamed:@"xp_setting_arrow"];
    arrowImageView.frame = CGRectMake(bankButton.mj_w - 24 -24, 12, 24, 24);
//    [bankButton addSubview:arrowImageView];
    
    UIView *sepView2 = [UIView new];
    sepView2.backgroundColor = UIColorFromRGB(0xF2F2F2);
    sepView2.frame = CGRectMake(0, 108, self.mainView.mj_w, 1);
    [self.mainView addSubview:sepView2];
    
    NSArray *titleArray = @[@"补差价"];
//    NSArray *titleArray = @[@"补差价",@"手机号",@"验证码"];
    for (int i = 0 ; i<titleArray.count; i++) {
        UILabel *tempLabel = [UILabel new];
        tempLabel.font = kFontRegular(14);
        tempLabel.textColor = UIColorFromRGB(0x8A8A8A);
        tempLabel.text = titleArray[i];
        [self.mainView addSubview:tempLabel];
        tempLabel.frame = CGRectMake(36, 148 + 48*i, 42, 24);
//        tempLabel.frame = CGRectMake(36, 110 + 48*i, 42, 24);
    }
    
    CGFloat leftMargin = 102;
    
    UILabel *extraMoneyLabel = [UILabel new];
    extraMoneyLabel.textColor = app_font_color;
    extraMoneyLabel.font = kFontRegular(32);
    extraMoneyLabel.frame = CGRectMake(leftMargin, 132, self.mj_w - leftMargin, 40);
    extraMoneyLabel.frame = CGRectMake(leftMargin, 132, self.mj_w - leftMargin, 40);
    self.extraMoneyLabel = extraMoneyLabel;
    [self.mainView addSubview:extraMoneyLabel];
    
    UILabel *phoneLabel = [UILabel new];
    phoneLabel.textColor = app_font_color;
    phoneLabel.font = kFontRegular(14);
    phoneLabel.frame = CGRectMake(leftMargin, 148 + 48, self.mj_w - leftMargin, 24);
    phoneLabel.text = @"选择银行卡后自动填充";
    self.phoneLabel = phoneLabel;
//    [self.mainView addSubview:phoneLabel];
    
    UITextField *codeTextField = [[UITextField alloc] initWithFrame:CGRectMake(leftMargin, 184 + 48, self.mainView.mj_w - 102 - 118, 40)];
    codeTextField.placeholder = @"请输入验证码";
    codeTextField.font = kFontMedium(14);
    codeTextField.textColor = UIColorFromRGB(0x3A3C41);
    codeTextField.layer.cornerRadius = 4;
    codeTextField.backgroundColor = UIColorFromRGB(0xF2F2F2);
    codeTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12, 40)];
    codeTextField.leftViewMode = UITextFieldViewModeAlways;
    codeTextField.delegate = self;
    self.codeTextField = codeTextField;
    codeTextField.keyboardType = UIKeyboardTypeNumberPad;
    if (@available(iOS 12.0, *)) {
        codeTextField.textContentType = UITextContentTypeOneTimeCode;
    }
//    [self.mainView addSubview:codeTextField];
    
    //验证码
    self.codeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.codeButton.frame = CGRectMake(self.mainView.mj_w - 106, 184 + 48,70,40);
    [self.codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.codeButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    self.codeButton.titleLabel.font = kFontRegular(14);
    [self.codeButton addTarget:self action:@selector(codeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [self.mainView addSubview:self.codeButton];
    
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(24, 200, self.mainView.mj_w - 48, 40);
    [cancelButton setTitle:@"确定" forState:UIControlStateNormal];
    cancelButton.layer.cornerRadius = 20;
    cancelButton.layer.masksToBounds = YES;
    cancelButton.titleLabel.font = kFontMedium(16);
    [cancelButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:cancelButton];
    
    [cancelButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :cancelButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
}

- (void)showWithMoney:(NSString *)money{
    
    NSString *moneyStr = money;
    NSString *priceStr = [NSString stringWithFormat:@"￥%@",moneyStr?:@(0)];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(24)} range:NSMakeRange(0, 1)];
    self.extraMoneyLabel.attributedText = attr;
//    [self.codeTextField becomeFirstResponder];
    [UIView animateWithDuration:xpShareDuring animations:^{
        self.mainView.frame = CGRectMake(0,self.mj_h - mainHeight - kBottomHeight, self.mj_w , mainHeight + kBottomHeight);
    }];
}

//发送验证码
-(void)codeButtonClicked:(UIButton *)sender {
    [self.mainView endEditing:YES];
    
    if ([self.bankButton.titleLabel.text containsString:@"选择"]) {
        [[XPShowTipsTool shareInstance]showMsg:@"请先选择银行卡" ToView:k_keyWindow];
        
    }else{
        if (self.codeBlock) {
            self.codeBlock(self.bankModel);
        }
    }
}

- (void)freshBankCard:(NSNotification *)noti {
    
    XPBankModel *model = (XPBankModel *)[noti object];
    if (self.bankModel.productId && [model.productId isEqualToNumber:self.bankModel.productId]) {
        [self.bankButton setTitle:@"选择银行卡" forState:UIControlStateNormal];
        self.phoneLabel.text = @"选择银行卡后自动填充";
        self.bankModel = [XPBankModel new];
    }
}


//正则
-(BOOL)validVerifyPredicate:(NSString *) pString TargetString:(NSString *)targerString
{
   NSPredicate *num = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pString];
   return [num evaluateWithObject:targerString];
}

#pragma mark -- 倒计时
-(void)countDown{
    
    XPWeakSelf
    __block int i = 60;
        //一分钟倒计时
    self.oneMinTimer = [NSTimer jk_timerWithTimeInterval:1 block:^{
        if (i >= 0) {
            [weakSelf.codeButton setTitle:[NSString stringWithFormat:@" %d秒 ",i] forState:0];
            i--;
            weakSelf.codeButton.enabled = NO;
            weakSelf.codeButton.alpha = 0.5;
        } else{
            [weakSelf.oneMinTimer invalidate];
            weakSelf.oneMinTimer = nil;
            [ weakSelf.codeButton setTitle:@"获取验证码" forState:0];
            weakSelf.codeButton.enabled = YES;
            weakSelf.codeButton.alpha = 1.0;
        }
    } repeats:YES];
    [[NSRunLoop mainRunLoop]addTimer:self.oneMinTimer forMode:NSRunLoopCommonModes];
    [self.oneMinTimer fire];
}

-(void)bankButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPMyBankCardController *vc = [XPMyBankCardController new];
    vc.enterType = 0;
    XPWeakSelf;
    vc.chooseBankBlock = ^(XPBankModel * _Nonnull bankModel) {
        weakSelf.bankModel = bankModel;
        [weakSelf updateUI];
    };
    [[XPCommonTool currentNavigationController] pushViewController:vc animated:YES];
    [self.codeTextField resignFirstResponder];
}

- (void)updateUI{
    NSString *titleStr = self.bankModel.bankName.length > 0 ? [NSString stringWithFormat:@"%@(%@)",self.bankModel.bankName,[self.bankModel.cardNo stringByReplacingCharactersInRange:NSMakeRange(0, self.bankModel.cardNo.length - 4) withString:@""]] : @"选择银行";
    [self.bankButton setTitle:titleStr forState:UIControlStateNormal];
    NSString *secretPhone = self.bankModel.bankPhone.length > 8 ? [self.bankModel.bankPhone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"]:self.bankModel.bankPhone;
    self.phoneLabel.text = secretPhone;
}

- (void)buttonClicked:(UIButton *)sender{
    buttonCanUseAfterOneSec(sender);
    
    [self dismiss:YES];
}

- (void)tap {
    buttonCanUseAfterOneSec(self.bgView);
    [self dismiss:YES];
}
//确认按钮点击
- (void)cancelButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.sureBlock) {
        self.sureBlock(@"");
    }
    
    [self dismiss:YES];
    
   /*
    NSString *codeStr = [[XPVerifyTool shareInstance] verifyCode:self.codeTextField.text];
    
    if ([self.bankButton.titleLabel.text containsString:@"选择"]) {
        [[XPShowTipsTool shareInstance]showMsg:@"请先选择银行卡" ToView:k_keyWindow];
        
    }else if (codeStr) {
        [[XPShowTipsTool shareInstance] showMsg:codeStr ToView:k_keyWindow];
    }else {
        if (self.sureBlock) {
            self.sureBlock(self.codeTextField.text);
        }
        
        [self dismiss:YES];
    }
    */
}
-(void)codeTextBecomeFirstResponder {
    [self.codeTextField becomeFirstResponder];
}

- (void)dismiss:(BOOL)animation {
    if (animation) {
        [UIView animateWithDuration:xpShareDuring animations:^{
            self.mainView.frame = CGRectMake(0,self.mj_h, self.mj_w , mainHeight + kBottomHeight);
            self.bgView.alpha = 0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }else {
        [self removeFromSuperview];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.codeTextField) {
        // 获取当前文本框中的文本，包括用户输入的新字符
        NSString *currentText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        // 如果当前文本长度超过6位，则不允许输入更多字符
        if (currentText.length > 6) {
            return NO;
        }
    }
    // 允许输入
    return YES;
}

@end
