//
//  XPNewsCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import "XPNewsCell.h"

@interface XPNewsCell ()

@property (nonatomic , strong) UILabel *titleLabel;
@property (nonatomic , strong) UILabel *timeLabel;
@property (nonatomic , strong) UIImageView *iconImageView;

@end

@implementation XPNewsCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPNewsCell";
    XPNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPNewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    CGFloat mainW = SCREEN_WIDTH - 24;
    CGFloat imagW = SCREEN_WIDTH - 48;
    CGFloat imageH = imagW * 0.5;
    
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 12, mainW, imageH + 74 + 12);
    mainView.layer.cornerRadius = 8;
    mainView.clipsToBounds = YES;
//    mainView.backgroundColor = UIColor.yellowColor;
    [self addSubview:mainView];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = kFontMedium(16);
    self.titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.titleLabel.frame = CGRectMake(12, 12, mainView.mj_w - 24, 24);
    [mainView addSubview:self.titleLabel];
    
    self.timeLabel = [UILabel new];
    self.timeLabel.font = kFontBold(12);
    self.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.timeLabel.frame = CGRectMake(12, MaxY(self.titleLabel) + 6, self.titleLabel.mj_w, 24);
    [mainView addSubview:self.timeLabel];

    UIImageView *icon = [UIImageView new];
    icon.frame = CGRectMake(12, 74, imagW,imageH);
    self.iconImageView = icon;
    [mainView addSubview:icon];
    
    
}

-(CGFloat)cellWithModel:(XPNewsModel *)model {
    
    self.titleLabel.text = model.title;
    self.timeLabel.text = [NSString stringWithFormat:@"发布日期: %@",model.newsTime];
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"xp_update_banner"]];
    
    return 0;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
