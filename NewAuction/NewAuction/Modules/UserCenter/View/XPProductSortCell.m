//
//  XPProductSortCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/11.
//

#import "XPProductSortCell.h"
#import "XPWeakTimer.h"

@interface XPProductSortCell ()

@property (nonatomic, strong) UIView *mainView;
//产品图片
@property (nonatomic, strong) UIImageView *pImageView;
//产品名称
@property (nonatomic, strong) UILabel *pTitleLabel;
//价格
@property (nonatomic, strong) UILabel *label1;
//倒计时标题
@property (nonatomic, strong) UILabel *label2;
//倒计时具体时间
@property (nonatomic, strong) UILabel *label3;
//左侧按钮
@property (nonatomic, strong) UIButton *leftButton;
//右侧按钮
@property (nonatomic, strong) UIButton *rightButton;

@property (nonatomic, strong)  XPWeakTimer *timer;

@property (nonatomic, assign) NSInteger currentSconds;

@end


@implementation XPProductSortCell

/// cell 初始化方法
+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPProductSortCell";
    XPProductSortCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPProductSortCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xFAFAFA);
        [self setupViews];
    }
    return self;
}

-(void)setupViews {
    self.mainView = [UIView new];
    self.mainView.layer.cornerRadius = 4;
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(12, 10, SCREEN_WIDTH - 12 * 2, 140);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:self.mainView];

    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(0, 0, 140, 140);
    [self.mainView addSubview:_pImageView];
    
    CGFloat LabelX = MaxX(self.pImageView) + 10;
    CGFloat LabelW = self.mainView.frame.size.width - LabelX  - 10;
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontBold(16);
    self.pTitleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.pTitleLabel.frame = CGRectMake(LabelX, 15, LabelW, 24);
    [self.mainView addSubview:_pTitleLabel];
    
    self.label1 = [UILabel new];
    [self.mainView addSubview:self.label1];
    self.label2 = [UILabel new];
    [self.mainView addSubview:self.label2];
    self.label3 = [UILabel new];
    [self.mainView addSubview:self.label3];
   
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.frame = CGRectMake(self.mainView.bounds.size.width - 10 - 70, self.mainView.frame.size.height - 10 - 18, 70, 18);
    [self.mainView addSubview:self.rightButton];
    UIImageView *starImageView2 = [[UIImageView alloc] init];
    starImageView2.image = [UIImage imageNamed:@"xp_product_uncollect"];
    starImageView2.frame = CGRectMake(0, 2, 14, 14);
    [self.rightButton addSubview:starImageView2];
    UILabel *starLabel2 = [UILabel new];
    starLabel2.text = @"取消收藏";
    starLabel2.font = kFontRegular(12);
    starLabel2.textColor = UIColorFromRGB(0x8A8A8A);
    starLabel2.frame = CGRectMake(MaxX(starImageView2) + 5, 0, self.rightButton.frame.size.width -5 - 14, 18);
    [self.rightButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.rightButton addSubview:starLabel2];
    

    
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.frame = CGRectMake(MaxX(self.pImageView) + 10, self.mainView.frame.size.height - 15 - 18, 70, 18);
    self.leftButton.titleLabel.font = kFontRegular(12);
    [self.mainView addSubview:self.leftButton];
    
    UIImageView *starImageView = [[UIImageView alloc] init];
    starImageView.image = [UIImage imageNamed:@"xp_product_collected"];
    starImageView.frame = CGRectMake(0, 2, 14, 14);
    [self.leftButton addSubview:starImageView];
    UILabel *starLabel = [UILabel new];
    starLabel.text = @"已收藏";
    starLabel.font = kFontRegular(12);
    starLabel.textColor = UIColorFromRGB(0x6E6BFF);
    starLabel.frame = CGRectMake(MaxX(starImageView) + 5, 0, self.leftButton.frame.size.width -5 - 14, 18);
    [self.leftButton addSubview:starLabel];
    
    _timer = [XPWeakTimer shareTimer];
}
- (UILabel *)getRoutLabel {
    UILabel *routLabel = [UILabel new];
    [self.mainView addSubview:routLabel];
    return routLabel;
}


- (void)cellWithModel:(XPProductModel *)model passedTime:(NSInteger)passedTime{
    
    self.pTitleLabel.text = model.name;
    if (!model.productPicSubject || model.productPicSubject.length == 0) {
        [self.pImageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    }else {
        [self.pImageView sd_setImageWithURL:[NSURL URLWithString:model.productPicSubject] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    }
   
    //;
    [self.timer stopTimer];
    CGFloat LabelX = MaxX(self.pImageView) + 10;
    CGFloat LabelW = self.mainView.frame.size.width - LabelX  - 10;
    //分类 1 收藏 2
    
    if (_cellType == 1) {
        
        self.label1.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel) + 10, LabelW, 28);
        self.label1.font = kFontMedium(18);
        self.label1.textColor = UIColorFromRGB(0xFF4A4A);
        
        self.label2.frame = CGRectMake(LabelX, MaxY(self.label1) + 24, 72, 24);
        self.label2.font = kFontRegular(12);
        self.label2.textColor = UIColorFromRGB(0x8A8A8A);
        //倒计时
        self.label3.frame = CGRectMake(MaxX(self.label2), self.label2.frame.origin.y, MaxX(self.label2) - 10, self.label2.frame.size.height);
        self.label3.font = kFontRegular(14);
        self.label3.textColor = UIColorFromRGB(0x6E6BFF);
        
        self.label1.hidden = NO;
        self.label2.hidden = NO;
        self.label3.hidden = NO;
        self.leftButton.hidden = YES;
        self.rightButton.hidden = YES;
        
        NSString *priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceWindupLatest]];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(12)} range:NSMakeRange(0, 1)];
        
        self.label1.attributedText = attr;
        self.label2.text = @"开拍倒计时";
        
        if (model.isAuction.intValue == 1) {
            self.label3.text = @"";
            self.label2.text = @"开拍进行中";
            self.label2.font = kFontMedium(14);
            self.label2.textColor = UIColorFromRGB(0x6E6BFF);
            [self.timer stopTimer];
        }else if(model.isAuction.intValue == 2) {
            self.label3.text = @"";
            self.label2.text = @"敬请期待";
            self.label2.font = kFontMedium(14);
            self.label2.textColor = UIColorFromRGB(0x6E6BFF);
            [self.timer stopTimer];
        }
        else {
            self.label2.font = kFontRegular(12);
            self.label2.textColor = UIColorFromRGB(0x8A8A8A);
            self.label2.text = @"开拍倒计时:";
            
            //计算时间差
            _currentSconds = (model.startTimeLong.integerValue - model.currentTimeLong.integerValue)/1000. - passedTime;
            
            NSString *hour = [self getHour];
            NSString *min = [self getMM];
            NSString *second = [self getSS];
            if (_currentSconds < 0) {
                self.label3.text = @"00:00:00";
            }else {
                self.label3.text = [NSString stringWithFormat:@"%@:%@:%@",hour,min,second];
                
                [_timer startTimerWithTime:1 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
            }
        }
        
//        self.label3.text = @"11:11:11";

    }else if (_cellType == 2) {
        self.label1.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel) + 10, LabelW, 28);
        self.label1.font = kFontMedium(18);
        self.label1.textColor = UIColorFromRGB(0xFF4A4A);
        
        self.label1.hidden = NO;
        self.label2.hidden = YES;
        self.label3.hidden = YES;
        self.leftButton.hidden = NO;
        self.rightButton.hidden = NO;
        
        self.label1.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceWindupLatest]];
    }
    
}

- (void)cancelButtonClicked:(UIButton *)sender {
//    buttonCanUseAfterOneSec(sender);
    
    if (self.tapBlock) {
        self.tapBlock();
    }
}



-(void)refreshTimer{//活动倒计时刷新
    
    if (self.currentSconds <= 0) {//刷新首页
        [self.timer stopTimer];
        [[NSNotificationCenter defaultCenter] postNotificationName:Notification_homepage_fresh object:nil userInfo:@{@"status":@"0"}];
    }else{
        self.currentSconds --;
        NSString *hour = [self getHour];
        NSString *min = [self getMM];
        NSString *second = [self getSS];

        self.label3.text = [NSString stringWithFormat:@"%@:%@:%@",hour,min,second];
                
    }
    
}

- (NSString *)getHour{
    return [NSString stringWithFormat:@"%02ld",self.currentSconds/3600];
}
- (NSString *)getMM{
    return [NSString stringWithFormat:@"%02ld",(self.currentSconds%3600)/60];
}
- (NSString *)getSS{
    return [NSString stringWithFormat:@"%02ld",self.currentSconds%60];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
