//
//  XPPayTypeListView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/31.
//

#import <UIKit/UIKit.h>
#import "XPHitButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPPayTypeListView : UIView
@property (nonatomic, strong) UIView *sepView;
//选择按钮
@property (nonatomic, strong) XPHitButton *selectedButton;

@property (nonatomic, copy) void(^tipBlock)(void);

@property (nonatomic, copy) void(^selectedBlock)(NSInteger tag);

-(void)viewWithDict:(NSDictionary *)dic showTips:(BOOL)isShow showDesc:(BOOL)showDesc;

@end

NS_ASSUME_NONNULL_END
