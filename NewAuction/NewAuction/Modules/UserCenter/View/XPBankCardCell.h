//
//  XPBankCardView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/17.
//

#import <UIKit/UIKit.h>
#import "XPBankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPBankCardCell : UITableViewCell

@property (nonatomic, assign) NSInteger cellType;
@property (nonatomic, copy) void(^rightBlock)(void);

+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (void)cellWithModel:(XPBankModel *)model;
@end

NS_ASSUME_NONNULL_END
