//
//  XPProductCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/11.
//

#import "XPProductCell.h"
#import "UIButton+parameter.h"

@interface XPProductCell ()

@property (nonatomic, strong) UIView *mainView;
//产品图片
@property (nonatomic, strong) UIImageView *pImageView;
//产品名称
@property (nonatomic, strong) UILabel *pTitleLabel;

//以下四个用于适配不同cell类型
@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;
@property (nonatomic, strong) UILabel *label3;
@property (nonatomic, strong) UILabel *label4;
//成功参拍数量
@property (nonatomic, strong) UILabel *bottomLabel;
//左侧按钮
@property (nonatomic, strong) UIButton *leftButton;
//右侧按钮
@property (nonatomic, strong) UIButton *rightButton;

@end


@implementation XPProductCell

/// cell 初始化方法
+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPProductCell";
    XPProductCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xFAFAFA);
        [self setupViews];
    }
    return self;
}

-(void)setupViews {
    self.mainView = [UIView new];
    self.mainView.layer.cornerRadius = 8;
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(12, 10, SCREEN_WIDTH - 12 * 2, 180);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:self.mainView];

    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(12, 12, 120, 120);
    self.pImageView.layer.cornerRadius = 4;
    self.pImageView.layer.masksToBounds = YES;
    [self.mainView addSubview:_pImageView];
    
    CGFloat LabelX = MaxX(self.pImageView) + 12;
    CGFloat LabelW = self.mainView.frame.size.width - LabelX  - 12;
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontMedium(16);
    self.pTitleLabel.textColor = app_font_color;
    self.pTitleLabel.frame = CGRectMake(LabelX, 16, LabelW, 24);
    [self.mainView addSubview:_pTitleLabel];
    
    self.label1 = [UILabel new];
    [self.mainView addSubview:self.label1];
    self.label2 = [UILabel new];
    [self.mainView addSubview:self.label2];
    self.label3 = [UILabel new];
    [self.mainView addSubview:self.label3];
    self.label4 = [UILabel new];
    [self.mainView addSubview:self.label4];
    
    self.bottomLabel = [UILabel new];
    self.bottomLabel.font = kFontRegular(14);
    self.bottomLabel.textColor = app_font_color;
    self.bottomLabel.frame = CGRectMake(self.pImageView.mj_x, MaxY(self.pImageView) + 12, self.mainView.mj_w - 24, 24);
    [self.mainView addSubview:self.bottomLabel];
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.frame = CGRectMake(self.mainView.bounds.size.width - 12 - 80, self.mainView.frame.size.height - 12 - 28, 80, 28);
    self.rightButton.titleLabel.font = kFontRegular(14);
    [self.rightButton addTarget:self action:@selector(rightButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.rightButton];
    
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.frame = CGRectMake(self.mainView.bounds.size.width - 12 - 80 - 80 - 8, self.mainView.frame.size.height - 12 - 28, 80, 28);
    self.leftButton.titleLabel.font = kFontRegular(14);
    [self.leftButton addTarget:self action:@selector(leftButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.leftButton];
    
    
    
    
}
- (UILabel *)getRoutLabel {
    UILabel *routLabel = [UILabel new];
    [self.mainView addSubview:routLabel];
    return routLabel;
}


- (void)cellWithModel:(XPAuctionModel *)model {
    
    self.pTitleLabel.text = model.name;
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:model.image]  placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    CGFloat LabelX = MaxX(self.pImageView) + 12;
    CGFloat LabelW = self.mainView.frame.size.width - LabelX  - 12;
    //1 委托参拍商品  2 委托委拍商品  3 仓库参拍 4 仓库待提货 5 仓库已提货
    if (_cellType == 1) {
        
        self.label1.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel) + 4, LabelW, 14);
        self.label1.font = kFontRegular(11);
        self.label1.textColor = UIColorFromRGB(0x3A3C41);
        
        self.label2.frame = CGRectMake(LabelX, MaxY(self.label1) + 2, LabelW, 14);
        self.label2.font = kFontRegular(11);
        self.label2.textColor = UIColorFromRGB(0x3A3C41);
        
        self.label3.frame = CGRectMake(LabelX, MaxY(self.label2) + 4, LabelW, 14);
        self.label3.font = kFontRegular(11);
        self.label3.textColor = UIColorFromRGB(0x3A3C41);
        
        self.label1.hidden = NO;
        self.label2.hidden = NO;
        self.label3.hidden = NO;
        self.label4.hidden = YES;
        self.leftButton.hidden = YES;
        self.rightButton.hidden = NO;
        self.bottomLabel.hidden = YES;
        
        self.label1.text = [NSString stringWithFormat:@"拍品个数: %@",model.amountPar];
        self.label2.text = [NSString stringWithFormat:@"拍品价值: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.lotPar]];
        
        NSString *picePar = [NSString stringWithFormat:@"%@",model.pricePar];
        NSString *str = [NSString stringWithFormat:@"拍品价格：￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.pricePar]];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:str];
        [attr addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(str.length - picePar.length - 1, picePar.length + 1)];
        self.label3.attributedText = attr;
        
        [self.rightButton setTitle:@"取消参拍" forState:UIControlStateNormal];
        self.rightButton.layer.cornerRadius = 14;
        self.rightButton.layer.borderWidth = 1;
        self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
        [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
        
        if (model.entStatus.intValue == 0) {
            //状态判断
            self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
            [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
            self.rightButton.userInteractionEnabled = YES;
        }else {
            self.rightButton.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
            [self.rightButton setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
            self.rightButton.userInteractionEnabled = NO;
        }
        
        self.rightButton.attachString = [NSString stringWithFormat:@"%@",model.autionId];

    }else if (_cellType == 2) {
        self.label1.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel) + 4, LabelW, 14);
        self.label1.font = kFontRegular(11);
        self.label1.textColor = UIColorFromRGB(0x3A3C41);
        
        self.label2.frame = CGRectMake(LabelX, MaxY(self.label1) + 2, LabelW, 14);
        self.label2.font = kFontRegular(11);
        self.label2.textColor = UIColorFromRGB(0x3A3C41);
        
        self.label3.frame = CGRectMake(LabelX, MaxY(self.label2) + 2, LabelW, 14);
        self.label3.font = kFontRegular(11);
        self.label3.textColor = UIColorFromRGB(0x3A3C41);
        
        self.label1.hidden = NO;
        self.label2.hidden = NO;
        self.label3.hidden = NO;
        self.label4.hidden = YES;
        self.leftButton.hidden = YES;
        self.rightButton.hidden = NO;
        self.bottomLabel.hidden = YES;
        
        self.label1.text = [NSString stringWithFormat:@"拍品个数: %@",model.amountEnt];
        self.label2.text = [NSString stringWithFormat:@"拍品价值: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.valueEnt]];
        self.label3.text = [NSString stringWithFormat:@"拍品价格：￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceEnt]];
        
        [self.rightButton setTitle:@"取消委拍" forState:UIControlStateNormal];
        self.rightButton.layer.cornerRadius = 14;
        self.rightButton.layer.borderWidth = 1;
        if (model.entStatus.intValue == 0) {
            //状态判断
            self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
            [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
            self.rightButton.userInteractionEnabled = YES;
        }else {
            self.rightButton.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
            [self.rightButton setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
            self.rightButton.userInteractionEnabled = NO;
        }
        
        
    }else if (_cellType == 3){
        
        self.label1.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel) + 2, LabelW, 18);
        self.label1.font = kFontMedium(12);
        self.label1.textColor = UIColorFromRGB(0x3A3C41);
        
        self.label2.frame = CGRectMake(LabelX, MaxY(self.label1) + 2, LabelW, 18);
        self.label2.font = kFontRegular(12);
        
        self.label3.frame = CGRectMake(LabelX, MaxY(self.label2) + 2, LabelW, 18);
        self.label3.font = kFontRegular(12);
        self.label3.textColor = UIColorFromRGB(0x8A8A8A);
        
        self.label4.frame = CGRectMake(LabelX, MaxY(self.label3) + 2, LabelW, 18);
        self.label4.font = kFontRegular(12);
        self.label4.textColor = UIColorFromRGB(0x8A8A8A);
        
        self.label1.hidden = NO;
        self.label2.hidden = NO;
        self.label3.hidden = NO;
        self.label4.hidden = NO;
        self.leftButton.hidden = NO;
        self.rightButton.hidden = NO;
        self.bottomLabel.hidden = NO;

        if (model.auctionStatus.intValue == 1) {//参拍成功
            self.label2.textColor = app_font_color_8A;
            self.label1.text = [NSString stringWithFormat:@"参拍时间: %@",model.createTime];
            self.label2.text = [NSString stringWithFormat:@"成功参拍价值: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.lotParSuccess]];
            self.label3.text = [NSString stringWithFormat:@"可委拍数量: %@",model.amountLast];
            self.label4.text = [NSString stringWithFormat:@"可委拍价值：￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.amountLastPar]];
            
            self.bottomLabel.text = model.auctionStatusMapping;
        }else {//参拍中
            self.label2.textColor = app_font_color;
            self.label1.text = [NSString stringWithFormat:@"参拍时间: %@",model.createTime];
            self.label2.text = [NSString stringWithFormat:@"参拍数量: %@",model.amountPar];
            self.label3.text = [NSString stringWithFormat:@"拍品价格: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.pricePar]];
            self.label4.text = [NSString stringWithFormat:@"参拍预估价值：￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.lotPar]];
            self.bottomLabel.text = model.auctionStatusMapping;
        }
        
        [self.rightButton setTitle:@"利润结算" forState:UIControlStateNormal];
        self.rightButton.layer.cornerRadius = 14;
        self.rightButton.layer.borderWidth = 1;
        self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
        [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
        if (model.commissionedGoodsButtonStatus.intValue != 0) {
            self.rightButton.layer.cornerRadius = 14;
            self.rightButton.layer.borderWidth = 1;
            self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
            [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
            self.rightButton.userInteractionEnabled = YES;
        }else {
            self.rightButton.layer.cornerRadius = 14;
            self.rightButton.layer.borderWidth = 1;
            self.rightButton.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
            [self.rightButton setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
            self.rightButton.userInteractionEnabled = NO;
        }
        
        
        [self.leftButton setTitle:@"委拍商品" forState:UIControlStateNormal];
        if (model.remainingCommissionableQuantity.intValue > 0) {
            self.leftButton.layer.cornerRadius = 14;
            self.leftButton.layer.borderWidth = 1;
            self.leftButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
            [self.leftButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
            self.leftButton.userInteractionEnabled = YES;
        }else {
            self.leftButton.layer.cornerRadius = 14;
            self.leftButton.layer.borderWidth = 1;
            self.leftButton.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
            [self.leftButton setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
            self.leftButton.userInteractionEnabled = NO;
        }
        
        
    }else if (_cellType == 4) {
        
        self.label1.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel) + 4, LabelW, 18);
        self.label1.font = kFontRegular(12);
        self.label1.textColor = UIColorFromRGB(0x3A3C41);
        
        self.label2.frame = CGRectMake(LabelX, MaxY(self.label1) + 4, LabelW, 18);
        self.label2.font = kFontRegular(12);
        self.label2.textColor = UIColorFromRGB(0x3A3C41);
        
        self.label3.frame = CGRectMake(LabelX, MaxY(self.label2) + 4, LabelW, 18);
        self.label3.font = kFontRegular(12);
        self.label3.textColor = app_font_color_8A;
        
        self.label1.hidden = NO;
        self.label2.hidden = NO;
        self.label3.hidden = NO;
        self.label4.hidden = YES;
        self.leftButton.hidden = YES;
        self.rightButton.hidden = NO;
        self.bottomLabel.hidden = YES;
        
        self.label1.text = [NSString stringWithFormat:@"提货价: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.pricePick]];
        self.label2.text = [NSString stringWithFormat:@"总货款: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.profitProductTotal]];
        self.label3.text = [NSString stringWithFormat:@"结算时间: %@",model.createTime];

        self.rightButton.userInteractionEnabled = YES;
        [self.rightButton setTitle:@"提货" forState:UIControlStateNormal];
        self.rightButton.layer.cornerRadius = 14;
        self.rightButton.layer.borderWidth = 1;
        self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
        [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    }else if (_cellType == 5) {
        
        self.label1.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel) + 4, LabelW, 18);
        self.label1.font = kFontRegular(12);
        self.label1.textColor = UIColorFromRGB(0x3A3C41);
        
        self.label2.frame = CGRectMake(LabelX, MaxY(self.label1) + 4, LabelW, 18);
        self.label2.font = kFontRegular(12);
        self.label2.textColor = UIColorFromRGB(0x3A3C41);
        
        self.label3.frame = CGRectMake(LabelX, MaxY(self.label2) + 4, LabelW, 18);
        self.label3.font = kFontRegular(12);
        self.label3.textColor = app_font_color_8A;
        
        self.label1.hidden = NO;
        self.label2.hidden = NO;
        self.label3.hidden = NO;
        self.label4.hidden = YES;
        self.leftButton.hidden = YES;
        self.rightButton.hidden = NO;
        self.bottomLabel.hidden = YES;
        
        self.rightButton.userInteractionEnabled = YES;
        self.label1.text = [NSString stringWithFormat:@"提货价: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceTotal]];
        self.label2.text = [NSString stringWithFormat:@"总货款: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.profitProductAmount]];
        self.label3.text = [NSString stringWithFormat:@"提货时间: %@",model.createTime];
        
        [self.rightButton setTitle:@"订单详情" forState:UIControlStateNormal];
        self.rightButton.layer.cornerRadius = 14;
        self.rightButton.layer.borderWidth = 1;
        self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
        [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    }
    
}


- (void)rightButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.rightBlock) {
        self.rightBlock(@"取消收藏");
    }
    
}
- (void)leftButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.leftBlock) {
        self.leftBlock(@"");
    }
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
