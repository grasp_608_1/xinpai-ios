//
//  XPShopProductBaseView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopProductBaseView : UIView

@property (nonatomic, strong) UIView *mainView;

- (void)initDataWithTile:(NSString *)titleStr productUrl:(NSString *)imageUrl descrptionString:(NSString *)descString;

@end

NS_ASSUME_NONNULL_END
