//
//  XPLogisticsCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/19.
//

#import <UIKit/UIKit.h>
#import "XPLogisticsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPLogisticsCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (CGFloat)cellWithModel:(XPLogisticsModel *)model;

@property (nonatomic, strong) UIView *topLine;
@property (nonatomic, strong) UIView *downLine;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *desLabel;
@property (nonatomic, strong) UIView *circleView;
@property (nonatomic, strong) UIView *mainView;


@end

NS_ASSUME_NONNULL_END
