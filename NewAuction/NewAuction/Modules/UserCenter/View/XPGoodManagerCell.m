//
//  XPGoodManagerCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/17.
//

#import "XPGoodManagerCell.h"

@interface XPGoodManagerCell ()

@property (nonatomic, strong) UIView *mainView;
//产品图片
@property (nonatomic, strong) UIImageView *pImageView;
//产品名称
@property (nonatomic, strong) UILabel *pTitleLabel;
//列表详情
@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;
@property (nonatomic, strong) UILabel *label3;
@property (nonatomic, strong) UILabel *label4;
//左侧按钮
@property (nonatomic, strong) UIButton *leftButton;
//右侧按钮
@property (nonatomic, strong) UIButton *rightButton;
//车
@property (nonatomic, strong) UIImageView *cardImageView;
//物流状态
@property (nonatomic, strong) UILabel *statusLabel;

@end


@implementation XPGoodManagerCell

/// cell 初始化方法
+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPGoodManagerCell";
    XPGoodManagerCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPGoodManagerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xFAFAFA);
        [self setupViews];
    }
    return self;
}

-(void)setupViews {
    
    self.mainView = [UIView new];
    self.mainView.layer.cornerRadius = 4;
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(12, 12, SCREEN_WIDTH - 12 * 2, 200);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:self.mainView];

    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(12, 12, 120, 120);
    self.pImageView.layer.cornerRadius = 4;
    self.pImageView.layer.masksToBounds = YES;
//    self.pImageView.backgroundColor = UIColor.grayColor;
    [self.mainView addSubview:self.pImageView];
    
    CGFloat LabelX = MaxX(self.pImageView) + 6;
    CGFloat LabelW = self.mainView.mj_w - LabelX  - 12;
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontMedium(16);
    self.pTitleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.pTitleLabel.frame = CGRectMake(LabelX, 10, LabelW, 20);
    [self.mainView addSubview:_pTitleLabel];
    
    self.label1 = [UILabel new];
    self.label1.font = kFontRegular(14);
    self.label1.textColor = UIColorFromRGB(0x3A3C41);
    self.label1.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel)+ 8, LabelW, 20);
    [self.mainView addSubview:self.label1];
    
    self.label2 = [UILabel new];
    self.label2.font = kFontRegular(14);
    self.label2.textColor = UIColorFromRGB(0x3A3C41);
    self.label2.frame = CGRectMake(LabelX, MaxY(self.label1)+ 4, LabelW, 20);
    [self.mainView addSubview:self.label2];
    
    self.label3 = [UILabel new];
    self.label3.font = kFontRegular(14);
    self.label3.textColor = UIColorFromRGB(0x3A3C41);
    self.label3.frame = CGRectMake(LabelX, MaxY(self.label2) + 4, LabelW, 20);
    [self.mainView addSubview:self.label3];
    
    self.label4 = [UILabel new];
    self.label4.font = kFontRegular(14);
    self.label4.textColor = UIColorFromRGB(0x8A8A8A);
    self.label4.frame = CGRectMake(LabelX, MaxY(self.label3)+ 4, LabelW, 20);
    [self.mainView addSubview:self.label4];
    
    
    self.cardImageView = [[UIImageView alloc] init];
    self.cardImageView.frame = CGRectMake(12, MaxY(self.pImageView) + 28, 24, 24);
    self.cardImageView.image  = [UIImage imageNamed:@"xp_car_head_left"];
    [self.mainView addSubview:self.cardImageView];
    
    self.statusLabel = [UILabel new];
    self.statusLabel.font = kFontMedium(14);
    self.statusLabel.textColor = app_font_color;
    self.statusLabel.frame = CGRectMake(MaxX(self.cardImageView) + 7, self.cardImageView.mj_y, 80, 24);
    self.statusLabel.text = @"待发货";
    [self.mainView addSubview:self.statusLabel];
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.frame = CGRectMake(self.mainView.mj_w - 12 - 90, self.mainView.frame.size.height - 12 - 32, 90, 32);
    self.rightButton.titleLabel.font = kFontRegular(14);
    self.rightButton.layer.borderWidth = 1;
    self.rightButton.layer.cornerRadius = 16;
    self.rightButton.layer.masksToBounds = YES;
    [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    [self.rightButton setTitle:@"订单详情" forState:UIControlStateNormal];
    [self.rightButton addTarget:self action:@selector(rightButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.rightButton];
    
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.frame = CGRectMake(self.mainView.mj_w - 12 - 90* 2 - 12, self.rightButton.mj_y, 90, 32);
    self.leftButton.titleLabel.font = kFontRegular(14);
    self.leftButton.layer.borderWidth = 1;
    self.leftButton.layer.cornerRadius = 16;
    self.leftButton.layer.masksToBounds = YES;
    [self.leftButton setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
    self.leftButton.layer.borderColor = UIColorFromRGB(0x8A8A8A).CGColor;
    [self.leftButton setTitle:@"物流信息" forState:UIControlStateNormal];
    [self.leftButton addTarget:self action:@selector(leftButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.leftButton];
}

- (void)cellWithModel:(XPAuctionModel *)model {
    
    self.pTitleLabel.text = model.name;
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:model.image]  placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    self.label1.text = [NSString stringWithFormat:@"数量 %@",model.amount];
    self.label2.text = [NSString stringWithFormat:@"单价 %@",[XPMoneyFormatTool formatMoneyWithNum:model.price]];
    self.label3.text = [NSString stringWithFormat:@"总价 %@",[XPMoneyFormatTool formatMoneyWithNum:model.priceTotal]];
    self.label4.text = [NSString stringWithFormat:@"时间 %@",model.createTime];
    self.statusLabel.text = [XPOrderDataTool getStatusTitle:model.orderStatus];
    if (model.expressNo) {
        self.leftButton.hidden = NO;
    }else {
        self.leftButton.hidden = YES;
    }
}


- (void)rightButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.rightBlock) {
        self.rightBlock();
    }
    
}
- (void)leftButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.leftBlock) {
        self.leftBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
