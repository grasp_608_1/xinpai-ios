//
//  XPNewsCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import <UIKit/UIKit.h>
#import "XPNewsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPNewsCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (CGFloat)cellWithModel:(XPNewsModel *)model;

@end

NS_ASSUME_NONNULL_END
