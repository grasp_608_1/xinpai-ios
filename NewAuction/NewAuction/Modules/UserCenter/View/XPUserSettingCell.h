//
//  XPUserSettingCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPUserSettingCell : UITableViewCell

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UIView *downLine;
//标题头
@property (nonatomic, strong) UILabel *extraLabel;

@property (nonatomic, strong) UILabel *desLabel;

@property (nonatomic, strong) UIImageView *rightArrow;

+ (instancetype)cellWithTableView:(UITableView *)tableView;


@end

NS_ASSUME_NONNULL_END
