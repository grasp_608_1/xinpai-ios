//
//  XPLogisticsCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/19.
//

#import "XPLogisticsCell.h"

@interface XPLogisticsCell ()

@end

@implementation XPLogisticsCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPLogisticsCell";
    XPLogisticsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPLogisticsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    
    self.mainView = [UIView new];
    self.mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12 * 2, 0);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:self.mainView];
    
    self.timeLabel = [UILabel new];
    self.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.timeLabel.font = kFontRegular(14);
    self.timeLabel.frame = CGRectMake(48, 12,self.mainView.mj_w - 48 - 12, 24);
    [self.mainView addSubview:self.timeLabel];
    
    self.desLabel = [UILabel new];
    self.desLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.desLabel.font = kFontRegular(14);
    self.desLabel.numberOfLines = NO;
    self.desLabel.frame = CGRectMake(48, 12,self.mainView.mj_w - 48 - 12, 24);
    [self.mainView addSubview:self.desLabel];
    
    self.topLine = [UIView new];
    self.topLine.backgroundColor = UIColorFromRGB(0xDEDEDE);
    [self.mainView addSubview:self.topLine];
    
    self.downLine = [UIView new];
    self.downLine.backgroundColor = UIColorFromRGB(0xDEDEDE);
    [self.mainView addSubview:self.downLine];
    
    self.circleView = [UIView new];
    self.circleView.backgroundColor = UIColorFromRGB(0xDEDEDE);
    self.circleView.layer.cornerRadius = 3.5;
    [self.mainView addSubview:self.circleView];

}

- (CGFloat)cellWithModel:(XPLogisticsModel *)model{
    
    self.timeLabel.text = model.ftime;
    self.desLabel.text = model.context;
    
    CGFloat desW = self.mainView.mj_w - 48 - 12;
    CGFloat desH = [model.context boundingRectWithSize:CGSizeMake(desW, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.desLabel.font} context:nil].size.height;
    self.desLabel.frame = CGRectMake(48, MaxY(self.timeLabel) + 4, desW, desH);
    
    self.circleView.frame = CGRectMake(21, 22, 7, 7);
    
    self.topLine.frame = CGRectMake(24, 0, 1, 23);
    
    self.downLine.frame = CGRectMake(24, 23, 1,MaxY(self.desLabel) + 4);
    
    self.mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12 * 2, MaxY(self.desLabel) + 4);
    
    CGFloat height = self.mainView.mj_h;
//    self.mainView.backgroundColor = RandColor;
//    NSLog(@" --- >%f",height);
    return height;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
