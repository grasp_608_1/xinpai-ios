//
//  XPImageAddView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/12.
//

#import "XPImageAddView.h"
#import <Photos/PHPhotoLibrary.h>
#import "UIImage+Compression.h"
#import "XPHitButton.h"
#import "AFNetworking.h"
#import "XPOSSUploadTool.h"

@interface XPImageAddView ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, assign) NSInteger maxCount;

@property (nonatomic, strong) UIView *defaultView;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) NSMutableArray *imagesArray;

@property (nonatomic, strong) UIViewController *controller;

@property (nonatomic, strong) UIImage *chooseImage;

@end

@implementation XPImageAddView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.controller = [XPCommonTool getCurrentViewController];
    self.dataArray = [NSMutableArray array];
    self.imagesArray = [NSMutableArray array];
    
    self.maxCount = 3;
    CGFloat imageWH = (self.mj_w - (self.maxCount - 1)* 12)/self.maxCount;
    
    self.defaultView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, imageWH, imageWH)];
    self.defaultView.tag = 999;
    self.defaultView.userInteractionEnabled = YES;
    self.defaultView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [self addSubview:self.defaultView];
    
    UIImageView *defaultImageView = [[UIImageView alloc] init];
    defaultImageView.image = [UIImage imageNamed:@"xp_default_empty2"];
    defaultImageView.frame = CGRectMake(imageWH/2 - 12, imageWH/2 - 24, 24, 24);
    [self.defaultView addSubview:defaultImageView];
    
    UILabel *defaultLabel = [UILabel new];
    defaultLabel.frame = CGRectMake(0, imageWH/2., imageWH, 32);
    defaultLabel.textAlignment = NSTextAlignmentCenter;
    defaultLabel.numberOfLines = NO;
    defaultLabel.textColor = UIColorFromRGB(0xB8B8B8);
    defaultLabel.font = kFontRegular(10);
    defaultLabel.text = @"添加图片\n最多3张";
    [self.defaultView addSubview:defaultLabel];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.defaultView addGestureRecognizer:tap];
    
}
- (void)tap:(UIGestureRecognizer *)tap {
    buttonCanUseAfterOneSec(tap.view);
    
    [[XPVerifyTool shareInstance] phoneAlbumCompleteBlock:^{
        [self showAlertWithMsg:nil];
    }];
    
}

//上传图片
-(void)showAlertWithMsg:(NSString *)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       
    }];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openCamera];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openAlbum];
    }];
    [alert addAction:cancel];
    [alert addAction:action];
    [alert addAction:action2];

    [self.controller presentViewController:alert animated:YES completion:nil];
    
    
}

- (void)openCamera {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.navigationBar.translucent = NO;
    [self.controller presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}
- (void)openAlbum {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.navigationBar.translucent = NO;
    [self.controller presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:true completion:^{
        
    }];
    NSString *type  = [info objectForKey:UIImagePickerControllerMediaType];
    //选择的类型图片
    if ([type isEqualToString:@"public.image"]) {
        //先把图片转成NSData
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        self.chooseImage = [image compressToImage];
    }
    [picker dismissViewControllerAnimated:YES completion:^{
        [self uploadCardWithImage:self.chooseImage];
    }];
  
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)reloadUI{
    
    CGFloat imageWH = (self.mj_w - (self.maxCount - 1)* 12)/self.maxCount;
    
    for (UIView  *view in self.subviews) {
        if (view.tag != 999) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i<self.dataArray.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] init];
        [self addSubview:imageView];
        imageView.layer.cornerRadius = 8;
        imageView.layer.masksToBounds = YES;
        imageView.frame = CGRectMake((imageWH + 12) * i, 0, imageWH, imageWH);
        
        XPHitButton *button = [[XPHitButton alloc] init];
        button.frame = CGRectMake(MaxX(imageView) - 20 , 0, 20, 20);
        button.hitRect = 40;
        button.tag = i;
        [button addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [button setImage:[UIImage imageNamed:@"xp_image_delete"] forState:UIControlStateNormal];
        [self addSubview:button];
        
        imageView.image = self.dataArray[i];
    
    }
    
    if (self.dataArray.count >= 3) {
        self.defaultView.hidden = YES;
    }else {
        self.defaultView.hidden = NO;
        self.defaultView.frame = CGRectMake((imageWH + 12) * self.dataArray.count, 0, imageWH, imageWH);
    }
    
    
}

- (void)deleteButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self.dataArray removeObjectAtIndex:sender.tag];
    [self.imagesArray removeObjectAtIndex:sender.tag];
    if (self.changeBlock) {
        self.changeBlock(self.imagesArray);
    }
    [self reloadUI];
}


- (void)uploadCardWithImage:(UIImage *)iConImage {
    
    iConImage = [iConImage compressToImage];
    NSString *filePath = [[XPOSSUploadTool shareInstance] saveImageToSandbox:iConImage];
    
    XPBaseViewController *controller = (XPBaseViewController *)[XPCommonTool getCurrentViewController];
    
    [controller startLoadingGif];
    
    XPMediaType type;
    if (self.type == 0) {
        type = XPMediaTypeFeedback;
    }else if(self.type == 1){
        type = XPMediaTypeShopAfterSale;
    }else{
        type = XPMediaTypeShopComment;
    }
    
    [[XPOSSUploadTool shareInstance] uploadFileWithMediaType:type filePath:filePath progress:^(CGFloat progress) {
        
    } completeBlock:^(BOOL success, NSString * _Nonnull remoteUrlStr) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [controller stopLoadingGif];
            if (success) {
                [self.dataArray addObject:iConImage];
                [self.imagesArray addObject:remoteUrlStr];
                if (self.changeBlock) {
                    self.changeBlock(self.imagesArray);
                }
                [self reloadUI];
            }else{
                [[XPShowTipsTool shareInstance] showMsg:@"上传失败,请稍后重试!" ToView:k_keyWindow];
            }
        });
    }];
}

-(void)setType:(NSInteger)type{
    _type = type;
}

-(NSArray *)urlImagesArray{
    
    return self.imagesArray.copy;
}

@end
