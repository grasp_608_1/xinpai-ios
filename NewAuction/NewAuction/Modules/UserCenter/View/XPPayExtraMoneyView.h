//
//  XPPayExtraMoneyView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/15.
//

#import <UIKit/UIKit.h>
#import "XPBankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPPayExtraMoneyView : UIView

//@property (nonatomic, strong) XPBankModel *bankModel;
//验证码点击
@property (nonatomic, copy) void(^codeBlock)(XPBankModel *bankModel);
//确定按钮点击
@property (nonatomic, copy) void(^sureBlock)(NSString *codeStr);
//验证码点击按钮
@property (nonatomic, strong) UIButton *codeButton;
- (void)showWithMoney:(NSString *)money;
/// 消失
/// - Parameter animation: 视图消失是否有动画
- (void)dismiss:(BOOL)animation;
/// 验证码输入框成为第一响应者
- (void)codeTextBecomeFirstResponder;

-(void)countDown;
@end

NS_ASSUME_NONNULL_END
