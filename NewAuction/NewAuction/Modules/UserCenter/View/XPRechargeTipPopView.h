//
//  XPRechargeTipPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPRechargeTipPopView : UIView


- (void)viewWithDictionary:(NSDictionary *)dict;


@end

NS_ASSUME_NONNULL_END
