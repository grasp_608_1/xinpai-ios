//
//  XPUserCenterCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPUserCenterCell : UITableViewCell

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UIView *downLine;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (void)cellWithtitle:(NSString *)title IconName:(NSString *)iconName;
@end

NS_ASSUME_NONNULL_END
