//
//  XPTrendTapHeaderView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPTrendTapHeaderView : UIView


@property (nonatomic, copy) void(^itemBlock)(NSInteger tag);


-(void)freshWithTopTitle:(NSString *)totalStr
                letTitle:(NSString *)leftTitle
         leftTitleDesStr:(NSString *)leftDesc
            leftAmountTitleStr:(NSString *)leftAmountTitleStr
                rightTitle:(NSString *)rightTitle
       rightTitleDescStr:(NSString *)rigthTitleDescStr
        rightAmountTitle:(NSString *)rightAmountTitleStr;

@end

NS_ASSUME_NONNULL_END
