//
//  XPPreferenceSetCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPPreferenceSetCell : UITableViewCell
@property (nonatomic, copy) NSDictionary *dataDic;
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end

NS_ASSUME_NONNULL_END
