//
//  XPUserCenterItemView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/11.
//

#import "XPUserCenterItemView.h"

@interface XPUserCenterItemView ()


@property (nonatomic, strong) UIButton *moreButton;

@end

@implementation XPUserCenterItemView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
    
}

- (CGFloat)configWithtitle:(NSString *)titleStr dataArray:(NSArray *)dataArray MaxCount:(NSInteger)maxCount showMoreButton:(BOOL)show showTopView:(BOOL)showTop{
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (maxCount > dataArray.count) {//加个容错,防止越界
        maxCount = dataArray.count;
    }
    
    if (showTop) {
        UILabel *titleLabel = [UILabel new];
        titleLabel.font = kFontMedium(16);
        titleLabel.textColor = UIColorFromRGB(0x3A3C41);
        titleLabel.text = titleStr;
        titleLabel.frame = CGRectMake(12 , 12, 200, 24);
        [self addSubview:titleLabel];
        
        if (show) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(self.mj_w - 70,titleLabel.mj_y,58,24);
            [button setTitleColor:UIColorFromRGB(0xB8B8B8) forState:UIControlStateNormal];
            [button setTitle:@"更多服务" forState:UIControlStateNormal];
            button.titleLabel.font = kFontMedium(14);
            [button addTarget:self action:@selector(morebuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            self.moreButton = button;
            [self addSubview:button];
        }
    }
    
    
    
    CGFloat itemW = (self.mj_w - 30)/4;
    CGFloat itemH =  70;
    
    CGFloat TopMargin = showTop ? 48 :24;
    CGFloat maxH = 0;
    
    for (int idx = 0; idx< maxCount; idx++) {
        NSDictionary *dic = dataArray[idx];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake((itemW + 10) * (idx%4), TopMargin +  (itemH + 12)*(idx/4), itemW, itemH);
        [self addSubview:button];
        
        UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(button.mj_w/2 - 20, 0, 40, 40)];
        imageV.image = [UIImage imageNamed:dic[@"iconName"]];
        [button addSubview:imageV];
        
        UILabel *titleLabel = [UILabel new];
        titleLabel.font = kFontRegular(14);
        titleLabel.textColor = UIColorFromRGB(0x3A3C41);
        titleLabel.frame = CGRectMake(0, itemH - 24, itemW, 24);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = dic[@"name"];
        [button addSubview:titleLabel];
        button.tag = idx;
        [button addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        maxH = MaxY(button);
    }
    return maxH + 12;
}

-(void)setMoreTitle:(NSString *)moreTitle{
    [self.moreButton setTitle:moreTitle forState:UIControlStateNormal];
}

-(void)itemButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.itemSelectBlock) {
        self.itemSelectBlock(sender.tag);
    }    
}

- (void)morebuttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.moreBlock) {
        self.moreBlock();
    }
}


@end
