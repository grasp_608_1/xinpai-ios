//
//  XPUserCenterValueView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/11.
//

#import "XPUserCenterValueView.h"

@interface XPUserCenterValueView ()

@property (nonatomic, strong) UIImageView *iconbgImageView;
@property (nonatomic, strong) UILabel *desLabel;

@end

@implementation XPUserCenterValueView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
    
    self.iconbgImageView = [UIImageView new];
    self.iconbgImageView.backgroundColor = [UIColor whiteColor];
    self.iconbgImageView.frame = self.bounds;
    [self addSubview:self.iconbgImageView];
    
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = kFontMedium(20);
    self.titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.titleLabel.frame = CGRectMake(12, 16, self.mj_w - 12, 24);
    [self addSubview:self.titleLabel];
    
    self.desLabel = [UILabel new];
    self.desLabel.font = kFontMedium(14);
    self.desLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.desLabel.frame = CGRectMake(self.titleLabel.mj_x, MaxY(self.titleLabel) + 6, self.mj_w - 12, 18);
    self.desLabel.alpha = 0.7;
    [self addSubview:self.desLabel];
    
    
}

- (void)configWithTitle:(NSString *)titleStr desStr:(NSString *)desStr image:(UIImage *)image {
    self.titleLabel.text = titleStr;
    self.desLabel.text = desStr;
    self.iconbgImageView.image = image;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
