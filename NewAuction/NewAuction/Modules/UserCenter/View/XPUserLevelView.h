//
//  XPUserLevelView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/8/28.
//

#import <UIKit/UIKit.h>
#import "XPUserInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPUserLevelView : UIButton

@property (nonatomic, strong) XPUserInfoModel *userModel;

@end

NS_ASSUME_NONNULL_END
