//
//  XPUserCenterValueView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPUserCenterValueView : UIView

@property (nonatomic, strong) UILabel *titleLabel;

/// - Parameters:
///   - titleStr: 标题
///   - desStr: 描述
///   - image: 图片背景Image
- (void)configWithTitle:(NSString *)titleStr
                 desStr:(NSString *)desStr
                  image:(UIImage *)image;

@end

NS_ASSUME_NONNULL_END
