//
//  XPShopProductBaseView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/12.
//

#import "XPShopProductBaseView.h"

@interface XPShopProductBaseView ()



@property (nonatomic, strong) UIImageView  *iconImageView;

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UILabel *timeLabel;

@end


@implementation XPShopProductBaseView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 0, self.mj_w - 12*2, self.mj_h);
    mainView.clipsToBounds = YES;
    mainView.layer.cornerRadius = 8;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.frame = CGRectMake(12, 12, 80, 80);
    self.iconImageView.layer.cornerRadius = 15;
    [self.mainView addSubview:self.iconImageView];
    
    UILabel *nameLabel = [UILabel new];
    nameLabel.font = kFontRegular(14);
    nameLabel.textColor = app_font_color;
    nameLabel.frame = CGRectMake(105, 14,self.mainView.mj_w - 105 - 12, 48);
    nameLabel.numberOfLines = NO;
    self.nameLabel = nameLabel;
    [self.mainView addSubview:nameLabel];
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = kFontRegular(14);
    timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
    timeLabel.frame = CGRectMake(105, 70,self.mainView.mj_w - 105 - 12, 24);
    self.timeLabel = timeLabel;
    [self.mainView addSubview:timeLabel];
    
}

- (void)initDataWithTile:(NSString *)titleStr productUrl:(NSString *)imageUrl descrptionString:(NSString *)descString {
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
   
    self.nameLabel.text = titleStr;
    
    self.timeLabel.text = descString;
}

@end
