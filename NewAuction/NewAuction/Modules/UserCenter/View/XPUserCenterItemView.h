//
//  XPUserCenterItemView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPUserCenterItemView : UIView
//具体item 点击事件
@property (nonatomic, copy) void(^itemSelectBlock)(NSUInteger index);
//更多按钮显示字段
@property (nonatomic, copy) NSString *moreTitle;
//更多按钮点击
@property (nonatomic, copy) void(^moreBlock)(void);

- (CGFloat)configWithtitle:(NSString *)titleStr dataArray:(NSArray *)dataArray MaxCount:(NSInteger)maxCount showMoreButton:(BOOL)show showTopView:(BOOL)showTop;
@end

NS_ASSUME_NONNULL_END
