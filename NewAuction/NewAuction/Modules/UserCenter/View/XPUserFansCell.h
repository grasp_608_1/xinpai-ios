//
//  XPUserFansCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/12.
//

#import <UIKit/UIKit.h>
#import "XPFansModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPUserFansCell : UITableViewCell

@property (nonatomic, strong) XPFansModel *model;

+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end

NS_ASSUME_NONNULL_END
