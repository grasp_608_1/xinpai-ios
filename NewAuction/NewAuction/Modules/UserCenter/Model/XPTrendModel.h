//
//  XPTrendModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPTrendModel : NSObject

@property (nonatomic, strong) NSNumber *amount;
//条目类型
@property (nonatomic, strong) NSNumber *itemType;
//条目名称
@property (nonatomic, copy)   NSString *itemTypeName;
//提现条目名称
@property (nonatomic, copy)   NSString *withdrawTypeName;
@property (nonatomic, copy)   NSString *createTime;
@property (nonatomic, copy)   NSString *name;
@property (nonatomic, copy)   NSString *referName;//粉丝名字
@property (nonatomic, copy)   NSString *updateTime;
//手续费
@property (nonatomic, strong) NSNumber *serviceCharge;
@property (nonatomic, strong) NSNumber *alterType;

@property (nonatomic, copy)   NSString *itemName;

@property (nonatomic, strong) NSNumber *countPricePar;//总价值

@property (nonatomic, strong) NSNumber *amountPar;//总数量
@property (nonatomic, strong) NSNumber *amountParSuccess;//总数量

@property (nonatomic, strong) NSString *entTime;//
@property (nonatomic, strong) NSNumber *priceEnt;//
@property (nonatomic, strong) NSNumber *amountEnt;//
@property (nonatomic, strong) NSNumber *valueEnt;//

@property (nonatomic, strong) NSString *rechargeNo;

@property (nonatomic, strong) NSNumber *changeCount;//积分数值变化
@property (nonatomic, strong) NSNumber *sourceType;//积分变化类型
@property (nonatomic, strong) NSNumber *changeType;//积分变化类型 0 增加 1 减少
@property (nonatomic, strong) NSString *sourceTypeName;

@end

NS_ASSUME_NONNULL_END
