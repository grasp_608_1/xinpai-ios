//
//  XPFansModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPFansModel : NSObject

@property (nonatomic, copy) NSString *head;
@property (nonatomic, copy) NSString *nickname;//昵称1
@property (nonatomic, copy) NSString *userNickname;//昵称2
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *createDate;//注册时间
@property (nonatomic, copy) NSString *createTime;//注册时间
//收益
@property (nonatomic, strong) NSNumber *cashCount;
//用户等级
@property (nonatomic, strong) NSNumber *userType;
//关注状态 0 未关注 1 已关注
@property (nonatomic, strong) NSNumber *status;
//被关注用户id
@property (nonatomic, strong) NSNumber *toUserId;
//关注人用户id
@property (nonatomic, strong) NSNumber *fromUserId;
//被关注用户id
@property (nonatomic, strong) NSNumber *userId;
@end

NS_ASSUME_NONNULL_END
