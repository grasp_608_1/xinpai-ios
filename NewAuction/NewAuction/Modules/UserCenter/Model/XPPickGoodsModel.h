//
//  XPPickGoodsModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPPickGoodsModel : NSObject

@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *orderNo;
//快递单号
@property (nonatomic, copy) NSString *expressNo;
//快递公司
@property (nonatomic, copy) NSString *expressCompany;//快递公司
@property (nonatomic, copy) NSString *code100;//快递公司简称
//发货时间
@property (nonatomic, copy) NSString *expressTime;
//收货时间
@property (nonatomic, copy) NSString *receiptTime;
//物流的手机号
@property (nonatomic, copy) NSString *phoneNum;

@property (nonatomic, strong) NSNumber *pricePick;
@property (nonatomic, strong) NSNumber *profitPool;
@property (nonatomic, strong) NSNumber *profitProduct;
@property (nonatomic, strong) NSNumber *earningsProduct;
@property (nonatomic, strong) NSNumber *maxNumProduct;
@property (nonatomic, strong) NSNumber *addressId;
@property (nonatomic, strong) NSNumber *priceTotal;
//是否包邮，0:否 1：是
@property (nonatomic, strong) NSNumber *isPostage;
@property (nonatomic, strong) NSNumber *postageBase;
@property (nonatomic, strong) NSNumber *postageStep;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *stockAmount;//库存
//邮价
@property (nonatomic, strong) NSNumber *mailPriceTotal;
//商品总价,不含邮费
@property (nonatomic, strong) NSNumber *productPriceTotal;
//补差价
@property (nonatomic, strong) NSNumber *priceDifference;
@property (nonatomic, strong) NSNumber *orderStatus;


@end

NS_ASSUME_NONNULL_END
