//
//  XPAdressModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPAdressModel : NSObject

@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *updateTime;

@property (nonatomic, strong) NSNumber *isDefault;
@property (nonatomic, strong) NSNumber *isDelete;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSNumber *productId;

//自定义属性,标记是否选中 1 选中  0 未选中
@property (nonatomic, strong) NSString *isSelected;


/*
 address = "路他他英语他";
 area = "北京市市辖区东城区";
 createTime = "2024-06-18T14:02:12";
 id = 26;
 isDefault = 1;
 isDelete = 0;
 name = "阿尔山";
 phone = 15010738532;
 updateTime = "2024-06-18T14:02:12";
 userId = 9;
 */

@end

NS_ASSUME_NONNULL_END
