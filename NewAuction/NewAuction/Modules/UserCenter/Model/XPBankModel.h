//
//  XPBankModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPBankModel : NSObject

@property (nonatomic, strong) NSString *bankName;
@property (nonatomic, strong) NSString *cardNo;
@property (nonatomic, strong) NSString *bankPhone;
@property (nonatomic, strong) NSString *updateTime;
@property (nonatomic, strong) NSString *createTime;


@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSNumber *cardType;
@property (nonatomic, strong) NSNumber *isDelete;
@property (nonatomic, strong) NSNumber *productId;
@property (nonatomic, strong) NSNumber *unbindOrderNo;;

@end

NS_ASSUME_NONNULL_END
