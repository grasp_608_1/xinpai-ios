//
//  XPAuctionModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPAuctionModel : NSObject

@property (nonatomic, strong) NSNumber *amount;//库存
@property (nonatomic, strong) NSNumber *amountEnt;
@property (nonatomic, strong) NSNumber *amountLast;//剩余拍品个数
@property (nonatomic, strong) NSNumber *amountPar;//拍品个数
@property (nonatomic, strong) NSNumber *amountParSuccess;//成功参拍数量
//成功参拍拍品价值
@property (nonatomic, strong) NSNumber *lotParSuccess;
@property (nonatomic, strong) NSNumber *amountLastPar;//剩余拍品价值
@property (nonatomic, strong) NSNumber *lotPar;//拍品价值
@property (nonatomic, strong) NSNumber *auctionItemId;
//0：待支付 1：已支付 2：支付失败 3：已发货 4：已收货 5：待支付确认 6：已确认 7：待提货 8：提货成功 9：提货失败"
@property (nonatomic, strong) NSString *orderStatus;
@property (nonatomic, strong) NSNumber *auctionStatus;//参拍状态
@property (nonatomic, copy)   NSString *auctionStatusMapping;//参拍数量/参拍中状态展示
@property (nonatomic, strong) NSNumber *productId;//商品id
@property (nonatomic, strong) NSNumber *productSkuId;//规格id
@property (nonatomic, strong) NSNumber *autionId;//拍品id
@property (nonatomic, strong) NSNumber *pricePar;//拍品价格
@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSNumber *priceEnt;//拍品价格2
@property (nonatomic, strong) NSNumber *valueEnt;//拍品价值2
@property (nonatomic, strong) NSNumber *entStatus;//是否可为委拍
@property (nonatomic, strong) NSNumber *commissionedGoodsButtonStatus;//是否已利润结算
@property (nonatomic, strong) NSNumber *remainingCommissionableQuantity;//剩余可委拍个数(剩余拍品个数)
@property (nonatomic, strong) NSNumber *pricePick;//提货价
@property (nonatomic, strong) NSNumber *price;//单价
@property (nonatomic, strong) NSNumber *profitProductTotal;//总货款
@property (nonatomic, strong) NSNumber *priceTotal;//总货款
@property (nonatomic, strong) NSNumber *profitProductAmount;//总货款

@property (nonatomic, copy) NSString *auditPerson;
@property (nonatomic, copy) NSString *auditTime;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *timePar;
@property (nonatomic, copy) NSString *updateTime;
@property (nonatomic, copy) NSString *image;//商品图片地址
@property (nonatomic, copy) NSString *name;//商品名称

@property (nonatomic, copy) NSString *expressNo;//快递单号
@property (nonatomic, copy) NSString *expressCompany;//快递公司
@property (nonatomic, copy) NSString *code100;//快递公司简称
@property (nonatomic, copy) NSString *phone;//邮寄手机号

@end


NS_ASSUME_NONNULL_END
