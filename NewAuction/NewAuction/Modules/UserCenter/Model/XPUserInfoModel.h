//
//  XPUserInfoModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPUserInfoModel : NSObject

@property (nonatomic, copy) NSString *head;//头像地址
@property (nonatomic, copy) NSString *nickname;//昵称
@property (nonatomic, copy) NSString *name;//姓名
@property (nonatomic, strong) NSString *idCard;//身份证号
@property (nonatomic, strong) NSString *invCode;//我的邀请码
@property (nonatomic, strong) NSString *supInvCode;//我的父级邀请码
@property (nonatomic, strong) NSString *introduce;//个性签名
@property (nonatomic, strong) NSString *invitePic;//积分申请背景图

@property (nonatomic, copy)  NSNumber *estimatedPrice;//预估价值
@property (nonatomic, strong) NSNumber *auctionsNumber;//参拍数量
@property (nonatomic, strong) NSNumber *entNumber;//委拍量
@property (nonatomic, strong) NSNumber *inventoryNumber;//库存量

@property (nonatomic, strong) NSNumber *isCertified;//是否实名认证 0 是 1 否
@property (nonatomic, strong) NSNumber *phone;//手机号码
@property (nonatomic, strong) NSNumber *userType;//用户类型
@property (nonatomic, strong) NSNumber *makerId;//用户类型



@end

NS_ASSUME_NONNULL_END
