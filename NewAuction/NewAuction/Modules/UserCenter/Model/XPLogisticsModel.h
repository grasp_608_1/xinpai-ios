//
//  XPLogisticsModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPLogisticsModel : NSObject
//时间
@property (nonatomic, copy) NSString *ftime;
//
@property (nonatomic, copy) NSString *context;
//快递公司名称
@property (nonatomic, copy) NSString *deliveryCompany;
//快递单号
@property (nonatomic, copy) NSString *deliverySn;
//快递编码
@property (nonatomic, copy) NSString *deliveryCode;

@end

NS_ASSUME_NONNULL_END
