//
//  XPUserAddressController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/13.
//

#import "XPUserAddressController.h"
#import "XPUserAddressCell.h"
#import "XPUserAddressEditController.h"
#import "XPEmptyView.h"


@interface XPUserAddressController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *mainTableView;

@property (nonatomic, strong) XPEmptyView *emptyView;

//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation XPUserAddressController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [self setupUI];
    
    
}
-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"我的收货地址";
    [self.view addSubview:self.navBarView];
}
- (void)setupUI {
    
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.showsVerticalScrollIndicator = NO;
    self.mainTableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mainTableView.frame = CGRectMake(0,kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -kTopHeight - 80 );
    
    [self.view addSubview:self.mainTableView];
    
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(12,SCREEN_HEIGHT -30 - 50, SCREEN_WIDTH - 2 * 12, 50);
    [saveButton setTitle:@"添加收货地址" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    saveButton.layer.cornerRadius = 6;
    saveButton.clipsToBounds = YES;
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveButton];
    
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_empty_location"] Description:@"您还没有收货地址"];
    [self.view addSubview:self.emptyView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPUserAddressCell *cell = [XPUserAddressCell cellWithTableView:tableView];
    
    
    cell.rightArrow.hidden = NO;
    
    XPAdressModel *model = self.dataArray[indexPath.row];
    
    [cell cellWithDic:model];
    //只有一行时
    if (indexPath.row == 0) {
            [XPCommonTool cornerRadius:cell.mainView andType:1 radius:4];
        }else if (indexPath.row == self.dataArray.count - 1) {
            [XPCommonTool cornerRadius:cell.mainView andType:2 radius:4];
        }else {
            
        }
        
        if (indexPath.row == self.dataArray.count - 1) {
            cell.downLine.hidden = YES;
        }else {
            cell.downLine.hidden = NO;
        }
    
    [cell setEditBlock:^{
        XPAdressModel *model = self.dataArray[indexPath.row];
        [self jumpEditWithModel:model];
    }];
    
    return cell;
    
}

- (void)jumpEditWithModel:(XPAdressModel *)model{
    XPUserAddressEditController *vc = [XPUserAddressEditController new];
    vc.enterType = 1;
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 86;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XPAdressModel *model = self.dataArray[indexPath.row];
    if (self.entryType == 1) {
        if (self.chooseAdress) {
            self.chooseAdress(model);
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)saveButtonClicked:(UIButton *)sender {

    buttonCanUseAfterOneSec(sender);
    XPUserAddressEditController *vc = [XPUserAddressEditController new];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getData];
}
- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kUser_adress_list];

//    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:nil requestResultBlock:^(XPRequestResult * _Nonnull data) {
//        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *modelArray = [XPAdressModel mj_objectArrayWithKeyValuesArray:data.userData];
            
            if (modelArray.count > 0) {
                self.dataArray = modelArray.mutableCopy;
                [self.mainTableView reloadData];
                self.mainTableView.hidden = NO;
                self.emptyView.hidden = YES;
            }else {
                self.mainTableView.hidden = YES;
                self.emptyView.hidden = NO;
            }
            
            
        }else {
            
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];//kVERIFY_product_share
}

-(void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
