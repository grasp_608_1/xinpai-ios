//
//  XPUserFeedbackController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/12.
//

#import "XPUserFeedbackController.h"
#import "XPImageAddView.h"
#import "XPLimitInputTextView.h"

@interface XPUserFeedbackController ()
//反馈类型
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, assign) NSInteger typeIndex;

@property (nonatomic, strong) UIView *commentView;

@property (nonatomic, strong) XPLimitInputTextView *textView;

@property (nonatomic, strong) XPImageAddView *imageAddView;
//联系方式
@property (nonatomic, strong) UITextField *textfield;

@property (nonatomic, copy)   NSArray *imagesArray;

@end

@implementation XPUserFeedbackController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [self setupViews];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"提交反馈";
    [self.view addSubview:self.navBarView];
}


- (void)setupViews {
    
    UIButton *topView = [[UIButton alloc] initWithFrame:CGRectMake(12, kTopHeight + 12, self.view.mj_w - 24, 56)];
    topView.backgroundColor = [UIColor whiteColor];
    topView.layer.cornerRadius = 8;
    [topView addTarget:self action:@selector(topButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topView];
    
    UILabel *typeDescabel = [UILabel new];
    typeDescabel.font = kFontRegular(16);
    typeDescabel.textColor = app_font_color;
    typeDescabel.text = @"反馈类型";
    typeDescabel.frame = CGRectMake(16, 0, 150, topView.mj_h);
    [topView addSubview:typeDescabel];
    
    UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(topView.mj_w - 16 - 24, 16, 24, 24)];
    arrow.image = [UIImage imageNamed:@"Group_57"];
    [topView addSubview:arrow];
    
    UILabel *typeLabel = [UILabel new];
    typeLabel.font = kFontRegular(14);
    typeLabel.textColor = UIColorFromRGB(0x8A8A8A);
    typeLabel.textAlignment = NSTextAlignmentRight;
    self.typeLabel = typeLabel;
    typeLabel.frame = CGRectMake(topView.mj_w - 40 - 155, 0, 150, topView.mj_h);
    [topView addSubview:typeLabel];

    
    self.commentView = [[UIView alloc]initWithFrame:CGRectMake(12, MaxY(topView) + 12, self.view.mj_w - 24, 281)];
    self.commentView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:self.commentView];
    
    self.textView = [[XPLimitInputTextView alloc] initWithFrame:CGRectMake(12,20, self.commentView.mj_w - 24, 120)];
    [self.textView configWithMaxCount:200 textViewPlaceholder:@"请输入反馈内容"];
    [self.commentView addSubview:self.textView];
    
    self.imageAddView = [[XPImageAddView alloc] initWithFrame:CGRectMake(24, MaxY(self.textView) + 12, self.view.mj_w - 48, 101)];
    self.imageAddView.type = 0;
    [self.commentView addSubview:self.imageAddView];
    
    UIView *bottom = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 48 -self.safeBottom, SCREEN_WIDTH, 48 + self.safeBottom)];
    bottom.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:bottom];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(24, 4,bottom.mj_w - 48 , 40);
    rightButton.layer.cornerRadius = 20;
    rightButton.clipsToBounds = YES;
    [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    rightButton.titleLabel.font = kFontMedium(14);
    [rightButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setTitle:@"提交反馈" forState:UIControlStateNormal];
    [bottom addSubview:rightButton];
    
    [rightButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :rightButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    UIView *connectView = [[UIView alloc] initWithFrame:CGRectMake(12, MaxY(self.commentView) + 12, SCREEN_WIDTH - 24, 56)];
    connectView.layer.cornerRadius = 8;
    connectView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:connectView];
    
    UILabel *connectDescLabel = [UILabel new];
    connectDescLabel.font = kFontRegular(16);
    connectDescLabel.textColor = app_font_color;
    connectDescLabel.frame = CGRectMake(16, 0, 64, connectView.mj_h);
    connectDescLabel.text = @"联系方式";
    [connectView addSubview:connectDescLabel];
    
    self.textfield = [[UITextField alloc] init];
    self.textfield.font = kFontRegular(14);
    self.textfield.textColor = app_font_color;
    self.textfield.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.textfield.layer.cornerRadius = 4;
    self.textfield.placeholder = @" 邮箱/手机号";
    self.textfield.keyboardType  = UIKeyboardTypeURL;
    self.textfield.frame = CGRectMake(88, 8, connectView.mj_w - 88 - 44 , 40);
    self.textfield.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, self.textfield.mj_h)];
    self.textfield.leftViewMode = UITextFieldViewModeAlways;
    [connectView addSubview:self.textfield];
    
    UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [editButton setImage:[UIImage imageNamed:@"xp_text_edit"] forState:UIControlStateNormal];
    [editButton addTarget:self action:@selector(editButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    editButton.frame = CGRectMake(connectView.mj_w - 24 - 12, 16, 24, 24);
    [connectView addSubview:editButton];
    
    XPWeakSelf
    [self.imageAddView setChangeBlock:^(NSArray * _Nonnull imagesArray) {
        weakSelf.imagesArray = imagesArray;
    }];
    
}

- (void)topButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"反馈类型"
                                                                    message:nil
                                                             preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSArray *array = @[@"意见与建议",@"功能异常",@"系统卡顿",@"界面错位",@"其他"];
    for (int i = 0; i< array.count; i++) {
        [alertC addAction:[UIAlertAction actionWithTitle:array[i]                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            self.typeLabel.text = array[i];
            self.typeIndex = i;
        }]];
    }
    [alertC addAction:[UIAlertAction actionWithTitle:@"取消"                                                               style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    
    [self presentViewController:alertC animated:YES completion:nil];
}

- (void)editButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self.textfield becomeFirstResponder];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)bottomButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.typeLabel.text.length == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"请选择您的反馈类型" ToView:k_keyWindow];
    }else if ([XPCommonTool isEmpty:self.textView.contentView.text]){
        [[XPShowTipsTool shareInstance] showMsg:@"请输入您的反馈内容" ToView:k_keyWindow];
    }else if (self.textfield.text.length >0){
        if ([XPCommonTool validVerifyPredicate:phonePredicate TargetString:self.textfield.text] || [self.textfield.text containsString:@".com"]) {
            [self postUserAdvice];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的手机号或邮箱" ToView:k_keyWindow];
        }
        
    }else {
       //提交
        [self postUserAdvice];
    }
    
}
//提交服务器反馈意见
- (void)postUserAdvice{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_submit_feedback];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"content"] = self.textView.contentView.text;
    paraM[@"type"] = @(self.typeIndex);
    if (self.textfield.text.length > 0) {
        paraM[@"phone"] = self.textfield.text;
    }
    if (self.imagesArray.count > 0) {
        paraM[@"pics"] = [self.imagesArray componentsJoinedByString:@","];
    }
    
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
           
            [[XPShowTipsTool shareInstance] showLongDuringMsg:@"提交成功!" ToView:k_keyWindow];
            [self.navigationController popViewControllerAnimated:YES];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//上传图片


@end
