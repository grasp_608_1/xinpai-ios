//
//  XPPayToCompanyController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/29.
//

#import "XPPayToCompanyController.h"
#import "BRPickerView.h"
#import "UIImage+Compression.h"
#import "XPOSSUploadTool.h"

@interface XPPayToCompanyController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>

@property (nonatomic, strong) UIScrollView *mainView;

@property (nonatomic, strong) UILabel *tipDesLabel;
//公款信息账户
@property (nonatomic, strong) UIView *bankInfoView;
@property (nonatomic, strong) UILabel *label1;//账户名称
@property (nonatomic, strong) UIButton *button1;
@property (nonatomic, strong) UILabel *label2;//账户号
@property (nonatomic, strong) UIButton *button2;
@property (nonatomic, strong) UILabel *label3;//开户银行
@property (nonatomic, strong) UIButton *button3;
//注意事项标题头
@property (nonatomic, strong) UILabel *attentionLabel;
//注意事项
@property (nonatomic, strong) UILabel *attentionDescLabel;
//上传凭证
@property (nonatomic, strong) UILabel *uploadTitleLabel;
//图片
@property (nonatomic, strong) UIImageView *uploadImageView;
//输入框
@property (nonatomic, strong) UIView *inputView;
//支付时间
@property (nonatomic, strong) UILabel *payTimeLabel;
//打款人
@property (nonatomic, strong) UITextField *nameTextField;
//回款金额
@property (nonatomic, strong) UITextField *moneyTextField;
//确定按钮
@property (nonatomic, strong) UIButton *sureButton;

@property (nonatomic, strong) NSDictionary *dataDict;
//上传的图片
@property (nonatomic, strong) UIImage *chooseImage;
//图片地址
@property (nonatomic, copy)   NSString *picUrl;

@end

@implementation XPPayToCompanyController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupUI];
    [self getData];
}
-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"支付订货款";
    [self.view addSubview:self.navBarView];
}


- (void)setupUI{
    self.mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTopHeight,self.view.mj_w, SCREEN_HEIGHT - kTopHeight - self.safeBottom - 50 - 5)];
    [self.view addSubview:self.mainView];
    
    UILabel *tipLabel = [[UILabel alloc]init];
    tipLabel.frame = CGRectMake(24, 24, SCREEN_WIDTH, 24);
    tipLabel.text = @"温馨提示:";
    tipLabel.font = kFontMedium(16);
    tipLabel.textColor = app_font_color;
    [self.mainView addSubview:tipLabel];
    
    UILabel *tipDesLabel = [[UILabel alloc]init];
    tipDesLabel.frame = CGRectMake(24, 80, SCREEN_WIDTH - 24 *2, 0);
    tipDesLabel.font = kFontRegular(14);
    tipDesLabel.textColor = app_font_color;
    tipDesLabel.numberOfLines = NO;
    self.tipDesLabel = tipDesLabel;
    [self.mainView addSubview:tipDesLabel];
    
    UIView *bankInfoView = [UIView new];
    bankInfoView.layer.cornerRadius = 8;
    bankInfoView.backgroundColor = UIColor.whiteColor;
    bankInfoView.clipsToBounds = YES;
    bankInfoView.frame = CGRectMake(12, MaxY(self.tipDesLabel) + 20, self.view.mj_w - 24, 0);
    self.bankInfoView = bankInfoView;
    [self.mainView addSubview:bankInfoView];
    
    UILabel *label1 = [UILabel new];
    label1.frame = CGRectMake(12, 0, 0, 46);
    label1.font = kFontMedium(14);
    label1.textColor = app_font_color;
    self.label1 = label1;
    [self.bankInfoView addSubview:label1];
    
    UILabel *label2 = [UILabel new];
    label2.frame = CGRectMake(12, MaxY(self.label1), 0, 46);
    label2.font = kFontMedium(14);
    label2.textColor = app_font_color;
    self.label2 = label2;
    [self.bankInfoView addSubview:label2];
    
    
    UILabel *label3 = [UILabel new];
    label3.frame = CGRectMake(12, MaxY(self.label2), 0, 46);
    label3.font = kFontMedium(14);
    label3.textColor = app_font_color;
    self.label3 = label3;
    [self.bankInfoView addSubview:label3];
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.layer.cornerRadius = 4;
    button1.layer.borderWidth = 1;
    button1.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    button1.titleLabel.font = kFontRegular(16);
    [button1 setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    [button1 setTitle:@"复制" forState:UIControlStateNormal];
    button1.tag = 1;
    [button1 addTarget:self action:@selector(copybuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.button1 = button1;
    [self.bankInfoView addSubview:button1];
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.layer.cornerRadius = 4;
    button2.layer.borderWidth = 1;
    button2.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    button2.titleLabel.font = kFontRegular(16);
    [button2 setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    [button2 setTitle:@"复制" forState:UIControlStateNormal];
    button2.tag = 2;
    [button2 addTarget:self action:@selector(copybuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.button2 = button2;
    [self.bankInfoView addSubview:button2];
    
    UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
    button3.layer.cornerRadius = 4;
    button3.layer.borderWidth = 1;
    button3.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    button3.titleLabel.font = kFontRegular(16);
    [button3 setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    [button3 setTitle:@"复制" forState:UIControlStateNormal];
    button3.tag = 3;
    [button3 addTarget:self action:@selector(copybuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.button3 = button3;
    [self.bankInfoView addSubview:button3];
    
    UILabel *attentionLabel = [[UILabel alloc]init];
    attentionLabel.frame = CGRectMake(24, MaxY(self.bankInfoView) + 24, SCREEN_WIDTH - 48, 40);
    attentionLabel.text = @"注意事项:";
    attentionLabel.font = kFontMedium(16);
    attentionLabel.textColor = app_font_color;
    self.attentionLabel = attentionLabel;
    [self.mainView addSubview:attentionLabel];
    
    UILabel *attentionDescLabel = [[UILabel alloc]init];
    attentionDescLabel.frame = CGRectMake(24, MaxY(self.attentionLabel), SCREEN_WIDTH - 24 *2, 0);
    attentionDescLabel.font = kFontRegular(14);
    attentionDescLabel.textColor = app_font_color;
    attentionDescLabel.numberOfLines = NO;
    self.attentionDescLabel = attentionDescLabel;
    [self.mainView addSubview:attentionDescLabel];
    
    UILabel *uploadTitleLabel = [[UILabel alloc]init];
    uploadTitleLabel.frame = CGRectMake(24, MaxY(self.bankInfoView) + 12, SCREEN_WIDTH, 24);
    uploadTitleLabel.font = kFontMedium(16);
    uploadTitleLabel.textColor = app_font_color;
    self.uploadTitleLabel = uploadTitleLabel;
    [self.mainView addSubview:uploadTitleLabel];
    
    UIImageView *uploadImageView = [[UIImageView alloc] init];
    uploadImageView.image = [UIImage imageNamed:@"xp_payment_pic_add"];
    uploadImageView.clipsToBounds = YES;
    uploadImageView.layer.cornerRadius = 4;
    uploadImageView.userInteractionEnabled = YES;
    self.uploadImageView = uploadImageView;
    [self.mainView addSubview:uploadImageView];
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.uploadImageView addGestureRecognizer:tapGes];
    

    UIView *inputView = [UIView new];
    inputView.backgroundColor = UIColor.whiteColor;
    inputView.layer.cornerRadius = 8;
    inputView.frame = CGRectMake(12, MaxY(self.uploadImageView) + 12, self.view.mj_w - 24, 0);
    self.inputView = inputView;
    [self.mainView addSubview:inputView];
    
    NSArray *titleArray = @[@"*支付时间",@"*打款人",@"*汇款金额"];
    for (int i = 0; i< titleArray.count; i++) {
        UILabel *tempLabel = [UILabel new];
        tempLabel.frame = CGRectMake(12, 56 * i, 70, 56);
        tempLabel.font = kFontRegular(14);
        tempLabel.textColor = app_font_color;
        [self.inputView addSubview:tempLabel];
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:titleArray[i]];
        [attributedText addAttributes:@{NSForegroundColorAttributeName:app_font_color_FF4A4A} range:NSMakeRange(0, 1)];
        tempLabel.attributedText = attributedText;
    }
    
    CGFloat inputLeft = 92;
    UIButton *chooseButton = [UIButton buttonWithType: UIButtonTypeCustom];
    chooseButton.backgroundColor = UIColorFromRGB(0xF2F2F2);
    chooseButton.layer.cornerRadius = 4;
    chooseButton.frame = CGRectMake(inputLeft,12, self.view.mj_w - 24 - 98 , 40);
    [chooseButton addTarget:self action:@selector(chooseButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:chooseButton];
    
    self.payTimeLabel = [UILabel new];
    self.payTimeLabel.font = kFontRegular(14);
    self.payTimeLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.payTimeLabel.text = @"请选择支付时间";
    self.payTimeLabel.frame = CGRectMake(12, 0, chooseButton.mj_w - 28 - 2, chooseButton.mj_h);
    [chooseButton addSubview:self.payTimeLabel];
    
    UIImageView *arrow = [[UIImageView alloc] init];
    arrow.image = [UIImage imageNamed:@"xp_calendar"];
    arrow.frame = CGRectMake(chooseButton.mj_w - 28, 7, 24, 24);
    [chooseButton addSubview:arrow];
    
    UITextField *nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(inputLeft, MaxY(chooseButton) + 12,chooseButton.mj_w, chooseButton.mj_h)];
    nameTextField.placeholder = @"请输入打款人";
    nameTextField.font = kFontRegular(14);
    nameTextField.textColor = UIColorFromRGB(0x3A3C41);
    nameTextField.layer.cornerRadius = 4;
    nameTextField.backgroundColor = UIColorFromRGB(0xF2F2F2);
    nameTextField.leftViewMode = UITextFieldViewModeAlways;
    nameTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12, 0)];
    nameTextField.delegate = self;
    self.nameTextField = nameTextField;
    [self.inputView addSubview:nameTextField];
    
    UITextField *moneyTextField = [[UITextField alloc] initWithFrame:CGRectMake(inputLeft, MaxY(nameTextField) + 12,chooseButton.mj_w, chooseButton.mj_h)];
    moneyTextField.placeholder = @"请输入汇款金额";
    moneyTextField.delegate = self;
    moneyTextField.font = kFontMedium(14);
    moneyTextField.textColor = UIColorFromRGB(0x3A3C41);
    moneyTextField.leftViewMode = UITextFieldViewModeAlways;
    moneyTextField.keyboardType = UIKeyboardTypeDecimalPad;
    moneyTextField.layer.cornerRadius = 4;
    moneyTextField.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.moneyTextField = moneyTextField;
    [self.inputView addSubview:moneyTextField];
    
    UILabel *moneyLeftLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, moneyTextField.mj_h)];
    moneyLeftLabel.textAlignment = NSTextAlignmentRight;
    moneyLeftLabel.text = @"  ￥";
    moneyLeftLabel.font = moneyTextField.font;
    moneyLeftLabel.textColor = moneyTextField.textColor;
    self.moneyTextField.leftView = moneyLeftLabel;
    
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sureButton.frame = CGRectMake(12,SCREEN_HEIGHT - 50 - self.safeBottom, self.mainView.mj_w - 24, 50);
    [sureButton setTitle:@"确认转入" forState:UIControlStateNormal];
    sureButton.layer.cornerRadius = 25;
    sureButton.layer.masksToBounds = YES;
    sureButton.titleLabel.font = kFontMedium(16);
    [sureButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [sureButton addTarget:self action:@selector(sureButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [sureButton setBackgroundImage:[UIImage imageNamed:@"xp_login_btn"] forState:UIControlStateNormal];
    self.sureButton = sureButton;
    [self.view addSubview:sureButton];
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:@"*上传凭证"];
    [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color_FF4A4A} range:NSMakeRange(0, 1)];
    self.uploadTitleLabel.attributedText = attr;
    
    [self reloadUI];
}

- (void)reloadUI {
    
    self.tipDesLabel.text = @"    请各位用户使用自己已签约绑定银行卡的个人网银 或手机银行转账汇款至平台账户支付购货订金(必须用已签约的银行卡操作)";
    CGFloat tipH = [self.tipDesLabel.text boundingRectWithSize:CGSizeMake(self.tipDesLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.tipDesLabel.font} context:nil].size.height;
    self.tipDesLabel.frame = CGRectMake(24, 50, SCREEN_WIDTH - 24 *2, tipH);

    NSString *nameStr = [NSString stringWithFormat:@"账户名称: %@",self.dataDict[@"accountName"]];
    CGFloat nameW = [nameStr sizeWithAttributes:@{NSFontAttributeName:self.label1.font}].width;
    self.label1.text = nameStr;
    self.label1.frame = CGRectMake(12, 0, nameW, 46);
    self.button1.frame = CGRectMake(MaxX(self.label1)+ 8, 11, 48, 24);
    
    NSString *accountStr = [NSString stringWithFormat:@"账户号: %@",self.dataDict[@"accountNumber"]];
    CGFloat accountW = [accountStr sizeWithAttributes:@{NSFontAttributeName:self.label1.font}].width;
    self.label2.text= accountStr;
    self.label2.frame = CGRectMake(12, 46, accountW, 46);
    self.button2.frame = CGRectMake(MaxX(self.label2)+ 8, MaxY(self.label1) + 11, 48, 24);
    
    NSString *bankStr = [NSString stringWithFormat:@"开户银行: %@",self.dataDict[@"bank"]];
    CGFloat bankW = [bankStr sizeWithAttributes:@{NSFontAttributeName:self.label1.font}].width;
    self.label3.text = bankStr;
    self.label3.frame = CGRectMake(12, 92, bankW, 46);
    self.button3.frame = CGRectMake(MaxX(self.label3) + 8, MaxY(self.label2) + 11, 48, 24);
    
    self.bankInfoView.frame = CGRectMake(12, MaxY(self.tipDesLabel) + 20, self.view.mj_w - 24, 136);
    self.attentionLabel.frame = CGRectMake(24, MaxY(self.bankInfoView) + 12, SCREEN_WIDTH, 24);
    
    self.attentionDescLabel.text= @"  1、请您确保已签约绑定银行卡，再向卖家账户转账，否则购货订金无法到达账户显示。\n  2、支付完成后，购货订金会到达您账户并显示，如有疑问，请联系客服微信 xptt888。";
    
    CGFloat attentionH = [self.attentionDescLabel.text boundingRectWithSize:CGSizeMake(self.attentionDescLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.attentionDescLabel.font} context:nil].size.height;

    self.attentionDescLabel.frame = CGRectMake(24, MaxY(self.attentionLabel), SCREEN_WIDTH - 24 *2, attentionH);
    
    self.uploadTitleLabel.frame = CGRectMake(24, MaxY(self.attentionDescLabel) + 24, 200, 24);
    
    self.uploadImageView.frame = CGRectMake(24, MaxY(self.uploadTitleLabel) + 12, 100, 100);
    
    self.inputView.frame = CGRectMake(12, MaxY(self.uploadImageView) + 24, self.mainView.mj_w - 24, 168);
    
    self.mainView.contentSize = CGSizeMake(self.view.mj_w, MaxY(self.inputView) + 24);
    
    self.sureButton.frame = CGRectMake(12,SCREEN_HEIGHT - 50 - self.safeBottom, self.mainView.mj_w - 24, 50);
}

#pragma mark -- netWork request

- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_bank_company_rechargeInfo];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.dataDict = data.userData;
            [self reloadUI];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)submitMoneyInfo:(NSDictionary *)parM {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_bank_company_rechargeSubmit];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:parM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:@"提交成功" ToView:k_keyWindow];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

#pragma mark -- buttonAction

- (void)sureButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.chooseImage == nil) {
        [[XPShowTipsTool shareInstance] showMsg:@"请上传汇款截图" ToView:k_keyWindow];
    }else if ([self.payTimeLabel.text containsString:@"请选择"]){
        [[XPShowTipsTool shareInstance] showMsg:@"请选择支付时间" ToView:k_keyWindow];
    }else if ([self.nameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0){
        [[XPShowTipsTool shareInstance] showMsg:@"请输入打款人" ToView:k_keyWindow];
    }else if (self.moneyTextField.text.length == 0 || [self.moneyTextField.text floatValue] == 0){
        [[XPShowTipsTool shareInstance] showMsg:@"请输入汇款金额" ToView:k_keyWindow];
    }else {
        NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
        paraM[@"transferMoney"] = self.moneyTextField.text;
        paraM[@"transferTime"] = [NSString stringWithFormat:@"%@:00",self.payTimeLabel.text];
        paraM[@"pic"] = self.picUrl;
        paraM[@"transferUser"] = self.nameTextField.text;
        paraM[@"accountName"] = self.dataDict[@"accountName"];
        paraM[@"accountNo"] = self.dataDict[@"accountNumber"];
        paraM[@"accountBank"] = self.dataDict[@"bank"];
        
        [self submitMoneyInfo:paraM.copy];
    }
}

- (void)tap:(UITapGestureRecognizer *)ges {
    buttonCanUseAfterOneSec(ges.view);
    [self showAlertWithMsg:nil];
}
//复制按钮点击
- (void)copybuttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    if (sender.tag == 1) {
        pasteboard.string = self.dataDict[@"accountName"];
    }else if (sender.tag == 2){
        pasteboard.string = self.dataDict[@"accountNumber"];
    }else if (sender.tag == 3){
        pasteboard.string = self.dataDict[@"bank"];
    }
    [[XPShowTipsTool shareInstance] showMsg:@"复制成功" ToView:k_keyWindow];
}

- (void)chooseButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPWeakSelf;
    BRDatePickerView *pickView = [[BRDatePickerView alloc] initWithPickerMode:BRDatePickerModeYMDHM];
    [pickView show];
    [pickView setResultBlock:^(NSDate * _Nullable selectDate, NSString * _Nullable selectValue) {
        weakSelf.payTimeLabel.text = selectValue;
    }];
}

#pragma mark -- 图片上传
//上传图片
-(void)showAlertWithMsg:(NSString *)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       
    }];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openCamera];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openAlbum];
    }];
    [alert addAction:cancel];
    [alert addAction:action];
    [alert addAction:action2];

    [self presentViewController:alert animated:YES completion:nil];
}
- (void)openCamera {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}
- (void)openAlbum {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:true completion:^{
        
    }];
    NSString *type  = [info objectForKey:UIImagePickerControllerMediaType];
    //选择的类型图片
    if ([type isEqualToString:@"public.image"]) {
        //先把图片转成NSData
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        self.chooseImage = [image compressToImage];
    }
    [picker dismissViewControllerAnimated:YES completion:^{
        [self uploadCardWithImage:self.chooseImage];
    }];
  
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)uploadCardWithImage:(UIImage *)iConImage {
    
    iConImage = [iConImage compressToImage];
    NSString *filePath = [[XPOSSUploadTool shareInstance] saveImageToSandbox:iConImage];
    [self startLoadingGif];
    [[XPOSSUploadTool shareInstance] uploadFileWithMediaType:XPMediaTypeMoneyRecharge filePath:filePath progress:^(CGFloat progress) {
        
    } completeBlock:^(BOOL success, NSString * _Nonnull remoteUrlStr) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self stopLoadingGif];
            if (success) {
                //修改用户名
                self.uploadImageView.image = self.chooseImage;
                self.picUrl = remoteUrlStr;
            }else{
                [[XPShowTipsTool shareInstance] showMsg:@"上传失败,请稍后重试!" ToView:k_keyWindow];
            }
        });
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

#pragma mark --- textfield Delegate
//限制只能输入金额
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.moneyTextField) {
        NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        //限制.后面最多有两位，且不能再输入.
        if ([textField.text rangeOfString:@"."].location != NSNotFound) {
            //有.了 且.后面输入了两位  停止输入
            if (toBeString.length > [toBeString rangeOfString:@"."].location+3) {
                return NO;
            }
            //有.了，不允许再输入.
            if ([string isEqualToString:@"."]) {
                return NO;
            }
        }
        
        //限制首位0，后面只能输入. 或 删除
        if ([textField.text isEqualToString:@"0"]) {
            if (!([string isEqualToString:@"."] || [string isEqualToString:@""])) {
                return NO;
            }
        }
        
        //首位. 前面补全0
        if ([toBeString isEqualToString:@"."]) {
            textField.text = @"0";
            return YES;
        }
        
        //限制只能输入：1234567890.
        NSCharacterSet * characterSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890."] invertedSet];
        NSString * filtered = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }else if (textField == self.nameTextField) {
        
        if ([XPSwiftTool.new containsEmoji:string]) {
            return NO;
        }else {
            return YES;
        }
    }
    return YES;
    
}
@end
