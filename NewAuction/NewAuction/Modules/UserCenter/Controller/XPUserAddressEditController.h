//
//  XPUserAddressEditController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/13.
//

#import "XPBaseViewController.h"
#import "XPAdressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPUserAddressEditController : XPBaseViewController

@property (nonatomic, strong) XPAdressModel *model;
//编辑 1   添加 2
@property (nonatomic, assign) NSInteger enterType;

@end

NS_ASSUME_NONNULL_END
