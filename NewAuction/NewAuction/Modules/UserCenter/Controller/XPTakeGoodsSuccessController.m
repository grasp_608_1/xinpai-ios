//
//  XPTakeGoodsSuccessController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/21.
//

#import "XPTakeGoodsSuccessController.h"
#import "XPMyStoreHouseViewController.h"
#import "XPTakeGoodsDetailController.h"
#import "XPShopingController.h"

@interface XPTakeGoodsSuccessController ()

@end

@implementation XPTakeGoodsSuccessController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    [self setupUI];
    
}

- (void)setupUI {
    UIImageView *successLogo = [UIImageView new];
    successLogo.image = [UIImage imageNamed:@"Group_67"];
    successLogo.frame = CGRectMake(self.view.mj_w/2 - 20, kTopHeight + 100, 40, 40);
    [self.view addSubview:successLogo];
    
    
    UILabel *successLabel = [UILabel new];
    successLabel.text = @"提货成功";
    successLabel.textColor = UIColorFromRGB(0x3A3C41);
    successLabel.font = kFontRegular(16);
    successLabel.textAlignment = NSTextAlignmentCenter;
    successLabel.frame = CGRectMake(0, MaxY(successLogo) + 12, self.view.mj_w, 24);
    [self.view addSubview:successLabel];
    
    
    UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    detailButton.frame = CGRectMake(self.view.mj_w/2. - 90,MaxY(successLabel) + 100, 180, 50);
    [detailButton setTitle:@"查看订单详情" forState:UIControlStateNormal];
    detailButton.titleLabel.font = kFontMedium(16);
    [detailButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    detailButton.layer.cornerRadius = 25;
    detailButton.clipsToBounds = YES;
    detailButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
    [detailButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:detailButton];
    
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(self.view.mj_w/2. - 90,MaxY(detailButton), 180, 50);
    [backButton setTitle:@"返 回" forState:UIControlStateNormal];
    backButton.titleLabel.font = kFontMedium(16);
    [backButton setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton]; 
}


- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPTakeGoodsDetailController *vc = [XPTakeGoodsDetailController new];
    vc.goodsNum = self.goodsNum;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)backButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    [self goBack];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    self.navBarView.backgroundColor = UIColor.whiteColor;
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"提货";
    [self.view addSubview:self.navBarView];
}

- (void)goBack {
    if ([self containsShopingController]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        NSUInteger index = [[self.navigationController viewControllers]indexOfObject:self];
        if (index >= 2) {
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:index-2]animated:YES];
        }
    }
}

- (BOOL)containsShopingController{
    NSArray *controllers = self.navigationController.viewControllers;
    BOOL contains = NO;
    for (int i = 0; i<controllers.count; i++) {
        UIViewController *controler = controllers[i];
        if ([controler isKindOfClass:[XPShopingController class]]) {
            contains = YES;
            break;
        }
    }
    return contains;
    
}
//防止侧滑返回
- (void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}


@end
