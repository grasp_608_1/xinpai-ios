//
//  XPTakeGoodsDetailController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/21.
//

#import "XPTakeGoodsDetailController.h"
#import "XPPickGoodsModel.h"
#import "XPMyStoreHouseViewController.h"
#import "XPTakeGoodsSuccessController.h"
#import "XPPickGoodsManagerController.h"
#import "XPLogisticsInfoController.h"
#import "XPGoodManagerCell.h"
#import "XPShopingController.h"


@interface XPTakeGoodsDetailController ()

@property (nonatomic, strong) UIScrollView *mainView;

@property (nonatomic, strong) UIView *locationView;
@property (nonatomic, strong) UIView *midView;
@property (nonatomic, strong) UIView *downView;
@property (nonatomic, strong) UILabel *userInfoLabel;
@property (nonatomic, strong) UILabel *phoneSerectLabel;
@property (nonatomic, strong) UILabel *adressLabel;
@property (nonatomic, strong) UIImageView *rightArrow;

@property (nonatomic, strong) UIImageView *adressImageView;

@property (nonatomic, strong) UIImageView *pImageView;
@property (nonatomic, strong) UILabel *pTitleLabel;
@property (nonatomic, strong) UILabel *pPriceLabel;
@property (nonatomic, strong) UILabel *sendePriceLabel;
@property (nonatomic, strong) UILabel *payWayLabel;
@property (nonatomic, strong) UILabel *numberLabel;
//补差价
@property (nonatomic, strong) UILabel *extraMoneyLabel;
//商品总价
@property (nonatomic, strong) UILabel *totalPriceLabel;
//快递单号
@property (nonatomic, strong) UILabel *orderNumLabel;
//发货时间
@property (nonatomic, strong) UILabel *sendTimeLabel;
//收货时间
@property (nonatomic, strong) UILabel *getTimeLabel;

@property (nonatomic, strong) UILabel *orderLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIButton *saveButton;

@property (nonatomic, strong) XPPickGoodsModel *model;

@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UILabel *statusDesLabel;
@end

@implementation XPTakeGoodsDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupViews];
}

- (void)setupViews {
    
    self.mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.view.mj_w, SCREEN_HEIGHT - kTopHeight)];
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self.view addSubview:self.mainView];
    
    UIButton *statusView = [UIButton new];
    statusView.frame = CGRectMake(12,12, self.view.mj_w - 24, 72);
    statusView.backgroundColor = UIColor.whiteColor;
    statusView.layer.cornerRadius = 8;
    statusView.layer.masksToBounds = YES;
    [statusView addTarget:self action:@selector(locationClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:statusView];
    
    UIImageView *carLogo = [UIImageView new];
    carLogo = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, 48, 48)];
    carLogo.image = [UIImage imageNamed:@"xp_car"];
    [statusView addSubview:carLogo];
    
    UILabel *statusLabel = [UILabel new];
    statusLabel.textColor = UIColorFromRGB(0x3A3C41);
    statusLabel.font = kFontMedium(16);
    statusLabel.frame = CGRectMake(MaxX(carLogo) + 12, 12, statusView.mj_w - MaxX(carLogo) - 12 - 12 - 24 - 5, 24);
    self.statusLabel = statusLabel;
    [statusView addSubview:statusLabel];
    
    UILabel *statusTipLabel = [UILabel new];
    statusTipLabel.textColor = UIColorFromRGB(0x8A8A8A);
    statusTipLabel.font = kFontRegular(14);
    statusTipLabel.frame = CGRectMake(statusLabel.mj_x, MaxY(statusLabel), statusLabel.mj_w, 24);
    self.statusDesLabel = statusTipLabel;
    [statusView addSubview:statusTipLabel];
    
    UIImageView *arrowImageView = [UIImageView new];
    arrowImageView.image = [UIImage imageNamed:@"xp_setting_arrow"];
    arrowImageView.frame = CGRectMake(statusView.mj_w - 12 -24, 24, 24, 24);
    [statusView addSubview:arrowImageView];
    
    
    UIView *locationView = [UIView new];
    locationView.backgroundColor = UIColor.whiteColor;
    locationView.layer.cornerRadius = 8;
    self.locationView = locationView;
    [self.mainView addSubview:locationView];
    
    UIImageView *locationLogo = [UIImageView new];
    locationLogo.image = [UIImage imageNamed:@"xp_location"];
    self.adressImageView = locationLogo;
    [locationView addSubview:locationLogo];
    
    UILabel *userInfoLabel = [UILabel new];
    userInfoLabel.textColor = UIColorFromRGB(0x3A3C41);
    userInfoLabel.font = kFontMedium(16);
    self.userInfoLabel = userInfoLabel;
    [locationView addSubview:userInfoLabel];
    
    self.phoneSerectLabel = [UILabel new];
    self.phoneSerectLabel.font = kFontRegular(12);
    self.phoneSerectLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.phoneSerectLabel.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
    self.phoneSerectLabel.layer.borderWidth = 1;
    self.phoneSerectLabel.textAlignment = NSTextAlignmentCenter;
    [locationView addSubview:self.phoneSerectLabel];
    
    UILabel *adressLabel = [UILabel new];
    adressLabel.textColor = UIColorFromRGB(0x8A8A8A);
    adressLabel.numberOfLines = NO;
    adressLabel.font = kFontRegular(14);
    self.adressLabel = adressLabel;
    [locationView addSubview:adressLabel];
        
    UIView *midView = [UIView new];
    midView.backgroundColor = UIColor.whiteColor;
    midView.layer.cornerRadius = 8;
    midView.layer.masksToBounds = YES;
    midView.frame = CGRectMake(12, 0, self.view.mj_w - 12 *2, 0);
    self.midView = midView;
    [_mainView addSubview:midView];
    
    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(12, 12, 100, 100);
    [midView addSubview:_pImageView];
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontMedium(14);
    self.pTitleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.pTitleLabel.numberOfLines = 2;
    self.pTitleLabel.frame = CGRectMake(124, self.pImageView.mj_y,midView.mj_w - 124 - 12, 24);
    [midView addSubview:_pTitleLabel];
    
    self.numberLabel = [UILabel new];
    self.numberLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.numberLabel.font = kFontRegular(14);
    self.numberLabel.frame = CGRectMake(self.pTitleLabel.mj_x, 12 + MaxY(self.pTitleLabel), midView.mj_w/2. -12, 24);
    [midView addSubview:self.numberLabel];
    
    self.pPriceLabel = [UILabel new];
    self.pPriceLabel.font = kFontRegular(14);
    self.pPriceLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.pPriceLabel.frame = CGRectMake(self.pTitleLabel.mj_x, MaxY(self.numberLabel) + 6, self.pTitleLabel.mj_w, 24);
    [midView addSubview:self.pPriceLabel];
    
    UIView *sepView = [UIView new];
    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    sepView.frame = CGRectMake(12, MaxY(_pImageView) + 12, midView.mj_w - 24, 1);
    [midView addSubview:sepView];
    
    NSArray *titleArray = @[@"商品总价",@"商品补差价",@"支付方式",@"运费/包装费",@"快递单号",@"发货时间",@"收货时间"];
    for (int i = 0; i<titleArray.count ; i++) {
        UILabel *tempLabel = [UILabel new];
        tempLabel.text = titleArray[i];
        tempLabel.textColor = UIColorFromRGB(0x8A8A8A);
        tempLabel.font = kFontRegular(14);
        tempLabel.frame = CGRectMake(12, MaxY(sepView) + 12 + i *(12 + 24), 80, 24);
        [midView addSubview:tempLabel];
    }
    
    UILabel *totalPriceLabel = [UILabel new];
    totalPriceLabel.textColor = UIColorFromRGB(0x3A3C41);
    totalPriceLabel.font = kFontMedium(16);
    totalPriceLabel.textAlignment = NSTextAlignmentRight;
    totalPriceLabel.frame = CGRectMake(midView.mj_w/2., 12 + MaxY(sepView), midView.mj_w/2. -12, 24);
    self.totalPriceLabel = totalPriceLabel;
    [midView addSubview:totalPriceLabel];
    
    UILabel *extraMoneyLabel = [UILabel new];
    extraMoneyLabel.textColor = UIColorFromRGB(0x3A3C41);
    extraMoneyLabel.font = kFontMedium(14);
    extraMoneyLabel.textAlignment = NSTextAlignmentRight;
    extraMoneyLabel.frame = CGRectMake(midView.mj_w/2., MaxY(self.self.totalPriceLabel) + 12, midView.mj_w/2. -12, 24);
    self.extraMoneyLabel = extraMoneyLabel;
    [midView addSubview:extraMoneyLabel];
    
    UILabel *payWayLabel = [UILabel new];
    payWayLabel.font = kFontRegular(14);
    payWayLabel.textColor = UIColorFromRGB(0x8A8A8A);
    payWayLabel.frame = CGRectMake(midView.mj_w/2., MaxY(self.self.extraMoneyLabel) + 12, midView.mj_w/2. -12, 24);
    payWayLabel.textAlignment = NSTextAlignmentRight;
    self.payWayLabel = payWayLabel;
    [midView addSubview:payWayLabel];
    
    UILabel *sendePriceLabel = [UILabel new];
    sendePriceLabel.font = kFontRegular(14);
    sendePriceLabel.textColor = UIColorFromRGB(0x8A8A8A);
    sendePriceLabel.frame = CGRectMake(midView.mj_w/2., MaxY(self.payWayLabel) + 12, midView.mj_w/2. -12, 24);
    sendePriceLabel.textAlignment = NSTextAlignmentRight;
    self.sendePriceLabel = sendePriceLabel;
    [midView addSubview:sendePriceLabel];
    
    
    UIButton *pastPostButton = [[UIButton alloc] init];
    [pastPostButton setTitle:@"复制" forState:UIControlStateNormal];
    pastPostButton.titleLabel.font = kFontRegular(14);
    [pastPostButton setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
    pastPostButton.frame = CGRectMake(self.midView.mj_w - 46,MaxY(self.sendePriceLabel) + 12, 46,24);
    [pastPostButton addTarget:self action:@selector(pasteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    pastPostButton.tag = 1;
    [midView addSubview:pastPostButton];
    
    UIView *pastSepView2 = [UIView new];
    pastSepView2.frame = CGRectMake(midView.mj_w - 47,pastPostButton.center.y - 8, 1, 16);
    pastSepView2.backgroundColor = UIColorFromRGB(0xDEDEDE);
    [midView addSubview: pastSepView2];
    
    UILabel *orderNumLabel = [UILabel new];
    orderNumLabel.font = kFontRegular(14);
    orderNumLabel.textColor = UIColorFromRGB(0x8A8A8A);
    orderNumLabel.frame = CGRectMake(100, MaxY(self.sendePriceLabel) + 12, midView.mj_w - 100 - 47 - 6 , 24);
    orderNumLabel.textAlignment = NSTextAlignmentRight;
    self.orderNumLabel = orderNumLabel;
    [midView addSubview:orderNumLabel];
    
    UILabel *sendTimeLabel = [UILabel new];
    sendTimeLabel.font = kFontRegular(14);
    sendTimeLabel.textColor = UIColorFromRGB(0x8A8A8A);
    sendTimeLabel.frame = CGRectMake(midView.mj_w/2., MaxY(self.orderNumLabel) + 12, midView.mj_w/2. -12, 24);
    sendTimeLabel.textAlignment = NSTextAlignmentRight;
    self.sendTimeLabel = sendTimeLabel;
    [midView addSubview:sendTimeLabel];
    
    UILabel *getTimelabel = [UILabel new];
    getTimelabel.font = kFontRegular(14);
    getTimelabel.textColor = UIColorFromRGB(0x8A8A8A);
    getTimelabel.frame = CGRectMake(midView.mj_w/2., MaxY(self.sendTimeLabel) + 12, midView.mj_w/2. -12, 24);
    getTimelabel.textAlignment = NSTextAlignmentRight;
    self.getTimeLabel = getTimelabel;
    [midView addSubview:getTimelabel];
    
    //设置高度
    UIView *downView = [UIView new];
    downView.backgroundColor = UIColor.whiteColor;
    downView.layer.cornerRadius = 8;
    downView.clipsToBounds = YES;
    downView.frame = CGRectMake(0, 0, self.view.mj_w - 24, 0);
    self.downView = downView;
    [self.mainView addSubview:downView];
    
    UILabel *label6 = [UILabel new];
    label6.textColor = UIColorFromRGB(0x8A8A8A);
    label6.font = kFontRegular(14);
    label6.frame = CGRectMake(12, 0, 100, 42);
    label6.text = @"提货编号";
    [downView addSubview:label6];
    
    UILabel *label7 = [UILabel new];
    label7.textColor = UIColorFromRGB(0x8A8A8A);
    label7.font = kFontRegular(14);
    label7.frame = CGRectMake(12, MaxY(label6), 100, 42);
    label7.text = @"提货时间";
    [downView addSubview:label7];
    
    UILabel *label8 = [UILabel new];
    label8.textColor = UIColorFromRGB(0x8A8A8A);
    label8.font = kFontRegular(14);
    label8.textAlignment = NSTextAlignmentRight;
    label8.frame = CGRectMake(112, 0, self.downView.mj_w - 112 - 53, 42);
    label8.text = @"";
    label8.textAlignment = NSTextAlignmentRight;
    self.orderLabel = label8;
    [downView addSubview:label8];
    
    UIButton *pastButton = [[UIButton alloc] init];
    [pastButton setTitle:@"复制" forState:UIControlStateNormal];
    pastButton.titleLabel.font = kFontRegular(14);
    [pastButton setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
    pastButton.frame = CGRectMake(self.downView.mj_w - 46, 0, 46, 42);
    [pastButton addTarget:self action:@selector(pasteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    pastButton.tag = 2;
    [downView addSubview:pastButton];
    
    UILabel *label9 = [UILabel new];
    label9.textColor = UIColorFromRGB(0x8A8A8A);
    label9.font = kFontRegular(14);
    label9.frame = CGRectMake(112, 42, self.downView.mj_w - 112 - 12, 42);
    label9.text = @"";
    label9.textAlignment = NSTextAlignmentRight;
    self.timeLabel = label9;
    [downView addSubview:label9];
    
    UIView *pastSepView = [UIView new];
    pastSepView.frame = CGRectMake( self.downView.mj_w - 47,12, 1, 16);
    pastSepView.backgroundColor = UIColorFromRGB(0xDEDEDE);
    [downView addSubview: pastSepView];
}



- (void)freshUIWithModel:(XPPickGoodsModel *)model {
    self.model = model;
    
    self.statusLabel.text = [XPOrderDataTool getStatusTitle:[NSString stringWithFormat:@"%@",model.orderStatus]];
    self.statusDesLabel.text = @"您的货品正在运输中，请耐心等待";
    self.locationView.frame = CGRectMake(12,96, self.mainView.mj_w - 24,0);
    
    self.adressImageView.frame = CGRectMake(12,12, 24, 24);
    NSString *userPhone = model.phone.length >= 11 ? [model.phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"] : model.phone;
    self.userInfoLabel.text = [NSString stringWithFormat:@"%@ %@",model.name,userPhone];
    CGFloat userInfoW = [self.userInfoLabel.text sizeWithAttributes:@{NSFontAttributeName:self.userInfoLabel.font}].width;
    self.userInfoLabel.frame = CGRectMake(48, 12, userInfoW + 12, 24);
    self.phoneSerectLabel.text = @"号码保护中";
    self.phoneSerectLabel.frame = CGRectMake(MaxX(self.userInfoLabel), 15, 72, 18);
    
    self.adressLabel.text = [NSString stringWithFormat:@"%@%@",[model.area stringByReplacingOccurrencesOfString:@"," withString:@""],model.address];
    CGFloat addressW  = self.locationView.mj_w - 48 - 12;
    CGFloat addressH = [self.adressLabel.text boundingRectWithSize:CGSizeMake(addressW, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.adressLabel.font} context:nil].size.height;
    
    self.adressLabel.frame = CGRectMake(48, MaxY(self.userInfoLabel) + 6, self.locationView.mj_w - 48 - 12, addressH);
    self.locationView.frame = CGRectMake(12,96, self.mainView.mj_w - 24, MaxY(self.adressLabel) + 12);
    
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    self.pTitleLabel.text = model.itemName;
    
    NSString *amountStr = [NSString stringWithFormat:@"%@",model.amount];
    NSString *amoutTempStr = [NSString stringWithFormat:@"提货数量 %@",amountStr];
    NSMutableAttributedString *amoutAttr = [[NSMutableAttributedString alloc] initWithString:amoutTempStr];
    [amoutAttr addAttributes:@{NSForegroundColorAttributeName:app_font_color,NSFontAttributeName:kFontMedium(14)}
                       range:NSMakeRange(amoutTempStr.length - amountStr.length,amountStr.length)];
    
    self.numberLabel.attributedText = amoutAttr;
    
    
    NSString *priceStr = [NSString stringWithFormat:@"提货单价 ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.price?:@(0)]];
    NSString *priceNum = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:model.price]];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
    [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color,NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(priceStr.length - 1 - priceNum.length, 1 + priceNum.length)];
    self.pPriceLabel.attributedText = attr;
    //text = [NSString stringWithFormat:@"提货单价 %@￥",[XPMoneyFormatTool formatMoneyWithNum:model.price]];
    
    self.extraMoneyLabel.text = [NSString stringWithFormat:@"￥%@",model.priceDifference?[XPMoneyFormatTool formatMoneyWithNum:model.priceDifference]:@(0)];
    
    self.payWayLabel.text = @"收入支付";
    
    self.totalPriceLabel.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.productPriceTotal]];
    if (model.isPostage.intValue == 1) {
        self.sendePriceLabel.text = @"包邮";
    }else{
        self.sendePriceLabel.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.mailPriceTotal]];
    }
    if (!model.expressNo) {
        self.midView.frame = CGRectMake(12, MaxY(self.locationView) + 12, self.mainView.mj_w - 24, MaxY(self.sendePriceLabel) + 12);
    }else {
        self.orderNumLabel.text = model.expressNo;
        if (!model.expressTime) {
            self.midView.frame = CGRectMake(12, MaxY(self.locationView) + 12, self.mainView.mj_w - 24, MaxY(self.orderNumLabel) + 12);
        }else {
            self.sendTimeLabel.text = model.expressTime;
            if (!model.receiptTime) {
                self.midView.frame = CGRectMake(12, MaxY(self.locationView) + 12, self.mainView.mj_w - 24, MaxY(self.sendTimeLabel) + 12);
            }else {
                self.getTimeLabel.text = model.receiptTime;
                self.midView.frame = CGRectMake(12, MaxY(self.locationView) + 12, self.mainView.mj_w - 24, MaxY(self.getTimeLabel) + 12);
            }
        }
    }
    
    self.orderLabel.text = [NSString stringWithFormat:@"%@",model.orderNo];
    self.timeLabel.text = [NSString stringWithFormat:@"%@",model.createTime];
    
    self.downView.frame = CGRectMake(12, MaxY(self.midView) + 12, self.view.mj_w - 24, 84);
    
    self.mainView.contentSize = CGSizeMake(self.view.mj_w, MaxY(self.downView) + 17 + kBottomHeight);
}

- (void)pasteButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    if (sender.tag == 1) {
        pasteboard.string = self.orderNumLabel.text;
    }else if (sender.tag == 2) {
        pasteboard.string = self.orderLabel.text;
    }
    [[XPShowTipsTool shareInstance] showMsg:@"复制成功!" ToView:k_keyWindow];
    
    
}

- (void)locationClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.model.expressNo) {
        XPLogisticsInfoController *vc = [XPLogisticsInfoController new];
        vc.orderId = self.model.expressNo;
        vc.tplCode = self.model.code100;
        vc.phone = self.model.phone;
        vc.orderStatus = [NSString stringWithFormat:@"%@",self.model.orderStatus];
        vc.expressCompany = self.model.expressCompany;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        [[XPShowTipsTool shareInstance] showMsg:@"暂无物流信息" ToView:k_keyWindow];
    }
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    self.navBarView.backgroundColor = UIColor.whiteColor;
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"订单详情";
    [self.view addSubview:self.navBarView];
}
- (void)goBack {
    
    if ([self containsShopingController]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        NSArray *vcArray = self.navigationController.viewControllers;
        
        if(vcArray.count > 3 && [self containsSuccessViewController]){//从提货成功页跳转过来的
            NSUInteger index = [[self.navigationController viewControllers]indexOfObject:self];
            if (index >= 3) {
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:index-3]animated:YES];
            }
            
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (BOOL)containsSuccessViewController{
    NSArray *controllers = self.navigationController.viewControllers;
    BOOL contains = NO;
    for (int i = 0; i<controllers.count; i++) {
        UIViewController *controler = controllers[i];
        if ([controler isKindOfClass:[XPTakeGoodsSuccessController class]]) {
            contains = YES;
            break;
        }
    }
    return contains;
    
}

- (BOOL)containsShopingController{
    NSArray *controllers = self.navigationController.viewControllers;
    BOOL contains = NO;
    for (int i = 0; i<controllers.count; i++) {
        UIViewController *controler = controllers[i];
        if ([controler isKindOfClass:[XPShopingController class]]) {
            contains = YES;
            break;
        }
    }
    return contains;
    
}

- (void)getData {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kProduct_store_pick_detail];    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = self.goodsNum;
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             XPPickGoodsModel *model = [XPPickGoodsModel mj_objectWithKeyValues:data.userData];
             [self freshUIWithModel:model];
        
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
}

//防止侧滑返回
- (void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    [self getData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

@end
