//
//  XPLogisticsInfoController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/19.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPLogisticsInfoController : XPBaseViewController
@property (nonatomic, copy) NSString *orderStatus;
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *tplCode;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *expressCompany;


@end

NS_ASSUME_NONNULL_END
