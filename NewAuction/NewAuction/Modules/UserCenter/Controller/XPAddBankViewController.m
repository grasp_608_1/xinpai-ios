//
//  XPAddBankViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/17.
//

#import "XPAddBankViewController.h"
#import "NSTimer+JKBlocks.h"
#import <Photos/Photos.h>
#import <AVFoundation/AVCaptureDevice.h>
#import <AVFoundation/AVMediaFormat.h>
#import "AFNetworking.h"
#import "XPUploadIconView.h"
#import "UIImage+Compression.h"


@interface XPAddBankViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>

@property (nonatomic, strong) UIScrollView *mainView;
@property (nonatomic, strong) UIButton *codeButton;
@property (nonatomic, strong) UIView *manualView;
@property (nonatomic, strong) UIView *autoShowView;
@property (nonatomic, strong) UIButton *autoAdd;

@property (nonatomic, strong) UITextField *textField1;
@property (nonatomic, strong) UITextField *textField2;
@property (nonatomic, strong) UITextField *textField3;
@property (nonatomic, strong) UITextField *textField4;
@property (nonatomic, strong) UITextField *textField5;

@property (nonatomic, copy) NSString *codeUUid;

@property (nonatomic, strong) UIImage *photo;

@property (nonatomic, strong) UIButton *idCardButton;

@property (nonatomic, strong) UILabel *bankNameLabel;
//计时器
@property(nonatomic,strong)NSTimer * oneMinTimer;

@property (nonatomic, strong) XPUploadIconView *uploadView;

@property (nonatomic, copy) NSString *cardType;

@property (nonatomic, strong) UIButton *nextButton;

@property (nonatomic, strong) NSString *applyId;

@property (nonatomic, strong) NSString *HFCode;

@property (nonatomic, strong) UITextField *applyCodeTextField;
//服务器ocr返回结果查下银行卡信息
@property (nonatomic, copy) id responseObject;

@end

@implementation XPAddBankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =  UIColorFromRGB(0xFAFAFA);
    
    
    [self mannualAdd];
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:self.navBarView];
    self.navBarView.backgroundColor = UIColor.clearColor;
    if (self.enterType == 0) {
        self.navBarView.titleLabel.text = @"添加支付卡";
    }else {
        self.navBarView.titleLabel.text = @"添加结算卡";
    }
}

- (void)mannualAdd {
    
    UIButton *autoAdd = [UIButton new];
    autoAdd.backgroundColor = UIColor.whiteColor;
    autoAdd.layer.cornerRadius = 4;
    autoAdd.layer.masksToBounds = YES;
    autoAdd.frame = CGRectMake(12, kTopHeight + 12, SCREEN_WIDTH - 24 , 56);
    [autoAdd addTarget:self action:@selector(autoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.autoAdd = autoAdd;
    [self.view addSubview:autoAdd];
    
    UILabel *autoTileLabel = [UILabel new];
    autoTileLabel.frame = CGRectMake(12, 0,200, 55);
    autoTileLabel.text = @"上传银行卡图片";
    autoTileLabel.font = kFontRegular(14);
    autoTileLabel.textColor = UIColorFromRGB(0x3A3C41);
    autoTileLabel.frame = CGRectMake(12, 0, 200, autoAdd.mj_h);
    [autoAdd addSubview:autoTileLabel];
    
    UIImageView *camera = [[UIImageView alloc] initWithFrame:CGRectMake(autoAdd.mj_w -16 - 24 , 16, 24, 24)];
    camera.image = [UIImage imageNamed:@"xp_camera"];
    [autoAdd addSubview:camera];
    
    
    UIView *manualView = [UIView new];
    manualView.backgroundColor = UIColor.whiteColor;
    manualView.frame = CGRectMake(12, kTopHeight + 80, SCREEN_WIDTH - 24, 500 - 60);
    [XPCommonTool cornerRadius:manualView andType:1 radius:8];
    self.manualView = manualView;
    [self.view addSubview:manualView];
    
//    UILabel *desLable = [UILabel new];
//    desLable.frame = CGRectMake(12, 0,200, 55);
//    desLable.text = @"输入银行卡号添加";
//    desLable.font = kFontRegular(14);
//    desLable.textColor = UIColorFromRGB(0x3A3C41);
//    [manualView addSubview:desLable];
//    
//    UIView *sepView = [UIView new];
//    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
//    sepView.frame = CGRectMake(12, MaxY(desLable), manualView.mj_w - 24, 1);
//    [manualView addSubview:sepView];
    
    NSArray *titleArray = @[@"银行名称",@"银行卡卡号",@"持卡人姓名",@"身份证号",@"手机号"];
    
    for (int i = 0; i<titleArray.count; i++) {
        UILabel *titleLabel1 = [UILabel new];
        titleLabel1.frame = CGRectMake(12, 12 + i*52, 86, 40);
        titleLabel1.text = titleArray[i];
        titleLabel1.font = kFontRegular(14);
        titleLabel1.textColor = UIColorFromRGB(0x3A3C41);
        [manualView addSubview:titleLabel1];
    }
    
    UIButton *bankButton = [UIButton buttonWithType:UIButtonTypeCustom];
    bankButton.frame = CGRectMake(98,12, manualView.mj_w - 98 - 12, 40);
    [bankButton addTarget:self action:@selector(selectBank:) forControlEvents:UIControlEventTouchUpInside];
    bankButton.backgroundColor = UIColorFromRGB(0xF2F2F2);
    bankButton.layer.cornerRadius = 4;
    bankButton.layer.masksToBounds = YES;
    [manualView addSubview:bankButton];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(12, 0, bankButton.mj_w - 14 -8 - 12 , bankButton.mj_h);
    titleLabel.text = @"请上传银行卡图片进行识别";
    titleLabel.font = kFontRegular(14);
    titleLabel.textColor = UIColorFromRGB(0xB8B8B8);
    self.bankNameLabel = titleLabel;
    [bankButton addSubview:titleLabel];
    
//    UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(bankButton.mj_w -14 -8 , 14, 8, 14)];
//    arrow.image = [UIImage imageNamed:@"xp_setting_arrow"];
//    [bankButton addSubview:arrow];

    
    UIView *bgView1 = [[UIView alloc] init];
    bgView1.layer.cornerRadius = 4;
    bgView1.layer.masksToBounds = YES;
    bgView1.backgroundColor = UIColorFromRGB(0xF2F2F2);
    bgView1.frame = CGRectMake(98, MaxY(bankButton) + 12, manualView.mj_w - 98 - 12, 40);
    [manualView addSubview:bgView1];
    
    UITextField *textField1 = [[UITextField alloc] init];
    textField1.frame = CGRectMake(12, 0, bgView1.mj_w - 12, 40);
    textField1.placeholder = @"请上传银行卡图片进行识别";
    textField1.font = kFontRegular(14);
    textField1.keyboardType = UIKeyboardTypeNumberPad;
    self.textField1 = textField1;
    textField1.userInteractionEnabled = NO;
    textField1.textColor = UIColorFromRGB(0x3A3C41);
    [bgView1 addSubview:textField1];
    
    UIView *bgView2 = [[UIView alloc] init];
    bgView2.layer.cornerRadius = 4;
    bgView2.layer.masksToBounds = YES;
    bgView2.backgroundColor = UIColorFromRGB(0xF2F2F2);
    bgView2.frame = CGRectMake(98, MaxY(bgView1) + 12, manualView.mj_w - 98 - 12, 40);
    [manualView addSubview:bgView2];
    
    UITextField *textField2 = [[UITextField alloc] init];
    textField2.frame = CGRectMake(12, 0, bgView2.mj_w - 12, 40);
    textField2.placeholder = @"姓名";
    textField2.font = kFontRegular(14);
    textField2.userInteractionEnabled = NO;
    self.textField2 = textField2;
    textField2.textColor = UIColorFromRGB(0x3A3C41);
    [bgView2 addSubview:textField2];
    
    
    UIView *bgView3 = [[UIView alloc] init];
    bgView3.layer.cornerRadius = 4;
    bgView3.layer.masksToBounds = YES;
    bgView3.backgroundColor = UIColorFromRGB(0xF2F2F2);
    bgView3.frame = CGRectMake(98, MaxY(bgView2) + 12, manualView.mj_w - 98 - 12, 40);
    [manualView addSubview:bgView3];
    
    UITextField *textField3 = [[UITextField alloc] init];
    textField3.frame = CGRectMake(12, 0, bgView3.mj_w - 12, 40);
    textField3.placeholder = @"身份证号";
    textField3.font = kFontRegular(14);
    textField3.userInteractionEnabled = NO;
    self.textField3 = textField3;
    textField3.textColor = UIColorFromRGB(0x3A3C41);
    [bgView3 addSubview:textField3];
    
    UIView *bgView4 = [[UIView alloc] init];
    bgView4.layer.cornerRadius = 4;
    bgView4.layer.masksToBounds = YES;
    bgView4.backgroundColor = UIColorFromRGB(0xF2F2F2);
    bgView4.frame = CGRectMake(98, MaxY(bgView3) + 12, manualView.mj_w - 98 - 12, 40);
    [manualView addSubview:bgView4];
    
    UITextField *textField4 = [[UITextField alloc] init];
    textField4.frame = CGRectMake(12, 0, bgView4.mj_w - 12, 40);
    textField4.placeholder = @"手机号";
    textField4.font = kFontRegular(14);
    textField4.textColor = UIColorFromRGB(0x3A3C41);
    textField4.keyboardType = UIKeyboardTypeNumberPad;
    textField4.delegate = self;
    self.textField4 = textField4;
    [bgView4 addSubview:textField4];
    
    UIButton *editButton1 = [[UIButton alloc] init];
    editButton1.frame = CGRectMake(bgView4.mj_w - 24 - 12, 8, 24, 24);
    [editButton1 setImage:[UIImage imageNamed:@"xp_text_edit"] forState:UIControlStateNormal];
//    [editButton1 addTarget:self action:@selector(editButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    editButton1.userInteractionEnabled = NO;
    [bgView4 addSubview:editButton1];
    
    if (self.enterType == 1) {
        
            UIView *bgView5 = [[UIView alloc] init];
            bgView5.layer.cornerRadius = 4;
            bgView5.layer.masksToBounds = YES;
            bgView5.backgroundColor = UIColorFromRGB(0xF2F2F2);
            bgView5.frame = CGRectMake(98, MaxY(bgView4) + 12, manualView.mj_w - 98 - 12 - 82-12, 40);
            [manualView addSubview:bgView5];
            
            //验证码
            self.codeButton = [UIButton buttonWithType:UIButtonTypeCustom];
            self.codeButton.frame = CGRectMake(manualView.mj_w - 12 - 82, bgView5.mj_y,82, 40);
            [self.codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
            [self.codeButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
            self.codeButton.titleLabel.font = kFontRegular(14);
            [self.codeButton addTarget:self action:@selector(codeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
            [manualView addSubview:self.codeButton];
            
            UITextField *textField5 = [[UITextField alloc] init];
            textField5.frame = CGRectMake(12, 0, bgView4.mj_w - 12 , 40);
            textField5.placeholder = @"输入验证码";
            textField5.font = kFontRegular(14);
            textField5.textColor = UIColorFromRGB(0x3A3C41);
            textField5.keyboardType =  UIKeyboardTypeNumberPad;
            self.textField5 = textField5;
            self.textField5.delegate = self;
            [bgView5 addSubview:textField5];
        
        self.manualView.frame = CGRectMake(12, kTopHeight + 80, SCREEN_WIDTH - 24, 330);
    }else {
        self.manualView.frame = CGRectMake(12, kTopHeight + 80, SCREEN_WIDTH - 24, 270);
    }
    
//    self.manualView.backgroundColor = UIColor.lightGrayColor;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    nextButton.titleLabel.font = kFontRegular(14);
    nextButton.frame = CGRectMake(84, MaxY(manualView) + 48, self.manualView.mj_w - 84 *2, 50);
    nextButton.layer.cornerRadius = 25;
    nextButton.backgroundColor = UIColorFromRGB(0xB8B8B8);
    nextButton.userInteractionEnabled = NO;
    [nextButton addTarget:self action:@selector(nextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.nextButton = nextButton;
    [self.view addSubview:nextButton];
    
    UIView *autoCardView = [UIView new];
    autoCardView.frame = CGRectMake(12, 12 + kTopHeight, SCREEN_WIDTH - 24, 190);
    autoCardView.layer.cornerRadius = 4;
    autoCardView.clipsToBounds = YES;
    autoCardView.backgroundColor = UIColor.whiteColor;
    self.autoShowView = autoCardView;
    autoCardView.hidden = YES;
    [self.view addSubview:autoCardView];
    
    UILabel *autoCardTitleLabel = [UILabel new];
    autoCardTitleLabel.font = kFontRegular(14);
    autoCardTitleLabel.textColor = UIColorFromRGB(0x3A3C41);
    autoCardTitleLabel.frame = CGRectMake(12, 0, 45, autoCardView.mj_h);
    autoCardTitleLabel.text = @"银行卡";
    [autoCardView addSubview:autoCardTitleLabel];
    
    UIButton *cardImageView = [UIButton new];
    cardImageView.frame = CGRectMake(110, 12, 230, 140);
    self.idCardButton = cardImageView;
    [cardImageView addTarget:self action:@selector(autoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [autoCardView addSubview:cardImageView];
    
    UILabel *resultLabel = [UILabel new];
    resultLabel.font = kFontRegular(12);
    resultLabel.textColor = UIColorFromRGB(0x8A8A8A);
    resultLabel.frame = CGRectMake(cardImageView.mj_origin.x, MaxY(cardImageView) + 6, cardImageView.mj_w, 20);
    resultLabel.text = @"银行卡图片内容已识别";
    [autoCardView addSubview:resultLabel];
    
    NSString *idnumber = [XPLocalStorage readUserIdNumber];
    
    self.textField2.text = [XPLocalStorage readUserUserName];
    self.textField3.text = idnumber;
    self.textField4.text = [XPLocalStorage readUserUserPhone];

    XPUploadIconView *uploadView = [[XPUploadIconView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
    uploadView.titleLabel.text = @"添加银行卡";
    uploadView.hidden = YES;
    self.uploadView = uploadView;
    [[UIApplication sharedApplication].keyWindow addSubview:uploadView];
    
    XPWeakSelf
    [uploadView setOpenAlbum:^{
        [weakSelf openAlbum];
    }];
    
    [uploadView setOpenCamera:^{
        [weakSelf openCamera];
    }];
    
    [uploadView setSubmitBlock:^{
        
    }];
}


- (void)selectBank:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
}
//发送验证码
-(void)codeButtonClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    if (![self validVerifyPredicate:phonePredicate TargetString:self.textField4.text]) {
        [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的手机号！" ToView:self.view];
        return;
    }else {
        NSString *codeUrlString = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Get_Code];
        NSDictionary *paraM =   @{@"phoneNum":self.textField4.text,@"type":@5};
        [self startLoadingGif];
        [[XPRequestManager shareInstance] commonPostWithURL:codeUrlString Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
            [self stopLoadingGif];
            if (data.isSucess) {
                self.codeUUid = data.userData;
                [self countDown];
                [self.textField5 becomeFirstResponder];
                [[XPShowTipsTool shareInstance]showMsg:@"验证码发送成功!" ToView:self.view];
            }else {
            [[XPShowTipsTool shareInstance]showMsg:data.msg ToView:self.view];
            }
        }];
    }
    
}

- (void)autoButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender)
    
//    [self openCamera];
    self.uploadView.hidden = NO;
    
}


//正则
-(BOOL)validVerifyPredicate:(NSString *) pString TargetString:(NSString *)targerString
{
   NSPredicate *num = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pString];
   return [num evaluateWithObject:targerString];
}

#pragma mark -- 倒计时
-(void)countDown{
    
    XPWeakSelf
    __block int i = 60;
        //一分钟倒计时
    self.oneMinTimer = [NSTimer jk_timerWithTimeInterval:1 block:^{
        if (i >= 0) {
            [weakSelf.codeButton setTitle:[NSString stringWithFormat:@" %d秒 ",i] forState:0];
            i--;
            weakSelf.codeButton.enabled = NO;
            weakSelf.codeButton.alpha = 0.5;
        } else{
            [weakSelf.oneMinTimer invalidate];
            weakSelf.oneMinTimer = nil;
            [ weakSelf.codeButton setTitle:@"获取验证码" forState:0];
            weakSelf.codeButton.enabled = YES;
            weakSelf.codeButton.alpha = 1.0;
        }
    } repeats:YES];
    [[NSRunLoop mainRunLoop]addTimer:self.oneMinTimer forMode:NSRunLoopCommonModes];
    [self.oneMinTimer fire];
}


- (void)openCamera {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}
- (void)openAlbum {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.uploadView.hidden = YES;
    [picker dismissViewControllerAnimated:true completion:^{
        
    }];
    self.photo = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSString *type  = [info objectForKey:UIImagePickerControllerMediaType];
    //选择的类型图片
    if ([type isEqualToString:@"public.image"]) {
        //先把图片转成NSData
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
//        self.idCardButton.image = image;
        [self.idCardButton setImage:image forState:UIControlStateNormal];
        [self uploadCardWithImage:image];
        //调整UI
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
  
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}



- (void)uploadCardWithImage:(UIImage *)iConImage {
    UIImage *image = iConImage;
//    UIImage *smallImage = [self scaleFromImage:image toSize:CGSizeMake(200, 200)];
    NSData *data = UIImageJPEGRepresentation([iConImage compressToImage], 1);

    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kUpload_card_url];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    //为了不使图片名字重复,根据时间更改图片在服务器的名称.
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *datastr = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@.jpg", datastr];

manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"multipart/form-data; boundary=----WebKitFormBoundaryHzyefUottpz7ltKf"forHTTPHeaderField:@"Content-Type"];

    [self startLoadingGif];
    [manager POST:urlStr parameters:@{} headers:@{@"token":[XPLocalStorage readUserToken]} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {

        [formData appendPartWithFileData:data name:@"bankCardFile" fileName:fileName mimeType:@"image/jpg"];

    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@" ----  %@",uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"---- >%@",responseObject);
        if (responseObject) {
            
            self.responseObject = responseObject;
            [self getCardMessage];
            
        }else {
            [self stopLoadingGif];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self stopLoadingGif];
    }];
}

- (void)getCardMessage{
    
    NSString *cardStr = self.responseObject[@"data"][@"ocrId"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kBank_Card_query,cardStr];
    [[XPRequestManager shareInstance] commonGetWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        if (data.isSucess) {
            [self stopLoadingGif];
            NSString *cardType = data.userData[@"cardType"];
            NSString *bankName = data.userData[@"bankName"];
            NSString *cardNumber = data.userData[@"cardNumber"];
            self.cardType = cardType;
            if (![cardType isEqualToString:@"DC"]) {
                [self.xpAlertView showAlertWithTitle:@"请上传正确的银行卡" actionBlock:^(NSString * _Nullable extra) {
                    
                }];
                
                self.nextButton.backgroundColor = UIColorFromRGB(0xB8B8B8);
                self.nextButton.userInteractionEnabled = NO;
            }else {
                self.autoAdd.hidden = YES;
                self.autoShowView.hidden = NO;
                self.manualView.frame = self.manualView.frame = CGRectMake(12, MaxY(self.autoShowView) + 12, SCREEN_WIDTH - 24, self.manualView.mj_h);
                self.textField1.text = cardNumber;
                self.bankNameLabel.text = bankName;
                self.bankNameLabel.textColor = app_font_color;
                self.uploadView.hidden = YES;
                
                self.nextButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
                self.nextButton.userInteractionEnabled = YES;
                self.nextButton.frame = CGRectMake(84, MaxY(self.manualView) + 48, self.manualView.mj_w - 84 *2, 50);
            }
        }else if(data.code.intValue == 10000){//pengding
            [self performSelector:@selector(getCardMessage) withObject:nil afterDelay:2.0];
         }else{
            [self stopLoadingGif];
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
}


- (void)nextButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender)

    if (self.enterType == 1) {
        NSString *codeStr = [[XPVerifyTool shareInstance] verifyCode:self.textField5.text];
        if (codeStr) {
            [[XPShowTipsTool shareInstance] showMsg:codeStr ToView:self.view];
            return;
        }
    }else {
        if (![self validVerifyPredicate:phonePredicate TargetString:self.textField4.text]) {
            [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的手机号" ToView:self.view];
            return;
        }
        
    }
    
//
    NSString *urlString;
    if (self.enterType == 0) {
        urlString = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kBank_card_add];
    }else {
        urlString = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kBank_profit_card_add];
    }
    
    
        NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
        paraM[@"name"] = self.textField2.text;
        paraM[@"idNo"] = self.textField3.text;
    if (self.enterType == 1) {
        paraM[@"uuid"] = self.codeUUid;
        paraM[@"code"] = self.textField5.text;
    }
        paraM[@"bankName"] = self.bankNameLabel.text;
        paraM[@"cardNo"] = self.textField1.text;
        paraM[@"bankPhone"] = self.textField4.text;
        paraM[@"bankCardType"] = self.cardType;
        paraM[@"cardType"] = self.cardType == 0 ? @(0):@(1);
        
    [self startLoadingGif];
        [[XPRequestManager shareInstance] commonPostWithURL:urlString Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
            [self stopLoadingGif];
            if (data.isSucess) {
 
                if (self.enterType == 1) {//添加结算卡
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:[UIApplication sharedApplication].keyWindow];
                    [self goBack];
                }else {//添加支付卡
                    NSString *statusStr = data.userData[@"status"];
                    if ([statusStr isEqualToString:@"succeeded"]) {
                        //银行卡添加成功
                         [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:[UIApplication sharedApplication].keyWindow];
                            self.applyId = data.userData[@"id"];
                            [self showAlert];
                        
                    }else if ([statusStr isEqualToString:@"failed"]){
                        [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
                    }else {
                        [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
                    }
                }
            }else {
                [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
            };
            
        }];
}

- (void)showAlert{
    XPWeakSelf
    NSString *tipsString = [NSString stringWithFormat:@"已向%@发送验证码\n请注意查收",[self.textField4.text stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"]];
    self.xpAlertView.autoDissmiss = NO;
    
    [self.xpAlertView configWithTitle:@"验证码" desCriptionStr:tipsString placeHolderStr:@"请输入验证码" leftAction:^(NSString * _Nullable extra) {
        self.xpAlertView.autoDissmiss = YES;
    } rightAction:^(NSString * _Nullable extra) {
        NSString *codeStr = [[XPVerifyTool shareInstance] verifyCode:weakSelf.xpAlertView.textField.text];
    if (codeStr) {
        [[XPShowTipsTool shareInstance] showMsg:codeStr ToView:k_keyWindow];
    }else {
        self.xpAlertView.autoDissmiss = YES;
        [weakSelf.xpAlertView dismiss];
        [weakSelf appForBindCardShowLoading:YES];
    }
    
    } alertType:XPAlertTypeInput];
}

- (void)appForBindCardShowLoading:(BOOL)isShow{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kBank_result_Confirm];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"applyId"] = self.applyId;
    paraM[@"smsCode"] = self.xpAlertView.textField.text;
    
    if (isShow) {
        [self startLoadingGif];
    }
        [[XPRequestManager shareInstance] commonPostWithURL:urlString Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
            [self stopLoadingGif];
            if (data.isSucess) {
                
                NSString *pendingStr = data.userData[@"status"];
                if ([pendingStr isEqualToString:@"succeeded"]) {
                    [self stopLoadingGif];
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:[UIApplication sharedApplication].keyWindow];
                    [self.view endEditing:YES];
                    [self goBack];
                }else if ([pendingStr isEqualToString:@"failed"]){
                    [self stopLoadingGif];
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
                }else {
                    [self addBankQurey];
                }
            }else {//接口请求失败
                [self stopLoadingGif];
               //银行卡添加成功
                [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:[UIApplication sharedApplication].keyWindow];
            }
        }];
}

- (void)addBankQurey {
    NSString *urlString = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kBank_result_Confirm_query,self.applyId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    
        [[XPRequestManager shareInstance] commonPostWithURL:urlString Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
            if (data.isSucess) {
                
                NSString *pendingStr = data.userData[@"status"];
                if ([pendingStr isEqualToString:@"succeeded"]) {
                    [self stopLoadingGif];
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:[UIApplication sharedApplication].keyWindow];
                    [self goBack];
                }else if ([pendingStr isEqualToString:@"failed"]){
                    [self stopLoadingGif];
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
                }else {
//                    [self addBankQurey];
                    [self performSelector:@selector(addBankQurey) withObject:nil afterDelay:2.0];
                }
            }else {
                [self stopLoadingGif];
                [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
            }
        }];
}

#pragma mark --- textField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.textField4){
        NSString *currentText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        // 如果当前文本长度超过6位，则不允许输入更多字符
        if (currentText.length > 11) {
            return NO;
        }
    }else if(textField == self.textField5){
        NSString *currentText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        // 如果当前文本长度超过6位，则不允许输入更多字符
        if (currentText.length > 6) {
            return NO;
        }
    }
    // 允许输入
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
