//
//  XPFaceVerifyController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/20.
//

#import "XPFaceVerifyController.h"
#import "AFNetworking.h"
#import "NSTimer+JKBlocks.h"
#import "XPUserSettingController.h"
#import "UIImage+Compression.h"

@interface XPFaceVerifyController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (nonatomic, strong) UIButton *frontButton;
@property (nonatomic, strong) UIImage *backPhoto;

@property (nonatomic, strong) UIImageView *resultImageView;
@property (nonatomic, strong) UILabel *resultLabel;
@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIImageView *carImageView;

//计时器
@property(nonatomic,strong)NSTimer * oneMinTimer;
//服务器ocr返回证件识别信息
@property (nonatomic, copy) id responseObject;
@end

@implementation XPFaceVerifyController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    
    [self setupView];
}
-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.navBarView];
    self.navBarView.titleLabel.text = @"人脸识别";

}

- (void)setupView {
    
    UIView *topView = [UIView new];
    topView.backgroundColor = UIColor.whiteColor;
    topView.frame = CGRectMake(0, 0, self.view.mj_w, 200);
    [self.view addSubview:topView];
    
    CGFloat btnW = topView.mj_w - 118 *2;
    //身份证正面
    UIButton *frontBtn = [UIButton new];
    frontBtn.frame = CGRectMake(118, 60 + kTopHeight, btnW, btnW);
    self.frontButton = frontBtn;
    [frontBtn addTarget:self action:@selector(frontButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    frontBtn.layer.cornerRadius = btnW/2.;
    frontBtn.layer.masksToBounds = YES;
    frontBtn.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [topView addSubview:frontBtn];
    
    UIImageView *carImageView = [[UIImageView alloc] initWithFrame:CGRectMake(btnW/2. - 14, btnW/2. - 14, 28, 28)];
    carImageView.image = [UIImage imageNamed:@"xp_camera"];
    self.carImageView = carImageView;
    [frontBtn addSubview:carImageView];
    
    
    UILabel *fLabel = [UILabel new];
    fLabel.frame = CGRectMake(0, MaxY(frontBtn) + 50, topView.mj_w, 24);
    fLabel.text = @"拍摄时，请保持正对手机，光线充足";
    fLabel.textColor = UIColorFromRGB(0x3A3C41);
    fLabel.font = kFontRegular(14);
    fLabel.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:fLabel];

    topView.frame = CGRectMake(0, 0, self.view.mj_w, MaxY(fLabel) + 30);
    
    UIImageView *bImageView = [UIImageView new];
    bImageView.frame = CGRectMake(self.view.mj_w/2. - 20, MaxY(topView) + 40, 40, 40);
    bImageView.image = [UIImage imageNamed:@"xp_camera"];
    self.resultImageView = bImageView;
    bImageView.hidden = YES;
    [self.view addSubview:bImageView];
    
    UILabel *resultLabel = [UILabel new];
    resultLabel.frame = CGRectMake(0, MaxY(bImageView) + 14, self.view.mj_w, 24);
    resultLabel.font = kFontRegular(16);
    resultLabel.textAlignment = NSTextAlignmentCenter;
    self.resultLabel= resultLabel;
    [self.view addSubview:resultLabel];
    
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.frame = CGRectMake(0, MaxY(resultLabel) + 14, self.view.mj_w, 24);
    timeLabel.font = kFontRegular(14);
    timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel = timeLabel;
    [self.view addSubview:timeLabel];
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(12,self.view.mj_h - 80, SCREEN_WIDTH - 2 * 12, 50);
    [saveButton setTitle:@"下一步" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    saveButton.layer.cornerRadius = 25;
    saveButton.clipsToBounds = YES;
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    saveButton.userInteractionEnabled = YES;
    self.saveButton = saveButton;
    [self.view addSubview:saveButton];
    
}
- (void)frontButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender)
    [self openCamera];
    
    
}
- (void)openCamera {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:true completion:^{
        
    }];
    self.backPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSString *type  = [info objectForKey:UIImagePickerControllerMediaType];
    //选择的类型图片
    if ([type isEqualToString:@"public.image"]) {
        //先把图片转成NSData
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        [self uploadCardWithImage:image];
        //调整UI
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
  
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)uploadCardWithImage:(UIImage *)iConImage {
//    iConImage = ;
    NSData *faceCompressData = UIImageJPEGRepresentation([iConImage compressToImage], 1);
    NSData *idFrontCompressData =  UIImageJPEGRepresentation([self.frontPhoto compressToImage], 1);
    
//    NSLog(@"data--- >%lu   ----- >%lu",data.length,idcardData.length);
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kUser_id_face_ocr];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    //为了不使图片名字重复,根据时间更改图片在服务器的名称.
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *datastr = [formatter stringFromDate:[NSDate date]];
    
    NSString *idCardFaceName = [NSString stringWithFormat:@"%@_face.jpg", datastr];
    NSString *idCardFrontName = [NSString stringWithFormat:@"%@_front.jpg", datastr];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"multipart/form-data; boundary=----WebKitFormBoundaryHzyefUottpz7ltKf"forHTTPHeaderField:@"Content-Type"];

    NSString *severName = @"faceFile";
    NSString *idSeverName = @"idCardFaceFile";
    [self startLoadingGif];
    
    [manager POST:urlStr parameters:self.extraDic headers:@{@"token":[XPLocalStorage readUserToken]} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {

        [formData appendPartWithFileData:faceCompressData name:severName fileName:idCardFaceName mimeType:@"image/jpg"];
        [formData appendPartWithFileData:idFrontCompressData name:idSeverName fileName:idCardFrontName mimeType:@"image/jpg"];

    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@" ----  %@",uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"---- >%@",responseObject);
        if (responseObject&&[[responseObject objectForKey:@"status"] isEqualToString:@"succeeded"]) {
            self.responseObject = responseObject;
            [self getFaceMessageQuery];
            
        }else {
            [self stopLoadingGif];
            [[XPShowTipsTool shareInstance] showMsg:responseObject[@"msg"] ToView:k_keyWindow];
            [self faildResetUI];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error--- >%@",error);
        [self stopLoadingGif];
        [self faildResetUI];
        
    }];
}
- (void)getFaceMessageQuery{
    
    NSString *userId = self.responseObject[@"data"][@"userId"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kUser_id_face_ocr_query,userId];
    
    [[XPRequestManager shareInstance] commonGetWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        if (data.isSucess) {
            [self stopLoadingGif];
            [self successFreshUI];
        }else if(data.code.intValue == 10000){//pengding
            [self performSelector:@selector(getFaceMessageQuery) withObject:nil afterDelay:2.0];
         }else{
            [self stopLoadingGif];
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
            [self faildResetUI];
        }
    }];
}

- (void)successFreshUI {
    self.carImageView.hidden = YES;
    self.frontButton.userInteractionEnabled = NO;
    [self.frontButton setImage:self.backPhoto forState:UIControlStateNormal];
    self.resultImageView.hidden = NO;
    self.resultImageView.image = [UIImage imageNamed:@"Group_67"];
    self.resultLabel.text = @"认证成功";
    self.resultLabel.textColor = UIColorFromRGB(0x6E6BFF);
    [self.saveButton setTitle:@"立即退出" forState:UIControlStateNormal];
    
    XPLocalStorage *storage = [XPLocalStorage new];
    [storage writeUserName:self.extraDic[@"name"]];
    [storage writeUserCertified:@"1"];
    [storage writeUserIdNumber:self.extraDic[@"idNumber"]];
    [self countDown];
}

-(void)faildResetUI {
    self.carImageView.hidden = YES;
    self.frontButton.userInteractionEnabled = NO;
    [self.frontButton setImage:self.backPhoto forState:UIControlStateNormal];
    self.resultImageView.hidden = NO;
    self.resultImageView.image = [UIImage imageNamed:@"Group_68"];
    self.resultLabel.text = @"认证失败";
    self.resultLabel.textColor = UIColorFromRGB(0xFF4A4A);
    self.timeLabel.text = @"请重新认证";
    
    [self.saveButton setTitle:@"重新认证" forState:UIControlStateNormal];
}


- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    //回到setting 页面
    
    if ([sender.titleLabel.text isEqualToString:@"重新认证"]) {
        [self openCamera];
    }if ([sender.titleLabel.text isEqualToString:@"下一步"]) {
        [self openCamera];
    } else if([sender.titleLabel.text isEqualToString:@"立即退出"]) {//回到setting 页面
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[XPUserSettingController class]])
            {
                [self.navigationController popToViewController:controller animated:YES];
            }else {
                @try {
                    int index = [[self.navigationController viewControllers]indexOfObject:self];
                    if (index >= 2) {
                        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:index-2]animated:YES];
                    }
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                } 
            }
        }
    }
}

#pragma mark -- 倒计时
-(void)countDown{
    
    XPWeakSelf
    __block int i = 4;
        //一分钟倒计时
    self.oneMinTimer = [NSTimer jk_timerWithTimeInterval:1 block:^{
        if (i >= 0) {
            self.timeLabel.text = [NSString stringWithFormat:@"在 %d 秒后自动返回",i];
            i--;
        } else{
            [weakSelf.oneMinTimer invalidate];
            weakSelf.oneMinTimer = nil;
            [weakSelf saveButtonClicked:weakSelf.saveButton];
        }
    } repeats:YES];
    [[NSRunLoop mainRunLoop]addTimer:self.oneMinTimer forMode:NSRunLoopCommonModes];
    [self.oneMinTimer fire];
}

-(void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
