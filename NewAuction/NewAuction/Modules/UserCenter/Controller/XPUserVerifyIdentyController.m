//
//  XPUserVerifyIdentyController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/13.
//

#import "XPUserVerifyIdentyController.h"
#import "BRPickerView.h"
#import "XPLocalStorage.h"
#import "XPUploadIconView.h"
#import "AFNetworking.h"
#import "XPFaceVerifyController.h"

@interface XPUserVerifyIdentyController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) UITextField *textField1;
@property (nonatomic, strong) UITextField *textField2;
@property (nonatomic, strong) UITextField *textField3;
@property (nonatomic, strong) UITextField *textField4;
@property (nonatomic, strong) UITextField *textField5;
@property (nonatomic, strong) UIButton *button;

@property (nonatomic, strong) UIButton *frontButton;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIImageView *frontImageView;
@property (nonatomic, strong) UIImageView *backImageView;

//1 正面点击  2 国徽面点击
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) XPUploadIconView *uploadView;

@property (nonatomic, strong) UIImage *frontPhoto;
@property (nonatomic, strong) UIImage *backPhoto;
@property (nonatomic, assign) bool frontSuccess;
@property (nonatomic, assign) bool backSuccess;

@property (nonatomic, strong) UIButton *saveButton;

@property (nonatomic, strong) NSMutableDictionary *paraM;

@property (nonatomic, strong) UIScrollView *mainView;
//服务器ocr返回证件识别信息
@property (nonatomic, copy) id responseObject;
@end

@implementation XPUserVerifyIdentyController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xDEDEDE);
    self.paraM = [NSMutableDictionary dictionary];
    [self setupUI];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"实名认证";
    [self.view addSubview:self.navBarView];
    self.navBarView.backgroundColor = UIColorFromRGB(0xF2F2F2);
}

- (void)setupUI {
    
    UIScrollView *mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight)];
    mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.mainView = mainView;
    [self.view addSubview:mainView];
    
    UIView *topView = [UIView new];
    topView.backgroundColor = UIColor.whiteColor;
    topView.layer.cornerRadius = 8;
    topView.frame = CGRectMake(12, 12, self.view.mj_w - 24, 332);
    [mainView addSubview:topView];
    
    UILabel *deslabel = [UILabel new];
    deslabel.frame = CGRectMake(24, 12, 80, 24);
    deslabel.font = kFontRegular(14);
    deslabel.textColor = UIColorFromRGB(0x3A3C41);
    deslabel.text = @"身份证识别";
    [topView addSubview:deslabel];
    
    UILabel *deslabel2 = [UILabel new];
    deslabel2.frame = CGRectMake(24, 12, topView.mj_w - 48, 24);
    deslabel2.font = kFontRegular(14);
    deslabel2.textColor = UIColorFromRGB(0x8A8A8A);
    deslabel2.text = @"上传成功后自动识别信息";
    deslabel2.textAlignment = NSTextAlignmentRight;
    [topView addSubview:deslabel2];
    
    
    CGFloat cardW = topView.mj_w - 24 *2;
    CGFloat cardH = cardW * 186/303.;
    UIView *bgView1 = [UIView new];
//    bgView1.backgroundColor =UIColorFromRGB(0xDEDEDE);
    bgView1.layer.cornerRadius = 4;
    bgView1.frame = CGRectMake(24, 48, cardW, cardH);
    [topView addSubview:bgView1];
    
    self.frontImageView = [[UIImageView alloc] initWithFrame:bgView1.bounds];
    self.frontImageView.image = [UIImage imageNamed:@"xp_id_front"];
    [bgView1 addSubview:self.frontImageView];
    
    //身份证正面
    UIButton *frontBtn = [UIButton new];
    frontBtn.frame = bgView1.bounds;
    self.frontButton = frontBtn;
    [frontBtn addTarget:self action:@selector(frontButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bgView1 addSubview:frontBtn];
    
    
    UILabel *fLabel = [UILabel new];
    fLabel.frame = CGRectMake(0, frontBtn.center.y + 5, frontBtn.mj_w, 20);
    fLabel.text = @"上传身份证正面照片";
    fLabel.textColor = UIColorFromRGB(0x3A3C41);
    fLabel.font = kFontRegular(14);
    fLabel.textAlignment = NSTextAlignmentCenter;
    [self.frontImageView addSubview:fLabel];
    
    UIImageView *fImageView = [UIImageView new];
    fImageView.frame = CGRectMake(frontBtn.mj_w/2. - 10, frontBtn.center.y - 5 - 20, 20, 20);
    fImageView.image = [UIImage imageNamed:@"xp_camera"];
    [self.frontImageView addSubview:fImageView];
    
    //身份证国徽面
    UIView *bgView2 = [UIView new];
    bgView2.backgroundColor =UIColorFromRGB(0xDEDEDE);
    bgView2.layer.cornerRadius = 4;
    bgView2.frame = CGRectMake(24, 12 + MaxY(bgView1), cardW, cardH);
    [topView addSubview:bgView2];
    
    
    self.backImageView = [[UIImageView alloc] initWithFrame:bgView2.bounds];
    self.backImageView.image = [UIImage imageNamed:@"xp_id_back"];
    [bgView2 addSubview:self.backImageView];
    
    UIButton *backBtn = [UIButton new];
    backBtn.frame = bgView2.bounds;
    self.backButton = backBtn;
    [backBtn addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bgView2 addSubview:backBtn];
    
    UILabel *bLabel = [UILabel new];
    bLabel.frame = CGRectMake(0, frontBtn.center.y + 5, frontBtn.mj_w, 20);
    bLabel.text = @"上传身份证国徽面照片";
    bLabel.textColor = UIColorFromRGB(0x3A3C41);
    bLabel.font = kFontRegular(14);
    bLabel.textAlignment = NSTextAlignmentCenter;
    [self.backImageView addSubview:bLabel];
    
    UIImageView *bImageView = [UIImageView new];
    bImageView.frame = CGRectMake(frontBtn.mj_w/2. - 10, frontBtn.center.y - 5 - 20, 20, 20);
    bImageView.image = [UIImage imageNamed:@"xp_camera"];
    [self.backImageView addSubview:bImageView];
    
    topView.frame = CGRectMake(12, 12, self.view.mj_w - 24, MaxY(bgView2) + 12);
    
    UIView *downView = [UIView new];
    downView.backgroundColor = UIColor.whiteColor;
    downView.layer.cornerRadius = 8;
    downView.frame = CGRectMake(12,  MaxY(topView) + 14, self.view.mj_w - 24, 300);
    [mainView addSubview:downView];
    
    CGFloat leftMargin = 12;
    
    UILabel *label1 = [UILabel new];
    label1.frame = CGRectMake(leftMargin, 12, 86, 40);
    label1.font = kFontRegular(14);
    label1.textColor = UIColorFromRGB(0x8A8A8A);
    label1.text = @"姓名";
    [downView addSubview:label1];
    
    UITextField *textfield1 = [[UITextField alloc] initWithFrame:CGRectMake(MaxX(label1), label1.mj_y, downView.mj_w - 110 , 40)];
    textfield1.font = kFontRegular(14);
    textfield1.textColor = UIColorFromRGB(0x3A3C41);
    textfield1.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [downView addSubview:textfield1];
    textfield1.userInteractionEnabled = NO;
    textfield1.leftView = [self createTextFieldLeftView];
    textfield1.leftViewMode = UITextFieldViewModeAlways;
    self.textField1 = textfield1;
    
    UILabel *label2 = [UILabel new];
    label2.frame = CGRectMake(leftMargin, 12 + MaxY(label1), 86, 40);
    label2.font = kFontRegular(14);
    label2.textColor = UIColorFromRGB(0x8A8A8A);
    label2.text = @"性别";
    [downView addSubview:label2];
    
    UITextField *textfield2 = [[UITextField alloc] initWithFrame:CGRectMake(MaxX(label2), label2.mj_y, downView.mj_w - 110 , 40)];
    textfield2.font = kFontRegular(14);
    textfield2.textColor = UIColorFromRGB(0x3A3C41);
    [downView addSubview:textfield2];
    textfield2.userInteractionEnabled = YES;
    textfield2.leftView = [self createTextFieldLeftView];
    textfield2.leftViewMode = UITextFieldViewModeAlways;
    textfield2.userInteractionEnabled = NO;
    textfield2.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    self.textField2 = textfield2;
    
    UILabel *label3 = [UILabel new];
    label3.frame = CGRectMake(leftMargin, 12 + MaxY(label2), 86, 40);
    label3.font = kFontRegular(14);
    label3.textColor = UIColorFromRGB(0x8A8A8A);
    label3.text = @"身份证号";
    [downView addSubview:label3];
    
    UITextField *textfield3 = [[UITextField alloc] initWithFrame:CGRectMake(MaxX(label3), label3.mj_y, downView.mj_w - 110 , 40)];
    textfield3.font = kFontRegular(14);
    textfield3.textColor = UIColorFromRGB(0x3A3C41);
    [downView addSubview:textfield3];
    textfield3.backgroundColor = UIColorFromRGB(0xF2F2F2);
    textfield3.userInteractionEnabled = YES;
    textfield3.leftView = [self createTextFieldLeftView];
    textfield3.leftViewMode = UITextFieldViewModeAlways;
    textfield3.userInteractionEnabled = NO;
    self.textField3 = textfield3;
    
    UILabel *label4 = [UILabel new];
    label4.frame = CGRectMake(leftMargin, 12 + MaxY(label3), 86, 40);
    label4.font = kFontRegular(14);
    label4.textColor = UIColorFromRGB(0x8A8A8A);
    label4.text = @"发证机关";
    [downView addSubview:label4];
    
    UITextField *textfield4 = [[UITextField alloc] initWithFrame:CGRectMake(MaxX(label4), label4.mj_y, downView.mj_w - 110 , 40)];
    textfield4.font = kFontRegular(14);
    textfield4.textColor = UIColorFromRGB(0x3A3C41);
    [downView addSubview:textfield4];
    textfield4.backgroundColor = UIColorFromRGB(0xF2F2F2);
    textfield4.userInteractionEnabled = YES;
    textfield4.leftView = [self createTextFieldLeftView];
    textfield4.leftViewMode = UITextFieldViewModeAlways;
    textfield4.userInteractionEnabled = NO;
    self.textField4 = textfield4;

    
    UILabel *label5 = [UILabel new];
    label5.frame = CGRectMake(leftMargin, 12 + MaxY(label4), 86, 40);
    label5.font = kFontRegular(14);
    label5.textColor = UIColorFromRGB(0x8A8A8A);
    label5.text = @"有效期";
    [downView addSubview:label5];
    
    UITextField *textfield5 = [[UITextField alloc] initWithFrame:CGRectMake(MaxX(label5), label5.mj_y, downView.mj_w - 110 , 40)];
    textfield5.font = kFontRegular(14);
    textfield5.textColor = UIColorFromRGB(0x3A3C41);
    [downView addSubview:textfield5];
    textfield5.backgroundColor = UIColorFromRGB(0xF2F2F2);
    textfield5.userInteractionEnabled = YES;
    textfield5.leftView = [self createTextFieldLeftView];
    textfield5.leftViewMode = UITextFieldViewModeAlways;
    textfield5.userInteractionEnabled = NO;
    self.textField5 = textfield5;
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(84,MaxY(textfield5) + 48, SCREEN_WIDTH - 2 * 84, 50);
    [saveButton setTitle:@"下一步" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    saveButton.layer.cornerRadius = 25;
    saveButton.clipsToBounds = YES;
    [saveButton setTitleColor:UIColorFromRGB(0xB8B8B8) forState:UIControlStateNormal];
    [saveButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [saveButton setBackgroundColor:UIColorFromRGB(0xF2F2F2)];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    saveButton.userInteractionEnabled = NO;
    self.saveButton = saveButton;
    [downView addSubview:saveButton];
    
    downView.frame = CGRectMake(12,  MaxY(topView) + 14, self.view.mj_w - 24, MaxY(saveButton) + 12);
    
    mainView.contentSize = CGSizeMake(SCREEN_WIDTH, MaxY(downView) + self.safeBottom);
    /* UI暂时废弃
    
    XPUploadIconView *uploadView = [[XPUploadIconView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
    uploadView.titleLabel.text = @"添加身份证";
    uploadView.hidden = YES;
    self.uploadView = uploadView;
    [[UIApplication sharedApplication].keyWindow addSubview:uploadView];
    
    XPWeakSelf
    [uploadView setOpenAlbum:^{
        [weakSelf openAlbum];
    }];
    
    [uploadView setOpenCamera:^{
        [weakSelf openCamera];
    }];
    
    [uploadView setSubmitBlock:^{
        weakSelf.uploadView.hidden = YES;
    }];
    */
}

//上传图片
-(void)showAlertWithMsg:(NSString *)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       
    }];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openCamera];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openAlbum];
    }];
    [alert addAction:cancel];
    [alert addAction:action];
    [alert addAction:action2];

    [self presentViewController:alert animated:YES completion:nil];
    
    
}


- (void)openCamera {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}
- (void)openAlbum {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:true completion:^{
        
    }];
    if (self.currentIndex == 1) {
        self.frontPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    }else {
        self.backPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    NSString *type  = [info objectForKey:UIImagePickerControllerMediaType];
    //选择的类型图片
    if ([type isEqualToString:@"public.image"]) {
        //先把图片转成NSData
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        [self uploadCardWithImage:image];
        //调整UI
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
  
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}



- (void)uploadCardWithImage:(UIImage *)iConImage {
    UIImage *image = iConImage;
//    UIImage *smallImage = [self scaleFromImage:image toSize:CGSizeMake(200, 200)];
    NSData *data = [XPCommonTool zipImageWithMaxSize:450 * 1024 image:iConImage];

    NSString *urlStr ;
    if (self.currentIndex == 1) {
        urlStr  = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kUser_id_front_ocr];
    }else {
        urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kUser_id_back_ocr];
    }
    

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    //为了不使图片名字重复,根据时间更改图片在服务器的名称.
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *datastr = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@.jpg", datastr];

    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"multipart/form-data; boundary=----WebKitFormBoundaryHzyefUottpz7ltKf"forHTTPHeaderField:@"Content-Type"];

    NSString *severName;
    if (self.currentIndex == 1) {
        severName = @"idCardFaceFile";
    }else {
        severName = @"idCardBackFile";
    }
    
    [self startLoadingGif];
    [manager POST:urlStr parameters:@{} headers:@{@"token":[XPLocalStorage readUserToken]} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {

        [formData appendPartWithFileData:data name:severName fileName:fileName mimeType:@"image/jpg"];

    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@" ----  %@",uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"---- >%@",responseObject);
        if (responseObject&&[[responseObject objectForKey:@"status"] isEqualToString:@"succeeded"]) {
            self.responseObject = responseObject;
            [self getCardMessageQuery];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:@"识别失败,请重试" ToView:k_keyWindow];
            [self stopLoadingGif];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error--- >%@",error);
        [self stopLoadingGif];
        
        [[XPShowTipsTool shareInstance] showMsg:@"识别失败,请重试" ToView:k_keyWindow];
        self.uploadView.hidden = YES;
    }];
}

- (void)getCardMessageQuery{
    
    NSString *cardStr = self.responseObject[@"data"][@"ocrId"];
    
    NSString *url;
    
    if (self.currentIndex == 1) {
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kUser_id_front_ocrQuery,cardStr];
    }else{
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kUser_id_back_ocrQuery,cardStr];
    }
    
    [[XPRequestManager shareInstance] commonGetWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        if (data.isSucess) {
                [self stopLoadingGif];
                if (self.currentIndex == 1) {
                    self.frontSuccess = YES;
                    NSString *idNumber = data.userData[@"idNumber"];
                    NSString *sex = data.userData[@"sex"];
                    NSString *birthDate = data.userData[@"birthDate"];
                    NSString *address = data.userData[@"address"];
                    NSString *name = data.userData[@"name"];
                    
                    self.paraM[@"idNumber"] = idNumber;
                    self.paraM[@"name"] = name;
                    self.paraM[@"sex"] = sex;
                    self.paraM[@"birthDate"] = birthDate;
                    self.paraM[@"address"] = address;
                    
                    self.frontImageView.hidden = YES;
                    [self.frontButton setImage:self.frontPhoto forState:UIControlStateNormal];
                    
                    self.textField1.text = name;
                    self.textField2.text = sex;
                    self.textField3.text = idNumber;
                    
                }else {
                    self.backSuccess = YES;
                    NSString *idCardIndate = data.userData[@"idCardIndate"];
                    NSString *idCardIssuer = data.userData[@"idCardIssuer"];
                    self.paraM[@"idCardIndate"] = idCardIndate;
                    self.paraM[@"idCardIssuer"] = idCardIssuer;
                    self.backImageView.hidden = YES;
                    [self.backButton setImage:self.backPhoto forState:UIControlStateNormal];
                    
                    self.textField4.text = idCardIssuer;
                    self.textField5.text = idCardIndate;
                    
                }
            
            if (self.frontSuccess && self.backSuccess) {
                [self.saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
                [self.saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
                self.saveButton.userInteractionEnabled = YES;
            }
        }else if(data.code.intValue == 10000){//pengding
           [self performSelector:@selector(getCardMessageQuery) withObject:nil afterDelay:2.0];
        }else{
            [self stopLoadingGif];
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
            self.uploadView.hidden = YES;
        }
    }];
}


- (UIView *)createTextFieldLeftView {
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 0, 12, 45);
    return view;
}


- (void)frontButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    self.currentIndex = 1;
//    self.uploadView.hidden = NO;
    [self showAlertWithMsg:@"身份证正面照"];
}

- (void)backButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    self.currentIndex = 2;
//    self.uploadView.hidden = NO;
    [self showAlertWithMsg:@"身份证反面照"];
    
}

- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPFaceVerifyController *faceVc = [XPFaceVerifyController new];
    faceVc.extraDic = [self.paraM copy];
    faceVc.frontPhoto = self.frontPhoto;
    [self.navigationController pushViewController:faceVc animated:YES];
    
}

-(void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
@end
