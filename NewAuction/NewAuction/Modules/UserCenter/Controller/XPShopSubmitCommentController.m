//
//  XPShopSubmitCommentController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/12.
//

#import "XPShopSubmitCommentController.h"
#import "XPShopProductBaseView.h"
#import "XPStarView.h"
#import "XPImageAddView.h"
#import "SZTextView.h"

@interface XPShopSubmitCommentController ()

@property (nonatomic, strong) XPShopProductBaseView *topView;

@property (nonatomic, strong) UIView *commentView;

@property (nonatomic, strong) SZTextView *textView;

@property (nonatomic, strong) XPImageAddView *imageAddView;
@end

@implementation XPShopSubmitCommentController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [self setupViews];
    
}
-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"待评价";
    [self.view addSubview:self.navBarView];
}

- (void)setupViews {
    
    self.topView = [[XPShopProductBaseView alloc] initWithFrame:CGRectMake(0, kTopHeight + 12, self.view.mj_w, 112)];
    
    [self.view addSubview:self.topView];
    
    
    
    self.commentView = [[UIView alloc]initWithFrame:CGRectMake(0, MaxY(self.topView) + 12, self.view.mj_w, 321)];
    self.commentView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:self.commentView];
    
    UILabel *desLabel = [UILabel new];
    desLabel.font = kFontMedium(12);
    desLabel.textColor = app_font_color;
    desLabel.frame = CGRectMake(24, 20,56, 24);
    desLabel.text = @"商品评价";
    [self.commentView addSubview:desLabel];
    
    XPStarView *starView = [[XPStarView alloc] initWithFrame:CGRectMake(MaxX(desLabel) + 12, desLabel.mj_y, 140, 20)];
    starView.canOption = YES;
    starView.star = 0;
    [self.commentView addSubview:starView];
    
    self.textView = [[SZTextView alloc] initWithFrame:CGRectMake(24, MaxY(desLabel) + 20, self.view.mj_w - 48, 120)];
    self.textView.layer.borderColor = UIColorFromRGB(0xF2F2F2).CGColor;
    self.textView.layer.borderWidth = 1;
    self.textView.layer.cornerRadius = 8;
    self.textView.placeholder = @"请输入您的评价";
    [self.commentView addSubview:self.textView];
    
    self.imageAddView = [[XPImageAddView alloc] initWithFrame:CGRectMake(24, MaxY(self.textView) + 12, self.view.mj_w - 48, 101)];
    [self.commentView addSubview:self.imageAddView];
    
    
    UIView *bottom = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 48 -self.safeBottom, SCREEN_WIDTH, 48 + self.safeBottom)];
    bottom.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:bottom];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(bottom.mj_w - 100 - 12, 4, 100, 40);
    rightButton.layer.cornerRadius = 20;
    rightButton.clipsToBounds = YES;
    [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    rightButton.titleLabel.font = kFontMedium(14);
    [rightButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setTitle:@"提交评价" forState:UIControlStateNormal];
    [bottom addSubview:rightButton];
    
    [rightButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :rightButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    
    [self.topView initDataWithTile:@"五常大米精选5斤斤稻花长粒香"  productUrl:@"" descrptionString:@"盒装精品，2.5kg"];
    
}

- (void)bottomButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
}

@end
