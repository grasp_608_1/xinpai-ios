//
//  XPLogisticsInfoController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/19.
//

#import "XPLogisticsInfoController.h"
#import "XPEmptyView.h"
#import "XPLogisticsModel.h"
#import "XPLogisticsCell.h"

@interface XPLogisticsInfoController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) XPEmptyView *emptyView;

@property (nonatomic, strong) UILabel *titleLabel;
//状态label
@property (nonatomic, strong) UILabel *statusLabel;
//状态描述label
@property (nonatomic, strong) UILabel *statusTipLabel;
//数据源
@property (nonatomic, copy) NSArray *dataArray;
//高度Array
@property (nonatomic, copy) NSArray *heightArray;
//当前页码
@property (nonatomic, assign) NSInteger page;

@end

@implementation XPLogisticsInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    self.dataArray = [NSMutableArray array];
    self.page = 1;
    
    [self setupUI];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getData];
}


- (void)setupUI {
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBottomHeight)];
    [self.view addSubview:self.tableView];
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无物流信息"];
    [self.view addSubview:self.emptyView];
    
    self.tableView.tableHeaderView = [self getTabelViewHeaderView];
    self.tableView.tableFooterView = [self getTabelViewFooterView];
    
}
- (UIView *)getTabelViewHeaderView {
    UIView *header = [UIView new];
    header.frame = CGRectMake(0, 0, SCREEN_WIDTH, 48 + 98);
    
    UIImageView *carLogo = [UIImageView new];
    carLogo = [[UIImageView alloc] initWithFrame:CGRectMake(24, 24, 48, 48)];
    carLogo.image = [UIImage imageNamed:@"xp_car"];
    [header addSubview:carLogo];
    
    UILabel *statusLabel = [UILabel new];
    statusLabel.textColor = UIColorFromRGB(0x3A3C41);
    statusLabel.font = kFontMedium(16);
    statusLabel.frame = CGRectMake(MaxX(carLogo) + 12, 24, header.mj_w - MaxX(carLogo) - 12 - 12 - 24 - 5, 24);
//    statusLabel.text = [XPOrderDataTool getStatusTitle:self.orderStatus];
    self.statusLabel = statusLabel;
    [header addSubview:statusLabel];
    
    UILabel *statusTipLabel = [UILabel new];
    statusTipLabel.textColor = UIColorFromRGB(0x8A8A8A);
    statusTipLabel.font = kFontRegular(14);
    statusTipLabel.frame = CGRectMake(statusLabel.mj_x, MaxY(statusLabel), statusLabel.mj_w, 24);
//    statusTipLabel.text = @"您的货品正在运输中，请耐心等待";
    self.statusTipLabel = statusTipLabel;
    [header addSubview:statusTipLabel];

    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 98, header.mj_w - 12 * 2, 48);
    [header addSubview:mainView];
    [XPCommonTool cornerRadius:mainView andType:1 radius:8];
    
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontRegular(14);
    titleLabel.textColor = app_font_color;
    titleLabel.text = [NSString stringWithFormat:@"%@  %@",self.expressCompany,self.orderId];
    titleLabel.frame = CGRectMake(12, 0, header.mj_w, mainView.mj_h);
    self.titleLabel = titleLabel;
    [mainView addSubview:titleLabel];
    
    UIButton *pastButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [pastButton setTitle:@"复制" forState:UIControlStateNormal];
    pastButton.frame = CGRectMake(mainView.mj_w - 52, 0,52,mainView.mj_h);
    [pastButton setTitleColor:app_font_color forState:UIControlStateNormal];
    pastButton.titleLabel.font = kFontMedium(14);
    [pastButton addTarget:self action:@selector(pasteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:pastButton];
    
    UIView *sepView = [UIView new];
    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    sepView.frame = CGRectMake(0, mainView.mj_h - 1, mainView.mj_w, 1);
    [mainView addSubview:sepView];
    
    return header;
}

- (UIView *)getTabelViewFooterView {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 12 + kBottomHeight)];
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 0, footerView.mj_w - 12 * 2, 12);
    [footerView addSubview:mainView];
    [XPCommonTool cornerRadius:mainView andType:2 radius:8];
    return footerView;
}


- (void)pasteButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.orderId;
    [[XPShowTipsTool shareInstance] showMsg:@"复制成功!" ToView:k_keyWindow];
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"物流信息";
    [self.view addSubview:self.navBarView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPLogisticsCell *cell = [XPLogisticsCell cellWithTableView:tableView];
    XPLogisticsModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    
    if (self.dataArray.count == 1) {
        cell.topLine.hidden = YES;
        cell.downLine.hidden = YES;
        cell.circleView.backgroundColor = UIColorFromRGB(0x6E6BFF);
        cell.timeLabel.textColor = UIColorFromRGB(0x6E6BFF);
        cell.desLabel.textColor = app_font_color;
//        [XPCommonTool cornerRadius:cell.mainView andType:2 radius:8];
        
    }else if (indexPath.row == 0){
        cell.topLine.hidden = YES;
        cell.downLine.hidden = NO;
        cell.circleView.backgroundColor = UIColorFromRGB(0x6E6BFF);
        cell.timeLabel.textColor = UIColorFromRGB(0x6E6BFF);
        cell.desLabel.textColor = app_font_color;
//        [XPCommonTool cornerRadius:cell.mainView andType:0 radius:8];
        
    }else if (indexPath.row == self.dataArray.count - 1){
        cell.topLine.hidden = NO;
        cell.downLine.hidden = YES;
        cell.circleView.backgroundColor = UIColorFromRGB(0xDEDEDE);
        cell.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
        cell.desLabel.textColor = UIColorFromRGB(0x8A8A8A);
//        [XPCommonTool cornerRadius:cell.mainView andType:2 radius:8];
    }else {
        cell.topLine.hidden = NO;
        cell.downLine.hidden = NO;
        cell.circleView.backgroundColor = UIColorFromRGB(0xDEDEDE);
        cell.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
        cell.desLabel.textColor = UIColorFromRGB(0x8A8A8A);
//        [XPCommonTool cornerRadius:cell.mainView andType:0 radius:8];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = self.heightArray[indexPath.row];
    return height.floatValue;
}


- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_product_express];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"orderId"] = self.orderId;
    paraM[@"tplCode"] = self.tplCode;
    paraM[@"phone"]   = self.phone;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"data"];
            NSArray *modelArray = [XPLogisticsModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.statusLabel.text = TO_STR(data.userData[@"stateText"]);
            self.statusTipLabel.text = TO_STR(data.userData[@"stateDesc"]);
            
            self.dataArray = modelArray;

            self.heightArray = [self getCellHeightArray];
            
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (NSArray *)getCellHeightArray {
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPLogisticsCell *cell = [XPLogisticsCell new];
    for (int i = 0 ; i< self.dataArray.count; i++) {
        XPLogisticsModel *model = self.dataArray[i];
        CGFloat height = [cell cellWithModel:model];
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    return tempArray.copy;
}

- (void)canShowEmptyView {
    if (self.dataArray.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}
@end
