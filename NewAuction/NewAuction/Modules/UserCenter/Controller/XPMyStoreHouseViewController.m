//
//  XPMyStoreHouseViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/11.
//

#import "XPMyStoreHouseViewController.h"
#import "XPSelectItemView.h"
#import "XPProductCell.h"
#import "XPEmptyView.h"
#import "XPAuctionModel.h"
#import "XPAutionPopView.h"
#import "XPTakeGoodsController.h"
#import "XPTakeGoodsDetailController.h"


@interface XPMyStoreHouseViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) XPEmptyView *emptyView1;
@property (nonatomic, strong) XPEmptyView *emptyView2;
@property (nonatomic, strong) XPEmptyView *emptyView3;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//当前页码
@property (nonatomic, assign) NSInteger page;

@property (nonatomic, strong) XPAutionPopView *pop;


@end

@implementation XPMyStoreHouseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
//默认选中第一个
    if (self.entertype == 0) {
        self.currentIndex = 1;
    }else{
        self.currentIndex = self.entertype;
    }
    
    self.page = 1;
    self.dataArray = [NSMutableArray array];
    [self setupUI];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.page = 1;
    [self getListData];
    
}
- (void)setupUI {
    
    XPSelectItemView *selectItemView = [[XPSelectItemView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, 40)];
    [selectItemView configWithtitleArray:@[@"参拍",@"待提货",@"已提货"]];
    [self.view addSubview:selectItemView];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.tableView.frame = CGRectMake(0, MaxY(selectItemView), SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - 40);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:self.tableView];

    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.mj_w, self.safeBottom)];
    
    XPWeakSelf
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf headRefresh];
    }];
    
    [selectItemView setTapBlock:^(NSInteger index) {
        //网络请求
        weakSelf.currentIndex = index;
        weakSelf.page = 1;
        [weakSelf.dataArray removeAllObjects];
        [weakSelf getListData];
    }];
    
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    
    self.emptyView1 = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView1.hidden = YES;
    self.tableView.hidden = NO;
    [self.emptyView1 configWithImage:[UIImage imageNamed:@"xp_empty_storeHouse"] Description:@"您还没有参拍商品"];
    [self.view addSubview:self.emptyView1];
    self.emptyView2 = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView2.hidden = YES;
    [self.emptyView2 configWithImage:[UIImage imageNamed:@"xp_empty_storeHouse"] Description:@"您还没有待提货商品"];
    [self.view addSubview:self.emptyView2];
    
    self.emptyView3 = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView3.hidden = YES;
    [self.emptyView3 configWithImage:[UIImage imageNamed:@"xp_empty_storeHouse"] Description:@"您还没有已提货商品"];
    [self.view addSubview:self.emptyView3];
    selectItemView.defaultSeletIndex = self.entertype - 1;
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"我的仓库";
    [self.view addSubview:self.navBarView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPProductCell *cell = [XPProductCell cellWithTableView:tableView];
    // //1 委托参拍商品  2 委托委拍商品  3 仓库参拍 4 仓库待提货 5 仓库已提货
    if (self.currentIndex == 1) {
        cell.cellType = 3;
    }else if (self.currentIndex == 2){
        cell.cellType = 4;
    }else if (self.currentIndex == 3) {
        cell.cellType = 5;
    }
    XPAuctionModel *model = self.dataArray[indexPath.row];
    
    [cell cellWithModel:model];
    
    XPWeakSelf;
    
    [cell setLeftBlock:^(NSString * _Nonnull extra) {
        if (cell.cellType == 3) {
            [weakSelf.xpAlertView configWithTitle:@"确定要委拍商品吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                
            } rightAction:^(NSString * _Nullable extra) {
                [weakSelf delegateButtonClicked:indexPath model:model];
            } alertType:XPAlertTypeNormal];
        }
        
    }];
    
    [cell setRightBlock:^(NSString * _Nonnull extra) {
        if (cell.cellType == 3) {
            [weakSelf.xpAlertView configWithTitle:@"确定要利润结算吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                
            } rightAction:^(NSString * _Nullable extra) {
                [weakSelf profitSettlementWithModel:model indexPath:indexPath];
            } alertType:XPAlertTypeNormal];
            
        }else if (cell.cellType == 4) {
            [weakSelf pickGoodsWithModel:model];
        }else if (cell.cellType == 5){//订单详情
            [weakSelf showDetailWithModel:model];
        }
    }];
    
    return cell;
}

- (void)pickGoodsWithModel:(XPAuctionModel *)model {
    XPTakeGoodsController *vc = [XPTakeGoodsController new];
    vc.goodsNum = model.productId;
    vc.productSkuId = model.productSkuId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showDetailWithModel:(XPAuctionModel *)model{
    
        XPTakeGoodsDetailController *vc = [XPTakeGoodsDetailController new];
        vc.goodsNum = model.autionId;
        [self.navigationController pushViewController:vc animated:YES];
}

//委拍
- (void)delegateButtonClicked:(NSIndexPath *)indexPath model:(XPAuctionModel *)model {
    
    XPAutionPopView *pop = [[XPAutionPopView alloc] initWithFrame:self.view.bounds];
    self.pop = pop;
    [k_keyWindow addSubview:pop];
    [pop showWithPrice:model.pricePar totalPrice:model.amountLastPar count:model.amountLast.integerValue imageUrl:model.image title:model.name];
    
//    [pop showWithPrice:model.amountLastPar maxCount:model.amountLast.integerValue enterType:2 imageUrl:model.image title:model.name];
    XPWeakSelf
    [pop setActionBlock:^(NSInteger num) {
        [weakSelf joinActionWithCount:num itemId:model.autionId indexPath:indexPath];
    }];
    self.pop = pop;
}

//仓库委拍
- (void)joinActionWithCount:(NSInteger)num  itemId:(NSNumber *)productID indexPath:(NSIndexPath *)indexPath{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host, kProduct_store_delegate];
    
    NSDictionary *paraM = @{@"number":@(num),
                            @"orderId":productID};
    
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {

        if (data.isSucess) {
            
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
            [self.pop removeFromSuperview];
            
            XPAuctionModel *model = self.dataArray[indexPath.row];
            model.remainingCommissionableQuantity = @(0);
            model.commissionedGoodsButtonStatus = @(1);
            model.amountLast = @(0);
            model.amountLastPar = @(0);
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
        
    }];
}

//利润结算
- (void)profitSettlementWithModel:(XPAuctionModel *)model indexPath:(NSIndexPath *)indexPath{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host, kVERIFY_money_count];
    
    NSDictionary *paraM = @{@"id":model.autionId};
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
            [self.pop removeFromSuperview];
            
            XPAuctionModel *model = self.dataArray[indexPath.row];
            model.commissionedGoodsButtonStatus = @(0);
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
        
    }];
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 192;
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getListData {
    NSString *url = [self getCurrentUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(10);
    if (self.currentIndex == 1) {
        paraM[@"orderStatus"] = @(1);
    }
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = [NSArray array];
            if (self.currentIndex == 1) {
                dataArray = data.userData[@"list"];
            }else {
                dataArray = data.userData[@"records"];
            }
            NSArray *modelArray = [XPAuctionModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            
            if (dataArray.count == 0) {
                [self showEmptyView:YES];
            }else {
                [self showEmptyView:NO];
            }
            
        }else {
            [self showEmptyView:YES];
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)headRefresh{
    self.page = 1;
    
    NSString *urlStr = [self getCurrentUrlPath];
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(10);
    if (self.currentIndex == 1) {
        paraM[@"orderStatus"] = @(1);
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = [NSArray array];
            self.dataArray = [NSMutableArray array];
            if (self.currentIndex == 1) {
                dataArray = data.userData[@"list"];
            }else {
                dataArray = data.userData[@"records"];
            }
            NSArray *modelArray = [XPAuctionModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = [modelArray mutableCopy];
            [self.tableView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *urlStr = [self getCurrentUrlPath];
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(10);
    if (self.currentIndex == 1) {
        paraM[@"orderStatus"] = @(1);
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = [NSArray array];
            if (self.currentIndex == 1) {
                dataArray = data.userData[@"list"];
            }else {
                dataArray = data.userData[@"records"];
            }
            
            if (dataArray.count == 0) {
                self.page --;
            }else {
                NSArray *modelArray = [XPAuctionModel mj_objectArrayWithKeyValuesArray:dataArray];
                [self.dataArray addObjectsFromArray:modelArray];
                [self.tableView reloadData];
            }
        }else {
            self.page --;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//获取当前选中页请求地址
- (NSString *)getCurrentUrlPath {
    NSString *urlStr;
    if (self.currentIndex == 1) {//参拍
       urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kProduct_store_auction_list];
    }else if(self.currentIndex == 2){//待提货
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kProduct_store_wait_send];
    }else {//已提货
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_pick_goods_list];
    }
    return urlStr;
}
- (void)showEmptyView:(BOOL)shouldShow {
    
    self.tableView.hidden = shouldShow;
    if (self.currentIndex == 1) {//参拍
        self.emptyView1.hidden = !shouldShow;
        self.emptyView2.hidden =YES;
        self.emptyView3.hidden =YES;
    }else if(self.currentIndex == 2){//待提货
        self.emptyView2.hidden = !shouldShow;
        self.emptyView1.hidden =YES;
        self.emptyView3.hidden =YES;
    }else {//已提货
        self.emptyView3.hidden = !shouldShow;
        self.emptyView1.hidden = YES;
        self.emptyView2.hidden = YES;
    }
}


@end
