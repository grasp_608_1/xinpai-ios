//
//  XPMoreServiceController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/22.
//

#import "XPMoreServiceController.h"
#import "XPUserCenterItemView.h"
#import "XPMyFansViewController.h"
#import "XPMyFavoriteViewController.h"
#import "XPMyBankCardController.h"
#import "XPShareUMView.h"
#import "XPUMShareTool.h"
#import "XPBaseWebViewViewController.h"

@interface XPMoreServiceController ()

@property (nonatomic, strong) NSArray *myServiceArray;

@property (nonatomic, strong) XPUserCenterItemView *itemView;

@end

@implementation XPMoreServiceController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    
    [self initData];
    [self setupUI];
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"更多服务";
    [self.view addSubview:self.navBarView];
}
- (void)setupUI {
    self.itemView = [[XPUserCenterItemView alloc] initWithFrame:CGRectMake(12, kTopHeight + 12, self.view.mj_w - 24, 130)];
    CGFloat itemViewH = [self.itemView configWithtitle:@""dataArray:self.myServiceArray MaxCount:self.myServiceArray.count showMoreButton:YES showTopView:NO];
    self.itemView.frame = CGRectMake(12, kTopHeight + 12, self.view.mj_w - 24, itemViewH + 12);
    [self.view addSubview:self.itemView];
    
    
    XPWeakSelf
    [self.itemView setItemSelectBlock:^(NSUInteger index) {
        
        if (![[XPVerifyTool shareInstance] checkLogin]) {
            [[XPVerifyTool shareInstance] userShouldLoginFirst];
            return;
        }
        
        NSDictionary *dic = weakSelf.myServiceArray[index];
        [weakSelf pushByItemId:[dic[@"mid"] intValue]];
    }];
    
    
}


- (void)pushByItemId:(NSInteger)mId{
    
    if (mId == 1) {//分享
        
        bool installWechat=  [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession];
        
        if (!installWechat) {
            [[XPShowTipsTool shareInstance] showMsg:@"功能暂不支持" ToView:self.view];
            return;
        }
        
        [self getShareUrl];
        
    }else if (mId == 2){//我的粉丝
        XPMyFansViewController *vc = [XPMyFansViewController new];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if (mId == 3){//我的收藏
        
        XPMyFavoriteViewController *vc = [XPMyFavoriteViewController new];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if (mId == 4){//支付卡
        XPMyBankCardController *vc = [XPMyBankCardController new];
        vc.enterType = 0;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (mId == 5){//结算卡
        XPMyBankCardController *vc = [XPMyBankCardController new];
        vc.enterType = 1;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (mId == 6){//我的客服
        XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
        vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,k_user_help];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (void)initData {
    //我的服务
    self.myServiceArray = @[
        @{
            @"name":@"分享APP",
            @"iconName":@"xp_usercenter_g5",
            @"mid":@"1"
        },@{
            @"name":@"我的粉丝",
            @"iconName":@"xp_usercenter_g6",
            @"mid":@"2"
        },@{
            @"name":@"我的收藏",
            @"iconName":@"xp_usercenter_g7",
            @"mid":@"3"
        },
        //@{
//            @"name":@"支付卡",
//            @"iconName":@"xp_usercenter_g8",
//            @"mid":@"4"
//        }
          @{
            @"name":@"结算卡",
            @"iconName":@"xp_usercenter_g9",
            @"mid":@"5"
        },@{
            @"name":@"我的客服",
            @"iconName":@"xp_usercenter_g10",
            @"mid":@"6"
        }
    ];
    
}

#pragma mark -- 分享相关
//使用友盟sdk
- (void)getShareUrl {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_product_share];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:nil requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            //分享
            NSDictionary *shareDic = data.userData;
            if (!shareDic || shareDic.allKeys.count == 0) {
                //后台数据不对,暂不拉起
            }else {
                [self shareWithdata:shareDic];
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:UIApplication.sharedApplication.keyWindow];
        }
    }];
}
- (void)shareWithdata:(NSDictionary *)shareDic {
    
    XPShareUMView *shareView = [[XPShareUMView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
    [[UIApplication sharedApplication].keyWindow addSubview:shareView];
    [shareView show];
    
    XPWeakSelf
    [shareView setActionBlock:^(NSInteger index) {
        if (index == 1) {//分享好友
            
            [weakSelf shareWithPlatform:UMSocialPlatformType_WechatSession shareWithdata:shareDic];
        }else if (index == 2){
            [weakSelf shareWithPlatform:UMSocialPlatformType_WechatTimeLine shareWithdata:shareDic];
        }
        
    }];
}

- (void)shareWithPlatform:(UMSocialPlatformType)platformType shareWithdata:(NSDictionary *)shareDic{
    
    [[XPUMShareTool shareInstance] shareWebUrlWithPlatform:platformType title:shareDic[@"title"]?: @"分享"
                                                     descr:shareDic[@"content"]?:@"新拍分享链接"
                                              webUrlString:shareDic[@"shareAddress"]?:@""
                                     currentViewController:self
                                                completion:^(id result, NSError *error) {
        
        if(error){
        NSLog(@"************Share fail with error %@*********",error);
    //        [[XPShowTipsTool shareInstance] showMsg:@"分享失败" ToView:self.view];
        }else{
        NSLog(@"response data is %@",result);
    //        [[XPShowTipsTool shareInstance] showMsg:@"分享成功" ToView:self.view];
        }
    }];
}
@end
