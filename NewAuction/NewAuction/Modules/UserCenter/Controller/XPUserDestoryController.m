//
//  XPUserDestoryController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/8/26.
//

#import "XPUserDestoryController.h"

@interface XPUserDestoryController ()
@property (nonatomic, strong) UIButton *logoutButton;

@end

@implementation XPUserDestoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupUI];
}

- (void)setupUI {
    
    NSString *titleString = @"为了保证你的账号安全,在您提交的注销申请生效前,需确认满足以下条件:";
    NSString *desString = @" 1、账号处于安全状态,无盗号风险\n\n 2、账号财产已结清,未存在资产、未结清的资金情况,本账号及通过本账号接入的第三方中没有未完成或存在争议的服务\n\n 3、您确认要放弃虚拟资产(包括但不限于优惠券,积分等数据)\n\n 4、账号无任何纠纷,包括投诉举报";
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontBold(18);
    titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel.numberOfLines = NO;
    titleLabel.frame = CGRectMake(24, kTopHeight + 12,SCREEN_WIDTH - 48,100);
    [self.view addSubview:titleLabel];
    
    UILabel *deslabel = [UILabel new];
    deslabel.font = kFontBold(14);
    deslabel.textColor = UIColorFromRGB(0x8A8A8A);
    deslabel.numberOfLines = NO;
    deslabel.frame = CGRectMake(12, MaxY(titleLabel),SCREEN_WIDTH - 24,200);
    [self.view addSubview:deslabel];
    
    titleLabel.text= titleString;
    deslabel.text = desString;
    
    self.logoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.logoutButton.layer.cornerRadius = 25;
    [self.logoutButton setTitle:@"确认注销" forState:UIControlStateNormal];
    [self.logoutButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    self.logoutButton.titleLabel.font = kFontMedium(14);
    [self.logoutButton addTarget:self action:@selector(logOut:) forControlEvents:UIControlEventTouchUpInside];
    self.logoutButton.frame = CGRectMake(20,SCREEN_HEIGHT - kBottomHeight - 50, SCREEN_WIDTH - 20*2, 50);
    self.logoutButton.backgroundColor = UIColorFromRGB(0xFF4A4A);
    [self.view addSubview:self.logoutButton];
    
}

-(void)logOut:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPWeakSelf
    
    [self.xpAlertView configWithTitle:@"提交注销后:\n我们将为您在5个工作日内完成核实注销,您确定要注销账号吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
        
    } rightAction:^(NSString * _Nullable extra) {
        [weakSelf destoryUser];
    } alertType:XPAlertTypeNormal];
}


- (void)destoryUser {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_user_destory];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [self.navigationController popToRootViewControllerAnimated:NO];
            [[NSNotificationCenter defaultCenter] postNotificationName:Notification_user_login_out object:nil];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"注销账户";
    [self.view addSubview:self.navBarView];
}

@end
