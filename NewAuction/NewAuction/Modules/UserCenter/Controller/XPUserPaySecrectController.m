//
//  XPUserPaySecrectController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/13.
//

#import "XPUserPaySecrectController.h"
#import "NSTimer+JKBlocks.h"

@interface XPUserPaySecrectController ()
@property (nonatomic, strong) UITextField *textField1;
@property (nonatomic, strong) UITextField *textField2;
@property (nonatomic, strong) UITextField *textField3;
@property (nonatomic, strong) UITextField *textField4;
//验证码点击按钮
@property (nonatomic, strong) UIButton *codeButton;

@property (nonatomic, copy) NSString *codeUUid;
//计时器
@property(nonatomic,strong)NSTimer * oneMinTimer;

@end

@implementation XPUserPaySecrectController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [self setupUI];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:self.navBarView];
    self.navBarView.backgroundColor = UIColor.clearColor;
    
    if (self.enterType == 1) {
        self.navBarView.titleLabel.text = @"忘记密码";
    }else if (self.enterType == 2) {
        self.navBarView.titleLabel.text = @"设置登录密码";
    }else {
        self.navBarView.titleLabel.text = @"设置支付密码";
    }
    
}

- (void)setupUI {
    
    CGFloat leftMargin = 24;
    
    UILabel *label1 = [UILabel new];
    label1.frame = CGRectMake(leftMargin, kTopHeight + 32, 250, 24);
    label1.font = kFontRegular(14);
    label1.textColor = UIColorFromRGB(0x8A8A8A);
    label1.text = @"手机号";
    [self.view addSubview:label1];
    
    UITextField *textfield1 = [[UITextField alloc] initWithFrame:CGRectMake(leftMargin, MaxY(label1) + 5, self.view.mj_w - 24*2 , 24)];
    textfield1.placeholder = @"请输入手机号";
    textfield1.font = kFontMedium(14);
    textfield1.textColor = UIColorFromRGB(0x3A3C41);
    [self.view addSubview:textfield1];
    self.textField1 = textfield1;
    if (self.enterType == 1) {
        textfield1.userInteractionEnabled = YES;
    }else {
        textfield1.userInteractionEnabled = NO;
    }
    
    
    UIView *sepView1 = [[UIView alloc] init];
    [sepView1 setBackgroundColor: UIColorFromRGB(0xF2F2F2)];
    sepView1.frame = CGRectMake(leftMargin, MaxY(textfield1) + 4, self.view.mj_w - 24 * 2, 1);
    [self.view addSubview:sepView1];
    
    
    UILabel *label2 = [UILabel new];
    label2.frame = CGRectMake(leftMargin, MaxY(sepView1) + 32, 250, 24);
    label2.font = kFontRegular(14);
    label2.textColor = UIColorFromRGB(0x8A8A8A);
    label2.text = @"验证码";
    [self.view addSubview:label2];
    
    UITextField *textfield2 = [[UITextField alloc] initWithFrame:CGRectMake(leftMargin, MaxY(label2) + 5, self.view.mj_w - 24*2 - 70 , 24)];
    textfield2.placeholder = @"请输入验证码";
    textfield2.font = kFontMedium(14);
    textfield2.textColor = UIColorFromRGB(0x3A3C41);
    textfield2.keyboardType = UIKeyboardTypeURL;
    if (@available(iOS 12.0, *)) {
        textfield2.textContentType = UITextContentTypeOneTimeCode;
    }
    [self.view addSubview:textfield2];
    self.textField2 = textfield2;
    
    //验证码
    self.codeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.codeButton.frame = CGRectMake(self.view.mj_w - 24 - 70, MaxY(label2), 70,32);
    [self.codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.codeButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    self.codeButton.titleLabel.font = kFontRegular(14);
    [self.codeButton addTarget:self action:@selector(codeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.codeButton];
    
    UIView *sepView2 = [[UIView alloc] init];
    [sepView2 setBackgroundColor: UIColorFromRGB(0xF2F2F2)];
    sepView2.frame = CGRectMake(leftMargin, MaxY(textfield2) + 4, self.view.mj_w - 24 * 2, 1);
    [self.view addSubview:sepView2];
    
    
    UILabel *label3 = [UILabel new];
    label3.frame = CGRectMake(leftMargin, MaxY(sepView2) + 32, 250, 24);
    label3.font = kFontRegular(14);
    label3.textColor = UIColorFromRGB(0x8A8A8A);
    label3.text = @"密码";
    [self.view addSubview:label3];
    
    UITextField *textfield3 = [[UITextField alloc] initWithFrame:CGRectMake(leftMargin, MaxY(label3) + 5, self.view.mj_w - 24*2 , 24)];
    textfield3.placeholder = @"请输入密码";
    textfield3.font = kFontMedium(14);
    textfield3.textColor = UIColorFromRGB(0x3A3C41);
    textfield3.secureTextEntry = YES;
    [self.view addSubview:textfield3];
    self.textField3 = textfield3;
    
    UIView *sepView3 = [[UIView alloc] init];
    [sepView3 setBackgroundColor: UIColorFromRGB(0xF2F2F2)];
    sepView3.frame = CGRectMake(leftMargin, MaxY(textfield3) + 4, self.view.mj_w - 24 * 2, 1);
    [self.view addSubview:sepView3];
    
    UILabel *label4 = [UILabel new];
    label4.frame = CGRectMake(leftMargin, MaxY(sepView3) + 32, 250, 24);
    label4.font = kFontRegular(14);
    label4.textColor = UIColorFromRGB(0x8A8A8A);
    label4.text = @"确认密码";
    [self.view addSubview:label4];
    
    UITextField *textfield4 = [[UITextField alloc] initWithFrame:CGRectMake(leftMargin, MaxY(label4) + 5, self.view.mj_w - 24*2 , 24)];
    textfield4.placeholder = @"请确认密码";
    textfield4.font = kFontMedium(14);
    textfield4.textColor = UIColorFromRGB(0x3A3C41);
    textfield4.secureTextEntry = YES;
    [self.view addSubview:textfield4];
    self.textField4 = textfield4;
    
    UIView *sepView4 = [[UIView alloc] init];
    [sepView4 setBackgroundColor: UIColorFromRGB(0xF2F2F2)];
    sepView4.frame = CGRectMake(leftMargin, MaxY(textfield4) + 4, self.view.mj_w - 24 * 2, 1);
    [self.view addSubview:sepView4];
    
    UILabel *descriptionLabel = [UILabel new];
    descriptionLabel.font = kFontRegular(12);
    descriptionLabel.textColor = UIColorFromRGB(0x8A8A8A);
    descriptionLabel.frame = CGRectMake(leftMargin, MaxY(sepView4) +  10, self.view.mj_w - leftMargin, 20);
    [self.view addSubview:descriptionLabel];
    
    //IS_IPHONEX?40:0;
    CGFloat safeBottom =  [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom;
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(12,self.view.mj_h - safeBottom - 50, SCREEN_WIDTH - 2 *12, 50);
    [saveButton setTitle:@"确认" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    saveButton.layer.cornerRadius = 6;
    saveButton.clipsToBounds = YES;
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveButton];
    
    self.textField1.text = [XPLocalStorage readUserUserPhone];
    
    if (self.enterType == 1 || self.enterType == 2) {
        descriptionLabel.text = @"温馨提示:登录密码由8~20位字母、数字和特殊字符-!组成哦~";

    }else if(self.enterType == 3){
        
        descriptionLabel.text = @"温馨提示:支付密码由6位数字组成哦~";
    }
    
}

//发送验证码
-(void)codeButtonClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    if (![self validVerifyPredicate:phonePredicate TargetString:self.textField1.text]) {
        [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的手机号！" ToView:self.view];
        return;
    }else {
        
        NSNumber *codeType;
        if (self.enterType == 1 || self.enterType == 2) {
            codeType = @(3);
        }else if (self.enterType == 3){
            codeType = @4;
        }
        NSString *codeUrlString = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Get_Code];
        NSDictionary *paraM = @{@"phoneNum":self.textField1.text,@"type":codeType};
        [self startLoadingGif];
        [[XPRequestManager shareInstance] commonPostWithURL:codeUrlString Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
            [self stopLoadingGif];
            if (data.isSucess) {
                self.codeUUid = data.userData;
                [self.textField2 becomeFirstResponder];
                [self countDown];
                [[XPShowTipsTool shareInstance]showMsg:@"验证码发送成功!" ToView:self.view];
            }else {
            [[XPShowTipsTool shareInstance]showMsg:data.msg ToView:self.view];
            }
        }];
    }
    
}

//正则
-(BOOL)validVerifyPredicate:(NSString *) pString TargetString:(NSString *)targerString
{
   NSPredicate *num = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pString];
   return [num evaluateWithObject:targerString];
}

#pragma mark -- 倒计时
-(void)countDown{
    
    XPWeakSelf
    __block int i = 60;
        //一分钟倒计时
    self.oneMinTimer = [NSTimer jk_timerWithTimeInterval:1 block:^{
        if (i >= 0) {
            [weakSelf.codeButton setTitle:[NSString stringWithFormat:@" %d秒 ",i] forState:0];
            i--;
            weakSelf.codeButton.enabled = NO;
            weakSelf.codeButton.alpha = 0.5;
        } else{
            [weakSelf.oneMinTimer invalidate];
            weakSelf.oneMinTimer = nil;
            [ weakSelf.codeButton setTitle:@"获取验证码" forState:0];
            weakSelf.codeButton.enabled = YES;
            weakSelf.codeButton.alpha = 1.0;
        }
    } repeats:YES];
    [[NSRunLoop mainRunLoop]addTimer:self.oneMinTimer forMode:NSRunLoopCommonModes];
    [self.oneMinTimer fire];
}

- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    NSString *codeStr = [[XPVerifyTool shareInstance] verifyCode:self.textField2.text];
    if (![self validVerifyPredicate:phonePredicate TargetString:self.textField1.text]) {
        [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的手机号" ToView:self.view];
        return;
    }else if (codeStr){
        [[XPShowTipsTool shareInstance] showMsg:codeStr ToView:self.view];
        return;
    }else {
        if (self.enterType == 1 || self.enterType == 2) {
            //登录密码
            if (self.textField3.text.length < 8 || self.textField3.text.length > 20) {
                [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的密码" ToView:self.view];
                return;
            }else if (![self validVerifyPredicate:@"^(?=.*[A-Za-z])(?=.*\\d)(?=.*[-!%])[A-Za-z\\d-!%]{8,20}$" TargetString:self.textField3.text]){
                [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的密码！" ToView:self.view];
            }else if (![self.textField3.text isEqualToString:self.textField4.text]) {
                [[XPShowTipsTool shareInstance] showMsg:@"两次输入密码不一致！" ToView:self.view];
                return;
            }else {
                //上传到服务器
                NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
                paraM[@"password"] = self.textField3.text;
                paraM[@"code"] = self.textField2.text;
                paraM[@"uuid"] = self.codeUUid;
                paraM[@"phone"] = self.textField1.text;
                
                [self postToSeverWithDIc:paraM];
            }
            
            
        }else {//支付密码
            if (![self validVerifyPredicate:codePredicate TargetString:self.textField3.text]){
                [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的密码！" ToView:self.view];
                return;
            }else if (![self.textField3.text isEqualToString:self.textField4.text]) {
                [[XPShowTipsTool shareInstance] showMsg:@"两次输入密码不一致！" ToView:self.view];
                return;
            }else {
                //上传到服务器
                NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
                paraM[@"password"] = self.textField3.text;
                paraM[@"code"] = self.textField2.text;
                paraM[@"uuid"] = self.codeUUid;
                [self postToSeverWithDIc:paraM];
            }
        }
        
    }
}

- (void)postToSeverWithDIc:(NSDictionary *)paraM{
    NSString *urlString;
    if (self.enterType == 1  ||self.enterType == 2) {//登录密码
        urlString = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_User_login_password];
    }else {//支付密码
        urlString =[NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_User_pay_password];
    }
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:urlString Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSString *toastStr;
            if (self.enterType == 1 || self.enterType == 2) {
                toastStr = @"登录密码设置成功";
            }else {
                toastStr = @"支付密码设置成功";
            }
       //记录当前用户信息
            [[XPShowTipsTool shareInstance] showMsg:toastStr ToView:k_keyWindow];
            [self goBack];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}


-(void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
@end
