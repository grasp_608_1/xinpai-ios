//
//  XPUserAddressController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/13.
//

#import "XPBaseViewController.h"
#import "XPAdressModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface XPUserAddressController : XPBaseViewController

@property (nonatomic, assign) NSInteger entryType;

@property (nonatomic, copy) void(^chooseAdress)(XPAdressModel *model);

@end

NS_ASSUME_NONNULL_END
