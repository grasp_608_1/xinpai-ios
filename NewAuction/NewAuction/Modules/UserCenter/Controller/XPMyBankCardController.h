//
//  XPMyBankCardController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/17.
//

#import "XPBaseViewController.h"
#import "XPBankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPMyBankCardController : XPBaseViewController

@property (nonatomic, assign) NSInteger enterType;

@property (nonatomic, copy) void(^chooseBankBlock)(XPBankModel *bankModel);

@end

NS_ASSUME_NONNULL_END
