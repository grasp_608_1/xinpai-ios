//
//  XPTrendDetailViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/18.
//

#import "XPTrendDetailViewController.h"
#import "XPEmptyView.h"
#import "XPTrendModel.h"
#import "XPTrendDetailCell.h"
#import "XPTrendTapHeaderView.h"

@interface XPTrendDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) XPEmptyView *emptyView;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//当前页码
@property (nonatomic, assign) NSInteger page;
//可用积分
@property (nonatomic, strong) UILabel *avaiableScoreLabel;
//累计积分
@property (nonatomic, strong) UILabel *totalScoreLabel;

@property (nonatomic, strong) XPTrendTapHeaderView *headerView;

//当前类型 0 现金 1 货款
@property (nonatomic, assign) NSInteger currentIndex;

@end

@implementation XPTrendDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    self.dataArray = [NSMutableArray array];
    self.page = 1;
    
    [self setupUI];
    [self getData];

}

- (void)setupUI {
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    self.tableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.safeBottom)];
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    if (self.enterType == 4 ||self.enterType == 5  || self.enterType == 10 ){
       self.headerView = [[XPTrendTapHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.mj_w, 80)];
       self.tableView.tableHeaderView = self.headerView;
   }else if (self.enterType == 11 ){
        self.tableView.tableHeaderView = [self createRechargeTableViewHeaderView];
    }
    else if ( self.enterType == 12){
        self.tableView.tableHeaderView = [self createDefaultTableViewHeaderView];
    }else if ( self.enterType == 13){
        self.tableView.tableHeaderView = [self createDefaultTableViewHeaderView];
    }else {
        self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 12)];
    }
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无明细"];
    [self.view addSubview:self.emptyView];
    
    XPWeakSelf
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [self headerRefresh];
    }];
    
    [self callback];
    
}

- (void)callback {
    
    XPWeakSelf
    
    [self.headerView setItemBlock:^(NSInteger tag) {
        weakSelf.currentIndex = tag;
        [weakSelf getData];
    }];
    
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:
     @selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    if (self.enterType != 10) {
        self.navBarView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    }
    [self.view addSubview:self.navBarView];
    ////1全部账单     2参拍明细       3委拍明细     4收益明细  5盈利明细
    ///6盈利兑换明细   7收益兑换明细 8货款转出明细
    ///9支付货款明细  10我的积分  11充值明细
    /// 12 提现明细
    if (self.enterType == 1) {
        self.navBarView.titleLabel.text = @"全部账单";
    }else if (self.enterType == 2){
        self.navBarView.titleLabel.text = @"参拍明细";
    }else if (self.enterType == 3){
        self.navBarView.titleLabel.text = @"委拍明细";
    }else if (self.enterType == 4){
        self.navBarView.titleLabel.text = @"补贴明细";
    }else if (self.enterType == 5){
        self.navBarView.titleLabel.text = @"收入明细";
    }else if (self.enterType == 6){
        self.navBarView.titleLabel.text = @"收入兑换明细";
    }else if (self.enterType == 7){
        self.navBarView.titleLabel.text = @"补贴兑换明细";
    }else if (self.enterType == 8){
        self.navBarView.titleLabel.text = @"保证金转出明细";
    }else if (self.enterType == 9){
        self.navBarView.titleLabel.text = @"支付保证金明细";
    }else if (self.enterType == 10){
        self.navBarView.titleLabel.text = @"我的积分";
    }else if (self.enterType == 11){
        self.navBarView.titleLabel.text = @"保证金明细";
    }else if (self.enterType == 12){
        self.navBarView.titleLabel.text = @"提现明细";
    }else if (self.enterType == 13){
        self.navBarView.titleLabel.text = @"余额明细";
    }
}

- (UIView *)createScoreTableViewHeaderView {
    UIView *headerView = [UIView new];
    headerView.frame = CGRectMake(0, 0, self.view.mj_w,12 + 84 + 12);
    
    UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(12, 12, self.view.mj_w - 24, 84)];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.layer.cornerRadius = 8;
    [headerView addSubview:mainView];
    
    self.avaiableScoreLabel = [UILabel new];
    self.avaiableScoreLabel.font = kFontMedium(16);
    self.avaiableScoreLabel.textColor = app_font_color;
    self.avaiableScoreLabel.textAlignment = NSTextAlignmentCenter;
    self.avaiableScoreLabel.frame = CGRectMake(0, 16, mainView.mj_w, 24);
    [mainView addSubview:self.avaiableScoreLabel];
    
    self.totalScoreLabel = [UILabel new];
    self.totalScoreLabel.font = kFontMedium(12);
    self.totalScoreLabel.textColor = app_font_color_8A;
    self.totalScoreLabel.textAlignment = NSTextAlignmentCenter;
    self.totalScoreLabel.frame = CGRectMake(0, MaxY(self.avaiableScoreLabel) + 4, mainView.mj_w, 24);
    [mainView addSubview:self.totalScoreLabel];

    return headerView;
}

- (UIView *)createDefaultTableViewHeaderView {
    UIView *headerView = [UIView new];
    headerView.frame = CGRectMake(0, 0, self.view.mj_w,12 + 24 + 12);

    self.totalScoreLabel = [UILabel new];
    self.totalScoreLabel.font = kFontMedium(16);
    self.totalScoreLabel.textColor = app_font_color;
    self.totalScoreLabel.textAlignment = NSTextAlignmentCenter;
    self.totalScoreLabel.frame = CGRectMake(0, 12, headerView.mj_w, 24);
    [headerView addSubview:self.totalScoreLabel];
    return headerView;
}

- (UIView *)createRechargeTableViewHeaderView {
    UIView *headerView = [UIView new];
    headerView.frame = CGRectMake(0, 0, self.view.mj_w,12 + 52 + 12);

    self.avaiableScoreLabel = [UILabel new];
    self.avaiableScoreLabel.font = kFontMedium(16);
    self.avaiableScoreLabel.textColor = app_font_color;
    self.avaiableScoreLabel.textAlignment = NSTextAlignmentCenter;
    self.avaiableScoreLabel.frame = CGRectMake(0, 16, headerView.mj_w, 24);
    [headerView addSubview:self.avaiableScoreLabel];
    
    self.totalScoreLabel = [UILabel new];
    self.totalScoreLabel.font = kFontMedium(12);
    self.totalScoreLabel.textColor = app_font_color_8A;
    self.totalScoreLabel.textAlignment = NSTextAlignmentCenter;
    self.totalScoreLabel.frame = CGRectMake(0, MaxY(self.avaiableScoreLabel) + 4, headerView.mj_w, 24);
    [headerView addSubview:self.totalScoreLabel];
    
    return headerView;
}

- (void)freshHeaderWithAvalialbelAmout:(NSNumber *)avaliable avaliableDesc:(NSString *)avaliableStr TotalAmount:(NSNumber *)totalAmount totalDes:(NSString *)totalDesc{

    if(self.enterType == 10){
        self.totalScoreLabel.text = [NSString stringWithFormat:@"%@ %@",totalDesc,totalAmount];
        NSString *avaiableStr = [NSString stringWithFormat:@"%@",avaliable];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",avaliableStr,avaliable]];
        [attr addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0x6E6BFF)} range:NSMakeRange(attr.length - avaiableStr.length, avaiableStr.length)];
        
        self.avaiableScoreLabel.attributedText = attr;
    }else if (self.enterType == 11){
        
        NSString *totalStr = [NSString stringWithFormat:@"%@: ￥%@",totalDesc,totalAmount];
        NSString *avaiableStr = [NSString stringWithFormat:@"%@: ￥%@",avaliableStr,avaliable];
        self.totalScoreLabel.text = totalStr;
        self.avaiableScoreLabel.text = avaiableStr;
        if (totalAmount.intValue < 0) {
            self.totalScoreLabel.hidden = YES;
        }
    }
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPTrendDetailCell *cell = [XPTrendDetailCell cellWithTableView:tableView];
    XPTrendModel *model = self.dataArray[indexPath.row];
    cell.cellType = self.enterType;
    ////1全部账单     2参拍明细       3委拍明细     4收益明细  5盈利明细
    ///6盈利兑换明细   7收益兑换明细 8货款转出明细
    ///9支付货款明细  10我的积分  11充值明细
    /// 12 提现明细 13 佣金提现明细
    if (self.enterType == 1) {//
        [cell cellWithModel:model image:[UIImage imageNamed:@"detail_4"]];
    }else if(self.enterType == 2){//
        [cell cellWithModel:model image:[UIImage imageNamed:@"detail_6"]];
    }else if(self.enterType == 3){//
        [cell cellWithModel:model image:[UIImage imageNamed:@"detail_3"]];
    }else if(self.enterType == 4){//
        [cell cellWithModel:model image:[UIImage imageNamed:@"detail_4"]];
    }else if(self.enterType == 5){//
        [cell cellWithModel:model image:[UIImage imageNamed:@"detail_5"]];
    }else if (self.enterType == 8 || self.enterType == 13){
        [cell cellWithModel:model image:[UIImage imageNamed:@"detail_2"]];
    }else if (self.enterType == 9){
        [cell cellWithModel:model image:[UIImage imageNamed:@"detail_1"]];
    }else if (self.enterType == 10){
        [cell cellWithModel:model image:[UIImage imageNamed:@"detail_2"]];
    }else if (self.enterType == 11){
        [cell cellWithModel:model image:[UIImage imageNamed:@"detail_7"]];
    }else if (self.enterType == 12){
        [cell cellWithModel:model image:[UIImage imageNamed:@"detail_8"]];
    }else {
        [cell cellWithModel:model image:[UIImage imageNamed:@"detail_2"]];
    }
    
    if (self.dataArray.count == 1) {
        [XPCommonTool cornerRadius:cell.mainView andType:3 radius:8];
    }else if (indexPath.row == 0) {
        [XPCommonTool cornerRadius:cell.mainView andType:1 radius:8];
    }else if (indexPath.row == self.dataArray.count - 1) {
        [XPCommonTool cornerRadius:cell.mainView andType:2 radius:8];
    }else {
        [XPCommonTool cornerRadius:cell.mainView andType:3 radius:0];
    }
    
    if (indexPath.row == self.dataArray.count - 1) {
        cell.downLine.hidden = YES;
    }else {
        cell.downLine.hidden = NO;
    }
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 68;
}

- (void)getData {
    NSString *url = [self getCurrentUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    if (self.enterType == 2) {
        paraM[@"orderStatus"] = @(1);
    }
    
    if (self.enterType == 4 || self.enterType == 5) {
        paraM[@"itemsType"] = @(self.currentIndex);
    }
    
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            NSArray *dataArray;
            ////1全部账单     2参拍明细       3委拍明细     4收益明细  5盈利明细
            ///6盈利兑换明细   7收益兑换明细 8货款转出明细
            ///9支付货款明细  10我的积分  11充值明细
            /// 12 提现明细 13 佣金明细
            if (self.enterType == 4 ){
                [self refreshProfitHeaderViewWithDic:data.userData];

                dataArray = data.userData[@"records"];
                
            }else if (self.enterType == 5 ){
                
                [self refreshIncomeHeaderViewWithDic:data.userData];

                dataArray = data.userData[@"records"];
            }else if (self.enterType == 10) {
                
                NSDictionary *tempDic = @{
                    @"currentPoints":data.userData[@"currentPoints"],
                    @"historyPoints":data.userData[@"historyPoints"],
                    @"usedPoints":[NSNumber numberWithLong:[data.userData[@"historyPoints"] longValue] - [data.userData[@"currentPoints"] longValue]]
                };
                
                [self refreshUserScoreViewWithDic:tempDic];
                
                dataArray = data.userData[@"mallUserPointsHistoryVOS"][@"records"];
            } else if (self.enterType == 11){
                
                [self freshHeaderWithAvalialbelAmout:data.userData[@"totalAmount"] avaliableDesc:@"累计金额: " TotalAmount:data.userData[@"cumulativeRechargeSurplus"] totalDes:@"剩余保证金额度: "];
                dataArray = data.userData[@"records"];
                
            }else if (self.enterType == 12){
                self.totalScoreLabel.text= [NSString stringWithFormat:@"累计金额: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:data.userData[@"totalAmount"]maximumFractionDigits:4]];
                dataArray = data.userData[@"records"];
            } else if (self.enterType == 13){
                self.totalScoreLabel.text= [NSString stringWithFormat:@"累计金额: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:data.userData[@"totalAmount"]maximumFractionDigits:4]];
                dataArray = data.userData[@"records"];
            }else {
                dataArray = data.userData[@"records"];
            }
            NSArray *modelArray = [XPTrendModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            
            if (self.dataArray.count == 0) {
                self.emptyView.hidden = NO;
                self.tableView.hidden = YES;
            }else {
                self.emptyView.hidden = YES;
                self.tableView.hidden = NO;
            }
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
            if (self.dataArray.count == 0) {
                self.emptyView.hidden = NO;
                self.tableView.hidden = YES;
            }else {
                self.emptyView.hidden = YES;
                self.tableView.hidden = NO;
            }
        }
    }];
}

//收益头部信息
- (void)refreshProfitHeaderViewWithDic:(NSDictionary *)dic {
    [self.headerView freshWithTopTitle:[NSString stringWithFormat:@"补贴总金额: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:dic[@"totalAmount"]maximumFractionDigits:4]]
                              letTitle:@"补贴现金"
                       leftTitleDesStr:@"补贴现金总金额"
                    leftAmountTitleStr:[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:dic[@"typeTotalAmount"]maximumFractionDigits:4]]
                            rightTitle:@"补贴货款"
                     rightTitleDescStr:@"补贴货款总金额"
                      rightAmountTitle:[NSString stringWithFormat:@" ￥%@",[XPMoneyFormatTool formatMoneyWithNum:dic[@"typeTotalAmount"]maximumFractionDigits:4]]];
}

//盈利头部信息
-(void)refreshIncomeHeaderViewWithDic:(NSDictionary *)dic{
    [self.headerView freshWithTopTitle:[NSString stringWithFormat:@"收入总金额: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:dic[@"totalAmount"]maximumFractionDigits:4]]
                              letTitle:@"收入现金"
                       leftTitleDesStr:@"收入现金总金额"
                    leftAmountTitleStr:[NSString stringWithFormat:@" ￥%@",[XPMoneyFormatTool formatMoneyWithNum:dic[@"typeTotalAmount"]maximumFractionDigits:4]]
                            rightTitle:@"收入货款"
                     rightTitleDescStr:@"收入货款总金额"
                      rightAmountTitle:[NSString stringWithFormat:@" ￥%@",[XPMoneyFormatTool formatMoneyWithNum:dic[@"typeTotalAmount"]maximumFractionDigits:4]]];
}

//收益头部信息
- (void)refreshUserScoreViewWithDic:(NSDictionary *)dic {
    [self.headerView freshWithTopTitle:[NSString stringWithFormat:@"累计积分: %@",[XPMoneyFormatTool formatMoneyWithNum:dic[@"historyPoints"]maximumFractionDigits:2]]
                              letTitle:@"可用积分"
                       leftTitleDesStr:@"可用积分"
                    leftAmountTitleStr:[NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:dic[@"currentPoints"]maximumFractionDigits:2]]
                            rightTitle:@"已用积分"
                     rightTitleDescStr:@"已用积分"
                      rightAmountTitle:[NSString stringWithFormat:@" ￥%@",[XPMoneyFormatTool formatMoneyWithNum:dic[@"usedPoints"]maximumFractionDigits:2]]];
}


- (void)headerRefresh{
    NSString *url = [self getCurrentUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    if (self.enterType == 2) {
        paraM[@"orderStatus"] = @(1);
    }
    
    if (self.enterType == 4 || self.enterType == 5) {
        paraM[@"itemsType"] = @(self.currentIndex);
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            
            NSArray *dataArray;
            if (self.enterType == 4 ){
                
                [self refreshProfitHeaderViewWithDic:data.userData];

                dataArray = data.userData[@"records"];
                
            }else if (self.enterType == 5 ){
                
                [self refreshIncomeHeaderViewWithDic:data.userData];
                dataArray = data.userData[@"records"];
            }else if (self.enterType == 10) {
                [self freshHeaderWithAvalialbelAmout:data.userData[@"currentPoints"] avaliableDesc:@"可用积分"  TotalAmount:data.userData[@"historyPoints"] totalDes:@"累计积分"];
                dataArray = data.userData[@"mallUserPointsHistoryVOS"][@"records"];
            }else if (self.enterType == 11){
                
                [self freshHeaderWithAvalialbelAmout:data.userData[@"totalAmount"] avaliableDesc:@"累计金额: " TotalAmount:data.userData[@"cumulativeRechargeSurplus"] totalDes:@"剩余保证金额度: "];
                dataArray = data.userData[@"records"];
                
            }else if (self.enterType == 12){
                self.totalScoreLabel.text= [NSString stringWithFormat:@"累计金额: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:data.userData[@"totalAmount"] maximumFractionDigits:4]];
                dataArray = data.userData[@"records"];
            } else {
                dataArray = data.userData[@"records"];
            }
            
            NSArray *modelArray = [XPTrendModel mj_objectArrayWithKeyValuesArray:dataArray];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:modelArray];
            [self.tableView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *url = [self getCurrentUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    if (self.enterType == 2) {
        paraM[@"orderStatus"] = @(1);
    }
    
    if (self.enterType == 4 || self.enterType == 5) {
        paraM[@"itemsType"] = @(self.currentIndex);
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray;
            if (self.enterType == 4 ){
                
                [self refreshProfitHeaderViewWithDic:data.userData];
                dataArray = data.userData[@"records"];
                
            }else if (self.enterType == 5 ){
                
                [self refreshIncomeHeaderViewWithDic:data.userData];
                dataArray = data.userData[@"records"];
            }else if (self.enterType == 10) {
                [self freshHeaderWithAvalialbelAmout:data.userData[@"currentPoints"] avaliableDesc:@"可用积分"  TotalAmount:data.userData[@"historyPoints"] totalDes:@"累计积分"];
                dataArray = data.userData[@"mallUserPointsHistoryVOS"][@"records"];
            }else if (self.enterType == 11){
                
                [self freshHeaderWithAvalialbelAmout:data.userData[@"totalAmount"] avaliableDesc:@"累计金额: " TotalAmount:data.userData[@"cumulativeRechargeSurplus"] totalDes:@"剩余充值额度: "];
                dataArray = data.userData[@"records"];
                
            }else if (self.enterType == 12){
                self.totalScoreLabel.text= [NSString stringWithFormat:@"累计金额: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:data.userData[@"totalAmount"] maximumFractionDigits:4]];
                dataArray = data.userData[@"records"];
            } else {
                dataArray = data.userData[@"records"];
            }
            
            NSArray *modelArray = [XPTrendModel mj_objectArrayWithKeyValuesArray:dataArray];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self.tableView reloadData];
            }
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

//获取当前选中页请求地址
- (NSString *)getCurrentUrlPath {
    ////1全部账单     2参拍明细       3委拍明细     4收益明细  5盈利明细
    ///6盈利兑换明细   7收益兑换明细 8货款转出明细
    ///9支付货款明细  10我的积分  11充值明细
    /// 12 提现明细 13 佣金提现
    NSString *urlStr;
    if (self.enterType == 1) {//
       urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kDetail_all];
    }else if(self.enterType == 2){//
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kProduct_store_auction_list_new];
    }else if(self.enterType == 3){//
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kDetail_aution_delegate];
    }else if(self.enterType == 4){//
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kDetail_income];
    }else if(self.enterType == 5){//
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kDetail_profit];
    }else if (self.enterType == 6) {//
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kDetail_profit_exchange];
    }else if (self.enterType == 7) {
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kDetail_income_exchange];
    }else if (self.enterType == 8) {
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_money_to_cash_detail];
    }else if (self.enterType == 9) {
        urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kDetail_pay_in];
    }else if (self.enterType == 10) {
        urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kDetail_user_score];
    }else if (self.enterType == 11) {
        urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kDetail_recharge_detail];
    }else if (self.enterType == 12) {
        urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kDetail_cash_detail];
    }else if (self.enterType == 13){
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_money_exchange_detail];
    }
    return urlStr;
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
