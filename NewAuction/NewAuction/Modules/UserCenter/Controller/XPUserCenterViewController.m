//
//  XPUserCenterViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "XPUserCenterViewController.h"
#import "XPUserSettingController.h"
#import "XPUserInfoUpdateController.h"
#import "XPUserInfoModel.h"
#import "XPUserCenterValueView.h"
#import "XPUserCenterItemView.h"
#import "XPShareUMView.h"
#import "XPUMShareTool.h"
#import "XPHitButton.h"
#import "XPMoreServiceController.h"
#import "XPShareViewController.h"
#import "XPUserLevelView.h"
#import "XPShopOrderStatusController.h"
#import "XPUserNoticeViewController.h"
#import "XPUserScorePreUploadController.h"
#import "UIButton+WebCache.h"

@interface XPUserCenterViewController ()

@property (nonatomic, strong) UIScrollView *mainView;
//顶部背景图
@property (nonatomic, strong) UIImageView *topBanner;
//用户头像
@property (nonatomic, strong) UIImageView *userIcon;

@property (nonatomic, strong) UILabel *userNickNameLabel;

@property (nonatomic, strong) XPUserLevelView *userLeveLabel;
//预估价值
@property (nonatomic,strong) XPUserCenterValueView *pValueView;
//库存量
@property (nonatomic, strong) XPUserCenterValueView *totalNumView;
//我的新拍
@property (nonatomic, strong) XPUserCenterItemView *itemView1;
@property (nonatomic, strong) NSArray *myManagerArray;
//我的新拍
@property (nonatomic, strong) XPUserCenterItemView *makerItemView;
@property (nonatomic, strong) NSArray *myMakerArray;
//我的商城
@property (nonatomic, strong) XPUserCenterItemView *shopItemView;
@property (nonatomic, strong) NSArray *myShopArray;
//我的商城
@property (nonatomic, strong) XPUserCenterItemView *creationItemView;
@property (nonatomic, strong) NSArray *myCreationArray;

//我的服务
@property (nonatomic, strong) XPUserCenterItemView *itemView2;
@property (nonatomic, strong) NSArray *myServiceArray;

//用户设置个人中心
@property (nonatomic, strong) UIButton *settingButton;
//站内信
@property (nonatomic, strong) XPHitButton *noticeButton;

@property (nonatomic, strong) XPHitButton *commonSetButton;

@property (nonatomic, strong) XPUserInfoModel *userModel;
//是否展示造物
@property (nonatomic, strong) NSNumber *isShow;

@end

@implementation XPUserCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;;
    [self initData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData) name:Notification_user_login_success object:nil];
}

#pragma mark --- UI
- (void)freshUI {
    
    CGFloat bottom = 0;
    [self.mainView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    self.mainView = [[UIScrollView alloc] init];
    self.mainView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.mainView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kTabBarHeight);
    self.mainView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.mainView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.mainView];
    
    UIImageView *topBanner = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 251)];
    self.topBanner = topBanner;
    topBanner.image = [UIImage imageNamed:@"xp_user_temp1"];
    topBanner.frame = CGRectMake(0, 0, SCREEN_WIDTH, IS_IPHONEX ? 251 :251 - self.safeTop);
    [self.mainView addSubview:topBanner];
    
    self.commonSetButton = [[XPHitButton alloc] init];
    self.commonSetButton.hitRect = 25;
    [self.commonSetButton setImage:[UIImage imageNamed:@"setting_set_log"] forState:UIControlStateNormal];
    self.commonSetButton.frame = CGRectMake(self.mainView.mj_w - 20 - 24, self.safeTop + 17, 20, 20);
    [self.commonSetButton addTarget:self action:@selector(commonSetButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    self.commonSetButton.backgroundColor = UIColor.grayColor;
    [self.mainView addSubview:self.commonSetButton];
    
    self.noticeButton = [[XPHitButton alloc] init];
    self.noticeButton.hitRect = 25;
    [self.noticeButton setImage:[UIImage imageNamed:@"xp_sys_notice"] forState:UIControlStateNormal];
    self.noticeButton.frame = CGRectMake(self.mainView.mj_w - 20 - 24 - 20 - 12, self.safeTop + 17, 20, 20);
    [self.noticeButton addTarget:self action:@selector(noticeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.noticeButton];
    
    self.userIcon = [UIImageView new];
    self.userIcon.frame = CGRectMake(20, self.safeTop + 55, 48, 48);
    self.userIcon.layer.cornerRadius = 24;
    self.userIcon.layer.masksToBounds = YES;
    self.userIcon.layer.borderColor = [UIColor whiteColor].CGColor;
    self.userIcon.layer.borderWidth = 1;
    [self.mainView addSubview:self.userIcon];
    
    self.userNickNameLabel = [UILabel new];
    self.userNickNameLabel.font = kFontMedium(18);
    self.userNickNameLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.userNickNameLabel.frame = CGRectMake(MaxX(self.userIcon) + 10, self.userIcon.frame.origin.y, 200, 24);
    [self.mainView addSubview:self.userNickNameLabel];
    
    
    self.userLeveLabel = [[XPUserLevelView alloc] initWithFrame:CGRectMake(MaxX(self.userIcon) + 10, MaxY(self.userNickNameLabel) + 5, 96, 18)];
    [self.mainView addSubview:self.userLeveLabel];
    
    
    CGFloat valueW = (self.view.mj_w - 12* 5)/2.;
    CGFloat valueH = valueW * 94 /170;
    bottom = MaxY(_userIcon);
    
    CGFloat bannerButtonH = (self.view.mj_w - 24) * 95 /351.;
    UIButton *bannerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [bannerButton setBackgroundImage:[UIImage imageNamed:@"xp_usercenter_join"] forState:UIControlStateNormal];
    bannerButton.frame = CGRectMake(12, bottom + 12, self.view.mj_w - 24, bannerButtonH);
    bannerButton.layer.cornerRadius = 8;
    bannerButton.adjustsImageWhenHighlighted = NO;
    [bannerButton addTarget:self action:@selector(bannerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:bannerButton];
    
    bottom = MaxY(bannerButton);

    if (APPTYPE == 0 || self.isShow.intValue == 1) {
        UIView *myAutionView = [[UIView alloc] init];
        myAutionView.frame = CGRectMake(12, bottom + 12, SCREEN_WIDTH - 12*2, valueH + 236 - 94);
        myAutionView.backgroundColor = [UIColor whiteColor];
        myAutionView.layer.cornerRadius = 8;
        myAutionView.layer.masksToBounds = YES;
        [self.mainView addSubview:myAutionView];
        
        UILabel *autionLabel = [UILabel new];
        autionLabel.text = @"我的新拍";
        autionLabel.font = kFontMedium(16);
        autionLabel.textColor = UIColorFromRGB(0x3A3C41);
        autionLabel.frame = CGRectMake(12, 12, 200, 24);
        [myAutionView addSubview:autionLabel];
        
        self.itemView1 = [[XPUserCenterItemView alloc] initWithFrame:CGRectMake(0, myAutionView.mj_h - 130 + 12, myAutionView.mj_w , 130)];
        self.itemView1.layer.cornerRadius = 0;
        CGFloat itemViewH = [self.itemView1 configWithtitle:@"" dataArray:self.myManagerArray MaxCount:self.myManagerArray.count showMoreButton:NO showTopView:NO];
        self.itemView1.frame = CGRectMake(0, myAutionView.mj_h - 130 + 12 + 12, myAutionView.mj_w , itemViewH);
        [myAutionView addSubview:self.itemView1];
        
        self.pValueView = [[XPUserCenterValueView alloc] initWithFrame:CGRectMake(12, 48, valueW, valueH)];
        [myAutionView addSubview:self.pValueView];
        
        self.totalNumView = [[XPUserCenterValueView alloc] initWithFrame:CGRectMake(24 + valueW, 48, valueW, valueH)];
        [myAutionView addSubview:self.totalNumView];
        
        bottom = MaxY(myAutionView);
    }
    
    //创客
    if (self.userModel.makerId && self.userModel.makerId.intValue > 0) {
        self.makerItemView = [[XPUserCenterItemView alloc] initWithFrame:CGRectMake(12, bottom + 12, self.view.mj_w - 24, 130)];
        [self.makerItemView configWithtitle:@"创客"dataArray:self.myMakerArray MaxCount:4 showMoreButton:NO showTopView:YES];
        [self.mainView addSubview:self.makerItemView];
        
        bottom = MaxY(self.makerItemView);
    }

    self.shopItemView = [[XPUserCenterItemView alloc] initWithFrame:CGRectMake(12, bottom + 12, self.view.mj_w - 24, 130)];
    [self.shopItemView configWithtitle:@"我的商城"dataArray:self.myShopArray MaxCount:4 showMoreButton:YES showTopView:YES];
    self.shopItemView.moreTitle = @"全部订单";
    [self.mainView addSubview:self.shopItemView];
    
    bottom = MaxY(self.shopItemView);

//    self.creationItemView = [[XPUserCenterItemView alloc] initWithFrame:CGRectMake(12, bottom + 12, self.view.mj_w - 24, 130)];
//    [self.creationItemView configWithtitle:@"我的造物"dataArray:self.myCreationArray MaxCount:4 showMoreButton:NO showTopView:YES];
//    [self.mainView addSubview:self.creationItemView];
//    
//    bottom = MaxY(self.creationItemView);
    
    self.itemView2 = [[XPUserCenterItemView alloc] initWithFrame:CGRectMake(12, MaxY(self.shopItemView) + 12, self.view.mj_w - 24, 130)];
    CGFloat severiceH = [self.itemView2 configWithtitle:@"我的服务"dataArray:self.myServiceArray MaxCount:8 showMoreButton:NO showTopView:YES];
    self.itemView2.frame = CGRectMake(12, bottom + 12, self.view.mj_w - 24, severiceH);
    [self.mainView addSubview:self.itemView2];
    
    bottom = MaxY(self.itemView2);
    
    NSString *nickName = [XPLocalStorage readUserNickName];
    self.userNickNameLabel.text = nickName;
    self.userIcon.backgroundColor = UIColor.whiteColor;
    
    //settingButton
    UIButton *settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    settingButton.layer.cornerRadius = 12;
    settingButton.layer.masksToBounds = YES;
    settingButton.backgroundColor = RGBACOLOR(107, 158, 255, 0.6);
    [settingButton addTarget:self action:@selector(userSettinButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    settingButton.frame = CGRectMake(self.mainView.mj_w - 75, self.userIcon.frame.origin.y + 13, 75 + 12, 24);
    [self.mainView addSubview:settingButton];
    [settingButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :settingButton colors:
                                                          @[(__bridge id)UIColorFromRGB(0xB5C9FF).CGColor,
                                                            (__bridge id)UIColorFromRGB(0xC0BBFF).CGColor]] atIndex:0];
    
    UILabel *settingTitleLabel = [UILabel new];
    settingTitleLabel.text = @"个人信息";
    settingTitleLabel.font = kFontMedium(10);
    settingTitleLabel.textColor = UIColor.whiteColor;
    settingTitleLabel.frame = CGRectMake(12, 0, 42, settingButton.frame.size.height);
    [settingButton addSubview:settingTitleLabel];
    
    UIImageView *settingArrowImageView = [[UIImageView alloc] init];
    settingArrowImageView.image  = [UIImage imageNamed:@"xp_user_setting_arrow"];
    settingArrowImageView.frame = CGRectMake(MaxX(settingTitleLabel) + 6, 7, 6, 10);
    [settingButton addSubview:settingArrowImageView];
    
    [self.pValueView configWithTitle:@"￥0" desStr:@"商品预估价值" image:[UIImage imageNamed:@"xp_usercenter_value"]];
    [self.totalNumView configWithTitle:@"0" desStr:@"总库存量" image:[UIImage imageNamed:@"xp_usercenter_number"]];
    
    UIButton *scoreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [scoreButton sd_setImageWithURL:[NSURL URLWithString:TO_STR(self.userModel.invitePic)] forState:UIControlStateNormal];
    scoreButton.frame = CGRectMake(12, bottom + 12, self.view.mj_w - 24, bannerButtonH);
    scoreButton.layer.cornerRadius = 8;
    scoreButton.adjustsImageWhenHighlighted = NO;
    [scoreButton addTarget:self action:@selector(scoreButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:scoreButton];
    
    self.mainView.contentSize = CGSizeMake(SCREEN_WIDTH, MaxY(scoreButton) + 12);
    
    [self blockCallBack];
}

- (void)blockCallBack{
    
    XPWeakSelf
    //我的新拍
    [self.itemView1 setItemSelectBlock:^(NSUInteger index) {
        
        if (![[XPVerifyTool shareInstance] checkLogin]) {
            [[XPVerifyTool shareInstance] userShouldLoginFirst];
            return;
        }

        NSDictionary *dic = weakSelf.myManagerArray[index];
        [XPRoutetool pushWithDic:dic];
    }];
    
    [self.makerItemView setItemSelectBlock:^(NSUInteger index) {
        NSDictionary *dic = weakSelf.myMakerArray[index];
        [XPRoutetool pushWithDic:dic];
    }];
    
    //我的造物
    [self.creationItemView setItemSelectBlock:^(NSUInteger index) {
        NSDictionary *dic = weakSelf.myCreationArray[index];
        [XPRoutetool pushWithDic:dic];
    }];
    
    //我的商城
    [self.shopItemView setItemSelectBlock:^(NSUInteger index) {
        NSDictionary *dic = weakSelf.myShopArray[index];
        [XPRoutetool pushWithDic:dic];
    }];
    //商城更多
    [self.shopItemView setMoreBlock:^{
        XPShopOrderStatusController *vc = [XPShopOrderStatusController new];
        vc.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    
    //我的服务
    [self.itemView2 setItemSelectBlock:^(NSUInteger index) {
        
        if (![[XPVerifyTool shareInstance] checkLogin]) {
            [[XPVerifyTool shareInstance] userShouldLoginFirst];
            return;
        }
        
        NSDictionary *dic = weakSelf.myServiceArray[index];
        NSString *mId = [dic objectForKey:@"mid"];
        if (mId) {
            if (mId.integerValue == 1) {
                bool installWechat=  [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession];
                
                if (!installWechat) {
                    [[XPShowTipsTool shareInstance] showMsg:@"功能暂不支持" ToView:weakSelf.view];
                    return;
                }
                
                [weakSelf getShareUrl];
            }else {
                [XPRoutetool pushWithDic:dic];
            }
        }
    }];
    
    [self.itemView2 setMoreBlock:^{
        XPMoreServiceController *vc = [XPMoreServiceController new];
        vc.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
}

#pragma mark --- button 点击事件
//个人中心点击
-(void)userSettinButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPUserInfoUpdateController *vc = [XPUserInfoUpdateController new];
    vc.userModel = self.userModel;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
// 设置点击
- (void)commonSetButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPUserSettingController *vc = [XPUserSettingController new];
    vc.userModel = self.userModel;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)noticeButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPUserNoticeViewController *vc = [XPUserNoticeViewController new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

//分享点击
- (void)bannerButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (APPTYPE == 1) {
        return;
    }
    XPShareViewController *vc = [XPShareViewController new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

//积分点击
- (void)scoreButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPUserScorePreUploadController *vc = [XPUserScorePreUploadController new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark -- 数据获取
- (void)initData {
    //我的新拍
    self.myManagerArray = @[
        @{
            @"name":@"账户管理",
            @"iconName":@"xp_usercenter_g1",
            @"mid":@"19"
        },@{
            @"name":@"提货管理",
            @"iconName":@"xp_usercenter_g2",
            @"mid":@"20"
        },@{
            @"name":@"我的仓库",
            @"iconName":@"xp_usercenter_g3",
            @"mid":@"21"
        },@{
            @"name":@"结算卡",
            @"iconName":@"xp_usercenter_g8",
            @"mid":@"5"
        }
    ];
    //我的创客
    self.myMakerArray = @[
        @{
            @"name":@"我的作品",
            @"iconName":@"xp_maker_work",
            @"mid":@"26"
        },@{
            @"name":@"我的徽章",
            @"iconName":@"xp_maker_badge",
            @"mid":@"25"
        },@{
            @"name":@"我的余额",
            @"iconName":@"xp_maker_money",
            @"mid":@"24"
        }
    ];
    //我的商城
    self.myShopArray = @[
        @{
            @"name":@"待支付",
            @"iconName":@"xp_usercenter_g25",
            @"mid":@"7"
        },@{
            @"name":@"待发货",
            @"iconName":@"xp_usercenter_g11",
            @"mid":@"8"
        },@{
            @"name":@"优惠券",
            @"iconName":@"xp_usercenter_g17",
            @"mid":@"14"
        },@{
            @"name":@"售后",
            @"iconName":@"xp_usercenter_g13",
            @"mid":@"10"
        }
    ];
    //我的造物
    self.myCreationArray = @[
//        @{
//            @"name":@"我的订单",
//            @"iconName":@"xp_usercenter_g14",
//            @"mid":@"11"
//        },
    @{
            @"name":@"打印记录",
            @"iconName":@"xp_usercenter_g15",
            @"mid":@"12"
        },@{
            @"name":@"我的设备",
            @"iconName":@"xp_usercenter_g16",
            @"mid":@"13"
        }
    ];
    //我的服务
    self.myServiceArray = @[
        @{
            @"name":@"我的积分",
            @"iconName":@"xp_usercenter_g26",
            @"mid":@"22"
        },
        @{
            @"name":@"我的收藏",
            @"iconName":@"xp_usercenter_g23",
            @"mid":@"3"
        },@{
            @"name":@"我的地址",
            @"iconName":@"xp_usercenter_g18",
            @"mid":@"15"
        },@{
            @"name":@"用户反馈",
            @"iconName":@"xp_usercenter_g19",
            @"mid":@"16"
        }
//        ,@{
//            @"name":@"我的关注",
//            @"iconName":@"xp_usercenter_g27",
//            @"mid":@"23"
//        }
        ,@{
            @"name":@"我的粉丝",
            @"iconName":@"xp_usercenter_g22",
            @"mid":@"2"
        },@{
            @"name":@"资讯动态",
            @"iconName":@"xp_usercenter_g24",
            @"mid":@"18"
        },@{
            @"name":@"联系客服",
            @"iconName":@"xp_usercenter_g20",
            @"mid":@"17"
        }
//        ,@{
//            @"name":@"分享APP",
//            @"iconName":@"xp_usercenter_g21",
//            @"mid":@"1"
//        }
    ];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (![[XPVerifyTool shareInstance]checkLogin] ) {
        [[XPVerifyTool shareInstance] userLoginFirstHandler];
    }else {
        [self getData];
        
    }
}

- (void)getData {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_User_get_info];
     NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             XPUserInfoModel *userModel = [XPUserInfoModel mj_objectWithKeyValues:data.userData];
             //数值没有变化就不用更新页面
             if ([XPCommonTool areDictionariesValuesEqual:userModel.mj_keyValues secondDict:self.userModel.mj_keyValues]) {
                 return;
             }
             self.userModel = userModel;
             
             XPLocalStorage *storage = [XPLocalStorage new];
             [storage writeUserCertified:TO_STR(self.userModel.nickname)];
             [storage writeUserName:self.userModel.name];
             [storage writeUserPhone:TO_STR(self.userModel.phone)];
             [storage writeUserIconUrl:self.userModel.head];
             [storage writeUserIdNumber:TO_STR(self.userModel.idCard)];
             [storage writeUserCertified:TO_STR(self.userModel.isCertified)];
             [storage writeUserInventCode:TO_STR(self.userModel.invCode)];
             [storage writeUserSubInventCode:TO_STR(self.userModel.supInvCode)];
             self.isShow = data.isShow;
             [self freshUI];
             [self updateUserInfo];
             
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
    
}

- (void)updateUserInfo {
    
    self.topBanner.image = [XPUserDataTool getUserCenterBackgroundImage:self.userModel.userType.integerValue];
    self.userLeveLabel.userModel = self.userModel;
    
    NSString *headIconUrl = self.userModel ? self.userModel.head: [XPLocalStorage readUserIconUrl];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:headIconUrl] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    NSString *nickName = self.userModel.nickname.length > 0 ? self.userModel.nickname : [XPLocalStorage readUserNickName];
    self.userNickNameLabel.text = nickName;
    
    NSString *valueStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.userModel.estimatedPrice]];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:valueStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(16)} range:NSMakeRange(0, 1)];
    
    self.pValueView.titleLabel.attributedText = attr;
//
    self.totalNumView.titleLabel.text = self.userModel.inventoryNumber?[NSString stringWithFormat:@"%@",self.userModel.inventoryNumber]:@"0";
}


#pragma mark -- 分享相关
//使用友盟sdk
- (void)getShareUrl {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_product_share];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            //分享
            NSDictionary *shareDic = data.userData;
            if (!shareDic || shareDic.allKeys.count == 0) {
                //后台数据不对,暂不拉起
            }else {
                [self shareWithdata:shareDic];
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
}
- (void)shareWithdata:(NSDictionary *)shareDic {
    
    XPShareUMView *shareView = [[XPShareUMView alloc] initWithFrame:k_keyWindow.bounds];
    [k_keyWindow addSubview:shareView];
    [shareView show];
    
    XPWeakSelf
    [shareView setActionBlock:^(NSInteger index) {
        if (index == 1) {//分享好友
            
            [weakSelf shareWithPlatform:UMSocialPlatformType_WechatSession shareWithdata:shareDic];
        }else if (index == 2){
            [weakSelf shareWithPlatform:UMSocialPlatformType_WechatTimeLine shareWithdata:shareDic];
        }
        
    }];
}

- (void)shareWithPlatform:(UMSocialPlatformType)platformType shareWithdata:(NSDictionary *)shareDic{
    
    [[XPUMShareTool shareInstance] shareWebUrlWithPlatform:platformType title:shareDic[@"title"]?: @"分享"
                                                     descr:shareDic[@"content"]?:@"新拍分享链接"
                                              webUrlString:shareDic[@"shareAddress"]?:@""
                                     currentViewController:self
                                                completion:^(id result, NSError *error) {
        
        if(error){
        NSLog(@"************Share fail with error %@*********",error);
    //        [[XPShowTipsTool shareInstance] showMsg:@"分享失败" ToView:self.view];
        }else{
        NSLog(@"response data is %@",result);
    //        [[XPShowTipsTool shareInstance] showMsg:@"分享成功" ToView:self.view];
        }
    }];
}



@end
