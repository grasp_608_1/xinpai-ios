//
//  XPTakeGoodsController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/20.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPTakeGoodsController : XPBaseViewController

@property (nonatomic, strong) NSNumber *goodsNum;

@property (nonatomic, strong) NSNumber *productSkuId;


@end

NS_ASSUME_NONNULL_END
