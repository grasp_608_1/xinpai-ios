//
//  XPMyStoreHouseViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/11.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPMyStoreHouseViewController : XPBaseViewController
//1 参拍 2 待提货 3 已提货
@property (nonatomic, assign)NSInteger entertype;

@end

NS_ASSUME_NONNULL_END
