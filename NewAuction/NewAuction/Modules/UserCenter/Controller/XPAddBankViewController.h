//
//  XPAddBankViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/17.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPAddBankViewController : XPBaseViewController

@property (nonatomic, assign) NSInteger enterType;

@property (nonatomic, strong) UIImage *faceImage;

@end


NS_ASSUME_NONNULL_END
