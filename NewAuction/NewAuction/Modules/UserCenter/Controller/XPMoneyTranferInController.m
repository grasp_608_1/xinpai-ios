//
//  XPMoneyTranferInController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/18.
//

#import "XPMoneyTranferInController.h"
#import "XPMyBankCardController.h"
#import "XPBankModel.h"
#import "XPIpTool.h"
#import "XPBaseWebViewViewController.h"
#import "XPTrendDetailViewController.h"
#import "XPPayTypeListView.h"
#import "XPPayToCompanyController.h"
#import "XPRechargeTipPopView.h"


@interface XPMoneyTranferInController ()<UITextFieldDelegate>
//账户资金
@property (nonatomic, strong) UILabel *accountDescLabel;
//充值金额
@property (nonatomic, strong) UILabel *rechargeDescLabel;

@property (nonatomic, strong) UILabel *totalLabel;

@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, strong) UILabel *sandPayLabel;

@property (nonatomic, strong) XPBankModel *bankModel;

@property (nonatomic, strong) UITextField *applyCodeTextField;

@property (nonatomic, strong) NSString *paymentId;

@property (nonatomic, copy) NSString *orderId;

@property (nonatomic, copy) NSString *sendyPayOrder;

@property (nonatomic, assign) BOOL isPayShow;

@property (nonatomic, strong) UIView *midView;
@property (nonatomic, strong) UIView *downView;

@property (nonatomic, assign) NSInteger payIndex;

@property (nonatomic, copy) NSDictionary *mainData;

@end

@implementation XPMoneyTranferInController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.payIndex = 0;
    [self setupViews];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(freshBankCard:) name:Notification_user_choose_bankcard object:nil];
}

#pragma mark -- Notification
- (void)applicationWillEnterForeground:(NSNotification *)noti {
    if ([self.navigationController.viewControllers lastObject] == self) {
        [self queryOrderStatus];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];

    if (self.isPayShow) {
        [self queryOrderStatus];
    }else {
        [self getData];
    }
    
}

- (void)queryOrderStatus {
    
    if (self.isPayShow) {
        
        self.isPayShow = NO;
        [[XPPaymentManager shareInstance] orderPayResultQueryWithType:SendayPayRechargeNormal orderId:self.sendyPayOrder controller:self requestResultBlock:^(XPRequestResult * _Nonnull data) {
            if (data.isSucess) {
                [[XPShowTipsTool shareInstance] showMsg:@"支付成功" ToView:k_keyWindow];
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                if (data.code.intValue == 10000) {//支付查询结束,未完成支付
                    
                }else {//失败
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
                }
            }
        }shouldQuery:YES];
    }
}

- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_reCharge_Info];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonGetWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.mainData = data.userData;
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict addEntriesFromDictionary:data.userData];
            self.mainData = dict.copy;
            [self refreshUI];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)refreshUI{
    //免手续费充值总额度
    NSNumber *thirdPartyRechargeFreeLimit = self.mainData[@"thirdPartyRechargeFreeLimit"];
    //剩余免手续费额度
    NSNumber *thirdPartyRechargeFreeLimitRemaining = self.mainData[@"thirdPartyRechargeFreeLimitRemaining"];
    
    NSString *freeStr = @"";
    NSString *remainStr = @"";
    if (thirdPartyRechargeFreeLimitRemaining.floatValue > 0) {
        freeStr = [NSString stringWithFormat:@"新人专享充值￥%@内免手续费",thirdPartyRechargeFreeLimit];
        remainStr = [NSString stringWithFormat:@"剩余免手续费额度:￥%@",[XPMoneyFormatTool formatMoneyWithNum:thirdPartyRechargeFreeLimitRemaining]];
    }
    
    NSString *totalStr = [NSString stringWithFormat:@"账户资金 %@",freeStr];
    NSString *totalRemainStr = [NSString stringWithFormat:@"充值金额 %@",remainStr];
    
    NSMutableAttributedString *attr1 = [[NSMutableAttributedString alloc] initWithString:totalStr];
    [attr1 addAttributes:@{NSFontAttributeName:kFontRegular(12)} range:NSMakeRange( totalStr.length - freeStr.length, freeStr.length)];
    self.accountDescLabel.attributedText = attr1;
 
    NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc] initWithString:totalRemainStr];
    [attr2 addAttributes:@{NSFontAttributeName:kFontRegular(12),NSForegroundColorAttributeName:app_font_color_8A} range:NSMakeRange( totalRemainStr.length - remainStr.length, remainStr.length)];
    self.rechargeDescLabel.attributedText = attr2;
    
    self.totalLabel.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.mainData[@"funds"]]];
    
    [self.midView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    UILabel *label3 = [UILabel new];
    label3.font = kFontRegular(14);
    label3.textColor = UIColorFromRGB(0x3A3C41);
    label3.frame = CGRectMake(24, 12, 100, 24);
    label3.text = @"转入方式";
    [self.midView addSubview:label3];
    
    UIButton *tableButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tableButton.frame = CGRectMake(self.midView.mj_w - 12 - 100 , 12, 100, 24);
    tableButton.titleLabel.font = kFontRegular(14);
    [tableButton setTitle:@"快捷支付限额表" forState:UIControlStateNormal];
    [tableButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    [tableButton addTarget:self action:@selector(tableButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [midView addSubview:tableButton];
    
    
    self.totalLabel.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.dataDict[@"funds"]]];
    
    NSNumber *percentSandy = self.mainData[@"sandFeesRatio"];
    NSString *percentSandyStr = [XPMoneyFormatTool  formatMoneyWithNum:[NSNumber numberWithFloat:percentSandy.floatValue * 100]];
    
    NSNumber *percentWechat = self.mainData[@"weiXinFeesRatio"];
    NSString *percentWechatStr = [XPMoneyFormatTool  formatMoneyWithNum:[NSNumber numberWithFloat:percentWechat.floatValue * 100]];
    
    NSNumber *percentAlipay = self.mainData[@"aliPayFeesRatio"];
    NSString *percentAlipayStr = [XPMoneyFormatTool  formatMoneyWithNum:[NSNumber numberWithFloat:percentAlipay.floatValue * 100]];
    
    NSArray *payArray;
    if (APPTYPE == 0){
        payArray = @[
            @{
                @"title":@"对公支付",
                @"desc" : @"对公支付不收手续费",
                @"pic":@"xp_payment_sandy"
            },@{
                @"title":@"杉德支付",
                @"desc" : [NSString stringWithFormat:@"使用杉德支付将收取您%@%%手续费",percentSandyStr],
                @"pic":@"xp_payment_tocompany"
            },@{
                @"title":@"微信支付",
                @"desc" : [NSString stringWithFormat:@"使用微信支付将收取您%@%%手续费",percentWechatStr],
                @"pic":@"xp_payment_wechat"
            },@{
                @"title":@"支付宝支付",
                @"desc" : [NSString stringWithFormat:@"使用支付宝支付将收取您%@%%手续费",percentAlipayStr],
                @"pic":@"xp_payment_alipay"
            }
            
        ];
    }else if (APPTYPE == 1){
        payArray = @[
            @{
                @"title":@"对公支付",
                @"desc" : @"对公支付不收手续费",
                @"pic":@"xp_payment_sandy"
            },@{
                @"title":@"微信支付",
                @"desc" : [NSString stringWithFormat:@"使用微信支付将收取您%@%%手续费",percentWechatStr],
                @"pic":@"xp_payment_wechat"
            }
        ];
    }
    
    self.midView.frame = CGRectMake(12, MaxY(self.downView) + 12, SCREEN_WIDTH - 24, 56 + 64 *payArray.count);
    for (int i = 0; i<payArray.count; i++) {
        NSDictionary *dict = payArray[i];
        bool isShowTipView;
        
        
        XPPayTypeListView *payView = [[XPPayTypeListView alloc]initWithFrame:CGRectMake(0, 42 + 64 *i, self.midView.mj_w, 64)];
        payView.selectedButton.tag = i;
        [self.midView addSubview:payView];
        
        payView.sepView.hidden = NO;
        
        if (i == 0) {
            isShowTipView = NO;
        }else {
            isShowTipView = YES;
        }
        [payView viewWithDict:dict showTips:isShowTipView showDesc:YES];
        
        XPWeakSelf
       [payView setTipBlock:^{
           [self showTipView];
        }];
        
        [payView setSelectedBlock:^(NSInteger tag) {
            [weakSelf selectedPayTypeTag:tag];
        }];
    }
    //默认选中
    [self selectedPayTypeTag:0];
    
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.navBarView];
    self.navBarView.titleLabel.text = @"支付保证金";
    
    [self.navBarView.rightButton setTitle:@"明细" forState:UIControlStateNormal];
    [self.navBarView.rightButton setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
    [self.navBarView.rightButton addTarget:self action:@selector(detailButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.rightButton.titleLabel.font = kFontRegular(16);
    self.navBarView.rightButton.frame = CGRectMake(SCREEN_WIDTH - 10 - 120, self.navBarView.mj_h - 44, 120, 44);
}

- (void)detailButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPTrendDetailViewController *vc = [XPTrendDetailViewController new];
    vc.enterType = 9;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setupViews {
    
    UIView *topView = [UIView new];
    topView.backgroundColor = UIColorFromRGB(0x6E6BFF);
    topView.layer.cornerRadius = 8;
    topView.layer.masksToBounds = YES;
    topView.frame = CGRectMake(12, 12 + kTopHeight, SCREEN_WIDTH - 24, 96);
    [self.view addSubview:topView];
    
    UILabel *label1 = [UILabel new];
    label1.font = kFontRegular(14);
    label1.textColor = UIColor.whiteColor;
    label1.frame = CGRectMake(24, 12, topView.mj_w - 24, 24);
    label1.text = @"账户资金";
    self.accountDescLabel = label1;
    [topView addSubview:label1];
    
    UILabel *label2 = [UILabel new];
    label2.font = kFontMedium(28);
    label2.textColor = UIColor.whiteColor;
    label2.frame = CGRectMake(24, MaxY(label1) +12, topView.mj_w - 24, 36);
    label2.text = @"0.00";
    self.totalLabel  = label2;
    [topView addSubview:label2];
    
    UIView *downView = [UIView new];
    downView.backgroundColor = UIColor.whiteColor;
    downView.frame = CGRectMake(12, MaxY(topView) + 12, SCREEN_WIDTH - 24, 132);
    downView.layer.cornerRadius = 16;
    downView.layer.masksToBounds = YES;
    self.downView = downView;
    [self.view addSubview:downView];
    
    UILabel *label5 = [UILabel new];
    label5.font = kFontRegular(14);
    label5.textColor = UIColorFromRGB(0x3A3C41);
    label5.frame = CGRectMake(24, 12, topView.mj_w - 24, 24);
    label5.text = @"转入金额";
    self.rechargeDescLabel = label5;
    [downView addSubview:label5];
    
    UILabel *label6 = [UILabel new];
    label6.font = kFontMedium(24);
    label6.textColor = UIColorFromRGB(0x000000);
    label6.frame = CGRectMake(24, 64, 24, 24);
    label6.text = @"￥";
    [downView addSubview:label6];
    
    UITextField *textField = [[UITextField alloc] init];
    textField.font = kFontMedium(32);
    textField.textColor = UIColor.blackColor;
    textField.keyboardType = UIKeyboardTypeDecimalPad;
    textField.delegate = self;
    self.textField = textField;
    textField.frame = CGRectMake(MaxX(label6), 56, downView.mj_w - 66 - 24, 40);
    [downView addSubview:textField];
    
    UIView *sepView = [UIView new];
    sepView.frame = CGRectMake(24, MaxY(textField) + 12, downView.mj_w - 48, 1);
    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [downView addSubview:sepView];
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(12,SCREEN_HEIGHT - self.safeBottom - 30-50, SCREEN_WIDTH - 2 *12, 50);
    [saveButton setTitle:@"确认转入" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    saveButton.layer.cornerRadius = 25;
    saveButton.clipsToBounds = YES;
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveButton];
    
    UIButton *midView = [UIButton new];
    midView.backgroundColor = UIColor.whiteColor;
    midView.layer.cornerRadius = 8;
    midView.layer.masksToBounds = YES;
    midView.frame = CGRectMake(12, MaxY(downView) + 12, SCREEN_WIDTH - 24, 0);
//    [midView addTarget:self action:@selector(bankButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.midView = midView;
    [self.view addSubview:midView];
    
    
}


- (void)selectedPayTypeTag:(NSInteger)tag{
    self.payIndex = tag;
    for (UIView *view in self.midView.subviews) {
        if ([view isKindOfClass:[XPPayTypeListView class]]) {
            XPPayTypeListView *tempView = (XPPayTypeListView *)view;
            if (tempView.selectedButton.tag == tag) {
                tempView.selectedButton.selected = YES;
            }else {
                tempView.selectedButton.selected = NO;
            }
        }
    }
}

- (void)tableButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
    vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,kBank_description];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showTipView {
    NSNumber *percentSandy = self.mainData[@"sandFeesRatio"];
    NSString *percentSandyStr = [XPMoneyFormatTool  formatMoneyWithNum:[NSNumber numberWithFloat:percentSandy.floatValue * 100]];
    
    NSNumber *percentWechat = self.mainData[@"weiXinFeesRatio"];
    NSString *percentWechatStr = [XPMoneyFormatTool  formatMoneyWithNum:[NSNumber numberWithFloat:percentWechat.floatValue * 100]];
    
    NSNumber *percentAliPay= self.mainData[@"aliPayFeesRatio"];
    NSString *percentAlipayStr = [XPMoneyFormatTool  formatMoneyWithNum:[NSNumber numberWithFloat:percentAliPay.floatValue * 100]];
    
    XPRechargeTipPopView *popView = [[XPRechargeTipPopView alloc] initWithFrame:k_keyWindow.bounds];
    [popView viewWithDictionary:@{
        @"percentSandyStr":percentSandyStr,
        @"percentWechatStr":percentWechatStr,
        @"percentAlipayStr":percentAlipayStr
    }];
    [k_keyWindow addSubview:popView];
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    [self.view endEditing:YES];
    
    if (![[XPVerifyTool shareInstance] checkUserAuthentication]) {
        [[XPVerifyTool shareInstance] userShouldAuthentication];
        return;
    }
    
    if (self.payIndex == 1 ||self.payIndex == 2 ||self.payIndex == 3) {
        if(self.textField.text.floatValue == 0 || self.textField.text.length == 0){
            [[XPShowTipsTool shareInstance] showMsg:@"请输入支付金额!" ToView:self.view];
        }else {
            
         XPWeakSelf
            //剩余免手续费充值额度
            NSNumber *thirdPartyRechargeFreeLimitRemaining = self.mainData[@"thirdPartyRechargeFreeLimitRemaining"];
            //剩余可充值额度
            NSNumber *cumulativeRechargeRemaining = self.mainData[@"cumulativeRechargeRemaining"];
           
            NSNumber *feesRatio = @(0);
            if (self.payIndex == 1) { //杉德手续费
                feesRatio = self.mainData[@"sandFeesRatio"];
            }else if (self.payIndex == 2){ //微信手续费
                feesRatio = self.mainData[@"weiXinFeesRatio"];
            }else if (self.payIndex == 3){//支付宝手续费
                feesRatio = self.mainData[@"aliPayFeesRatio"];
            }
            //转换一下精度,防止精度缺失
            feesRatio = [NSNumber numberWithFloat:[XPMoneyFormatTool formatMoneyWithNum:feesRatio maximumFractionDigits:4].floatValue];
            
            NSString *percentfeePayStr = [XPMoneyFormatTool  formatMoneyWithNum:[NSNumber numberWithFloat:feesRatio.floatValue * 100]];
            //输入的金额
            CGFloat rechargeAmout = self.textField.text.floatValue;

            if (thirdPartyRechargeFreeLimitRemaining.floatValue > rechargeAmout) {
                //还在免费充值额度内,不需要弹窗
                [self sandayPay];
            }else if (cumulativeRechargeRemaining.floatValue < rechargeAmout){
                //当前充值金额大于可充值总额度
                NSString *contentStr = [NSString stringWithFormat:@"您输入的金额已超过充值上限，当前充值额度还剩￥%@，请重新输入",[XPMoneyFormatTool formatMoneyWithNum:cumulativeRechargeRemaining]];
                [self.xpAlertView showAlertWithTitle:contentStr actionBlock:^(NSString * _Nullable extra) {
                    
                }];
            }else if(thirdPartyRechargeFreeLimitRemaining.floatValue <= 0){
                //免手续费额度用完了,充值所有金额都需要收取手续费
                NSString *contentStr;
                //收取收取费
                NSString *fee;
                //实际到账金额
                NSString *actuallyAmout;
                if (self.payIndex == 1) { //杉德支付
                    NSNumber *feeNum = [NSNumber numberWithFloat:(feesRatio.floatValue * rechargeAmout)];
                    fee = [XPMoneyFormatTool formatMoneyWithNum:feeNum];
                    actuallyAmout = [XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:rechargeAmout - feeNum.floatValue]];
                    contentStr = [NSString stringWithFormat:@"您的免手续费充值额度已用尽，本次充值￥%@，将收取%@%%手续费￥%@，直接从充值金额中扣减，实际到账￥%@，是否继续充值?",self.textField.text,percentfeePayStr,fee,actuallyAmout];
                }else{
                    
                    NSNumber *feeNum = [NSNumber numberWithFloat:(feesRatio.floatValue * rechargeAmout)];
                    fee = [XPMoneyFormatTool formatMoneyWithNum:feeNum];
                    actuallyAmout = [XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:rechargeAmout - feeNum.floatValue]];
                    contentStr = [NSString stringWithFormat:@"您的免手续费充值额度已用尽，本次充值￥%@，将收取%@%%手续费￥%@，直接从充值金额中扣减，实际到账￥%@，是否继续充值?",self.textField.text,percentfeePayStr,fee,actuallyAmout];
                }
                
                [self.xpAlertView showCancelAlertWithTitle:contentStr leftAction:^(NSString * _Nullable extra) {
                    
                } rightAction:^(NSString * _Nullable extra) {
                    [weakSelf sandayPay];
                }];
                
            }else {
                //额度未用完,部分需要收取手续费
               //充值超额  您还有M元免费充值额度， 本次充值超额xx元，将收取N%手续费yy元，直接从充值金额中扣减，实际到账zz元，是否继续充值？

                NSString *contentStr;
                //收取收取费
                NSString *fee;
                //实际到账金额
                NSString *actuallyAmout;
                if (self.payIndex == 1) { //杉德支付
                    NSNumber *feeNum = [NSNumber numberWithFloat:(feesRatio.floatValue * (rechargeAmout - thirdPartyRechargeFreeLimitRemaining.floatValue))];
                    fee = [XPMoneyFormatTool formatMoneyWithNum:feeNum];
                    actuallyAmout = [XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:rechargeAmout - feeNum.floatValue]];
                    contentStr = [NSString stringWithFormat:@"您还有￥%@免手续费充值额度， 本次充值超额￥%@，将收取%@%%手续费￥%@，直接从充值金额中扣减，实际到账￥%@，是否继续充值？",[XPMoneyFormatTool formatMoneyWithNum:thirdPartyRechargeFreeLimitRemaining],
                                  [XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:rechargeAmout - thirdPartyRechargeFreeLimitRemaining.floatValue]],
                                  percentfeePayStr,
                                fee,
                                  actuallyAmout];
                }else{
                    
                    NSNumber *feeNum = [NSNumber numberWithFloat:(feesRatio.floatValue * (rechargeAmout - thirdPartyRechargeFreeLimitRemaining.floatValue))];
                    fee = [XPMoneyFormatTool formatMoneyWithNum:feeNum];
                    actuallyAmout = [XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:rechargeAmout - feeNum.floatValue]];
                    contentStr = [NSString stringWithFormat:@"您还有￥%@免手续费充值额度， 本次充值超额￥%@，将收取%@%%手续费￥%@，直接从充值金额中扣减，实际到账￥%@，是否继续充值？",[XPMoneyFormatTool formatMoneyWithNum:cumulativeRechargeRemaining],
                                  [XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:rechargeAmout - thirdPartyRechargeFreeLimitRemaining.floatValue]],
                                  percentfeePayStr,
                                fee,
                                  actuallyAmout];
                }
                
                [self.xpAlertView showCancelAlertWithTitle:contentStr leftAction:^(NSString * _Nullable extra) {
                    
                } rightAction:^(NSString * _Nullable extra) {
                    [weakSelf sandayPay];
                }];
            }
        }
    }else if(self.payIndex == 0){
        
        XPPayToCompanyController *vc = [XPPayToCompanyController new];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}



#pragma mark -- 杉德支付
- (void)sandayPay{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_sandPay_recharge];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"rechargeAmount"] = self.textField.text;
    if (APPTYPE == 0) {
        if (self.payIndex == 1) {
            paraM[@"payType"] = @(11);
        }else if (self.payIndex == 2){
            paraM[@"payType"] = @(2);
        }else if (self.payIndex == 3){
            paraM[@"payType"] = @(1);
        }
    }else{
        if (self.payIndex == 1) {
            paraM[@"payType"] = @(2);
        }
    }
    
    [self startLoadingGif];
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            if(self.payIndex == 3){
                [[XPPaymentManager shareInstance] showAlipayWithDic:data.userData viewController:self];
                self.isPayShow = YES;
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if (self.payIndex == 2) {//微信支付
                self.isPayShow = YES;
                [[XPPaymentManager shareInstance] showWeChatPayWithDic:data.userData viewController:self];
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if (self.payIndex == 1){
                self.isPayShow = YES;
                [[XPPaymentManager shareInstance] showWeChatPayWithDic:data.userData viewController:self];
                self.sendyPayOrder = data.userData[@"orderNo"];
            }
        }else{
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

#pragma mark --- textfield Delegate
//限制只能输入金额
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //限制.后面最多有两位，且不能再输入.
    if ([textField.text rangeOfString:@"."].location != NSNotFound) {
        //有.了 且.后面输入了两位  停止输入
        if (toBeString.length > [toBeString rangeOfString:@"."].location+3) {
            return NO;
        }
        //有.了，不允许再输入.
        if ([string isEqualToString:@"."]) {
            return NO;
        }
    }
    
    //限制首位0，后面只能输入. 或 删除
    if ([textField.text isEqualToString:@"0"]) {
        if (!([string isEqualToString:@"."] || [string isEqualToString:@""])) {
            return NO;
        }
    }
    
    //首位. 前面补全0
    if ([toBeString isEqualToString:@"."]) {
        textField.text = @"0";
        return YES;
    }
    
    //限制只能输入：1234567890.
    NSCharacterSet * characterSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890."] invertedSet];
    NSString * filtered = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}

@end
