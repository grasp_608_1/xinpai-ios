//
//  XPTakeGoodsController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/20.
//

#import "XPTakeGoodsController.h"
#import "XPHitButton.h"
#import "XPTakeGoodsSuccessController.h"
#import "XPPickGoodsModel.h"
#import "XPUserAddressController.h"
#import "XPPaymentView.h"
#import "XPNoMenuTextField.h"

@interface XPTakeGoodsController ()<UITextFieldDelegate>

@property (nonatomic, strong) UIScrollView *mainView;

@property (nonatomic, strong) UIButton *locationView;
@property (nonatomic, strong) UIView *midView;
@property (nonatomic, strong) UIView *downView;
@property (nonatomic, strong) UILabel *userInfoLabel;
@property (nonatomic, strong) UILabel *phoneSerectLabel;
@property (nonatomic, strong) UILabel *adressLabel;
@property (nonatomic, strong) UIImageView *rightArrow;

@property (nonatomic, strong) UIImageView *adressImageView;

@property (nonatomic, strong) UIImageView *pImageView;
@property (nonatomic, strong) UILabel *pTitleLabel;
@property (nonatomic, strong) UILabel *pPriceLabel;
@property (nonatomic, strong) UILabel *productNumLabel;
@property (nonatomic, strong) UILabel *payWayLabel;
//数量
@property (nonatomic, strong) UILabel *numDesLable;
@property (nonatomic, strong) UIButton *mButton;
//提货输入数量
@property (nonatomic, strong) XPNoMenuTextField *numberTextField;


@property (nonatomic, strong) UILabel *totalPriceLabel;

@property (nonatomic, strong) UILabel *extraMoney;
@property (nonatomic, strong) UILabel *limitMoney;
@property (nonatomic, strong) UILabel *limitPoolMoney;
@property (nonatomic, strong) UILabel *profitMoney;

@property (nonatomic, strong) UIButton *saveButton;
//支付
@property (nonatomic, strong)XPPaymentView *payView;

//单个价格
@property (nonatomic, assign) CGFloat price;
//最大数量
@property (nonatomic, assign) NSInteger max;
//当前数量
@property (nonatomic, assign) NSInteger count;

@property (nonatomic, strong) XPPickGoodsModel *model;
//汇付支付支付id
@property (nonatomic, strong) NSString *paymentId;
//汇付支付订单号
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *extraMoneyValue;
//验证码
@property (nonatomic, copy) NSString *codeStr;

@property (nonatomic, copy) NSString *sendyPayOrder;

@property (nonatomic, assign) BOOL isPayShow;

@end

@implementation XPTakeGoodsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupViews];
    [self getData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearDefaultAddress:) name:Notification_user_choose_adress object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    if (self.isPayShow) {
        [self queryOrderStatus];
    }
}

-(void)queryOrderStatus{
    if (self.isPayShow) {
        
        [[XPPaymentManager shareInstance] orderPayResultQueryWithType:SendayPayRechargePick orderId:self.sendyPayOrder controller:self requestResultBlock:^(XPRequestResult * _Nonnull data) {
            if (data.isSucess) {
                self.isPayShow = NO;
                [[NSNotificationCenter defaultCenter] postNotificationName:Notification_shopList_fresh object:nil];
                [self jumpToSuccessControllerWithNum:data.userData[@"id"]];
            }else {
                if (data.code.intValue == 10000) {//支付查询结束,未完成支付
                    
                }else {
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
                }
            }
        }shouldQuery:YES];
    }
}
- (void)applicationWillEnterForeground:(NSNotification *)noti {
    if ([self.navigationController.viewControllers lastObject] == self) {
        [self queryOrderStatus];
    }
}

- (void)setupViews {
    
    self.mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.view.mj_w, SCREEN_HEIGHT - kTopHeight)];
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self.view addSubview:self.mainView];
    
    UIButton *locationView = [UIButton new];
    locationView.backgroundColor = UIColor.whiteColor;
    locationView.layer.cornerRadius = 8;
    self.locationView = locationView;
    
    [locationView addTarget:self action:@selector(chooseAdress:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:locationView];
    
    UIImageView *locationLogo = [UIImageView new];
    locationLogo.image = [UIImage imageNamed:@"xp_location"];
    self.adressImageView = locationLogo;
    [locationView addSubview:locationLogo];
    
    UILabel *userInfoLabel = [UILabel new];
    userInfoLabel.textColor = UIColorFromRGB(0x3A3C41);
    userInfoLabel.font = kFontMedium(16);
    self.userInfoLabel = userInfoLabel;
    [locationView addSubview:userInfoLabel];
    
    self.phoneSerectLabel = [UILabel new];
    self.phoneSerectLabel.font = kFontRegular(12);
    self.phoneSerectLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.phoneSerectLabel.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
    self.phoneSerectLabel.layer.borderWidth = 1;
    self.phoneSerectLabel.textAlignment = NSTextAlignmentCenter;
    [locationView addSubview:self.phoneSerectLabel];
    
    
    UILabel *adressLabel = [UILabel new];
    adressLabel.textColor = UIColorFromRGB(0x8A8A8A);
    adressLabel.numberOfLines = NO;
    adressLabel.font = kFontRegular(14);
    self.adressLabel = adressLabel;
    [locationView addSubview:adressLabel];
    
    UIImageView *rightArrow = [UIImageView new];
    rightArrow.image = [UIImage imageNamed:@"xp_setting_arrow"];
    self.rightArrow = rightArrow;
    [locationView addSubview:rightArrow];
    
    UIView *midView = [UIView new];
    midView.backgroundColor = UIColor.whiteColor;
    midView.layer.cornerRadius = 8;
    midView.frame = CGRectMake(12, 0, self.view.mj_w - 12 *2, 0);
    midView.clipsToBounds = YES;
    self.midView = midView;
    [_mainView addSubview:midView];
    
    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(12, 12, 120, 120);
    [midView addSubview:_pImageView];
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontMedium(14);
    self.pTitleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.pTitleLabel.frame = CGRectMake(144, 12,midView.mj_w - 144 - 12, 24);
    [midView addSubview:_pTitleLabel];
    
    self.pPriceLabel = [UILabel new];
    self.pPriceLabel.font = kFontRegular(14);
    self.pPriceLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.pPriceLabel.frame = CGRectMake(self.pTitleLabel.mj_x, MaxY(self.pTitleLabel) + 6, self.pTitleLabel.mj_w, 24);
    [midView addSubview:self.pPriceLabel];
    
    self.productNumLabel = [UILabel new];
    self.productNumLabel.font = kFontRegular(14);
    self.productNumLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.productNumLabel.frame = CGRectMake(self.pPriceLabel.mj_x, MaxY(self.pPriceLabel) + 6, self.pPriceLabel.mj_w, 24);
    [midView addSubview:self.productNumLabel];
    
    UIView *sepView = [UIView new];
    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    sepView.frame = CGRectMake(12, MaxY(_pImageView) + 12, midView.mj_w - 24, 1);
    [midView addSubview:sepView];
    
    NSArray *titleArray = @[@"商品数量",@"商品总价",@"运费/包装费"];
    for (int i = 0; i<titleArray.count ; i++) {
        UILabel *tempLabel = [UILabel new];
        tempLabel.text = titleArray[i];
        tempLabel.textColor = UIColorFromRGB(0x8A8A8A);
        tempLabel.font = kFontRegular(14);
        tempLabel.frame = CGRectMake(12, MaxY(sepView) + 12 + i *(12 + 24), midView.mj_w/2. - 12, 24);
        [midView addSubview:tempLabel];
    }

    
    XPHitButton *button1 = [[XPHitButton alloc] init];
//    button1.hitRect = 30;
    button1.backgroundColor = [UIColor whiteColor];
    [button1 addTarget:self action:@selector(mButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button1 setImage:[UIImage imageNamed:@"Group_99"] forState:UIControlStateNormal];
    button1.frame = CGRectMake(midView.mj_w - 152, MaxY(sepView) + 14, 21, 21);
    self.mButton = button1;
    [midView addSubview:button1];
    
    UILabel *numDesLable = [UILabel new];
    numDesLable.text = @"数量";
    numDesLable.textColor = UIColorFromRGB(0x8A8A8A);
    numDesLable.font = kFontRegular(14);
    numDesLable.frame = CGRectMake(button1.mj_x - 30 - 12, button1.mj_y, 30, 24);
    numDesLable.textAlignment = NSTextAlignmentRight;
    self.numDesLable = numDesLable;
    [midView addSubview:numDesLable];
    
    self.numberTextField = [XPNoMenuTextField new];
    self.numberTextField.textColor = UIColorFromRGB(0x3A3C41);
    self.numberTextField.font = kFontMedium(16);
    self.numberTextField.frame = CGRectMake(MaxX(button1)+ 2, 12 + MaxY(sepView), 46, 24);
    self.numberTextField.text = @"0";
    self.numberTextField.textAlignment = NSTextAlignmentCenter;
    self.numberTextField.layer.cornerRadius = 4;
    self.numberTextField.backgroundColor = UIColorFromRGB(0xF2F2F2);
//    self.numberTextField.layer.borderColor = UIColorFromRGB(0xF2F2F2).CGColor;
//    self.numberTextField.layer.borderWidth = 1;
    self.numberTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.numberTextField.delegate = self;
    [midView addSubview:self.numberTextField];
    
    XPHitButton *button2 = [[XPHitButton alloc] init];
//    button2.hitRect = 30;
    button2.backgroundColor = [UIColor whiteColor];
    [button2 addTarget:self action:@selector(aButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button2 setImage:[UIImage imageNamed:@"Group_100"] forState:UIControlStateNormal];
    button2.frame = CGRectMake(MaxX(self.numberTextField) + 2, button1.mj_y, 21, 21);
    [midView addSubview:button2];
    
    UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button3 addTarget:self action:@selector(allButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button3 setTitle:@"全部" forState:UIControlStateNormal];
    [button3 setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    button3.titleLabel.font = kFontRegular(14);
    button3.frame = CGRectMake(MaxX(button2) + 17, button1.mj_y, 30, 24);
    [midView addSubview:button3];
  
    UILabel *totalPriceLabel = [UILabel new];
    totalPriceLabel.textColor = UIColorFromRGB(0xFF4A4A);
    totalPriceLabel.font = kFontRegular(16);
    totalPriceLabel.textAlignment = NSTextAlignmentRight;
    totalPriceLabel.frame = CGRectMake(midView.mj_w/2., MaxY(button1) + 12, midView.mj_w/2. -12, 24);
    totalPriceLabel.text = @"0";
    self.totalPriceLabel = totalPriceLabel;
    [midView addSubview:totalPriceLabel];
    
    UILabel *payWayLabel = [UILabel new];
    payWayLabel.font = kFontRegular(14);
    payWayLabel.textColor = UIColorFromRGB(0x3A3C41);
    payWayLabel.frame = CGRectMake(midView.mj_w/2., MaxY(totalPriceLabel) + 12, midView.mj_w/2. -12, 24);
    payWayLabel.textAlignment = NSTextAlignmentRight;
    self.payWayLabel = payWayLabel;
    [midView addSubview:payWayLabel];
    //设置高度
    
    
    UIView *downView = [UIView new];
    downView.backgroundColor = UIColor.whiteColor;
    downView.layer.cornerRadius = 8;
    downView.clipsToBounds = YES;
    self.downView = downView;
    [self.mainView addSubview:downView];
    
    UILabel *label6 = [UILabel new];
    label6.textColor = UIColorFromRGB(0x8A8A8A);
    label6.font = kFontRegular(14);
    label6.frame = CGRectMake(12, 0, midView.mj_w/2. -12, 42);
    label6.text = @"限定该拍品货款";
    [downView addSubview:label6];
    
    UILabel *label7 = [UILabel new];
    label7.textColor = UIColorFromRGB(0x8A8A8A);
    label7.font = kFontRegular(14);
    label7.frame = CGRectMake(12, MaxY(label6), midView.mj_w/2. -12, 42);
    label7.text = @"不限定拍品货款";
    [downView addSubview:label7];
    
    UILabel *label11 = [UILabel new];
    label11.textColor = UIColorFromRGB(0x8A8A8A);
    label11.font = kFontRegular(14);
    label11.frame = CGRectMake(12, MaxY(label7), midView.mj_w/2. -12, 42);
    label11.text = @"补贴货款";
    [downView addSubview:label11];
    
    UILabel *label5 = [UILabel new];
    label5.textColor = UIColorFromRGB(0x8A8A8A);
    label5.font = kFontRegular(14);
    label5.frame = CGRectMake(12, MaxY(label11), midView.mj_w/2. -12, 42);
    label5.text = @"需补差价";
    [downView addSubview:label5];
    
    UILabel *label8 = [UILabel new];
    label8.textColor = UIColorFromRGB(0x3A3C41);
    label8.font = kFontRegular(14);
    label8.textAlignment = NSTextAlignmentRight;
    label8.frame = CGRectMake(midView.mj_w/2, 0, midView.mj_w/2. -12, 42);
    label8.textAlignment = NSTextAlignmentRight;
    self.limitMoney = label8;
    [downView addSubview:label8];
    
    UILabel *label9 = [UILabel new];
    label9.textColor = UIColorFromRGB(0x3A3C41);
    label9.font = kFontRegular(14);
    label9.textAlignment = NSTextAlignmentRight;
    label9.frame = CGRectMake(midView.mj_w/2, 42, midView.mj_w/2. -12, 42);
    label9.textAlignment = NSTextAlignmentRight;
    self.limitPoolMoney = label9;
    [downView addSubview:label9];
    
    UILabel *label12 = [UILabel new];
    label12.textColor = UIColorFromRGB(0x3A3C41);
    label12.font = kFontRegular(14);
    label12.textAlignment = NSTextAlignmentRight;
    label12.frame = CGRectMake(midView.mj_w/2, 84, midView.mj_w/2. -12, 42);
    label12.textAlignment = NSTextAlignmentRight;
    self.profitMoney = label12;
    [downView addSubview:label12];
    
    UILabel *label10 = [UILabel new];
    label10.textColor = UIColorFromRGB(0x3A3C41);
    label10.font = kFontRegular(14);
    label10.textAlignment = NSTextAlignmentRight;
    label10.frame = CGRectMake(midView.mj_w/2, 126, midView.mj_w/2. -12, 42);
    label10.textAlignment = NSTextAlignmentRight;
    self.extraMoney = label10;
    self.extraMoney.text = @"￥0";
    [downView addSubview:label10];
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveButton setTitle:@"确认提货" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    saveButton.layer.cornerRadius = 25;
    saveButton.clipsToBounds = YES;
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.saveButton = saveButton;
    [self.mainView addSubview:saveButton];
}

- (void)freshUIWithModel:(XPPickGoodsModel *)model {
    self.model = model;
    
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    self.pTitleLabel.text = model.itemName;
    self.productNumLabel.text = [NSString stringWithFormat:@"货品库存 %@",model.stockAmount];
    
    NSString *formatPiecePriceStr = [XPMoneyFormatTool formatMoneyWithNum:model.pricePick];
    NSString *piecePriceStr = [NSString stringWithFormat:@"提货单价 ￥%@",formatPiecePriceStr];
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:piecePriceStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(14),NSForegroundColorAttributeName:UIColorFromRGB(0x3A3C41)} range:NSMakeRange(piecePriceStr.length - formatPiecePriceStr.length -1, formatPiecePriceStr.length + 1)];
    self.pPriceLabel.attributedText = attr;
    
    self.max =  model.stockAmount.integerValue;
    self.price = model.pricePick.floatValue * 1000;
    
    if (self.max >0) {
        self.count = 1;
    }else {
        self.count = 0;
    }
    
    self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.count];
    [self layoutCountView];
    
    self.locationView.frame = CGRectMake(12, 12, self.mainView.mj_w - 24, 60);
    
    CGFloat totalPrice = model.pricePick.floatValue *self.count;
    NSString *totalPriceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:totalPrice]]];

    self.totalPriceLabel.text = totalPriceStr;
    
    if (model.isPostage.intValue == 1) {//包邮
        self.payWayLabel.text = @"包邮";
    }else {
        self.payWayLabel.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.postageBase]];
    }
    
    [self showTotalPrice];
    
    CGFloat addressW  = self.locationView.mj_w - 48 - 36;
    if (!self.model.addressId || self.model.addressId.intValue == 0) {
        NSString * adressStr = @"请先添加收货地址";
        self.adressLabel.text = adressStr;
        self.userInfoLabel.hidden = YES;
        self.phoneSerectLabel.hidden = YES;
        
        self.locationView.frame = CGRectMake(12, 12, self.mainView.mj_w - 24,48);
        self.adressLabel.frame = CGRectMake(48, 12, addressW, 24);
        self.adressImageView.frame = CGRectMake(12,12, 24, 24);
                
    }else {
        
        self.userInfoLabel.hidden = NO;
        self.phoneSerectLabel.hidden = NO;
        self.adressImageView.frame = CGRectMake(12,12, 24, 24);
        NSString *userPhone = self.model.phone.length >= 11 ? [self.model.phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"] : self.model.phone;
        self.userInfoLabel.text = [NSString stringWithFormat:@"%@ %@",self.model.name,userPhone];
        CGFloat userInfoW = [self.userInfoLabel.text sizeWithAttributes:@{NSFontAttributeName:self.userInfoLabel.font}].width;
        self.userInfoLabel.frame = CGRectMake(48, 12, userInfoW + 12, 24);
        self.phoneSerectLabel.text = @"号码保护中";
        self.phoneSerectLabel.frame = CGRectMake(MaxX(self.userInfoLabel), 15, 72, 18);
         
        self.adressLabel.text = [NSString stringWithFormat:@"%@%@",[model.area stringByReplacingOccurrencesOfString:@"," withString:@""],model.address];
        CGFloat addressH = [self.adressLabel.text boundingRectWithSize:CGSizeMake(addressW, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.adressLabel.font} context:nil].size.height;
        
        self.adressLabel.frame = CGRectMake(self.userInfoLabel.mj_x, MaxY(self.userInfoLabel) + 6, addressW, addressH);
        
    }
    
    self.locationView.frame = CGRectMake(12, 12, self.mainView.mj_w - 24,MaxY(self.self.adressLabel) + 12);
    self.rightArrow.frame = CGRectMake(self.locationView.mj_w - 30, self.locationView.mj_h/2 - 12, 24, 24);
    
    self.limitMoney.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.profitProduct]];
    self.limitPoolMoney.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.profitPool]];
    self.profitMoney.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.earningsProduct]];
    
    self.midView.frame = CGRectMake(12, MaxY(self.locationView) + 12, self.mainView.mj_w - 24, MaxY(self.payWayLabel) + 12);
    
    self.downView.frame = CGRectMake(12, MaxY(self.midView) + 12, self.view.mj_w - 24, 42 * 4);
    
    self.saveButton.frame = CGRectMake(12, MaxY(self.downView) + 67, self.view.mj_w - 24, 50);
    
    self.mainView.contentSize = CGSizeMake(self.view.mj_w, MaxY(self.saveButton) + 30);
}


- (void)layoutCountView{

    CGFloat numW = [self.numberTextField.text sizeWithAttributes:@{NSFontAttributeName:self.numberTextField.font}].width + 30;
    if (numW <= 46) {
        numW = 46;
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.numberTextField.frame = CGRectMake(self.midView.mj_w - 81 - numW - 4, MaxY(self.pImageView) + 26, numW, 24);
        self.mButton.frame = CGRectMake(self.numberTextField.mj_x - 2 - 24, self.numberTextField.mj_y, 24, 24);
        self.numDesLable.frame = CGRectMake( self.mButton.mj_x - 12 - 24 , self.numberTextField.mj_y, 30, 24);
    }];
    
}

- (void)mButtonClicked:(UIButton *)sender {
//    buttonCanUseAfterOneSec(sender);
    
    if (self.count <= 0) {
        self.count = 0;
        self.numberTextField.text = @"0";
        self.totalPriceLabel.text = @"0";
    }else {
        self.count --;
        
        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.count];
    }
    [self showTotalPrice];
}

- (void)aButtonClicked:(UIButton *)sender {
    if (self.count >= self.max) {
        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.max];
        self.count = self.max;
    }else {
        self.count ++;
        
        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.count];
    }
    [self showTotalPrice];
}

- (void)allButtonClicked:(UIButton *)sender {
    self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.max];
    self.count = self.max;
    [self showTotalPrice];
    [self layoutCountView];
}

- (void)showTotalPrice {
    if (self.max == 0) {
        return;
    }
    CGFloat totalMoney  = [self getTotalGoodsPrice];
    CGFloat totalPostMoney = [self getTotalPostMoney];

    self.payWayLabel.text = [NSString stringWithFormat:@"￥%@",[NSNumber numberWithFloat:totalPostMoney/1000]];
    if (self.count == 0) {
        self.totalPriceLabel.text = [NSString stringWithFormat:@"￥%@",[NSNumber numberWithFloat:0]];
        self.extraMoney.text = [NSString stringWithFormat:@"￥%@",[NSNumber numberWithFloat:0]];
        self.extraMoney.textColor = UIColorFromRGB(0x3A3C41);
        
        if (self.model.isPostage.intValue == 1) {//包邮
            self.payWayLabel.text = @"包邮";
        }else {
            self.payWayLabel.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.postageBase]];
        }
    }else{
        self.totalPriceLabel.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:totalMoney/1000.]]];
        CGFloat extraMoney = totalMoney + totalPostMoney - self.model.profitProduct.floatValue *1000 - self.model.profitPool.floatValue*1000  - self.model.earningsProduct.floatValue*1000;
        
        if (extraMoney > 0) {
            self.extraMoney.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:extraMoney/1000.]]];
            self.extraMoney.textColor = UIColorFromRGB(0xFF4A4A);
        }else {
            self.extraMoney.text = [NSString stringWithFormat:@"￥%@",[NSNumber numberWithFloat:0]];
            self.extraMoney.textColor = UIColorFromRGB(0xFF4A4A);
        }
        
        if (self.model.isPostage.intValue == 1) {//包邮
            self.payWayLabel.text = @"包邮";
        }else {
            self.payWayLabel.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:totalPostMoney/1000.]]];
        }
    }
    
}

//商品总价
- (double)getTotalGoodsPrice {
    return self.price * self.count;
}
//邮费
- (double)getTotalPostMoney {
    double totalPostMoney;
    if (self.model.isPostage.intValue == 1) {//包邮
        totalPostMoney = 0;
    }else {
        totalPostMoney = self.model.postageBase.integerValue*1000 + (self.count - 1)*self.model.postageStep.doubleValue*1000;
    }
    return totalPostMoney;
}


- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.model.area.length == 0 ||[_model.area isEqualToString:@"null"]) {
        [[XPShowTipsTool shareInstance] showMsg:@"请先添加收货地址!" ToView:k_keyWindow];
        return;
    }
    //当前可提货数量为0
    if (self.count <= 0 && self.max >0) {
        [[XPShowTipsTool shareInstance] showMsg:@"请选择提货数量" ToView:k_keyWindow];
        return;
    }
    if (self.count <= 0 && self.max == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"当前可提货数量为0" ToView:k_keyWindow];
        return;
    }
    
    //计算差价
    double extraMoneyValue =  [self getTotalGoodsPrice] + [self getTotalPostMoney] - self.model.profitProduct.doubleValue *1000 - self.model.profitPool.doubleValue *1000 - self.model.earningsProduct.doubleValue *1000;
    
    NSNumber *extraNumber = [NSNumber numberWithDouble:extraMoneyValue/1000.];
    self.extraMoneyValue = [XPMoneyFormatTool formatMoneyWithNum:extraNumber];
    if (self.extraMoneyValue.doubleValue <= 0) {//没有差价,直接提货
        
        [self.xpAlertView configWithTitle:@"确定要提货吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
            
        } rightAction:^(NSString * _Nullable extra) {
            [self postProduct];
        } alertType:XPAlertTypeNormal];
        
    }else {
        [self showPayView];
    }
}
- (void)showPayView{

    XPPaymentView *payView = [[XPPaymentView alloc] initWithFrame:self.view.bounds];
    self.payView = payView;
    [self.view addSubview:payView];
    
    [payView viewWithNeedMoney:[NSNumber numberWithFloat:self.extraMoneyValue.floatValue] accountMoney:@(0) type:XPPaymentViewTypeTakeGoods];
    
    XPWeakSelf
    [payView setNextBlock:^(NSInteger status, NSString * _Nonnull accountAmount) {
        [weakSelf orderPayWithPayType:status];
    }];
}
//三方支付支付
- (void)orderPayWithPayType:(NSInteger)payType{

    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_pick_goods];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"rechargeAmount"] = self.numberTextField.text;
    paraM[@"cashAmount"] = self.extraMoneyValue;
    paraM[@"id"] = [NSString stringWithFormat:@"%@",self.goodsNum];
    paraM[@"count"] = [NSString stringWithFormat:@"%zd",self.count];
    paraM[@"addressId"] = [NSString stringWithFormat:@"%@",self.model.addressId];
    paraM[@"productSkuId"] = self.productSkuId;
    paraM[@"payType"] = @(payType);
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            if(payType == 1){//支付宝
                [[XPPaymentManager shareInstance] showAlipayWithDic:data.userData viewController:self];
                self.isPayShow = YES;
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if (payType == 2) {//微信支付
                self.isPayShow = YES;
                [[XPPaymentManager shareInstance] showWeChatPayWithDic:data.userData viewController:self];
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if (payType == 11){//杉德
                if ([data.userData isKindOfClass:[NSDictionary class]]  && data.userData[@"fastPayUrl"]) {
                    self.isPayShow = YES;
                    self.sendyPayOrder = data.userData[@"orderNo"];
                    [[XPPaymentManager shareInstance] showPayViewWithUrl:data.userData[@"fastPayUrl"] viewController:self];
                }else{
                    [[XPShowTipsTool shareInstance] showMsg:@"服务器内部错误,请稍后重试" ToView:k_keyWindow];
                }
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
}
- (void)clearDefaultAddress:(NSNotification *)noti{
    
    XPAdressModel *model = [noti object];
    if (self.model.addressId && [model.productId isEqualToNumber:self.model.addressId]) {
        self.model.addressId = @(0);
        self.model.area = @"";
        [self freshUIWithModel:self.model];
    }
    
}
- (void)chooseAdress:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPUserAddressController *vc = [XPUserAddressController new];
    vc.entryType = 1;
    XPWeakSelf
    vc.chooseAdress = ^(XPAdressModel * _Nonnull model) {
        weakSelf.model.area = model.area;
        weakSelf.model.address = model.address;
        weakSelf.model.addressId = model.productId;
        weakSelf.model.phone = model.phone;
        weakSelf.model.name = model.name;
        [weakSelf freshUIWithModel:weakSelf.model];
    };
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    self.navBarView.backgroundColor = UIColor.whiteColor;
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"提货";
    [self.view addSubview:self.navBarView];
}

#pragma mark -- 页面数据
- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_pick_good_detail];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = self.goodsNum;
    paraM[@"productSkuId"] = self.productSkuId;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            XPPickGoodsModel *model = [XPPickGoodsModel mj_objectWithKeyValues:data.userData];
            self.model = model;
            [self freshUIWithModel:self.model];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//提货
- (void)postProduct {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_pick_goods];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = [NSString stringWithFormat:@"%@",self.goodsNum];
    paraM[@"count"] = [NSString stringWithFormat:@"%zd",self.count];
    paraM[@"addressId"] = [NSString stringWithFormat:@"%@",self.model.addressId];
    paraM[@"productSkuId"] = self.productSkuId;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[NSNotificationCenter defaultCenter] postNotificationName:Notification_shopList_fresh object:nil];
            [self jumpToSuccessControllerWithNum:data.userData[@"pickId"]];
        }else {
            [[XPShowTipsTool shareInstance] showLongDuringMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)jumpToSuccessControllerWithNum:(NSNumber *)num {
    XPTakeGoodsSuccessController *vc = [XPTakeGoodsSuccessController new];
    vc.goodsNum = num;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark --- textfieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == self.numberTextField) {
        if (self.numberTextField.text.length == 0) {
            self.count = 0;
            self.numberTextField.text = @"0";
            
            [self showTotalPrice];
            [self layoutCountView];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.numberTextField) {
        
        //限制只能输入：1234567890.
        NSCharacterSet * characterSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
        NSString * filtered = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        if (![string isEqualToString:filtered]) {
            return NO;
        }
        //首位是0
        if ([self.numberTextField.text isEqualToString:@"0"]) {
            if ([string isEqual:@"0"]) {
                return NO;
            }else{
                self.numberTextField.text = @"";
                return YES;
            }
        }
        // 获取当前文本框中的文本，包括用户输入的新字符
        NSString *currentText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        // 如果当前文本长度超过6位，则不允许输入更多字符
        NSInteger inputNum = [currentText integerValue];
        
        if (inputNum > self.max) {
            self.count = self.max;
            self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.count];
            
            [self showTotalPrice];
            [self layoutCountView];
            
            return NO;
        }else {
            self.count = inputNum;
            [self showTotalPrice];
            [self layoutCountView];
            return YES;
        }
    }
    // 允许输入
    return YES;
}
@end
