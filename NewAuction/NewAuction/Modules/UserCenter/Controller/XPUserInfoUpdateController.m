//
//  XPUserInfoUpdateController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/13.
//

#import "XPUserInfoUpdateController.h"
#import <Photos/Photos.h>
#import <AVFoundation/AVCaptureDevice.h>
#import <AVFoundation/AVMediaFormat.h>
#import "XPMd5Tool.h"
#import "UIButton+WebCache.h"
#import "AFNetworking.h"
#import "XPOSSUploadTool.h"
#import "UIImage+Compression.h"


@interface XPUserInfoUpdateController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) UITextField *roleTextField;
//用户角色图标
@property (nonatomic, strong) UIImageView *userRoleIcon;
//昵称
@property (nonatomic, strong) UITextField *nickTextField;
//qianming
@property (nonatomic, strong) UITextField *signTextField;
//用户名
@property (nonatomic, strong) UITextField *nameTextField;

@property (nonatomic, strong) UITextField *phoneTextField;

@property (nonatomic, strong) UITextField *inventCodetextfield;

@property (nonatomic, strong) UIImage *photo;

@property (nonatomic, strong) UIImageView *imageview;

@property (nonatomic, strong) UIButton *userIcon;

//顶部背景图
@property (nonatomic, strong) UIImageView *topBanner;

@end

@implementation XPUserInfoUpdateController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [self setupUI];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"个人信息";
    [self.view addSubview:self.navBarView];
    self.navBarView.backgroundColor = UIColor.clearColor;
}
- (void)setupUI {
    
    UIImageView *topBanner = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 290)];
    topBanner.image = [UIImage imageNamed:@"xp_user_temp1"];
    [self.view addSubview:topBanner];
    
    UIView *mainView = [[UIView alloc] init];
    mainView.layer.cornerRadius = 8;
    mainView.layer.masksToBounds = NO;
    mainView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
    mainView.frame = CGRectMake(12, 70 + kTopHeight, self.view.mj_w - 24, 300);
    mainView.layer.shadowColor = RGBACOLOR(51, 53, 113, 0.1).CGColor;
    mainView.layer.shadowOffset = CGSizeMake(0, 4);
    mainView.layer.shadowOpacity = 1;
    mainView.layer.shadowRadius = 4;
    
    [self.view addSubview:mainView];
    
    CGFloat leftMargin = 24;
    
    
    UILabel *label0 = [UILabel new];
    label0.frame = CGRectMake(leftMargin, 64, 250, 24);
    label0.font = kFontRegular(14);
    label0.textColor = UIColorFromRGB(0x8A8A8A);
    label0.text = @"用户等级";
    [mainView addSubview:label0];
    
    self.userRoleIcon = [UIImageView new];
    self.userRoleIcon.frame = CGRectMake(mainView.mj_w - 24 - 48, label0.mj_y + 5, 48, 48);
    [mainView addSubview:self.userRoleIcon];
    
    UITextField *textfield0 = [[UITextField alloc] initWithFrame:CGRectMake(leftMargin, MaxY(label0) + 5, mainView.mj_w - 24 * 3 , 24)];
    textfield0.font = kFontMedium(14);
    textfield0.textColor = UIColorFromRGB(0x3A3C41);
    textfield0.userInteractionEnabled = NO;
    self.roleTextField = textfield0;
    [mainView addSubview:textfield0];
    
    UIView *sepView0 = [[UIView alloc] init];
    [sepView0 setBackgroundColor: UIColorFromRGB(0xF2F2F2)];
    sepView0.frame = CGRectMake(leftMargin, MaxY(textfield0) + 4, mainView.mj_w - 24 * 2, 1);
    [mainView addSubview:sepView0];
    
    UILabel *label1 = [UILabel new];
    label1.frame = CGRectMake(leftMargin, MaxY(sepView0)+ 24, 250, 24);
    label1.font = kFontRegular(14);
    label1.textColor = UIColorFromRGB(0x8A8A8A);
    label1.text = @"用户昵称";
    [mainView addSubview:label1];
    
    UITextField *textfield1 = [[UITextField alloc] initWithFrame:CGRectMake(leftMargin, MaxY(label1) + 5, mainView.mj_w - 24 * 3 , 24)];
    textfield1.placeholder = @"";
    textfield1.font = kFontMedium(14);
    textfield1.textColor = UIColorFromRGB(0x3A3C41);
    [mainView addSubview:textfield1];
    self.nickTextField = textfield1;
    
    UIButton *editButton1 = [[UIButton alloc] init];
    editButton1.frame = CGRectMake(mainView.mj_w - 48, textfield1.mj_y, 24, 24);
    [editButton1 setImage:[UIImage imageNamed:@"xp_text_edit"] forState:UIControlStateNormal];
    [editButton1 addTarget:self action:@selector(editButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    editButton1.tag = 0;
    [mainView addSubview:editButton1];
    
    UIView *sepView = [[UIView alloc] init];
    [sepView setBackgroundColor: UIColorFromRGB(0xF2F2F2)];
    sepView.frame = CGRectMake(leftMargin, MaxY(textfield1) + 4, mainView.mj_w - 24 * 2, 1);
    [mainView addSubview:sepView];
    
    UILabel *label5 = [UILabel new];
    label5.frame = CGRectMake(leftMargin, MaxY(sepView)+ 24, 250, 24);
    label5.font = kFontRegular(14);
    label5.textColor = UIColorFromRGB(0x8A8A8A);
    label5.text = @"个人签名";
    [mainView addSubview:label5];
    
    UITextField *textfield5 = [[UITextField alloc] initWithFrame:CGRectMake(leftMargin, MaxY(label5) + 5, mainView.mj_w - 24 * 3 , 24)];
    textfield5.placeholder = @"";
    textfield5.font = kFontMedium(14);
    textfield5.textColor = UIColorFromRGB(0x3A3C41);
    [mainView addSubview:textfield5];
    self.signTextField = textfield5;
    
    UIButton *editButton5 = [[UIButton alloc] init];
    editButton5.frame = CGRectMake(mainView.mj_w - 48, textfield5.mj_y, 24, 24);
    [editButton5 setImage:[UIImage imageNamed:@"xp_text_edit"] forState:UIControlStateNormal];
    [editButton5 addTarget:self action:@selector(editButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    editButton1.tag = 1;
    [mainView addSubview:editButton5];
    
    
    UIView *sepView5 = [[UIView alloc] init];
    [sepView5 setBackgroundColor: UIColorFromRGB(0xF2F2F2)];
    sepView5.frame = CGRectMake(leftMargin, MaxY(textfield5) + 4, mainView.mj_w - 24 * 2, 1);
    [mainView addSubview:sepView5];
    
    
    UILabel *label2 = [UILabel new];
    label2.frame = CGRectMake(leftMargin, MaxY(sepView5)+ 24, 250, 24);
    label2.font = kFontRegular(14);
    label2.textColor = UIColorFromRGB(0x8A8A8A);
    label2.text = @"用户名";
    [mainView addSubview:label2];
    
    UITextField *textfield2 = [[UITextField alloc] initWithFrame:CGRectMake(leftMargin, MaxY(label2) + 5, mainView.mj_w - 24 * 3 , 24)];
    textfield2.font = kFontMedium(14);
    textfield2.textColor = UIColorFromRGB(0x3A3C41);
    textfield2.userInteractionEnabled = NO;
    textfield2.placeholder = @"实名认证后自动显示";
    [mainView addSubview:textfield2];
    
    self.nameTextField = textfield2;
    
    UIView *sepView2 = [[UIView alloc] init];
    [sepView2 setBackgroundColor: UIColorFromRGB(0xF2F2F2)];
    sepView2.frame = CGRectMake(leftMargin, MaxY(textfield2) + 4, mainView.mj_w - 24 * 2, 1);
    [mainView addSubview:sepView2];
    
    UILabel *label3 = [UILabel new];
    label3.frame = CGRectMake(leftMargin, MaxY(sepView2)+ 24, 250, 24);
    label3.font = kFontRegular(14);
    label3.textColor = UIColorFromRGB(0x8A8A8A);
    label3.text = @"手机号";
    [mainView addSubview:label3];
    
    UITextField *textfield3 = [[UITextField alloc] initWithFrame:CGRectMake(leftMargin, MaxY(label3) + 5, mainView.mj_w - 24 * 3 , 24)];
    textfield3.font = kFontMedium(14);
    textfield3.textColor = UIColorFromRGB(0x3A3C41);
    textfield3.userInteractionEnabled = NO;
    [mainView addSubview:textfield3];
    self.phoneTextField = textfield3;
    
    UIView *sepView3 = [[UIView alloc] init];
    [sepView3 setBackgroundColor: UIColorFromRGB(0xF2F2F2)];
    sepView3.frame = CGRectMake(leftMargin, MaxY(textfield3) + 4, mainView.mj_w - 24 * 2, 1);
    [mainView addSubview:sepView3];
    
    UILabel *label4 = [UILabel new];
    label4.frame = CGRectMake(leftMargin, MaxY(sepView3)+ 24, 250, 24);
    label4.font = kFontRegular(14);
    label4.textColor = UIColorFromRGB(0x8A8A8A);
    label4.text = @"邀请码";
    [mainView addSubview:label4];
    
    UITextField *textField4 = [[UITextField alloc] initWithFrame:CGRectMake(leftMargin, MaxY(label4) + 5, mainView.mj_w - 24 * 3 , 24)];
    textField4.font = kFontMedium(14);
    textField4.textColor = UIColorFromRGB(0x3A3C41);
    textField4.userInteractionEnabled = NO;
    [mainView addSubview:textField4];
    self.inventCodetextfield = textField4;
    
    UIButton *pastButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [pastButton setTitle:@"复制" forState:UIControlStateNormal];
    pastButton.frame = CGRectMake(mainView.mj_w - 60, textField4.mj_y,50, textField4.mj_h);
    [pastButton setImage:[UIImage imageNamed:@"xp_copy"] forState:UIControlStateNormal];
    [pastButton setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
    pastButton.titleLabel.font = kFontMedium(14);
    [pastButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, - 26)];
    [pastButton setTitleEdgeInsets:UIEdgeInsetsMake(0, - 77, 0, 0)];
    [pastButton addTarget:self action:@selector(pasteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:pastButton];
    
    UIView *sepView4 = [[UIView alloc] init];
    [sepView4 setBackgroundColor: UIColorFromRGB(0xF2F2F2)];
    sepView4.frame = CGRectMake(leftMargin, MaxY(textField4) + 4, mainView.mj_w - 24 * 2, 1);
    [mainView addSubview:sepView4];
    mainView.frame = CGRectMake(12, 70 + kTopHeight, self.view.mj_w - 24, MaxY(sepView4) + 24);
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(28,SCREEN_HEIGHT - self.safeBottom - 50, SCREEN_WIDTH - 2 *28, 50);
    [saveButton setTitle:@"保 存" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    saveButton.layer.cornerRadius = 25;
    saveButton.clipsToBounds = YES;
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveButton];
    
    UIButton *userIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    userIcon.frame = CGRectMake(36, kTopHeight + 30, 80, 80);;
    userIcon.layer.cornerRadius = 40;
    userIcon.layer.masksToBounds = YES;
    userIcon.layer.borderColor = [UIColor whiteColor].CGColor;
    userIcon.layer.borderWidth = 1;
    [userIcon addTarget:self action:@selector(iconClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.userIcon = userIcon;
    [self.view addSubview:userIcon];
    
    
    NSString *nickName;
    if (self.userModel.nickname.length > 0) {
        nickName = self.userModel.nickname;
    }else if ([XPLocalStorage readUserNickName]) {
        nickName = [XPLocalStorage readUserNickName];
    }else {
        nickName = @"";
    }
    
    NSString *userName = [XPLocalStorage readUserUserName];
    if (self.userModel.name.length > 0) {
        userName = self.userModel.name;
    }else if (userName.length >0) {
        
    }else {
        userName = @"";
    }
    
    NSString *userInventCode = [XPLocalStorage readUserInventCode];
    if (self.userModel.invCode.length > 0) {
        userInventCode = self.userModel.invCode;
    }
    
    NSString *userPhone = [XPLocalStorage readUserUserPhone];
    if (self.userModel.phone) {
        userPhone = [NSString stringWithFormat:@"%@",self.userModel.phone];
    }else {
        userPhone = @"";
    }
    
    NSString *introduce;
    if (self.userModel.introduce && self.userModel.introduce.length > 0) {
        introduce = [NSString stringWithFormat:@"%@",self.userModel.introduce];
    }else {
        introduce = @"";
    }
    
    topBanner.image = [XPUserDataTool getUserRoleBackgroundImage:self.userModel.userType.integerValue];
    self.roleTextField.text = [NSString stringWithFormat:@"LV%@ %@",self.userModel.userType,[XPUserDataTool getUserRole:self.userModel.userType.integerValue]];
    self.userRoleIcon.image = [XPUserDataTool getUserRoleIcon:self.userModel.userType.integerValue];
    
    self.nickTextField.text = nickName;
    self.nameTextField.text = userName;
    self.phoneTextField.text = userPhone;
    self.signTextField.text = introduce;
    self.inventCodetextfield.text = userInventCode;
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:self.userModel.head] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"xp_user_icon"] options:0];
    
}
- (void)editButtonClicked:(UIButton *)sender {
    if (sender.tag == 0) {
        [self.nickTextField becomeFirstResponder];
    }else if (sender.tag == 1){
        [self.signTextField becomeFirstResponder];
    }
}
- (void)saveButtonClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    if (![self validVerifyPredicate:@"^[\u4e00-\u9fa5A-Za-z]{2,10}$" TargetString:self.nickTextField.text]) {
        [[XPShowTipsTool shareInstance]showMsg:@"请输入2-10位汉字或字母" ToView:self.view];
        return;
    }
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"nickName"] = self.nickTextField.text;
    paraM[@"introduce"] = self.signTextField.text;
    [self updateUserInfoWithParaM:paraM];
}
//更新用户信息
-(void)updateUserInfoWithParaM:(NSDictionary *)paraM{
    //修改用户名
   NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_User_update_userinfo];
    
    [self startLoadingGif];
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.userModel.nickname = self.nickTextField.text;
            [[XPShowTipsTool shareInstance]showMsg:@"修改成功" ToView:k_keyWindow];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else {
            [[XPShowTipsTool shareInstance]showMsg:@"修改失败,请稍后重试" ToView:k_keyWindow];
        }
    }];
}


//正则
-(BOOL)validVerifyPredicate:(NSString *) pString TargetString:(NSString *)targerString
{
   NSPredicate *num = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pString];
   return [num evaluateWithObject:targerString];
}

-(void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)pasteButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.inventCodetextfield.text;
    [[XPShowTipsTool shareInstance] showMsg:@"复制成功!" ToView:k_keyWindow];
    
}

- (void)openAlbum {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}

- (void)openCamera {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:true completion:^{
        
    }];
    self.photo = [info objectForKey:UIImagePickerControllerEditedImage];
    NSString *type  = [info objectForKey:UIImagePickerControllerMediaType];
    //选择的类型图片
    if ([type isEqualToString:@"public.image"]) {
        //先把图片转成NSData
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        NSData *data;
            data = UIImageJPEGRepresentation(image, 0.5);
        [self uploadCardWithImage:image];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
//    上传
    
}
- (void)uploadCardWithImage:(UIImage *)iConImage {
    
    iConImage = [iConImage compressToImage];
    NSString *filePath = [[XPOSSUploadTool shareInstance] saveImageToSandbox:iConImage];
    [self startLoadingGif];
    [[XPOSSUploadTool shareInstance] uploadFileWithMediaType:XPMediaTypeUserHead filePath:filePath progress:^(CGFloat progress) {
        
    } completeBlock:^(BOOL success, NSString * _Nonnull remoteUrlStr) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success) {
                //修改用户名
               NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_User_update_userinfo];
               NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
               paraM[@"head"] = remoteUrlStr;
               [self updateUserInfoWithParaM:paraM];
            }else{
                [[XPShowTipsTool shareInstance] showMsg:@"上传失败,请稍后重试!" ToView:k_keyWindow];
            }
        });
    }];
    
}

//上传图片
-(void)showAlert{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"上传头像" preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       
    }];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openCamera];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openAlbum];
    }];
    [alert addAction:cancel];
    [alert addAction:action];
    [alert addAction:action2];

    [self presentViewController:alert animated:YES completion:nil];
    
    
}
- (void)iconClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self showAlert];
    
}

@end
