//
//  XPShareViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/8/26.
//

#import "XPShareViewController.h"
#import <Photos/PHPhotoLibrary.h>

@interface XPShareViewController ()

@property (nonatomic, strong) UIScrollView *mainView;

@property (nonatomic, strong) UIImageView *bgImageView;

@property (nonatomic, strong) UIImageView *codeImageView;

@property (nonatomic, strong) UILabel *inventLabel;

@end

@implementation XPShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0x668cee);
//    self.view.backgroundColor = UIColor.redColor;
    [self setupUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getData];
}

- (void)setupUI{
    
    UIImage *bgImage = [UIImage imageNamed:@"xp_share_save"];
    CGFloat bgH = bgImage.size.height * SCREEN_WIDTH /bgImage.size.width;
//    CGFloat originY = (SCREEN_HEIGHT - kTopHeight - bgH)/2.;
    CGFloat codeW = SCREEN_WIDTH *0.39;
    
    UIImageView *bgImageView = [[UIImageView alloc] init];
    bgImageView.image = bgImage;
    bgImageView.frame = CGRectMake(0,kTopHeight, SCREEN_WIDTH, bgH);
    self.bgImageView = bgImageView;
    [self.view addSubview:bgImageView];
    
    UIImageView *codeImageView = [UIImageView new];
    codeImageView.frame = CGRectMake((SCREEN_WIDTH - codeW)/2., bgH * 0.685, codeW, codeW);
//    codeImageView.backgroundColor = UIColor.redColor;
    self.codeImageView = codeImageView;
    [bgImageView addSubview:codeImageView];
    
    UILabel *inventLabel = [UILabel new];
    inventLabel.font = kFontMedium(20);
    inventLabel.textColor = UIColorFromRGB(0x2766ff);
    inventLabel.textAlignment = NSTextAlignmentCenter;
    inventLabel.frame = CGRectMake(0, MaxY(self.codeImageView), SCREEN_WIDTH, 0.06 * bgH);
    self.inventLabel = inventLabel;
    [bgImageView addSubview:inventLabel];
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"我的分享";
    [self.view addSubview:self.navBarView];
    
    [self.navBarView.rightButton setTitle:@"保存" forState:UIControlStateNormal];
    [self.navBarView.rightButton setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
    [self.navBarView.rightButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.rightButton.titleLabel.font = kFontRegular(16);
    self.navBarView.rightButton.frame = CGRectMake(SCREEN_WIDTH - 10 - 120, self.navBarView.mj_h - 44, 120, 44);
}

- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kUser_share_url];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [self.codeImageView sd_setImageWithURL:[NSURL URLWithString:data.userData[@"qrCodeAddress"]]];
            self.inventLabel.text = [NSString stringWithFormat:@"邀请码: %@",data.userData[@"inventCode"]];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (status == PHAuthorizationStatusAuthorized) {
                
                UIImage *image = [XPCommonTool viewToImage:self.bgImageView];
                //权限申请
                UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
                
            }else {
                UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示"
                                                                                message:@"相册未授权,请开启相册权限"
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *left = [UIAlertAction actionWithTitle:@"取消"                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                }];
                
                UIAlertAction *right = [UIAlertAction actionWithTitle:@"前往设置"                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    if ([[UIApplication sharedApplication] canOpenURL:url]) {
                        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                    }
                }];
                [alertC addAction:left];
                [alertC addAction:right];
                [self presentViewController:alertC animated:YES completion:nil];
            }
            
        });
    }];
    
    
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (!error) {
        [[XPShowTipsTool shareInstance] showMsg:@"保存成功,请前往相册查看!" ToView:k_keyWindow];
    }else {
        [[XPShowTipsTool shareInstance] showMsg:@"保存失败!" ToView:k_keyWindow];
    }
}

@end
