//
//  XPUserScorePreUploadController.m
//  NewAuction
//
//  Created by Grasp_L on 2025/3/3.
//

#import "XPUserScorePreUploadController.h"
#import "XPUserScoreUploadController.h"
#import "XPHitButton.h"

@interface XPUserScorePreUploadController ()
//返回按钮
@property (nonatomic, strong) XPHitButton *backButton;
@end

@implementation XPUserScorePreUploadController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupViews];
}

-(void)setupViews {
    UIImageView *icon = [[UIImageView alloc] initWithFrame:self.view.bounds];
    icon.image = [UIImage imageNamed:@"xp_keep_bg"];
    [self.view addSubview:icon];
    
    
    self.backButton = [[XPHitButton alloc] init];
    self.backButton.frame = CGRectMake(20, 17 + self.safeTop, 27, 27);
    self.backButton.hitRect = 50;
    [self.backButton setImage:[UIImage imageNamed:@"xp_nav_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.backButton];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(50, self.view.mj_h - 300, self.view.mj_w - 100, 200);
//    button.backgroundColor = UIColor.lightGrayColor;
    [self.view addSubview:button];
    
    
}

-(void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPUserScoreUploadController *vc = [XPUserScoreUploadController new];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}




@end
