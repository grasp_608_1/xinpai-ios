//
//  XPPackageDetailController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/2.
//

#import "XPPackageDetailController.h"
#import "XPLogisticsModel.h"
#import "XPLogisticsCell.h"
#import "XPPackageView.h"
#import "XPPackagePopView.h"
#import "XPShopOrderModel.h"

@interface XPPackageDetailController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *emptyView;
//顶部包裹信息
@property (nonatomic, strong) XPPackageView *packageView;
//顶部包裹信息
@property (nonatomic, strong) XPPackagePopView *packagePopView;
//收货地址
@property (nonatomic, strong) UIView *locationView;
//地址栏目
@property (nonatomic, strong) UILabel *userInfoLabel;
@property (nonatomic, strong) UILabel *phoneSerectLabel;
@property (nonatomic, strong) UILabel *adressLabel;
@property (nonatomic, strong) UIImageView *rightArrow;
@property (nonatomic, strong) UIImageView *adressImageView;

@property (nonatomic, strong) XPShopOrderModel *model;

@property (nonatomic, strong) UILabel *titleLabel;
//数据源
@property (nonatomic, copy) NSArray *dataArray;
//高度Array
@property (nonatomic, copy) NSArray *heightArray;
//当前页码
@property (nonatomic, assign) NSInteger page;

@property (nonatomic, assign) NSInteger currentIndex;

@end

@implementation XPPackageDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    self.dataArray = [NSMutableArray array];
    self.page = 1;
    self.currentIndex = self.defaultIndex;
    
    [self setupUI];
    [self getPackageInfo];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}


- (void)setupUI {
    
    XPPackageView *packageView = [[XPPackageView alloc] initWithFrame:CGRectMake(12,kTopHeight + 12, self.view.mj_w - 24, 116)];
    packageView.hidden = YES;
    packageView.canSelect = YES;
    self.packageView = packageView;
    [self.view addSubview:packageView];
    
    UIView *locationView = [UIView new];
    locationView.backgroundColor = UIColor.whiteColor;
    locationView.layer.cornerRadius = 8;
    locationView.frame = CGRectMake(12,MaxY(self.packageView) + 12, self.view.mj_w - 24, 0);
    locationView.clipsToBounds = YES;
    self.locationView = locationView;
    [self.view addSubview:locationView];
    
    UIImageView *locationLogo = [UIImageView new];
    locationLogo.image = [UIImage imageNamed:@"xp_location"];
    self.adressImageView = locationLogo;
    [locationView addSubview:locationLogo];
    
    UILabel *userInfoLabel = [UILabel new];
    userInfoLabel.textColor = UIColorFromRGB(0x3A3C41);
    userInfoLabel.font = kFontMedium(16);
    self.userInfoLabel = userInfoLabel;
    [locationView addSubview:userInfoLabel];
    
    self.phoneSerectLabel = [UILabel new];
    self.phoneSerectLabel.font = kFontRegular(12);
    self.phoneSerectLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.phoneSerectLabel.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
    self.phoneSerectLabel.layer.borderWidth = 1;
    self.phoneSerectLabel.textAlignment = NSTextAlignmentCenter;
    [locationView addSubview:self.phoneSerectLabel];
    
    UILabel *adressLabel = [UILabel new];
    adressLabel.textColor = UIColorFromRGB(0x8A8A8A);
    adressLabel.numberOfLines = NO;
    adressLabel.font = kFontRegular(14);
    self.adressLabel = adressLabel;
    [locationView addSubview:adressLabel];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, MaxY(self.locationView) + 12, SCREEN_WIDTH, SCREEN_HEIGHT - MaxY(self.locationView) - 12);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBottomHeight)];
    [self.view addSubview:self.tableView];

    self.emptyView = [[UIView alloc] initWithFrame:CGRectMake(12, MaxY(self.locationView) + 50, self.view.mj_w - 24,80 )];
    self.emptyView.backgroundColor = UIColor.whiteColor;
    self.emptyView.layer.cornerRadius = 8;
    self.emptyView.hidden = YES;
    self.tableView.hidden = YES;
    [self.view addSubview:self.emptyView];
    UILabel *emptyStatusLabel = [UILabel new];
    emptyStatusLabel.font = kFontMedium(16);
    emptyStatusLabel.textColor = app_font_color;
    emptyStatusLabel.textAlignment = NSTextAlignmentCenter;
    emptyStatusLabel.text = @"包裹未发货";
    emptyStatusLabel.frame = CGRectMake(0, 12, self.emptyView.mj_w, 24);
    [self.emptyView addSubview:emptyStatusLabel];
    
    UILabel *emptyDescLabel = [UILabel new];
    emptyDescLabel.font = kFontRegular(14);
    emptyDescLabel.textColor = app_font_color_8A;
    emptyDescLabel.textAlignment = NSTextAlignmentCenter;
    emptyDescLabel.text = @"您的商品还打包中，请耐心等待";
    emptyDescLabel.frame = CGRectMake(0, 44, self.emptyView.mj_w, 24);
    [self.emptyView addSubview:emptyDescLabel];
    
    self.tableView.tableHeaderView = [self getTabelViewHeaderView];
    self.tableView.tableFooterView = [self getTabelViewFooterView];
    
}
- (UIView *)getTabelViewHeaderView {
    UIView *header = [UIView new];
    header.frame = CGRectMake(0, 0, SCREEN_WIDTH, 48);
    
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 0, header.mj_w - 12 * 2, 48);
    [header addSubview:mainView];
    [XPCommonTool cornerRadius:mainView andType:1 radius:8];

    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontRegular(14);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(12, 0, header.mj_w, mainView.mj_h);
    self.titleLabel = titleLabel;
    [mainView addSubview:titleLabel];
    
    UIButton *pastButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [pastButton setTitle:@"复制" forState:UIControlStateNormal];
    pastButton.frame = CGRectMake(mainView.mj_w - 52, 0,52,mainView.mj_h);
    [pastButton setTitleColor:app_font_color forState:UIControlStateNormal];
    pastButton.titleLabel.font = kFontMedium(14);
    [pastButton addTarget:self action:@selector(pasteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:pastButton];
    
    UIView *sepView = [UIView new];
    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    sepView.frame = CGRectMake(0, mainView.mj_h - 1, mainView.mj_w, 1);
    [mainView addSubview:sepView];
    
    return header;
}

- (UIView *)getTabelViewFooterView {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 12 + kBottomHeight)];
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 0, footerView.mj_w - 12 * 2, 12);
    [footerView addSubview:mainView];
    [XPCommonTool cornerRadius:mainView andType:2 radius:8];
    return footerView;
}


- (void)freshUI {
    self.packageView.hidden = NO;
    self.locationView.hidden = NO;
    [self.packageView viewWithArray:self.model.mallOrderPackageListVOS];
    //地址信息
    //送货地址信息
    self.adressImageView.frame = CGRectMake(12,12, 24, 24);
    NSString *userPhone = self.model.receiverPhone.length >= 11 ? [self.model.receiverPhone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"] : self.model.receiverPhone;
    self.userInfoLabel.text = [NSString stringWithFormat:@"%@ %@",self.model.receiverName,userPhone];
    CGFloat userInfoW = [self.userInfoLabel.text sizeWithAttributes:@{NSFontAttributeName:self.userInfoLabel.font}].width;
    self.userInfoLabel.frame = CGRectMake(48, 12, userInfoW + 12, 24);
    self.phoneSerectLabel.text = @"号码保护中";
    self.phoneSerectLabel.frame = CGRectMake(MaxX(self.userInfoLabel), 15, 72, 18);
    
    self.adressLabel.text = self.model.receiverAddress;
    CGFloat addressW  = self.locationView.mj_w - 48 - 12;
    CGFloat addressH = [self.adressLabel.text boundingRectWithSize:CGSizeMake(addressW, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.adressLabel.font} context:nil].size.height;
    
    self.adressLabel.frame = CGRectMake(48, MaxY(self.userInfoLabel) + 6, self.locationView.mj_w - 48 - 12, addressH);
    self.locationView.frame = CGRectMake(12,MaxY(self.packageView) + 12, self.view.mj_w - 24, MaxY(self.adressLabel) + 12);
    
    self.tableView.frame = CGRectMake(0, MaxY(self.locationView) + 12, SCREEN_WIDTH, SCREEN_HEIGHT - MaxY(self.locationView) - 12);
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView.frame = CGRectMake(12, MaxY(self.locationView) + 12, self.view.mj_w - 24,80);
    
    
    XPWeakSelf
    
    [self.packageView setItemBlock:^(NSInteger tag) {
        [weakSelf selectPackAtIndex:tag];
    }];
    
    [self.packageView setRightBlock:^{
        XPPackagePopView *popView = [[XPPackagePopView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        weakSelf.packagePopView = popView;
        [k_keyWindow addSubview:popView];
        [popView viewWithArray:weakSelf.model.mallOrderPackageListVOS];
        
        [weakSelf.packagePopView setArrowBlock:^(NSInteger tag) {
            weakSelf.currentIndex = tag;
            [weakSelf selectPackAtIndex:tag];
        }];
        
    }];
    
}

- (void)selectPackAtIndex:(NSInteger)index{
    
    self.dataArray = @[];
    self.heightArray = [self getCellHeightArray];
    [self.tableView reloadData];

    NSArray *packageArray = self.model.mallOrderPackageListVOS;
    NSDictionary *dict = packageArray[index];
    NSNumber *deliveryStatus = dict[@"deliveryStatus"];
    
    if (deliveryStatus.intValue == 1) {
        self.titleLabel.text = [NSString stringWithFormat:@"%@  %@",dict[@"deliveryCompany"],dict[@"deliverySn"]];
        [self getDataWithInfo:dict];
        
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
        
    }else {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }
}


- (void)pasteButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    NSArray *packageArray = self.model.mallOrderPackageListVOS;
    NSDictionary *dict = packageArray[self.currentIndex];
    pasteboard.string = dict[@"deliverySn"];
    
    [[XPShowTipsTool shareInstance] showMsg:@"复制成功!" ToView:k_keyWindow];
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"物流信息";
    [self.view addSubview:self.navBarView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPLogisticsCell *cell = [XPLogisticsCell cellWithTableView:tableView];
    XPLogisticsModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    
    if (self.dataArray.count == 1) {
        cell.topLine.hidden = YES;
        cell.downLine.hidden = YES;
        cell.circleView.backgroundColor = UIColorFromRGB(0x6E6BFF);
        cell.timeLabel.textColor = UIColorFromRGB(0x6E6BFF);
        cell.desLabel.textColor = app_font_color;
//        [XPCommonTool cornerRadius:cell.mainView andType:2 radius:8];
        
    }else if (indexPath.row == 0){
        cell.topLine.hidden = YES;
        cell.downLine.hidden = NO;
        cell.circleView.backgroundColor = UIColorFromRGB(0x6E6BFF);
        cell.timeLabel.textColor = UIColorFromRGB(0x6E6BFF);
        cell.desLabel.textColor = app_font_color;
//        [XPCommonTool cornerRadius:cell.mainView andType:0 radius:8];
        
    }else if (indexPath.row == self.dataArray.count - 1){
        cell.topLine.hidden = NO;
        cell.downLine.hidden = YES;
        cell.circleView.backgroundColor = UIColorFromRGB(0xDEDEDE);
        cell.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
        cell.desLabel.textColor = UIColorFromRGB(0x8A8A8A);
//        [XPCommonTool cornerRadius:cell.mainView andType:2 radius:8];
    }else {
        cell.topLine.hidden = NO;
        cell.downLine.hidden = NO;
        cell.circleView.backgroundColor = UIColorFromRGB(0xDEDEDE);
        cell.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
        cell.desLabel.textColor = UIColorFromRGB(0x8A8A8A);
//        [XPCommonTool cornerRadius:cell.mainView andType:0 radius:8];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = self.heightArray[indexPath.row];
    return height.floatValue;
}

- (void)getDataWithInfo:(NSDictionary *)dict {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_product_express];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"orderId"] = dict[@"deliverySn"];
    paraM[@"tplCode"] = dict[@"deliveryCode"];
    
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             
             NSArray *dataArray = data.userData[@"data"];
             NSArray *modelArray = [XPLogisticsModel mj_objectArrayWithKeyValuesArray:dataArray];

             self.dataArray = modelArray;
             self.heightArray = [self getCellHeightArray];

             [self.tableView reloadData];
             
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
}

- (void)getPackageInfo {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_product_package_detail];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = self.orderId;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.model = [XPShopOrderModel mj_objectWithKeyValues:data.userData];
            [self freshUI];
            self.packageView.defaultSeletIndex = self.defaultIndex;
            [self selectPackAtIndex:self.defaultIndex];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}


- (NSArray *)getCellHeightArray {
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPLogisticsCell *cell = [XPLogisticsCell new];
    for (int i = 0 ; i< self.dataArray.count; i++) {
        XPLogisticsModel *model = self.dataArray[i];
        CGFloat height = [cell cellWithModel:model];
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    return tempArray.copy;
}
@end
