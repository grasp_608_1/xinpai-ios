//
//  XPUserAddressEditController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/13.
//

#import "XPUserAddressEditController.h"
#import "BRAddressPickerView.h"
#import "XPHitButton.h"
#import "IQKeyboardManager.h"

@interface XPUserAddressEditController ()

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UILabel *phoneLabel;

@property (nonatomic, strong) UILabel *areaLabel;

@property (nonatomic, strong) UILabel *adresslabel;


@property (nonatomic, strong) UITextField *nameTextfield;
@property (nonatomic, strong) UITextField *phoneTextfield;
@property (nonatomic, strong) UILabel *areaChoosedLabel;
@property (nonatomic, strong) IQTextView *areaTextView;
//本地存的地址,带,分割
@property (nonatomic, copy) NSString *localArea;

@property (nonatomic, copy) NSString *areaStr;

@property (nonatomic, strong) XPHitButton *selectButton;

@end

@implementation XPUserAddressEditController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    
    [self setupUI];
    [self initData];
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.navBarView];
    
    [self.navBarView.rightButton setTitle:@"删除" forState:UIControlStateNormal];
    [self.navBarView.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    [self.navBarView.rightButton addTarget:self action:@selector(deleteAdress:) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.rightButton.titleLabel.font = kFontRegular(16);
    
}
- (void)setupUI {
    
    UIView *mainView = [[UIView alloc] init];
    mainView.layer.cornerRadius = 4;
    mainView.layer.masksToBounds = YES;
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 12 + kTopHeight, self.view.mj_w - 24, 376);
    [self.view addSubview:mainView];
    
    UILabel *msgLabel = [[UILabel alloc] init];
    msgLabel.font = kFontRegular(14);
    msgLabel.textColor = UIColorFromRGB(0x3A3C41);
    msgLabel.text = @"地址信息";
    msgLabel.frame = CGRectMake(12, 16, 56, 20);
    [mainView addSubview:msgLabel];
    
    UILabel *defaultLabel = [[UILabel alloc] init];
    defaultLabel.font = kFontRegular(14);
    defaultLabel.textColor = UIColorFromRGB(0x3A3C41);
    defaultLabel.text = @"设为默认收货地址";
    defaultLabel.frame = CGRectMake(mainView.mj_w - 112 - 18, 16, 112, 20);
    [mainView addSubview:defaultLabel];
    
    XPHitButton *selectButton = [[XPHitButton alloc] init];
    selectButton.hitRect = 30;
    self.selectButton = selectButton;
    [selectButton setImage:[UIImage imageNamed:@"xp_xbox_selected"] forState:UIControlStateSelected];
    [selectButton setImage:[UIImage imageNamed:@"xp_xbox_unselect"] forState:UIControlStateNormal];
    if (self.model.isDefault.intValue == 1) {
        selectButton.selected = YES;
    }else {
        selectButton.selected = NO;
    }
    
    selectButton.frame = CGRectMake(mainView.mj_w - 136 -16, 19, 16, 16);
    [selectButton addTarget:self action:@selector(selectedButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:selectButton];
    
    
    
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.font = kFontRegular(14);
    nameLabel.textColor = UIColorFromRGB(0x3A3C41);
    nameLabel.text = @"收件人";
    nameLabel.frame = CGRectMake(12, 26 + MaxY(defaultLabel), 56, 20);
    [mainView addSubview:nameLabel];
    
    UILabel *starLabel = [[UILabel alloc] init];
    starLabel.font = kFontRegular(14);
    starLabel.textColor = UIColorFromRGB(0xFF4A4A);
    starLabel.text = @"*";
    starLabel.frame = CGRectMake(12,26 + MaxY(defaultLabel) , 56, 10);
    starLabel.textAlignment = NSTextAlignmentRight;
    [mainView addSubview:starLabel];
    
    UITextField *textField1 = [UITextField new];
    textField1.backgroundColor = UIColorFromRGB(0xFAFAFA);
    textField1.font = kFontRegular(14);
    textField1.textColor = UIColorFromRGB(0x3A3C41);
    textField1.placeholder = @"请输入收件人名字";
    textField1.frame= CGRectMake(MaxX(nameLabel) + 28, 16 + MaxY(defaultLabel), mainView.mj_w - (MaxX(nameLabel) + 28) - 12, 44);
    self.nameTextfield = textField1;
    [mainView addSubview:textField1];
    
    
    UILabel *phoneLabel = [[UILabel alloc] init];
    phoneLabel.font = kFontRegular(16);
    phoneLabel.textColor = UIColorFromRGB(0x3A3C41);
    phoneLabel.text = @"手机号";
    phoneLabel.frame = CGRectMake(12, MaxY(nameLabel) + 38, 56, 20);
    [mainView addSubview:phoneLabel];
    
    UILabel *starLabel2 = [[UILabel alloc] init];
    starLabel2.font = kFontRegular(14);
    starLabel2.textColor = UIColorFromRGB(0xFF4A4A);
    starLabel2.text = @"*";
    starLabel2.frame = CGRectMake(12, phoneLabel.mj_y, 56, 10);
    starLabel2.textAlignment = NSTextAlignmentRight;
    [mainView addSubview:starLabel2];
    
    UITextField *textField2 = [UITextField new];
    textField2.backgroundColor = UIColorFromRGB(0xFAFAFA);
    textField2.font = kFontRegular(14);
    textField2.textColor = UIColorFromRGB(0x3A3C41);
    textField2.frame= CGRectMake(textField1.mj_x, MaxY(textField1) + 12, textField1.mj_w, 44);
    textField2.keyboardType = UIKeyboardTypeNumberPad;
    textField2.placeholder = @"请输入手机号";
    self.phoneTextfield = textField2;
    [mainView addSubview:textField2];
    
    UILabel *areaLabel = [[UILabel alloc] init];
    areaLabel.font = kFontRegular(16);
    areaLabel.textColor = UIColorFromRGB(0x3A3C41);
    areaLabel.text = @"所在地区";
    areaLabel.frame = CGRectMake(12, MaxY(phoneLabel) + 38, 72, 20);
    [mainView addSubview:areaLabel];
    
    UILabel *starLabel3 = [[UILabel alloc] init];
    starLabel3.font = kFontRegular(14);
    starLabel3.textColor = UIColorFromRGB(0xFF4A4A);
    starLabel3.text = @"*";
    starLabel3.frame = CGRectMake(12, areaLabel.mj_y, 72, 10);
    starLabel3.textAlignment = NSTextAlignmentRight;
    [mainView addSubview:starLabel3];
    
    UIButton *chooseButton = [UIButton buttonWithType: UIButtonTypeCustom];
    chooseButton.frame = CGRectMake(textField1.mj_x, MaxY(textField2) + 12, textField1.mj_w, 44);
    [chooseButton addTarget:self action:@selector(chooseButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    chooseButton.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [mainView addSubview:chooseButton];
    
    self.areaChoosedLabel = [UILabel new];
    self.areaChoosedLabel.font = kFontRegular(14);
    self.areaChoosedLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.areaChoosedLabel.text = @"请选择所属地区";
    self.areaChoosedLabel.frame = CGRectMake(12, 0, chooseButton.mj_w - 28 - 2, chooseButton.mj_h);
    [chooseButton addSubview:self.areaChoosedLabel];
    
    UIImageView *arrow = [[UIImageView alloc] init];
    arrow.image = [UIImage imageNamed:@"xp_setting_arrow"];
    arrow.frame = CGRectMake(chooseButton.mj_w - 28, 12, 24, 24);
    [chooseButton addSubview:arrow];
    
    
    UILabel *detailLabel = [[UILabel alloc] init];
    detailLabel.font = kFontRegular(16);
    detailLabel.textColor = UIColorFromRGB(0x3A3C41);
    detailLabel.text = @"详细地址";
    detailLabel.frame = CGRectMake(12, MaxY(areaLabel) + 40, 72, 20);
    [mainView addSubview:detailLabel];
    
    UILabel *starLabel4 = [[UILabel alloc] init];
    starLabel4.font = kFontRegular(14);
    starLabel4.textColor = UIColorFromRGB(0xFF4A4A);
    starLabel4.text = @"*";
    starLabel4.frame = CGRectMake(12, detailLabel.mj_y, 72, 10);
    starLabel4.textAlignment = NSTextAlignmentRight;
    [mainView addSubview:starLabel4];
    
    IQTextView *textView = [[IQTextView alloc] init];
    textView.font = kFontRegular(14);
    textView.textColor = UIColorFromRGB(0x3A3C41);
    textView.contentInset = UIEdgeInsetsMake(12, 12, 12, 12);
    textView.frame = CGRectMake(textField1.mj_x, MaxY(chooseButton) + 12, textField1.mj_w, 96);
    textView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    textView.placeholder = @"请输入详细地址";
    self.areaTextView = textView;
    [mainView addSubview:textView];
    self.areaTextView.text = @"";
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(12,SCREEN_HEIGHT -30 - 50, SCREEN_WIDTH - 2 * 12, 50);
    [saveButton setTitle:@"保存" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    saveButton.layer.cornerRadius = 6;
    saveButton.clipsToBounds = YES;
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveButton];
    
}

- (void)selectedButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    sender.selected = !sender.selected;
}

- (void)initData {
    if (self.enterType == 1) {
        self.navBarView.titleLabel.text = @"编辑收货地址";
        self.nameTextfield.text= self.model.name;
        self.phoneTextfield.text = [NSString stringWithFormat:@"%@",self.model.phone];
        self.localArea = self.model.area;
        NSString *areaStr = [self.model.area stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.areaChoosedLabel.text = areaStr;
        self.areaTextView.text = self.model.address;
    }else {
        self.navBarView.titleLabel.text = @"新增收货地址";
        self.navBarView.rightButton.hidden = YES;
        
    }
    
}

- (void)deleteAdress:(UIButton *)sender {
    XPWeakSelf
    [self.xpAlertView configWithTitle:@"确定要删除该地址吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
        
    } rightAction:^(NSString * _Nullable extra) {
        [weakSelf deleteAdress];
    } alertType:XPAlertTypeNormal];
}

- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (![self validVerifyPredicate:@"^[\u4e00-\u9fa5·]{2,10}$" TargetString:self.nameTextfield.text]) {
        [[XPShowTipsTool shareInstance]showMsg:@"请输入正确的姓名" ToView:self.view];
        return;
    }else if (![self validVerifyPredicate:phonePredicate TargetString:self.phoneTextfield.text]) {
        [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的手机号！" ToView:self.view];
        return;
    }else if ([self.areaChoosedLabel.text containsString:@"请选择"]) {
        [[XPShowTipsTool shareInstance]showMsg:@"请选择地区" ToView:self.view];
        return;
    }else if (self.areaTextView.text.length < 6 || self.areaTextView.text.length > 200) {
        [[XPShowTipsTool shareInstance]showMsg:@"请输入正确地址" ToView:self.view];
    }else {
        [self saveData];
    }
    
}


- (void)deleteAdress {
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kUser_adress_delete,self.model.productId];
    
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[NSNotificationCenter defaultCenter] postNotificationName:Notification_user_choose_adress object:self.model];
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:[UIApplication sharedApplication].keyWindow];
            [self goBack];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:[UIApplication sharedApplication].keyWindow];
        }
    }];
}



- (void)saveData {

    NSString *url;
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    if (self.enterType == 1) {//编辑
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kUser_adress_edit];
        paraM[@"id"] = [NSString stringWithFormat:@"%@",self.model.productId];
    }else {
        //添加
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kUser_adress_add];
    }
    
    if (self.enterType == 1) {
        paraM[@"userId"] = _model.userId;
        
    }
    paraM[@"isDefault"] = self.selectButton.selected ? @(1):@(0);
    paraM[@"name"] = self.nameTextfield.text;
    paraM[@"phone"] = self.phoneTextfield.text;
    paraM[@"address"] = self.areaTextView.text;
    paraM[@"area"] = self.localArea;
    
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:[UIApplication sharedApplication].keyWindow];
            [self goBack];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:[UIApplication sharedApplication].keyWindow];
        }
    }];

    
}

//正则
-(BOOL)validVerifyPredicate:(NSString *) pString TargetString:(NSString *)targerString
{
   NSPredicate *num = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pString];
   return [num evaluateWithObject:targerString];
}
- (void)chooseButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self.view endEditing:YES];
    BRAddressPickerView *adressPick = [[BRAddressPickerView alloc] initWithPickerMode:0];
    [adressPick show];
    
    XPWeakSelf
    [adressPick setResultBlock:^(BRProvinceModel * _Nullable province, BRCityModel * _Nullable city, BRAreaModel * _Nullable area) {
        weakSelf.areaStr = [NSString stringWithFormat:@"%@,%@,%@",province.name,city.name,area.name];
        weakSelf.localArea = [NSString stringWithFormat:@"%@,%@,%@",province.name,city.name,area.name];
        weakSelf.areaChoosedLabel.text =  [NSString stringWithFormat:@"%@%@%@",province.name,city.name,area.name];
    }];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
-(void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
