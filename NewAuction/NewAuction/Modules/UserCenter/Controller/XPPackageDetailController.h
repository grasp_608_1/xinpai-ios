//
//  XPPackageDetailController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/2.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPPackageDetailController : XPBaseViewController
//默认选中
@property (nonatomic, assign) NSInteger defaultIndex;
//订单id
@property (nonatomic, copy) NSString *orderId;

@end

NS_ASSUME_NONNULL_END
