//
//  XPUserSettingController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/12.
//

#import "XPUserSettingController.h"
#import "XPLocalStorage.h"
#import "XPUserSettingCell.h"
#import "XPUserVerifyIdentyController.h"
#import "XPUserPaySecrectController.h"
#import "XPUserAddressController.h"
#import "XPBaseWebViewViewController.h"
#import "XPUserInfoUpdateController.h"
#import "XPUserDestoryController.h"
#import "XPUserLevelView.h"
#import "XPPreferenceSettingController.h"

@interface XPUserSettingController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *mainTableView;

@property (nonatomic, strong) UIImageView *userIcon;

@property (nonatomic, strong) UILabel *userNickNameLabel;

@property (nonatomic, strong) UIImageView *topBanner;

@property (nonatomic, strong) XPUserLevelView *userLeveLabel;

@property (nonatomic, copy) NSArray *accountArray;

@property (nonatomic, copy) NSArray *accountDesArray;

@property (nonatomic, copy) NSArray *appInfoArray;

@property (nonatomic, strong) UIButton *logoutButton;

@end

@implementation XPUserSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [self initData];
    [self setupUI];
    
}
- (void)initData {
    NSString *isVerfied = [XPLocalStorage readUserCertified];
    NSString *verifiedDetail = [isVerfied isEqualToString:@"0"] ?@"个人实名":@"已实名";
    self.accountArray = @[@"账户与安全",@"实名认证",@"我的收货地址",@"偏好设置"];
    self.accountDesArray = @[@"设置密码",verifiedDetail,@"管理我的地址",@"我的偏好设置"];
    
    if (APPTYPE == 0) {
        self.appInfoArray = @[@"用户隐私政策",@"用户服务协议",@"拍卖提货须知",@"客服中心",@"退换货政策",@"账号信息安全",@"版本号"];
    }else if (APPTYPE == 1){
        self.appInfoArray = @[@"用户隐私政策",@"用户服务协议",@"客服中心",@"退换货政策",@"账号信息安全",@"版本号"];
    }
    
   
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"设置";
    [self.view addSubview:self.navBarView];
}
- (void)setupUI {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.showsVerticalScrollIndicator = NO;
    self.mainTableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mainTableView.frame = CGRectMake(0,kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -kTopHeight);
    
    self.mainTableView.tableHeaderView = [self setupTableViewHeaderView];
    self.mainTableView.tableFooterView = [self setupTableViewFooter];
    [self.view addSubview:self.mainTableView];
}
- (UIView *)setupTableViewHeaderView {
    UIView *hearderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.mainTableView.frame.size.width, 115)];
    
    UIImageView *mainView = [UIImageView new];
    mainView.frame = CGRectMake(20, 15, hearderView.frame.size.width - 40, 100);
    mainView.layer.cornerRadius = 4;
    mainView.layer.masksToBounds = YES;
    mainView.image = [UIImage imageNamed:@"xp_user_temp2"];
    mainView.userInteractionEnabled = YES;
    self.topBanner = mainView;
    [hearderView addSubview:mainView];
    
    self.userIcon = [UIImageView new];
    self.userIcon.frame = CGRectMake(20, 26, 48, 48);
    self.userIcon.layer.cornerRadius = 24;
    self.userIcon.layer.masksToBounds = YES;
    self.userIcon.layer.borderColor = [UIColor whiteColor].CGColor;
    self.userIcon.layer.borderWidth = 1;
    [mainView addSubview:self.userIcon];
    
    self.userNickNameLabel = [UILabel new];
    self.userNickNameLabel.font = kFontMedium(18);
    self.userNickNameLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.userNickNameLabel.frame = CGRectMake(MaxX(self.userIcon) + 10, self.userIcon.frame.origin.y, 200, 24);
    [mainView addSubview:self.userNickNameLabel];
    
    self.userLeveLabel = [[XPUserLevelView alloc] initWithFrame:CGRectMake(MaxX(self.userIcon) + 10, MaxY(self.userNickNameLabel) + 5, 96, 18)];
    [mainView addSubview:self.userLeveLabel];
    
    UIButton *settingButton = [[UIButton alloc] init];
    [settingButton setImage:[UIImage imageNamed:@"setting_set_log"] forState:UIControlStateNormal];
    settingButton.frame = CGRectMake(mainView.bounds.size.width - 20 - 24 , 38, 24, 24);
//    [settingButton addTarget:self action:@selector(settingButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [settingButton addTarget:self action:@selector(settingButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:settingButton];
    
    NSString *iconUrlStr = self.userModel.head.length > 0 ? self.userModel.head: [XPLocalStorage readUserIconUrl];
    
    self.topBanner.image = [XPUserDataTool getUserSettingBackgroundImage:self.userModel.userType.integerValue];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:iconUrlStr] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    self.userNickNameLabel.text = self.userModel.name.length > 0 ? self.userModel.nickname : [XPLocalStorage readUserNickName];
    
    return hearderView;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self refreshHeader];
}

- (void)refreshHeader {
    
    self.userLeveLabel.userModel = self.userModel;
    
    NSString *iconUrlStr = self.userModel.head.length > 0 ? self.userModel.head: [XPLocalStorage readUserIconUrl];
    
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:iconUrlStr] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    self.userNickNameLabel.text = self.userModel.nickname.length > 0 ? self.userModel.nickname : [XPLocalStorage readUserNickName];
}

- (void)settingButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPUserInfoUpdateController *vc = [XPUserInfoUpdateController new];
    vc.userModel = self.userModel;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (UIView *)setupTableViewFooter {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.mainTableView.frame.size.width, 40+ 50 + 15 + self.safeBottom)];

    self.logoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.logoutButton setTitle:@"退出登录" forState:UIControlStateNormal];
    [self.logoutButton setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
    self.logoutButton.titleLabel.font = kFontMedium(14);
    [self.logoutButton addTarget:self action:@selector(logOut:) forControlEvents:UIControlEventTouchUpInside];
    self.logoutButton.frame = CGRectMake(20, 40, self.mainTableView.frame.size.width - 20*2, 50);
    self.logoutButton.backgroundColor = UIColor.whiteColor;
    [footerView addSubview:self.logoutButton];
    
    return footerView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.accountArray.count;
    }else {
        
        return self.appInfoArray.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPUserSettingCell *cell = [XPUserSettingCell cellWithTableView:tableView];
    
    
    if (indexPath.section == 0) {
        cell.desLabel.text = self.accountArray[indexPath.row];
        cell.extraLabel.text = self.accountDesArray[indexPath.row];
        cell.rightArrow.hidden = NO;
        if (indexPath.row == 0) {
            [XPCommonTool cornerRadius:cell.mainView andType:1 radius:8];
        }else if (indexPath.row == self.accountArray.count - 1) {
            [XPCommonTool cornerRadius:cell.mainView andType:2 radius:8];
        }else {
            [XPCommonTool cornerRadius:cell.mainView andType:3 radius:0];
        }
        
        if (indexPath.row == self.accountArray.count - 1) {
            cell.downLine.hidden = YES;
        }else {
            cell.downLine.hidden = NO;
        }

    }else {
        cell.desLabel.text = self.appInfoArray[indexPath.row];
        
        if (indexPath.row == 0) {
            [XPCommonTool cornerRadius:cell.mainView andType:1 radius:8];
        }else if (indexPath.row == self.appInfoArray.count - 1) {
            [XPCommonTool cornerRadius:cell.mainView andType:2 radius:8];
        }else {
            [XPCommonTool cornerRadius:cell.mainView andType:3 radius:0];
        }
        
        if (indexPath.row == self.appInfoArray.count - 1) {
            
            NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
            NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
            
            cell.downLine.hidden = YES;
            cell.rightArrow.hidden = YES;
            cell.extraLabel.text = [NSString stringWithFormat:@"v%@",app_Version];
            cell.extraLabel.frame = CGRectMake(cell.mainView.frame.size.width - 100-24, 0, 100, cell.mainView.frame.size.height);
        }else {
            cell.downLine.hidden = NO;
            cell.rightArrow.hidden = NO;
            cell.extraLabel.frame = CGRectMake(cell.mainView.frame.size.width - 100 - 20 - 24 -5, 0, 100, cell.mainView.frame.size.height);
        }

    }
    
    
    
    return cell;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *sectionView = [UIView new];
    sectionView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 15);
    sectionView.backgroundColor = UIColor.clearColor;
    return  sectionView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            XPUserPaySecrectController *vc = [XPUserPaySecrectController new];
            vc.enterType = 2;
            [self.navigationController pushViewController:vc animated:YES];
            
        }else if (indexPath.row == 1){
            
            NSString *isVerfied = [XPLocalStorage readUserCertified];
            if ([isVerfied isEqualToString:@"0"] ) {
                XPUserVerifyIdentyController *vc = [XPUserVerifyIdentyController new];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
        //此版本暂时关闭支付密码功能
//        else if (indexPath.row == 2){
//            XPUserPaySecrectController *vc = [XPUserPaySecrectController new];
//            vc.enterType = 3;
//            [self.navigationController pushViewController:vc animated:YES];
//        }
        else if (indexPath.row == 2){
            XPUserAddressController *vc = [XPUserAddressController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 3){
            XPPreferenceSettingController *vc = [XPPreferenceSettingController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else {
        
        if (APPTYPE == 0) {
            if (indexPath.row == 0) {
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,kUser_Policy];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            }else if (indexPath.row == 1){
                
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,kUser_Agreement];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else if (indexPath.row == 2){
                
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,kUser_Aution_goods];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else if (indexPath.row == 3){
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,k_user_help];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else if (indexPath.row == 4){
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,k_goodsExchange_policy];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else if (indexPath.row == 5){
                XPUserDestoryController *vc = [XPUserDestoryController new];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else if(APPTYPE == 1){
            if (indexPath.row == 0) {
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,kUser_Policy];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            }else if (indexPath.row == 1){
                
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,kUser_Agreement];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else if (indexPath.row == 2){
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,k_user_help];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else if (indexPath.row == 3){
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,k_goodsExchange_policy];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else if (indexPath.row == 4){
                XPUserDestoryController *vc = [XPUserDestoryController new];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    }
    
}

- (void)freshData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Get_fav_list];
       NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
       
       [self startLoadingGif];
       [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
           [self stopLoadingGif];
           if (data.isSucess) {
               
           }else {
               
           }
       }];
}


-(void)logOut:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPWeakSelf
    
    [self.xpAlertView configWithTitle:@"您确定要退出登录吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
        
    } rightAction:^(NSString * _Nullable extra) {
        [weakSelf.navigationController popToRootViewControllerAnimated:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:Notification_user_login_out object:nil];
    } alertType:XPAlertTypeNormal];
}

-(void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
