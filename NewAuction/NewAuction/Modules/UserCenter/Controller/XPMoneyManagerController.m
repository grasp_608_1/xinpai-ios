//
//  XPMoneyManagerController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/18.
//

#import "XPMoneyManagerController.h"
#import "XPUserCenterCell.h"
#import "XPMoneyTranferOutController.h"
#import "XPMoneyTranferInController.h"
#import "XPTrendDetailViewController.h"
#import "XPMoneyExchangeController.h"

@interface XPMoneyManagerController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *mainTableView;

//设置标题第一列
@property (nonatomic, copy) NSArray *titleArray1;
//设置iconArray第一列
@property (nonatomic, copy) NSArray *iconArray1;
//设置标题 第二列
@property (nonatomic, copy) NSArray *titleArray2;
//设置iconArray 第二列
@property (nonatomic, copy) NSArray *iconArray2;

@property (nonatomic, copy) NSDictionary *dataDic;

@end

@implementation XPMoneyManagerController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor =  UIColorFromRGB(0xFAFAFA);
    [self setupUI];
    [self initData];
    [self.mainTableView reloadData];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:self.navBarView];
    self.navBarView.backgroundColor = UIColor.clearColor;
    self.navBarView.titleLabel.text = @"账户管理";
    
    [self.navBarView.rightButton setTitle:@"全部账单" forState:UIControlStateNormal];
    [self.navBarView.rightButton setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
    [self.navBarView.rightButton addTarget:self action:@selector(detailButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.rightButton.titleLabel.font = kFontRegular(16);
    self.navBarView.rightButton.frame = CGRectMake(SCREEN_WIDTH - 10 - 120, self.navBarView.mj_h - 44, 120, 44);
    
}

- (void)detailButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPTrendDetailViewController *vc = [XPTrendDetailViewController new];
    vc.enterType = 1;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)initData {
    
    self.titleArray1 = @[@"保证金转出",@"支付保证金"];
    self.iconArray1 = @[@"money_6",@"money_7"];
    self.titleArray2 = @[@"保证金明细",@"提现明细",@"参拍明细",@"委拍明细",@"收入明细",@"补贴明细",@"增值兑换"];
    self.iconArray2 = @[@"money_8",@"money_9",@"money_1",@"money_5",@"money_4",@"money_3",@"money_2"];
    
}

- (void)setupUI {
    
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.showsVerticalScrollIndicator = NO;
    self.mainTableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mainTableView.frame = CGRectMake(0,kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    self.mainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, self.safeBottom)];
    
    
    if (@available(iOS 11.0, *)) {
        self.mainTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self.view addSubview:self.mainTableView];
    
}

- (UIView *)setupTableViewFooter {
    UIView *footer = [UIView new];
    footer.frame = CGRectMake(0, 0, SCREEN_WIDTH, 30);
    return footer;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.titleArray1.count;
    }else {
        return self.titleArray2.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XPUserCenterCell *cell = [XPUserCenterCell cellWithTableView:tableView];
    if (indexPath.section == 0) {
        [cell cellWithtitle:self.titleArray1[indexPath.row] IconName:self.iconArray1[indexPath.row]];
        
        if (indexPath.row == 0) {
            [XPCommonTool cornerRadius:cell.mainView andType:1 radius:8];
        }else if (indexPath.row == self.titleArray1.count - 1) {
            [XPCommonTool cornerRadius:cell.mainView andType:2 radius:8];
        }else {
            [XPCommonTool cornerRadius:cell.mainView andType:3 radius:0];
        }
        
        if (indexPath.row == self.titleArray2.count - 1) {
            cell.downLine.hidden = YES;
        }else {
            cell.downLine.hidden = NO;
        }
        
        
    } else {
        [cell cellWithtitle:self.titleArray2[indexPath.row] IconName:self.iconArray2[indexPath.row]];
        if (indexPath.row == 0) {
            [XPCommonTool cornerRadius:cell.mainView andType:1 radius:8];
        }else if (indexPath.row == self.titleArray2.count - 1) {
            [XPCommonTool cornerRadius:cell.mainView andType:2 radius:8];
        }else {
            [XPCommonTool cornerRadius:cell.mainView andType:3 radius:0];
        }
        
        if (indexPath.row == self.titleArray2.count - 1) {
            cell.downLine.hidden = YES;
        }else {
            cell.downLine.hidden = NO;
        }
    }
    
    return cell;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 58;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 12;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 12);
    return view;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {//转出
            XPMoneyTranferOutController *vc = [XPMoneyTranferOutController new];
            vc.dataDict = self.dataDic;
            [self.navigationController pushViewController:vc animated:YES];
        }else {//转入
            
            XPMoneyTranferInController *vc = [XPMoneyTranferInController new];
            vc.dataDict = self.dataDic;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
        
    }else {//
        if (indexPath.row == 0) {//充值明细
            XPTrendDetailViewController *vc = [XPTrendDetailViewController new];
            vc.enterType = 11;
            [self.navigationController pushViewController:vc animated:YES];
        }if (indexPath.row == 1) {//提现明细
            XPTrendDetailViewController *vc = [XPTrendDetailViewController new];
            vc.enterType = 12;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 2) {//参拍明细
            XPTrendDetailViewController *vc = [XPTrendDetailViewController new];
            vc.enterType = 2;
            [self.navigationController pushViewController:vc animated:YES];
        }else if(indexPath.row == 3) {//委拍明细
            XPTrendDetailViewController *vc = [XPTrendDetailViewController new];
            vc.enterType = 3;
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if(indexPath.row == 4) {//收益明细
            XPTrendDetailViewController *vc = [XPTrendDetailViewController new];
            vc.enterType = 5;
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if(indexPath.row == 5) {//盈利明细
            XPTrendDetailViewController *vc = [XPTrendDetailViewController new];
            vc.enterType = 4;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
        else if(indexPath.row == 6) {
            XPMoneyExchangeController *vc = [XPMoneyExchangeController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
    
}

- (UIView *)setupTableViewHeaderView {
    UIView *topView = [UIView new];
    topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 220);
    
    UIImageView *banner = [UIImageView new];
    banner.layer.cornerRadius = 8;
    banner.layer.masksToBounds = YES;
    banner.frame = CGRectMake(12, 12, SCREEN_WIDTH - 24, 120);
    banner.image = [UIImage imageNamed:@"Mask_group"];
    [topView addSubview:banner];
    
    UILabel *label1 = [UILabel new];
    label1.font = kFontMedium(28);
    label1.textColor = UIColor.whiteColor;
    label1.frame = CGRectMake(0, 36, banner.mj_w, 36);
    label1.textAlignment = NSTextAlignmentCenter;
    [banner addSubview:label1];
    
    NSString *priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.dataDic[@"estimateTotalValue"]]];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
                [attr addAttributes:@{NSFontAttributeName:kFontMedium(12)} range:NSMakeRange(0, 1)];
    
    label1.attributedText = attr;
    
    UILabel *label2 = [UILabel new];
    label2.font = kFontMedium(14);
    label2.textColor = RGBACOLOR(255, 255, 255, 0.8);
    label2.frame = CGRectMake(0, MaxY(label1), banner.mj_w, 24);
    label2.text = @"保证金 + 商品预估价值";
    label2.textAlignment = NSTextAlignmentCenter;
    [banner addSubview:label2];
    
    
    NSArray *temp = @[@"账户保证金",@"商品预估价值",@"个人收入",@"个人收入冻结",@"个人货款余额",@"补贴",@"补贴货款"];
    NSArray *tempIconArray = @[@"xp_money_1",@"xp_money_2",@"xp_money_3",@"xp_money_4",@"xp_money_1",@"xp_money_2",@"xp_money_3"];
    NSArray *valueArray = @[
        [NSString stringWithFormat:@"%@",self.dataDic[@"funds"]],
        [NSString stringWithFormat:@"%@",self.dataDic[@"estimate"]],
        [NSString stringWithFormat:@"%@",self.dataDic[@"profit"]],
        [NSString stringWithFormat:@"%@",self.dataDic[@"profitFreeze"]],
        [NSString stringWithFormat:@"%@",self.dataDic[@"profitProduct"]],
        [NSString stringWithFormat:@"%@",self.dataDic[@"earnings"]],
        [NSString stringWithFormat:@"%@",self.dataDic[@"earningsProduct"]],
    ];
    
    CGFloat imageW = 140;
    CGFloat imageH = 76;
    
    UIScrollView *scrollView = [UIScrollView new];
    scrollView.frame = CGRectMake(0, MaxY(banner) + 12,SCREEN_WIDTH, imageH);
    scrollView.contentSize = CGSizeMake((140 + 12)* temp.count + 12, imageH);
    scrollView.showsHorizontalScrollIndicator = NO;
    [topView addSubview:scrollView];
    
    for (int i = 0; i<temp.count; i++) {
        
        UIImageView *imageView = [UIImageView new];
        imageView.layer.cornerRadius = 8;
        imageView.layer.masksToBounds = YES;
        imageView.frame = CGRectMake(i * (140 + 12) + 12,0 , imageW, imageH);
        imageView.image = [UIImage imageNamed:tempIconArray[i]];
        [scrollView addSubview:imageView];
        
        UILabel *label3 = [UILabel new];
        label3.font = kFontRegular(14);
        label3.textColor = UIColorFromRGB(0x3A3C41);
        label3.frame = CGRectMake(12, 15, imageView.mj_w - 12, 16);
        [imageView addSubview:label3];
        
        UILabel *label4 = [UILabel new];
        label4.font = kFontMedium(18);
        label4.textColor = UIColorFromRGB(0x3A3C41);
        label4.frame = CGRectMake(12, 37, imageView.mj_w - 12, 24);
        [imageView addSubview:label4];
        
        label3.text = temp[i];
        
        NSString *valueStr = valueArray[i];
        NSString *priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:[valueStr floatValue]]]];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(12)} range:NSMakeRange(0, 1)];
        label4.attributedText = attr;
        
    }
    
    
    
    return topView;
}


- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}
 
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    [self getData];
}

- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_money_Info];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            self.dataDic = data.userData;
            self.mainTableView.tableHeaderView = [self setupTableViewHeaderView];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
@end
