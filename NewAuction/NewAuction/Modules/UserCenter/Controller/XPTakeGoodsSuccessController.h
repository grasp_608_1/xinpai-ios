//
//  XPTakeGoodsSuccessController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/21.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPTakeGoodsSuccessController : XPBaseViewController
@property (nonatomic, strong) NSNumber *goodsNum;
@end

NS_ASSUME_NONNULL_END
