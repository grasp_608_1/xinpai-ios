//
//  XPMyFavoriteViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/12.
//

#import "XPMyFavoriteViewController.h"
#import "XPProductSortCell.h"
#import "XPProductModel.h"
#import "XPEmptyView.h"
#import "XPProductDetailController.h"
#import "XPSelectItemView.h"
#import "XPShopGoodsDetailController.h"

@interface XPMyFavoriteViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) XPEmptyView *emptyView;
//tab
@property (nonatomic, strong) XPSelectItemView *selectItemView;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//当前页码
@property (nonatomic, assign) NSInteger page;
//当前选中栏
@property (nonatomic, assign) NSInteger currentIndex;

@end

@implementation XPMyFavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    
    self.dataArray = [NSMutableArray array];
    self.page = 1;
    //默认选中第一个
    self.currentIndex = 1;
    
    [self setupUI];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.page = 1;
    [self getData];
}


- (void)setupUI {
    
    XPSelectItemView *selectItemView = [[XPSelectItemView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, 40)];
    self.selectItemView = selectItemView;
    [self.view addSubview:selectItemView];
    [self.selectItemView configWithtitleArray:@[@"拍品收藏",@"商品收藏"]];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, MaxY(selectItemView), SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    self.tableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_empty_favorite"] Description:@"您还没有任何收藏"];
    [self.view addSubview:self.emptyView];
    
    XPWeakSelf
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf headerRefresh];
    }];
    
    [selectItemView setTapBlock:^(NSInteger index) {
        //网络请求
        weakSelf.currentIndex = index;
        weakSelf.page = 1;
        [weakSelf.dataArray removeAllObjects];
        [weakSelf.tableView reloadData];
        [weakSelf getData];
    }];
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"我的收藏";
    [self.view addSubview:self.navBarView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPProductSortCell *cell = [XPProductSortCell cellWithTableView:tableView];
    cell.cellType = 2;
    XPProductModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model passedTime:0];
    
    XPWeakSelf
    [cell setTapBlock:^{
        if (self.currentIndex == 1) {
            [weakSelf deleteFavByID:model.auctionItemsId indexPath:indexPath];
        }else{
            [weakSelf deleteFavByID:model.productSkuId indexPath:indexPath];
        }
        
    }];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    XPProductModel *model = self.dataArray[indexPath.row];
    if (self.currentIndex == 1) {
        XPProductDetailController *vc = [XPProductDetailController new];
        vc.auctionItemId = model.auctionItemsId;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
        vc.productSkuId = model.productSkuId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)deleteFavByID:(NSNumber *)favid indexPath:(NSIndexPath *)indexPath{
    NSString *url = [NSString stringWithFormat:@"%@%@",[self getDeletetPath],favid];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [self.dataArray removeObjectAtIndex:indexPath.row];
            [self.tableView reloadData];
            if (self.dataArray.count == 0) {
                [self canShowEmptyView];
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)getData {
    NSString *url = [self getCollectedListPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    if (self.currentIndex == 1) {
        paraM[@"type"] = @(0);
    }else if (self.currentIndex == 2){
        paraM[@"type"] = @(1);
    }
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPProductModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)headerRefresh{
    NSString *url = [self getCollectedListPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    if (self.currentIndex == 1) {
        paraM[@"type"] = @(0);
    }else if (self.currentIndex == 2){
        paraM[@"type"] = @(1);
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPProductModel mj_objectArrayWithKeyValuesArray:dataArray];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:modelArray];
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *url = [self getCollectedListPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    if (self.currentIndex == 1) {
        paraM[@"type"] = @(0);
    }else if (self.currentIndex == 2){
        paraM[@"type"] = @(1);
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPProductModel mj_objectArrayWithKeyValuesArray:dataArray];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self.tableView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

//获取列表数据
- (NSString *)getCollectedListPath {
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Get_fav_list];
    return urlStr;
}

//删除地址

//获取当前选中页请求地址
- (NSString *)getDeletetPath {
    NSString *urlStr;
    if (self.currentIndex == 1) {//删除拍品收藏
       urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Get_fav_delete];
    }else if(self.currentIndex == 2){//删除商品收藏
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Get_shopFav_delete];
    }
    return urlStr;
}

- (void)canShowEmptyView {
    if (self.dataArray.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}
@end
