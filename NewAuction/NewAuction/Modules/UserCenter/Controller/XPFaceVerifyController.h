//
//  XPFaceVerifyController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/20.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPFaceVerifyController : XPBaseViewController

@property (nonatomic, copy) NSDictionary *extraDic;

@property (nonatomic, strong) UIImage *frontPhoto;

@end

NS_ASSUME_NONNULL_END
