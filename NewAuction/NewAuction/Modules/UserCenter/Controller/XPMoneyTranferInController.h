//
//  XPMoneyTranferInController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/18.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPMoneyTranferInController : XPBaseViewController
@property (nonatomic, copy) NSDictionary *dataDict;
@end

NS_ASSUME_NONNULL_END
