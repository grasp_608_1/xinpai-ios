//
//  XPUserInfoUpdateController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/13.
//

#import "XPBaseViewController.h"
#import "XPUserInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPUserInfoUpdateController : XPBaseViewController
@property (nonatomic, strong) XPUserInfoModel *userModel;
@end

NS_ASSUME_NONNULL_END
