//
//  XPMoneyExchangeController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/20.
//

#import "XPMoneyExchangeController.h"
#import "XPSelectItemView.h"
#import "XPHitButton.h"
#import "XPTrendDetailViewController.h"
#import "XPHitButton.h"


@interface XPMoneyExchangeController ()<UITextFieldDelegate>

@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, strong) UILabel *totalLabel;

@property (nonatomic, strong) XPHitButton *rightButton;

@property (nonatomic, strong) XPHitButton *allButton;

@property (nonatomic, strong) UILabel *percentLabel;

@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, strong) UILabel *avaliableLabel;
//冻结金额描述
@property (nonatomic, strong) UILabel *frozenDesLabel;
//冻结金额
@property (nonatomic, strong) UILabel *frozenLabel;

@property (nonatomic, strong) UILabel *titleDesLabel;

@property (nonatomic, strong) UIView *downView;

@property (nonatomic, strong)  NSNumber *availableNumber;
@property (nonatomic, strong)  NSNumber *totalNumber;

@end

@implementation XPMoneyExchangeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.currentIndex = 1;
    [self setupUI];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    self.navBarView.backgroundColor = UIColor.whiteColor;
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"增值兑换";
    [self.view addSubview:self.navBarView];
}

- (void)setupUI {
    
    XPSelectItemView *selectItemView = [[XPSelectItemView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, 40)];
    [selectItemView configWithtitleArray:@[@"收入兑换",@"补贴兑换"]];
    [self.view addSubview:selectItemView];
    XPWeakSelf
    [selectItemView setTapBlock:^(NSInteger index) {
        //网络请求
        weakSelf.currentIndex = index;
        if (index == 1) {
            weakSelf.titleDesLabel.text = @"收入总金额";
            weakSelf.frozenLabel.hidden = NO;
            weakSelf.frozenDesLabel.hidden = NO;
            [weakSelf getData];
            self.textField.text = @"";
            weakSelf.downView.hidden = NO;
        }else {
            weakSelf.titleDesLabel.text = @"补贴总金额";
            weakSelf.frozenLabel.hidden = YES;
            weakSelf.frozenDesLabel.hidden = YES;
            [weakSelf getData];
            weakSelf.downView.hidden = YES;
            self.textField.text = @"";
        }
        
        
    }];
    
    UIView *topView = [UIView new];
    topView.backgroundColor = UIColorFromRGB(0x6E6BFF);
    topView.layer.cornerRadius = 8;
    topView.layer.masksToBounds = YES;
    topView.frame = CGRectMake(12, MaxY(selectItemView) + 12, SCREEN_WIDTH - 24, 96);
    [self.view addSubview:topView];
    
    UILabel *label1 = [UILabel new];
    label1.font = kFontRegular(14);
    label1.textColor = UIColor.whiteColor;
    label1.frame = CGRectMake(24, 12, 200, 24);
    label1.text = @"收入总金额";
    self.titleDesLabel = label1;
    [topView addSubview:label1];
    
    UILabel *label2 = [UILabel new];
    label2.font = kFontMedium(28);
    label2.textColor = UIColor.whiteColor;
    label2.frame = CGRectMake(24, MaxY(label1) +12, topView.mj_w - 24, 36);
    label2.text = @"0.00";
    self.totalLabel  = label2;
    [topView addSubview:label2];
    
    self.rightButton = [[XPHitButton alloc] init];
    self.rightButton.hitRect = 40;
    self.rightButton.frame = CGRectMake(topView.mj_w - 24 -30, 12, 30, 24);
    [self.rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [self.rightButton setTitle:@"明细" forState:UIControlStateNormal];
    self.rightButton.titleLabel.font = kFontRegular(14);
    [self.rightButton addTarget:self action:@selector(detailButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:self.rightButton];
    
    UIView *midView = [UIView new];
    midView.backgroundColor = UIColor.whiteColor;
    midView.frame = CGRectMake(12, MaxY(topView) + 12, SCREEN_WIDTH -24, 120);
    midView.layer.cornerRadius = 8;
    [self.view addSubview:midView];
    
    UILabel *label5 = [UILabel new];
    label5.font = kFontRegular(14);
    label5.textColor = UIColorFromRGB(0x3A3C41);
    label5.frame = CGRectMake(24, 12, 60, 24);
    label5.text = @"转出金额";
    [midView addSubview:label5];
    
    UILabel *label6 = [UILabel new];
    label6.font = kFontMedium(24);
    label6.textColor = UIColorFromRGB(0x000000);
    label6.frame = CGRectMake(24, 64, 24, 24);
    label6.text = @"￥";
    [midView addSubview:label6];
    
    UILabel *label7 = [UILabel new];
    label7.font = kFontRegular(14);
    label7.textColor = UIColorFromRGB(0xB8B8B8);
    label7.frame = CGRectMake(0, 12, midView.mj_w - 12, 24);
    label7.textAlignment = NSTextAlignmentRight;
    label7.text = @"";
    self.percentLabel = label7;
    [midView addSubview:label7];
    
    XPHitButton *button3 = [[XPHitButton alloc] init];
    button3.hitRect = 40;
    [button3 addTarget:self action:@selector(allButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button3 setTitle:@"全部" forState:UIControlStateNormal];
    [button3 setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    button3.titleLabel.font = kFontRegular(16);
    button3.frame = CGRectMake(midView.mj_w - 32 - 24, 66, 32, 24);
    self.allButton = button3;
    [midView addSubview:button3];
    
    UITextField *textField = [[UITextField alloc] init];
    textField.font = kFontMedium(32);
    textField.textColor = UIColor.blackColor;
    textField.keyboardType = UIKeyboardTypeDecimalPad;
    textField.delegate = self;
    self.textField = textField;
    textField.frame = CGRectMake(MaxX(label6), 56, midView.mj_w - 32 - 24 - MaxX(label6), 40);
    [midView addSubview:textField];
    
    
    UIView *downView = [UIView new];
    downView.backgroundColor = UIColor.whiteColor;
    downView.layer.cornerRadius = 8;
    downView.layer.masksToBounds = YES;
    downView.frame = CGRectMake(12, 12 + MaxY(midView), SCREEN_WIDTH - 24, 96);
    self.downView = downView;
    [self.view addSubview:downView];
    
    UILabel *label11 = [UILabel new];
    label11.font = kFontRegular(14);
    label11.textColor = UIColorFromRGB(0x8A8A8A);
    label11.frame = CGRectMake(24, 24, 60, 16);
    label11.text = @"可用金额";
    [downView addSubview:label11];
    
    UILabel *label12 = [UILabel new];
    label12.font = kFontMedium(18);
    label12.textColor = UIColorFromRGB(0x8A8A8A);
    label12.frame = CGRectMake(24, MaxY(label11) + 6, downView.mj_w/2 - 24, 20);
    label12.text = @"0.00";
    self.avaliableLabel  = label12;
    [downView addSubview:label12];
    
    
    UILabel *label13 = [UILabel new];
    label13.font = kFontRegular(14);
    label13.textColor = UIColorFromRGB(0x8A8A8A);
    label13.frame = CGRectMake(downView.mj_w/2.+ 24, 24, 60, 16);
    label13.text = @"冻结金额";
    self.frozenDesLabel = label13;
    [downView addSubview:label13];
    
    XPHitButton *tipsButton = [[XPHitButton alloc] init];
    [tipsButton setImage:[UIImage imageNamed:@"xp_need_help"] forState:UIControlStateNormal];
    [tipsButton addTarget:self action:@selector(tipButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    tipsButton.frame = CGRectMake(MaxX(label13) + 6, 24, 16, 16);
    tipsButton.hitRect = 40;
    [downView addSubview:tipsButton];
    
    UILabel *label14 = [UILabel new];
    label14.font = kFontMedium(18);
    label14.textColor = UIColorFromRGB(0x8A8A8A);
    label14.frame = CGRectMake(downView.mj_w/2.+ 24, MaxY(label11) + 6, downView.mj_w/2 - 24, 20);
    label14.text = @"0.00";
    self.frozenLabel  = label14;
    [downView addSubview:label14];
    
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(12,self.view.mj_h - 80, SCREEN_WIDTH - 2 * 12, 50);
    [saveButton setTitle:@"确认兑换" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    saveButton.layer.cornerRadius = 25;
    saveButton.clipsToBounds = YES;
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveButton];
    
}
- (void)allButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.currentIndex == 1) {
        self.textField.text = [XPMoneyFormatTool formatMoneyWithNum:self.availableNumber];
    }else {
        self.textField.text = [XPMoneyFormatTool formatMoneyWithNum:self.totalNumber];
    }
}

- (void)detailButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPTrendDetailViewController *vc = [XPTrendDetailViewController new];
    vc.enterType = self.currentIndex + 5;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.currentIndex == 1) {
        
        if (self.textField.text.floatValue > self.availableNumber.floatValue) {
            [[XPShowTipsTool shareInstance] showMsg:@"输入金额不可大于可用金额" ToView:self.view];
            return;
        }
        if (self.textField.text.length == 0 || self.textField.text.floatValue == 0) {
            [[XPShowTipsTool shareInstance] showMsg:@"请输入金额" ToView:self.view];
            return;
        }
        
    }else {
        if (self.textField.text.length == 0 || self.textField.text.floatValue == 0) {
            [[XPShowTipsTool shareInstance] showMsg:@"请输入金额" ToView:self.view];
            return;
        }
        
        if (self.textField.text.floatValue > self.totalNumber.floatValue) {
            [[XPShowTipsTool shareInstance] showMsg:@"输入金额不可大于总金额" ToView:self.view];
            return;
        }
    }
    
    [self saveData];
    
}

- (void)tipButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self.xpAlertView showAlertWithTitle:@"冻结资金是委拍后的利润结算，需提货才可解冻" actionBlock:^(NSString * _Nullable extra) {
        
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getData];
}

- (void)getData {
    
    NSString *url = [self getPath];
     NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         self.totalNumber = @(0);
         if (data.isSucess) {
             
             if (self.currentIndex == 1) {
                 self.availableNumber = data.userData[@"availableAmount"]? data.userData[@"availableAmount"]:@(0);
                 
                 NSString *avaliabelStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.availableNumber]];
                        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:avaliabelStr];
                            [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(0, 1)];
                 
                 self.avaliableLabel.attributedText = attr;
                 
                 NSNumber *frozenNumber = data.userData[@"frozenAmount"] ?:@(0);
                 NSString *frozenStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:frozenNumber]];
                        NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc] initWithString:frozenStr];
                            [attr2 addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(0, 1)];
                 
                 self.frozenLabel.attributedText = attr2;
             }
             
             self.totalNumber = data.userData[@"totalAmount"];
             NSString *priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.totalNumber]];
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
                        [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(0, 1)];
             
             self.totalLabel.attributedText = attr;
             
             NSNumber *percent = data.userData[@"transactionFee"];
             self.percentLabel.text = [NSString stringWithFormat:@"兑换时需要额外扣除%@%%手续费",[XPMoneyFormatTool  formatMoneyWithNum:[NSNumber numberWithFloat:percent.floatValue * 100]]];
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
}
- (void)saveData{
    
    NSString *url = [self getExchangePath];
     NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    CGFloat num = [self.textField.text floatValue];
    paraM[@"money"] = [NSString stringWithFormat:@"%.2f",num];
    
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             self.textField.text= @"";
             [self getData];
             
         }
         [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
     }];
}

- (NSString *)getPath {
    if (self.currentIndex == 1) {//盈利兑换
        return [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_monney_income_info];
    }else {
        return [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_monney_profit_info];
    }
}
- (NSString *)getExchangePath {
    if (self.currentIndex == 1) {//盈利兑换
        return [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_monney_profit_exchange];
    }else {
        return [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_monney_income_exchange];
    }
    
}

#pragma mark --- textfield Delegate
//限制只能输入金额
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //限制.后面最多有两位，且不能再输入.
    if ([textField.text rangeOfString:@"."].location != NSNotFound) {
        //有.了 且.后面输入了两位  停止输入
        if (toBeString.length > [toBeString rangeOfString:@"."].location+3) {
            return NO;
        }
        //有.了，不允许再输入.
        if ([string isEqualToString:@"."]) {
            return NO;
        }
    }
    
    //限制首位0，后面只能输入. 或 删除
    if ([textField.text isEqualToString:@"0"]) {
        if (!([string isEqualToString:@"."] || [string isEqualToString:@""])) {
            return NO;
        }
    }
    
    //首位. 前面补全0
    if ([toBeString isEqualToString:@"."]) {
        textField.text = @"0";
        return YES;
    }
    
    //限制只能输入：1234567890.
    NSCharacterSet * characterSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890."] invertedSet];
    NSString * filtered = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}


@end
