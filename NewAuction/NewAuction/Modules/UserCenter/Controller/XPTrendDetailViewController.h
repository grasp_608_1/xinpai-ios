//
//  XPTrendDetailViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/18.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPTrendDetailViewController : XPBaseViewController
////1全部账单     2参拍明细       3委拍明细     4收益明细  5盈利明细
///6盈利兑换明细   7收益兑换明细 8货款转出明细
///9支付货款明细  10我的积分  11充值明细
/// 12 提现明细 13 佣金提现明细
@property (nonatomic, assign) NSInteger enterType;

@end

NS_ASSUME_NONNULL_END
