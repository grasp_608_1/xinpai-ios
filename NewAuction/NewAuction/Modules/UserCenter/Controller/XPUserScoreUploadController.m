//
//  XPUserScoreUploadController.m
//  NewAuction
//
//  Created by Grasp_L on 2025/3/3.
//

#import "XPUserScoreUploadController.h"
#import "XPOSSUploadTool.h"
#import "UIImage+Compression.h"

@interface XPUserScoreUploadController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
@property (nonatomic, strong) UIScrollView *mainView;
@property (nonatomic, strong) UIButton *frontButton;
@property (nonatomic, strong) UIImageView *frontImageView;
@property (nonatomic, strong) UITextField *textfield;
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UIImage *frontPhoto;
@property (nonatomic, copy) NSString *uploadUrl;
@end

@implementation XPUserScoreUploadController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupUI];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"上传资料";
    [self.view addSubview:self.navBarView];
    self.navBarView.backgroundColor = UIColorFromRGB(0xF2F2F2);
}

- (void)setupUI {
    UIScrollView *mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight)];
    mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.mainView = mainView;
    [self.view addSubview:mainView];
    
    UIView *topView = [UIView new];
    topView.backgroundColor = UIColor.whiteColor;
    topView.layer.cornerRadius = 8;
    topView.frame = CGRectMake(12, 12, self.view.mj_w - 24, 258);
    [mainView addSubview:topView];
    
    UILabel *deslabel = [UILabel new];
    deslabel.frame = CGRectMake(24, 12, 300, 24);
    deslabel.font = kFontRegular(14);
    deslabel.textColor = UIColorFromRGB(0x3A3C41);
    deslabel.text = @"上传KEPP步数截图，参考请看活动详情";
    [topView addSubview:deslabel];

    CGFloat cardW = topView.mj_w - 24 *2;
    CGFloat cardH = cardW * 186/303.;
    UIView *bgView1 = [UIView new];
//    bgView1.backgroundColor =UIColorFromRGB(0xDEDEDE);
    bgView1.layer.cornerRadius = 4;
    bgView1.frame = CGRectMake(24, 48, cardW, cardH);
    [topView addSubview:bgView1];
    
    self.frontImageView = [[UIImageView alloc] initWithFrame:bgView1.bounds];
    self.frontImageView.backgroundColor = UIColorFromRGB(0xFFDFBE);
    [bgView1 addSubview:self.frontImageView];
    
    UIView *circleBgView = [UIView new];
    circleBgView.layer.cornerRadius = 4;
    circleBgView.layer.borderColor = UIColorFromRGB(0xFFB265).CGColor;
    circleBgView.layer.borderWidth = 2;
//    circleBgView.backgroundColor = UIColor.clearColor;
    circleBgView.frame = CGRectMake(8, 8, self.frontImageView.mj_w - 16, self.frontImageView.mj_h - 16);
    [self.frontImageView addSubview:circleBgView];
    
    //身份证正面
    UIButton *frontBtn = [UIButton new];
    frontBtn.frame = bgView1.bounds;
    self.frontButton = frontBtn;
    [frontBtn addTarget:self action:@selector(frontButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bgView1 addSubview:frontBtn];
    
    
    UILabel *fLabel = [UILabel new];
    fLabel.frame = CGRectMake(0, frontBtn.center.y + 5, frontBtn.mj_w, 20);
    fLabel.text = @"上传图片";
    fLabel.textColor = UIColorFromRGB(0x3A3C41);
    fLabel.font = kFontRegular(14);
    fLabel.textAlignment = NSTextAlignmentCenter;
    [self.frontImageView addSubview:fLabel];
    
    UIImageView *fImageView = [UIImageView new];
    fImageView.frame = CGRectMake(frontBtn.mj_w/2. - 10, frontBtn.center.y - 5 - 20, 20, 20);
    fImageView.image = [UIImage imageNamed:@"xp_camera"];
    [self.frontImageView addSubview:fImageView];
    
    topView.frame = CGRectMake(12, 12, self.view.mj_w - 24, MaxY(bgView1) + 24);
    
    UIView *connectView = [[UIView alloc] initWithFrame:CGRectMake(12, MaxY(topView) + 12, SCREEN_WIDTH - 24, 315)];
    connectView.layer.cornerRadius = 8;
    connectView.backgroundColor = UIColor.whiteColor;
    [mainView addSubview:connectView];
    
    UILabel *connectDescLabel = [UILabel new];
    connectDescLabel.font = kFontRegular(14);
    connectDescLabel.textColor = app_font_color;
    connectDescLabel.frame = CGRectMake(16, 12, 64, 40);
    connectDescLabel.text = @"公里数";
    [connectView addSubview:connectDescLabel];
    
    self.textfield = [[UITextField alloc] init];
    self.textfield.font = kFontRegular(14);
    self.textfield.textColor = app_font_color;
    self.textfield.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.textfield.layer.cornerRadius = 4;
    self.textfield.placeholder = @" 请输入当天公里数";
    self.textfield.delegate = self;
    self.textfield.keyboardType  = UIKeyboardTypeNumberPad;
    self.textfield.frame = CGRectMake(98, 8, connectView.mj_w - 98 - 12 , 40);
    self.textfield.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, self.textfield.mj_h)];
    self.textfield.leftViewMode = UITextFieldViewModeAlways;
    [connectView addSubview:self.textfield];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(connectView.mj_w / 2 - 90, 62 + MaxY(self.textfield),180 , 50);
    rightButton.layer.cornerRadius = 25;
    rightButton.backgroundColor = UIColorFromRGB(0xF2F2F2);
    rightButton.clipsToBounds = YES;
    [rightButton setTitleColor:UIColorFromRGB(0xB8B8B8) forState:UIControlStateNormal];
    [rightButton setTitleColor:app_font_color forState:UIControlStateSelected];
    rightButton.titleLabel.font = kFontMedium(16);
    [rightButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setTitle:@"提交" forState:UIControlStateNormal];
    self.rightButton = rightButton;
    [connectView addSubview:rightButton];
    
    
    UITextView *textView = [[UITextView alloc] init];
    textView.userInteractionEnabled = NO;
    textView.font = kFontRegular(13);
    textView.text = @"兑换规则:\n1、本活动仅支持户外跑步运动的公里数进行兑换\n2、上传截图需为当天的KEEP户外跑步截图\n3、截图公里数要与填写公里数相同，否则取两者最小数兑换积分\n4、兑换比例为1公里=10积分";
    textView.frame = CGRectMake(12, MaxY(rightButton) + 20, connectView.mj_w - 24, 130);
    [connectView addSubview:textView];
   
}


-(void)frontButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self showAlertWithMsg:@"上传图片"];
}

-(void)bottomButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.uploadUrl.length == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"请上传图片" ToView:k_keyWindow];
    }else if (self.textfield.text.intValue == 0){
        [[XPShowTipsTool shareInstance] showMsg:@"请输入公里数" ToView:k_keyWindow];
    }else {
        [self uploadInfo];
    }
}

//上传图片
-(void)showAlertWithMsg:(NSString *)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       
    }];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openCamera];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openAlbum];
    }];
    [alert addAction:cancel];
    [alert addAction:action];
    [alert addAction:action2];

    [self presentViewController:alert animated:YES completion:nil];
}
- (void)openCamera {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}
- (void)openAlbum {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.navigationBar.translucent = NO;
    [self presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:true completion:^{
        
    }];
    self.frontPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSString *type  = [info objectForKey:UIImagePickerControllerMediaType];
    //选择的类型图片
    if ([type isEqualToString:@"public.image"]) {
        //先把图片转成NSData
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        [self uploadCardWithImage:image];
        //调整UI
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
  
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark --- textfield Delegate
//限制只能输入金额
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //限制.后面最多有两位，且不能再输入.
    if ([textField.text rangeOfString:@"."].location != NSNotFound) {
        //有.了 且.后面输入了两位  停止输入
        if (toBeString.length > [toBeString rangeOfString:@"."].location+3) {
            return NO;
        }
        //有.了，不允许再输入.
        if ([string isEqualToString:@"."]) {
            return NO;
        }
    }
    
    //限制首位0，后面只能输入. 或 删除
    if ([textField.text isEqualToString:@"0"]) {
        if (!([string isEqualToString:@"."] || [string isEqualToString:@""])) {
            return NO;
        }
    }
    
    //首位. 前面补全0
    if ([toBeString isEqualToString:@"."]) {
        textField.text = @"0";
        return YES;
    }
    //限制只能输入：1234567890.
    NSCharacterSet * characterSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890."] invertedSet];
    NSString * filtered = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.textfield) {
        [self check];
    }
}

- (void)uploadCardWithImage:(UIImage *)iConImage {
    
    iConImage = [iConImage compressToImage];
    NSString *filePath = [[XPOSSUploadTool shareInstance] saveImageToSandbox:iConImage];
    [self startLoadingGif];
    [[XPOSSUploadTool shareInstance] uploadFileWithMediaType:XPMediaTypeScoreImage filePath:filePath progress:^(CGFloat progress) {
        
    } completeBlock:^(BOOL success, NSString * _Nonnull remoteUrlStr) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self stopLoadingGif];
            
            if (success) {
                self.uploadUrl = remoteUrlStr;
                self.frontImageView.hidden = YES;
                [self.frontButton setImage:self.frontPhoto forState:UIControlStateNormal];
                
                [self check];
                
            }else{
                [[XPShowTipsTool shareInstance] showMsg:@"上传失败,请稍后重试!" ToView:k_keyWindow];
            }
        });
    }];
}

-(void)check{
    if (self.uploadUrl.length > 0 && self.textfield.text.intValue > 0) {
        [self.rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        self.rightButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
        
    }else {
        [self.rightButton setTitleColor:UIColorFromRGB(0xB8B8B8) forState:UIControlStateNormal];
        self.rightButton.backgroundColor = UIColorFromRGB(0xF2F2F2);
    }
}

- (void)uploadInfo {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kUser_score_submit];
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pic"] = self.uploadUrl;
    paraM[@"pointsExpect"] = self.textfield.text;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            //上传成功，请耐心等待审核，审核通过后于【我的】-【积分】查看积分情况
            XPWeakSelf
            [self.xpAlertView showAlertWithTitle:@"上传成功，请耐心等待审核，审核通过后于【我的】-【我的积分】查看积分情况" actionBlock:^(NSString * _Nullable extra) {
                [weakSelf goBack];
            }];
            
        }else{
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
       
    }];
}

@end
