//
//  XPMyDelegateViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/12.
//

#import "XPMyDelegateViewController.h"
#import "XPSelectItemView.h"
#import "XPAuctionModel.h"
#import "XPProductCell.h"
#import "XPEmptyView.h"


@interface XPMyDelegateViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, strong) XPEmptyView *emptyView1;
@property (nonatomic, strong) XPEmptyView *emptyView2;

//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//当前页码
@property (nonatomic, assign) NSInteger page;



@end

@implementation XPMyDelegateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
//默认选中第一个
    self.currentIndex = 1;
    self.page = 1;
    self.dataArray = [NSMutableArray array];
    [self setupUI];
    [self getListData];
}
- (void)setupUI {
    
    XPSelectItemView *selectItemView = [[XPSelectItemView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, 40)];
    [selectItemView configWithtitleArray:@[@"参拍商品",@"委拍商品"]];
    [self.view addSubview:selectItemView];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, MaxY(selectItemView), SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - 40);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:self.tableView];
    
    XPWeakSelf
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf headRefresh];
    }];
    
    [selectItemView setTapBlock:^(NSInteger index) {
        //网络请求
        weakSelf.currentIndex = index;
        weakSelf.page = 1;
        [weakSelf.dataArray removeAllObjects];
        [weakSelf getListData];
    }];
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    
    self.emptyView1 = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView1.hidden = YES;
    self.tableView.hidden = NO;
    [self.emptyView1 configWithImage:[UIImage imageNamed:@"xp_empty_delegate"] Description:@"您还没有参拍商品"];
    [self.view addSubview:self.emptyView1];
    self.emptyView2 = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView2.hidden = YES;
    [self.emptyView2 configWithImage:[UIImage imageNamed:@"xp_empty_delegate"] Description:@"您还没有可委拍商品"];
    [self.view addSubview:self.emptyView2];
    
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"委托列表";
    [self.view addSubview:self.navBarView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPProductCell *cell = [XPProductCell cellWithTableView:tableView];
    if (self.currentIndex == 1) {
        cell.cellType = 1;
    }else if (self.currentIndex == 2){
        cell.cellType = 2;
    }
    XPAuctionModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    
    XPWeakSelf
    [cell setRightBlock:^(NSString * _Nonnull extra) {
       
        [weakSelf showAlertWithModel:model];
        
    }];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

- (void)showAlertWithModel:(XPAuctionModel *)model {
    XPWeakSelf
    [self.xpAlertView configWithTitle:@"确定要取消吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
        
    } rightAction:^(NSString * _Nullable extra) {
        [weakSelf deleteCurrentCellWithModel:model];
    } alertType:XPAlertTypeNormal];
    
}


- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)deleteCurrentCellWithModel:(XPAuctionModel *)model {
    [self startLoadingGif];
    NSString *url;
    NSDictionary *paraM = [NSDictionary dictionary];
    if (self.currentIndex == 1) {//取消参拍
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_join_delete_aution];
        paraM= @{@"id":[NSString stringWithFormat:@"%@",model.autionId]};
    }else {//取消委拍
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kVERIFY_delegate_delete_aution,model.autionId];
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            [self getListData];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}


- (void)getListData {
    NSString *url = [self getCurrentUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(10);
    if (self.currentIndex == 1) {
        paraM[@"orderStatus"] = @(0);
    }
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = [NSArray array];
            if (self.currentIndex == 1) {
                dataArray = data.userData[@"list"];
            }else {
                dataArray = data.userData[@"records"];
            }
            
            NSArray *modelArray = [XPAuctionModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            
            if (dataArray.count == 0) {
                [self showEmptyView:YES];
            }else {
                [self showEmptyView:NO];
            }
            
        }else {
            [self showEmptyView:YES];
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)headRefresh{
    self.page = 1;
    
    NSString *urlStr = [self getCurrentUrlPath];
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(10);
    if (self.currentIndex == 1) {
        paraM[@"orderStatus"] = @(0);
    }
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = [NSArray array];
            if (self.currentIndex == 1) {
                dataArray = data.userData[@"list"];
            }else {
                dataArray = data.userData[@"records"];
            }
            NSArray *modelArray = [XPAuctionModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *urlStr = [self getCurrentUrlPath];
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(10);
    if (self.currentIndex == 1) {
        paraM[@"orderStatus"] = @(0);
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            
            NSArray *dataArray = [NSArray array];
            if (self.currentIndex == 1) {
                dataArray = data.userData[@"list"];
            }else {
                dataArray = data.userData[@"records"];
            }
            if (dataArray.count == 0) {
                self.page --;
            }else {
                NSArray *modelArray = [XPAuctionModel mj_objectArrayWithKeyValuesArray:dataArray];
                [self.dataArray addObjectsFromArray:modelArray];
                [self.tableView reloadData];
            }
        }else {
            self.page --;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//获取当前选中页请求地址
- (NSString *)getCurrentUrlPath {
    NSString *urlStr;
    if (self.currentIndex == 1) {//参拍商品
       urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kProduct_my_delegte_list_new];
    }else if(self.currentIndex == 2){//委拍商品
        //kProduct_store_list
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kProduct_store_list];
    }
    return urlStr;
}
- (void)showEmptyView:(BOOL)shouldShow {
    
    self.tableView.hidden = shouldShow;
    if (self.currentIndex == 1) {//参拍
        self.emptyView1.hidden = !shouldShow;
        self.emptyView2.hidden = YES;
    }else if(self.currentIndex == 2){//待提货
        self.emptyView2.hidden = !shouldShow;
        self.emptyView1.hidden = YES;
    }
}
@end
