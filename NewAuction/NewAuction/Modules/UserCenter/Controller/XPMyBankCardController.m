//
//  XPMyBankCardController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/17.
//

#import "XPMyBankCardController.h"
#import "XPBankCardCell.h"
#import "XPAddBankViewController.h"


@interface XPMyBankCardController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UILabel *emptyLabel;

//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//当前页码
@property (nonatomic, assign) NSInteger page;

@end

@implementation XPMyBankCardController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.page = 1;
    self.dataArray = [NSMutableArray array];
    [self setupUI];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:self.navBarView];
    self.navBarView.backgroundColor = UIColor.clearColor;
    if (self.enterType == 0) {
        self.navBarView.titleLabel.text = @"支付卡";
    }else {
        self.navBarView.titleLabel.text = @"结算卡";
    }
    
}

- (void)setupUI {
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.tableView.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [self createFooterView];
    [self.view addSubview:self.tableView];
}

- (UIView *)createFooterView {
    UIView *footerView = [UIView new];
    footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 80 + 12 + 17 + 12 + self.safeBottom);
    
    UIButton *mainView = [[UIButton alloc] init];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 12, SCREEN_WIDTH, 80);
    mainView.layer.cornerRadius = 8;
    [mainView addTarget:self action:@selector(addButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:mainView];
    
    UIImageView *addImageView = [[UIImageView alloc]init];
    addImageView.frame = CGRectMake(20, 33, 15, 15);
    addImageView.image = [UIImage imageNamed:@"Frame_31"];
    [mainView addSubview:addImageView];
    
    
    UILabel *addLabel = [UILabel new];
    addLabel.frame = CGRectMake(MaxX(addImageView) + 13, 0, 300, mainView.mj_h);
    if (self.enterType == 0) {
        addLabel.text = @"添加支付卡";
    }else {
        
        addLabel.text = @"添加结算卡";
    }
    addLabel.font = kFontRegular(16);
    addLabel.textColor = UIColorFromRGB(0x3A3C41);
    [mainView addSubview:addLabel];
    
    self.emptyLabel= [[UILabel alloc] init];
    self.emptyLabel.frame = CGRectMake(0, 92 + 12, SCREEN_WIDTH, 17);
    self.emptyLabel.textColor = UIColorFromRGB(0x8A8A8A);
    if (self.enterType == 0) {
        self.emptyLabel.text = @"您还没有支付卡,请添加";
    }else {
        self.emptyLabel.text = @"您还没有结算卡,请添加";
    }
    
    self.emptyLabel.font = kFontRegular(12);
    self.emptyLabel.textAlignment = NSTextAlignmentCenter;
    [footerView addSubview:_emptyLabel];
    
    return footerView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    XPBankCardCell *cell = [XPBankCardCell cellWithTableView:tableView];
    XPBankModel *model = self.dataArray[indexPath.row];
    cell.cellType = self.enterType;
    [cell cellWithModel:model];
    
    XPWeakSelf
    [cell setRightBlock:^{

        [weakSelf.xpAlertView configWithTitle:@"您确定要解绑此银行卡吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
            
        } rightAction:^(NSString * _Nullable extra) {
            [weakSelf deleteCardWithModel:model];
        } alertType:XPAlertTypeNormal];
        
    }];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120 + 12;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XPBankModel *model = self.dataArray[indexPath.row];
    if (self.chooseBankBlock) {
        self.chooseBankBlock(model);
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (![[XPVerifyTool shareInstance] checkUserAuthentication]) {
        [[XPVerifyTool shareInstance] userShouldAuthentication];
    }else {
        XPAddBankViewController *vc = [XPAddBankViewController new];
        vc.enterType = self.enterType;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getData];
}

- (void)deleteCardWithModel:(XPBankModel *)model {
    NSString *url;
    if (self.enterType == 0) {
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kBank_card_delete,model.productId];
    }else {
       url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kBank_result_delete,model.productId];
    }
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"cardId"] = [NSString stringWithFormat:@"%@",model.productId];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        
        if (data.isSucess) {
            
            if (self.enterType == 0) {//解绑支付卡
                
                NSString *pendingStr = data.userData[@"status"];
                if ([pendingStr isEqualToString:@"failed"]) {
                    [self stopLoadingGif];
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
                }else {
                    NSString *orderIdStr = data.userData[@"order_no"];
                    [self deletePaymentCardQueryWithOrderId:orderIdStr bankModel:model];
                }
                
            }else {//解绑提现卡
                [self stopLoadingGif];
                [self getData];
            }
            
            
        }else {
            [self stopLoadingGif];
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)deletePaymentCardQueryWithOrderId:(NSString *)orderStr bankModel:(XPBankModel *)model{
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,k_bank_delete_query,orderStr];
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        
        if (data.isSucess) {
            
            NSString *pendingStr = data.userData[@"status"];
            if ([pendingStr isEqualToString:@"succeeded"]) {
                [self stopLoadingGif];
                [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
                [[NSNotificationCenter defaultCenter] postNotificationName:Notification_user_choose_bankcard object:model];
                [self getData];
            }else if ([pendingStr isEqualToString:@"failed"]){
                [self stopLoadingGif];
                [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
            }else {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self deletePaymentCardQueryWithOrderId:orderStr bankModel:model];
                    
                });
            }
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}



- (void)getData{
    NSString *url = [self getUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];

            NSArray *modelArray = [XPBankModel mj_objectArrayWithKeyValuesArray:dataArray];
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            if (self.dataArray.count == 0) {
                self.emptyLabel.hidden = NO;
            }else {
                self.emptyLabel.hidden = YES;
            }

        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)headerRefresh{
    NSString *url = [self getUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPBankModel mj_objectArrayWithKeyValuesArray:dataArray];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:modelArray];
            [self.tableView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *url = [self getUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPBankModel mj_objectArrayWithKeyValuesArray:dataArray];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self.tableView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

- (NSString *)getUrlPath {
    NSString *url;
    if (self.enterType == 0) {
        url =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kBank_card_list];
    }else {
        url =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kBank_result_list];
    }
    return url;
}


//kBank_card_list

@end
