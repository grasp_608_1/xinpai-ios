//
//  XPPreferenceSettingController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/7.
//

#import "XPPreferenceSettingController.h"
#import "XPPreferenceSetCell.h"

@interface XPPreferenceSettingController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
//数据源
@property (nonatomic, strong) NSArray *dataArray;

@property (nonatomic, assign) NSInteger selectedIndex;
@end

@implementation XPPreferenceSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    self.dataArray = [NSMutableArray array];
    [self setupUI];
    [self initData];
    [self.tableView reloadData];
    
    NSString *selectedStr = [XPLocalStorage readUserHomeStatus];
    NSInteger index = selectedStr.integerValue;
    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    
}


- (void)initData{
    
    if (APPTYPE == 0) {
        self.dataArray = @[
            @{@"image":@"xp_preference_xp",
              @"title":@"新拍探讨",
              @"description":@"数字经济时代全新拍卖模式数字经济时代全新拍卖模式",
              @"status":@"0"
            },
            @{@"image":@"xp_preference_shop",
                @"title":@"商城购物",
                @"description":@"电商平台便捷购物,海量商品任你选,足不出户畅享优质消费体验",
                @"status":@"0"
              },
    //        @{@"image":@"xp_preference_3d",
    //              @"title":@"圈子",
    //              @"description":@"社交新圈汇挚友，欢乐同行创精彩",
    //              @"status":@"0"
    //        }
        ];
    }else if (APPTYPE == 1){
        self.dataArray = @[
        
            @{@"image":@"xp_preference_3d",
                  @"title":@"造物",
                  @"description":@"社交新圈汇挚友，欢乐同行创精彩",
                  @"status":@"0"
            },
           @{@"image":@"xp_preference_shop",
                @"title":@"商城购物",
                @"description":@"电商平台便捷购物,海量商品任你选,足不出户畅享优质消费体验",
                @"status":@"0"
              },
    //        @{@"image":@"xp_preference_3d",
    //              @"title":@"圈子",
    //              @"description":@"社交新圈汇挚友，欢乐同行创精彩",
    //              @"status":@"0"
    //        }
        ];
    }
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"偏好设置";
    [self.view addSubview:self.navBarView];
}

- (void)setupUI {
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - self.safeBottom - 50);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self.view addSubview:self.tableView];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 24)];
    self.tableView.tableFooterView = [self getTableViewFooterView];
    
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(12,self.view.mj_h - self.safeBottom - 50, SCREEN_WIDTH - 2 *12, 50);
    [saveButton setTitle:@"确定" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    saveButton.layer.cornerRadius = 6;
    saveButton.clipsToBounds = YES;
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_57"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveButton];
}

#pragma mark --- tableview datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPPreferenceSetCell *cell = [XPPreferenceSetCell cellWithTableView:tableView];
    NSDictionary *dataDic = self.dataArray[indexPath.row];
    cell.dataDic = dataDic;
    return cell;
}

#pragma mark --- tableview delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 132;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.selectedIndex = indexPath.row;
    
    NSMutableArray *tempArray = [NSMutableArray array];
    for (int i = 0; i<self.dataArray.count; i++) {
        NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
        [mDict addEntriesFromDictionary:self.dataArray[i]];
        if (i == indexPath.row) {
            [mDict setObject:@"1" forKey:@"status"];
        }else {
            [mDict setObject:@"0" forKey:@"status"];
        }
        [tempArray addObject:mDict];
    }
    
    self.dataArray = [tempArray copy];
    [self.tableView reloadData];
}


- (UIView *)getTableViewFooterView{
    UIView *footer = [UIView new];
    footer.frame = CGRectMake(0, 0, SCREEN_WIDTH, 90*SCREEN_WIDTH/327.);
    UIImageView *imageView = [UIImageView new];
    imageView.image = [UIImage imageNamed:@"xp_preference_commander"];
    imageView.frame = CGRectMake(24, 0, footer.mj_w - 24 *2, footer.mj_h);
    [footer addSubview:imageView];
    
    return footer;
}

- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    
    NSString *urlString =  [NSString stringWithFormat:@"%@%@/%zd",kVERIFY_host,k_user_preferenceHome,self.selectedIndex];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonGetWithURL:urlString Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [XPLocalStorage.new writeUserHomeStatus:[NSString stringWithFormat:@"%zd",self.selectedIndex]];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            
        }
        
        
    }];
    
    
}

@end
