//
//  XPUserShopCouponController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/21.
//

#import "XPUserShopCouponController.h"
#import "XPSelectItemView.h"
#import "XPShopCouponCell.h"
#import "XPEmptyView.h"
#import "XPCouponModel.h"
#import "XPShopingController.h"


@interface XPUserShopCouponController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) XPEmptyView *emptyView;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//当前页码
@property (nonatomic, assign) NSInteger page;

@end

@implementation XPUserShopCouponController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
//默认选中第一个
    self.currentIndex = 1;
    self.page = 1;
    self.dataArray = [NSMutableArray array];
    [self setupUI];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.page = 1;
    [self getListData];
    
}
- (void)setupUI {
    
    XPSelectItemView *selectItemView = [[XPSelectItemView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, 40)];
    [selectItemView configWithtitleArray:@[@"待使用",@"已使用",@"已过期"]];
    [self.view addSubview:selectItemView];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.tableView.frame = CGRectMake(0, MaxY(selectItemView), SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - 40);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:self.tableView];

    UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.mj_w, 12)];
    header.backgroundColor = UIColor.whiteColor;
    self.tableView.tableHeaderView = header;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.mj_w, self.safeBottom)];
    
    XPWeakSelf
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf headRefresh];
    }];
    
    [selectItemView setTapBlock:^(NSInteger index) {
        //网络请求
        weakSelf.currentIndex = index;
        weakSelf.page = 1;
        [weakSelf.dataArray removeAllObjects];
        [weakSelf getListData];
    }];
    
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = YES;
    self.tableView.hidden = NO;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无优惠券"];
    [self.view addSubview:self.emptyView];
    
    selectItemView.defaultSeletIndex = 0;
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"优惠券";
    [self.view addSubview:self.navBarView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPShopCouponCell *cell = [XPShopCouponCell cellWithTableView:tableView];
    XPCouponModel *model = self.dataArray[indexPath.row];
    if (self.currentIndex == 1) {
        model.receiveStatus = @(1);
    }else if (self.currentIndex == 2){
        model.receiveStatus = @(2);
    }else {
        model.receiveStatus = @(3);
    }
    cell.cellType = self.currentIndex;
    cell.model = model;
    
    
    [cell setReceiveBlock:^{
        XPShopingController *vc = [XPShopingController new];
        vc.noteLongTitle = model.noteLong;
        vc.enterType = 3;
        vc.couponId = model.couponId;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 112;
}

- (void)getListData {
    NSString *url = [self getCurrentUrlPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(10);
    if (self.currentIndex == 1) {
        paraM[@"orderStatus"] = @(1);
    }
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPCouponModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            
            if (dataArray.count == 0) {
                [self showEmptyView:YES];
            }else {
                [self showEmptyView:NO];
            }
            
        }else {
            [self showEmptyView:YES];
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)headRefresh{
    self.page = 1;
    
    NSString *urlStr = [self getCurrentUrlPath];
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(10);
    if (self.currentIndex == 1) {
        paraM[@"orderStatus"] = @(1);
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPCouponModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = [modelArray mutableCopy];
            [self.tableView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *urlStr = [self getCurrentUrlPath];
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(10);
    if (self.currentIndex == 1) {
        paraM[@"orderStatus"] = @(1);
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            
            if (dataArray.count == 0) {
                self.page --;
            }else {
                NSArray *modelArray = [XPCouponModel mj_objectArrayWithKeyValuesArray:dataArray];
                [self.dataArray addObjectsFromArray:modelArray];
                [self.tableView reloadData];
            }
        }else {
            self.page --;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//获取当前选中页请求地址
- (NSString *)getCurrentUrlPath {
    NSString *urlStr;
    if (self.currentIndex == 1) {//待使用
       urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_coupon_unused];
    }else if(self.currentIndex == 2){//已使用
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_coupon_used];
    }else {//已过期
        urlStr =  [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_coupon_expired];
    }
    return urlStr;
}
- (void)showEmptyView:(BOOL)shouldShow {
    self.tableView.hidden = shouldShow;
    self.emptyView.hidden = !shouldShow;
}
@end
