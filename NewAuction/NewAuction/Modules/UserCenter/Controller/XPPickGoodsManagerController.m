//
//  XPPickGoodsManagerController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/17.
//

#import "XPPickGoodsManagerController.h"
#import "XPEmptyView.h"
#import "XPGoodManagerCell.h"
#import "XPAuctionModel.h"
#import "XPTakeGoodsDetailController.h"
#import "XPLogisticsInfoController.h"

@interface XPPickGoodsManagerController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) XPEmptyView *emptyView;

//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//当前页码
@property (nonatomic, assign) NSInteger page;

@end

@implementation XPPickGoodsManagerController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    self.dataArray = [NSMutableArray array];
    self.page = 1;
    
    [self setupUI];
    [self getData];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"提货管理";
    [self.view addSubview:self.navBarView];
}


- (void)setupUI {
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBottomHeight)];
    [self.view addSubview:self.tableView];
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无数据"];
    [self.view addSubview:self.emptyView];
    
    XPWeakSelf
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf headerRefresh];
    }];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPGoodManagerCell *cell = [XPGoodManagerCell cellWithTableView:tableView];
    XPAuctionModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    XPWeakSelf
    [cell setLeftBlock:^{
        
        if (model.expressNo) {
            XPLogisticsInfoController *vc = [XPLogisticsInfoController new];
            vc.orderId = model.expressNo;
            vc.tplCode = model.code100;
            vc.phone = model.phone;
            vc.orderStatus = model.orderStatus;
            vc.expressCompany = model.expressCompany;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
        
    }];
    [cell setRightBlock:^{
        XPTakeGoodsDetailController *vc = [XPTakeGoodsDetailController new];
        vc.goodsNum = model.autionId;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 212;
}


- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_pick_goods_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPAuctionModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)headerRefresh{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_pick_goods_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPAuctionModel mj_objectArrayWithKeyValuesArray:dataArray];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:modelArray];
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_pick_goods_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPAuctionModel mj_objectArrayWithKeyValuesArray:dataArray];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self.tableView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

- (void)canShowEmptyView {
    if (self.dataArray.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}
@end
