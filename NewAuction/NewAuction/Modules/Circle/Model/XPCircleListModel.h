//
//  XPCircleListModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPCircleListModel : NSObject

@property (nonatomic, strong) NSNumber *circleId;//圈子id
@property (nonatomic, strong) NSNumber *type;//0 发图片 1 发视频
@property (nonatomic, strong) NSNumber *isComment;//是否已评论
@property (nonatomic, strong) NSNumber *isLike;//是否已点赞
@property (nonatomic, strong) NSNumber *isShare;//是否已分享
@property (nonatomic, strong) NSNumber *isSelf;//是否是本人发的圈子
@property (nonatomic, strong) NSNumber *likeNum;//点赞数
@property (nonatomic, strong) NSNumber *commentNum;//评论数
@property (nonatomic, strong) NSNumber *shareNum;//分享次数
@property (nonatomic, strong) NSNumber *userId;//发布人id
@property (nonatomic, strong) NSNumber *topStatus;//推荐置顶状态 0 否 1 推荐
@property (nonatomic, strong) NSNumber *degree;//热度
@property (nonatomic, strong) NSNumber *officialStatus;//是否是官方发布

@property (nonatomic, copy) NSString *video;//视频地址
@property (nonatomic, copy) NSString *name;//name
@property (nonatomic, copy) NSString *content;//发表的文字
@property (nonatomic, copy) NSString *createTime;//创建时间
@property (nonatomic, copy) NSString *userNickname;//用户昵称
@property (nonatomic, copy) NSString *userHead;//用户头像
@property (nonatomic, copy) NSString *pics;//圈子图片列表

@property (nonatomic, copy) NSArray *circleCommentList;//评论列表
@property (nonatomic, copy) NSArray *circleImagesList;//圈子图片列表(废弃)
@property (nonatomic, copy) NSArray *circleCommentLikeList;//圈子点赞列表

@property (nonatomic, assign) bool isfold;//是否展开 1 展开 0 折叠

@end

NS_ASSUME_NONNULL_END
