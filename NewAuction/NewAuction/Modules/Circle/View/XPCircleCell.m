//
//  XPCircleCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/14.
//

#import "XPCircleCell.h"
#import  "XPOpenSourceTool.h"

@interface XPCircleCell ()
@property (nonatomic, strong) UIView *mainView;
//用户头像
@property (nonatomic, strong) UIImageView  *iconImageView;
//用户昵称
@property (nonatomic, strong) UILabel *nameLabel;
//官方标签
@property (nonatomic, strong) UIImageView *signImageView;
//评价时间
@property (nonatomic, strong) UILabel *timeLabel;
//🔥
@property (nonatomic, strong) UIImageView *hotIcon;
//热度数量
@property (nonatomic, strong) UILabel *hotNumLabel;
//描述
@property (nonatomic, strong) UILabel *descLabel;
//是否折叠
@property (nonatomic, strong) UIButton *foldButton;
//资源( 图片 ,视频 )
@property (nonatomic, strong) UIView *sourceView;
//视频展位图
@property (nonatomic, strong) UIImageView *videoImageView;
//分享 点赞 评论
@property (nonatomic, strong) UIView *activeView;
//发表评论
@property (nonatomic, strong)UIButton *sendButton;
//评论 展示
@property (nonatomic, strong) UIView *commentView;

@property (nonatomic, strong) NSMutableArray *photos;
//cell model
@property (nonatomic, strong) XPCircleListModel *model;

@end

@implementation XPCircleCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"XPCircleCell";
    XPCircleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPCircleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    self.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12*2, 0);
    mainView.clipsToBounds = YES;
    mainView.layer.cornerRadius = 8;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self.contentView addSubview:mainView];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.frame = CGRectMake(12, 16, 36, 36);
    self.iconImageView.layer.cornerRadius = 18;
    self.iconImageView.layer.masksToBounds = YES;
    self.iconImageView.userInteractionEnabled = YES;
    [self.mainView addSubview:self.iconImageView];
    UITapGestureRecognizer *headTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headTap:)];
    [self.iconImageView addGestureRecognizer:headTap];

    UILabel *nameLabel = [UILabel new];
    nameLabel.font = kFontMedium(16);
    nameLabel.textColor = UIColorFromRGB(0x6E73AF);
    nameLabel.frame = CGRectMake(MaxX(self.iconImageView) + 4, 16,0, 22);
    self.nameLabel = nameLabel;
    [self.mainView addSubview:nameLabel];
    
    self.signImageView = [[UIImageView alloc] init];
    self.signImageView.frame = CGRectMake(12, 21, 32, 14);
    self.signImageView.image = [UIImage imageNamed:@"xp_circle_offical"];
    self.signImageView.hidden = YES;
    self.signImageView.layer.masksToBounds = YES;
    [self.mainView addSubview:self.signImageView];
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = kFontRegular(12);
    timeLabel.textColor = UIColorFromRGB(0xB8B8B8);
    timeLabel.frame = CGRectMake(MaxX(self.iconImageView) + 4, MaxY(self.nameLabel),self.nameLabel.mj_w, 14);
    self.timeLabel = timeLabel;
    [self.mainView addSubview:timeLabel];
    
    self.hotIcon = [[UIImageView alloc] init];
    self.hotIcon.frame = CGRectMake(12, 16, 20, 20);
    self.hotIcon.layer.cornerRadius = 10;
    self.hotIcon.layer.masksToBounds = YES;
    self.hotIcon.image = [UIImage imageNamed:@"xp_circle_fire"];
    [self.mainView addSubview:self.hotIcon];
    
    UILabel *hotNumLabel = [UILabel new];
    hotNumLabel.font = kFontRegular(12);
    hotNumLabel.textColor = app_font_color;
    hotNumLabel.frame = CGRectMake(50, 28,200, 14);
    self.hotNumLabel = hotNumLabel;
    [self.mainView addSubview:hotNumLabel];
    
    UILabel *descLabel = [UILabel new];
    descLabel.font = kFontRegular(16);
    descLabel.textColor = app_font_color;
    descLabel.numberOfLines = NO;
    descLabel.userInteractionEnabled = YES;
    self.descLabel = descLabel;
    [self.mainView addSubview:descLabel];
    
    UIButton *foldButton = [UIButton buttonWithType:UIButtonTypeCustom];
    foldButton.hidden = YES;
    [foldButton setImage:[UIImage imageNamed:@"xp_circle_fold"] forState:UIControlStateNormal];
    [foldButton addTarget:self action:@selector(foldButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.foldButton = foldButton;
    [descLabel addSubview:foldButton];
    
    UIView *sourceView = [UIView new];
    self.sourceView = sourceView;
    [self.mainView addSubview:sourceView];
    
    UIView *activeView = [UIView new];
    self.activeView = activeView;
    [self.mainView addSubview:activeView];
    
    UIButton *sendButton = [[UIButton alloc] init];
    [sendButton setTitle:@"   发表评论" forState:UIControlStateNormal];
    sendButton.titleLabel.font = kFontRegular(14);
    sendButton.layer.cornerRadius = 16;
    sendButton.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [sendButton setTitleColor:UIColorFromRGB(0xB8B8B8) forState:UIControlStateNormal];
    sendButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [sendButton addTarget:self action:@selector(sendButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.sendButton = sendButton;
    [self.mainView addSubview:sendButton];
    
    UIView *commentView = [UIView new];
    self.commentView = commentView;
    [self.mainView addSubview:commentView];

}


-(CGFloat)cellWithModel:(XPCircleListModel *)model {
    _model = model;
    NSArray *commentArray = model.circleCommentList;
    
    CGFloat height = 0;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.userHead] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    self.iconImageView.frame =  CGRectMake(12, 16, 36, 36);
    
    self.hotNumLabel.text = [NSString stringWithFormat:@"%@",model.degree];
    CGFloat hotW = [self.hotNumLabel.text sizeWithAttributes:@{NSFontAttributeName:self.hotNumLabel.font}].width;
    self.hotNumLabel.frame = CGRectMake(self.mainView.mj_w - 24 - hotW, 19, hotW, 14);
    self.hotIcon.frame = CGRectMake(self.hotNumLabel.mj_x - 20, 16, 20, 20);
    
    self.nameLabel.text = model.userNickname;
    
    CGFloat nickNameW = [self.nameLabel.text sizeWithAttributes:@{NSFontAttributeName: self.nameLabel.font}].width;
    self.nameLabel.frame = CGRectMake(MaxX(self.iconImageView) + 4, 16, nickNameW, 22);
    
    if (model.officialStatus.intValue == 1) {
        self.signImageView.hidden = NO;
        self.signImageView.frame = CGRectMake(MaxX(self.nameLabel) + 5, 19, 32, 14);
    }else {
        self.signImageView.hidden = YES;
    }
    
    self.timeLabel.text = model.createTime;
    self.timeLabel.frame = CGRectMake(MaxX(self.iconImageView) + 4, MaxY(self.nameLabel),self.hotIcon.mj_x - MaxX(self.iconImageView) - 8, 14);
    
    CGFloat contentH = [model.content boundingRectWithSize:CGSizeMake(self.mainView.mj_w - 24 , CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.descLabel.font} context:nil].size.height;
    self.descLabel.text = model.content;
    
    if (contentH <= 70) {
        self.descLabel.frame = CGRectMake(12, MaxY(self.iconImageView) + 12, self.mainView.mj_w - 24, contentH);
        self.foldButton.hidden = YES;
    }else if (contentH > 70 && !model.isfold){
        self.descLabel.frame = CGRectMake(12, MaxY(self.iconImageView) + 12, self.mainView.mj_w - 24, 70);
        self.foldButton.hidden = NO;
        self.foldButton.frame = CGRectMake(self.descLabel.mj_w - 50, self.descLabel.mj_h - 22, 50, 20);
    }else {
        self.descLabel.frame = CGRectMake(12, MaxY(self.iconImageView) + 12, self.mainView.mj_w - 24, contentH);
        self.foldButton.hidden = YES;
    }
    
    
    height = MaxY(self.descLabel);
    
    [self.sourceView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (model.type.intValue == 0) {//图片
        CGFloat margin = 12;
        CGFloat imageWH = (self.mainView.mj_w - 12 * 4 )/3;
        NSArray *picsArray  = [NSArray array];
        if (model.pics.length > 0) {
            picsArray = [model.pics componentsSeparatedByString:@","];
        }
        if (picsArray.count == 0) {
            self.sourceView.frame = CGRectMake(12,height + 12,self.mainView.mj_w - 24, 0);
        }else {
            for (int i = 0; i< picsArray.count; i++) {
                UIImageView *imageView = [UIImageView new];
                [imageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(picsArray[i])] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
                imageView.frame = CGRectMake((i%3)* (imageWH + margin), i/3* (imageWH + margin), imageWH, imageWH);
                imageView.layer.cornerRadius = 4;
                imageView.layer.masksToBounds = YES;
                imageView.contentMode =  UIViewContentModeScaleAspectFill;
                imageView.tag = i;
                imageView.userInteractionEnabled = YES;
                [self.sourceView addSubview:imageView];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
                [imageView addGestureRecognizer:tap];
                
                if (picsArray.count - 1 == i) {
                    self.sourceView.frame = CGRectMake(12,height + 12,self.mainView.mj_w - 24, MaxY(imageView));
                }
            }
            height = MaxY(self.sourceView);
        }
        
        
    }else if (model.type.intValue == 1){//视频
       
        if (model.video.length > 0) {
            CGFloat imageW = (self.mainView.mj_w - 24)/2;
            CGFloat imageH = imageW * 101/160.;
            UIImageView *videoImageView = [[UIImageView alloc] init];
            videoImageView.frame = CGRectMake(0, 0, imageW, imageH);
//            videoImageView.backgroundColor = UIColor.grayColor;
            videoImageView.layer.cornerRadius = 4;
            videoImageView.layer.masksToBounds = YES;
            videoImageView.userInteractionEnabled = YES;
            self.videoImageView = videoImageView;
            [self.sourceView addSubview:videoImageView];
            
            UIImageView *videoPlayImageView = [[UIImageView alloc] init];
            videoPlayImageView.frame = videoImageView.bounds;
            videoPlayImageView.image = [UIImage imageNamed:@"xp_video_paly"];
            [videoImageView addSubview:videoPlayImageView];
            
            [videoImageView sd_setImageWithURL:[NSURL URLWithString:model.pics]];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoTap:)];
            [videoImageView addGestureRecognizer:tap];
            
            self.sourceView.frame = CGRectMake(12,height + 12 ,self.mainView.mj_w - 24, imageH);
            height = MaxY(self.sourceView);
            
        }else {
            self.sourceView.frame = CGRectMake(12,MaxY(self.descLabel) + 12 ,self.mainView.mj_w - 24, 0);
        }
    }
    
    self.sendButton.frame = CGRectMake(12, height + 12, 117, 32);
    
    //转发评论点赞
    self.activeView.frame = CGRectMake(MaxX(self.sendButton), self.sendButton.mj_y,self.mainView.mj_w - MaxX(self.sendButton) - 12, self.sendButton.mj_h);
    
    NSArray *activityArray = @[
        @{@"image":@"xp_circle_comment",@"num" : model.commentNum },
        @{@"image":@"xp_circle_like",@"num" : model.likeNum },
        @{@"image":@"xp_circle_share",@"num" : model.shareNum },
    ];
    [self.activeView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGFloat right = self.activeView.mj_w;
    for (int i = 0 ; i< activityArray.count; i++) {
        NSDictionary *dic = activityArray[i];
        NSString *numStr = [NSString stringWithFormat:@"%@",dic[@"num"]];
        CGFloat numW = [numStr sizeWithAttributes:@{NSFontAttributeName:kFontRegular(10)}].width;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(right - numW - 24 - 12, 4, numW + 24 + 12, 24);
        button.tag = i;
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.activeView addSubview:button];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 0, 24, 24)];
        imageView.image = [UIImage imageNamed:dic[@"image"]];
        [button addSubview:imageView];
        
        UILabel *label = [UILabel new];
        label.font = kFontRegular(10);
        label.textColor = app_font_color_8A;
        label.textAlignment = NSTextAlignmentRight;
        label.text = numStr;
        label.frame = CGRectMake(0, 8, button.mj_w , 16);
        [button addSubview:label];
        
        right = button.mj_x;
    }
    
    height = MaxY(self.sendButton);
    
    [self.commentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (commentArray.count > 0) {
        
        CGFloat bottom = 0;
        for (int j = 0; j< commentArray.count; j++) {
            
            UIView *bgView = [UIView new];
            bgView.backgroundColor = UIColorFromRGB(0xF2F2F2);
            bgView.layer.cornerRadius = 4;
            bgView.userInteractionEnabled = YES;
            bgView.tag = j;
            [self.commentView addSubview:bgView];
            
            UILongPressGestureRecognizer *longGes = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longGes:)];
            [bgView addGestureRecognizer:longGes];
            
            NSDictionary *dic = commentArray[j];
            NSString *content = TO_STR(dic[@"content"]);
            NSString *nickName = TO_STR(dic[@"nickname"]);
            NSString *str = [NSString stringWithFormat:@"%@: %@",nickName,content];
        
            CGFloat commentItemH = [str boundingRectWithSize:CGSizeMake(self.mainView.mj_w - 64 , CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:kFontRegular(14)} context:nil].size.height;
            
            UILabel *label = [UILabel new];
            label.font = kFontRegular(14);
            label.textColor = app_font_color;
            label.numberOfLines = NO;
            label.frame = CGRectMake(12, 4, self.mainView.mj_w - 64, commentItemH);
            [bgView addSubview:label];
            
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:str];
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(14),NSForegroundColorAttributeName:UIColorFromRGB(0x6E73AF)} range:NSMakeRange(0, nickName.length)];
            label.attributedText = attr;
            
            bgView.frame = CGRectMake(0, 12 + bottom, self.mainView.mj_w - 24, commentItemH + 8);
            
            bottom = MaxY(bgView);
            
            if (j == commentArray.count - 1) {
                self.commentView.frame = CGRectMake(12, height, self.mainView.mj_w - 24, bottom);
                height = MaxY(self.commentView);
            }
        }
    }else {
        self.commentView.frame = CGRectZero;
    }

    self.mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12*2, height + 16);
    
    return MaxY(self.mainView) + 12;
}

- (void)headTap:(UITapGestureRecognizer *)tap{
    buttonCanUseAfterOneSec(tap.view);
    if (self.headTapBlock) {
        self.headTapBlock();
    }
}

//图片点击
- (void)tap:(UITapGestureRecognizer *)tap {
    buttonCanUseAfterOneSec(tap.view);
    
    self.photos = [NSMutableArray array];

    NSArray *circleImagesList = [self.model.pics componentsSeparatedByString:@","];
    for (int i = 0; i<circleImagesList.count; i++) {
        GKPhoto *photo = [GKPhoto new];
        photo.url = [NSURL URLWithString:TO_STR(circleImagesList[i])];
        [self.photos addObject:photo];
    }
    GKPhotoBrowser *browser = [GKPhotoBrowser photoBrowserWithPhotos:self.photos currentIndex:tap.view.tag];
    [browser showFromVC:[XPCommonTool getCurrentViewController]];
}
//视频点击
- (void)videoTap:(UITapGestureRecognizer *)tap {
    buttonCanUseAfterOneSec(tap.view);
    if (self.videoBlock) {
        self.videoBlock(self.model.video);
    }
}

- (void)longGes:(UILongPressGestureRecognizer *)longGes {
    buttonCanUseAfterOneSec(longGes.view);
    if (longGes.state  == UIGestureRecognizerStateEnded) {
        NSDictionary *dic = self.model.circleCommentList[longGes.view.tag];
        NSNumber *isSelf = dic[@"isSelf"];
        if (isSelf.intValue == 1) {
            if (self.deleteCommentBlock) {
                self.deleteCommentBlock(longGes.view.tag);
            }
        }else {
            return;
        }
    }
}

- (void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.itemTapBlock) {
        self.itemTapBlock(sender.tag);
    }
    
}

- (void)sendButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.commentBlock) {
        self.commentBlock();
    }
}
- (void)foldButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.foldButtonBlock) {
        self.foldButtonBlock();
    }
}
@end
