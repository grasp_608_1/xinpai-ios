//
//  XPCircleDetailView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/19.
//

#import "XPCircleDetailView.h"
#import "XPOpenSourceTool.h"

@interface XPCircleDetailView ()
@property (nonatomic, strong) UIView *mainView;
//用户头像
@property (nonatomic, strong) UIImageView  *iconImageView;
//用户昵称
@property (nonatomic, strong) UILabel *nameLabel;
//评价时间
@property (nonatomic, strong) UILabel *timeLabel;
//🔥
@property (nonatomic, strong) UIImageView *hotIcon;
//热度数量
@property (nonatomic, strong) UILabel *hotNumLabel;
//描述
@property (nonatomic, strong) UILabel *descLabel;
//资源( 图片 ,视频 )
@property (nonatomic, strong) UIView *sourceView;
//视频展位图
@property (nonatomic, strong) UIImageView *videoImageView;
//分享 点赞 评论
@property (nonatomic, strong) UIView *activeView;
//发表评论
@property (nonatomic, strong)UIButton *sendButton;
//点赞的人View
@property (nonatomic, strong) UIButton *likeView;

@property (nonatomic, strong) XPCircleListModel *model;

@property (nonatomic, strong) NSMutableArray *photos;
@end


@implementation XPCircleDetailView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    self.backgroundColor = UIColorFromRGB(0xF7F8FB);
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 12, SCREEN_WIDTH - 12*2, 0);
    mainView.clipsToBounds = YES;
    mainView.layer.cornerRadius = 8;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.frame = CGRectMake(12, 16, 36, 36);
    self.iconImageView.layer.cornerRadius = 18;
    self.iconImageView.layer.masksToBounds = YES;
    [self.mainView addSubview:self.iconImageView];
    
    UILabel *nameLabel = [UILabel new];
    nameLabel.font = kFontMedium(16);
    nameLabel.textColor = UIColorFromRGB(0x6E73AF);
    nameLabel.frame = CGRectMake(MaxX(self.iconImageView) + 4, 16,0, 22);
    self.nameLabel = nameLabel;
    [self.mainView addSubview:nameLabel];
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = kFontRegular(12);
    timeLabel.textColor = UIColorFromRGB(0xB8B8B8);
    timeLabel.frame = CGRectMake(MaxX(self.iconImageView) + 4, MaxY(self.nameLabel),self.nameLabel.mj_w, 14);
    self.timeLabel = timeLabel;
    [self.mainView addSubview:timeLabel];
    
    self.hotIcon = [[UIImageView alloc] init];
    self.hotIcon.frame = CGRectMake(12, 16, 20, 20);
    self.hotIcon.layer.cornerRadius = 10;
    self.hotIcon.layer.masksToBounds = YES;
    self.hotIcon.image = [UIImage imageNamed:@"xp_circle_fire"];
    [self.mainView addSubview:self.hotIcon];
    
    UILabel *hotNumLabel = [UILabel new];
    hotNumLabel.font = kFontRegular(12);
    hotNumLabel.textColor = app_font_color;
    hotNumLabel.frame = CGRectMake(50, 28,200, 14);
    self.hotNumLabel = hotNumLabel;
    [self.mainView addSubview:hotNumLabel];
    
    UILabel *descLabel = [UILabel new];
    descLabel.font = kFontRegular(16);
    descLabel.textColor = app_font_color;
    descLabel.numberOfLines = NO;
    self.descLabel = descLabel;
    [self.mainView addSubview:descLabel];
    
    UIView *sourceView = [UIView new];
    self.sourceView = sourceView;
    [self.mainView addSubview:sourceView];
    
    UIButton *likeView = [UIButton new];
    [likeView addTarget:self action:@selector(likeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.likeView = likeView;
    [self.mainView addSubview:likeView];
    
    UIView *activeView = [UIView new];
    self.activeView = activeView;
    [self.mainView addSubview:activeView];
    
    UIButton *sendButton = [[UIButton alloc] init];
    [sendButton setTitle:@"   发表评论" forState:UIControlStateNormal];
    sendButton.titleLabel.font = kFontRegular(14);
    sendButton.layer.cornerRadius = 16;
    sendButton.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [sendButton setTitleColor:UIColorFromRGB(0xB8B8B8) forState:UIControlStateNormal];
    sendButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [sendButton addTarget:self action:@selector(sendButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.sendButton = sendButton;
    [self.mainView addSubview:sendButton];
    
}


-(CGFloat)ViewWithModel:(XPCircleListModel *)model {
    
    _model = model;
    
    CGFloat height = 0;
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.userHead] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    self.iconImageView.frame =  CGRectMake(12, 16, 36, 36);
    
    self.hotNumLabel.text = [NSString stringWithFormat:@"%@",model.degree];
    CGFloat hotW = [self.hotNumLabel.text sizeWithAttributes:@{NSFontAttributeName:self.hotNumLabel.font}].width;
    self.hotNumLabel.frame = CGRectMake(self.mainView.mj_w - 24 - hotW, 19, hotW, 14);
    self.hotIcon.frame = CGRectMake(self.hotNumLabel.mj_x - 20, 16, 20, 20);
    
    self.nameLabel.text = model.userNickname;
    self.nameLabel.frame = CGRectMake(MaxX(self.iconImageView) + 4, 16, self.hotIcon.mj_x - MaxX(self.iconImageView) - 8, 22);
    
    self.timeLabel.text = model.createTime;
    self.timeLabel.frame = CGRectMake(MaxX(self.iconImageView) + 4, MaxY(self.nameLabel),self.nameLabel.mj_w, 14);
    
    CGFloat contentH = [model.content boundingRectWithSize:CGSizeMake(self.mainView.mj_w - 24 , CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.descLabel.font} context:nil].size.height;
    self.descLabel.text = model.content;
    self.descLabel.frame = CGRectMake(12, MaxY(self.iconImageView) + 12, self.mainView.mj_w - 24, contentH);
    height = MaxY(self.descLabel);
    
    [self.sourceView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (model.type.intValue == 0) {//图片
        CGFloat margin = 12;
        CGFloat imageWH = (self.mainView.mj_w - 12 * 4 )/3;
        NSArray *picsArray  = [NSArray array];
        if (model.pics.length > 0) {
            picsArray = [model.pics componentsSeparatedByString:@","];
        }
        if (picsArray.count == 0) {
            self.sourceView.frame = CGRectMake(12,height + 12,self.mainView.mj_w - 24, 0);
        }else {
            for (int i = 0; i< picsArray.count; i++) {
                UIImageView *imageView = [UIImageView new];
                [imageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(picsArray[i])] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
                imageView.frame = CGRectMake((i%3)* (imageWH + margin), i/3* (imageWH + margin), imageWH, imageWH);
                imageView.layer.cornerRadius = 4;
                imageView.layer.masksToBounds = YES;
                imageView.contentMode =  UIViewContentModeScaleAspectFill;
                imageView.tag = i;
                imageView.userInteractionEnabled = YES;
                [self.sourceView addSubview:imageView];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
                [imageView addGestureRecognizer:tap];
                
                if (picsArray.count - 1 == i) {
                    self.sourceView.frame = CGRectMake(12,height + 12,self.mainView.mj_w - 24, MaxY(imageView));
                }
            }
            height = MaxY(self.sourceView);
        }
        
    }else if (model.type.intValue == 1){//视频
       
        if (model.video.length > 0) {
            CGFloat imageW = (self.mainView.mj_w - 24)/2;
            CGFloat imageH = imageW * 101/160.;
            UIImageView *videoImageView = [[UIImageView alloc] init];
            videoImageView.frame = CGRectMake(12, 0, imageW, imageH);
            videoImageView.layer.cornerRadius = 4;
            videoImageView.layer.masksToBounds = YES;
            videoImageView.userInteractionEnabled = YES;
            self.videoImageView = videoImageView;
            [self.sourceView addSubview:videoImageView];
            
            UIImageView *videoPlayImageView = [[UIImageView alloc] init];
            videoPlayImageView.frame = videoImageView.bounds;
            videoPlayImageView.layer.cornerRadius = 4;
            videoPlayImageView.layer.masksToBounds = YES;
            videoPlayImageView.image = [UIImage imageNamed:@"xp_video_paly"];
            [videoImageView addSubview:videoPlayImageView];
            
            videoImageView.image = [self getVideoPreViewImageWithPath:[NSURL URLWithString:model.video]];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoTap:)];
            [videoImageView addGestureRecognizer:tap];
            
            self.sourceView.frame = CGRectMake(12,height + 12 ,self.mainView.mj_w - 24, imageH);
            height = MaxY(self.sourceView);
            
        }else {
            self.sourceView.frame = CGRectMake(12,MaxY(self.descLabel) + 12 ,self.mainView.mj_w - 24, 0);
        }
    }
    
    self.sendButton.frame = CGRectMake(12, height + 12, 117, 32);
    
    //转发评论点赞
    self.activeView.frame = CGRectMake(MaxX(self.sendButton), self.sendButton.mj_y,self.mainView.mj_w - MaxX(self.sendButton) - 12, self.sendButton.mj_h);
    
    NSArray *activityArray = @[
        @{@"image":@"xp_circle_comment",@"num" : model.commentNum },
        @{@"image":@"xp_circle_like",@"num" : model.likeNum },
        @{@"image":@"xp_circle_share",@"num" : model.shareNum },
    ];
    [self.activeView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGFloat right = self.activeView.mj_w;
    for (int i = 0 ; i< activityArray.count; i++) {
        NSDictionary *dic = activityArray[i];
        NSString *numStr = [NSString stringWithFormat:@"%@",dic[@"num"]];
        CGFloat numW = [numStr sizeWithAttributes:@{NSFontAttributeName:kFontRegular(10)}].width;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(right - numW - 24 - 12, 4, numW + 24 + 12, 24);
        button.tag = i;
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.activeView addSubview:button];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 0, 24, 24)];
        imageView.image = [UIImage imageNamed:dic[@"image"]];
        [button addSubview:imageView];
        
        UILabel *label = [UILabel new];
        label.font = kFontRegular(10);
        label.textColor = app_font_color_8A;
        label.textAlignment = NSTextAlignmentRight;
        label.text = numStr;
        label.frame = CGRectMake(0, 8, button.mj_w , 16);
        [button addSubview:label];
        
        right = button.mj_x;
        
    }
    
    height = MaxY(self.sendButton);

    [self.likeView.subviews  makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (model.circleCommentLikeList.count > 0) {
        self.likeView.frame = CGRectMake(12, height + 12, self.mainView.mj_w - 24, 28);
        UILabel *label1 = [UILabel new];
        label1.font = kFontRegular(14);
        label1.textColor = app_font_color;
        label1.text = @"点赞评论";
        label1.frame = self.likeView.bounds;
        [self.likeView addSubview:label1];
        
        UILabel *label2 = [UILabel new];
        label2.font = kFontRegular(14);
        label2.textColor = app_font_color;
        label2.text = @"更多";
        label2.textAlignment = NSTextAlignmentRight;
        [self.likeView addSubview:label2];
        
        NSMutableArray *array = [NSMutableArray array];
        CGFloat left = [label1.text sizeWithAttributes:@{NSFontAttributeName:label1.font}].width + 8;
        label1.frame = CGRectMake(0, 0, left, self.likeView.mj_h);
        
        for (int i = 0; i < model.circleCommentLikeList.count; i++) {
            
            NSDictionary *dic = model.circleCommentLikeList[i];
            UIImageView *iconImageView = [UIImageView new];
            iconImageView.layer.cornerRadius = self.likeView.mj_h/2.;
            iconImageView.layer.borderColor = UIColor.whiteColor.CGColor;
            iconImageView.layer.borderWidth = 1;
            iconImageView.layer.masksToBounds = YES;
            iconImageView.frame = CGRectMake(left + 24*i, 0, self.likeView.mj_h, self.likeView.mj_h);
            [iconImageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(dic[@"head"])] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
            [self.likeView addSubview:iconImageView];
            [array addObject:iconImageView];
            
            label2.frame = CGRectMake(MaxX(iconImageView) + 8, 0, 30, self.likeView.mj_h);
            if (MaxX(iconImageView) > self.likeView.mj_w - 30 - 8) {
                break;
            }
        }
        //设置z - index
        [array enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj = (UIView *)obj;
            [self.likeView bringSubviewToFront:obj];
        }];
        
        
        
        height = MaxY(self.likeView);
        
    }else {
        self.likeView.frame = CGRectMake(12, height + 12, self.mainView.mj_w - 24, 0);
    }
    
    
    
    self.mainView.frame = CGRectMake(12, 12, SCREEN_WIDTH - 12*2, height + 12);
    
    return MaxY(self.mainView) + 12;
}

//图片点击
- (void)tap:(UITapGestureRecognizer *)tap {
    buttonCanUseAfterOneSec(tap.view);
    
    self.photos = [NSMutableArray array];

    NSArray *circleImagesList = [self.model.pics componentsSeparatedByString:@","];
    for (int i = 0; i<circleImagesList.count; i++) {
        GKPhoto *photo = [GKPhoto new];
        photo.url = [NSURL URLWithString:TO_STR(circleImagesList[i])];
        [self.photos addObject:photo];
    }
    GKPhotoBrowser *browser = [GKPhotoBrowser photoBrowserWithPhotos:self.photos currentIndex:tap.view.tag];
    [browser showFromVC:[XPCommonTool getCurrentViewController]];
}
//视频点击
- (void)videoTap:(UITapGestureRecognizer *)tap {
    buttonCanUseAfterOneSec(tap.view);
    
    [[XPOpenSourceTool shareInstance] openVideoControllerWithUrl:self.model.video];
}

- (void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.itemTapBlock) {
        self.itemTapBlock(sender.tag);
    }
    
}

-(void)likeButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.likeButtonBlock) {
        self.likeButtonBlock();
    }
}

- (void)sendButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.commentBlock) {
        self.commentBlock();
    }
}



////获取视频的第一帧截图, 返回UIImage
////需要导入AVFoundation.h
- (UIImage*) getVideoPreViewImageWithPath:(NSURL *)videoPath
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoPath options:nil];

    AVAssetImageGenerator *gen         = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;

    CMTime time      = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error   = nil;

    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *img     = [[UIImage alloc] initWithCGImage:image];

    return img;
}

@end
