//
//  XPCircleCommentCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPCircleCommentCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

-(CGFloat)cellWithDic:(NSDictionary *)dic;
//删除评论
@property (nonatomic, copy) void(^deleteCommentBlock)();

@end

NS_ASSUME_NONNULL_END
