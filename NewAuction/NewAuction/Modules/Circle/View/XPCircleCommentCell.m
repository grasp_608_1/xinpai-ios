//
//  XPCircleCommentCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/19.
//

#import "XPCircleCommentCell.h"

@interface XPCircleCommentCell ()

@property (nonatomic, strong) UIView *commentView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *label;

@property (nonatomic, strong) NSDictionary *dataDic;
@end

@implementation XPCircleCommentCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    
    static NSString *cellID = @"XPCircleCommentCell";
    XPCircleCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPCircleCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    self.backgroundColor = UIColorFromRGB(0xFAFAFA);
    
    UIView *commentView = [[UIView alloc] initWithFrame:CGRectMake(12, 0, SCREEN_WIDTH - 24, 0)];
    commentView.layer.cornerRadius = 8;
    commentView.backgroundColor = UIColor.whiteColor;
    self.commentView = commentView;
    [self.contentView addSubview:commentView];
    
    UILongPressGestureRecognizer *longGes = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longGes:)];
    [commentView addGestureRecognizer:longGes];
    
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12,12, 36, 36)];
    iconImageView.layer.cornerRadius = 18;
    iconImageView.layer.masksToBounds = YES;
    self.iconImageView = iconImageView;
    [self.commentView addSubview:iconImageView];
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    bgView.layer.cornerRadius = 4;
    bgView.frame = CGRectMake(52, 12, self.commentView.mj_w - 52 - 12, 0);
    self.bgView = bgView;
    [self.commentView addSubview:bgView];
    
    UILabel *label = [UILabel new];
    label.font = kFontRegular(14);
    label.textColor = app_font_color;
    label.numberOfLines = NO;
    label.frame = CGRectMake(7, 4, bgView.mj_w - 14, 0);
    self.label = label;
    [bgView addSubview:label];
    
}

-(CGFloat)cellWithDic:(NSDictionary *)dic {
    
    _dataDic = dic;
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(dic[@"head"])] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    
    CGFloat bottom = 0;
    NSString *content = TO_STR(dic[@"content"]);
    NSString *nickName = TO_STR(dic[@"nickname"]);
    NSString *str = [NSString stringWithFormat:@"%@: %@",nickName,content];
    
    CGFloat commentItemH = [str boundingRectWithSize:CGSizeMake(self.bgView.mj_w - 14 , CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:kFontRegular(14)} context:nil].size.height;
    
    self.label.frame = CGRectMake(7, 4, self.bgView.mj_w - 14, commentItemH);
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:str];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(14),NSForegroundColorAttributeName:UIColorFromRGB(0x6E73AF)} range:NSMakeRange(0, nickName.length)];
    self.label.attributedText = attr;
    
    self.bgView.frame = CGRectMake(52, 12 + bottom, self.commentView.mj_w - 52 - 12, commentItemH + 8);
    
    if (MaxY(self.bgView) > MaxY(self.iconImageView)) {
        bottom = MaxY(self.bgView);
    }else {
        bottom = MaxY(self.iconImageView);
    }

    self.commentView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 24, bottom + 12);
    
    return bottom;
}

- (void)longGes:(UILongPressGestureRecognizer *)longGes {
    buttonCanUseAfterOneSec(longGes.view);
    
    if (longGes.state  == UIGestureRecognizerStateEnded) {
        
        NSNumber *isSelf = self.dataDic[@"isSelf"];
        if (isSelf.intValue == 1) {
            if (self.deleteCommentBlock) {
                self.deleteCommentBlock();
            }
        }else {
            return;
        }
    }
   
    
    
}

@end
