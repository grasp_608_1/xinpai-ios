//
//  XPPublishSourceView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/15.
//

#import "XPPublishSourceView.h"
#import <Photos/PHPhotoLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import "UIImage+Compression.h"
#import "XPHitButton.h"
#import "AFNetworking.h"
#import "XPOSSUploadTool.h"
#import "WEPhotoAlbum.h"


#define VideoMaxMBSize  10

@interface XPPublishSourceView ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, assign) NSInteger maxCount;
//添加图片按钮
@property (nonatomic, strong) UIImageView *defaultView;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) NSMutableArray *imagesArray;

@property (nonatomic, strong) XPBaseViewController *controller;
//已选择的图片
@property (nonatomic, strong) UIImage *chooseImage;
//已选择的视频地址
@property (strong, nonatomic) NSURL   *videoURL;
//上传视频的首图
@property (nonatomic, strong) UIImage *videoFirstImage;
//视频删除
@property (nonatomic, strong) XPHitButton *videoDeleteButton;
// 0 图片 1 视频
@property (nonatomic, assign) NSInteger type;
//但上传图片列表
@property (nonatomic, strong) NSMutableArray *waitForUploadArray;

@end

@implementation XPPublishSourceView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.controller = (XPBaseViewController *)[XPCommonTool getCurrentViewController];
    self.dataArray = [NSMutableArray array];
    self.imagesArray = [NSMutableArray array];
    self.waitForUploadArray = [NSMutableArray array];
    
    self.maxCount = 3;
    CGFloat imageWH = (self.mj_w - (self.maxCount - 1)* 12)/self.maxCount;
    
    self.defaultView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imageWH, imageWH)];
    self.defaultView.tag = 999;
    self.defaultView.userInteractionEnabled = YES;
    self.defaultView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [self addSubview:self.defaultView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.defaultView addGestureRecognizer:tap];
    
}

- (void)initWithType:(NSInteger)sourceType maxCount:(NSInteger)max {
    self.maxCount = max;
    self.type = sourceType;
    
    if (sourceType == 0){
        self.defaultView.image = [UIImage imageNamed:@"xp_image_upload"];
    }else {
        self.defaultView.image = [UIImage imageNamed:@"xp_video_upload"];
    }
}

- (void)reloadUI{
    
    CGFloat imageWH = (self.mj_w - 2* 12)/3;
    
    for (UIView  *view in self.subviews) {
        if (view.tag != 999) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i<self.dataArray.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] init];
        [self addSubview:imageView];
        imageView.layer.cornerRadius = 8;
        imageView.layer.masksToBounds = YES;
        imageView.frame = CGRectMake((imageWH + 12) * (i%3),(i/3)*(imageWH + 12) , imageWH, imageWH);
        
        XPHitButton *button = [[XPHitButton alloc] init];
        button.frame = CGRectMake(MaxX(imageView) - 20 , imageView.mj_y, 20, 20);
        button.hitRect = 40;
        button.tag = i;
        [button addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [button setImage:[UIImage imageNamed:@"xp_image_delete"] forState:UIControlStateNormal];
        [self addSubview:button];
        
        imageView.image = self.dataArray[i];
    
    }
    
    if (self.dataArray.count >= self.maxCount) {
        self.defaultView.hidden = YES;
    }else {
        self.defaultView.hidden = NO;
        self.defaultView.frame = CGRectMake((imageWH + 12) * (self.dataArray.count %3), (self.dataArray.count/3)*(imageWH + 12), imageWH, imageWH);
    }
    
    if (self.frameChange) {
        self.frameChange(MaxY(self.defaultView) + 12);
    }
    
}

- (void)deleteButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self.dataArray removeObjectAtIndex:sender.tag];
    [self.imagesArray removeObjectAtIndex:sender.tag];
    if (self.changeBlock) {
        self.changeBlock(self.imagesArray);
    }
    [self reloadUI];
}

- (void)tap:(UIGestureRecognizer *)tap {
    buttonCanUseAfterOneSec(tap.view);
    XPWeakSelf
    [[XPVerifyTool shareInstance] phoneAlbumCompleteBlock:^{
        if (weakSelf.type == 0) {
            [weakSelf openAlbum];
        }else if (weakSelf.type == 1){
            [weakSelf openVideo];
        }
    }];
}


- (void)openVideo {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.navigationBar.translucent = NO;
    imagePickerController.mediaTypes = @[(NSString *)kUTTypeMovie];// 只选择视频
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary; // 从相册选择
    
    [self.controller presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}

- (void)openCamera {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.navigationBar.translucent = NO;
    [self.controller presentViewController:imagePickerController animated:true completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }];
}
- (void)openAlbum {
    NSArray *albums = [[WEAlbumHandler shareInstance] getAllPhotoAlbumList];
    WEPhotoAlbumListModel *albumListModel = albums[0];
    WEThumbnailViewController *thumbnailVC = [[WEThumbnailViewController alloc] initWithTitle:albumListModel.title maxSelectCount:9 - self.imagesArray.count assetCollection:albumListModel.assetCollection];
    thumbnailVC.modalPresentationStyle  = UIModalPresentationOverFullScreen;
    [self.controller presentModalViewController:thumbnailVC animated:YES];
    XPWeakSelf
    thumbnailVC.assetCompleteBlock = ^(NSArray *images) {
        
        weakSelf.waitForUploadArray = images.mutableCopy;
        [weakSelf.controller startLoadingGif];
        [weakSelf uploadImageArray];
    };
}

//图片,视频上传
- (void)uploadSource:(NSString *)filePath type:(XPMediaType)type image:(UIImage *)image{
    
    if (XPMediaTypeVideo == type) {
        [self.controller showProgressView];
    }
    
    
    [[XPOSSUploadTool shareInstance]uploadFileWithMediaType:type filePath:filePath progress:^(CGFloat progress) {
        if (XPMediaTypeVideo == type) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.controller.progress = progress;
            });
        }
    } completeBlock:^(BOOL success, NSString * _Nonnull remoteUrlStr) {
        
        NSLog(@"remoteurl ----- >%@",remoteUrlStr);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (XPMediaTypeVideo == type) {
                [self.controller hideProgressView];
            }
            if (success) {
                if (type == XPMediaTypeImage) {
                    [self.dataArray addObject:image];
                    [self.imagesArray addObject:remoteUrlStr];
                    [self.waitForUploadArray removeObjectAtIndex:0];
                    [self uploadImageArray];
                    
                }else if (type == XPMediaTypeVideo){
//                    self.defaultView.image = self.videoFirstImage;
                    [self.dataArray removeAllObjects];
                    [self.dataArray addObject:self.videoFirstImage];
                    
                    [self.imagesArray removeAllObjects];
                    [self.imagesArray addObject:remoteUrlStr];
                    
                    if (self.changeBlock) {
                        self.changeBlock(self.imagesArray);
                    }
                    [[XPShowTipsTool shareInstance] showMsg:@"视频上传成功" ToView:k_keyWindow];
                }
                [self reloadUI];
            }else {
                if (XPMediaTypeImage == type){
                    [self.controller stopLoadingGif];
                }
                [[XPShowTipsTool shareInstance] showMsg:@"上传失败,请稍后重试" ToView:k_keyWindow];
            }
        });
    }];
}

//多图上传
- (void)uploadImageArray{
    
    if (self.waitForUploadArray.count > 0) {
        
        UIImage *image = self.waitForUploadArray.firstObject;
        //上传
        NSString *filePath = [self saveImageToSandbox:image];
        [self uploadSource:filePath type:XPMediaTypeImage image:image];
    }else{
        [self.controller stopLoadingGif];
        if (self.changeBlock) {
            self.changeBlock(self.imagesArray);
        }
    }
}


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    XPWeakSelf
    // 获取所选媒体的类型
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];

    if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
        // 处理视频
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        // 做其他操作，比如播放视频或保存视频
        NSLog(@"选择的视频URL: %@", videoURL);
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];//用时间给文件全名，以免重复，在测试的时候其实可以判断文件是否存在若存在，则删除，重新生成文件即可
        [formater setDateFormat:@"yyyy_MM_dd_HHmmss"];
        NSURL  *newVideoUrl = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingFormat:@"/Documents/output-%@.mp4", [formater stringFromDate:[NSDate date]]]] ;//这个是保存在app自己的沙盒路径里，后面可以选择是否在上传后删除掉。我建议删除掉，免得占空间。
        
        [self convertVideoQuailtyWithInputURL:videoURL outputURL:newVideoUrl completeHandler:^{
            [picker dismissViewControllerAnimated:YES completion:^{
                //获取当前视频大小
                double sizeMB = [self getFileSize:newVideoUrl.path];
                if (sizeMB > 10) {
                    [[XPShowTipsTool shareInstance] showMsg:@"压缩后的文件过大,请重新上传!" ToView:k_keyWindow During:4];
                    
                }else {
                    [weakSelf uploadSource:newVideoUrl.path type:XPMediaTypeVideo image:nil];
                }
                
                
            }];
        }];
        
    }else if ([mediaType isEqualToString:@"public.image"]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        //压缩图片
        image = [image compressToImage];

        NSString *filePath = [self saveImageToSandbox:image];
        
        //单图上传
        [picker dismissViewControllerAnimated:YES completion:^{
            [weakSelf uploadSource:filePath type:XPMediaTypeImage image:image];
        }];
    }
  
}

//图片写入本地
- (NSString *)saveImageToSandbox:(UIImage *)image {
    // 1. 将 UIImage 转换为 NSData (保存为 PNG 格式)
    NSData *imageData = UIImagePNGRepresentation(image);
    
    if (imageData == nil) {
        NSLog(@"图片转换失败");
        return nil;
    }
    // 2. 获取沙盒 Documents 目录路径
    NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    // 3. 创建保存文件的路径 (使用文件名来保存图片)
    NSString *filePath = [documentDirectory stringByAppendingPathComponent:@"saved_image.png"];
    NSError *err;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        BOOL remove = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&err];
        if (remove) {
            NSLog(@"已存在,成功移除图片");
        } else {
            NSLog(@"移除失败: %@", err.localizedDescription);
        }
    }
    // 4. 将图片数据写入文件
    NSError *error;
    BOOL success = [imageData writeToFile:filePath options:NSDataWritingAtomic error:&error];
    
    if (success) {
        NSLog(@"图片已成功保存到路径: %@", filePath);
        return filePath;
    } else {
        NSLog(@"保存图片失败: %@", error.localizedDescription);
        return nil;
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(NSArray *)urlImagesArray{
    
    return self.imagesArray.copy;
}

//视频处理方法
- (void) convertVideoQuailtyWithInputURL:(NSURL*)inputURL
                               outputURL:(NSURL*)outputURL
                         completeHandler:(void (^)(void))handler
{
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:avAsset presetName:AVAssetExportPresetMediumQuality];
    //  NSLog(resultPath);
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.shouldOptimizeForNetworkUse= YES;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         switch (exportSession.status) {
             case AVAssetExportSessionStatusCancelled:
                 NSLog(@"AVAssetExportSessionStatusCancelled");
                 break;
             case AVAssetExportSessionStatusUnknown:
                 NSLog(@"AVAssetExportSessionStatusUnknown");
                 break;
             case AVAssetExportSessionStatusWaiting:
                 NSLog(@"AVAssetExportSessionStatusWaiting");
                 break;
             case AVAssetExportSessionStatusExporting:
                 NSLog(@"AVAssetExportSessionStatusExporting");
                 break;
             case AVAssetExportSessionStatusCompleted:
             {
                 NSLog(@"output url ---->%@",[outputURL absoluteString]);
                 
                 NSLog(@"AVAssetExportSessionStatusCompleted");
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     self.videoFirstImage = [self getVideoPreViewImageWithPath:outputURL];
                     
                 });
                 
                 self.videoURL = outputURL;
                 dispatch_async(dispatch_get_main_queue(), handler);

             }
                 break;
             case AVAssetExportSessionStatusFailed:
                 NSLog(@"AVAssetExportSessionStatusFailed");
                 break;
         }
         
     }];
    
}

////获取视频的第一帧截图, 返回UIImage
////需要导入AVFoundation.h
- (UIImage*) getVideoPreViewImageWithPath:(NSURL *)videoPath
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoPath options:nil];

    AVAssetImageGenerator *gen         = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;

    CMTime time      = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error   = nil;

    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *img     = [[UIImage alloc] initWithCGImage:image];

    return img;
}

- (unsigned long long)getFileSize:(NSString *)filePath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error = nil;
    
    // 获取文件的属性
    NSDictionary *fileAttributes = [fileManager attributesOfItemAtPath:filePath error:&error];
    
    if (error) {
        NSLog(@"Error reading file attributes for %@: %@", filePath, error.localizedDescription);
        return 0;
    }
    
    // 获取文件大小
    NSNumber *fileSize = [fileAttributes objectForKey:NSFileSize];
    
    return [fileSize unsignedLongLongValue]/(1024.0 * 1024.0);
}
@end
