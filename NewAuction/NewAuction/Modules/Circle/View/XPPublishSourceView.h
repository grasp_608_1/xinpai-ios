//
//  XPPublishSourceView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPPublishSourceView : UIView

- (void)initWithType:(NSInteger)sourceType maxCount:(NSInteger)max;

@property (nonatomic, copy) void(^changeBlock)(NSArray *urlArray);

@property (nonatomic, copy) void(^frameChange)(CGFloat maxHeight);

@end

NS_ASSUME_NONNULL_END
