//
//  XPCircleDetailView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/19.
//

#import <UIKit/UIKit.h>
#import "XPCircleListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPCircleDetailView : UIView

-(CGFloat)ViewWithModel:(XPCircleListModel *)model;
//0 转发 1 点赞 2 评论
@property (nonatomic, copy) void(^itemTapBlock)(NSInteger tag);
//0 转发 1 点赞 2 评论
@property (nonatomic, copy) void(^commentBlock)(void);
@property (nonatomic, copy) void(^likeButtonBlock)(void);

@end

NS_ASSUME_NONNULL_END
