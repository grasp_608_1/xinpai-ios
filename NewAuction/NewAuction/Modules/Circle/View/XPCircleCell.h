//
//  XPCircleCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/14.
//

#import <UIKit/UIKit.h>
#import "XPCircleListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPCircleCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

-(CGFloat)cellWithModel:(XPCircleListModel *)model;

//0 转发 1 点赞 2 评论
@property (nonatomic, copy) void(^itemTapBlock)(NSInteger tag);
//评论点击
@property (nonatomic, copy) void(^commentBlock)(void);
//折叠
@property (nonatomic, copy) void(^foldButtonBlock)(void);
//头像点击
@property (nonatomic, copy) void(^headTapBlock)(void);
//评论删除
@property (nonatomic, copy) void(^deleteCommentBlock)(NSInteger index);
//视频播放
@property (nonatomic, copy) void(^videoBlock)(NSString *videoUrlStr);

@end

NS_ASSUME_NONNULL_END
