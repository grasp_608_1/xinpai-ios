//
//  XPCircleViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/14.
//

#import "XPCircleViewController.h"
#import "XPMainViewController.h"
#import "XPHitButton.h"
#import "XPCircleSelectTagController.h"
#import "XPCirclePublishController.h"
#import "XPShopSortItemView.h"
#import "XPSearchDefaultView.h"
#import "XPSlideItemView.h"
#import "XPCircleCell.h"
#import "XPCircleListModel.h"
#import "XPCircleDetailViewController.h"
#import "XPCreationViewController.h"
#import "XPExtraContentView.h"
#import "XPShareUMView.h"
#import "XPUMShareTool.h"
#import "XPEmptyView.h"
#import "XPOpenSourceTool.h"
#import "XPBaseWebViewViewController.h"

@interface XPCircleViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIView *topView;
//顶部返回按钮
@property (nonatomic, strong) XPHitButton *backButton;
//顶部背景图
@property (nonatomic, strong) UIImageView *topBanner;
//用户头像
@property (nonatomic, strong) UIImageView *userIcon;

@property (nonatomic, strong) UILabel *userNickNameLabel;
//顶部返回按钮
@property (nonatomic, strong) XPHitButton *attentionButton;
//设置按钮
@property (nonatomic, strong) XPHitButton *commonSetButton;
//3d 按钮
@property (nonatomic, strong) XPHitButton *creationButton;
//发布按钮
@property (nonatomic, strong) XPHitButton *publishButton;

@property (nonatomic, strong) UIView *signsView;
//热点 最新 圈子
@property (nonatomic, strong) XPShopSortItemView *selectItemView;
//全部 点赞 评论
@property (nonatomic, strong) XPSlideItemView *slideView;
//列表
@property (nonatomic, strong) UITableView *tableView;
//无数据
@property (nonatomic, strong) XPEmptyView *emptyView;
//搜索遮罩
@property (nonatomic, strong) UIView *searchBgView;
//顶部数据
@property (nonatomic, copy)   NSDictionary *topDic;
//是否展示造物
@property (nonatomic, strong) NSNumber *isShow;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//cell高度
@property (nonatomic, strong) NSArray *heightArray;
//当前页码
@property (nonatomic, assign) NSInteger page;
//发表评论
@property (nonatomic, strong) XPExtraContentView *extraTitle;
//全部 点赞 评论 index
@property (nonatomic, assign) NSInteger currentIndex;
//类型 0 热点 1 最新 2 我的(他的)
@property (nonatomic, assign) NSInteger businessIndex;
//关键字
@property (nonatomic, copy) NSString *keyWords;

@property (nonatomic, assign) BOOL isOpenVideo;

@end

@implementation XPCircleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    [self setupViews];
    self.view.backgroundColor = UIColorFromRGB(0xF7F8FB);
    self.page = 1;
    self.currentIndex = 1;
    self.keyWords = @"";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(freshData) name:Notification_user_login_success object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (![[XPVerifyTool shareInstance]checkLogin] ) {
        [[XPVerifyTool shareInstance] userLoginFirstHandler];
    }else if (self.isOpenVideo){
        self.isOpenVideo = NO;
    }else {
        
        [self freshData];
        
    }
}

- (void)freshData {
    self.page = 1;
    [self getData];
    [self getTopData];
}


- (void)setupViews {
    
    UIView *topView = [[UIView alloc]init];
    topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 139 + kTopHeight);
    topView.clipsToBounds = YES;
    topView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.topView = topView;
    [self.view addSubview:topView];
    
    UIImageView *topBanner = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.topView.mj_w, kTopHeight + 139)];
    self.topBanner = topBanner;
    topBanner.image = [UIImage imageNamed:@"xp_circle_topbg"];
    [self.topView addSubview:topBanner];
    
    if (self.entryType == 0){
        XPSearchDefaultView *searchView = [[XPSearchDefaultView alloc] initWithFrame:CGRectMake(16, self.safeTop + 8,SCREEN_WIDTH - 16 - 112, 40)];
        searchView.backgroundColor = UIColor.whiteColor;
        searchView.searchIcon.image = [UIImage imageNamed:@"xp_search_black"];
        searchView.canSearch = YES;
        [self.topView addSubview:searchView];
       
        XPWeakSelf
        [searchView setSearchBlock:^(NSString * _Nonnull keyStr) {
            [weakSelf.view endEditing:YES];
            
            weakSelf.dataArray = @[].mutableCopy;
            weakSelf.keyWords = keyStr;
            weakSelf.page = 1;
            [weakSelf getData];
            
        }];
        
        [searchView setSearchStateBlock:^(BOOL state) {
            if (state) {
                weakSelf.tableView.hidden = YES;
                weakSelf.searchBgView.hidden = NO;
                weakSelf.emptyView.hidden = YES;
            }else{
//                weakSelf.topView.hidden = NO;
                weakSelf.searchBgView.hidden = YES;
                [weakSelf canShowEmptyView];
            }
        }];
        
    }else if (self.entryType == 1 || self.entryType == 2) {
        self.backButton = [XPHitButton new];
        self.backButton.hitRect = 40;
        self.backButton.frame = CGRectMake( 16, 16 + self.safeTop, 24, 24);
        [self.backButton setImage:[UIImage imageNamed:@"back_gray"] forState:UIControlStateNormal];
        self.backButton.layer.cornerRadius = 0;
        [self.backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        [self.topView addSubview:self.backButton];
        
        XPSearchDefaultView *searchView = [[XPSearchDefaultView alloc] initWithFrame:CGRectMake(52, self.safeTop + 8,SCREEN_WIDTH - 112 - 52, 40)];
        searchView.backgroundColor = UIColor.whiteColor;
        searchView.searchIcon.image = [UIImage imageNamed:@"xp_search_black"];
        searchView.canSearch = YES;
        [self.topView addSubview:searchView];
    }
    
    self.commonSetButton = [[XPHitButton alloc] init];
    self.commonSetButton.hitRect = 40;
    [self.commonSetButton setImage:[UIImage imageNamed:@"setting_set_log"] forState:UIControlStateNormal];
    self.commonSetButton.frame = CGRectMake(self.topView.mj_w - 24 - 24, self.safeTop + 16, 24, 24);
    [self.commonSetButton addTarget:self action:@selector(commonSetButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.commonSetButton];
    
    self.creationButton = [[XPHitButton alloc] init];
    self.creationButton.hitRect = 40;
    if (APPTYPE == 0) {
        [self.creationButton setImage:[UIImage imageNamed:@"xp_create_icon"] forState:UIControlStateNormal];
    }else if (APPTYPE == 1){
        self.creationButton.layer.cornerRadius = 4;
        self.creationButton.layer.masksToBounds = YES;
        self.creationButton.hidden = YES;
        [self.creationButton setImage:[UIImage imageNamed:@"xp_auction_logo"] forState:UIControlStateNormal];
    }
    
    self.creationButton.frame = CGRectMake(self.topView.mj_w - 16 - 72, self.safeTop + 16, 24, 24);
    [self.creationButton addTarget:self action:@selector(creationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.creationButton];
    
    
    self.userIcon = [UIImageView new];
    self.userIcon.frame = CGRectMake(20, kTopHeight + 24, 48, 48);
    self.userIcon.layer.cornerRadius = 24;
    self.userIcon.layer.masksToBounds = YES;
    self.userIcon.layer.borderColor = [UIColor whiteColor].CGColor;
    self.userIcon.layer.borderWidth = 1;
    [self.topView addSubview:self.userIcon];
    
    self.userNickNameLabel = [UILabel new];
    self.userNickNameLabel.font = kFontMedium(18);
    self.userNickNameLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.userNickNameLabel.frame = CGRectMake(MaxX(self.userIcon) + 10, self.userIcon.frame.origin.y, 200, 24);
    [self.topView addSubview:self.userNickNameLabel];
    
    
    if (self.entryType == 0 || self.entryType == 1) {
        self.publishButton = [[XPHitButton alloc] init];
        self.publishButton.frame = CGRectMake(self.topView.mj_w - 20 - 64, kTopHeight + 32, 64, 32);
        self.publishButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
        self.publishButton.layer.cornerRadius = 16;
        [self.publishButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        [self.publishButton setTitle:@"发布" forState:UIControlStateNormal];
        self.publishButton.titleLabel.font = kFontRegular(14);
        [self.publishButton addTarget:self action:@selector(publishButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.topView addSubview:self.publishButton];
    }else {
        self.publishButton = [[XPHitButton alloc] init];
        self.publishButton.frame = CGRectMake(self.topView.mj_w - 20 - 102, kTopHeight + 32, 102, 32);
        self.publishButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
        self.publishButton.layer.cornerRadius = 16;
        [self.publishButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        [self.publishButton setTitle:@"返回我的圈子" forState:UIControlStateNormal];
        self.publishButton.titleLabel.font = kFontRegular(14);
        [self.publishButton addTarget:self action:@selector(publishButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.topView addSubview:self.publishButton];
        
        self.attentionButton = [[XPHitButton alloc] init];
        self.attentionButton.backgroundColor = UIColorFromRGB(0xEEEEFF);
        self.attentionButton.layer.cornerRadius = 10;
        self.attentionButton.frame = CGRectMake(0, 0, 48, 0);
        [self.attentionButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
        self.attentionButton.titleLabel.font = kFontRegular(12);
        [self.attentionButton addTarget:self action:@selector(attentionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.topView addSubview:self.attentionButton];
        
    }
    
    self.signsView = [[UIView alloc] init];
    self.signsView.frame = CGRectMake(self.userNickNameLabel.mj_x, MaxY(self.userNickNameLabel) + 5, self.publishButton.mj_x - self.userNickNameLabel.mj_x - 12 , 20);
    [self.topView addSubview:self.signsView];
    
    [self freshTopData];
    
    XPShopSortItemView *selectItemView = [[XPShopSortItemView alloc] initWithFrame:CGRectMake(12, MaxY(self.userIcon) + 24, SCREEN_WIDTH - 36, 35)];
    [self.topView addSubview:selectItemView];
    selectItemView.itemSpace = 24;
    self.selectItemView = selectItemView;
    
    
    self.slideView = [[XPSlideItemView alloc] initWithFrame:CGRectMake(20, MaxY(self.topBanner) + 6, SCREEN_WIDTH, 40)];
    self.slideView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self.topView addSubview:self.slideView];
    
    if (self.entryType == 0 || self.entryType == 1) {
        [self.selectItemView configWithtitleArray:@[
            @{ @"name":@"热点"},
            @{ @"name":@"最新"},
            @{@"name":@"我的圈子"}
        ]];
    }else if (self.entryType == 2){
        [self.selectItemView configWithtitleArray:@[
            @{ @"name":@"热点"},
            @{ @"name":@"最新"},
            @{@"name":@"他的圈子"}
        ]];
    }
    if (self.entryType == 0 || self.entryType == 1) {
        [self.slideView configWithtitleArray:@[@"全部",@"我的点赞",@"我的评论"]];
        self.slideView.hidenDownLine = YES;
    }else if (self.entryType == 2){
        [self.slideView configWithtitleArray:@[@"全部",@"他的点赞",@"他的评论"]];
        self.slideView.hidenDownLine = YES;
    }
    
    [self callBack];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    if (self.entryType == 0 || self.entryType == 1) {
        self.tableView.frame = CGRectMake(0, MaxY(self.topView), SCREEN_WIDTH, SCREEN_HEIGHT - self.topView.mj_h - kTabBarHeight);
    }else if (self.entryType == 2){
        self.tableView.frame = CGRectMake(0, MaxY(self.topView), SCREEN_WIDTH, SCREEN_HEIGHT - self.topView.mj_h );
    }
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 12)];
    [self.view addSubview:self.tableView];
     
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, self.tableView.mj_y + 100, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无数据"];
    [self.view addSubview:self.emptyView];
    
    UIView *searchBgView = [[UIView alloc] init];
    searchBgView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    searchBgView.frame = CGRectMake(0, kTopHeight + 12, SCREEN_WIDTH, SCREEN_HEIGHT);
    self.searchBgView = searchBgView;
    self.searchBgView.hidden = YES;
    [self.view addSubview:searchBgView];
    
    XPWeakSelf
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf headerRefresh];
    }];
}


-(void)freshTopData {
    
    if (APPTYPE == 1) {
        NSNumber *isShow = self.isShow;
        //0 显示,  1 隐藏
        if (isShow.intValue == 0) {
            self.creationButton.hidden = YES;
        }else {
            self.creationButton.hidden = NO;
        }
    }
    
    NSArray *titleArray = self.topDic[@"circleLabelVOs"];
    [self.signsView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (titleArray.count > 0) {
        CGFloat left = 0;
        for (int i = 0; i<titleArray.count; i++) {
            
            NSDictionary *dic = titleArray[i];
            UILabel *label = [[UILabel alloc] init];
            label.textColor = [XPCommonTool colorWithHex:dic[@"fontColour"]];
            label.font = kFontRegular(12);
            label.layer.cornerRadius = 4;
            label.layer.borderColor = label.textColor.CGColor;
            label.layer.borderWidth = 0.8;
            label.textAlignment = NSTextAlignmentCenter;
            label.text = TO_STR(dic[@"name"]);
            CGFloat labelW = [label.text sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
            label.frame = CGRectMake(left, 1, labelW + 4, 18);
            [self.signsView addSubview:label];
            left = MaxX(label) + 4;
        }
    }
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:TO_STR(self.topDic[@"userHead"])] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    
    self.userNickNameLabel.text = TO_STR(self.topDic[@"userNickname"]);
    CGFloat nameW = [self.userNickNameLabel.text sizeWithAttributes:@{NSFontAttributeName: self.userNickNameLabel.font}].width;
    self.userNickNameLabel.frame = CGRectMake(MaxX(self.userIcon) + 10, self.userIcon.frame.origin.y, nameW, 24);
    [self.topView addSubview:self.userNickNameLabel];
    
    if (self.entryType == 2) {
        self.attentionButton.hidden = NO;
        
        self.attentionButton.frame = CGRectMake(MaxX(self.userNickNameLabel) + 8, self.userNickNameLabel.mj_y + 2, 48, 20);
        NSNumber *status = self.topDic[@"status"];
        if (status.intValue == 1) {
            [self.attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
        }else{
            [self.attentionButton setTitle:@"关注" forState:UIControlStateNormal];
        }
    }else {
        self.attentionButton.hidden = YES;
    }
    
    
}


- (void)callBack{
    XPWeakSelf
    [self.selectItemView setTapBlock:^(NSInteger index) {
        weakSelf.dataArray = @[].mutableCopy;
        [weakSelf getCellHeightArray];
        [weakSelf.tableView reloadData];
        
        weakSelf.currentIndex = index;
        if (index == 1) {//热点
            weakSelf.topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 139 + kTopHeight);
            weakSelf.tableView.frame = CGRectMake(0, MaxY(weakSelf.topView), SCREEN_WIDTH, SCREEN_HEIGHT - weakSelf.topView.mj_h - kTabBarHeight);
            [weakSelf getData];
        }else if(index == 2){//最新
            weakSelf.topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 139 + kTopHeight);
            weakSelf.tableView.frame = CGRectMake(0, MaxY(weakSelf.topView), SCREEN_WIDTH, SCREEN_HEIGHT - weakSelf.topView.mj_h - kTabBarHeight);
            [weakSelf getData];
        }else if(index == 3){//我的圈子
            weakSelf.topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 179 + kTopHeight);
            weakSelf.tableView.frame = CGRectMake(0, MaxY(weakSelf.topView), SCREEN_WIDTH, SCREEN_HEIGHT - weakSelf.topView.mj_h - kTabBarHeight);
            [weakSelf getData];
        }
       /*
        if (weakSelf.entryType == 0) {//圈子
            
            
        }else if (weakSelf.entryType == 1){//我的圈子
            if (index == 1) {//热点
                
            }else if(index == 2){//最新
                
            }else if(index == 3){//我的圈子
                
            }
        }else if (weakSelf.entryType == 2){//他的圈子
            
            if (index == 1) {//热点
                
            }else if(index == 2){//最新
                
            }else if(index == 3){//我的圈子
                
            }
        }
       */
    }];
    
    [self.slideView setTapBlock:^(NSInteger index) {
        weakSelf.businessIndex = index - 1;
        weakSelf.page = 1;
        [weakSelf getData];
    }];
    
}


//获取cell高度
- (void)getCellHeightArray{
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPCircleCell *cell = XPCircleCell.new;
    for (int i = 0; i<self.dataArray.count; i++) {
        XPCircleListModel *model = self.dataArray[i];
        CGFloat height = [cell cellWithModel:model];
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    self.heightArray = tempArray.copy;
}

#pragma mark --- button action
- (void)commonSetButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPCircleSelectTagController *vc = [[XPCircleSelectTagController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)creationButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (APPTYPE == 0) {
        XPCreationViewController *vc = [XPCreationViewController new];
        vc.enterType = 1;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (APPTYPE == 1){
        XPMainViewController *vc = [XPMainViewController new];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

- (void)attentionButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    NSNumber *status = self.topDic[@"status"];
    if (status.intValue == 1 ) {
        [self cancelAttentionWithUserId:self.topDic[@"userId"]];
    }else {
        [self addAttentionWithUserId:self.topDic[@"userId"]];
    }
    
}

//发布按钮
- (void)publishButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.entryType == 2) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    
    if (APPTYPE == 1) {
        NSString *hasRead = [[NSUserDefaults standardUserDefaults] objectForKey:@"readCirlePolicy"];
        if (!hasRead || ![hasRead isEqualToString:@"1"]) {
            
            [self.xpAlertView showAlertWithTitle:@"首次使用圈子功能须阅读同意《圈子用户使用协议》" actionBlock:^(NSString * _Nullable extra) {
                
                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"circle_userAgreement_policy" ofType:@"html"];

                XPBaseWebViewViewController *webView = [XPBaseWebViewViewController new];
                webView.hidesBottomBarWhenPushed = YES;
                webView.webViewType = 1;
                webView.urlString = filePath;
                [self.navigationController pushViewController:webView animated:YES];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"readCirlePolicy"];
                
            }];
            return;
        }
    }

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       
    }];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"从手机相册选择图片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        XPCirclePublishController *vc = [XPCirclePublishController new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.entryType = 0;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"从手机相册选择视频" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        XPCirclePublishController *vc = [XPCirclePublishController new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.entryType = 1;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    [alert addAction:cancel];
    [alert addAction:action];
    [alert addAction:action2];

    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark --- tableview delegete dataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPCircleCell *cell = [XPCircleCell cellWithTableView:tableView];
    XPCircleListModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    
    XPWeakSelf;
    [cell setCommentBlock:^{
        if (model.isComment.intValue == 1) {
            [[XPShowTipsTool shareInstance] showMsg:@"已评论过!" ToView:k_keyWindow];
            return;
        }
        
        XPExtraContentView *extraTitle = [[XPExtraContentView alloc] initWithFrame:k_keyWindow.bounds];
        weakSelf.extraTitle = extraTitle;
        extraTitle.canInputEmoji = YES;
        [k_keyWindow addSubview:extraTitle];
    
        [extraTitle showWithContent:@"" Title:@"发表评论"];
        extraTitle.inputPlaceHolder = @"请输入评论内容";
        extraTitle.maxWords = 100;
        [extraTitle setFinishBlock:^(NSString * _Nonnull content) {
            if (content.length == 0) {
                content = @"";
                [[XPShowTipsTool shareInstance] showMsg:@"请输入评论" ToView:k_keyWindow];
            }else {
                [weakSelf submitComment:content Model:model IndexPath:indexPath];
                [weakSelf.extraTitle removeFromSuperview];
            }
        }];
    }];
    [cell setItemTapBlock:^(NSInteger tag) {
        if (tag == 0) {//转发
            
        }else if (tag == 1){//点赞
            if (model.isLike.intValue == 1) {
                [[XPShowTipsTool shareInstance] showMsg:@"已点赞过!" ToView:k_keyWindow];
                return;
            }
            [weakSelf likeCircleWithModel:model IndexPath:indexPath];
        }else if(tag == 2){
            [weakSelf shareCircleWithModel:model IndexPath:indexPath];
        }
    }];
    
    [cell setFoldButtonBlock:^{
        model.isfold = YES;
        [weakSelf getCellHeightArray];
        [weakSelf.tableView reloadData];
    }];
    
    [cell setHeadTapBlock:^{
        if (model.isSelf.intValue == 1) {
            XPCircleViewController *vc = [XPCircleViewController new];
            vc.entryType = 1;
            vc.userId = 0;
            vc.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else {
            XPCircleViewController *vc = [XPCircleViewController new];
            vc.entryType = 2;
            vc.userId = model.userId.intValue;
            vc.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
        
    }];
    
    [cell setDeleteCommentBlock:^(NSInteger index) {
        
        [weakSelf.xpAlertView configWithTitle:@"您确定要删除评论吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
  
        } rightAction:^(NSString * _Nullable extra) {
            [weakSelf deleteCommentWithIndex:index Model:model IndexPath:indexPath];
        } alertType:XPAlertTypeNormal];

    }];
    
    [cell setVideoBlock:^(NSString * _Nonnull videoUrlStr) {
        weakSelf.isOpenVideo = YES;
        [[XPOpenSourceTool shareInstance] openVideoControllerWithUrl:videoUrlStr];
    }];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = self.heightArray[indexPath.row];
    return height.floatValue;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XPCircleDetailViewController *vc = [XPCircleDetailViewController new];
    XPCircleListModel *model = self.dataArray[indexPath.row];
    vc.model = model;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark --- netWork
- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_Page_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    //类型 0 热点 1 最新 2 我的(他的)
    paraM[@"type"] = @(self.currentIndex - 1);
    paraM[@"userId"] = @(self.userId);
    //业务类型 0 发布的 1 点赞的 2 评论的
    paraM[@"keywords"] = self.keyWords;
    
    if (self.currentIndex >2) {
        paraM[@"businessType"] = @(self.businessIndex);
    }
    
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPCircleListModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self getCellHeightArray];
            [self.tableView reloadData];
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)getTopData {
    
    NSNumber *userId;
    if (self.entryType == 0 || self.entryType == 1) {
        userId = @(0);
    }else {
        userId = @(self.userId);
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kCircle_top,userId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.topDic = data.userData;
            self.isShow = data.isShow;
            [self freshTopData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)headerRefresh{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_Page_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    //类型 0 热点 1 最新 2 我的(他的)
    paraM[@"type"] = @(self.currentIndex -1);
    paraM[@"userId"] = @(0);
    paraM[@"keywords"] = self.keyWords;
    if (self.currentIndex >2) {
        paraM[@"businessType"] = @(self.businessIndex);
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPCircleListModel mj_objectArrayWithKeyValuesArray:dataArray];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:modelArray];
            [self getCellHeightArray];
            [self.tableView reloadData];
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_Page_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(self.page);
    paraM[@"pageSize"] = @(20);
    //类型 0 热点 1 最新 2 我的(他的)
    paraM[@"type"] = @(self.currentIndex -1);
    paraM[@"userId"] = @(0);
    paraM[@"keywords"] = self.keyWords;
    
    if (self.currentIndex >2) {
        paraM[@"businessType"] = @(self.businessIndex);
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPCircleListModel mj_objectArrayWithKeyValuesArray:dataArray];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self getCellHeightArray];
                [self.tableView reloadData];
            }
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

#pragma mark -- network
//发表评论
- (void)submitComment:(NSString *)content Model:(XPCircleListModel *)model IndexPath:(NSIndexPath *)indexPath{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_comment_submit];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"circleId"] = model.circleId;
    paraM[@"content"] = content;
    paraM[@"isList"] = @(1);
    //业务类型 0 发布的 1 点赞的 2 评论的
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
            NSArray *commentList = data.userData;
            model.commentNum = [NSNumber numberWithInteger:model.commentNum.integerValue + 1];
            model.circleCommentList = commentList;
            model.isComment = @(1);
            [self getCellHeightArray];
            [self.tableView reloadData];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
//点赞
- (void)likeCircleWithModel:(XPCircleListModel *)model IndexPath:(NSIndexPath *)indexPath{
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kCircle_like,model.circleId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
            model.isLike = @(1);
            model.likeNum = [NSNumber numberWithInteger:model.likeNum.integerValue + 1];
            [self getCellHeightArray];
            [self.tableView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)deleteCommentWithIndex:(NSInteger)index  Model:(XPCircleListModel *)model IndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = model.circleCommentList[index];
    
     NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kCircle_comment_delete,dic[@"id"]];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            NSMutableArray *tempArray = model.circleCommentList.mutableCopy;
            [tempArray removeObject:dic];
            model.circleCommentList = tempArray.copy;
            
            [self getCellHeightArray];
            [self.tableView reloadData];
            
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
//转发
- (void)shareCircleWithModel:(XPCircleListModel *)model IndexPath:(NSIndexPath *)indexPath{
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kCircle_share,model.circleId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            //分享
            NSDictionary *shareDic = data.userData;
            if (!shareDic || shareDic.allKeys.count == 0) {
                //后台数据不对,暂不拉起
            }else {
                if (model.isShare.intValue == 0) {
                    model.isShare = @(1);
                    model.shareNum = [NSNumber numberWithInteger:model.shareNum.integerValue + 1];
                    [self getCellHeightArray];
                    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                }
                
                
                [self shareWithdata:shareDic];
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)shareWithdata:(NSDictionary *)shareDic {
    
    XPShareUMView *shareView = [[XPShareUMView alloc] initWithFrame:k_keyWindow.bounds];
    [k_keyWindow addSubview:shareView];
    [shareView show];
    
    XPWeakSelf
    [shareView setActionBlock:^(NSInteger index) {
        if (index == 1) {//分享好友
            
            [weakSelf shareWithPlatform:UMSocialPlatformType_WechatSession shareWithdata:shareDic];
        }else if (index == 2){
            [weakSelf shareWithPlatform:UMSocialPlatformType_WechatTimeLine shareWithdata:shareDic];
        }
        
    }];
}

- (void)shareWithPlatform:(UMSocialPlatformType)platformType shareWithdata:(NSDictionary *)shareDic{
    
    [[XPUMShareTool shareInstance] shareWebUrlWithPlatform:platformType title:shareDic[@"title"]?: @"分享"
                                                     descr:shareDic[@"content"]?:@"新拍分享链接"
                                              webUrlString:shareDic[@"shareUrl"]?:@""
                                     currentViewController:self
                                                completion:^(id result, NSError *error) {
        
        if(error){
        NSLog(@"************Share fail with error %@*********",error);
    //        [[XPShowTipsTool shareInstance] showMsg:@"分享失败" ToView:self.view];
        }else{
        NSLog(@"response data is %@",result);
    //        [[XPShowTipsTool shareInstance] showMsg:@"分享成功" ToView:self.view];
        }
    }];
}
//关注
-(void)addAttentionWithUserId:(NSNumber *)userId{
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_creation_attention_someOne,userId];
   
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:@"关注成功" ToView:self.view];
            [self getTopData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

-(void)cancelAttentionWithUserId:(NSNumber *)userId {
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_creation_disAttention_someOne,userId];
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:@"取消成功" ToView:self.view];
            [self getTopData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)canShowEmptyView {
    if (self.dataArray.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}
@end
