//
//  XPCircleViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/14.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPCircleViewController : XPBaseViewController
//进入样式 0 默认圈子 1 我的圈子 2 他的圈子
@property (nonatomic, assign) NSInteger entryType;
//用户id,查询本人传 0
@property (nonatomic, assign) NSInteger userId;

@end

NS_ASSUME_NONNULL_END
