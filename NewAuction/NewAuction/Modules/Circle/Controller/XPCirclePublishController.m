//
//  XPCirclePublishController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/14.
//

#import "XPCirclePublishController.h"
#import "XPLimitInputTextView.h"
#import "XPPublishSourceView.h"
#import "XPOSSUploadTool.h"
#import "XPHitButton.h"

@interface XPCirclePublishController ()
//文字内容
@property (nonatomic, strong) XPLimitInputTextView *textView;

@property (nonatomic, strong) UIScrollView *contentView;
//添加图片
@property (nonatomic, strong) XPPublishSourceView *imageAddView;

@property (nonatomic, strong) UIImageView *videoImageView;
//图片或者视频地址数组
@property (nonatomic, strong) NSArray *urlArray;

@end

@implementation XPCirclePublishController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    
    [self setupViews];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"发布文章";
    [self.view addSubview:self.navBarView];
}

-(void)setupViews {
    
    UIScrollView *contentView = [[UIScrollView alloc] init];
    contentView.frame = CGRectMake(12, kTopHeight + 12, self.view.mj_w - 24, SCREEN_HEIGHT - 48 - kBottomHeight - kTopHeight);
    self.contentView = contentView;
    [self.view addSubview:contentView];
    
    self.textView = [[XPLimitInputTextView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.mj_w , 152)];
    self.textView.canInputEmoji = YES;
    self.textView.backgroundColor = UIColor.whiteColor;
    [self.textView configWithMaxCount:1000 textViewPlaceholder:@"请输入文章内容"];
    [self.contentView addSubview:self.textView];
    
    XPWeakSelf
    if (self.entryType == 0) {//发布图片
     
        UIView *videoBgView = [[UIView alloc] init];
        videoBgView.backgroundColor = UIColor.whiteColor;
        videoBgView.layer.cornerRadius = 8;
        videoBgView.frame = CGRectMake(0, MaxY(self.textView) + 12, self.contentView.mj_w, 133);
        [self.contentView addSubview:videoBgView];
        
        self.imageAddView = [[XPPublishSourceView alloc] initWithFrame:CGRectMake(12,  12, videoBgView.mj_w - 24, 101)];
        self.imageAddView.backgroundColor = UIColor.whiteColor;
        [self.imageAddView initWithType:0 maxCount:9];
        
        [videoBgView addSubview:self.imageAddView];
        
        XPWeakSelf
        [self.imageAddView setFrameChange:^(CGFloat maxHeight) {
            weakSelf.imageAddView.frame = CGRectMake(12,  12, videoBgView.mj_w - 24, maxHeight);
            videoBgView.frame = CGRectMake(0, MaxY(weakSelf.textView) + 12, weakSelf.contentView.mj_w, 24 + maxHeight);
        }];
        
        [self.imageAddView setChangeBlock:^(NSArray * _Nonnull urlArray) {
            weakSelf.urlArray = urlArray;
        }];
        
    }else {//发布视频
       
        UIView *videoBgView = [[UIView alloc] init];
        videoBgView.backgroundColor = UIColor.whiteColor;
        videoBgView.layer.cornerRadius = 8;
        videoBgView.frame = CGRectMake(0, MaxY(self.textView) + 12, self.contentView.mj_w, 133);
        [self.contentView addSubview:videoBgView];
        
        self.imageAddView = [[XPPublishSourceView alloc] initWithFrame:CGRectMake(12,12, videoBgView.mj_w - 24, 101)];
        self.imageAddView.backgroundColor = UIColor.whiteColor;
        [self.imageAddView initWithType:1 maxCount:1];
        [videoBgView addSubview:self.imageAddView];
        
        [self.imageAddView setChangeBlock:^(NSArray * _Nonnull urlArray) {
            weakSelf.urlArray = urlArray;
        }];
        
    }
    
    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,self.view.mj_h - 48 - kBottomHeight, self.view.mj_w, 48 + kBottomHeight);
    bottomView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:bottomView];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"确认发布" forState:UIControlStateNormal];
    [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [rightButton setBackgroundColor:UIColorFromRGB(0x6E6BFF)];
    rightButton.frame = CGRectMake(24,4,bottomView.mj_w - 48,40);
    rightButton.layer.cornerRadius = 20;
    rightButton.layer.masksToBounds = YES;
    [rightButton addTarget:self action:@selector(sumbitOrder:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:rightButton];
    [rightButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :rightButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
}
#pragma mark --- 发布提交
- (void)sumbitOrder:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if ([self.textView.contentView.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"请输入文章内容" ToView:k_keyWindow];
    }else {
        //发布圈子

        NSString *url  = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_publish];
        
        NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
        paraM[@"type"] = @(self.entryType);
        if (self.urlArray.count > 0) {
            if (self.entryType == 0) {
                paraM[@"pics"] = [self.urlArray componentsJoinedByString:@","];
            }else if (self.entryType == 1){
                paraM[@"video"] = [self.urlArray componentsJoinedByString:@","];
            }
        }
        
        paraM[@"content"] = self.textView.contentView.text;
        
        [self startLoadingGif];
        [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
            [self stopLoadingGif];
            if (data.isSucess) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }];
    }
}

- (void)goBack{
    
    XPWeakSelf
    if ([self.textView.contentView.text stringByReplacingOccurrencesOfString:@" " withString:@""].length > 0  || self.urlArray.count > 0) {
        
        [self.xpAlertView configWithTitle:@"您确定要退出吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
            
        } rightAction:^(NSString * _Nullable extra) {
            
            [weakSelf.navigationController popViewControllerAnimated:YES];
            
        } alertType:XPAlertTypeNormal];
        
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
