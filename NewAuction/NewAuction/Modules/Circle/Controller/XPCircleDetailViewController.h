//
//  XPCircleDetailViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/19.
//

#import "XPBaseViewController.h"
#import "XPCircleListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPCircleDetailViewController : XPBaseViewController


@property (nonatomic, strong) XPCircleListModel *model;

@end

NS_ASSUME_NONNULL_END
