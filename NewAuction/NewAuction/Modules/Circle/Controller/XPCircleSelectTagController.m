//
//  XPCircleSelectTagController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/14.
//

#import "XPCircleSelectTagController.h"


@interface XPCircleSelectTagController ()

@property (nonatomic, strong) UIScrollView *mainView;
//数据源
@property (nonatomic, copy)   NSArray *dataArray;
//选中的标签
@property (nonatomic, strong) NSMutableArray *selectedArray;


@end

@implementation XPCircleSelectTagController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    self.selectedArray = [NSMutableArray array];
    [self setupViews];
    [self getData];
}

- (void)setupViews {
    UIImageView *bgImageView  = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [bgImageView setImage:[UIImage imageNamed:@"xp_circle_tagsbg"]];
    [self.view addSubview:bgImageView];
    
    UIButton *skipButton = [[UIButton alloc] init];
    [skipButton setTitle:@"跳过" forState:UIControlStateNormal];
    [skipButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    skipButton.layer.cornerRadius = 14;
    skipButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    skipButton.layer.borderWidth = 1;
    skipButton.titleLabel.font = kFontRegular(14);
    skipButton.frame = CGRectMake(self.view.mj_w - 20 - 68, self.safeTop + 20, 68, 28);
    [skipButton addTarget:self action:@selector(skipButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:skipButton];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(0, self.safeTop + 76, self.view.mj_w, 24);
    titleLabel.font = kFontMedium(20);
    titleLabel.textColor = app_font_color;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"请选择您喜欢的圈子";
    [self.view addSubview:titleLabel];
    
    self.mainView = [[UIScrollView alloc] init];
    self.mainView.frame = CGRectMake(24, MaxY(titleLabel) + 36, self.view.mj_w - 48, SCREEN_HEIGHT - MaxY(titleLabel) - 36 - 40 - 40 - 28 - 12);
    [self.view addSubview:self.mainView];

    
    UILabel *tipsLabel = [UILabel new];
    tipsLabel.frame = CGRectMake(0, MaxY(self.mainView) + 12, self.view.mj_w, 24);
    tipsLabel.font = kFontRegular(12);
    tipsLabel.textColor = UIColorFromRGB(0xB8B8B8);
    tipsLabel.textAlignment = NSTextAlignmentCenter;
    tipsLabel.text = @"您可选择三个感兴趣的圈子";
    [self.view addSubview:tipsLabel];
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(self.view.mj_w/2. - 192/2.,MaxY(tipsLabel) + 4, 192, 40);
    [closeButton setTitle:@"进入圈子" forState:UIControlStateNormal];
    closeButton.layer.cornerRadius = 20;
    closeButton.layer.masksToBounds = YES;
    closeButton.titleLabel.font = kFontMedium(16);
    [closeButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    closeButton.tag = 1;
    [closeButton addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];
    
    [closeButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :closeButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    [self createItemView:@[]];

}

- (void)createItemView:(NSArray *)titleArray {
    
    [self.mainView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    CGFloat contentW = self.mainView.mj_w;
    CGFloat marginTop = 12;
    CGFloat marginLeft = 12;
    CGFloat btnH = 36;
    
    CGFloat bottomY = 0;
    CGFloat currentX = 0;
    CGFloat currentY = 0;
    
    
    for (int i = 0; i<titleArray.count; i++) {
        
        NSDictionary *dic = titleArray[i];
//        NSString *name = [[titleArray objectAtIndex:i] objectForKey:@"name"];
        NSString *name = TO_STR(dic[@"name"]);
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i;
        button.layer.cornerRadius = 18;
        [button setTitle:name forState:UIControlStateNormal];
        button.titleLabel.font = kFontRegular(16);
        button.layer.borderWidth = 1;
        button.layer.borderColor = UIColor.clearColor.CGColor;
        button.tag = i ;
        [button addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        button.backgroundColor = [XPCommonTool colorWithHex:dic[@"backColour"]];
        [button setTitleColor:[XPCommonTool colorWithHex:dic[@"fontColour"]] forState:UIControlStateNormal];
        
        CGFloat stringW = [name sizeWithAttributes:@{NSFontAttributeName:button.titleLabel.font}].width;
        
        CGFloat btnW = stringW + 48;
        
        //一个规格一行的情况
        if (btnW >=  contentW) {
            currentY = currentY + btnH + marginTop;
            button.frame = CGRectMake(0, currentY, contentW, btnH);
            
        }else{//正常情况
            if (currentX + btnW + marginLeft > contentW) {//换行
                currentX = 0;
                currentY = currentY + btnH + marginTop;
                button.frame = CGRectMake(currentX, currentY,btnW, btnH);
                currentX = MaxX(button);
            }else{
                CGFloat left = 0;
                if (currentX != 0) {
                    left = currentX + marginLeft;
                }
                button.frame = CGRectMake(left, currentY,btnW, btnH);
                currentX = MaxX(button);
            }
        }
        [self.mainView addSubview:button];
        
        
        if (i == titleArray.count) {
            bottomY = MaxY(button);
        }
    }
    //重新布局,居中.

    self.mainView.contentSize = CGSizeMake(self.mainView.mj_w, bottomY);
    
    
}

#pragma mark --- 网络请求


#pragma mark -- button Action
- (void)skipButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)closeButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.selectedArray.count == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"请先选择您的标签" ToView:k_keyWindow];
        
    }else {
        [self selectMySign];
    }
    
}

- (void)itemButtonClicked:(UIButton *)sender {
    UIColor *color = sender.titleLabel.textColor;
    
    if (sender.selected) {
        [self.selectedArray removeObject:self.dataArray[sender.tag]];
        sender.layer.borderColor = UIColor.clearColor.CGColor;
    }else {
        if (self.selectedArray.count >= 3) {
            
            [[XPShowTipsTool shareInstance] showMsg:@"您最多可以选择三个标签" ToView:k_keyWindow];
            
            return;
        }
        sender.layer.borderColor = color.CGColor;
        
        [self.selectedArray addObject:self.dataArray[sender.tag]];
    }
    sender.selected = !sender.selected;
    
}

- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_sign_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.dataArray = data.userData;
            [self createItemView:self.dataArray];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)selectMySign{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_sign_Set];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    
    NSMutableArray *idsArray = [NSMutableArray array];
    NSMutableArray *namesArray = [NSMutableArray array];

    for (int i = 0; i<self.selectedArray.count; i++) {
        NSDictionary *dic = self.selectedArray[i];
        [idsArray addObject:dic[@"id"]];
        [namesArray addObject:dic[@"name"]];
    }
    
    paraM[@"circleLabelIds"] = [idsArray componentsJoinedByString:@","];
    paraM[@"circleLabelNames"] = [namesArray componentsJoinedByString:@","];

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:@"提交成功!" ToView:k_keyWindow];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

@end
