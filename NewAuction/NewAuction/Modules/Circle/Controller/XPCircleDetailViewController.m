//
//  XPCircleDetailViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/19.
//

#import "XPCircleDetailViewController.h"
#import "XPCircleDetailView.h"
#import "XPCircleCommentCell.h"
#import "XPExtraContentView.h"
#import "XPShareUMView.h"
#import "XPUMShareTool.h"
#import "XPCreationFansController.h"
#import "IQKeyboardManager.h"


@interface XPCircleDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

//列表
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) XPCircleDetailView *topView;

@property (nonatomic, strong) UIView *commentView;

@property (nonatomic, strong) NSMutableArray *dataArray;
//cell高度
@property (nonatomic, strong) NSArray *heightArray;
//当前页码
@property (nonatomic, assign) NSInteger page;

@property (nonatomic, strong) XPExtraContentView *extraTitle;

@end

@implementation XPCircleDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.dataArray = [NSMutableArray array];
    self.page = 1;
    
    [self setupViews];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 32;
    [self getTopData];
    [self getData];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 0;
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"文章详情";
    [self.view addSubview:self.navBarView];
}
- (void)setupViews {
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0,kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    self.tableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
     
    self.topView = [[XPCircleDetailView alloc] initWithFrame:CGRectMake(0, 0, self.view.mj_w, 0)];
    self.tableView.tableHeaderView = self.topView;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.mj_w, self.safeBottom)];
    
    XPWeakSelf
    //self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf headerRefresh];
    }];
    
    [self.topView setCommentBlock:^{
        if (weakSelf.model.isComment.intValue == 1) {
            [[XPShowTipsTool shareInstance] showMsg:@"已评论过!" ToView:k_keyWindow];
            return;
        }
        
        XPExtraContentView *extraTitle = [[XPExtraContentView alloc] initWithFrame:k_keyWindow.bounds];
        weakSelf.extraTitle = extraTitle;
        [weakSelf.view addSubview:extraTitle];
    
        [extraTitle showWithContent:@"" Title:@"发表评论"];
        extraTitle.inputPlaceHolder = @"请输入评论内容";
        extraTitle.maxWords = 100;
        extraTitle.canInputEmoji = YES;
        [extraTitle setFinishBlock:^(NSString * _Nonnull content) {
            if (content.length == 0) {
                content = @"";
                [[XPShowTipsTool shareInstance] showMsg:@"请输入评论" ToView:k_keyWindow];
            }else {
                [weakSelf submitComment:content];
                [weakSelf.extraTitle removeFromSuperview];
            }
        }];
    }];
    [self.topView setItemTapBlock:^(NSInteger tag) {
        if (tag == 0) {//转发
            
        }else if (tag == 1){//点赞
            if (weakSelf.model.isLike.intValue == 1) {
                [[XPShowTipsTool shareInstance] showMsg:@"已点赞过!" ToView:k_keyWindow];
                return;
            }
            [weakSelf likeCircle];
        }else if(tag == 2){
            [weakSelf shareCircle];
        }
    }];
    [self.topView setLikeButtonBlock:^{
        XPCreationFansController *vc = [XPCreationFansController new];
        vc.entryType = 4;
        vc.userId = weakSelf.model.userId;
        vc.circleId = weakSelf.model.circleId;
        vc.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:vc animated:YES];
        
    }];
}

- (void)refreshData{
    
    CGFloat topH = [self.topView ViewWithModel:self.model];
    self.topView.frame = CGRectMake(0, 12, self.view.mj_w, topH);
    self.tableView.tableHeaderView = self.topView;
    
}

#pragma mark --- tableview delegete dataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPCircleCommentCell *cell = [XPCircleCommentCell cellWithTableView:tableView];
    
    [cell cellWithDic:self.dataArray[indexPath.row]];
    XPWeakSelf
    [cell setDeleteCommentBlock:^{
        [weakSelf.xpAlertView configWithTitle:@"您确定要删除评论吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
  
        } rightAction:^(NSString * _Nullable extra) {
            [weakSelf deleteCommentWithDic:self.dataArray[indexPath.row] IndexPath:indexPath];
        } alertType:XPAlertTypeNormal];
        
    }];

    return cell;
}
- (void)deleteCommentWithDic:(NSDictionary *)dic IndexPath:(NSIndexPath *)indexPath{
    
     NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kCircle_comment_delete,dic[@"id"]];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            [self getData];
            [self getTopData];
            
            [self.dataArray removeObject:dic];
            [self getCellHeightArray];
            [self.tableView reloadData];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = self.heightArray[indexPath.row];
    return height.floatValue;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

//获取cell高度
- (void)getCellHeightArray{
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPCircleCommentCell *cell = XPCircleCommentCell.new;
    for (int i = 0; i<self.dataArray.count; i++) {
        NSDictionary *dic = self.dataArray[i];
        CGFloat height = [cell cellWithDic:dic];
        if (i == self.dataArray.count - 1) {
            height = height + 12;
        }
        
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    self.heightArray = tempArray.copy;
}


#pragma mark --- 页面列表数据
//文章详情
- (void)getTopData {
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kCircle_Page_detail,self.model.circleId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            self.model = [XPCircleListModel mj_objectWithKeyValues:data.userData];
            [self refreshData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
//评论列表
- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_comment_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    paraM[@"circleId"] = self.model.circleId;
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            self.dataArray = data.userData[@"records"];
            [self getCellHeightArray];
            [self.tableView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)headerRefresh{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_comment_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    paraM[@"circleId"] = self.model.circleId;
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            self.dataArray = data.userData[@"records"];
            [self getCellHeightArray];
            [self.tableView reloadData];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    
    self.page ++;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_comment_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    paraM[@"circleId"] = self.model.circleId;
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:dataArray];
                [self getCellHeightArray];
                [self.tableView reloadData];
            }
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

#pragma mark -- network
//发表评论
- (void)submitComment:(NSString *)content{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_comment_submit];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"circleId"] = self.model.circleId;
    paraM[@"content"] = content;
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
            //是否要刷新
            [self getData];
            [self getTopData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
//点赞
- (void)likeCircle{
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kCircle_like,self.model.circleId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
            //是否要刷新
            [self getTopData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//转发
- (void)shareCircle{
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kCircle_share,self.model.circleId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [self getTopData];
            //分享
            NSDictionary *shareDic = data.userData;
            if (!shareDic || shareDic.allKeys.count == 0) {
                //后台数据不对,暂不拉起
            }else {
                [self shareWithdata:shareDic];
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
//分享
- (void)shareWithdata:(NSDictionary *)shareDic {
    
    XPShareUMView *shareView = [[XPShareUMView alloc] initWithFrame:k_keyWindow.bounds];
    [k_keyWindow addSubview:shareView];
    [shareView show];
    
    XPWeakSelf
    [shareView setActionBlock:^(NSInteger index) {
        if (index == 1) {//分享好友
            
            [weakSelf shareWithPlatform:UMSocialPlatformType_WechatSession shareWithdata:shareDic];
        }else if (index == 2){
            [weakSelf shareWithPlatform:UMSocialPlatformType_WechatTimeLine shareWithdata:shareDic];
        }
        
    }];
}

- (void)shareWithPlatform:(UMSocialPlatformType)platformType shareWithdata:(NSDictionary *)shareDic{
    
    [[XPUMShareTool shareInstance] shareWebUrlWithPlatform:platformType title:shareDic[@"title"]?: @"分享"
                                                     descr:shareDic[@"content"]?:@"新拍分享链接"
                                              webUrlString:shareDic[@"shareUrl"]?:@""
                                     currentViewController:self
                                                completion:^(id result, NSError *error) {
        
        if(error){
        NSLog(@"************Share fail with error %@*********",error);
    //        [[XPShowTipsTool shareInstance] showMsg:@"分享失败" ToView:self.view];
        }else{
        NSLog(@"response data is %@",result);
    //        [[XPShowTipsTool shareInstance] showMsg:@"分享成功" ToView:self.view];
        }
    }];
}

@end
