//
//  XPCirclePublishController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/14.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPCirclePublishController : XPBaseViewController

//0 发布图片文字  1 发布视频文字
@property (nonatomic, assign)  NSInteger entryType;

@end

NS_ASSUME_NONNULL_END
