//
//  XPCreationAuthorView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPCreationAuthorView : UIView

//关注
@property (nonatomic, strong) UIButton *settingButton;

@property (nonatomic, copy) void(^fansBlock)(NSInteger tag);

- (CGFloat)viewWithDict:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
