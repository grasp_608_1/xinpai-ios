//
//  XPPrintRecoardCell.m
//  NewAuction
//
//  Created by Grasp_L on 2025/2/6.
//

#import "XPPrintRecoardCell.h"

@interface XPPrintRecoardCell ()

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel *userLabel;

@property (nonatomic, strong) UILabel *equipLabel;

@property (nonatomic, strong) UILabel *otherNameLabel;

@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIButton *rightButton;


@end

@implementation XPPrintRecoardCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPPrintRecoardCell";
    
    XPPrintRecoardCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPPrintRecoardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    self.mainView = [[UIView alloc] init];
    self.mainView.backgroundColor = UIColor.whiteColor;
    self.mainView.layer.cornerRadius = 8;
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(8, 8, SCREEN_WIDTH - 16, 134);
    [self.contentView addSubview:self.mainView];
    
    self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 100, 100)];
    [self.mainView addSubview:self.iconImageView];
    
    
    CGFloat labelW = self.mainView.mj_w - self.iconImageView.mj_w - 8;
    CGFloat labelX = MaxX(self.iconImageView) + 8;
    
    self.userLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelX, 8, labelW, 24)];
    self.userLabel.font = kFontRegular(12);
    self.userLabel.textColor = app_font_color;
    [self.mainView addSubview:self.userLabel];
    
    self.equipLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelX, MaxY(self.userLabel), labelW, 24)];
    self.equipLabel.font = kFontRegular(11);
    self.equipLabel.textColor = app_font_color;
    [self.mainView addSubview:self.equipLabel];
    
    self.otherNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelX, MaxY(self.equipLabel), labelW, 24)];
    self.otherNameLabel.font = kFontRegular(12);
    self.otherNameLabel.textColor = app_font_color;
    [self.mainView addSubview:self.otherNameLabel];
    
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelX, MaxY(self.otherNameLabel), labelW, 24)];
    self.timeLabel.font = kFontRegular(12);
    self.timeLabel.textColor = app_font_color;
    [self.mainView addSubview:self.timeLabel];
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.frame = CGRectMake(self.mainView.mj_w - 90 - 12, MaxY(self.timeLabel) + 2, 90, 24);
    self.rightButton.titleLabel.font = kFontRegular(14);
    self.rightButton.layer.borderWidth = 1;
    self.rightButton.layer.cornerRadius = 12;
    self.rightButton.layer.masksToBounds = YES;
    [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    [self.rightButton setTitle:@"打印详情" forState:UIControlStateNormal];
    [self.rightButton addTarget:self action:@selector(rightButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [self.mainView addSubview:self.rightButton];
}

- (void)cellWithModel:(XPPrintRecoardModel *)model {
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(model.modelPic)] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    self.userLabel.text = [NSString stringWithFormat:@"用户:%@",model.userPhone];
    self.equipLabel.text = [NSString stringWithFormat:@"设备:%@",model.devName];
    self.otherNameLabel.text = [NSString stringWithFormat:@"别名:%@",model.modelName];
    self.timeLabel.text = [NSString stringWithFormat:@"创建时间:%@",model.createTime];
    
}

- (void)rightButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.detailBlock) {
        self.detailBlock();
    }
}

@end
