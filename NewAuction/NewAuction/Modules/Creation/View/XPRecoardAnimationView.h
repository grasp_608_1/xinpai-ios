//
//  XPRecoardAnimationView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPRecoardAnimationView : UIView
//显示
-(void)showAnimationView;
//隐藏
- (void)dissmiss;
@end

NS_ASSUME_NONNULL_END
