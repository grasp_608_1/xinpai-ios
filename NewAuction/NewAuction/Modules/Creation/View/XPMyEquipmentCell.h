//
//  XPMyEquipmentCell.h
//  NewAuction
//
//  Created by Grasp_L on 2025/2/6.
//

#import <UIKit/UIKit.h>
#import "XPPrintRecoardModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPMyEquipmentCell : UITableViewCell

@property (nonatomic, copy) void(^detailBlock)(void);

- (CGFloat)cellWithModel:(XPPrintRecoardModel *)model;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
