//
//  XPCreateTopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPCreateTopView : UIView

//轮播图点击
@property (nonatomic, copy) void(^bannerSelectBlock)(NSDictionary *dic);
//区域切换
@property (nonatomic, copy) void(^itemSelectBlock)(NSDictionary *dic);

@property (nonatomic, copy) void(^hotitemSelectBlock)(NSDictionary *dic);

- (CGFloat)viewWithData:(NSDictionary *)dataDict;

@end

NS_ASSUME_NONNULL_END
