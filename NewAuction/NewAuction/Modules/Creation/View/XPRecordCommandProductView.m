//
//  XPRecordCommandProductView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/27.
//

#import "XPRecordCommandProductView.h"

static CGFloat popAnimationDuring = 5;

@interface XPRecordCommandProductView ()

@property (nonatomic, strong) UIView *contenView;

@property (nonatomic, strong) UIImageView *bgImageView;
//推荐商品View
@property (nonatomic, strong) UIButton *productView;
//推荐商品
@property (nonatomic, strong) UIImageView *commandImageView;
//取消按钮
@property (nonatomic, strong) UIButton *cancelButton;

@property (nonatomic, assign)CGImageSourceRef gif;

@property (nonatomic, copy) NSDictionary *dataDic;

@end

@implementation XPRecordCommandProductView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.frame = k_keyWindow.bounds;
    self.alpha = 0;
    [k_keyWindow addSubview:self];
    
    self.productView = [UIButton new];
    self.productView.backgroundColor = UIColor.whiteColor;
    self.productView.frame = CGRectMake( k_keyWindow.center.x, k_keyWindow.center.y - 50, 0, 0);
    [self.productView addTarget:self action:@selector(productViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.productView.layer.cornerRadius = 12;
    self.productView.layer.masksToBounds = YES;
    [self addSubview:self.productView];
    
    self.commandImageView = [[UIImageView alloc]initWithFrame:CGRectMake( k_keyWindow.center.x, k_keyWindow.center.y - 50, 0, 0)];
    self.commandImageView.layer.cornerRadius = 10;
    self.commandImageView.layer.masksToBounds = YES;
    [self addSubview:self.commandImageView];
  
    self.cancelButton = [UIButton new];
    [self.cancelButton setBackgroundImage:[UIImage imageNamed:@"xp_close_bg"] forState:UIControlStateNormal];
    self.cancelButton.frame = CGRectMake( k_keyWindow.center.x - 60, k_keyWindow.center.y + 25, 120, 50);
    [self.cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.cancelButton];
}

- (void)showCommandProduct{
    [self bringSubviewToFront:self.productView];
    [self bringSubviewToFront:self.commandImageView];
    
    [self.commandImageView sd_setImageWithURL:[NSURL URLWithString:self.dataDic[@"pic"]] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    CGFloat padding = 8;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.productView.frame = CGRectMake(k_keyWindow.center.x - 100,k_keyWindow.center.y - 200, 200, 200);
        self.commandImageView.frame = CGRectMake(k_keyWindow.center.x - 100 + padding,k_keyWindow.center.y - 200 + padding, 200 - padding * 2, 200 - padding *2);
    }completion:^(BOOL finished) {
        
    }];
}

-(void)keepBGAnimation{
    
    [self.bgImageView stopAnimating];
    
    self.bgImageView.image = [UIImage imageNamed:@"product_command_bg"];
    [self bringSubviewToFront:self.cancelButton];
}

-(void)showAnimationViewWithDic:(NSDictionary *)dic {
    
    _dataDic = dic;
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 1;
    }];

    CGRect rect = self.bounds;
    self.backgroundColor = UIColor.whiteColor;
    
    UIView *contenView = [[UIView alloc] initWithFrame:rect];
    UIImageView *imageView = [[UIImageView alloc] init];
    
    imageView.frame = contenView.bounds;
    NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:@"product_command" withExtension:@"gif"];
    //将GIF图片转换成对应的图片源
       CGImageSourceRef gifSource = CGImageSourceCreateWithURL((CFURLRef) fileUrl, NULL);
       //获取其中图片源个数，即由多少帧图片组成
       size_t frameCount = CGImageSourceGetCount(gifSource);
       //定义数组存储拆分出来的图片
       NSMutableArray *frames = [[NSMutableArray alloc] init];
        for (size_t i = 0; i < frameCount; i++) {
            @autoreleasepool {
                //从GIF图片中取出源图片
                CGImageRef imageRef = CGImageSourceCreateImageAtIndex(gifSource, i, NULL);
                //将图片源转换成UIimageView能使用的图片源
                UIImage *imageName = [UIImage imageWithCGImage:imageRef];
                //将图片加入数组中
                [frames addObject:imageName];
                CGImageRelease(imageRef);
            }
    }
       
    imageView.animationImages = frames;
    //每次动画时长
    imageView.animationDuration = popAnimationDuring;
//    imageView.animationRepeatCount = 1;
    [imageView startAnimating];
    self.bgImageView = imageView;
    [contenView addSubview:imageView];
    self.contenView = contenView;
    [self addSubview:contenView];
    
    [self performSelector:@selector(showCommandProduct) withObject:nil afterDelay:popAnimationDuring];
    [self performSelector:@selector(keepBGAnimation) withObject:nil afterDelay:popAnimationDuring];
    
    self.gif = gifSource;
}

- (void)dissmiss{
    
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];

}


- (void)productViewClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.sureBlock) {
        self.sureBlock();
    }
}

- (void)cancelButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.cancelBlock){
        self.cancelBlock();
    }
}

-(void)dealloc {

    CFRelease(_gif);
    
    NSLog(@"animationView dealloc");
}

@end
