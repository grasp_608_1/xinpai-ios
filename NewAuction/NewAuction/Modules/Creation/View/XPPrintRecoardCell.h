//
//  XPPrintRecoardCell.h
//  NewAuction
//
//  Created by Grasp_L on 2025/2/6.
//

#import <Foundation/Foundation.h>
#import "XPPrintRecoardModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPPrintRecoardCell : UITableViewCell

@property (nonatomic, copy) void(^detailBlock)(void);

- (void)cellWithModel:(XPPrintRecoardModel *)model;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
