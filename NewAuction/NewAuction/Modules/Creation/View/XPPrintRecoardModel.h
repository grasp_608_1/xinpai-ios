//
//  XPPrintRecoardModel.h
//  NewAuction
//
//  Created by Grasp_L on 2025/2/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPPrintRecoardModel : NSObject
//商品图片地址
@property (nonatomic, copy) NSString *modelPic;
//打印机二维码
@property (nonatomic, copy) NSString *devQrCode;
//商品名称
@property (nonatomic, copy) NSString *modelName;
//用户手机号
@property (nonatomic, copy) NSString *userPhone;

@property (nonatomic, copy) NSString *devAlias;
//设备号
@property (nonatomic, copy) NSString *devNum;
//设备名字
@property (nonatomic, copy) NSString *devName;
//创建打印时间
@property (nonatomic, copy) NSString *createTime;
//设备ID
@property (nonatomic, strong) NSNumber *devId;
//设备状态
@property (nonatomic, strong) NSNumber *status;

@end

NS_ASSUME_NONNULL_END
