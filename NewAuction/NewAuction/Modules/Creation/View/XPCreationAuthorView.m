//
//  XPCreationAuthorView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/1.
//

#import "XPCreationAuthorView.h"
#import "UIButton+parameter.h"

@interface XPCreationAuthorView ()

//顶部背景图
@property (nonatomic, strong) UIImageView *topBanner;
//用户头像
@property (nonatomic, strong) UIImageView *userIcon;
//用户头像
@property (nonatomic, strong) UIImageView *userBgIcon;
//用户名称
@property (nonatomic, strong) UILabel *userNickNameLabel;
//徽章
@property (nonatomic, strong) UIView *badgeView;
//用户签名
@property (nonatomic, strong) UILabel *descLabel;
//个性标签
@property (nonatomic, strong) UIView *signView;
//底部栏
@property (nonatomic, strong) UIView *bottomView;

@property (nonatomic, strong) UILabel *authorLabel;

@end

@implementation XPCreationAuthorView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    
    UIImageView *topBanner = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kTopHeight + 170)];
    self.topBanner = topBanner;
    topBanner.image = [UIImage imageNamed:@"xp_creation_top_bg"];
    [self addSubview:topBanner];
    
    self.userIcon = [UIImageView new];
    self.userIcon.frame = CGRectMake(20, kTopHeight, 48, 48);
    self.userIcon.layer.cornerRadius = 24;
    self.userIcon.layer.masksToBounds = YES;
    self.userIcon.layer.borderColor = [UIColor whiteColor].CGColor;
    self.userIcon.layer.borderWidth = 1;
    [self addSubview:self.userIcon];
    
    CGFloat bgWH = self.userIcon.mj_w * 1.4;
    self.userBgIcon = [UIImageView new];
    self.userBgIcon.frame = CGRectMake(0,0, bgWH, bgWH);
    [self addSubview:self.userBgIcon];
    self.userBgIcon.center = self.userIcon.center;
    
    self.userNickNameLabel = [UILabel new];
    self.userNickNameLabel.font = kFontMedium(18);
    self.userNickNameLabel.textColor = app_font_color;
    self.userNickNameLabel.frame = CGRectMake(MaxX(self.userIcon) + 16, self.userIcon.frame.origin.y, self.mj_w - 76 - 90 - 8, 24);
    [self addSubview:self.userNickNameLabel];
    
    self.descLabel = [UILabel new];
    self.descLabel.font = kFontRegular(14);
    self.descLabel.textColor = app_font_color;
    self.descLabel.frame = CGRectMake(MaxX(self.userIcon) + 16, MaxY(self.userNickNameLabel), self.mj_w - 76 - 90 -8, 24);
    [self addSubview:self.descLabel];
    
    self.badgeView = [UIView new];
    
    [self addSubview:self.badgeView];
    
    self.signView = [UIView new];
    [self addSubview:self.signView];
    
    
    UIButton *settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    settingButton.layer.cornerRadius = 14;
    settingButton.layer.masksToBounds = YES;
    settingButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
    [settingButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    settingButton.titleLabel.font = kFontRegular(14);
    settingButton.frame = CGRectMake(self.mj_w - 88, self.userIcon.frame.origin.y + 8, 68, 28);
    self.settingButton = settingButton;
    [self addSubview:settingButton];
    
    self.bottomView = [UIView new];
    self.bottomView.backgroundColor = UIColor.whiteColor;
    self.bottomView.layer.cornerRadius = 8;
    [self addSubview:self.bottomView];
    
    self.authorLabel = [UILabel new];
    self.authorLabel.backgroundColor = UIColor.whiteColor;
    self.authorLabel.textAlignment = NSTextAlignmentCenter;
    self.authorLabel.font = kFontMedium(14);
    self.authorLabel.textColor = app_font_color;
    self.authorLabel.text = @"作者模型";
    [self addSubview:self.authorLabel];
}

- (CGFloat)viewWithDict:(NSDictionary *)dict{
    
    CGFloat top = MaxY(self.userIcon);
    
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:TO_STR(dict[@"head"])] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    if (dict[@"nickname"]) {
        self.userNickNameLabel.text = TO_STR(dict[@"nickname"]);
        CGFloat nameW = [self.userNickNameLabel.text sizeWithAttributes:@{NSFontAttributeName:self.userNickNameLabel.font}].width;
        self.userNickNameLabel.frame = CGRectMake(MaxX(self.userIcon) + 16, self.userIcon.frame.origin.y, nameW, 24);
    }
    
    NSArray *titleArray = [NSArray array];
    NSString *badgePics = dict[@"badgePics"];
    CGFloat imageWH = 16;
    if (badgePics.length > 0) {
        titleArray = [badgePics componentsSeparatedByString:@","];
    }
    self.badgeView.frame = CGRectMake(MaxX(self.userNickNameLabel) + 4, self.userIcon.frame.origin.y + 3, imageWH * titleArray.count, imageWH);
    [self.badgeView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (titleArray.count > 0) {
       
        for (int i = 0; i< titleArray.count; i++) {
            UIImageView *iconImageView = [[UIImageView alloc] init];
            iconImageView.frame = CGRectMake(imageWH * i, 0, imageWH, imageWH);
            [iconImageView sd_setImageWithURL:[NSURL URLWithString:titleArray[i]] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
            [self.badgeView addSubview:iconImageView];
        }
    }
    
    
    self.userBgIcon.image = [UIImage imageNamed:[NSString stringWithFormat:@"maker_leverl_%@",dict[@"level"]]];
    
    self.descLabel.text = dict[@"introduce"];
    
    NSNumber *status = dict[@"status"];
    if (status && status.intValue == 1) {
        [self.settingButton setTitle:@"已关注" forState:UIControlStateNormal];
    }else {
        [self.settingButton setTitle:@"关注" forState:UIControlStateNormal];
    }
    NSMutableArray *tempArray = [NSMutableArray array];
    NSString *rewardInfo = dict[@"rewardInfo"];
    NSString *badgeInfo = dict[@"badgeInfo"];
    if (rewardInfo && rewardInfo.length > 0) {
        [tempArray addObject:rewardInfo];
    }
    if (badgeInfo && badgeInfo.length > 0) {
        [tempArray addObject:badgeInfo];
    }
    
    NSArray *signArray = tempArray.copy;
    [self.signView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    for (int i = 0; i<signArray.count; i++) {
        
        NSString *str = signArray[i];
        CGFloat strW = [str sizeWithAttributes:@{NSFontAttributeName:kFontRegular(14)}].width;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:str forState:UIControlStateNormal];
        [button setTitleColor:UIColorFromRGB(0xAC855F) forState:UIControlStateNormal];
        button.titleLabel.font = kFontRegular(14);
        button.layer.cornerRadius = 4;
        button.frame = CGRectMake( 20,i * (28 + 10), strW + 14, 28);
        [self.signView addSubview:button];
        [button.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :button colors:
                                         @[(__bridge id)UIColorFromRGB(0xFEFFF3).CGColor,
                                           (__bridge id)UIColorFromRGB(0xFFD29E).CGColor]] atIndex:0];
    }
    if (signArray.count > 0) {
        self.signView.frame = CGRectMake(0, top + 10, self.mj_w, signArray.count*38 - 10);
        top = MaxY(self.signView);
    }
    
    NSArray *bottomArray = @[
        @{
            @"name":@"关注",@"num":TO_STR(dict[@"fromFollowNum"])
        },@{
            @"name":@"粉丝",@"num":TO_STR(dict[@"toFollowNum"])
        },@{
            @"name":@"徽章",@"num":TO_STR(dict[@"rewardNum"])
        },@{
            @"name":@"作品",@"num":TO_STR(dict[@"worksNum"])
        },@{
            @"name":@"人气",@"num":TO_STR(dict[@"popularity"])
        },
    ];
    
    [self.bottomView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    self.bottomView.frame = CGRectMake(12, top + 20, self.mj_w - 24, 52);
    CGFloat ButtonH = 52;
    CGFloat ButtonW = self.bottomView.mj_w / bottomArray.count;
    for (int i= 0; i<bottomArray.count; i++) {
        NSDictionary *dict = bottomArray[i];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(i*ButtonW, 0, ButtonW, ButtonH);
        button.tag = i;
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:button];
        
        UILabel *topLabel = [UILabel new];
        topLabel.textAlignment = NSTextAlignmentCenter;
        topLabel.textColor = app_font_color;
        topLabel.font = kFontMedium(14);
        topLabel.frame = CGRectMake(0, 8,button.mj_w, 18);
        [button addSubview:topLabel];
       
        UILabel *bottomLabel = [UILabel new];
        bottomLabel.textAlignment = NSTextAlignmentCenter;
        bottomLabel.textColor = app_font_color_8A;
        bottomLabel.font = kFontRegular(12);
        bottomLabel.frame = CGRectMake(0, 26,button.mj_w, 18);
        [button addSubview:bottomLabel];
        
        topLabel.text = [NSString stringWithFormat:@"%@",dict[@"num"]];
        bottomLabel.text = dict[@"name"];
        
        if (i != bottomArray.count - 1) {
            UIView *sepView = [UIView new];
            sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
            sepView.frame = CGRectMake((i + 1) * ButtonW, 11, 0.5, 30);
            [button addSubview:sepView];
        }
    }
    top = MaxY(self.bottomView);
    
    self.topBanner.frame = CGRectMake(0, 0, SCREEN_WIDTH, top);
    
    top = MaxY(self.topBanner);
    self.authorLabel.frame = CGRectMake(0, top + 20, self.mj_w, 48);
        
    return MaxY(self.authorLabel);

}
- (void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.fansBlock) {
        self.fansBlock(sender.tag);
    }
}
@end
