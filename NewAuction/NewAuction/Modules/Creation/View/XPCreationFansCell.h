//
//  XPCreationFansCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/1.
//

#import <UIKit/UIKit.h>
#import "XPFansModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPCreationFansCell : UITableViewCell

@property (nonatomic, copy) void(^attentBlock)(void);

@property (nonatomic, strong) XPFansModel *model;

+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end

NS_ASSUME_NONNULL_END
