//
//  XPCreationCollectionViewCell.h
//  NewAuction
//
//  Created by Grasp_L on 2025/1/3.
//

#import <UIKit/UIKit.h>
#import "XPMakerProductModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPCreationCollectionViewCell : UICollectionViewCell

- (CGFloat)cellWithModel:(XPMakerProductModel *)model;

@end

NS_ASSUME_NONNULL_END
