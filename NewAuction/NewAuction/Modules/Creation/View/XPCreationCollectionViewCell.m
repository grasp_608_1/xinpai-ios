//
//  XPCreationCollectionViewCell.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/3.
//

#import "XPCreationCollectionViewCell.h"
#import "XPColorsSignView.h"

@interface XPCreationCollectionViewCell ()

//商品图片
@property (nonatomic, strong) UIImageView *imageView;
//商品名称
@property (nonatomic, strong) UILabel *titleLabel;
//标签
@property (nonatomic, strong) XPColorsSignView *signView;
//商品购买价格
@property (nonatomic, strong) UILabel *priceSaleLabel;
//热度
@property (nonatomic, strong) UIView *hotView;
//🔥
@property (nonatomic, strong) UIImageView *hotIcon;
//热度数量
@property (nonatomic, strong) UILabel *hotNumLabel;

@end

@implementation XPCreationCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = UIColorFromRGB(0xF7F4FF);
        self.layer.cornerRadius = 4;
        self.clipsToBounds = YES;
        [self  setupViews];
        
    }
    
    return self;
}

-(void)setupViews
{
    CGFloat margin = 8;
    CGFloat cellW = self.bounds.size.width;
    self.imageView = [[UIImageView alloc] init];
    self.imageView.frame = CGRectMake(0, 0,cellW ,cellW);
    self.imageView.image = [UIImage imageNamed:@"xp_pd_default"];
    [self addSubview:self.imageView];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.frame = CGRectMake(margin, MaxY(self.imageView) + margin, cellW - 2 * margin, 20);
    self.titleLabel.font = kFontMedium(14);
    self.titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.titleLabel.lineBreakMode  = NSLineBreakByTruncatingTail;
    self.titleLabel.numberOfLines = 2;
    [self addSubview:self.titleLabel];
    
    self.priceSaleLabel = [UILabel new];
    self.priceSaleLabel.frame = CGRectMake(margin,MaxY(self.titleLabel) + 4, cellW - 2 * margin, 20);
    self.priceSaleLabel.font = kFontRegular(16);
    self.priceSaleLabel.textColor = UIColorFromRGB(0xFF4A4A);
    [self addSubview:self.priceSaleLabel];
    
    self.signView = [[XPColorsSignView alloc] init];
    self.signView.layer.masksToBounds = YES;
    [self addSubview:self.signView];

    self.hotView = [XPColorsSignView new];
    self.hotView.frame = CGRectMake(100, 78, 100, 18);
    self.hotView.layer.cornerRadius = 2;
    self.hotView.backgroundColor = RGBACOLOR(58, 60, 65, 0.7);
    [self.imageView addSubview:self.hotView];
    
    self.hotIcon = [[UIImageView alloc] init];
    self.hotIcon.frame = CGRectMake(4, 3, 12, 12);
    self.hotIcon.image = [UIImage imageNamed:@"xp_circle_fire"];
    [self.hotView addSubview:self.hotIcon];
    
    UILabel *hotNumLabel = [UILabel new];
    hotNumLabel.font = kFontRegular(12);
    hotNumLabel.textColor = UIColor.whiteColor;
    hotNumLabel.frame = CGRectMake(17, 0,100, 16);
    self.hotNumLabel = hotNumLabel;
    [self.hotView addSubview:hotNumLabel];
}

- (CGFloat)cellWithModel:(XPMakerProductModel *)model {

    CGFloat margin = 8;
    CGFloat top = 0;
    CGFloat imageWH = (SCREEN_WIDTH - 36)/2.;
    self.imageView.frame = CGRectMake(0, 0, imageWH, imageWH);
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.pic]] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    top = MaxY(self.imageView);
    //热度
    NSString *hotNum = [NSString stringWithFormat:@"%@",model.popularity];
    CGFloat hotW = [hotNum sizeWithAttributes:@{NSFontAttributeName:self.hotNumLabel.font,}].width;
    self.hotNumLabel.frame = CGRectMake(17, 0, hotW, 18);
    self.hotNumLabel.text = hotNum;
    self.hotView.frame = CGRectMake(self.imageView.mj_w - 4 - 21 - hotW, self.imageView.mj_h - 4 - 18, 21 + hotW, 18);
    
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] init];
    NSAttributedString *titleStr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",model.name]];
    [attr appendAttributedString:titleStr];
    
    CGFloat titleH = [XPCommonTool calculateHeightForAttributedString:attr withWidth:imageWH - 2 * margin];

    self.titleLabel.frame = CGRectMake(margin, top +  margin, imageWH - 2 * margin, titleH > 40 ? 40:titleH);
    self.titleLabel.attributedText = attr;
    [self.titleLabel sizeToFit];
    
    top = MaxY(self.titleLabel);
    
    self.priceSaleLabel.frame = CGRectMake(margin,top + 4, imageWH - 2 * margin, 20);
    
    NSString *priceStr = [NSString stringWithFormat:@"售价:￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceSale]];
    NSMutableAttributedString *attr9 = [[NSMutableAttributedString alloc] initWithString:priceStr];
    [attr9 addAttributes:@{NSForegroundColorAttributeName:app_font_color,NSFontAttributeName:kFontRegular(14)} range:NSMakeRange(0,3)];
    self.priceSaleLabel.attributedText = attr9;

    top = MaxY(self.priceSaleLabel);
    
    [self.signView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    
    NSArray *titleArray = model.worksSkuLabelVos;
    if (titleArray && titleArray.count > 0) {
        [self.signView viewWithArray:titleArray];
        self.signView.frame = CGRectMake(8, top + 12, imageWH - 8 , 24);
        top = MaxY(self.signView);
    }
    return top + 8;
}


@end
