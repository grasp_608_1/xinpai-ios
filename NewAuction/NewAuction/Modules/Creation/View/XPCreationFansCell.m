//
//  XPCreationFansCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/1.
//

#import "XPCreationFansCell.h"

@interface XPCreationFansCell ()

@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *timeLabel;
//头像
@property (nonatomic, strong) UIImageView *iconImageView;
//等级
@property (nonatomic, strong) UIImageView *levelIcon;
//关注
@property (nonatomic, strong) UIButton *settingButton;

@end

@implementation XPCreationFansCell


+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPCreationFansCell";
    XPCreationFansCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPCreationFansCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    CGFloat mainW = SCREEN_WIDTH - 24;
    CGFloat imagWH = 48;
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.frame = CGRectMake(12, 12,mainW,72);
    mainView.layer.cornerRadius = 8;
    mainView.clipsToBounds = YES;
    self.mainView = mainView;
    [self.contentView addSubview:mainView];
    
    UIImageView *icon = [UIImageView new];
    icon.frame = CGRectMake(12, 12, imagWH,imagWH);
    icon.layer.cornerRadius = imagWH/2;
    icon.layer.masksToBounds = YES;
    self.iconImageView = icon;
    [mainView addSubview:icon];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = kFontMedium(16);
    self.titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    [mainView addSubview:self.titleLabel];
    
    UIImageView *levelIcon = [UIImageView new];
    levelIcon.frame = CGRectMake(12, 12,0,0);
    levelIcon.layer.cornerRadius = 12;
    levelIcon.layer.masksToBounds = YES;
    self.levelIcon = levelIcon;
    [mainView addSubview:levelIcon];
    
    self.timeLabel = [UILabel new];
    self.timeLabel.font = kFontRegular(12);
    self.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.timeLabel.textAlignment = NSTextAlignmentRight;
    [mainView addSubview:self.timeLabel];
    
    UIButton *settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    settingButton.layer.cornerRadius = 14;
    settingButton.layer.masksToBounds = YES;
    [settingButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    settingButton.titleLabel.font = kFontRegular(14);
    settingButton.frame = CGRectMake(self.mainView.mj_w - 16 -68, 34, 68, 28);
    [settingButton addTarget:self action:@selector(attentitonButtionClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.settingButton = settingButton;
    [self.mainView addSubview:settingButton];
    
}

-(void)setModel:(XPFansModel *)model{
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.head] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    
    NSString *timeStr = [NSString stringWithFormat:@"注册日期 %@",model.createDate?:model.createTime];
    CGFloat timeW = [timeStr sizeWithAttributes:@{NSFontAttributeName:self.timeLabel.font}].width;
    
    self.titleLabel.text = model.nickname;
    self.timeLabel.text = timeStr;
    self.levelIcon.image = [XPUserDataTool getUserRoleIcon:model.userType.integerValue];
    
    CGFloat titleW = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}].width;

    self.titleLabel.frame = CGRectMake(72, 12, titleW, 24);
    self.levelIcon.frame = CGRectMake(MaxX(self.titleLabel) + 4, self.titleLabel.mj_y, 24, 24);
    self.timeLabel.frame = CGRectMake(self.titleLabel.mj_x, MaxY(self.titleLabel), timeW, 24);
    
    self.settingButton.frame = CGRectMake(self.mainView.mj_w - 16 -68, 22, 68, 28);
    
    if (model.status.intValue == 1) {
        self.settingButton.backgroundColor = RGBACOLOR(110, 107, 255, 0.5);
        [self.settingButton setTitle:@"已关注" forState:UIControlStateNormal];
    }else {
        self.settingButton.backgroundColor = RGBACOLOR(110, 107, 255, 1);
        [self.settingButton setTitle:@"关注" forState:UIControlStateNormal];
    }
    
}

- (void)attentitonButtionClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.attentBlock) {
        self.attentBlock();
    }
}


@end
