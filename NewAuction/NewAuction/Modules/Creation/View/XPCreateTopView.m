//
//  XPCreateTopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/6.
//

#import "XPCreateTopView.h"
#import "XPLunboView.h"

#define createBannerHeight  338*SCREEN_WIDTH/375.

@interface XPCreateTopView ()<XPLunboViewDelegate>

@property (nonatomic, strong) UIView *mainView;
//轮播图
@property (nonatomic, strong) XPLunboView *topView;
//功能item
@property (nonatomic, strong) UIView *itemView;
@property (nonatomic, strong) NSArray *itemArray;
//人气商城
@property (nonatomic, strong) UIImageView *hotView;
@property (nonatomic, copy)   NSDictionary *hotDic;

@end


@implementation XPCreateTopView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(0, 0, self.mj_w, 0);
    mainView.clipsToBounds = YES;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    self.topView = [[XPLunboView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, createBannerHeight)];
    self.topView.cornerRadius = 0;
    self.topView.lunDelegate = self;
    [self.mainView addSubview:self.topView];
    
    UIImageView *leftImageView = [[UIImageView alloc] init];
    leftImageView.image = [UIImage imageNamed:@"xp_create_bannerleft"];
    leftImageView.frame = CGRectMake(0, kTopHeight - 44, 80, 70);
    [self.mainView addSubview:leftImageView];
    
    UIView *itemView = [UIView new];
    itemView.frame = CGRectMake(12, MaxY(self.topView), self.mainView.mj_w - 24, 0);
    self.itemView = itemView;
    [self.mainView addSubview:itemView];
    
    self.hotView = [[UIImageView alloc] init];
    self.hotView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hotTap:)];
    [self.hotView addGestureRecognizer:tap];
    [self.mainView addSubview:self.hotView];
    
}

- (CGFloat)viewWithData:(NSDictionary *)dataDict{
    
    [self.topView setLunBoArray:dataDict[@"printerRotationsVOs"]];
    
    NSArray *array = dataDict[@"homePrinterButton1VOs"];
    
    self.itemArray = array;
    [self.itemView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat itemW = (self.mainView.mj_w - 12 *4)/3.;
    CGFloat itemH = 140*SCREEN_WIDTH/375.;
    CGFloat maxItemBottom = 0;
    
    for (int i = 0; i<array.count; i++) {
        NSDictionary *dic = array[i];
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.frame = CGRectMake((i%3)*(itemW + 12), i/3 *(4 + itemH), itemW, itemH);
        [self.itemView addSubview:imageView];
        imageView.userInteractionEnabled = YES;
        imageView.tag = i;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        [imageView addGestureRecognizer:tap];
        [imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"pic"]]];
        
        UILabel *titleLable = [UILabel new];
        titleLable.font = kFontMedium(16);
        titleLable.textColor = UIColor.whiteColor;
        titleLable.frame = CGRectMake(12,imageView.mj_h - 24 - 31,imageView.mj_w - 24, 24);
        titleLable.text = dic[@"name"];
        [imageView addSubview:titleLable];
        
        UILabel *descLabel = [UILabel new];
        descLabel.font = kFontMedium(12);
        descLabel.textColor = UIColor.whiteColor;
        descLabel.frame = CGRectMake(12,imageView.mj_h - 12 - 17,imageView.mj_w - 24, 17);
        descLabel.text = dic[@"remark"];
        [imageView addSubview:descLabel];
        maxItemBottom = MaxY(imageView);
    }
    
    
    self.itemView.frame = CGRectMake(12, MaxY(self.topView) + 8, self.mainView.mj_w - 24, maxItemBottom);
    self.hotDic = dataDict[@"homePrinterButton2VO"];
    if (self.hotDic.allKeys.count != 0) {
        CGFloat hotW = self.mainView.mj_w - 24;
        CGFloat hotH = 95*SCREEN_WIDTH/375.;
        
        [self.hotView sd_setImageWithURL:[NSURL URLWithString:self.hotDic[@"pic"]]];
        self.hotView.frame = CGRectMake(12, MaxY(self.itemView) + 8, hotW, hotH);
        
        self.mainView.frame = CGRectMake(0,0, self.mj_w, MaxY(self.hotView));
        
        return MaxY(self.hotView);
    }else {
        return MaxY(self.itemView);
    }
}

-(void)tap:(UITapGestureRecognizer *)ges{
    buttonCanUseAfterOneSec(ges.view);
    if (self.itemSelectBlock) {
        NSDictionary *dic = self.itemArray[ges.view.tag];
        self.itemSelectBlock(dic);
    }
}

- (void)hotTap:(UITapGestureRecognizer *)ges{
    buttonCanUseAfterOneSec(ges.view);
    if (self.hotitemSelectBlock) {
        self.hotitemSelectBlock(self.hotDic);
    }
}

#pragma mark  -- banner delegate
-(void)tapImageClick:(NSDictionary *)dic{
    if (self.bannerSelectBlock) {
        self.bannerSelectBlock(dic);
    }
}
@end
