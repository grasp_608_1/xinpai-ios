//
//  XPRecordCommandProductView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPRecordCommandProductView : UIView

@property (nonatomic, copy) void(^sureBlock)(void);
@property (nonatomic, copy) void(^cancelBlock)(void);

-(void)showAnimationViewWithDic:(NSDictionary *)dic;
- (void)dissmiss;

@end

NS_ASSUME_NONNULL_END
