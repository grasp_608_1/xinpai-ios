//
//  XPRecoardAnimationView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/27.
//

#import "XPRecoardAnimationView.h"

@interface XPRecoardAnimationView ()

@property (nonatomic, strong) UIView *contenView;

@property (nonatomic, assign)CGImageSourceRef gif;

@end

@implementation XPRecoardAnimationView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.frame = CGRectMake(k_keyWindow.mj_w/2. - 75, k_keyWindow.mj_h/2. - 75, 150, 150);
    self.alpha = 0;
    [k_keyWindow addSubview:self];
}

-(void)showAnimationView {
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 1;
    }];
    
    CGRect rect = self.bounds;
//    self.backgroundColor = UIColor.whiteColor;
    
    UIView *contenView = [[UIView alloc] initWithFrame:rect];
    UIImageView *imageView = [[UIImageView alloc] init];
    
    imageView.frame = contenView.bounds;
    NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:@"voice_listen" withExtension:@"gif"];
    //将GIF图片转换成对应的图片源
       CGImageSourceRef gifSource = CGImageSourceCreateWithURL((CFURLRef) fileUrl, NULL);
    self.gif = gifSource;
       //获取其中图片源个数，即由多少帧图片组成
       size_t frameCount = CGImageSourceGetCount(gifSource);
       //定义数组存储拆分出来的图片
       NSMutableArray *frames = [[NSMutableArray alloc] init];
       for (size_t i = 0; i < frameCount; i++) {
           @autoreleasepool {
               //从GIF图片中取出源图片
               CGImageRef imageRef = CGImageSourceCreateImageAtIndex(gifSource, i, NULL);
               //将图片源转换成UIimageView能使用的图片源
               UIImage *imageName = [UIImage imageWithCGImage:imageRef];
               //将图片加入数组中
               [frames addObject:imageName];
               CGImageRelease(imageRef);
           }
          
       }
    imageView.animationImages = frames;
    //每次动画时长
    imageView.animationDuration = 2.0;
    [imageView startAnimating];
    [contenView addSubview:imageView];
    self.contenView = contenView;
    [self addSubview:contenView];
}

- (void)dissmiss{
    
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)dealloc{
    CFRelease(_gif);
}

@end
