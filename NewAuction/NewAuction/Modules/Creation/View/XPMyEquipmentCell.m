//
//  XPMyEquipmentCell.m
//  NewAuction
//
//  Created by Grasp_L on 2025/2/6.
//

#import "XPMyEquipmentCell.h"
#import "UIImage+QRCode.h"
@interface XPMyEquipmentCell ()

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel *equipLabel;

@property (nonatomic, strong) UILabel *otherNameLabel;

@property (nonatomic, strong) UILabel *statusLabel;

@property (nonatomic, strong) UIButton *rightButton;


@end

@implementation XPMyEquipmentCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPMyEquipmentCell";
    
    XPMyEquipmentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPMyEquipmentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    self.mainView = [[UIView alloc] init];
    self.mainView.backgroundColor = UIColor.whiteColor;
    self.mainView.layer.cornerRadius = 8;
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(8, 8, SCREEN_WIDTH - 16, 134);
    [self.contentView addSubview:self.mainView];
    
    self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 100, 100)];
    [self.mainView addSubview:self.iconImageView];
    
    
    CGFloat labelW = self.mainView.mj_w - self.iconImageView.mj_w - 8;
    CGFloat labelX = MaxX(self.iconImageView) + 8;
    
    
    self.equipLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelX, 12, labelW, 24)];
    self.equipLabel.font = kFontRegular(14);
    self.equipLabel.textColor = app_font_color;
    [self.mainView addSubview:self.equipLabel];
    
    self.statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelX, 12, labelW, 24)];
    self.statusLabel.font = kFontRegular(10);
    self.statusLabel.textColor = app_font_color_FF4A4A;
    self.statusLabel.layer.cornerRadius = 4;
    self.statusLabel.layer.borderColor = app_font_color_FF4A4A.CGColor;
    self.statusLabel.layer.borderWidth = 1;
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    [self.mainView addSubview:self.statusLabel];
    
    self.otherNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelX, MaxY(self.equipLabel), labelW, 24)];
    self.otherNameLabel.font = kFontRegular(14);
    self.otherNameLabel.textColor = app_font_color;
    [self.mainView addSubview:self.otherNameLabel];
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.frame = CGRectMake(self.mainView.mj_w - 90 - 12, MaxY(self.otherNameLabel) + 2, 90, 24);
    self.rightButton.titleLabel.font = kFontRegular(14);
    self.rightButton.layer.borderWidth = 1;
    self.rightButton.layer.cornerRadius = 14;
    self.rightButton.layer.masksToBounds = YES;
    [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    [self.rightButton setTitle:@"管理" forState:UIControlStateNormal];
    [self.rightButton addTarget:self action:@selector(rightButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [self.mainView addSubview:self.rightButton];
    
}

- (CGFloat)cellWithModel:(XPPrintRecoardModel *)model {
//    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(model.devQrCode)] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    self.iconImageView.image = [UIImage generateQRCodeFromString:model.devQrCode];
    
    CGFloat bottom = 0;
    
    self.equipLabel.text = [NSString stringWithFormat:@"%@",model.devName];
    CGFloat equipmentW = [self.equipLabel.text sizeWithAttributes:@{NSFontAttributeName:self.equipLabel.font}].width;
    self.equipLabel.frame = CGRectMake(12, 12, equipmentW, 24);
    
    self.statusLabel.text = [self getStatusWithModel:model];
    self.statusLabel.frame = CGRectMake(MaxX(self.equipLabel) + 3, self.equipLabel.mj_y + 4, 30, 16);
    
    bottom = MaxY(self.equipLabel);
    
    if (model.devAlias && model.devAlias.length > 0) {
        self.otherNameLabel.text = [NSString stringWithFormat:@"%@",model.devAlias];
        self.otherNameLabel.frame = CGRectMake(12, 12 + bottom, self.mainView.mj_w - 24, 24);
        bottom = MaxY(self.otherNameLabel);
    }
    
    self.rightButton.frame = CGRectMake(12, 12 + bottom, 80, 24);
    
    bottom = MaxY(self.rightButton);
    
    self.mainView.frame = CGRectMake(8, 0, SCREEN_WIDTH - 16, bottom + 12);
    
    self.iconImageView.frame = CGRectMake(self.mainView.mj_w -  60 - 12, self.mainView.mj_h/2. - 30 , 60, 60);
    
    return MaxY(self.mainView) + 12;
}

-(NSString *)getStatusWithModel:(XPPrintRecoardModel *)model {
    int status = model.status.intValue;
    
    NSString *str = @"空闲";
    
    switch (status) {
        case 0:
            str = @"空闲";
            break;
        case 1:
            str = @"空闲";
            break;
        case 2:
            str = @"打印";
            break;
        case 3:
            str = @"暂停";
            break;
        case 4:
            str = @"离线";
            break;
        default:
            break;
    }
    
    return str;
}

- (void)rightButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.detailBlock) {
        self.detailBlock();
    }
}

@end
