//
//  XPCreationFansController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/1.
//

#import "XPCreationFansController.h"
#import "XPEmptyView.h"
#import "XPCreationFansCell.h"
#import "XPFansModel.h"

@interface XPCreationFansController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) XPEmptyView *emptyView;

//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//当前页码
@property (nonatomic, assign) NSInteger page;

@end

@implementation XPCreationFansController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    self.dataArray = [NSMutableArray array];
    self.page = 1;
    
    [self setupUI];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getData];
}


- (void)setupUI {
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBottomHeight)];
    [self.view addSubview:self.tableView];
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无数据"];
    [self.view addSubview:self.emptyView];
    
    XPWeakSelf
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf headerRefresh];
    }];
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.navBarView];
    
    if (self.entryType == 1) {
        self.navBarView.titleLabel.text = [NSString stringWithFormat:@"%@的粉丝",self.nickName];
    }else if (self.entryType == 2){
        self.navBarView.titleLabel.text = [NSString stringWithFormat:@"%@的关注",self.nickName];
    }else if (self.entryType == 3){
        self.navBarView.titleLabel.text = @"我的关注";
    }else if (self.entryType == 4){
        self.navBarView.titleLabel.text = @"点赞评论用户";
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPCreationFansCell *cell = [XPCreationFansCell cellWithTableView:tableView];
    XPFansModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    
    XPWeakSelf
    [cell setAttentBlock:^{
        if (model.status.intValue == 0) {
            [weakSelf addAttentionWithModel:model indexPath:indexPath];
        }else {
            [weakSelf cancelAttentionWithModel:model indexPath:indexPath];
        }
    }];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 84;
}


- (void)getData {
    NSString *url = [self getPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    paraM[@"userId"] = self.userId;
    
    if (self.entryType == 4) {
        paraM[@"circleId"] = self.circleId;
    }
    
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPFansModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)headerRefresh{
    NSString *url = [self getPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    paraM[@"userId"] = self.userId;
    if (self.entryType == 4) {
        paraM[@"circleId"] = self.circleId;
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPFansModel mj_objectArrayWithKeyValuesArray:dataArray];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:modelArray];
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *url = [self getPath];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    paraM[@"userId"] = self.userId;
    if (self.entryType == 4) {
        paraM[@"circleId"] = self.circleId;
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPFansModel mj_objectArrayWithKeyValuesArray:dataArray];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self.tableView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

-(void)addAttentionWithModel:(XPFansModel *)model indexPath:(NSIndexPath *)indexPath{
    
    NSString *url;
    if (self.entryType == 3) {
       url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_creation_attention_someOne,model.toUserId];
    }else if (self.entryType == 4){
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_creation_attention_someOne,model.userId];
    }else {
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_creation_attention_someOne,model.fromUserId];
    }
   
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            model.status = @(1);
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
            [[XPShowTipsTool shareInstance] showMsg:@"关注成功" ToView:self.view];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

-(void)cancelAttentionWithModel:(XPFansModel *)model indexPath:(NSIndexPath *)indexPath{
    
    NSString *url;
    if (self.entryType == 3) {
       url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_creation_disAttention_someOne,model.toUserId];
    }else if (self.entryType == 4){
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_creation_disAttention_someOne,model.userId];
    }else {
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_creation_disAttention_someOne,model.fromUserId];
    }
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            model.status = @(0);
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            [[XPShowTipsTool shareInstance] showMsg:@"取消成功" ToView:self.view];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)canShowEmptyView {
    if (self.dataArray.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}

- (NSString *)getPath {
    NSString *url;
    if (self.entryType == 1) {
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Creation_fans_list];
    }else if (self.entryType == 2 || self.entryType == 3){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Creation_Attention_list];
    }else if (self.entryType == 4){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCircle_comment_like_list];
    }
    return url;
}



@end
