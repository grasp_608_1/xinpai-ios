//
//  XPCreateShowViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/30.
//

#import "XPCreateShowViewController.h"
#import <WebKit/WebKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/PHPhotoLibrary.h>
#import "XPSpeechRecognitionManager.h"
#import "LVRecordTool.h"
#import "XPRecoardAnimationView.h"
#import "XPCreationOrderManager.h"

@interface XPCreateShowViewController ()<WKScriptMessageHandler,WKNavigationDelegate>

@property (nonatomic, strong) WKWebView *webView;
//语音识别工具类
@property (nonatomic, strong) XPSpeechRecognitionManager *speechRecognitionManager;
//录音按钮
@property (nonatomic, strong) UIButton *recordBtn;
//录音播放动画
@property (nonatomic, strong) XPRecoardAnimationView *recordAnimationView;
//识别动画按钮
@property (nonatomic, strong) UIProgressView *progressView;
// 录音工具
@property (nonatomic, strong) LVRecordTool *recordTool;

@end

@implementation XPCreateShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupViews];
    if (self.type == 3) {
        [[XPVerifyTool shareInstance] phoneAlbumCompleteBlock:^{
            
        }];
    }else  if (self.type == 5) {
        [[XPVerifyTool shareInstance] checkMicrophonePermission];
    }
    
    NSLog(@"url is %@",self.urlString);
    
//    _urlString = @"http://192.168.1.5:10882/#/pages/AddText/AddText";
    [self loadUrl:_urlString];
}
- (void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = self.titleStr;
    [self.view addSubview:self.navBarView];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)setupViews {
    // 创建 WKWebView 配置
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        // 创建消息控制器，处理来自 JS 的消息
        WKUserContentController *contentController = [[WKUserContentController alloc] init];
        [contentController addScriptMessageHandler:self name:@"appOrderConfirm"];
        [contentController addScriptMessageHandler:self name:@"appCallBack"];
        config.userContentController = contentController;
    
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - self.safeBottom) configuration:config];
    _webView.backgroundColor = UIColor.lightGrayColor;
    
    [self.view addSubview:self.webView];
    
    if (self.type == 5) {
        // 初始化语音识别管理器
        self.recordTool = [LVRecordTool sharedRecordTool];
        self.speechRecognitionManager = [[XPSpeechRecognitionManager alloc] init];
        
        self.recordBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.recordBtn.frame = CGRectMake(SCREEN_WIDTH - 24 - 48, SCREEN_HEIGHT - 24 - 48 - kTabBarHeight, 48, 48);
        [self.recordBtn setImage:[UIImage imageNamed:@"xp_circle_record"] forState:UIControlStateNormal];
        // 录音按钮
        [self.recordBtn addTarget:self action:@selector(recordBtnDidTouchDown:) forControlEvents:UIControlEventTouchDown];
        [self.recordBtn addTarget:self action:@selector(recordBtnDidTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [self.recordBtn addTarget:self action:@selector(recordBtnDidTouchDragExit:) forControlEvents:UIControlEventTouchDragExit];
        [self.view addSubview:self.recordBtn];
    }
}

- (void)loadUrl:(NSString *)urlString {
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

#pragma mark - WKScriptMessageHandler

// 处理来自 Web 页面的 JS 消息
- (void)userContentController:(WKUserContentController *)userContentController
            didReceiveScriptMessage:(WKScriptMessage *)message {
    if ([message.name isEqualToString:@"appOrderConfirm"]) {
        // 从 Web 页面接收到消息
        NSLog(@"Received message from Web: %@", message.body);
        // 用于接收错误信息
                NSError *error = nil;
                NSDictionary *dict = [self dictionaryFromJSONString:message.body error:&error];
                if (error) {
                    NSLog(@"Error parsing JSON: %@", error.localizedDescription);
                } else {
                    NSLog(@"Parsed Dictionary: %@", dict);
                    if (dict) {
                        [[XPCreationOrderManager shareInstance] orderWithParaM:dict Controller:self];
                    }
                }
    }else if([message.name isEqualToString:@"appCallBack"]){
        [self goBack];
    }
}

- (void)goBack {
    
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

//iOS 调用web方法
- (void)callJavaScriptFunctionWithParameter:(NSString *)parameter {
    // 这里我们传递一个字符串到 JavaScript 函数中
    NSString *script = [NSString stringWithFormat:@"window.vueApp.randerText('%@')", parameter];
    
    [self.webView evaluateJavaScript:script completionHandler:^(id result, NSError *error) {
        if (error) {
            NSLog(@"Error evaluating JavaScript: %@", error);
        } else {
            NSLog(@"JavaScript returned result: %@", result);
        }
    }];
}
#pragma mark - 录音按钮事件
// 按下
- (void)recordBtnDidTouchDown:(UIButton *)recordBtn {
    
    NSInteger status = [[XPVerifyTool shareInstance] checkMicrophonePermission];
    if (status == 1) {
        [[XPShowTipsTool shareInstance] showMsg:@"麦克风权限已关闭" ToView:k_keyWindow];
        return;
    }
    
    [self.recordAnimationView showAnimationView];
    
    [self.recordTool startRecording];
    
    
}

// 点击
- (void)recordBtnDidTouchUpInside:(UIButton *)recordBtn {
    XPWeakSelf
    double currentTime = self.recordTool.recorder.currentTime;
    NSLog(@"%lf", currentTime);
    
    [self.recordAnimationView dissmiss];
    self.recordAnimationView = nil;
    
    if (currentTime < 1) {
        
        [[XPShowTipsTool shareInstance] showMsg:@"说话声音太短" ToView:k_keyWindow];
        
        [self.recordTool stopRecording];
        [self.recordTool destructionRecordingFile];
        
    } else {
        [self.recordTool stopRecording];
        NSLog(@"已成功录音");
        //开始语音识别
        [self.speechRecognitionManager startRecognizeLocalAudioCompletion:^(NSString * _Nullable resultStr, NSInteger code) {
            if (code == 0) {
                NSLog(@"reslut ---- .%@",resultStr);
                [weakSelf callJavaScriptFunctionWithParameter:resultStr];
            }else{
                [[XPShowTipsTool shareInstance] showMsg:resultStr ToView:k_keyWindow];
            }
        }];
    }
}// 手指从按钮上移除
- (void)recordBtnDidTouchDragExit:(UIButton *)recordBtn {
    [self.recordAnimationView dissmiss];
    self.recordAnimationView = nil;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [self.recordTool stopRecording];
        [self.recordTool destructionRecordingFile];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[XPShowTipsTool shareInstance] showMsg:@"已取消录音" ToView:k_keyWindow];
        });
    });
    
}


#pragma mark -- lazy load
-(XPRecoardAnimationView *)recordAnimationView{
    if (_recordAnimationView == nil) {
       _recordAnimationView = [[XPRecoardAnimationView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width/2. - 50, self.view.bounds.size.height/2. - 50, 100, 100)];
        [k_keyWindow addSubview:_recordAnimationView];
    }
    return _recordAnimationView;
}

- (NSDictionary *)dictionaryFromJSONString:(NSString *)jsonString error:(NSError **)error {
    // 将字符串转换为 NSData
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    // 使用 NSJSONSerialization 解析 JSON 数据
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:error];
    
    return dict;
}

@end
