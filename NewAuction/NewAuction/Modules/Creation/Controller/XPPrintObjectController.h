//
//  XPPrintObjectController.h
//  NewAuction
//
//  Created by Grasp_L on 2025/2/8.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPPrintObjectController : XPBaseViewController
@property (nonatomic, strong) NSString *urlString;//加载的url
@end

NS_ASSUME_NONNULL_END
