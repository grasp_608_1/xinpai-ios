//
//  XPPrintObjectController.m
//  NewAuction
//
//  Created by Grasp_L on 2025/2/8.
//

#import "XPPrintObjectController.h"
#import <WebKit/WebKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/PHPhotoLibrary.h>
#import "XPSpeechRecognitionManager.h"
#import "LVRecordTool.h"
#import "XPRecoardAnimationView.h"
#import "XPPrintObjectController.h"

@interface XPPrintObjectController ()<WKScriptMessageHandler>

@property (nonatomic, strong) WKWebView *webView;
//语音识别工具类
@property (nonatomic, strong) XPSpeechRecognitionManager *speechRecognitionManager;
//录音按钮
@property (nonatomic, strong) UIButton *recordBtn;
//录音播放动画
@property (nonatomic, strong) XPRecoardAnimationView *recordAnimationView;
//识别动画按钮
@property (nonatomic, strong) UIProgressView *progressView;
// 录音工具
@property (nonatomic, strong) LVRecordTool *recordTool;

@end

@implementation XPPrintObjectController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupViews];
}
- (void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"打印";
    [self.view addSubview:self.navBarView];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadUrl:_urlString];
}

- (void)setupViews {
    // 创建 WKWebView 配置
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        
        // 创建消息控制器，处理来自 JS 的消息
        WKUserContentController *contentController = [[WKUserContentController alloc] init];
        config.userContentController = contentController;
    
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - self.safeBottom) configuration:config];
    _webView.backgroundColor = UIColor.lightGrayColor;
    
    [self.view addSubview:self.webView];

}

- (void)loadUrl:(NSString *)urlString {
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

- (void)goBack {
    
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)callJavaScriptFunctionWithParameter:(NSString *)parameter {
    // 这里我们传递一个字符串到 JavaScript 函数中
//    NSString *script = [NSString stringWithFormat:@"window.vueApp.randerText('%@')", parameter];
//    
//    [self.webView evaluateJavaScript:script completionHandler:^(id result, NSError *error) {
//        if (error) {
//            NSLog(@"Error evaluating JavaScript: %@", error);
//        } else {
//            NSLog(@"JavaScript returned result: %@", result);
//        }
//    }];
}

@end
