//
//  XPCreateShowViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/30.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPCreateShowViewController : XPBaseViewController
@property (nonatomic, strong) NSString *titleStr;//标题
@property (nonatomic, strong) NSString *urlString;//加载的url
// 1 手绘打印 2 快乐搭建 3 照片打印 4 印章打印 5 语音造字 6 我的创搭
@property (nonatomic, assign) NSInteger type;
@end

NS_ASSUME_NONNULL_END
