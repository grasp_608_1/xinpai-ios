//
//  XPCreationFansController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/1.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPCreationFansController : XPBaseViewController
//1 粉丝列表  2 关注列表 3 我的关注 4 圈子点赞评论
@property (nonatomic, assign) NSInteger entryType;

@property (nonatomic, assign) NSString *nickName;

@property (nonatomic, strong) NSNumber *userId;

@property (nonatomic, strong) NSNumber *circleId;

@end

NS_ASSUME_NONNULL_END
