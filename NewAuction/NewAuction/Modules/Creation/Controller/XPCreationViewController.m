//
//  XPCreationViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/6.
//

#import "XPCreationViewController.h"
#import "XPShopModel.h"
#import "XPShopCollectionViewCell.h"
#import "XPCreateTopView.h"
#import "XPCreateShowViewController.h"
#import "XPShopingController.h"
#import "LVRecordTool.h"
#import "XPRecoardAnimationView.h"
#import "XPSpeechRecognitionManager.h"
#import "XPRecordCommandProductView.h"
#import "XPShopGoodsDetailController.h"

@interface XPCreationViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) XPCreateTopView *topView;
//当前页码
@property (nonatomic, assign) NSInteger page;
// 录音工具
@property (nonatomic, strong) LVRecordTool *recordTool;
//录音按钮
@property (nonatomic, strong) UIButton *recordBtn;
//录音播放动画
@property (nonatomic, strong) XPRecoardAnimationView *recordAnimationView;
//语音识别工具类
@property (nonatomic, strong) XPSpeechRecognitionManager *speechRecognitionManager;
//语音识别后推荐课程
@property (nonatomic, strong) XPRecordCommandProductView *productPopView;


@end

@implementation XPCreationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.page = 1;
    self.recordTool = [LVRecordTool sharedRecordTool];
    self.speechRecognitionManager = [[XPSpeechRecognitionManager alloc] init];
    [self setupViews];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getData];
//    [self getCurrentListData];
}
-(void)setupNavBarView {
    if (self.enterType == 1) {
        self.navBarView = [[XPNavTopView alloc] init];
        [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        self.navBarView.titleLabel.text = @"语音造物";
        [self.view addSubview:self.navBarView];
    }
}

-(void)setupViews{
    
    CGFloat margin = 13;
    CGFloat itemW = (SCREEN_WIDTH - margin*3)/2.;
    CGFloat itemH = 264;
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(itemW,itemH);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    CGRect rect = CGRectZero;
    if (self.enterType == 0) {
        rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kTabBarHeight);
    }else if (self.enterType == 1){
        rect = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    }
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
    self.collectionView.showsVerticalScrollIndicator = NO;
    [self.collectionView registerClass:[XPShopCollectionViewCell class] forCellWithReuseIdentifier:@"XPShopMainController"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"XPShopMainControllerHeader"];


    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = UIColorFromRGB(0xF8F8FF);
//    self.collectionView.bounces = NO;
    self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:self.collectionView];
    
    self.topView = [[XPCreateTopView alloc] initWithFrame:CGRectMake(0, 0, self.collectionView.mj_w, 0)];
    [self.collectionView addSubview:self.topView];
    CGFloat topH = 0;
    self.topView.frame = CGRectMake(0, -topH, self.collectionView.mj_w, 0);
    [self topCallBack];
    self.collectionView.contentInset = UIEdgeInsetsMake(topH, 0, 0, 0);
    self.collectionView.contentOffset = CGPointMake(0, -topH);
    
    self.recordBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.recordBtn.frame = CGRectMake(SCREEN_WIDTH - 24 - 48, SCREEN_HEIGHT - 24 - 48 - kTabBarHeight, 48, 48);
    [self.recordBtn setImage:[UIImage imageNamed:@"xp_circle_record"] forState:UIControlStateNormal];
    // 录音按钮
    [self.recordBtn addTarget:self action:@selector(recordBtnDidTouchDown:) forControlEvents:UIControlEventTouchDown];
    [self.recordBtn addTarget:self action:@selector(recordBtnDidTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    [self.recordBtn addTarget:self action:@selector(recordBtnDidTouchDragExit:) forControlEvents:UIControlEventTouchDragExit];
    [self.view addSubview:self.recordBtn];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat itemW = 170;
    CGFloat itemH = 268;
    CGSize size = CGSizeMake(itemW,itemH);
    
    return size;
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableView = nil;
       //返回段头段尾视图
       if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
           UICollectionReusableView *header=[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"XPShopMainControllerHeader" forIndexPath:indexPath];
           [header.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
           //添加头视图的内容
           UIImageView *titleImageView = [[UIImageView alloc] init];
           titleImageView.image = [UIImage imageNamed:@"xp_product_comment"];
           titleImageView.frame = CGRectMake(self.collectionView.mj_w/2 - 58,12, 116, 24);
           
           [header addSubview:titleImageView];
           reusableView = header;

           return reusableView;
       }
    return nil;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    XPShopCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"XPShopMainController" forIndexPath:indexPath];
    XPShopModel *model = self.dataArray[indexPath.row];
//    cell.model = model;
    [cell viewWithModel:model];
    cell.backgroundColor = UIColor.whiteColor;
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return (SCREEN_WIDTH - 170*2)/3.;
}

//cell的最小列间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(12, (SCREEN_WIDTH - 170*2)/3., 0, (SCREEN_WIDTH - 170*2)/3.);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

//header
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
//    return CGSizeMake(self.collectionView.bounds.size.width, 48);
    return CGSizeMake(0, 0);
}

#pragma mark -- top clicked callback
- (void)topCallBack{
    XPWeakSelf;
    //轮播图点击
    [self.topView setBannerSelectBlock:^(NSDictionary * _Nonnull dic) {
        [XPRoutetool entryWithDic:dic];
    }];
    
    //3d 项目点击
    [self.topView setItemSelectBlock:^(NSDictionary * _Nonnull dic) {
        if (![[XPVerifyTool shareInstance] checkLogin]) {
            [[XPVerifyTool shareInstance] userShouldLoginFirst];
            return;
        }
        
        if (dic[@"link"] && dic[@"name"]) {
            NSString *targetUrl = [NSString stringWithFormat:@"%@?jumpToken=%@&isAppOrder=1",dic[@"link"],[XPLocalStorage readUserToken]];
            [weakSelf openUrl:targetUrl title:dic[@"name"] type:dic[@"id"]];
        }
    }];
    
    //人气商城点击
    [self.topView setHotitemSelectBlock:^(NSDictionary * _Nonnull dic) {
        XPShopingController *shopVC = [XPShopingController new];
        shopVC.enterType = 7;
        shopVC.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:shopVC animated:YES];
    }];
    
}

- (void)openUrl:(NSString *)url title:(NSString *)title type:(NSNumber *)type{
    XPCreateShowViewController *webView = [XPCreateShowViewController new];
    webView.urlString = url;
    webView.titleStr = title;
    webView.type = type.intValue;
    webView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webView animated:YES];
}

#pragma mark --- scrollview delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.collectionView) {
//        NSLog(@"--- %@",NSStringFromCGPoint(scrollView.contentOffset));
        
    }
}

#pragma mark - 录音按钮事件
// 按下
- (void)recordBtnDidTouchDown:(UIButton *)recordBtn {
    
    NSInteger status = [[XPVerifyTool shareInstance] checkMicrophonePermission];
    if (status == 1) {
        [[XPShowTipsTool shareInstance] showMsg:@"麦克风权限已关闭" ToView:k_keyWindow];
        return;
    }
    
    if (![[XPVerifyTool shareInstance] checkLogin]) {
        [[XPVerifyTool shareInstance] userShouldLoginFirst];
        return;
    }
    
    
    [self.recordAnimationView showAnimationView];
    
    [self.recordTool startRecording];
    
    
}

// 点击
- (void)recordBtnDidTouchUpInside:(UIButton *)recordBtn {
   
    if (![[XPVerifyTool shareInstance] checkLogin]) {
        [[XPVerifyTool shareInstance] userShouldLoginFirst];
        return;
    }
    
    double currentTime = self.recordTool.recorder.currentTime;
    NSLog(@"%lf", currentTime);
    XPWeakSelf
    [self.recordAnimationView dissmiss];
    self.recordAnimationView = nil;
    
    if (currentTime < 1) {
        
        [[XPShowTipsTool shareInstance] showMsg:@"说话声音太短" ToView:k_keyWindow];
        
        [self.recordTool stopRecording];
        [self.recordTool destructionRecordingFile];
        
    } else {
        [self.recordTool stopRecording];
        NSLog(@"已成功录音");
        //开始语音识别
        [self.speechRecognitionManager startRecognizeLocalAudioCompletion:^(NSString * _Nullable resultStr, NSInteger code) {
            NSLog(@"reslut ---- .%@",resultStr);
            if (code == 0) {
                [weakSelf searchWithKeyWords:resultStr];
            }else{
                [[XPShowTipsTool shareInstance] showMsg:resultStr ToView:k_keyWindow];
            }
            
        }];
    }
}

// 手指从按钮上移除
- (void)recordBtnDidTouchDragExit:(UIButton *)recordBtn {
    [self.recordAnimationView dissmiss];
    self.recordAnimationView = nil;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [self.recordTool stopRecording];
        [self.recordTool destructionRecordingFile];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[XPShowTipsTool shareInstance] showMsg:@"已取消录音" ToView:k_keyWindow];
        });
    });
}
#pragma mark --- 语音搜索后推荐课程
- (void)showCommandProduct:(NSDictionary *)dic{
    [self.productPopView showAnimationViewWithDic:dic];
    XPWeakSelf
    [self.productPopView setSureBlock:^{
        XPShopGoodsDetailController *detailVC = [XPShopGoodsDetailController new];
        detailVC.productSkuId = dic[@"productSkuId"];
        detailVC.voiceSearchStatus = @(1);
        detailVC.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:detailVC animated:YES];
        [weakSelf.productPopView removeFromSuperview];
        weakSelf.productPopView = nil;
    }];
    
    [self.productPopView setCancelBlock:^{
        [weakSelf.productPopView removeFromSuperview];
        weakSelf.productPopView = nil;
    }];
}

-(XPRecoardAnimationView *)recordAnimationView{
    if (_recordAnimationView == nil) {
       _recordAnimationView = [[XPRecoardAnimationView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width/2. - 50, self.view.bounds.size.height/2. - 50, 100, 100)];
        [k_keyWindow addSubview:_recordAnimationView];
    }
    return _recordAnimationView;
}

-(XPRecordCommandProductView *)productPopView {
    if (_productPopView == nil) {
        _productPopView = [[XPRecordCommandProductView alloc] initWithFrame:k_keyWindow.bounds];
        [k_keyWindow addSubview:_productPopView];
    }
    return _productPopView;
}
#pragma mark --- network
- (void)getData{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCreate_main_data];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            CGFloat topH = [self.topView viewWithData:data.userData];
            self.topView.frame = CGRectMake(0, -topH, self.collectionView.mj_w, topH);
            self.collectionView.contentInset = UIEdgeInsetsMake(topH, 0, 0, 0);
            self.collectionView.contentOffset = CGPointMake(0, -topH);
//            [self.collectionView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

- (void)getCurrentListData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_data];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSString stringWithFormat:@"%zd",self.page];
    paraM[@"pageSize"] = @(10);
    
    paraM[@"categoryId"] = @(0);
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.dataArray = [XPShopModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            [self.collectionView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

- (void)headerRefresh {
    self.page = 1;
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_data];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSString stringWithFormat:@"%zd",self.page];
    paraM[@"pageSize"] = @(10);
    paraM[@"categoryId"] = @(0);
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.collectionView.mj_header endRefreshing];
        if (data.isSucess) {
            
            [self.dataArray removeAllObjects];
            self.dataArray = [XPShopModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            [self.collectionView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

- (void)footerRefresh {
    
    self.page ++;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_data];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSString stringWithFormat:@"%zd",self.page];
    paraM[@"pageSize"] = @(10);
    paraM[@"categoryId"] = @(0);
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.collectionView.mj_footer endRefreshing];
        if (data.isSucess) {
            
            NSArray *modelArray = [XPShopModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            if (modelArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self.collectionView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

- (void)searchWithKeyWords:(NSString *)keyWords{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCreate_command_product];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"keywords"] = keyWords;
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSNumber  *remainingSearchNum = data.userData[@"remainingSearchNum"];
            if (remainingSearchNum.intValue > 0) {
                [self showCommandProduct:data.userData];
            }else{
                [[XPShowTipsTool shareInstance] showMsg:@"当日搜索次数已达上限" ToView:self.view];
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

@end
