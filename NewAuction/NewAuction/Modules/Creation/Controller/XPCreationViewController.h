//
//  XPCreationViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/6.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPCreationViewController : XPBaseViewController
//创建初始状态 1作为首页,带有tabbar 2 不带tabbar
@property (nonatomic, assign) NSInteger enterType;

@end

NS_ASSUME_NONNULL_END
