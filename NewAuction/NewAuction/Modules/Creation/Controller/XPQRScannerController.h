//
//  XPQRScannerController.h
//  NewAuction
//
//  Created by Grasp_L on 2025/2/7.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPQRScannerController : XPBaseViewController

@property (nonatomic, copy) void(^block)(NSString *str);

@end

NS_ASSUME_NONNULL_END
