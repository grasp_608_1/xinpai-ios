//
//  XPCreationAuthorController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/1.
//

#import "XPCreationAuthorController.h"
#import "XPShopGoodsDetailController.h"
#import "XPCreationAuthorView.h"
#import "XPCreationCollectionViewCell.h"
#import "XPMakerProductModel.h"
#import "XPShopSpecPopView.h"
#import "XPHitButton.h"
#import "XPShopCarItemModel.h"
#import "XPShopOrderConfirmModel.h"
#import "XPShopOrderConfirmController.h"
#import "XPShopGoodsDetailController.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "CHTCollectionViewWaterfallHeader.h"
#import "CHTCollectionViewWaterfallFooter.h"
#import "XPCreationFansController.h"
#import "XPHitButton.h"
#import "XPEmptyView.h"
#import "XPMakerBadgeController.h"

#define XPAuthor_HEADER @"XPAuthorHeaderID"
#define XPAuthor_FOOTER @"XPAuthorFooterID"
#define XPAuthor_CELLID @"XPAuthorController"

@interface XPCreationAuthorController ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>
@property (nonatomic, strong) UICollectionView *collectionView;
//推荐商品列表
@property (nonatomic, strong) NSMutableArray *dataArray;
//购物车UI
@property (nonatomic, strong) XPCreationAuthorView *topView;
//规格弹窗
@property (nonatomic, strong) XPShopSpecPopView *specPopView;
//当前页码
@property (nonatomic, assign) NSInteger page;
//cell高度
@property (nonatomic, strong) NSArray *heightArray;
//返回按钮
@property (nonatomic, strong) XPHitButton *backButton;

@property (nonatomic, copy) NSDictionary *topData;

@property (nonatomic, strong) XPEmptyView *emptyView;

@end

@implementation XPCreationAuthorController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.page = 1;
    [self setupViews];
    self.dataArray = [NSMutableArray array];
    [self getcommendData];
}

-(void)setupViews{
    CGFloat authenH =  278 + 68;
    
    self.topView = [[XPCreationAuthorView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, authenH)];
    self.topView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [self.topView.settingButton addTarget:self action:@selector(attentionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.topView];
    
    CHTCollectionViewWaterfallLayout *flowLayout = [[CHTCollectionViewWaterfallLayout alloc] init];
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 12, 0, 12);
    flowLayout.headerHeight = 0;
    flowLayout.footerHeight = self.safeBottom;
    flowLayout.minimumColumnSpacing = 12;
    flowLayout.minimumInteritemSpacing = 12;
    
    CGRect rect = CGRectMake(0, MaxY(self.topView), SCREEN_WIDTH, SCREEN_HEIGHT - MaxY(self.topView));
    self.collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
    self.collectionView.showsVerticalScrollIndicator = NO;
    [self.collectionView registerClass:[XPCreationCollectionViewCell class] forCellWithReuseIdentifier:XPAuthor_CELLID];
    [self.collectionView registerClass:[CHTCollectionViewWaterfallHeader class]
        forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader
               withReuseIdentifier:XPAuthor_HEADER];
    [self.collectionView registerClass:[CHTCollectionViewWaterfallFooter class]
        forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter
               withReuseIdentifier:XPAuthor_FOOTER];

    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = UIColor.whiteColor;
    self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:self.collectionView];
    self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    
    self.backButton = [[XPHitButton alloc] init];
    self.backButton.frame = CGRectMake(20,self.safeTop, 27, 27);
    self.backButton.hitRect = 50;
    [self.backButton setImage:[UIImage imageNamed:@"back_gray"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.backButton];
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无数据"];
    [self.view addSubview:self.emptyView];

    [self topCallBack];
}

- (void)freshUI{
    
    [self.topView  viewWithDict:self.topData];
    
    CGFloat topH =  278 + 68;
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView.frame = CGRectMake(62, topH + self.safeTop + 60, emptyW,emptyW + 27);
}

- (void)attentionButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    NSNumber *status = self.topData[@"status"];
    if (status && status.intValue == 1) {
        [self cancelAttention];
    }else {
        [self addAttention];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.page = 1;
    [self getTopData];
    
    
}

-(void)addAttention {
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_creation_attention_someOne,self.authorUserId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [self getTopData];
            [[XPShowTipsTool shareInstance] showMsg:@"关注成功" ToView:self.view];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

-(void)cancelAttention {
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_creation_disAttention_someOne,self.authorUserId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [self getTopData];
            [[XPShowTipsTool shareInstance] showMsg:@"取消成功" ToView:self.view];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//顶部数据
- (void)getTopData{
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_creation_author,self.authorUserId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.topData = data.userData;
            [self freshUI];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//作者模型数据
- (void)getcommendData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSString stringWithFormat:@"%zd",self.page];
    paraM[@"pageSize"] = @(10);
    paraM[@"makerId"] = self.authorUserId;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.dataArray = [XPMakerProductModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            [self getCellHeightArray];
            [self.collectionView reloadData];
            
            if (self.dataArray.count == 0) {
                self.emptyView.hidden = NO;
            }else {
                self.emptyView.hidden = YES;
            }
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

- (void)headerRefresh {
        
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    paraM[@"makerId"] = self.authorUserId;
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        [self.collectionView.mj_header endRefreshing];
        if (data.isSucess) {
            self.dataArray = [XPMakerProductModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            [self getCellHeightArray];
            [self.collectionView reloadData];
            
            if (self.dataArray.count == 0) {
                self.emptyView.hidden = NO;
            }else {
                self.emptyView.hidden = YES;
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kMaker_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSString stringWithFormat:@"%zd",self.page];
    paraM[@"pageSize"] = @(20);
    paraM[@"makerId"] = self.authorUserId;
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.collectionView.mj_footer endRefreshing];
        if (data.isSucess) {
            
            NSArray *modelArray = [XPMakerProductModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            if (modelArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self getCellHeightArray];
                [self.collectionView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}
//获取cell高度
- (void)getCellHeightArray{
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPCreationCollectionViewCell *cell = XPCreationCollectionViewCell.new;
    for (int i = 0; i<self.dataArray.count; i++) {
        XPMakerProductModel *model = self.dataArray[i];
        CGFloat height = [cell cellWithModel:model];
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    self.heightArray = tempArray.copy;
}
#pragma mark -- block callBack

- (void)topCallBack {
    XPWeakSelf
    [self.topView setFansBlock:^(NSInteger tag) {
        if (tag == 0) {
            XPCreationFansController *vc = [XPCreationFansController new];
            vc.entryType = 2;
            vc.nickName = weakSelf.topData[@"nickname"];
            vc.userId = weakSelf.topData[@"id"];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else if(tag == 1){
            XPCreationFansController *vc = [XPCreationFansController new];
            vc.entryType = 1;
            vc.nickName = weakSelf.topData[@"nickname"];
            vc.userId = weakSelf.topData[@"id"];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else if(tag == 2){
            XPMakerBadgeController *vc = [XPMakerBadgeController new];
            vc.userId = weakSelf.topData[@"id"];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else if (tag == 3){
            //作者作品,不用跳转
        }
        
    }];
}

#pragma mark -- UICollectionView delegate dataSorce
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *height = self.heightArray[indexPath.row];
    
    return CGSizeMake(170, height.floatValue);
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableView = nil;
    //返回段头段尾视图
    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
      reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                        withReuseIdentifier:XPAuthor_HEADER
                                                               forIndexPath:indexPath];
        
//        CHTCollectionViewWaterfallHeader *header = (CHTCollectionViewWaterfallHeader *)reusableView;
//        header.backgroundColor = UIColor.whiteColor;
//        header.titleLabel.text = @"作者模型";
//        header.titleLabel.textColor = app_font_color;
//        header.titleLabel.font = kFontMedium(14);
        
    }
    else if ([kind isEqualToString:CHTCollectionElementKindSectionFooter]) {
      reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                        withReuseIdentifier:XPAuthor_FOOTER
                                                               forIndexPath:indexPath];
        reusableView.backgroundColor = UIColor.whiteColor;
    }

    return reusableView;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    XPCreationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:XPAuthor_CELLID forIndexPath:indexPath];
    XPMakerProductModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    
    cell.backgroundColor = UIColor.whiteColor;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    XPMakerProductModel *model = self.dataArray[indexPath.row];
    XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
    vc.hidesBottomBarWhenPushed = YES;
    vc.productSkuId = model.productSkuId;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
