//
//  XPCreationOrderManager.h
//  NewAuction
//
//  Created by Grasp_L on 2025/2/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPCreationOrderManager : NSObject

+ (instancetype)shareInstance;

-(void)orderWithParaM:(NSDictionary *)paraM Controller:(XPBaseViewController *)controller ;

@end

NS_ASSUME_NONNULL_END
