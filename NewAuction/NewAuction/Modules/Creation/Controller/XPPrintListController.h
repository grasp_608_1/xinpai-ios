//
//  XPPrintListController.h
//  NewAuction
//
//  Created by Grasp_L on 2025/2/6.
//

#import <Foundation/Foundation.h>
#import "XPPrintRecoardModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface XPPrintListController : XPBaseViewController
//1 选择打印机
@property (nonatomic, assign) NSInteger entryType;

@property (nonatomic, copy) void(^selectBlock)(XPPrintRecoardModel *model);


@end

NS_ASSUME_NONNULL_END
