//
//  XPCreationOrderManager.m
//  NewAuction
//
//  Created by Grasp_L on 2025/2/8.
//

#import "XPCreationOrderManager.h"
#import "XPShopOrderConfirmModel.h"
#import "XPShopOrderConfirmController.h"


@interface XPCreationOrderManager ()

@property (nonatomic,strong) XPBaseViewController *controller;

@end

static XPCreationOrderManager *instance;

@implementation XPCreationOrderManager

+ (instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

-(void)orderWithParaM:(NSDictionary *)paraM Controller:(XPBaseViewController *)controller {
    self.controller = controller;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderConfirmBuy];

    [controller startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [controller stopLoadingGif];
        if (data.isSucess) {
        
            XPShopOrderConfirmModel *model = [XPShopOrderConfirmModel mj_objectWithKeyValues:data.userData];
            XPShopOrderConfirmController *vc = [XPShopOrderConfirmController new];
            model.singleQuality = 1;
            vc.model = model;
            if (paraM[@"gcodeUrl"]) {
                vc.gCodeStr = paraM[@"gcodeUrl"];
            }
            [controller.navigationController pushViewController:vc animated:YES];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
    
}



@end
