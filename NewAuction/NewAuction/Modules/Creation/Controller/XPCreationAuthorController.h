//
//  XPCreationAuthorController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/1.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPCreationAuthorController : XPBaseViewController

@property (nonatomic, strong) NSNumber *authorUserId;

@end

NS_ASSUME_NONNULL_END
