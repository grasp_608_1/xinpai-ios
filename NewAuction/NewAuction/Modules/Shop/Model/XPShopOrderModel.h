//
//  XPShopOrderModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopOrderModel : NSObject


//订单状态 0 默认 1 待付款 2 已支付|待发货 3 待收货|已发货 4 已完成 5 已取消 6 已关闭
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) NSNumber *afterStatus;//评价状态 0未评价 1 已评价
@property (nonatomic, strong) NSNumber *productSkuId;//商品SKu id
@property (nonatomic, strong) NSNumber *afterId;//售后id
@property (nonatomic, strong) NSNumber *accountAmount;//账户金额
@property (nonatomic, strong) NSNumber *payAmount;//现金支付
@property (nonatomic, strong) NSNumber *spendAllAmount;//实付总金额
@property (nonatomic, strong) NSNumber *scoreAmout;//积分
@property (nonatomic, strong) NSNumber *currentPoints;//剩余积分
@property (nonatomic, strong) NSNumber *pointsAmount;//积分可抵扣金额
@property (nonatomic, strong) NSNumber *pointsUse;//最多抵扣
@property (nonatomic, strong) NSNumber *productSkuAmount;//商品总价
@property (nonatomic, strong) NSNumber *freightAmount;//运费
@property (nonatomic, strong) NSNumber *activityAmount;//活动减去的金额
@property (nonatomic, strong) NSNumber *reduceAllAmount;//共减
@property (nonatomic, strong) NSNumber *couponAmount;//优惠券金额
@property (nonatomic, strong) NSNumber *fundsAmount;//账户余额
@property (nonatomic, strong) NSNumber *packageStatus;//包裹状态 0 单包裹 1 多包裹
@property (nonatomic, strong) NSNumber *deliveryStatus;//发货状态 0 未发货 1 部分发货 2 全部发货
@property (nonatomic, strong) NSNumber *presaleStatus;//预售
@property (nonatomic, strong) NSNumber *groupStatus;//拼团
@property (nonatomic, strong) NSNumber *modelStatus;//造物商品  0 普通商品 1 造物商品
@property (nonatomic, strong) NSNumber *pointStatus;//商品类型 0其他商品 1 积分商品
@property (nonatomic, strong) NSNumber *pricePoint;//兑换所需积分

@property (nonatomic, copy) NSString *receiveTime;//完成时间
@property (nonatomic, copy) NSString *deliveryTime;//快递到达时间
@property (nonatomic, copy) NSString *receiverAddress;//收货地址
//@property (nonatomic, copy) NSString *pointsAmount;//积分满减
@property (nonatomic, copy) NSString *deliveryCode;
@property (nonatomic, copy) NSString *deliverySn;//
@property (nonatomic, copy) NSString *deliveryCompany;//快递公司
@property (nonatomic, copy) NSString *receiverPhone;//收货人手机号
@property (nonatomic, copy) NSString *payTime;
@property (nonatomic, copy) NSNumber *payType;
@property (nonatomic, copy) NSString *receiverName;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *productId;
@property (nonatomic, copy) NSString *orderSn;
@property (nonatomic, copy) NSString *message;//订单备注

@property (nonatomic, copy) NSArray *mallOrderItemListVOS;//单商品列表
@property (nonatomic, copy) NSArray *mallOrderPackageListVOS;//包裹拆弹信息
@property (nonatomic, copy) NSDictionary *appKuaiDi100FindOrderDTO;//快递信息
@property (nonatomic, copy) NSDictionary *orderPrepareVO;//预售
@property (nonatomic, copy) NSDictionary *orderGroupVO;//拼团



//拼团相关
@property (nonatomic, strong) NSNumber *groupPrice;
@property (nonatomic, strong) NSNumber *groupPeopleNumber;
@property (nonatomic, strong) NSNumber *groupPresentNumber;
@property (nonatomic, strong) NSNumber *groupPeopleNumberTotal;
//SKU匹配的可拼团列表
@property (nonatomic, copy)   NSArray *skuActivityGroupVOs;
//定金相关
@property (nonatomic, strong) NSNumber *presaleAmount;//预售促销-预售价
@property (nonatomic, strong) NSNumber *presaleDepositAmount;//预售促销-定金
@property (nonatomic, strong) NSNumber *presaleDiscountAmount;//预售促销-抵扣
@property (nonatomic, strong) NSNumber *presaleBalanceAmount;//预售促销-尾款
@property (nonatomic, strong) NSNumber *presalePayStatus;//预售-支付状态 0 未支付 1 已付定金未支付尾款(到达尾款支付时间) 2 已付尾款 10 已付定金未支付尾款(未到达尾款支付时间)

@property (nonatomic, copy) NSString *presaleDepositStartTime;//预售促销-开始时间(定金支付)
@property (nonatomic, copy) NSString *presaleDepositEndTime;//预售促销-结束时间(定金支付)
@property (nonatomic, copy) NSString *presaleBalanceStartTime;//预售促销-开始时间(尾款支付)
@property (nonatomic, copy) NSString *presaleBalanceEndTime;//预售促销-结束时间(尾款支付)
@property (nonatomic, copy) NSString *presaleBalanceDescribe;//尾款描述






@end

NS_ASSUME_NONNULL_END
