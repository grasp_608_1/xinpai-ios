//
//  XPShopProductDetailModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/14.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopProductDetailModel : NSObject

@property (nonatomic, strong) NSNumber *productId;//商品id
@property (nonatomic, strong) NSNumber *commentRate;//好评率
@property (nonatomic, strong) NSNumber *attributeNum;//属性数量
@property (nonatomic, strong) NSNumber *commentNum;//评论数
@property (nonatomic, strong) NSNumber *productCategoryId;//商品分类id
@property (nonatomic, strong) NSNumber *isFavorite;//商品是否收藏 0 未收藏, 1收藏
@property (nonatomic, strong) NSNumber *modelStatus;//1 造物商品 0 普通商品
@property (nonatomic, strong) NSNumber *groupStatus;//拼团
@property (nonatomic, strong) NSNumber *presaleStatus;//预售
@property (nonatomic, strong) NSNumber *pointStatus;//商品类型 0其他商品 1 积分商品
@property (nonatomic, strong) NSNumber *modelUserId;//3d 作者ID
@property (nonatomic, strong) NSNumber *makerId;//创客ID

@property (nonatomic, strong) NSNumber *pricePoint;//兑换所需积分
@property (nonatomic, strong) NSNumber *sale;//销量
@property (nonatomic, strong) NSNumber *stock;//库存
@property (nonatomic, strong) NSNumber *brandId;//品牌id
@property (nonatomic, strong) NSNumber *productSkuId;//商品skuid
@property (nonatomic, strong) NSNumber *activityId;//商品skuid

@property (nonatomic, copy)   NSString *productName;//商品名称
@property (nonatomic, copy)   NSString *keywords;//关键字
@property (nonatomic, copy)   NSString *attributeMergeJson;//属性拼接串
@property (nonatomic, copy)   NSString *activityNote;//商品活动
@property (nonatomic, copy)   NSString *activityNoteLong;//商品活动
@property (nonatomic, copy)   NSString *activityRemark;//商品活动描述
@property (nonatomic, copy)   NSString *freightNote;//邮费描述
@property (nonatomic, copy)   NSString *freightRemark;//邮费标记


@property (nonatomic, copy)   NSArray  *productSkuShortInfoVOs;
@property (nonatomic, copy)   NSArray  *productImagesVOs;//商品图片集合
@property (nonatomic, copy)   NSArray  *userCouponVOs;//用户可领取优惠卷
@property (nonatomic, copy)   NSArray  *productImagesRotationVOs;//商品轮播图片集合
@property (nonatomic, copy)   NSArray  *orderActivityVOs;//活动列表

@property (nonatomic, copy)   NSDictionary  *productCommentVO;//商品好评推荐评价
@property (nonatomic, copy)   NSDictionary  *userAddressVO;//用户默认收货地址
@property (nonatomic, copy)   NSDictionary  *productSkuShortInfoVO;//商品所包含规格数据
@property (nonatomic, copy)   NSDictionary  *productChartsVO;//排行版




@end

NS_ASSUME_NONNULL_END
