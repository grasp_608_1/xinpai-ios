//
//  XPShopProductBuyModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopProductBuyModel : NSObject

@property (nonatomic, strong) NSNumber *productCategoryId;
@property (nonatomic, strong) NSNumber *productId;
@property (nonatomic, strong) NSNumber *priceCost;
@property (nonatomic, strong) NSNumber *priceSale;//直接购买价
@property (nonatomic, strong) NSNumber *productSkuId;
@property (nonatomic, strong) NSNumber *cartItemId;
@property (nonatomic, strong) NSNumber *productQuantity;
@property (nonatomic, strong) NSNumber *pointStatus;//是否为积分兑换商品
@property (nonatomic, strong) NSNumber *pricePoint;//积分兑换所需单价


@property (nonatomic, copy) NSString *pic;
@property (nonatomic, copy) NSString *productName;
@property (nonatomic, copy) NSString *attributeValues;

@property (nonatomic, copy) NSArray *skuActivityCouponNoteVOs;

@end

NS_ASSUME_NONNULL_END
