//
//  XPNewsModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPNewsModel : NSObject

@property (nonatomic, strong) NSNumber *productId;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *brief;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *newsTime;
@property (nonatomic, copy) NSString *image;

@end

NS_ASSUME_NONNULL_END
