//
//  XPShopPreSaleModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopPreSaleModel : NSObject

@property (nonatomic, strong) NSNumber *presalePrice;//预售促销-预售价
@property (nonatomic, strong) NSNumber *presaleDepositPrice;//预售促销-定金
@property (nonatomic, strong) NSNumber *presaleDiscountPrice;//预售促销-抵扣
@property (nonatomic, strong) NSNumber *presaleBalancePrice;//预售促销-尾款
@property (nonatomic, strong) NSNumber *presalePayStatus;//预售-支付状态 0 未支付 1 已付定金未支付尾款(到达尾款支付时间) 2 已付尾款 10 已付定金未支付尾款(未到达尾款支付时间)

@property (nonatomic, copy) NSString *presaleDepositStartTime;//预售促销-开始时间(定金支付)
@property (nonatomic, copy) NSString *presaleDepositEndTime;//预售促销-结束时间(定金支付)
@property (nonatomic, copy) NSString *presaleBalanceStartTime;//预售促销-开始时间(尾款支付)
@property (nonatomic, copy) NSString *presaleBalanceEndTime;//预售促销-结束时间(尾款支付)
@property (nonatomic, copy) NSString *presaleBalanceDescribe;//尾款描述

@end

NS_ASSUME_NONNULL_END
