//
//  XPXPShopOrderProductListModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopOrderProductListModel : NSObject

@property (nonatomic, strong) NSString *defaultId;

@property (nonatomic, strong) NSNumber *productSkuAmount;
@property (nonatomic, strong) NSNumber *couponAmount;
@property (nonatomic, strong) NSNumber *productQuantity;
@property (nonatomic, strong) NSNumber *productCommentStatus;
@property (nonatomic, strong) NSNumber *payAmount;//现金支付
@property (nonatomic, strong) NSNumber *accountAmount;//账户余额支付
@property (nonatomic, strong) NSNumber *deliveryStatus;
@property (nonatomic, strong) NSNumber *presaleStatus;//预售
@property (nonatomic, strong) NSNumber *groupStatus;//拼团
@property (nonatomic, strong) NSNumber *pointStatus;//商品类型 0其他商品 1 积分商品
@property (nonatomic, strong) NSNumber *pricePoint;//兑换所需积分

@property (nonatomic, strong) NSNumber *afterStatus;//是否已售后 0 未售后 1 已售后

@property (nonatomic, copy)   NSString *skuName;
@property (nonatomic, copy)   NSString *productName;
@property (nonatomic, copy)   NSString *attributeValues;;
@property (nonatomic, copy)   NSString *image;
//活动标签
@property (nonatomic, copy) NSString *activityNote;
//优惠券标签
@property (nonatomic, copy) NSString *couponNote;
//优惠券列表标签
@property (nonatomic, copy) NSArray *skuActivityCouponNoteVOs;

@property (nonatomic, assign) bool hasSelected;

@end

NS_ASSUME_NONNULL_END
