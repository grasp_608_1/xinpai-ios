//
//  XPShopAfterSaleModel.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/20.
//

#import "XPShopAfterSaleModel.h"

@implementation XPShopAfterSaleModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"descStr":@"description",@"afterSaleId":@"id"};
}
@end
