//
//  XPCouponModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPCouponModel : NSObject
@property (nonatomic, strong) NSNumber *amount;//优惠券金额
@property (nonatomic, strong) NSNumber *minPoint;//使用门槛
@property (nonatomic, strong) NSNumber *receiveStatus;//领取状态 0 未领取 1 已领取
@property (nonatomic, strong) NSNumber *type;//优惠卷类型 0 全部可用 1 单商品 2 单sku 3 单品类
@property (nonatomic, strong) NSNumber *couponId;//优惠券id

@property (nonatomic, copy)   NSString *name;
@property (nonatomic, copy)   NSString *startTime;//开始时间
@property (nonatomic, copy)   NSString *endTime;//结束时间
@property (nonatomic, copy)   NSString *note;//优惠券标签
@property (nonatomic, copy)   NSString *remark;//优惠券描述
@property (nonatomic, copy)   NSString *noteLong;//优惠券长标签

@end

NS_ASSUME_NONNULL_END
