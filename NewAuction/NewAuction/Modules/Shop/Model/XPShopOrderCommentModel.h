//
//  XPShopOrderCommentModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopOrderCommentModel : NSObject

@property (nonatomic, copy) NSString *recoardId;//记录id
@property (nonatomic, copy) NSString *productId;//商品id
@property (nonatomic, copy) NSString *productSkuId;//商品sku id
@property (nonatomic, copy) NSString *attributeValues;//属性

@property (nonatomic, copy) NSString *image;//商品图片
@property (nonatomic, copy) NSString *productName;//商品名
@property (nonatomic, copy) NSString *skuName;//规格名
@property (nonatomic, copy) NSString *pics;
//图片地址使用,分割
@property (nonatomic, copy) NSString *imagesUrlsString;
//输入文本是否为空
@property (nonatomic, copy) NSString *textStr;

@property (nonatomic, strong) NSNumber *type;//反馈类型
@property (nonatomic, strong) NSNumber *star;//星星

@end

NS_ASSUME_NONNULL_END
