//
//  XPShopAfterSaleModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopAfterSaleModel : NSObject

/// 申请类型：0 默认 1 仅退款； 2仅退款 3 退货退款
@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, strong) NSNumber *returnFunds;//账户金额
@property (nonatomic, strong) NSNumber *returnAmount;//实付款
@property (nonatomic, strong) NSNumber *returnSumAmount;//退款总额
@property (nonatomic, strong) NSNumber *accountAmount;//账户金额
@property (nonatomic, strong) NSNumber *payAmount;//现金支付
@property (nonatomic, strong) NSNumber *returnPoints;//jifen
@property (nonatomic, copy) NSString *mailingAddress;//商家地址
//退货数量
@property (nonatomic, strong) NSNumber *productCount;
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy)   NSString *afterSaleId;
/// 售后状态 0 未提交 1 待审核(仅退款通过->5;退款退货通过->2) 2 审核成功|待归仓  3 已拒绝 4 商品回寄中 5 退款处理 中 6 已完成
@property (nonatomic, strong) NSNumber *status;//
@property (nonatomic, copy)   NSString *refundTime;
@property (nonatomic, copy)   NSString *auditTime;
//退货原因
@property (nonatomic, copy)   NSString *descStr;
//申请时间
@property (nonatomic, copy)   NSString *createTime;
//反馈原因
@property (nonatomic, copy)   NSString *reason;
//反馈图片地址
@property (nonatomic, copy)   NSString *pics;
//退货快递单号
@property (nonatomic, copy)   NSString *sendDeliverySn;
//快递编码
@property (nonatomic, copy)   NSString *sendDeliveryCompanyCode;
//快递公司名称
@property (nonatomic, copy)   NSString *sendDeliveryCompany;
//寄件手机号
@property (nonatomic, copy)   NSString *sendPhone;
//商品列表
@property (nonatomic, copy) NSArray *mallOrderItemListVOS;
//售后状态
@property (nonatomic, copy) NSArray *mallOrderAfterLogVOList;

@end

NS_ASSUME_NONNULL_END
