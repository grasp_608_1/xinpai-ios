//
//  XPShopSpecModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopSpecModel : NSObject

@property (nonatomic, copy) NSString *attributeValues;//规格
@property (nonatomic, copy) NSString *subTitle;//副标题,推荐标签(使用,分割)
@property (nonatomic, copy) NSString *productName;//商品名称
@property (nonatomic, copy) NSString *pic;//商品首图
@property (nonatomic, copy) NSString *sale;//销量
@property (nonatomic, strong) NSNumber *stock;//库存
@property (nonatomic, strong) NSNumber *isShow;//是否上架

@property (nonatomic, strong) NSNumber *productId;//商品id
@property (nonatomic, strong) NSNumber *priceMarket;//市场价|划线价
@property (nonatomic, strong) NSNumber *priceCost;//商品sku销售价 销售价
@property (nonatomic, strong) NSNumber *priceSale;//直接购买价
@property (nonatomic, strong) NSNumber *pricePoint;//积分兑换价
@property (nonatomic, strong) NSNumber *productSkuId;//商品skuid

@property (nonatomic, copy)   NSArray * skuActivityCouponNoteVOs;//活动
//轮播图
@property (nonatomic, copy) NSArray  *productImagesRotationVOs;

@end

NS_ASSUME_NONNULL_END
