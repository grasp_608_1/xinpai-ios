//
//  XPShopOrderConfirmModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopOrderConfirmModel : NSObject

@property (nonatomic, strong) NSNumber *productSkuAmount;//商品购买时sku金额
//优惠卷是否达到可用标准 0 未指定 1 可用;达到最低使用金额标准 2 不可用;未达最低使用金额标准
@property (nonatomic, strong) NSNumber *couponUsable;
@property (nonatomic, strong) NSNumber *couponAmount;//优惠卷抵扣金额
@property (nonatomic, strong) NSNumber *funds;//用户账户可用金额
@property (nonatomic, strong) NSNumber *accountAmount;//账户抵扣金额
@property (nonatomic, strong) NSNumber *payAmount;//实际支付金额
@property (nonatomic, strong) NSNumber *spendAllAmount;//需付款
@property (nonatomic, strong) NSNumber *freightAmount;//运费
@property (nonatomic, strong) NSNumber *activityAmount;//活动减去的金额
@property (nonatomic, strong) NSNumber *reduceAllAmount;//共减金额
@property (nonatomic, strong) NSNumber *activityId;//活动id
@property (nonatomic, strong) NSNumber *scoreAmout;//积分
@property (nonatomic, strong) NSNumber *currentPoints;//剩余积分
@property (nonatomic, strong) NSNumber *pointsAmount;//积分可抵扣金额
@property (nonatomic, strong) NSNumber *pointsUse;//最多抵扣
@property (nonatomic, copy) NSString *activityRemark;//活动描述
@property (nonatomic, strong) NSNumber *presaleStatus;//预售
@property (nonatomic, strong) NSNumber *groupStatus;//拼团
@property (nonatomic, strong) NSNumber *presaleDepositAmount;//需付定金
@property (nonatomic, strong) NSNumber *pointStatus;//商品类型 0其他商品 1 积分商品
@property (nonatomic, strong) NSNumber *pricePoint;//兑换所需积分

@property (nonatomic, copy) NSString *activityNoteLong;
@property (nonatomic, copy) NSArray *orderItemVOs;//订单包含商品sku
@property (nonatomic, copy) NSDictionary *userAddressVO;//用户默认收货地址
@property (nonatomic, copy) NSDictionary *orderPrepareVO;//预售
@property (nonatomic, copy) NSDictionary *orderGroupVO;//拼团
@property (nonatomic, copy) NSArray *userUsableCouponVOs;//用户可用优惠卷
@property (nonatomic, copy) NSArray *orderActivityVOs;//多活动标签
@property (nonatomic, copy) NSString *activityNotes;//活动标签

//单商品的数量
@property (nonatomic, assign) NSInteger singleQuality;
//是否使用积分
@property (nonatomic, assign) bool scoreSelected;
@end

NS_ASSUME_NONNULL_END
