//
//  XPShopProductCommentModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopProductCommentModel : NSObject

@property (nonatomic, copy) NSString *userNickname;//评价人员昵称

@property (nonatomic, copy) NSString *content;//评论内容
@property (nonatomic, copy) NSString *pics;//评价图片地址
@property (nonatomic, copy) NSString *userHead;//评论用户头像
@property (nonatomic, copy) NSString *createTime;//评论时间
@property (nonatomic, copy) NSString *title;//标题


@property (nonatomic, strong) NSNumber *type;//反馈类型
@property (nonatomic, strong) NSNumber *star;//星星


@end

NS_ASSUME_NONNULL_END
