//
//  XPShopCarItemModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCarItemModel : NSObject

@property (nonatomic, strong) NSNumber *productId;//商品id
@property (nonatomic, strong) NSNumber *shopCarId;//购物车ID
@property (nonatomic, strong) NSNumber *price;//商品价格
@property (nonatomic, strong) NSNumber *productSkuId;//skuid
@property (nonatomic, strong) NSNumber *quantity;//数量
@property (nonatomic, strong) NSNumber *productQuantity;//数量别名
@property (nonatomic, strong) NSNumber *stock;//库存
@property (nonatomic, strong) NSNumber *isShow;//是否上架

@property (nonatomic, copy) NSString *image;//商品图片
@property (nonatomic, copy) NSString *name;//商品名称
@property (nonatomic, copy) NSString *skuName;//规格
@property (nonatomic, copy) NSString *attributeValues;//商品属性值拼接串
@property (nonatomic, copy) NSArray  *skuActivityCouponNoteVOs;//优惠券/活动标签

@property (nonatomic, assign) bool isChoose;//是否被选中 0 未选中 1 选中

@end

NS_ASSUME_NONNULL_END
