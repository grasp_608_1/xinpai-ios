//
//  XPShopGroupModel.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopGroupModel : NSObject

@property (nonatomic, strong) NSNumber *groupPrice;
@property (nonatomic, strong) NSNumber *groupPeopleNumber;
@property (nonatomic, strong) NSNumber *groupPresentNumber;
@property (nonatomic, strong) NSNumber *groupPeopleNumberTotal;
//SKU匹配的可拼团列表
@property (nonatomic, copy)   NSArray *skuActivityGroupVOs;

@end

NS_ASSUME_NONNULL_END
