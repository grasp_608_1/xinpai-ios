//
//  XPExpressSelectController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/29.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPExpressSelectController : XPBaseViewController

@property (nonatomic, copy) void (^expressBlock)(NSDictionary *expressDict);

@end

NS_ASSUME_NONNULL_END
