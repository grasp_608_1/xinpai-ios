//
//  XPShopChartViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/17.
//

#import "XPShopChartViewController.h"
#import "XPSlideChartView.h"
#import "XPHitButton.h"
#import "XPShopChartCell.h"
#import "XPShopModel.h"
#import "XPShopGoodsDetailController.h"
#import "XPEmptyView.h"

@interface XPShopChartViewController ()<UITableViewDelegate,UITableViewDataSource>
//顶部背景图
@property (nonatomic, strong) UIImageView *topBanner;
//返回按钮
@property (nonatomic, strong) XPHitButton *backButton;
//排行榜分类
@property (nonatomic, strong) XPSlideChartView *slideView;
//分类
@property (nonatomic, strong) NSMutableArray *nameArray;
@property (nonatomic, strong) NSMutableArray *idArray;
@property (nonatomic, strong) XPEmptyView *emptyView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger currentIndex;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation XPShopChartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0x201506);
    
    [self setupUI];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initData];
}

- (void)setupUI{
    
    UIImageView *topBanner = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 76)];
    self.topBanner = topBanner;
    topBanner.image = [UIImage imageNamed:@"xp_chart_top"];
    topBanner.frame = CGRectMake(0, 0, SCREEN_WIDTH, 257 * SCREEN_WIDTH / 375.);
    [self.view addSubview:topBanner];
    
    self.backButton = [[XPHitButton alloc] init];
    self.backButton.frame = CGRectMake(20, 17 + self.safeTop, 27, 27);
    self.backButton.hitRect = 50;
    [self.backButton setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.backButton];
    
    self.slideView = [[XPSlideChartView alloc] initWithFrame:CGRectMake(0, MaxY(self.topBanner), self.view.mj_w, 40)];
    [self.view addSubview:self.slideView];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, MaxY(self.slideView), SCREEN_WIDTH, SCREEN_HEIGHT - MaxY(self.slideView));
    self.tableView.backgroundColor = UIColorFromRGB(0x201506);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIView *tableViewfooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 12)];
    self.tableView.tableFooterView = tableViewfooter;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.mj_w, self.safeBottom)];
    [self.view addSubview:self.tableView];
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62,MaxY(self.slideView) + 80, emptyW,emptyW + 27 )];
//    self.emptyView.hidden = NO;
//    self.tableView.hidden = YES;
    self.emptyView.hidden = YES;
    self.tableView.hidden = NO;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无数据~"];
    [self.view addSubview:self.emptyView];
    
    XPWeakSelf
    [self.slideView setTapBlock:^(NSInteger index) {
        //网络请求
        weakSelf.currentIndex = index - 1;
        weakSelf.chartId = weakSelf.idArray[weakSelf.currentIndex];
        [weakSelf.dataArray removeAllObjects];
        [weakSelf.tableView reloadData];
        [weakSelf getListDataBySortID:weakSelf.currentIndex];
    }];
  
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPShopChartCell *cell = [XPShopChartCell cellWithTableView:tableView];
    XPShopModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model Index:indexPath.row];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 156;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    XPShopModel *model = self.dataArray[indexPath.row];
    
    XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
    vc.productSkuId = model.productSkuId;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- network
- (void)initData {
    NSString *url = [NSString stringWithFormat:@"%@%@0",kVERIFY_host,kShop_Chart_category];
    
    [self startLoadingGif];
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            NSArray *categoryArray = data.userData;
            if (categoryArray.count  == 0) {
                return;
            }
            
            [self getTitleArray:categoryArray];
            
            [self.slideView configWithtitleArray:self.nameArray];
            
            self.currentIndex = [self getCurrentIndex];
            [self.slideView setDefaultSeletIndex:self.currentIndex];
            
            [self getListDataBySortID:self.currentIndex];
            
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
- (void)getTitleArray:(NSArray *)array {
    self.nameArray = [NSMutableArray array];
    self.idArray = [NSMutableArray array];
    
    for (int i = 0; i<array.count; i++) {
        NSDictionary *dic = array[i];
        [self.nameArray addObject:dic[@"name"]];
        [self.idArray addObject:dic[@"id"]];
    }
}

- (NSInteger)getCurrentIndex{
    NSInteger index = 0;
    for (int i = 0; i<self.idArray.count; i++) {
        NSNumber *idNum = self.idArray[i];
        if (self.chartId.intValue == idNum.intValue) {
            index = i;
            break;
        }
    }
    return index;
    
}

- (void)getListDataBySortID:(NSInteger )num {
    
    NSNumber *idNum = self.idArray[num];
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kShop_Chart_list,idNum];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            NSArray *dataArray = data.userData;
            NSArray *modelArray = [XPShopModel mj_objectArrayWithKeyValuesArray:dataArray];
        
            self.dataArray = modelArray.mutableCopy;
            [self.tableView reloadData];
            [self canShowEmptyViewWithDtaArray:modelArray];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)canShowEmptyViewWithDtaArray:(NSArray *)array {
    if (array.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}

@end
