//
//  XPShopProductDetailController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/25.
//

#import "XPBaseViewController.h"
#import "XPShopModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface XPShopProductDetailController : XPBaseViewController
@property (nonatomic, strong) XPShopModel *model;
@end

NS_ASSUME_NONNULL_END
