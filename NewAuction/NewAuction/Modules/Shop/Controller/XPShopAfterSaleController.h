//
//  XPShopAfterSaleController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/20.
//

#import "XPBaseViewController.h"
#import "XPShopOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopAfterSaleController : XPBaseViewController
// 1 //仅退款 2 退款退货
@property (nonatomic, assign) NSInteger entryType;

@property (nonatomic, strong) XPShopOrderModel *model;

@end

NS_ASSUME_NONNULL_END
