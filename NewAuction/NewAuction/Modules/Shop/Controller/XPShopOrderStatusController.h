//
//  XPShopOrderStatusController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopOrderStatusController : XPBaseViewController
//1  全部 2 待支付 3 待收货 4 已完成 5 已取消
@property (nonatomic, assign)NSInteger entertype;
@end

NS_ASSUME_NONNULL_END
