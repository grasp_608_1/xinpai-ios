//
//  XPShopCategoryController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/7.
//

#import "XPShopCategoryController.h"
#import "XPShopLeftCategoryView.h"
#import "XPShopRightCategoryView.h"
#import "XPEmptyView.h"
#import "XPShopingController.h"

@interface XPShopCategoryController ()
@property (nonatomic, strong) XPShopLeftCategoryView *leftView;

@property (nonatomic, strong) XPShopRightCategoryView *rightView;

@property (nonatomic, copy) NSArray *dataArray;

@property (nonatomic, strong) XPEmptyView *emptyView;

@end

@implementation XPShopCategoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [self setupViews];
    
    [self getData];
}
-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"分类";
    self.navBarView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    [self.view addSubview:self.navBarView];
}

- (void)setupViews {

    self.leftView = [[XPShopLeftCategoryView alloc] initWithFrame:CGRectMake(0, kTopHeight, 105,self.view.mj_h - kTopHeight)];
    [self.view addSubview:self.leftView];
    
    self.rightView = [[XPShopRightCategoryView alloc] initWithFrame:CGRectMake(105, kTopHeight, self.view.mj_w -105 - 12, SCREEN_HEIGHT - kTopHeight)];
    [self.view addSubview:self.rightView];
    
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(self.rightView.mj_x, self.rightView.center.y - (self.rightView.mj_w + 27)/2. - 50, self.rightView.mj_w,self.rightView.mj_w + 27)];
    self.emptyView.hidden = NO;
    self.rightView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无此分类"];
    [self.view addSubview:self.emptyView];
    
    
    XPWeakSelf
    [self.leftView setTapBlock:^(NSInteger index) {
        NSDictionary *dic = [weakSelf.dataArray objectAtIndex:index - 1];
        NSArray *tempArray = [dic objectForKey:@"children"];
        if (tempArray.count == 0) {
            weakSelf.emptyView.hidden = NO;
            weakSelf.rightView.hidden = YES;
        }else {
            weakSelf.emptyView.hidden = YES;
            weakSelf.rightView.hidden = NO;
        }
        weakSelf.rightView.dataArray = tempArray;
    }];
    [self.rightView setItemSelectBlock:^(NSDictionary * _Nonnull dic) {
        XPShopingController *shopVc = [[XPShopingController alloc] init];
        shopVc.enterType = 5;
        shopVc.categoryIdThree = dic[@"id"];
        [weakSelf.navigationController pushViewController:shopVc animated:YES];
        
    }];
    
}

- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_sort_one];
    [self startLoadingGif];
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
           
            self.dataArray = data.userData;
            
            [self.leftView configWithtitleArray:self.dataArray];
            
            NSDictionary *dic = [self.dataArray objectAtIndex:0];
            NSArray *tempArray = [dic objectForKey:@"children"];
            if (tempArray.count == 0) {
                self.emptyView.hidden = NO;
                self.rightView.hidden = YES;
            }else {
                self.emptyView.hidden = YES;
                self.rightView.hidden = NO;
            }
            self.rightView.dataArray = tempArray;
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

@end
