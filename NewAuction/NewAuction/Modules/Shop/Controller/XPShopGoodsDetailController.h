//
//  XPShopGoodsDetailController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/9.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopGoodsDetailController : XPBaseViewController

@property (nonatomic, strong) NSNumber *productSkuId;
//是否是语音搜索的商品
@property (nonatomic, strong) NSNumber *voiceSearchStatus;

@end

NS_ASSUME_NONNULL_END
