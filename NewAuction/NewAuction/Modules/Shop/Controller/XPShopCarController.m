//
//  XPShopCarController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/13.
//

#import "XPShopCarController.h"
#import "XPShopGoodsDetailController.h"
#import "XPShopCarTopView.h"
#import "XPShopCommendCollectionViewCell.h"
#import "XPShopModel.h"
#import "XPShopSpecPopView.h"
#import "XPHitButton.h"
#import "XPShopCarItemModel.h"
#import "XPShopOrderConfirmModel.h"
#import "XPShopOrderConfirmController.h"
#import "XPShopGoodsDetailController.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "CHTCollectionViewWaterfallHeader.h"
#import "CHTCollectionViewWaterfallFooter.h"
#import "XPShopProductDetailModel.h"

#define XPSHOPCar_HEADER @"XPShopCarHeaderID"
#define XPSHOPCar_FOOTER @"XPShopCarFooterID"
#define XPSHOPCar_CELLID @"XPShopCarController"

@interface XPShopCarController ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>
@property (nonatomic, strong) UICollectionView *collectionView;
//购物车列表
@property (nonatomic, strong) NSMutableArray *carListArray;
//推荐商品列表
@property (nonatomic, strong) NSMutableArray *dataArray;
//购物车UI
@property (nonatomic, strong) XPShopCarTopView *topView;
//规格弹窗
@property (nonatomic, strong) XPShopSpecPopView *specPopView;
//当前页码
@property (nonatomic, assign) NSInteger page;
//底部计算应付的价格
@property (nonatomic, strong) UILabel *resultLabel;
//底部选中按钮
@property (nonatomic, strong) XPHitButton *selectedButton;

@property (nonatomic, strong) UILabel *emptyCarLabel;

@property (nonatomic, assign) bool isFirstRequest;
//cell高度
@property (nonatomic, strong) NSArray *heightArray;

@property (nonatomic, strong) XPShopProductDetailModel *detailModel;
//选中活动index
@property (nonatomic, assign) NSInteger activityIndex;
//活动名称
@property (nonatomic, strong) NSMutableArray *nameArray;
//活动id
@property (nonatomic, strong) NSMutableArray *idArray;
//活动type
@property (nonatomic, strong) NSMutableArray *typeArray;

@end

@implementation XPShopCarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.page = 1;
    [self setupViews];
    self.dataArray = [NSMutableArray array];
    self.activityIndex = 1;
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    self.navBarView.titleLabel.text = @"购物车";
    
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.navBarView];
}
-(void)setupViews{
    
    CHTCollectionViewWaterfallLayout *flowLayout = [[CHTCollectionViewWaterfallLayout alloc] init];

    flowLayout.sectionInset = UIEdgeInsetsMake(0, 12, 0, 12);
    flowLayout.headerHeight = 48;
    flowLayout.footerHeight = 0;
    flowLayout.minimumColumnSpacing = 12;
    flowLayout.minimumInteritemSpacing = 12;
    
    CGRect rect = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - kTabBarHeight);
    self.collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
    self.collectionView.showsVerticalScrollIndicator = NO;
    [self.collectionView registerClass:[XPShopCommendCollectionViewCell class] forCellWithReuseIdentifier:XPSHOPCar_CELLID];
    [self.collectionView registerClass:[CHTCollectionViewWaterfallHeader class]
        forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader
               withReuseIdentifier:XPSHOPCar_HEADER];
    [self.collectionView registerClass:[CHTCollectionViewWaterfallFooter class]
        forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter
               withReuseIdentifier:XPSHOPCar_FOOTER];

    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:self.collectionView];

    self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    
    self.topView = [[XPShopCarTopView alloc] initWithFrame:CGRectMake(12, 0, self.collectionView.mj_w - 24, 0)];
    self.topView.showSelectItem = YES;
    self.topView.canSlideOption = YES;
    [self.collectionView addSubview:self.topView];
    
    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,self.view.mj_h - 48 - kBottomHeight, self.view.mj_w, 48 + kBottomHeight);
    bottomView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:bottomView];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"立即支付" forState:UIControlStateNormal];
    [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [rightButton setBackgroundColor:UIColorFromRGB(0x6E6BFF)];
    rightButton.frame = CGRectMake(bottomView.mj_w - 112,4,100,40);
    rightButton.layer.cornerRadius = 20;
    rightButton.layer.masksToBounds = YES;
    [rightButton addTarget:self action:@selector(sumbitOrder:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:rightButton];
    [rightButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :rightButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    self.selectedButton = [[XPHitButton alloc] init];
    self.selectedButton.frame = CGRectMake(12, 14, 20, 20);
    [self.selectedButton setBackgroundImage:[UIImage imageNamed:@"xp_dot_unselected"] forState:UIControlStateNormal];
    [self.selectedButton setBackgroundImage:[UIImage imageNamed:@"xp_dot_selected"] forState:UIControlStateSelected];
    [self.selectedButton addTarget:self action:@selector(selectedButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.selectedButton.hitRect = 40;
    [bottomView addSubview:self.selectedButton];
    
    self.resultLabel = [UILabel new];
    self.resultLabel.font = kFontRegular(14);
    self.resultLabel.textColor = app_font_color;
    self.resultLabel.frame = CGRectMake(40, 0, bottomView.mj_w - 40 - 100 - 12 - 6, 48);
    [bottomView addSubview:self.resultLabel];
   
    [self topCallBack];
    [self shoResultLabelTextWithMoney:@(0)];
}

- (void)resultReslultUI{
    self.selectedButton.selected = NO;
    [self shoResultLabelTextWithMoney:@(0)];
}

- (void)freshUI{
    
    CGFloat topH = [self.topView  viewWithDataArray:self.carListArray];
    self.topView.frame = CGRectMake(12, -topH, self.collectionView.mj_w - 24, topH);
    
    
    self.collectionView.contentInset = UIEdgeInsetsMake(topH, 0, 0, 0);
    self.collectionView.contentOffset = CGPointMake(0, -topH);
}

- (void)shoResultLabelTextWithMoney:(NSNumber *)num {
    NSString *desStr = @"全选   合计:￥";
    NSString *money = [XPMoneyFormatTool formatMoneyWithNum:num];
    NSString *resultStr = [NSString stringWithFormat:@"%@%@",desStr,money];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:resultStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(12),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(desStr.length -1, 1)];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(18),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(desStr.length, money.length)];
    
    self.resultLabel.attributedText = attr;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.page = 1;
//    [self getListData];
    [self getcommendData];
//    if (self.nameArray.count == 0) {
        [self getActivityData];
//    }
    
}

- (void)getActivityData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_carActivity];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
        
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        
        if (data.isSucess) {
            [self getTitleArray:data.userData];
            self.topView.activityArray = self.nameArray;
            [self getListData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//购物车列表
- (void)getListData{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_carList];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"type"] = self.typeArray[self.activityIndex - 1];
    paraM[@"activityId"] = self.idArray[self.activityIndex - 1];
        
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            self.carListArray = [XPShopCarItemModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            [self freshUI];
            [self calculatePrice];
        }else {
            self.carListArray = @[].mutableCopy;
            [self freshUI];
            [self calculatePrice];
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)getcommendData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_data_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSString stringWithFormat:@"%zd",self.page];
    paraM[@"pageSize"] = @(10);
    paraM[@"sortType"] = @(1);
    paraM[@"pageType"] = @(22);
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.dataArray = [XPShopModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            [self getCellHeightArray];
            [self.collectionView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

- (void)footerRefresh {
    
    self.page ++;
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_data_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSString stringWithFormat:@"%zd",self.page];
    paraM[@"pageSize"] = @(10);
    paraM[@"sortType"] = @(1);
    paraM[@"pageType"] = @(22);
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.collectionView.mj_footer endRefreshing];
        if (data.isSucess) {
            
            NSArray *modelArray = [XPShopModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            if (modelArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self getCellHeightArray];
                [self.collectionView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}
//获取cell高度
- (void)getCellHeightArray{
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPShopCommendCollectionViewCell *cell = XPShopCommendCollectionViewCell.new;
    for (int i = 0; i<self.dataArray.count; i++) {
        XPShopModel *model = self.dataArray[i];
        CGFloat height = [cell cellWithModel:model];
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    self.heightArray = tempArray.copy;
}
#pragma mark -- block callBack

- (void)topCallBack {
    XPWeakSelf
    
    [self.topView setSelectBlock:^(NSInteger index, _Bool isChoose) {
        
        XPShopCarItemModel *model = weakSelf.carListArray[index];
        model.isChoose = isChoose;
        [weakSelf calculatePrice];
        
    }];
    
    [self.topView setMinusBlock:^(NSInteger index, NSInteger count) {
        [weakSelf changeCarNumAtIndex:index Count:count carItemModel:nil];
    }];
    
    [self.topView setAddBlock:^(NSInteger index, NSInteger count) {
        [weakSelf changeCarNumAtIndex:index Count:count carItemModel:nil];
    }];
    [self.topView setDeleteBlock:^(NSInteger index) {
        [weakSelf deleteProductWithIndex:index];
    }];
    [self.topView setSpecBlock:^(NSInteger index) {
        XPShopCarItemModel *model = weakSelf.carListArray[index];
        [weakSelf getDataWithShopCarItemModel:model atIndex:index];
    }];
    
    [self.topView setItemSelect:^(id  _Nonnull modelObj) {
        XPShopCarItemModel *model = (XPShopCarItemModel *)modelObj;
        XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
        vc.productSkuId = model.productSkuId;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    [self.topView setActivityBlcok:^(NSInteger index) {
        weakSelf.activityIndex = index;
        [weakSelf getListData];
    }];
    
}

- (void)calculatePrice {
    
    int total = 0;
    int uselessCount = 0;
    double totalMoney = 0;
    for (int i = 0; i< self.carListArray.count; i++) {
        XPShopCarItemModel *model = self.carListArray[i];
        
        if (model.isShow.intValue != 1 || model.stock.intValue == 0) {
            uselessCount++;
        }
        
        if (model.isChoose == YES) {
            total ++;
            totalMoney = totalMoney + model.quantity.integerValue * (model.price.doubleValue *100);
        }
        
    }
    
    if ((total == self.carListArray.count - uselessCount) && self.carListArray.count != 0 ) {
        self.selectedButton.selected = YES;
    }else{
        self.selectedButton.selected = NO;
    }
    
    [self shoResultLabelTextWithMoney:[NSNumber numberWithDouble:totalMoney/100.]];
    
}

#pragma mark --结算
- (void)sumbitOrder:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
   
    //查看购物是否有选中的商品
    BOOL hasProduct = NO;
    
    //拼接购物车字符串
    NSMutableArray *tempArray = [NSMutableArray array];
    
    for (int i = 0; i< self.carListArray.count; i++) {
        XPShopCarItemModel *model = self.carListArray[i];
        if (model.isChoose == YES) {
            hasProduct = YES;
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:model.shopCarId forKey:@"cartItemId"];
            [dict setObject:model.productSkuId forKey:@"productSkuId"];
            [dict setObject:model.quantity forKey:@"productQuantity"];
            [tempArray addObject:dict];
        }
    }
    
    if (!hasProduct) {
        [[XPShowTipsTool shareInstance] showMsg:@"请先选择商品" ToView:k_keyWindow];
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_carOrderConfirmBuy];
    NSMutableDictionary *paraM  = [NSMutableDictionary dictionary];
    paraM[@"cartItemJsons"] =  [XPCommonTool convertToJsonData:tempArray];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
        
            XPShopOrderConfirmModel *model = [XPShopOrderConfirmModel mj_objectWithKeyValues:data.userData];
            XPShopOrderConfirmController *vc = [XPShopOrderConfirmController new];
            vc.entryType = 1;
            vc.model = model;
            [self.navigationController pushViewController:vc animated:YES];
            [self resultReslultUI];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

//删除购物车某个商品
- (void)deleteProductWithIndex:(NSInteger)index {
    
    XPShopCarItemModel *model = self.carListArray[index];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_carDelteItem];
    NSMutableDictionary *paraM  = [NSMutableDictionary dictionary];
    paraM[@"id"] = model.shopCarId;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [self.carListArray removeObjectAtIndex:index];
            [self freshUI];
            [self calculatePrice];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

#warning - 修改某个商品的规格时(不修改规格,skuid 必须传nil)
- (void)changeCarNumAtIndex:(NSInteger)index Count:(NSInteger)count carItemModel:(XPShopSpecModel * __nullable)changeModel{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_carChangeItem];
    NSMutableDictionary *paraM  = [NSMutableDictionary dictionary];
    XPShopCarItemModel *model = self.carListArray[index];
    paraM[@"id"] = model.shopCarId;
    
    if (changeModel == nil) {
        paraM[@"productSkuId"] = model.productSkuId;
    }else {
        paraM[@"productSkuId"] = changeModel.productSkuId;
    }
    
    paraM[@"quantity"] = @(count);
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            XPShopCarItemModel *tempModel = self.carListArray[index];
            if (changeModel == nil) {
                //数量改变
                tempModel.quantity = @(count);
            }else {
                //规格改变
                tempModel.skuActivityCouponNoteVOs = changeModel.skuActivityCouponNoteVOs;
                tempModel.productId = changeModel.productId;
                tempModel.price = changeModel.priceSale;
                tempModel.productSkuId = changeModel.productSkuId;
                tempModel.quantity = @(count);
                tempModel.stock = changeModel.stock;
                tempModel.isShow = changeModel.isShow;
                tempModel.attributeValues = changeModel.attributeValues;
                tempModel.image = changeModel.pic;
                tempModel.name = changeModel.productName;
                
//                [self.carListArray replaceObjectAtIndex:index withObject:tempModel];
            }
        
            
            [self calculatePrice];
            [self freshUI];

        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//
-(void)selectedButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (sender.selected == NO) {
        for (int i = 0; i< self.carListArray.count; i++) {
            XPShopCarItemModel *model = self.carListArray[i];
            if (model.isShow.intValue != 1 || model.stock.intValue == 0) {
                model.isChoose = NO;
            }else{
                model.isChoose = YES;
            }
        }
    }else {
        for (int i = 0; i< self.carListArray.count; i++) {
            XPShopCarItemModel *model = self.carListArray[i];
            model.isChoose = NO;
        }
        
        CGFloat topH = [self.topView  viewWithDataArray:self.carListArray];
        self.topView.frame = CGRectMake(0, -topH, self.collectionView.mj_w - 24, topH);
        
        self.collectionView.contentInset = UIEdgeInsetsMake(topH, 0, 0, 0);
        [self.collectionView reloadData];
        
    }
    sender.selected = !sender.selected;
    [self calculatePrice];
    [self freshUI];
}

- (void)getDataWithShopCarItemModel:(XPShopCarItemModel *)carModel atIndex:(NSInteger)index{
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,kShop_mall_selectAllSku,carModel.productId];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonGetWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.detailModel = [XPShopProductDetailModel mj_objectWithKeyValues:data.userData];
            self.detailModel.productSkuId = carModel.productSkuId;
            [self showSpectPopViewAtIndex:index];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)showSpectPopViewAtIndex:(NSInteger )itemIndex {
    
    XPShopSpecPopView *popView = [[XPShopSpecPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.specPopView = popView;
    [k_keyWindow addSubview:popView];
    [popView showWithModel:self.detailModel ViewType:XPProductSpecPopTypeChange];
    XPWeakSelf
    [popView setBottomBlock:^(NSInteger index, NSInteger countNum, XPShopSpecModel * _Nonnull model) {
        //修改规格接口
        [weakSelf changeCarNumAtIndex:itemIndex Count:countNum carItemModel:model];
    }];
}

- (void)getTitleArray:(NSArray *)array {
    self.nameArray = [NSMutableArray array];
    self.idArray = [NSMutableArray array];
    self.typeArray = [NSMutableArray array];
    
    for (int i = 0; i<array.count; i++) {
        NSDictionary *dic = array[i];
        [self.nameArray addObject:dic[@"activityNote"]];
        [self.idArray addObject:dic[@"activityId"]];
        [self.typeArray addObject:dic[@"type"]];
    }
}


#pragma mark -- UICollectionView delegate dataSorce
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *height = self.heightArray[indexPath.row];
    
    return CGSizeMake((SCREEN_WIDTH - 36)/2., height.floatValue);
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableView = nil;
    //返回段头段尾视图
    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
      reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                        withReuseIdentifier:XPSHOPCar_HEADER
                                                               forIndexPath:indexPath];
        
        CHTCollectionViewWaterfallHeader *header = (CHTCollectionViewWaterfallHeader *)reusableView;
        header.titleImageView.image = [UIImage imageNamed:@"xp_product_comment"];
        
    }
    else if ([kind isEqualToString:CHTCollectionElementKindSectionFooter]) {
      reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                        withReuseIdentifier:XPSHOPCar_FOOTER
                                                               forIndexPath:indexPath];
    }

    return reusableView;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    XPShopCommendCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:XPSHOPCar_CELLID forIndexPath:indexPath];
    XPShopModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    
    cell.backgroundColor = UIColor.whiteColor;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    XPShopModel *model = self.dataArray[indexPath.row];
    XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
    vc.hidesBottomBarWhenPushed = YES;
    vc.productSkuId = model.productSkuId;
    [self.navigationController pushViewController:vc animated:YES];
}

//header
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.collectionView.bounds.size.width, 48);
}
@end
