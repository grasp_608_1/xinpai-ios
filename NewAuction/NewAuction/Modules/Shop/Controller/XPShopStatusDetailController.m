//
//  XPShopStatusDetailController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import "XPShopStatusDetailController.h"
#import "XPShopStatusTopView.h"
#import "XPAfterSaleProductView.h"
#import "XPShopProductListView.h"
#import "XPShopAfterSalePopView.h"
#import "XPShopAfterSaleController.h"
#import "XPShopOrderProductListModel.h"
#import "XPShopGoodsDetailController.h"
#import "XPLogisticsInfoController.h"
#import "XPShopProductCommentController.h"
#import "XPBaseWebViewViewController.h"
#import "XPShopAfterSaleRecordController.h"
#import "XPExtraContentView.h"
#import "XPOrderPayMoneyDetailPopView.h"
#import "XPPaymentView.h"
#import "XPPackageView.h"
#import "XPPackagePopView.h"
#import "XPPackageDetailController.h"
#import "XPShopSingleTeamView.h"
#import "XPPreSaleStatusDetailView.h"
#import "XPShopPreSaleModel.h"
#import "XPPrintListController.h"

@interface XPShopStatusDetailController ()

@property (nonatomic, strong) UIScrollView *mainView;
//顶部状态栏
@property (nonatomic, strong) XPShopStatusTopView *statusView;
//顶部包裹信息
@property (nonatomic, strong) XPPackageView *packageView;
//顶部包裹信息
@property (nonatomic, strong) XPPackagePopView *packagePopView;
//拼团
@property (nonatomic, strong) XPShopSingleTeamView *teamView;
//预售
@property (nonatomic, strong) XPPreSaleStatusDetailView *preSaleView;
//收货地址
@property (nonatomic, strong) UIView *locationView;
//商品图片价格等
@property (nonatomic, strong) XPAfterSaleProductView *productView;
//付款信息
@property (nonatomic, strong) XPShopProductListView *moneyListView;
//订单记录
@property (nonatomic, strong) XPShopProductListView *recoardListView;
//地址栏目
@property (nonatomic, strong) UILabel *userInfoLabel;
@property (nonatomic, strong) UILabel *phoneSerectLabel;
@property (nonatomic, strong) UILabel *adressLabel;
@property (nonatomic, strong) UIImageView *rightArrow;
@property (nonatomic, strong) UIImageView *adressImageView;
//预售付款
@property (nonatomic, strong) UIButton *presaleButton;
//预售需付金额
@property (nonatomic, strong) UILabel *presaleLabel;
//备注订单
@property (nonatomic, strong) UIButton *extraButton;
@property (nonatomic, strong) UILabel *extraLabel;
//打印按钮
@property (nonatomic, strong) UIButton *printButton;
//删除按钮
@property (nonatomic, strong) UIButton *optionButton;
//底部按钮
@property (nonatomic, strong) UIView *downView;
//商品合计计算
@property (nonatomic, strong) UILabel *calculateLabel;
//售后弹窗
@property (nonatomic, strong) XPShopAfterSalePopView *popView;
//付款信息弹框
@property (nonatomic, strong) XPOrderPayMoneyDetailPopView *payInfoPopView;
//杉德支付number
@property (nonatomic, copy) NSString *sendyPayOrder;
//订单id
@property (nonatomic, copy) NSString *orderID;
//是否要查询支付结果
@property (nonatomic, assign) bool isPayShow;
//备注
@property (nonatomic, strong) XPExtraContentView *extraTitle;
//订单信息是否展开 yes 展开 No 折叠
@property (nonatomic, assign) bool isFold;
//支付
@property (nonatomic, strong)XPPaymentView *payView;
@end

@implementation XPShopStatusDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.isFold = YES;
    [self setupViews];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}
-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"订单详情";
    
    [self.navBarView.rightButton setImage:[UIImage imageNamed:@"xp_custom_service"] forState:UIControlStateNormal];
    [self.navBarView.rightButton addTarget:self action:@selector(topRightButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.rightButton.frame = CGRectMake(self.view.mj_w - 20 - 24, self.navBarView.mj_h - 36, 24, 24);
    
    [self.view addSubview:self.navBarView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.isPayShow) {
        [self queryOrderStatus];
    }else{
        [self getData];
    }
}

-(void)queryOrderStatus{
    if (self.isPayShow) {
        self.isPayShow = NO;
        [[XPPaymentManager shareInstance] orderPayResultQueryWithType:SendayPayShopOrder orderId:self.sendyPayOrder orderNo:self.orderID controller:self requestResultBlock:^(XPRequestResult * _Nonnull data) {
            if (data.isSucess) {
                [[XPShowTipsTool shareInstance] showMsg:@"支付成功!" ToView:k_keyWindow];
                [self getData];
            }else {
                if (data.code.intValue == 10000) {//支付查询结束,未完成支付
                    
                }else {//失败
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
                }
            }
        }shouldQuery:YES];
    }
}
- (void)applicationWillEnterForeground:(NSNotification *)noti {
    if ([self.navigationController.viewControllers lastObject] == self) {
        [self queryOrderStatus];
    }
}


- (void)setupViews {
    
    self.mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.view.mj_w, SCREEN_HEIGHT - kTopHeight - 48 - kBottomHeight)];
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.mainView.showsVerticalScrollIndicator = NO;
    self.mainView.clipsToBounds = YES;
    [self.view addSubview:self.mainView];
    
    XPShopStatusTopView *statusView = [[XPShopStatusTopView alloc] initWithFrame:CGRectMake(12,12, self.view.mj_w - 24, 72)];
    statusView.backgroundColor = UIColor.whiteColor;
    statusView.layer.cornerRadius = 8;
    statusView.layer.masksToBounds = YES;
    [statusView addTarget:self action:@selector(statusViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    statusView.hidden = YES;
    self.statusView = statusView;
    [self.mainView addSubview:statusView];
    //包裹
    XPPackageView *packageView = [[XPPackageView alloc] initWithFrame:CGRectMake(12,12, self.view.mj_w - 24, 116)];
    packageView.hidden = YES;
    self.packageView = packageView;
    [self.mainView addSubview:packageView];
    //拼团
    self.teamView = [[XPShopSingleTeamView alloc] initWithFrame:CGRectMake(12,MaxY(self.statusView) + 12, self.view.mj_w - 24, 0)];
    self.teamView.hidden = YES;
    [self.mainView addSubview:self.teamView];
    //预售
    self.preSaleView = [[XPPreSaleStatusDetailView alloc] initWithFrame:CGRectMake(12,MaxY(self.statusView) + 12, self.view.mj_w - 24, 0)];
    self.preSaleView.hidden = YES;
    [self.mainView addSubview:self.preSaleView];
    
    UIView *locationView = [UIView new];
    locationView.backgroundColor = UIColor.whiteColor;
    locationView.layer.cornerRadius = 8;
    locationView.frame = CGRectMake(12,MaxY(self.teamView) + 12, self.mainView.mj_w - 24, 0);
    locationView.clipsToBounds = YES;
    self.locationView = locationView;
    [self.mainView addSubview:locationView];
    
    UIImageView *locationLogo = [UIImageView new];
    locationLogo.image = [UIImage imageNamed:@"xp_location"];
    self.adressImageView = locationLogo;
    [locationView addSubview:locationLogo];
    
    UILabel *userInfoLabel = [UILabel new];
    userInfoLabel.textColor = UIColorFromRGB(0x3A3C41);
    userInfoLabel.font = kFontMedium(16);
    self.userInfoLabel = userInfoLabel;
    [locationView addSubview:userInfoLabel];
    
    self.phoneSerectLabel = [UILabel new];
    self.phoneSerectLabel.font = kFontRegular(12);
    self.phoneSerectLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.phoneSerectLabel.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
    self.phoneSerectLabel.layer.borderWidth = 1;
    self.phoneSerectLabel.textAlignment = NSTextAlignmentCenter;
    [locationView addSubview:self.phoneSerectLabel];
    
    UILabel *adressLabel = [UILabel new];
    adressLabel.textColor = UIColorFromRGB(0x8A8A8A);
    adressLabel.numberOfLines = NO;
    adressLabel.font = kFontRegular(14);
    self.adressLabel = adressLabel;
    [locationView addSubview:adressLabel];
        
    XPAfterSaleProductView *productView = [[XPAfterSaleProductView alloc] initWithFrame:CGRectMake(12, MaxY(self.statusView) + 12, self.view.mj_w - 24, 0)];
    productView.showUsedActivityAndCoupon = YES;
    productView.backgroundColor = UIColor.whiteColor;
    productView.showSepView = YES;
    productView.edgeInset = UIEdgeInsetsMake(16, 12, 16, 12);
    productView.layer.cornerRadius = 8;
    productView.layer.masksToBounds = YES;
    self.productView = productView;
    [_mainView addSubview:productView];
    
    XPShopProductListView *moneyListView = [[XPShopProductListView alloc] initWithFrame:CGRectMake(0, MaxY(self.statusView) + 12, self.mainView.mj_w, 0)];
    self.moneyListView = moneyListView;
    [self.mainView addSubview:moneyListView];
    
    XPShopProductListView *recoardListView = [[XPShopProductListView alloc] initWithFrame:CGRectMake(0, MaxY(self.statusView) + 12, self.mainView.mj_w, 0)];
    recoardListView.strokeTitle = YES;
    self.recoardListView = recoardListView;
    [self.mainView addSubview:recoardListView];
    
    self.presaleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.presaleButton.frame = CGRectMake(12,  12 + MaxY(self.recoardListView), self.mainView.mj_w - 24, 48);
    self.presaleButton.backgroundColor = UIColor.whiteColor;
    self.presaleButton.layer.cornerRadius = 8;
    self.presaleButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.presaleButton.titleLabel.font = kFontRegular(14);
    [self.presaleButton setTitleColor:app_font_color forState:UIControlStateNormal];
    self.presaleButton.hidden = YES;
    [self.mainView addSubview:self.presaleButton];
    
    self.presaleLabel = [UILabel new];
    self.presaleLabel.textColor = UIColorFromRGB(0xFF4A4A);
    self.presaleLabel.font = kFontMedium(14);
    self.presaleLabel.textAlignment = NSTextAlignmentRight;
    self.presaleLabel.frame = CGRectMake(70, 0, self.presaleButton.mj_w - 70- 12, self.presaleButton.mj_h);
    [self.presaleButton addSubview:self.presaleLabel];
    
    self.extraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.extraButton.frame = CGRectMake(12, 12 + MaxY(self.recoardListView), self.mainView.mj_w - 24, 48);
    self.extraButton.backgroundColor = UIColor.whiteColor;
    self.extraButton.layer.cornerRadius = 8;
    self.extraButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.extraButton setTitle:@"  订单备注" forState:UIControlStateNormal];
    self.extraButton.titleLabel.font = kFontRegular(14);
    [self.extraButton setTitleColor:app_font_color forState:UIControlStateNormal];
    [self.extraButton addTarget:self action:@selector(extraButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.extraButton.hidden = YES;
    [self.mainView addSubview:self.extraButton];
    
    UIButton *arrowButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [arrowButton2 setBackgroundImage:[UIImage imageNamed:@"Group_57"] forState:UIControlStateNormal];
    arrowButton2.frame = CGRectMake(self.extraButton.mj_w - 8 -18, 15, 18, 18);
    [self.extraButton addSubview:arrowButton2];
    
    self.extraLabel = [UILabel new];
    self.extraLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.extraLabel.font = kFontRegular(14);
    self.extraLabel.textAlignment = NSTextAlignmentRight;
    self.extraLabel.frame = CGRectMake(70, 0, self.extraButton.mj_w - 70 - 18 - 6 - 12, self.extraButton.mj_h);
    [self.extraButton addSubview:self.extraLabel];
    
    self.optionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.optionButton setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
    self.optionButton.titleLabel.font = kFontRegular(14);
    self.optionButton.backgroundColor = UIColor.whiteColor;
    self.optionButton.layer.cornerRadius = 8;
    [self.optionButton addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.optionButton];
    
    self.printButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.printButton setTitleColor:app_main_color forState:UIControlStateNormal];
    self.printButton.titleLabel.font = kFontRegular(14);
    self.printButton.backgroundColor = UIColor.whiteColor;
    self.printButton.layer.cornerRadius = 8;
    [self.printButton setTitle:@"我要打印" forState:UIControlStateNormal];
    [self.printButton addTarget:self action:@selector(printButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.printButton];
    
    
    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,self.view.mj_h - 48 - kBottomHeight, self.view.mj_w, 48 + kBottomHeight);
    bottomView.backgroundColor = UIColor.whiteColor;
    self.downView = bottomView;
    [self.view addSubview:bottomView];
    
    UILabel *calculateLabel = [UILabel new];
    calculateLabel.textColor = UIColorFromRGB(0x8A8A8A);
    calculateLabel.font = kFontRegular(14);
    calculateLabel.frame = CGRectMake(16, 4, self.downView.mj_w - 16 - 120, 48);
    self.calculateLabel = calculateLabel;
    [bottomView addSubview:calculateLabel];
    XPWeakSelf
    
    [self.teamView setCountDownBlock:^{
//        [weakSelf getData];
    }];
    
    [self.packageView setRightBlock:^{
        XPPackagePopView *popView = [[XPPackagePopView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [k_keyWindow addSubview:popView];
        weakSelf.packagePopView = popView;
        [popView viewWithArray:weakSelf.model.mallOrderPackageListVOS];
        
        [weakSelf.packagePopView setArrowBlock:^(NSInteger tag) {
            XPPackageDetailController *vc = [XPPackageDetailController new];
            vc.defaultIndex = tag;
            vc.orderId = weakSelf.model.productId;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }];
        
    }];
}
- (void)freshUI{
    self.statusView.hidden = NO;
    self.packageView.hidden = YES;
    NSInteger status = self.model.status.integerValue;
    
    NSMutableArray *moneyTitleDescArray = [NSMutableArray array];
    NSMutableArray *moneyTitleArray = [NSMutableArray array];
    NSMutableArray *orderTitleDescArray = [NSMutableArray array];
    NSMutableArray *orderTitleArray = [NSMutableArray array];
    NSArray *downArray = [NSArray array];
    
    CGFloat top = MaxY(self.statusView);
    
    
    ////订单状态 0 默认 1 待付款 2 已支付|待发货 3 待收货|已发货 4 已完成 5 已取消 6 已关闭
    if (status == 1) {
        //暂时不使用计时器,去掉
        [self.statusView viewWithShopOrderModel:self.model countDownBlock:^{
//            [weakSelf getData];
        }];
        //        moneyTitleDescArray= @[@"商品金额",@"运费",@"优惠券",@"活动",@"积分"].mutableCopy;
        if (self.model.groupStatus.intValue == 1 ||self.model.presaleStatus.intValue == 1) {
            moneyTitleDescArray= @[@"团购价",@"运费",@"优惠券"].mutableCopy;
            moneyTitleArray = @[
                [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.productSkuAmount]],
                [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.freightAmount]],
                [NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.couponAmount]],
            ].mutableCopy;
        }else{
            moneyTitleDescArray= @[@"商品金额",@"运费",@"优惠券",@"活动"].mutableCopy;
            moneyTitleArray = @[
                [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.productSkuAmount]],
                [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.freightAmount]],
                [NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.couponAmount]],
                [NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.activityAmount]],
                //                            [NSString stringWithFormat:@"减￥%@",self.model.pointsAmount]
            ].mutableCopy;
        }
        
       
        orderTitleDescArray = @[@"订单编号",@"支付方式"].mutableCopy;
        orderTitleArray = @[self.model.orderSn,[self getPayTypeStrWithModel:self.model],].mutableCopy;
        if (self.model.createTime.length > 0) {
            [orderTitleDescArray addObject:@"下单时间"];
            [orderTitleArray addObject:self.model.createTime];
        }
        if (self.model.presaleStatus.intValue == 1 && self.model.presalePayStatus.intValue == 10) {
            downArray =  @[];
        }else{
            downArray =  @[@"立即支付"];
        }
        
    }else if (status == 2 ||status == 3){
        
        if (self.model.packageStatus.intValue == 0) {
            self.packageView.hidden = YES;
            self.statusView.hidden = NO;
            [self.statusView viewWithShopOrderModel:self.model countDownBlock:nil];
        }else{
            self.packageView.hidden = NO;
            self.statusView.hidden = YES;
            [self.packageView viewWithArray:self.model.mallOrderPackageListVOS];
            
            top = MaxY(self.packageView);
        }
        
        if (self.model.groupStatus.intValue == 1 ||self.model.presaleStatus.intValue == 1) {
            moneyTitleDescArray= @[@"商品金额",@"运费",@"优惠券"].mutableCopy;
            moneyTitleArray = @[[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.productSkuAmount]],
                                [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.freightAmount]],
                                [NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.couponAmount]],
            ].mutableCopy;
            orderTitleDescArray = @[@"订单编号",@"支付方式"].mutableCopy;
            orderTitleArray = @[self.model.orderSn,[self getPayTypeStrWithModel:self.model],].mutableCopy;
        }else if (self.model.pointStatus.intValue == 1){
            moneyTitleDescArray= @[@"所需积分",@"运费"].mutableCopy;
            moneyTitleArray = @[[NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.pricePoint]],@"包邮"
            ].mutableCopy;
            orderTitleDescArray = @[@"订单编号",@"支付方式"].mutableCopy;
            orderTitleArray = @[self.model.orderSn,[self getPayTypeStrWithModel:self.model],].mutableCopy;
            
        }else{
            moneyTitleDescArray= @[@"商品金额",@"运费",@"优惠券",@"活动"].mutableCopy;
            moneyTitleArray = @[[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.productSkuAmount]],
                                [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.freightAmount]],
                                [NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.couponAmount]],
                                [NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.activityAmount]],
            //                    [NSString stringWithFormat:@"减￥%@",self.model.pointsAmount]
                               ].mutableCopy;
            orderTitleDescArray = @[@"订单编号",@"支付方式"].mutableCopy;
            orderTitleArray = @[self.model.orderSn,[self getPayTypeStrWithModel:self.model],].mutableCopy;

        }
        if (self.model.createTime.length > 0) {
            [orderTitleDescArray addObject:@"下单时间"];
            [orderTitleArray addObject:self.model.createTime];
        }
        if (self.model.payTime.length > 0) {
            [orderTitleDescArray addObject:@"支付时间"];
            [orderTitleArray addObject:self.model.payTime];
        }
//        if (self.model.deliveryTime.length > 0) {
//            [orderTitleDescArray addObject:@"发货时间"];
//            [orderTitleArray addObject:self.model.deliveryTime];
//        }

        
        if (self.model.presaleStatus.intValue == 1) {
            //已支付完尾款才可以售后
            if (self.model.presalePayStatus.intValue == 2) {
                downArray =  @[@"确认收货",@"售后申请"];
            }else{
                downArray =  @[@"确认收货"];
            }
        }else if (self.model.groupStatus.intValue == 1) {
            NSArray *skuTeamArray = self.model.orderGroupVO[@"skuActivityGroupVOs"];
            NSDictionary *firstTeamDic = skuTeamArray.firstObject;
            NSNumber *groupStatus = firstTeamDic[@"status"];
            //已成团才可以售后
            if (groupStatus.intValue == 1) {
                downArray =  @[@"确认收货",@"售后申请"];
            }else{
                downArray =  @[@"确认收货"];
            }
        }else{
            downArray =  @[@"确认收货",@"售后申请"];
        }
    }else if (status == 4){
        if (self.model.packageStatus.intValue == 0) {
            self.packageView.hidden = YES;
            self.statusView.hidden = NO;
            [self.statusView viewWithShopOrderModel:self.model countDownBlock:nil];
        }else{
            self.packageView.hidden = NO;
            self.statusView.hidden = YES;
            [self.packageView viewWithArray:self.model.mallOrderPackageListVOS];
            
            top = MaxY(self.packageView);
        }
        if (_model.pointStatus.intValue == 1) {
            orderTitleDescArray = @[@"积分兑换",@"订单编号",@"支付方式"].mutableCopy;
            orderTitleArray = @[[NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.pricePoint]],self.model.orderSn,[self getPayTypeStrWithModel:self.model]].mutableCopy;
        }else {
            orderTitleDescArray = @[@"实付款",@"订单编号",@"支付方式"].mutableCopy;
            orderTitleArray = @[[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.spendAllAmount]],self.model.orderSn,[self getPayTypeStrWithModel:self.model]].mutableCopy;
        }
       
        if (self.model.createTime.length > 0) {
            [orderTitleDescArray addObject:@"下单时间"];
            [orderTitleArray addObject:self.model.createTime];
        }
        if (self.model.payTime.length > 0) {
            [orderTitleDescArray addObject:@"支付时间"];
            [orderTitleArray addObject:self.model.payTime];
        }
        if (self.model.receiveTime.length > 0) {
            [orderTitleDescArray addObject:@"完成时间"];
            [orderTitleArray addObject:self.model.receiveTime];
        }
        if (self.model.pointStatus.intValue == 1) {
            downArray =  @[[self getCommentStatusWithModel:self.model]? @"已评价":@"待评价",@"再次兑换",@"售后申请"];
        }else {
            downArray =  @[[self getCommentStatusWithModel:self.model]? @"已评价":@"待评价",@"再次购买",@"售后申请"];
        }
        
    }else if (status == 5){
        [self.statusView viewWithShopOrderModel:self.model countDownBlock:nil];
        if (_model.pointStatus.intValue == 1) {
            orderTitleDescArray = @[@"积分兑换",@"订单编号",@"支付方式"].mutableCopy;
            orderTitleArray = @[[NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.spendAllAmount]],self.model.orderSn,[self getPayTypeStrWithModel:self.model]].mutableCopy;
        }else {
            orderTitleDescArray = @[@"需付款",@"订单编号",@"支付方式"].mutableCopy;
            orderTitleArray = @[[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.spendAllAmount]],self.model.orderSn,[self getPayTypeStrWithModel:self.model]].mutableCopy;
        }
        
        if (self.model.createTime.length > 0) {
            [orderTitleDescArray addObject:@"下单时间"];
            [orderTitleArray addObject:self.model.createTime];
        }
        if (self.model.payTime.length > 0) {
            [orderTitleDescArray addObject:@"支付时间"];
            [orderTitleArray addObject:self.model.payTime];
        }
        
        if (_model.pointStatus.intValue == 1) {
            downArray =  @[@"再次兑换"];
        }else {
            downArray =  @[@"再次购买"];
        }
        
    }else if (status == 6){
        [self.statusView viewWithShopOrderModel:self.model countDownBlock:nil];
        
        if (self.model.pointStatus.intValue == 1) {
            orderTitleDescArray = @[@"积分兑换",@"订单编号",@"支付方式"].mutableCopy;
            orderTitleArray = @[[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.spendAllAmount]],self.model.orderSn,[self getPayTypeStrWithModel:self.model]].mutableCopy;
        }else{
            orderTitleDescArray = @[@"实付款",@"订单编号",@"支付方式"].mutableCopy;
            orderTitleArray = @[[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.spendAllAmount]],self.model.orderSn,[self getPayTypeStrWithModel:self.model]].mutableCopy;
        }
        
        if (self.model.createTime.length > 0) {
            [orderTitleDescArray addObject:@"下单时间"];
            [orderTitleArray addObject:self.model.createTime];
        }
        if (self.model.payTime.length > 0) {
            [orderTitleDescArray addObject:@"支付时间"];
            [orderTitleArray addObject:self.model.payTime];
        }
//        if (self.model.deliveryTime.length > 0) {
//            [orderTitleDescArray addObject:@"发货时间"];
//            [orderTitleArray addObject:self.model.deliveryTime];
//            
//        }
        if (self.model.receiveTime.length > 0) {
            [orderTitleDescArray addObject:@"完成时间"];
            [orderTitleArray addObject:self.model.receiveTime];
        }

        if (_model.pointStatus.intValue == 1) {
            downArray =  @[@"再次兑换"];
        }else {
            downArray =  @[@"再次购买"];
        }
    }
    
    if (self.model.groupStatus.intValue == 1) { //拼团
        self.teamView.frame = CGRectMake(12, top + 12, self.view.mj_w - 24, 96);
        NSArray *skuTeamArray = self.model.orderGroupVO[@"skuActivityGroupVOs"];
        NSDictionary *firstTeamDic = skuTeamArray.firstObject;
        NSNumber *groupStatus = firstTeamDic[@"status"];
        if (groupStatus.intValue == 0) {//未成团
            [self.teamView viewWithDic:self.model.orderGroupVO Type:XPShopTeamTypeWait];
        }else{
            [self.teamView viewWithDic:self.model.orderGroupVO Type:XPShopTeamTypeConfirm];
        }
        self.teamView.hidden = NO;
        top = MaxY(self.teamView);
    }
    
    if (self.model.presaleStatus.intValue == 1) {//预售
        XPShopPreSaleModel *preModel = [XPShopPreSaleModel mj_objectWithKeyValues:_model.orderPrepareVO];
        NSString *money;
        NSString *moneyDesc;
        
        if (preModel.presalePayStatus.integerValue == 0) {
            money = [XPMoneyFormatTool formatMoneyWithNum:preModel.presaleDepositPrice];
            moneyDesc = @"   定金待付款";
        }else if (preModel.presalePayStatus.integerValue == 1){
            money = [XPMoneyFormatTool formatMoneyWithNum:preModel.presaleBalancePrice];
            moneyDesc = @"   需付尾款";
        }else if (preModel.presalePayStatus.integerValue == 2){
            money = [XPMoneyFormatTool formatMoneyWithNum:preModel.presaleBalancePrice];
            moneyDesc = @"   已付尾款";
        }else if (preModel.presalePayStatus.integerValue == 10){
            money = [XPMoneyFormatTool formatMoneyWithNum:preModel.presaleDepositPrice];
            moneyDesc = @"   定金已付款";
        }
        [self.presaleButton setTitle:moneyDesc forState:UIControlStateNormal];
        self.presaleLabel.text = [NSString stringWithFormat:@"￥%@",money];
        self.presaleButton.hidden = NO;

        CGFloat preH = [self.preSaleView viewWithDic:self.model.orderPrepareVO];
        self.preSaleView.frame = CGRectMake(12, top + 12, self.view.mj_w - 24, preH);
        self.preSaleView.hidden = NO;
        top = MaxY(self.preSaleView);

    }else {
        self.presaleButton.hidden = YES;
    }
    
    //送货地址信息
    self.adressImageView.frame = CGRectMake(12,12, 24, 24);
    NSString *userPhone = self.model.receiverPhone.length >= 11 ? [self.model.receiverPhone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"] : self.model.receiverPhone;
    self.userInfoLabel.text = [NSString stringWithFormat:@"%@ %@",self.model.receiverName,userPhone];
    CGFloat userInfoW = [self.userInfoLabel.text sizeWithAttributes:@{NSFontAttributeName:self.userInfoLabel.font}].width;
    self.userInfoLabel.frame = CGRectMake(48, 12, userInfoW + 12, 24);
    self.phoneSerectLabel.text = @"号码保护中";
    self.phoneSerectLabel.frame = CGRectMake(MaxX(self.userInfoLabel), 15, 72, 18);
    
    self.adressLabel.text = self.model.receiverAddress;
    CGFloat addressW  = self.locationView.mj_w - 48 - 12;
    CGFloat addressH = [self.adressLabel.text boundingRectWithSize:CGSizeMake(addressW, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.adressLabel.font} context:nil].size.height;
    
    self.adressLabel.frame = CGRectMake(48, MaxY(self.userInfoLabel) + 6, self.locationView.mj_w - 48 - 12, addressH);
    self.locationView.frame = CGRectMake(12,top + 12, self.mainView.mj_w - 24, MaxY(self.adressLabel) + 12);
    //商品信息
    CGFloat productH = [self.productView viewWithModel:self.model];
    self.productView.frame = CGRectMake(12, MaxY(self.locationView) + 12, self.view.mj_w - 24, productH);
    top = MaxY(self.productView);
    
    //预售详情页没有moneyListView
    if (self.model.presaleStatus.intValue == 1) {
        self.presaleButton.hidden = NO;
        self.presaleButton.frame = CGRectMake(12, top + 12, self.view.mj_w - 24, 48);
        
        top = MaxY(self.presaleButton);
        
    }else {
        self.presaleButton.hidden = YES;
    }
    
    if (status == 1 || status == 2 || status == 3) {
        
        if (self.model.presaleStatus.intValue == 1) {
            self.moneyListView.hidden = YES;
        }else{
            self.moneyListView.hidden = NO;
            //付款信息
            CGFloat moneyListViewH;
            if (self.model.pointStatus.intValue == 1) {
                moneyListViewH = [self.moneyListView viewWithKeyArray:moneyTitleDescArray ValueArray:moneyTitleArray bottomInfo:@{@"spendAllAmount":self.model.pricePoint,@"reduceAllAmount":self.model.reduceAllAmount,@"status":@(888)}];
            }else {
                moneyListViewH = [self.moneyListView viewWithKeyArray:moneyTitleDescArray ValueArray:moneyTitleArray bottomInfo:@{@"spendAllAmount":self.model.spendAllAmount,@"reduceAllAmount":self.model.reduceAllAmount,@"status":self.model.status}];
            }
            
            self.moneyListView.frame = CGRectMake(0, MaxY(self.productView) + 12, self.mainView.mj_w, moneyListViewH);
            if (self.model.pointStatus.intValue != 1) {
                [self.moneyListView showCouponViewAtIndex:2 type:2 signArray:@[]];
            }
            
//            if(self.model.pointStatus.intValue == 1){
//                
//            }else if (!(self.model.groupStatus.intValue == 1 || self.model.presaleStatus.intValue == 1)) {
//                [self.moneyListView showCouponViewAtIndex:3 type:1 signArray:@[]];
//            }
            if (self.model.pointStatus.intValue != 1) {
                UILabel *label1 = self.moneyListView.rightArray[0];
                label1.textColor = app_font_color;
                label1.font = kFontMedium(16);
                
                UILabel *label2 = self.moneyListView.rightArray[1];
                label2.textColor = UIColorFromRGB(0xFF4A4A);
                label2.font = kFontMedium(14);
                
                UILabel *label3 = self.moneyListView.rightArray[2];
                label3.textColor = UIColorFromRGB(0xFF4A4A);
                label3.font = kFontMedium(14);
                
                if (!(self.model.groupStatus.intValue == 1 ||self.model.presaleStatus.intValue == 1)) {
                    UILabel *label4 = self.moneyListView.rightArray[3];
                    label4.textColor = UIColorFromRGB(0xFF4A4A);
                    label4.font = kFontMedium(14);
                }
            }
            top = MaxY(self.moneyListView);
        }

        XPWeakSelf
        //底部记录
        CGFloat recoardListViewH = [self.recoardListView viewWithKeyArray:orderTitleDescArray ValueArray:orderTitleArray topTitle:@"订单信息" fold:self.isFold foldHander:^(bool isFold) {
            weakSelf.isFold = !weakSelf.isFold;
            [weakSelf freshUI];
        }];
        self.recoardListView.frame = CGRectMake(0, top + 12, self.mainView.mj_w, recoardListViewH);
        UILabel *label11 = self.recoardListView.rightArray[0];
        label11.textColor = app_font_color;
        if (self.model.pointStatus.intValue != 1) {
            UILabel *label22 = self.recoardListView.rightArray[1];
            label22.textColor = app_font_color;
            [self.recoardListView showRightQuestionAtIndex:1];
        }
        
        [self.recoardListView canPastAtIndex:0];
        
        top = MaxY(self.recoardListView);
    }else if (status == 4){
        self.moneyListView.hidden = YES;
        //底部记录
        CGFloat recoardListViewH = [self.recoardListView viewWithKeyArray:orderTitleDescArray ValueArray:orderTitleArray];
        self.recoardListView.frame = CGRectMake(0, top + 12, self.mainView.mj_w, recoardListViewH);
        UILabel *label11 = self.recoardListView.rightArray[0];
        label11.textColor = app_font_color;
//        [self.recoardListView showRightArrowAtIndex:0];
        
        if (self.model.pointStatus.intValue != 1) {
            UILabel *label22 = self.recoardListView.rightArray[1];
            label22.textColor = app_font_color;
            [self.recoardListView canPastAtIndex:1];
            
            UILabel *label33 = self.recoardListView.rightArray[1];
            label33.textColor = app_font_color;
        }
        
        top = MaxY(self.recoardListView);
    } else {
        self.moneyListView.hidden = YES;
        //底部记录
        CGFloat recoardListViewH = [self.recoardListView viewWithKeyArray:orderTitleDescArray ValueArray:orderTitleArray];
        self.recoardListView.frame = CGRectMake(0, top + 12, self.mainView.mj_w, recoardListViewH);
        UILabel *label11 = self.recoardListView.rightArray[0];
        label11.textColor = app_font_color;
        [self.recoardListView showRightArrowAtIndex:0];
        
        if (self.model.pointStatus.intValue != 1) {
            UILabel *label22 = self.recoardListView.rightArray[1];
            label22.textColor = app_font_color;
            [self.recoardListView canPastAtIndex:1];
            
            UILabel *label33 = self.recoardListView.rightArray[1];
            label33.textColor = app_font_color;
        }
        
        top = MaxY(self.recoardListView);
    }
    
    ////订单状态 0 默认 1 待付款 2 已支付|待发货 3 待收货|已发货 4 已完成 5 已取消 6 已关闭
    if (status == 1) {
        self.printButton.hidden = YES;
        if (!self.model.message || self.model.message.length == 0) {
            self.extraButton.hidden = YES;
            self.optionButton.frame = CGRectMake(12, 12 + top, self.mainView.mj_w - 24, 48);
            
            if (self.model.presaleStatus.intValue == 1) {
                self.optionButton.hidden = YES;
                self.mainView.contentSize = CGSizeMake(self.mainView.mj_w,top + 12);
            }else{
                self.optionButton.hidden = NO;
                [self.optionButton setTitle:@"取消订单" forState:UIControlStateNormal];
                
                self.mainView.contentSize = CGSizeMake(self.mainView.mj_w,MaxY(self.optionButton) + 12);
            }
        }else{
            self.extraButton.hidden = NO;
            self.extraLabel.text = self.model.message;
            self.extraButton.frame = CGRectMake(12, 12 + top, self.mainView.mj_w - 24, 48);
            self.optionButton.frame = CGRectMake(12, 12 + MaxY(self.extraButton), self.mainView.mj_w - 24, 48);
            
            if (self.model.presaleStatus.intValue == 1) {
                self.optionButton.hidden = YES;
                self.mainView.contentSize = CGSizeMake(self.mainView.mj_w,top + 12);
            }else{
                self.optionButton.hidden = NO;
                [self.optionButton setTitle:@"取消订单" forState:UIControlStateNormal];
                
                self.mainView.contentSize = CGSizeMake(self.mainView.mj_w,MaxY(self.optionButton) + 12);
            }
        }
        
    }else if(status == 2||status == 3){
        if (status == 2 && _model.modelStatus.intValue == 1) {
            self.printButton.hidden = YES;
            self.printButton.frame = CGRectMake(12, 12 + top, self.mainView.mj_w - 24, 48);
        }else{
            self.printButton.hidden = YES;
        }
        
        if (!self.model.message || self.model.message.length == 0) {
            self.extraButton.hidden = YES;
            self.mainView.contentSize = CGSizeMake(self.mainView.mj_w,top + 12);
        }else{
            self.extraButton.hidden = NO;
            self.extraLabel.text = self.model.message;
            self.extraButton.frame = CGRectMake(12, 12 + top, self.mainView.mj_w - 24, 48);
            self.mainView.contentSize = CGSizeMake(self.mainView.mj_w,MaxY(self.extraButton) + 12);
        }
 
    }else if (status == 4 || status == 6) {
        self.printButton.hidden = YES;
        if (!self.model.message || self.model.message.length == 0) {
            self.extraButton.hidden = YES;
            self.optionButton.frame = CGRectMake(12, 12 + top, self.mainView.mj_w - 24, 48);
        }else{
            self.extraButton.hidden = NO;
            self.extraLabel.text = self.model.message;
            self.extraButton.frame = CGRectMake(12, 12 + top, self.mainView.mj_w - 24, 48);
            self.optionButton.frame = CGRectMake(12, 12 + MaxY(self.extraButton), self.mainView.mj_w - 24, 48);
        }
        self.optionButton.hidden = NO;
        [self.optionButton setTitle:@"删除订单" forState:UIControlStateNormal];
        
        self.mainView.contentSize = CGSizeMake(self.mainView.mj_w,MaxY(self.optionButton) + 12);
    }else if (status == 5 ) {
        self.printButton.hidden = YES;
        if (!self.model.message || self.model.message.length == 0) {
            self.extraButton.hidden = YES;
            self.optionButton.frame = CGRectMake(12, 12 + top, self.mainView.mj_w - 24, 48);
        }else{
            self.extraButton.hidden = NO;
            self.extraLabel.text = self.model.message;
            self.extraButton.frame = CGRectMake(12, 12 + top, self.mainView.mj_w - 24, 48);
            self.optionButton.frame = CGRectMake(12, 12 + MaxY(self.extraButton), self.mainView.mj_w - 24, 48);
        }
        [self.optionButton setTitle:@"删除订单" forState:UIControlStateNormal];
        
        self.mainView.contentSize = CGSizeMake(self.mainView.mj_w,MaxY(self.optionButton) + 12);
    }
    //底部按钮
    [self.downView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    for (int j = 0; j< downArray.count; j++) {
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightButton setTitle:downArray[j] forState:UIControlStateNormal];
        [rightButton setTitleColor:app_font_color forState:UIControlStateNormal];
        rightButton.frame = CGRectMake(self.downView.mj_w - 112*(j+1),4,100,40);
        rightButton.layer.cornerRadius = 20;
        if (j != 0) {
            rightButton.layer.borderWidth = 1;
            rightButton.layer.borderColor = UIColorFromRGB(0xB8B8B8).CGColor;
        }
        rightButton.layer.masksToBounds = YES;
        rightButton.titleLabel.font = kFontRegular(14);
        rightButton.tag = j;
        [rightButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.downView addSubview:rightButton];
        if (j == 0) {
            [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
            
            [rightButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :rightButton colors:
                                             @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                               (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
        }
    }
    
    if (status == 1) {
        
        [self.downView addSubview:self.calculateLabel];

         __block int tempCount = 0;
        
        [self.model.mallOrderItemListVOS enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSNumber *quanity = obj[@"productQuantity"];
            tempCount = tempCount + quanity.intValue;
        }];
        
        NSString *payAmoutStr = [XPMoneyFormatTool formatMoneyWithNum:self.model.payAmount];
        
        NSString *totalStr = [NSString stringWithFormat:@"%zd款,共%d件 需付款: ￥%@",self.model.mallOrderItemListVOS.count,tempCount,payAmoutStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(18)} range:NSMakeRange(totalStr.length - payAmoutStr.length, payAmoutStr.length)];
        [attr addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(totalStr.length - payAmoutStr.length - 1, payAmoutStr.length + 1)];
        [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color} range:NSMakeRange(totalStr.length - payAmoutStr.length - 6, 4)];
        
        self.calculateLabel.attributedText = attr;
        
    }
    [self callBack];
    
}

- (void)callBack {
    
    XPWeakSelf
    [self.recoardListView setArrowBlock:^(NSInteger index) {
        
        weakSelf.payInfoPopView = [[XPOrderPayMoneyDetailPopView alloc] initWithFrame:k_keyWindow.bounds];
        [k_keyWindow addSubview:weakSelf.payInfoPopView];
        
        weakSelf.payInfoPopView.model = weakSelf.model;
    }];
}

- (NSString *)getPayTypeStrWithModel:(XPShopOrderModel *)model {
    NSString *str;
    switch (model.payType.intValue) {
        case 10:
            str = @"账户支付";
            break;
        case 11:
            if (model.accountAmount.floatValue > 0) {
                str = @"现金+账户支付";
            }else {
                str = @"现金支付";
            }
            break;
        case 21:
            str = @"积分兑换";
            break;
        default:
            str = @"线上支付";
            break;
    }
    return str;
}

#pragma mark -- 按钮点击回调

- (void)statusViewClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    ////订单状态 0 默认 1 待付款 2 已支付|待发货 3 待收货|已发货 4 已完成 5 已取消 6 已关闭
    NSInteger status = self.model.status.integerValue;
    
    if (status == 2 || status == 3 || status == 4) {
        
        if (self.model.deliveryStatus.intValue == 0) {
            [[XPShowTipsTool shareInstance] showMsg:@"暂无物流信息" ToView:k_keyWindow];
        }else if (self.model.deliveryStatus.intValue == 2 && self.model.packageStatus.intValue == 0){
            NSArray *packArray = self.model.mallOrderPackageListVOS;
            if (packArray.count > 0) {
                NSDictionary *dict = packArray.firstObject;
                XPLogisticsInfoController *vc = [XPLogisticsInfoController new];
                vc.orderId = dict[@"deliverySn"];
                vc.tplCode = dict[@"deliveryCode"];
                vc.phone = dict[@"receiverPhone"];
                vc.expressCompany = dict[@"deliveryCompany"];
                [self.navigationController pushViewController:vc animated:YES];
            }else if (self.model.deliveryCode && self.model.deliveryCode.length > 0) {
                XPLogisticsInfoController *vc = [XPLogisticsInfoController new];
                vc.orderId = self.model.deliverySn;
                vc.tplCode = self.model.deliveryCode;
                vc.phone = self.model.receiverPhone;
                vc.orderStatus = [NSString stringWithFormat:@"%@",self.model.status];
                vc.expressCompany = self.model.deliveryCompany;
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                [[XPShowTipsTool shareInstance] showMsg:@"暂无物流信息" ToView:k_keyWindow];
            }
        }else{
            XPPackageDetailController *vc = [XPPackageDetailController new];
            vc.defaultIndex = 0;
            vc.orderId = self.model.productId;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
}
-(void)printButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPPrintListController *printVc = [XPPrintListController new];
    printVc.entryType = 1;
    [self.navigationController pushViewController:printVc animated:YES];
}

- (void)deleteButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    ////订单状态 0 默认 1 待付款 2 已支付|待发货 3 待收货|已发货 4 已完成 5 已取消 6 已关闭
    NSInteger status = self.model.status.integerValue;
    XPWeakSelf
    if (status == 1) {
        
        [self.xpAlertView configWithTitle:@"确定要取消订单吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
            
        } rightAction:^(NSString * _Nullable extra) {
            [weakSelf orderCancel];
        } alertType:XPAlertTypeNormal];
        
    }else if (status == 4 || status == 6 || status == 5){
        [self.xpAlertView configWithTitle:@"确定要删除订单吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
            
        } rightAction:^(NSString * _Nullable extra) {
            [weakSelf orderDelete];
        } alertType:XPAlertTypeNormal];
    }
}

//订单备注
- (void)extraButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    XPExtraContentView *extraTitle = [[XPExtraContentView alloc] initWithFrame:k_keyWindow.bounds];
    self.extraTitle = extraTitle;
    extraTitle.canEdit = NO;
    [self.view addSubview:extraTitle];
   
    NSString *contentStr = self.model.message;
    if ([contentStr isEqualToString:@""]) {
        contentStr = @"暂无备注";
    }
    
    [extraTitle showWithContent:contentStr Title:@"订单备注"];
    XPWeakSelf
    [extraTitle setFinishBlock:^(NSString * _Nonnull content) {
        [weakSelf.extraTitle removeFromSuperview];
    }];
    
}

- (void)bottomButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPWeakSelf
    NSInteger status = self.model.status.integerValue;
    
    if (status == 1 ) {
        //立即支付
        [weakSelf getUserMoneyInfoWithModel:self.model];
    }else if(status == 2 || status == 3){
       
        if (sender.tag == 0) {//确认收货
            if (status == 2) {
                [[XPShowTipsTool shareInstance] showMsg:@"您还不能确认收货" ToView:k_keyWindow];
                return;
            }else{
                [self.xpAlertView configWithTitle:@"确定要确认收货吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                    
                } rightAction:^(NSString * _Nullable extra) {
                    [weakSelf orderConfirmlWithModel:weakSelf.model];
                } alertType:XPAlertTypeNormal];                
            }
            
            
        }else if(sender.tag == 1){//申请售后
            
            if (![self getAfterStatusWithModel:self.model]) {//存在未售后的商品
                [self showAfterSaleSeverice];
            }else {//所有商品全部售后
                XPShopAfterSaleRecordController *vc = [XPShopAfterSaleRecordController new];
                [self.navigationController pushViewController:vc animated:YES];
            }
            
        }
        
    }
    else if(status == 4){
        
        if (sender.tag == 0) {//评价
            if ([self getCommentStatusWithModel:self.model]) {
                //已评价不可以评价
                [[XPShowTipsTool shareInstance] showMsg:@"已评价!" ToView:self.view];
                return;
            }
            XPShopProductCommentController *vc = [XPShopProductCommentController new];
            vc.model = self.model;
            [self.navigationController pushViewController:vc animated:YES];
        }else if(sender.tag == 1){//再次购买
            
            if (weakSelf.model.pointStatus.intValue == 1) {
                [[XPShowTipsTool shareInstance] showMsg:@"请前往商城兑换" ToView:k_keyWindow];
                return;
            }
            
            [self.xpAlertView configWithTitle:@"确定加入购物车吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                
            } rightAction:^(NSString * _Nullable extra) {
                [weakSelf buyAgainWithModel:weakSelf.model];
            } alertType:XPAlertTypeNormal];
            
        }else if (sender.tag == 2){
            if (![self getAfterStatusWithModel:self.model]) {//存在未售后的商品
                [self showAfterSaleSeverice];
            }else {//所有商品全部售后
                XPShopAfterSaleRecordController *vc = [XPShopAfterSaleRecordController new];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
        
    }else if (status == 5 || status == 6){//已取消订单暂无详情
        if(sender.tag == 0){//再次购买
            if (weakSelf.model.pointStatus.intValue == 1) {
                [[XPShowTipsTool shareInstance] showMsg:@"请前往商城兑换" ToView:k_keyWindow];
                return;
            }
            
            [self.xpAlertView configWithTitle:@"确定加入购物车吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                
            } rightAction:^(NSString * _Nullable extra) {
                [weakSelf buyAgainWithModel:weakSelf.model];
            } alertType:XPAlertTypeNormal];
            
        }
    }
    
}
// Yes 已售后 No 可以售后
- (BOOL)getAfterStatusWithModel:(XPShopOrderModel *)model{
    bool totalAfterStatus = YES;
    if (model.status.intValue == 2) {//未发货,售后状态看外层afterstatus
        if (model.afterStatus.intValue == 0) {
            return NO;
        }else {
            return YES;
        }
    }
    
    for (int i = 0; i< model.mallOrderItemListVOS.count; i++) {
        NSDictionary *dic = model.mallOrderItemListVOS[i];
        if ([[dic objectForKey:@"afterStatus"] isEqualToNumber:@(0)]) {
            totalAfterStatus = NO;
            break;
        }
    }
    return totalAfterStatus;
}

//获取评价状态 yes 全部售后, NO 存在未售后的商品
- (BOOL)getCommentStatusWithModel:(XPShopOrderModel *)model{
    
    NSArray *productList = model.mallOrderItemListVOS;
    
    bool hasCommented = YES;
    for (int i = 0; i<productList.count; i++) {
        NSDictionary *dict = productList[i];
        NSNumber *stats = dict[@"productCommentStatus"];
        if (stats.intValue == 0) {
            hasCommented = NO;
            break;
        }
    }
    return hasCommented;
}

//再次购买(加入购物车)
- (void)buyAgainWithModel:(XPShopOrderModel *)model {
    
    NSArray *productArray = model.mallOrderItemListVOS;
    
    NSMutableArray *tempArray = [NSMutableArray array];
    
    for (int i = 0; i< productArray.count; i++) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict addEntriesFromDictionary:productArray[i]];
        [dict setObject:dict[@"id"] forKey:@"orderItemId"];
        [dict setObject:dict[@"productQuantity"] forKey:@"quantity"];
        [tempArray addObject:dict];
    }
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"mallCartItemInsertJson"] = [XPCommonTool convertToJsonData:tempArray.copy];
    
    [self productsJoinToShopCarWithParaM:paraM];
    
}

- (void)showAfterSaleSeverice {
    
    if (self.model.pointStatus.intValue == 1) {
        XPWeakSelf
        [self.xpAlertView showCancelAlertWithTitle:@"温馨提示\n积分商品仅支持通过联系客服办理退货事宜，客服将快速响应为您解决问题。" leftAction:^(NSString * _Nullable extra) {
            
        } rightAction:^(NSString * _Nullable extra) {
            [weakSelf topRightButtonClicked:[UIButton new]];
        }];
        return;
    }
    
    XPShopAfterSalePopView *popView = [[XPShopAfterSalePopView alloc] initWithFrame:k_keyWindow.bounds];
    self.popView = popView;
    [self.view addSubview:popView];
    [popView viewWithModel:self.model];
    
    XPWeakSelf
    [popView setFinishBlock:^(NSInteger index, NSArray * _Nonnull productModelArray) {
        
        if (productModelArray.count == 0) {
            [[XPShowTipsTool shareInstance] showMsg:@"请选择商品" ToView:k_keyWindow];
        }else {
            XPShopAfterSaleController *vc = [XPShopAfterSaleController new];
            
            NSMutableArray *array = [XPShopOrderProductListModel mj_keyValuesArrayWithObjectArray:productModelArray];
            weakSelf.model.mallOrderItemListVOS = array;
            vc.model = weakSelf.model;
            vc.entryType = index + 1;
            [self.navigationController pushViewController:vc animated:YES];
            [self.popView removeFromSuperview];
        }
        
//        if (weakSelf.model.status.intValue <= 2) {
//            XPShopAfterSaleController *vc = [XPShopAfterSaleController new];
//            vc.model = weakSelf.model;
//            vc.entryType = index + 1;
//            [self.navigationController pushViewController:vc animated:YES];
//            [self.popView removeFromSuperview];
//        }else {
//            
//            if (productModelArray.count == 0) {
//                [[XPShowTipsTool shareInstance] showMsg:@"请选择商品" ToView:k_keyWindow];
//            }else {
//                XPShopAfterSaleController *vc = [XPShopAfterSaleController new];
//                
//                NSMutableArray *array = [XPShopOrderProductListModel mj_keyValuesArrayWithObjectArray:productModelArray];
//                weakSelf.model.mallOrderItemListVOS = array;
//                vc.model = weakSelf.model;
//                vc.entryType = index + 1;
//                [self.navigationController pushViewController:vc animated:YES];
//                [self.popView removeFromSuperview];
//            }
//        }
        
    }];
}

#pragma mark -- 网络请求
- (void)getData {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderDetail];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = self.model.productId;
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             
             self.model = [XPShopOrderModel mj_objectWithKeyValues:data.userData];
             
             if (self.model.packageStatus.intValue == 1 || self.model.packageStatus.intValue == 2) {
                 [self getExpressStatus];
             }else {
                 [self freshUI];
             }
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
}

- (void)getExpressStatus {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_product_package_detail];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = self.model.productId;
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             
             self.model.mallOrderPackageListVOS = data.userData[@"mallOrderPackageListVOS"];
             [self freshUI];
             
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
}


//删除订单
- (void)orderDelete {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderDelte];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = self.model.productId;
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             [[XPShowTipsTool shareInstance] showMsg:@"删除订单成功" ToView:self.view];
             [self.navigationController popViewControllerAnimated:YES];
        
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
}
//取消订单
- (void)orderCancel {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderCancel];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = self.model.productId;
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             if (self.cancelOrderBlock) {
                 self.cancelOrderBlock();
             }
             [self.navigationController popViewControllerAnimated:YES];
        
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
}

//订单确认收货
- (void)orderConfirmlWithModel:(XPShopOrderModel *)model {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderconfirm];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = model.productId;
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             [self getData];
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
}

- (void)getUserMoneyInfoWithModel:(XPShopOrderModel *)model{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_money_Info];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            model.fundsAmount = data.userData[@"funds"];
            
            [self showPayViewWithModel:model];
    
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
- (void)showPayViewWithModel:(XPShopOrderModel *)model {
    
    XPPaymentView *payView = [[XPPaymentView alloc] initWithFrame:self.view.bounds];
    self.payView = payView;
    [self.view addSubview:payView];
    
    [payView viewWithNeedMoney:model.payAmount accountMoney:model.fundsAmount type:XPPaymentViewTypeBuy];
    
    XPWeakSelf
    [payView setNextBlock:^(NSInteger status, NSString * _Nonnull accountAmount) {
        if (status == 10) {//余额直接支付
            [weakSelf orderAccountPayWithModel:model];
        }else{
            [weakSelf orderPayWithOrderID:model.productId PayType:status accountAmount:accountAmount];
        }
    }];
}
//余额支付
-(void)orderAccountPayWithModel:(XPShopOrderModel *)model{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_AccountPay_shop_order];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = model.productId;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:@"支付成功!" ToView:k_keyWindow];
            [self getData];
            }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
}
//三方支付
- (void)orderPayWithOrderID:(NSString *)orderId PayType:(NSInteger)type accountAmount:(NSString *)accountStr{
    self.orderID = orderId;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_sandPay_shop_order];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"payType"] = @(type);
    paraM[@"orderId"] = orderId;
    paraM[@"accountAmount"] = accountStr;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        
        if (data.isSucess) {
            
            if(type == 1){
                [[XPPaymentManager shareInstance] showAlipayWithDic:data.userData viewController:self];
                self.isPayShow = YES;
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if (type == 2) {
                self.isPayShow = YES;
                [[XPPaymentManager shareInstance] showWeChatPayWithDic:data.userData viewController:self];
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if(type == 11){
                
                if ([data.userData isKindOfClass:[NSDictionary class]]  && data.userData[@"fastPayUrl"]) {
                    self.isPayShow = YES;
                    self.sendyPayOrder = data.userData[@"orderNo"];
                    [[XPPaymentManager shareInstance] showPayViewWithUrl:data.userData[@"fastPayUrl"] viewController:self];
                }else{
                    [[XPShowTipsTool shareInstance] showMsg:@"服务器内部错误,请稍后重试" ToView:k_keyWindow];
                }
            }
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
        
    }];
}
//多商品加入购物车
- (void)productsJoinToShopCarWithParaM:(NSDictionary *)paraM {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_carListAdd];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess){
            [[XPShowTipsTool shareInstance] showMsg:@"成功加入购物车!" ToView:k_keyWindow];
        }else{
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
    
}

- (void)topRightButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
    vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,k_user_help];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
