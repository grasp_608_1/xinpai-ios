//
//  XPShopOrderConfirmController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/18.
//

#import "XPBaseViewController.h"
#import "XPShopOrderConfirmModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopOrderConfirmController : XPBaseViewController

//数据源
@property (nonatomic, strong) XPShopOrderConfirmModel *model;

//0 单商品提交  1 购物车,多商品提交  2 拼团 3 开团 4 预售 5 积分商品
@property (nonatomic, assign) NSInteger entryType;
//拼团参数
@property (nonatomic, copy) NSDictionary *groupDic;
//是否是语音搜索的商品
@property (nonatomic, strong) NSNumber *voiceSearchStatus;
//是否来自造物商品,造物商品打印需穿gcode文件名
@property (nonatomic, copy) NSString *gCodeStr;


@end

NS_ASSUME_NONNULL_END
