//
//  XPShopAfterSaleDetailController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/16.
//

#import "XPShopAfterSaleDetailController.h"
#import "XPAfterStatusView.h"
#import "XPAfterSaleTransView.h"
#import "XPShopProductListView.h"
#import "XPAfterSaleInfoView.h"
#import "XPInputTransView.h"
#import "XPAfterSaleMoneyPopView.h"
#import "XPExpressSelectController.h"
#import "XPLogisticsInfoController.h"

@interface XPShopAfterSaleDetailController ()
@property (nonatomic, strong) UIScrollView *mainView;
//顶部状态栏
@property (nonatomic, strong) XPAfterStatusView *statusView;
//付款信息
@property (nonatomic, strong) XPShopProductListView *moneyListView;
//物流显示栏
@property (nonatomic, strong) XPAfterSaleTransView *transView;
//退货信息
@property (nonatomic, strong) XPAfterSaleInfoView *infoView;
//填写邮寄地址
@property (nonatomic, strong) XPInputTransView *expressView;

@property (nonatomic, strong) XPShopAfterSaleModel *model;
//金额
@property (nonatomic, strong) XPAfterSaleMoneyPopView *popView;
//进度
@property (nonatomic, strong) XPAfterSaleMoneyPopView *progressView;
//已选的快递公司
@property (nonatomic, copy)   NSDictionary *expressDict;

@end

@implementation XPShopAfterSaleDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupViews];
}
-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.navBarView];
    
    self.navBarView.titleLabel.text = @"售后详情";
    [self getSingleData];
    
}



- (void)setupViews {
    self.mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight)];
    [self.view addSubview:self.mainView];
    
    self.statusView = [[XPAfterStatusView alloc] initWithFrame:CGRectMake(12, 12,self.mainView.mj_w - 24, 0)];
    [self.mainView addSubview:self.statusView];
    
    
    self.transView = [[XPAfterSaleTransView alloc] initWithFrame:CGRectMake(12, MaxY(self.statusView) + 12, self.mainView.mj_w - 24, 56)];
    [self.transView addTarget:self action:@selector(transArrowClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.transView];
    
    XPShopProductListView *moneyListView = [[XPShopProductListView alloc] initWithFrame:CGRectMake(0, MaxY(self.statusView) + 12, self.mainView.mj_w, 0)];
    self.moneyListView = moneyListView;
    [self.mainView addSubview:moneyListView];
    
    self.infoView = [[XPAfterSaleInfoView alloc] initWithFrame:CGRectMake(0, MaxY(self.moneyListView) + 12, self.view.mj_w - 24, 0)];
    [self.mainView addSubview:self.infoView];
    
    self.expressView = [[XPInputTransView alloc] initWithFrame:CGRectMake(12, MaxY(self.transView) + 12, self.view.mj_w - 24, 0)];
    [self.mainView addSubview:self.expressView];
    [self callBack];
    [self freshUI];
  
}

- (void)callBack {
    XPWeakSelf
    [self.statusView setTapBlock:^{
        [weakSelf showPopView];
    }];
    
    [self.expressView setArrowBlock:^{
        XPExpressSelectController *vc = [XPExpressSelectController new];
        vc.expressBlock = ^(NSDictionary * _Nonnull expressDict) {
            weakSelf.expressDict = expressDict;
            XPLogisticsModel *logisticsModel = [XPLogisticsModel new];
            logisticsModel.deliveryCode = expressDict[@"code100"];
            logisticsModel.deliveryCompany = expressDict[@"name"];
            weakSelf.expressView.logisticsModel = logisticsModel;
        };
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    [self.expressView setSubmitBlock:^(XPLogisticsModel * _Nonnull model, NSString * _Nonnull addressStr, NSString * _Nonnull expressSn) {
        
        [weakSelf submitExpressInfo:model addressStr:addressStr orderSn:expressSn];
    }];
    
}


- (void)showPopView {
    
    if (self.model.status.intValue == 1) {
        XPAfterSaleMoneyPopView *popView = [[XPAfterSaleMoneyPopView alloc] initWithFrame:k_keyWindow.bounds];
        self.popView = popView;
        [k_keyWindow addSubview:popView];
        [popView listViewWithModel:self.model];
    }else if(self.model.status.intValue == 6){
        XPAfterSaleMoneyPopView *popView = [[XPAfterSaleMoneyPopView alloc] initWithFrame:k_keyWindow.bounds];
        self.popView = popView;
        [k_keyWindow addSubview:popView];
        [popView viewWithModel:self.model];
    }
    
    
}

- (void)freshUI {
    
    if (self.model == nil) {
        return;
    }
    
    XPShopAfterSaleModel *model = self.model;
    CGFloat bottom = 0;
    ///// 售后状态 0 未提交 1 待审核(仅退款通过->5;退款退货通过->2) 2 审核成功|待归仓  3 已拒绝 4 商品回寄中 5 退款处理 中 6 已完成
    CGFloat itemH = [self.statusView viewWithModel:model];
    self.statusView.frame = CGRectMake(12, 12,self.mainView.mj_w - 24, itemH);
    
    int status = model.status.intValue;
    bottom = MaxY(self.statusView);
    
    [self.transView viewWithModel:model];
    
    if (status == 1) {
        self.transView.hidden = YES;
        self.expressView.hidden = YES;
    }else if (status == 2) {
        self.transView.hidden = YES;
        self.expressView.hidden = NO;
//        [self.transView viewWithModel:model];
//        self.transView.frame = CGRectMake(12, MaxY(self.statusView) + 12, self.mainView.mj_w - 24, 56);
        
        CGFloat expressH = [self.expressView viewWithModel:model];
        self.expressView.frame = CGRectMake(12, MaxY(self.self.statusView) + 12, self.view.mj_w - 24, expressH);
        
        bottom = MaxY(self.expressView);
    }else if (status == 3) {
        self.transView.hidden = YES;
        self.expressView.hidden = YES;
    }else if (status == 4) {
        self.transView.hidden = NO;
        self.expressView.hidden = YES;
        self.transView.frame = CGRectMake(12, MaxY(self.statusView) + 12, self.mainView.mj_w - 24, 56);
        bottom = MaxY(self.transView);
    }else if (status == 5 || status == 6){
        self.expressView.hidden = YES;
        ///// 申请类型：0 默认 1 仅退款；2 退货退款
        if (self.model.type.intValue == 1 || self.model.type.intValue == 2) {
            self.transView.hidden = YES;
        }else {
            self.transView.hidden = NO;
            self.transView.frame = CGRectMake(12, MaxY(self.statusView) + 12, self.mainView.mj_w - 24, 56);
            bottom = MaxY(self.transView);
        }
        
    }
 
//    NSArray *moneyTitleDescArray = @[@"退款原因",@"退款金额",@"退回账户",@"返回积分"];
    NSArray *moneyTitleDescArray;
    if (model.status.intValue == 6) {
        moneyTitleDescArray = @[@"退款原因",@"退款金额",@"退回账户"];
    }else{
        moneyTitleDescArray = @[@"退款原因",@"预计退款金额",@"预计退回账户"];
    }
    NSArray *moneyTitleArray = @[model.reason,
                                 [NSString stringWithFormat:@"￥%@",[self getPreAmountWithKey:@"payAmount"]],
                                 [NSString stringWithFormat:@"￥%@",[self getPreAmountWithKey:@"accountAmount"]]
//                                 [NSString stringWithFormat:@"%@",model.returnAmount]
                                ];
    //付款信息
    CGFloat moneyListViewH = [self.moneyListView viewWithKeyArray:moneyTitleDescArray ValueArray:moneyTitleArray];
    self.moneyListView.frame = CGRectMake(0, bottom + 12, self.mainView.mj_w, moneyListViewH);
    
    CGFloat infoH = [self.infoView viewWithModel:model];
    self.infoView.frame = CGRectMake(0, MaxY(self.moneyListView) + 12, self.mainView.mj_w, infoH);
    
    self.mainView.contentSize = CGSizeMake(self.mainView.mj_w, MaxY(self.infoView) + self.safeBottom);
    
}

- (NSString *)getPreAmountWithKey:(NSString *)key {
    CGFloat totalPay = 0;
    for (int i = 0; i< self.model.mallOrderItemListVOS.count; i++) {
        NSDictionary *dict = self.model.mallOrderItemListVOS[i];
        NSNumber *payAmount = dict[key];
        CGFloat payf = [payAmount floatValue] * 1000.;
        totalPay = totalPay + payf;
    }
    
    NSNumber *payNum = [NSNumber numberWithFloat:totalPay/1000.];
    
    return [XPMoneyFormatTool formatMoneyWithNum:payNum];
}


- (void)getSingleData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_order_afterSaleSingle];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] =  self.afterSaleId;

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
    
            NSArray *modelArray = [XPShopAfterSaleModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            if (modelArray.count == 0) {
                self.model = nil;
            }else {
                self.model = modelArray.firstObject;
            }
            
            [self freshUI];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)submitExpressInfo:(XPLogisticsModel *)model addressStr:(NSString *)addressStr orderSn:(NSString *)orderSn{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_order_afterSaleExpress];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"afterOrderId"] =  self.afterSaleId;
    paraM[@"sendDeliveryCompany"] = model.deliveryCompany;
    paraM[@"sendDeliveryCompanyCode"] = model.deliveryCode;
    paraM[@"sendDeliverySn"] = orderSn;
    paraM[@"sendAddress"] = addressStr;

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [self getSingleData];
            [[XPShowTipsTool shareInstance] showMsg:@"物流信息提交成功" ToView:k_keyWindow];
            if (self.refreshBlock) {
                self.refreshBlock();
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)transArrowClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.model.sendDeliverySn && self.model.sendDeliverySn.length > 0) {
        XPLogisticsInfoController *vc = [XPLogisticsInfoController new];
        vc.orderId = self.model.sendDeliverySn;
        vc.phone = self.model.sendPhone;
        vc.orderStatus = [NSString stringWithFormat:@"%@",self.model.status];
        vc.tplCode = self.model.sendDeliveryCompanyCode;
        vc.expressCompany = self.model.sendDeliveryCompany;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        [[XPShowTipsTool shareInstance] showMsg:@"暂无物流信息" ToView:k_keyWindow];
    }  
}
@end
