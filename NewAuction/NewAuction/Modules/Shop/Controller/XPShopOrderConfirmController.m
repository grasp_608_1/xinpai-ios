//
//  XPShopOrderConfirmController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/18.
//

#import "XPShopOrderConfirmController.h"
#import "XPPickGoodsModel.h"
#import "XPShopCarTopView.h"
#import "XPShopSpecPopView.h"
#import "XPShopCouponPopView.h"
#import "XPExtraContentView.h"
#import "XPUserAddressController.h"
#import "XPAdressModel.h"
#import "XPCouponModel.h"
#import "XPShopProductBuyModel.h"
#import "XPShopOrderStatusController.h"
#import "XPShopStatusDetailController.h"
#import "IQKeyboardManager.h"
#import "XPShopProductListView.h"
#import "XPUserScorePopView.h"
#import "XPShopingController.h"
#import "XPShopActivityPopView.h"
#import "XPShopCarItemView.h"
#import "XPPaymentView.h"
#import "XPShopSingleTeamView.h"
#import "XPPreSaleStatusDetailView.h"

@interface XPShopOrderConfirmController ()

@property (nonatomic, strong) UIScrollView *mainView;
//拼团
@property (nonatomic, strong) XPShopSingleTeamView *teamView;
//预售
@property (nonatomic, strong) XPPreSaleStatusDetailView *preSaleView;
//选择地址栏
@property (nonatomic, strong) UIButton *locationView;
@property (nonatomic, strong) UILabel *userInfoLabel;
@property (nonatomic, strong) UILabel *phoneSerectLabel;
@property (nonatomic, strong) UILabel *adressLabel;
@property (nonatomic, strong) UIImageView *rightArrow;
@property (nonatomic, strong) UIImageView *adressImageView;
//商品栏
@property (nonatomic, strong) XPShopCarTopView *topView;
//付款信息
@property (nonatomic, strong) XPShopProductListView *payInfoView;
//积分
@property (nonatomic, strong) XPUserScorePopView *scorePopView;
//活动弹窗
@property (nonatomic, strong) XPShopActivityPopView *activityPopView;
//定金付款
@property (nonatomic, strong) UIButton *presaleButton;
//定金金额
@property (nonatomic, strong) UILabel *presaleLabel;
//备注订单
@property (nonatomic, strong) UIButton *extraButton;

@property (nonatomic, strong) UILabel *extraLabel;
//备注
@property (nonatomic, strong) XPExtraContentView *extraTitle;
//优惠券model
@property (nonatomic, strong) XPCouponModel *couponModel;
//地址model;
@property (nonatomic, strong) XPAdressModel *addressModel;

@property (nonatomic, strong) UILabel *resultLabel;

@property (nonatomic, assign) NSInteger productQuantity;
//购买的商品Array
@property (nonatomic, copy) NSArray *productArray;
//补差价流水杉德号
@property (nonatomic, copy) NSString *sendyPayOrder;
//订单号
@property (nonatomic, copy) NSString *confirmOrderID;
//是否要查询支付结果
@property (nonatomic, assign) BOOL isPayShow;
//是否使用积分
@property (nonatomic, assign) bool userScore;
//备注信息
@property (nonatomic, copy) NSString *extraStr;
//支付
@property (nonatomic, strong)XPPaymentView *payView;
//去结算
@property (nonatomic, strong) UIButton *confirmButton;

@end

@implementation XPShopOrderConfirmController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupViews];
    self.addressModel = [XPAdressModel mj_objectWithKeyValues:self.model.userAddressVO];
    if (self.entryType != 1) {
        self.productQuantity = self.model.singleQuality;
    }
    [self freshUIWithModel:self.model];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearDefaultAddress:) name:Notification_user_choose_adress object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 0;
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    self.navBarView.backgroundColor = UIColor.whiteColor;
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.navBarView];
    ////0 单商品提交  1 购物车,多商品提交  2 拼团 3 开团 4 预售
    if (self.entryType == 2) {
        self.navBarView.titleLabel.text = @"我要参团";
    }else if(self.entryType == 3){
        self.navBarView.titleLabel.text = @"我要开团";
    }else if(self.entryType == 4){
        self.navBarView.titleLabel.text = @"定金支付";
    }else{
        self.navBarView.titleLabel.text = @"确认订单";
    }
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 32;
    if (self.entryType == 5) {
        return;
    }
    [self queryOrderStatus];
}

- (void)applicationWillEnterForeground:(NSNotification *)noti {
    if (self.entryType == 5) {
        return;
    }
    if ([self.navigationController.viewControllers lastObject] == self) {
        [self queryOrderStatus];
    }
}

-(void)queryOrderStatus{
    XPWeakSelf
    if (self.isPayShow) {
        self.isPayShow = NO;
        [[XPPaymentManager shareInstance] orderPayResultQueryWithType:SendayPayShopOrder orderId:self.sendyPayOrder orderNo:self.confirmOrderID controller:self requestResultBlock:^(XPRequestResult * _Nonnull data) {
            if (data.isSucess) {
                [self finishPayHandle];
            }else if (data.code.intValue == 10000  ||data.code.intValue == 50000){//待支付状态
                [weakSelf showFailAlert];
            }
        }shouldQuery:NO];
    }
}
- (void)showFailAlert {
    XPWeakSelf
    [self.xpAlertView configWithTitle:@"请前往订单详情查看" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
        
        [weakSelf.navigationController popViewControllerAnimated:YES];
        
    } rightAction:^(NSString * _Nullable extra) {
        
        XPShopOrderModel *model = [XPShopOrderModel new];
        model.productId = self.confirmOrderID;
        XPShopStatusDetailController *vc = [XPShopStatusDetailController new];
        vc.model = model;
        [weakSelf.navigationController pushViewController:vc animated:YES];
        //将当前页从栈移除
        NSMutableArray *controllers = [NSMutableArray  arrayWithArray:self.navigationController.viewControllers];
        for (int i = 0; i<controllers.count; i++) {
            UIViewController *controler = controllers[i];
            if ([controler isKindOfClass:[XPShopOrderConfirmController class]]) {
                [controllers removeObjectAtIndex:i];
                break;
            }
        }
        weakSelf.navigationController.viewControllers = controllers.copy;
        
    } alertType:XPAlertTypeNormal];
}

//UI 搭建
- (void)setupViews {
    
    self.mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTopHeight + 12, self.view.mj_w, SCREEN_HEIGHT - kTopHeight - 48 - kBottomHeight)];
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self.view addSubview:self.mainView];
    
    self.teamView = [[XPShopSingleTeamView alloc] initWithFrame:CGRectMake(12, 0, self.view.mj_w - 24, 0)];
    [self.mainView addSubview:self.teamView];
    //预售
    self.preSaleView = [[XPPreSaleStatusDetailView alloc] initWithFrame:CGRectMake(12,0, self.view.mj_w - 24, 0)];
    [self.mainView addSubview:self.preSaleView];

    UIButton *locationView = [UIButton new];
    locationView.backgroundColor = UIColor.whiteColor;
    locationView.layer.cornerRadius = 8;
    locationView.frame = CGRectMake(12,0, self.mainView.mj_w - 24, 0);
    locationView.layer.masksToBounds = YES;
    self.locationView = locationView;
    
    [locationView addTarget:self action:@selector(chooseAdress:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:locationView];
    
    UIImageView *locationLogo = [UIImageView new];
    locationLogo.image = [UIImage imageNamed:@"xp_location"];
    self.adressImageView = locationLogo;
    [locationView addSubview:locationLogo];
    
    UILabel *userInfoLabel = [UILabel new];
    userInfoLabel.textColor = UIColorFromRGB(0x3A3C41);
    userInfoLabel.font = kFontMedium(16);
    self.userInfoLabel = userInfoLabel;
    [locationView addSubview:userInfoLabel];
    
    self.phoneSerectLabel = [UILabel new];
    self.phoneSerectLabel.font = kFontRegular(12);
    self.phoneSerectLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.phoneSerectLabel.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
    self.phoneSerectLabel.layer.borderWidth = 1;
    self.phoneSerectLabel.textAlignment = NSTextAlignmentCenter;
    [locationView addSubview:self.phoneSerectLabel];
    
    
    UILabel *adressLabel = [UILabel new];
    adressLabel.textColor = UIColorFromRGB(0x8A8A8A);
    adressLabel.numberOfLines = NO;
    adressLabel.font = kFontRegular(14);
    self.adressLabel = adressLabel;
    [locationView addSubview:adressLabel];
    
    UIImageView *rightArrow = [UIImageView new];
    rightArrow.image = [UIImage imageNamed:@"xp_setting_arrow"];
    self.rightArrow = rightArrow;
    [locationView addSubview:rightArrow];
    
    self.topView = [[XPShopCarTopView alloc] initWithFrame:CGRectMake(12, MaxY(self.locationView) + 12, self.mainView.mj_w - 24, 0)];
    self.topView.layer.cornerRadius = 8;
    self.topView.clipsToBounds = YES;
    self.topView.showSelectItem = NO;
    [self.mainView addSubview:self.topView];
   
    self.payInfoView = [[XPShopProductListView alloc] initWithFrame:CGRectMake(0, MaxY(self.topView) + 12, self.mainView.mj_w, 0)];
    self.payInfoView.strokeTitle = YES;
    self.payInfoView.strokeValue = YES;
    [self.mainView addSubview:self.payInfoView];
    
    self.presaleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.presaleButton.frame = CGRectMake(12,  12 + MaxY(self.payInfoView), self.mainView.mj_w - 24, 48);
    self.presaleButton.backgroundColor = UIColor.whiteColor;
    self.presaleButton.layer.cornerRadius = 8;
    self.presaleButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.presaleButton setTitle:@"  定金需付款" forState:UIControlStateNormal];
    self.presaleButton.titleLabel.font = kFontRegular(14);
    [self.presaleButton setTitleColor:app_font_color forState:UIControlStateNormal];
    self.presaleButton.hidden = YES;
    [self.mainView addSubview:self.presaleButton];
    
    self.presaleLabel = [UILabel new];
    self.presaleLabel.textColor = UIColorFromRGB(0xFF4A4A);
    self.presaleLabel.font = kFontMedium(14);
    self.presaleLabel.textAlignment = NSTextAlignmentRight;
    self.presaleLabel.frame = CGRectMake(70, 0, self.presaleButton.mj_w - 70- 12, self.presaleButton.mj_h);
    [self.presaleButton addSubview:self.presaleLabel];

    self.extraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.extraButton.frame = CGRectMake(12,  12 + MaxY(self.payInfoView), self.mainView.mj_w - 24, 48);
    self.extraButton.backgroundColor = UIColor.whiteColor;
    self.extraButton.layer.cornerRadius = 8;
    self.extraButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.extraButton setTitle:@"  订单备注" forState:UIControlStateNormal];
    self.extraButton.titleLabel.font = kFontRegular(14);
    [self.extraButton setTitleColor:app_font_color forState:UIControlStateNormal];
    self.extraButton.tag = 1;
    [self.extraButton addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.extraButton];
    
    UIButton *arrowButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [arrowButton2 setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    arrowButton2.frame = CGRectMake(self.extraButton.mj_w - 8 -18, 15, 18, 18);
    arrowButton2.tag = 1;
    [arrowButton2 addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.extraButton addSubview:arrowButton2];
    
    self.extraLabel = [UILabel new];
    self.extraLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.extraLabel.font = kFontRegular(14);
    self.extraLabel.textAlignment = NSTextAlignmentRight;
    self.extraLabel.frame = CGRectMake(70, 0, self.extraButton.mj_w - 70 - 18 - 6 - 12, self.extraButton.mj_h);
    [self.extraButton addSubview:self.extraLabel];
    
    
    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,self.view.mj_h - 48 - kBottomHeight, self.view.mj_w, 48 + kBottomHeight);
    bottomView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:bottomView];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [rightButton setBackgroundColor:UIColorFromRGB(0x6E6BFF)];
    rightButton.frame = CGRectMake(bottomView.mj_w - 112,4,100,40);
    rightButton.layer.cornerRadius = 20;
    rightButton.layer.masksToBounds = YES;
    [rightButton addTarget:self action:@selector(sumbitOrder:) forControlEvents:UIControlEventTouchUpInside];
    self.confirmButton = rightButton;
    [bottomView addSubview:rightButton];
    [rightButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :rightButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    
    self.resultLabel = [UILabel new];
    self.resultLabel.font = kFontRegular(14);
    self.resultLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.resultLabel.frame = CGRectMake(16, 4, bottomView.mj_w - 120 - 16, 48);
    [bottomView addSubview:self.resultLabel];
    [self callBack];
}
//block 事件回调
- (void)callBack{
    XPWeakSelf
    
    [self.teamView setCountDownBlock:^{
//        [[XPShowTipsTool shareInstance] showMsg:@"拼团结束" ToView:k_keyWindow];
//        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    [self.topView setSpecBlock:^(NSInteger index) {
        XPShopSpecPopView *popView = [[XPShopSpecPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [k_keyWindow addSubview:popView];
//        [popView showWithJsonString:@""];
    }];
    [self.topView setAddBlock:^(NSInteger index, NSInteger count) {
        weakSelf.productQuantity = count;
        XPShopProductBuyModel *model = weakSelf.productArray[index];
        model.productQuantity = @(count);
        
        [weakSelf orderComfirmChangeAccount:YES Index:index];
    }];
    
    
    [self.topView setMinusBlock:^(NSInteger index, NSInteger count) {
        weakSelf.productQuantity = count;
        XPShopProductBuyModel *model = weakSelf.productArray[index];
        model.productQuantity = @(count);
        
        [weakSelf orderComfirmChangeAccount:YES Index:index];
    }];
    
    [self.payInfoView setArrowBlock:^(NSInteger index) {
        if (index == 2) {//优惠券
            XPShopCouponPopView *popView = [[XPShopCouponPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            [weakSelf.view addSubview:popView];
            popView.canSelected = 1;
            [popView showWithDataArray:[XPCouponModel  mj_objectArrayWithKeyValuesArray:weakSelf.model.userUsableCouponVOs]];
            
            [popView setFinishBlock:^(XPCouponModel * _Nonnull model) {
                weakSelf.couponModel = model;
                
                UILabel *label2 = weakSelf.payInfoView.rightArray[2];
                label2.text = model.note;
//                //计算UI
//                [weakSelf.payInfoView showCouponViewAtIndex:2 type:2 signArray:weakSelf.model.userUsableCouponVOs];
                [weakSelf orderComfirmChangeAccount:NO Index:999];
            }];
        }else if (index == 3){//活动
            if (weakSelf.model.orderActivityVOs.count == 0) {
                [[XPShowTipsTool shareInstance] showMsg:@"暂无活动" ToView:k_keyWindow];
            }else {
                XPShopActivityPopView *popView = [[XPShopActivityPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
                weakSelf.activityPopView = popView;
                [k_keyWindow addSubview:popView];
                
                XPShopProductDetailModel *detailModel = [XPShopProductDetailModel new];
                detailModel.orderActivityVOs = weakSelf.model.orderActivityVOs;
                
                [popView viewWithModel:detailModel type:XPShopPopShowTypeActivities];
            
                [popView setTapBlock:^(NSDictionary * _Nonnull dic) {
                    XPShopingController *vc = [XPShopingController new];
                    vc.enterType = 4;
                    vc.noteLongTitle = TO_STR(dic[@"noteLong"]);
                    vc.activityId = dic[@"id"];
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                    [weakSelf.activityPopView removeFromSuperview];
                }];
            }
        }else if (index == 4){//积分
            XPUserScorePopView *popView = [[XPUserScorePopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            weakSelf.scorePopView = popView;
            [weakSelf.view addSubview:popView];
            popView.model = weakSelf.model;
            [popView setFinishBlock:^{
                if (weakSelf.scorePopView.model.scoreSelected) {
                    weakSelf.userScore = YES;
                    UILabel *label4 = weakSelf.payInfoView.rightArray[4];
                    label4.text = [NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:weakSelf.model.pointsAmount]];
                    label4.textColor = UIColorFromRGB(0xFF4A4A);
                    [weakSelf.scorePopView removeFromSuperview];
                    
                }else {
                    weakSelf.userScore = NO;
                    UILabel *label4 = weakSelf.payInfoView.rightArray[4];
                    label4.textColor = app_font_color_8A;
                    label4.text = @"使用积分";
                    [weakSelf.scorePopView removeFromSuperview];
                }
            }];
        }
    }];
    
}

//页面刷新
- (void)freshUIWithModel:(XPShopOrderConfirmModel *)model{
    CGFloat top = 0;
    
    if (model.groupStatus.intValue == 1) {
        self.teamView.hidden = NO;
        self.teamView.frame = CGRectMake(12, 0, self.view.mj_w - 24, 96);
        if (self.entryType == 3) {//开团
            [self.teamView viewWithDic:model.orderGroupVO Type:XPShopTeamTypeCreate];
        }else {//拼团
            [self.teamView viewWithDic:model.orderGroupVO Type:XPShopTeamTypeWait];
        }
        
        
        
        top = MaxY(self.teamView);
    }else{
        self.teamView.hidden = YES;
    }
    
    if (model.presaleStatus.intValue == 1) {
        self.preSaleView.hidden = NO;
        
        CGFloat preH = [self.preSaleView viewWithDic:model.orderPrepareVO];
        self.preSaleView.frame = CGRectMake(12, 0, self.view.mj_w - 24, preH);
        top = MaxY(self.preSaleView);
    }else {
        self.preSaleView.hidden = YES;
    }

    CGFloat addressW  = self.locationView.mj_w - 48 - 36;
    if (!self.addressModel.productId || self.addressModel.productId.intValue == 0) {
        NSString * adressStr = @"请先添加收货地址";
        self.adressLabel.text = adressStr;
        self.userInfoLabel.hidden = YES;
        self.phoneSerectLabel.hidden = YES;
        
        self.locationView.frame = CGRectMake(12, 12, self.mainView.mj_w - 24,48);
        self.adressLabel.frame = CGRectMake(48, 12, addressW, 24);
        self.adressImageView.frame = CGRectMake(12,top + 12, 24, 24);
                
    }else {
        
        self.userInfoLabel.hidden = NO;
        self.phoneSerectLabel.hidden = NO;
        self.adressImageView.frame = CGRectMake(12,12, 24, 24);
        NSString *userPhone = self.addressModel.phone.length >= 11 ? [self.addressModel.phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"] : self.addressModel.phone;
        self.userInfoLabel.text = [NSString stringWithFormat:@"%@ %@",self.addressModel.name,userPhone];
        CGFloat userInfoW = [self.userInfoLabel.text sizeWithAttributes:@{NSFontAttributeName:self.userInfoLabel.font}].width;
        self.userInfoLabel.frame = CGRectMake(48, 12, userInfoW + 12, 24);
        self.phoneSerectLabel.text = @"号码保护中";
        self.phoneSerectLabel.frame = CGRectMake(MaxX(self.userInfoLabel), 15, 72, 18);
        
        self.adressLabel.text = [NSString stringWithFormat:@"%@%@",[self.addressModel.area stringByReplacingOccurrencesOfString:@"," withString:@""],self.addressModel.address];
        CGFloat addressW  = self.locationView.mj_w - 48 - 12;
        CGFloat addressH = [self.adressLabel.text boundingRectWithSize:CGSizeMake(addressW, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.adressLabel.font} context:nil].size.height;
        
        self.adressLabel.frame = CGRectMake(48, MaxY(self.userInfoLabel) + 6, self.locationView.mj_w - 48 - 12, addressH);
        self.locationView.frame = CGRectMake(12,top + 12, self.mainView.mj_w - 24, MaxY(self.adressLabel) + 12);
        self.rightArrow.frame = CGRectMake(self.locationView.mj_w - 30, self.locationView.mj_h/2 - 12, 24, 24);
    }
    
    self.productArray  = [XPShopProductBuyModel mj_objectArrayWithKeyValuesArray:self.model.orderItemVOs];
    CGFloat topH = [self.topView viewWithDataArray:self.productArray];
    self.topView.frame = CGRectMake(12, MaxY(self.locationView) + 12, self.mainView.mj_w - 24, topH + 12);
    
    top = MaxY(self.topView);
    
    NSArray *descArray;
    NSArray *valueArray;
    if (self.entryType == 2 || self.entryType == 3) {
        descArray = @[@"拼团价",@"运费",@"优惠券"];
        valueArray = @[[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.orderGroupVO[@"groupPrice"]]],
                       [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.freightAmount]],
                       [NSString stringWithFormat:@"%@",self.couponModel.couponId.intValue == 0 ? @"暂未选择优惠券":[NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.couponModel.amount]]]];
    }else if (self.entryType == 5){
        descArray = @[@"所需积分",@"运费"];
        valueArray = @[[NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:model.pricePoint]] ,@"包邮"];
    }else {
        descArray = @[@"商品金额",@"运费",@"优惠券",@"活动"];
        valueArray = @[[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.productSkuAmount]],
                       [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.freightAmount]],
                       [NSString stringWithFormat:@"%@",self.couponModel.couponId.intValue == 0 ? @"暂未选择优惠券":[NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.couponModel.amount]]],
                       [NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.activityAmount]]];
    }
    
    if (self.entryType == 4) {
        
        self.presaleButton.hidden = NO;
        self.presaleLabel.text = [NSString stringWithFormat:@"￥%@",self.model.orderPrepareVO[@"presaleDepositPrice"]];
        self.presaleButton.frame = CGRectMake(12,  12 + top, self.mainView.mj_w - 24, 48);
        
        top = MaxY(self.presaleButton);
        
    }else if (self.entryType == 5){
        self.presaleButton.hidden = YES;
        
        CGFloat infoH = [self.payInfoView viewWithKeyArray:descArray ValueArray:valueArray bottomInfo:@{
            @"spendAllAmount":model.pricePoint,
            @"reduceAllAmount":model.reduceAllAmount,
            @"status":@(888)
        }];
        
        self.payInfoView.frame = CGRectMake(0, top + 12, self.mainView.mj_w, infoH);
        
        top = MaxY(self.payInfoView);
    } else{
        self.presaleButton.hidden = YES;
        
        CGFloat infoH = [self.payInfoView viewWithKeyArray:descArray ValueArray:valueArray bottomInfo:@{
            @"spendAllAmount":model.spendAllAmount,
            @"reduceAllAmount":model.reduceAllAmount,
            @"status":@(999)
        }];
        
        UILabel *label2 = self.payInfoView.rightArray[2];
        if (self.couponModel.couponId.intValue == 0) {
            label2.textColor = app_font_color_8A;
            label2.font = kFontRegular(14);
        }else {
            label2.textColor = UIColorFromRGB(0xFF4A4A);
            label2.font = kFontMedium(14);
        }
        if (self.model.groupStatus.intValue != 1) {
            UILabel *label3 = self.payInfoView.rightArray[3];
            label3.textColor = UIColorFromRGB(0xFF4A4A);
            label3.font = kFontMedium(14);
            [self.payInfoView showRightArrowAtIndex:3];
        }
        
        
        //积分
    //    UILabel *label4 = self.payInfoView.rightArray[4];
    //    if (self.model.scoreSelected) {
    //        label4.textColor = UIColorFromRGB(0xFF4A4A);
    //        label4.font = kFontMedium(14);
    //    }else {
    //        label4.textColor = app_font_color_8A;
    //        label4.font = kFontRegular(14);
    //    }
    //
        [self.payInfoView showRightArrowAtIndex:2];
        
    //    [self.payInfoView showRightArrowAtIndex:4];
        
        //优惠券
        if (self.couponModel.couponId.intValue == 0 || self.model.couponUsable.intValue == 2) {
            [self.payInfoView showCouponViewAtIndex:2 type:2 signArray:@[]];
        }else {
            NSDictionary *dict = [NSDictionary dictionary];
            for (int i = 0; i<model.userUsableCouponVOs.count; i++) {
                NSDictionary *tempDict = model.userUsableCouponVOs[i];
                if ([tempDict[@"couponId"] isEqualToNumber:self.couponModel.couponId]) {
                    dict = tempDict;
                    break;
                }
            }
            [self.payInfoView showCouponViewAtIndex:2 type:2 signArray:@[dict]];
        }
        
        if (self.model.groupStatus.intValue != 1) {
            if (model.activityAmount.floatValue > 0) {
                [self.payInfoView showCouponViewAtIndex:3 type:1 signArray:model.orderActivityVOs];
            }else {
                [self.payInfoView showCouponViewAtIndex:3 type:1 signArray:@[]];
            }
        }
        self.payInfoView.frame = CGRectMake(0, top + 12, self.mainView.mj_w, infoH);
        
        top = MaxY(self.payInfoView);
    }
    
    

    self.extraButton.frame = CGRectMake(12, 12 + top, self.mainView.mj_w - 24, 48);
    
    self.mainView.contentSize = CGSizeMake(self.mainView.mj_w, MaxY(self.extraButton) + 40);
    
    self.extraLabel.text = self.extraStr ?: @"";
    
    NSString *amountStr = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.payAmount]];

    __block int tempCount = 0;
    [self.model.orderItemVOs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSNumber *quanity = obj[@"productQuantity"];
        tempCount = tempCount + quanity.intValue;
    }];
    
    NSString *totalStr;
    
    if (self.entryType == 4) {
        [self.confirmButton setTitle:[NSString stringWithFormat:@"定金￥%@",[XPMoneyFormatTool formatMoneyWithNum:self.model.presaleDepositAmount]] forState:UIControlStateNormal];
        totalStr = [NSString stringWithFormat:@"%zd款,共%d件",self.model.orderItemVOs.count,tempCount];
        
        self.resultLabel.text = totalStr;
        
    }else if (self.entryType == 5){
        [self.confirmButton setTitle:@"立即支付" forState:UIControlStateNormal];
        NSString *moneyStr = [XPMoneyFormatTool formatMoneyWithNum:self.model.pricePoint];
        totalStr = [NSString stringWithFormat:@"%zd款,共%d件 需付款: %@积分",self.model.orderItemVOs.count,tempCount,moneyStr];
       
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(18)} range:NSMakeRange(totalStr.length - amountStr.length, amountStr.length)];

        [attr addAttributes:@{NSFontAttributeName:kFontMedium(18),NSForegroundColorAttributeName:app_font_color_FF4A4A} range:NSMakeRange(totalStr.length  - moneyStr.length - 2 , moneyStr.length)];

        self.resultLabel.attributedText = attr;
    }
    else{
        [self.confirmButton setTitle:@"立即支付" forState:UIControlStateNormal];
        totalStr = [NSString stringWithFormat:@"%zd款,共%d件 需付款: ￥%@",self.model.orderItemVOs.count,tempCount,[XPMoneyFormatTool formatMoneyWithNum:self.model.payAmount]];
       
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(18)} range:NSMakeRange(totalStr.length - amountStr.length, amountStr.length)];
        [attr addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(totalStr.length - amountStr.length - 1, amountStr.length + 1)];
        [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color} range:NSMakeRange(totalStr.length - amountStr.length - 6, 4)];

        self.resultLabel.attributedText = attr;
    }

    if (model.couponUsable.intValue == 2 && self.entryType != 4 && self.entryType != 5) {
        UILabel *label2 = self.payInfoView.rightArray[2];
        label2.textColor = app_font_color_8A;
        label2.font = kFontRegular(14);
        label2.text = @"暂未选择优惠券";
        self.couponModel.couponId =@(0);
        //计算UI
        [self.payInfoView showCouponViewAtIndex:2 type:2 signArray:@[]];
        [[XPShowTipsTool shareInstance] showMsg:@"未达最低使用金额标准" ToView:k_keyWindow];
    }
    
}

#pragma mark --- 按钮事件点击
- (void)chooseAdress:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    buttonCanUseAfterOneSec(sender);
    XPUserAddressController *vc = [XPUserAddressController new];
    vc.entryType = 1;
    XPWeakSelf
    vc.chooseAdress = ^(XPAdressModel * _Nonnull model) {
        weakSelf.addressModel = model;
        [weakSelf freshUIWithModel:weakSelf.model];
    };
    
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (void)arrowButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPWeakSelf
    if (sender.tag == 0) {//优惠券
        
    }else if (sender.tag == 1){//订单备注
        XPExtraContentView *extraTitle = [[XPExtraContentView alloc] initWithFrame:k_keyWindow.bounds];
        self.extraTitle = extraTitle;
        [weakSelf.view addSubview:extraTitle];
       
        NSString *contentStr = self.extraLabel.text;
        if ([contentStr isEqualToString:@""]) {
            contentStr = @"";
        }
        
        [extraTitle showWithContent:contentStr Title:@"订单备注"];
        [extraTitle setFinishBlock:^(NSString * _Nonnull content) {
            if (content.length == 0) {
                content = @"";
                [[XPShowTipsTool shareInstance] showMsg:@"请填写备注信息" ToView:k_keyWindow];
            }else {
                weakSelf.extraStr = content;
                weakSelf.extraLabel.text = content;
                [weakSelf.extraTitle removeFromSuperview];
            }
            
        }];
    }else if (sender.tag == 2){
        sender.selected = !sender.selected;
        [self orderComfirmChangeAccount:NO Index:999];
    }
}

#pragma mark --- 网络请求
//提交按钮点击,获取杉德支付地址
- (void)sumbitOrder:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    //先判断积分
    if (self.entryType == 5) {
    
        NSString *shouldPay = [NSString stringWithFormat:@"仅需%@积分,就能把这个商品带回家啦！您是否确认购买？",self.model.pricePoint];
        NSString *needPay = [NSString stringWithFormat:@"现在您的积分是%@,购买该商品需要%@积分,再加把劲攒%d积分,就能顺利把它带回家咯！去试试我们的积分赚取活动吧！",self.model.currentPoints,self.model.pricePoint,self.model.pricePoint.intValue - self.model.currentPoints.intValue];
        XPWeakSelf
        if (self.model.currentPoints.intValue >= self.model.pricePoint.intValue) {
            
            [self.xpAlertView configWithTitle:shouldPay desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                
            } rightAction:^(NSString * _Nullable extra) {
                
                [weakSelf orderPaymentWithPayType:21 accountAmount:@"0"];
                
            } alertType:XPAlertTypeNormal];
        }else{
            [self.xpAlertView configWithTitle:needPay desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                
            } rightAction:^(NSString * _Nullable extra) {
                
                
                
            } alertType:XPAlertTypeSure];
        }
        
        return;
    }
    
    //判断地址
    if (self.addressModel.area.length == 0 ||[self.addressModel.area isEqualToString:@"null"]) {
        [[XPShowTipsTool shareInstance] showMsg:@"请先添加收货地址!" ToView:k_keyWindow];
        return;
    }
    
    XPPaymentView *payView = [[XPPaymentView alloc] initWithFrame:self.view.bounds];
    self.payView = payView;
    [self.view addSubview:payView];
    
    [payView viewWithNeedMoney:self.model.payAmount accountMoney:self.model.funds type:XPPaymentViewTypeBuy];
    XPWeakSelf
    [payView setNextBlock:^(NSInteger status, NSString * _Nonnull accountAmount) {
        [weakSelf orderPaymentWithPayType:status accountAmount:accountAmount];
    }];
    
}

-(void)orderPaymentWithPayType:(NSInteger)type accountAmount:(NSString *)accountStr{
    //结算
    NSMutableDictionary *paraM = [self getParamatersType:type];
    
    NSString *url;
    if (self.entryType == 0 || self.entryType == 5) {
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderSubmitBuy];
    }else if(self.entryType == 1){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_carOrderSubmitBuy];
    }else if(self.entryType == 2){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderSubmitGroupBuy];
        paraM[@"groupId"] = self.groupDic[@"id"];
    }else if (self.entryType == 3){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderSubmitGroupBuy];
        paraM[@"groupId"] = @(0);
    }else if (self.entryType == 4){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderSubmitPresaleBuy];
    }
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.confirmOrderID = data.userData[@"orderId"];
            if (type == 10) {//直接使用账户余额支付
                [self orderAccountPayWithOrderID:self.confirmOrderID];
            }else if(type == 21){//积分兑换
                [self finishPayHandle];
            }else {//杉德支付
                [self orderPayWithOrderID:self.confirmOrderID Type:type accountAmount:accountStr];
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//余额支付
-(void)orderAccountPayWithOrderID:(NSString *)orderID{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_AccountPay_shop_order];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = orderID;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
          
            [self finishPayHandle];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
}

//支付
- (void)orderPayWithOrderID:(NSString *)orderId Type:(NSInteger)type accountAmount:(NSString *)accountStr{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_sandPay_shop_order];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"accountAmount"] = accountStr;
    paraM[@"orderId"] = orderId;
    if (type == 10) {
        paraM[@"payType"] = @(11);
    }else {
        paraM[@"payType"] = @(type);
    }
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            if(type == 1){
                [[XPPaymentManager shareInstance] showAlipayWithDic:data.userData viewController:self];
                self.isPayShow = YES;
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if (type == 2) {//微信支付
                self.isPayShow = YES;
                [[XPPaymentManager shareInstance] showWeChatPayWithDic:data.userData viewController:self];
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if (type == 11){
                if ([data.userData isKindOfClass:[NSDictionary class]]  && data.userData[@"fastPayUrl"]) {
                    self.isPayShow = YES;
                    self.sendyPayOrder = data.userData[@"orderNo"];
                    [[XPPaymentManager shareInstance] showPayViewWithUrl:data.userData[@"fastPayUrl"] viewController:self];
                }else{
                    [[XPShowTipsTool shareInstance] showMsg:@"服务器内部错误,请稍后重试" ToView:k_keyWindow];
                }
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
        
    }];
}

//订单修改
- (void)orderComfirmChangeAccount:(bool) isChangeCount Index:(NSInteger)index{
    
    NSMutableDictionary *paraM = [self getParamatersType:12];
    
    NSString *url;
    if (self.entryType == 0 || self.entryType == 1 || self.entryType == 5) {
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderConfirmBuy];
    }else if(self.entryType == 2){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderConfirmGroupBuy];
//        paraM[@"groupId"] = self.groupDic[@"id"];
        paraM[@"groupId"] = @(7);
    }else if (self.entryType == 3){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderConfirmGroupBuy];
        paraM[@"groupId"] = @(0);
    }else if (self.entryType == 4){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderConfirmPresaleBuy];
    }
   
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            XPShopOrderConfirmModel *model = [XPShopOrderConfirmModel mj_objectWithKeyValues:data.userData];
            bool lastScore = self.model.scoreSelected;
           self.model = model;
            self.model.scoreSelected = lastScore;
            [self freshUIWithModel:model];
        }else {
            
            if (isChangeCount) {//还原之前的数据
                
                XPShopCarItemView *itemView = self.topView.itemsArray[index];
                NSInteger count = [itemView.numberTextField.text intValue];
                
                self.productQuantity = count;
                XPShopProductBuyModel *model = self.productArray[index];
                model.productQuantity = @(count);
                
            }
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

#pragma mark -- 数据处理
//支付成功,跳转逻辑
- (void)finishPayHandle{
    XPWeakSelf
    [self.xpAlertView configWithTitle:@"支付成功,请前往订单列表查看" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightAction:^(NSString * _Nullable extra) {
        
        XPShopOrderStatusController *vc = [XPShopOrderStatusController new];
        if (self.model.presaleStatus.intValue == 1) {
            vc.entertype = 2;
        }else{
            vc.entertype = 3;
        }
        [weakSelf.navigationController pushViewController:vc animated:YES];
        //将当前页从栈移除
        NSMutableArray *controllers = [NSMutableArray  arrayWithArray:weakSelf.navigationController.viewControllers];
        for (int i = 0; i<controllers.count; i++) {
            UIViewController *controler = controllers[i];
            if ([controler isKindOfClass:[XPShopOrderConfirmController class]]) {
                [controllers removeObjectAtIndex:i];
                break;
            }
        }
        weakSelf.navigationController.viewControllers = controllers;
        
    } alertType:XPAlertTypeNormal];
}
//检查当前地址是否被删除
- (void)clearDefaultAddress:(NSNotification *)noti{
    
    XPAdressModel *model = [noti object];
    if (self.addressModel.productId && [model.productId isEqualToNumber:self.self.addressModel.productId]) {
        self.self.addressModel.productId = @(0);
        self.self.addressModel.area = @"";
        [self freshUIWithModel:self.model];
    }
    
}

//拼接请求参数
- (NSMutableDictionary *)getParamatersType:(NSInteger )type{
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    if (self.entryType == 1){
        paraM[@"userAddressId"] = self.addressModel.productId;
        //可选择参数
        paraM[@"couponId"] = self.couponModel.couponId?:@(0);//优惠券
        paraM[@"pointsStatus"] = self.model.scoreSelected ? @(1):@(0);//账户积分
        paraM[@"message"] = self.extraLabel.text.length >= 0 ?self.extraLabel.text :@"";
        
        //拼接购物车字符串
        NSMutableArray *tempArray = [NSMutableArray array];
        for (int i = 0; i< self.productArray.count; i++) {
            XPShopProductBuyModel *model = self.productArray[i];
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:model.cartItemId forKey:@"cartItemId"];
            [dict setObject:model.productSkuId forKey:@"productSkuId"];
            [dict setObject:model.productQuantity forKey:@"productQuantity"];
            [tempArray addObject:dict];
        }
        paraM[@"cartItemJsons"] =  [XPCommonTool convertToJsonData:tempArray];
    }else{
        paraM[@"userAddressId"] = self.addressModel.productId;
        paraM[@"productSkuId"] = self.model.orderItemVOs.firstObject[@"productSkuId"];
        paraM[@"productQuantity"] = self.productQuantity <= 0 ? @(1):@(self.productQuantity);
        //可选择参数
        paraM[@"couponId"] = self.couponModel.couponId?:@(0);
        paraM[@"pointsStatus"] = self.model.scoreSelected ? @(1):@(0);
        paraM[@"message"] = self.extraLabel.text.length >= 0 ?self.extraLabel.text :@"";
    }
    
    if ([self.payView getAccountStatus]) {//是否使用账户余额
        paraM[@"fundsStatus"] = @(1);
    }else {
        paraM[@"fundsStatus"] = @(0);
    }
    if (self.voiceSearchStatus.intValue == 1) {
        paraM[@"voiceSearchStatus"] = @(1);
    }
    if (self.gCodeStr && self.gCodeStr.length >0) {
        paraM[@"gcodeUrl"] = self.gCodeStr;
    }
    
    paraM[@"payType"] = @(type);

    return paraM;
}

@end
