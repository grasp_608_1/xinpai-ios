//
//  XPShopProductCommentController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/25.
//

#import "XPBaseViewController.h"
#import "XPShopOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopProductCommentController : XPBaseViewController

@property (nonatomic, strong) XPShopOrderModel *model;

@end

NS_ASSUME_NONNULL_END
