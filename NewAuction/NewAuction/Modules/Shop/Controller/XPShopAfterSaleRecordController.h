//
//  XPShopAfterSaleRecordController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/20.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopAfterSaleRecordController : XPBaseViewController
// 默认0 列表  1 单商品售后详情
@property (nonatomic, assign) NSInteger enterType;

@property (nonatomic, copy)   NSString *afterSaleId;

@end

NS_ASSUME_NONNULL_END
