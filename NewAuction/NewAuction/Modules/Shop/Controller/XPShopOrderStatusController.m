//
//  XPShopOrderStatusController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import "XPShopOrderStatusController.h"
#import "XPSelectItemView.h"
#import "XPShopOrderStatusCell.h"
#import "XPEmptyView.h"
#import "XPShopStatusDetailController.h"
#import "XPLogisticsInfoController.h"
#import "XPShopGoodsDetailController.h"
#import "XPSearchDefaultView.h"
#import "XPShopProductCommentController.h"
#import "XPPaymentView.h"
#import "XPPackageDetailController.h"
#import "XPCreateShowViewController.h"

@interface XPShopOrderStatusController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) XPEmptyView *emptyView;
//搜索
@property (nonatomic, strong) XPSearchDefaultView *searchView;

@property (nonatomic, strong) XPSelectItemView *topView;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//当前页码
@property (nonatomic, assign) NSInteger page;

//cell高度
@property (nonatomic, strong) NSArray *heightArray;
//keyStr
@property (nonatomic, copy) NSString *keyStr;
//补差价订单号
@property (nonatomic, copy) NSString *sendyPayOrder;
@property (nonatomic, copy) NSString *orderId;
//是否要查询支付结果
@property (nonatomic, assign) BOOL isPayShow;
//支付
@property (nonatomic, strong)XPPaymentView *payView;
//当前用于支付的订单信息
@property (nonatomic, strong) XPShopOrderModel *payCurrentModel;

@end

@implementation XPShopOrderStatusController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.keyStr = @"";
    self.page = 1;
    self.dataArray = [NSMutableArray array];
    [self setupUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated{
    self.page = 1;
    if (self.isPayShow) {
        [self queryOrderStatus];
    }else{
        [self getListData];
    }
}

-(void)queryOrderStatus{
    XPWeakSelf
    if (self.isPayShow) {
        self.isPayShow = NO;
        [[XPPaymentManager shareInstance] orderPayResultQueryWithType:SendayPayShopOrder orderId:self.sendyPayOrder orderNo:self.orderId controller:self requestResultBlock:^(XPRequestResult * _Nonnull data) {
            if (data.isSucess) {
                [[XPShowTipsTool shareInstance] showMsg:@"支付成功!" ToView:k_keyWindow];
                
                if (self.payCurrentModel.presalePayStatus.intValue == 0 && self.payCurrentModel.presaleStatus.intValue == 1) {
                    //先不处理,
                }else{
                    weakSelf.currentIndex = 3;
                    weakSelf.topView.defaultSeletIndex = 2;
                }
                
                [weakSelf getListData];
            }else {
                if (data.code.intValue == 10000) {//支付查询结束,未完成支付
                    
                }else {//失败
                    [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
                }
            }
        }shouldQuery:YES];
    }
}

- (void)applicationWillEnterForeground:(NSNotification *)noti {
    if ([self.navigationController.viewControllers lastObject] == self) {
        [self queryOrderStatus];
    }
}

- (void)setupUI {
    
    XPSelectItemView *selectItemView = [[XPSelectItemView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, 40)];
    [selectItemView configWithtitleArray:@[@"全部",@"待支付",@"待收货",@"已完成",@"已取消"]];
    self.topView = selectItemView;
    [self.view addSubview:selectItemView];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.tableView.frame = CGRectMake(0, MaxY(selectItemView), SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - 40);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:self.tableView];
    
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.mj_w, self.safeBottom)];
    
    XPWeakSelf
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf headRefresh];
    }];
    
    [selectItemView setTapBlock:^(NSInteger index) {
        //网络请求
        weakSelf.currentIndex = index;
        weakSelf.page = 1;
        weakSelf.keyStr = @"";
        weakSelf.searchView.defaultSearchStr = @"";
        [weakSelf.dataArray removeAllObjects];
        [weakSelf.tableView reloadData];
        [weakSelf getListData];
    }];
    
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = YES;
    self.tableView.hidden = NO;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无数据"];
    [self.view addSubview:self.emptyView];
    

    //默认选中第一个
        if (self.entertype == 0) {
            self.currentIndex = 1;
            selectItemView.defaultSeletIndex = 0;
        }else{
            selectItemView.defaultSeletIndex = self.entertype - 1;
            self.currentIndex = self.entertype;
        }
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    XPSearchDefaultView *searchView = [[XPSearchDefaultView alloc] initWithFrame:CGRectMake(52, self.safeTop, SCREEN_WIDTH - 52 - 24, 40)];
    searchView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    searchView.searchIcon.image = [UIImage imageNamed:@"xp_search_black"];
    searchView.canSearch = YES;
    searchView.defaultSearchStr = self.keyStr;
    self.searchView = searchView;
    [self.navBarView addSubview:searchView];
    
    [self.view addSubview:self.navBarView];
    
    XPWeakSelf;
    [searchView setSearchBlock:^(NSString * _Nonnull keyStr) {
        [weakSelf.view endEditing:YES];
        
        weakSelf.dataArray = @[].mutableCopy;
        [weakSelf.tableView reloadData];
        weakSelf.keyStr = keyStr;
        weakSelf.page = 1;
        [weakSelf getListData];
    }];
    
    [searchView setSearchStateBlock:^(BOOL state) {
        if (state) {
            weakSelf.tableView.hidden = YES;
            weakSelf.topView.hidden = YES;
            weakSelf.emptyView.hidden = YES;
        }else{
            
            weakSelf.topView.hidden = NO;
            if (weakSelf.dataArray.count > 0) {
                weakSelf.tableView.hidden = NO;
                weakSelf.emptyView.hidden = YES;
            }else {
                weakSelf.tableView.hidden = YES;
                weakSelf.emptyView.hidden = NO;
            }
        }
    }];
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPShopOrderStatusCell *cell = [XPShopOrderStatusCell cellWithTableView:tableView];
    cell.backgroundColor = UIColorFromRGB(0xFAFAFA);
    XPShopOrderModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    XPWeakSelf;
    
    [cell setButtonBlock:^(NSInteger tag) {
        
        NSInteger status = model.status.integerValue;
        ////订单状态 0 默认 1 待付款 2 已支付|待发货 3 待收货|已发货 4 已完成 5 已取消 6 已关闭
        if (status == 1) {//代付款
            if (tag == 0) {//去支付
                weakSelf.orderId = [NSString stringWithFormat:@"%@",model.productId];
                [weakSelf getUserMoneyInfoWithModel:model];
            }else if (tag == 1){//取消订单
                [weakSelf.xpAlertView configWithTitle:@"确定要取消订单吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                    
                } rightAction:^(NSString * _Nullable extra) {
                    [weakSelf orderCancelWithModel:model];
                } alertType:XPAlertTypeNormal];
                
                
            }
        }else if (status == 2 || status == 3) {//待收货
            if (tag == 0) {//确认收货
                if (status == 2) {
                    [[XPShowTipsTool shareInstance] showMsg:@"您还不能确认收货" ToView:k_keyWindow];
                }else {
                    [weakSelf.xpAlertView configWithTitle:@"确定要确认收货吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                        
                    } rightAction:^(NSString * _Nullable extra) {
                        [weakSelf orderConfirmlWithModel:model];
                    } alertType:XPAlertTypeNormal];
                }
   
            }else if (tag == 1){
                if (model.modelStatus.intValue == 1) {//造物商品去打印
                    [weakSelf showCreationWebViewWithModel:model];
                }else{//普通商品查看物流
                    [weakSelf showDeliverWithModel:model];
                }
                
            }
        }else if (status == 4 ){
            if (tag == 0) {//再次购买
                if (model.pointStatus.intValue == 1) {
                    [[XPShowTipsTool shareInstance] showMsg:@"请前往商城兑换" ToView:k_keyWindow];
                    return;
                }
                
                [self.xpAlertView configWithTitle:@"确定加入购物车吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                    
                } rightAction:^(NSString * _Nullable extra) {
                    [weakSelf buyAgainWithModel:model];
                } alertType:XPAlertTypeNormal];
                
            }else if (tag == 1){//物流信息
                [weakSelf showDeliverWithModel:model];
            }else if (tag == 2){
                [weakSelf showCommentWithModel:model];
            }
            
        }else if (status == 5 || status == 6 ){
            if (tag == 0) {//再次购买
                if (model.pointStatus.intValue == 1) {
                    [[XPShowTipsTool shareInstance] showMsg:@"请前往商城兑换" ToView:k_keyWindow];
                    return;
                }
                
                [self.xpAlertView configWithTitle:@"确定加入购物车吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                    
                } rightAction:^(NSString * _Nullable extra) {
                    [weakSelf buyAgainWithModel:model];
                } alertType:XPAlertTypeNormal];
            }else if (tag == 1){//删除订单
                [weakSelf.xpAlertView configWithTitle:@"确定要删除订单吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
                    
                } rightAction:^(NSString * _Nullable extra) {
                    [weakSelf orderDeleteWithModel:model IndexPath:indexPath];
                } alertType:XPAlertTypeNormal];
                
            }
        }
    }];
    
    [cell setTapBlock:^{
        XPShopOrderModel *model = self.dataArray[indexPath.row];
        XPShopStatusDetailController *vc = [XPShopStatusDetailController new];
        vc.model = model;
        vc.cancelOrderBlock = ^{
            weakSelf.topView.defaultSeletIndex = 4;
            weakSelf.currentIndex = 5;

        };
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = self.heightArray[indexPath.row];
    return height.floatValue;
}


//获取cell高度
- (void)getCellHeightArray{
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPShopOrderStatusCell *cell = XPShopOrderStatusCell.new;
    for (int i = 0; i<self.dataArray.count; i++) {
        XPShopOrderModel *model = self.dataArray[i];
        CGFloat height = [cell cellWithModel:model];
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    self.heightArray = tempArray.copy;
}


//获取当前选中type 类型
- (NSNumber *)getTypeParaM {
    NSNumber *num;
    switch (self.currentIndex) {
        case 1:
            num = @(0);
            break;
        case 2:
            num = @(1);
            break;
        case 3:
            num = @(2);
            break;
        case 4:
            num = @(4);
            break;
        case 5:
            num = @(5);
            break;
        default:
            num = @(0);
            break;
    }
    
    return num;
}
- (void)showEmptyView:(BOOL)shouldShow {
    self.emptyView.hidden = !shouldShow;
    self.tableView.hidden = shouldShow;
}

-(void)showCreationWebViewWithModel:(XPShopOrderModel *)model {
    NSString *urlStr = [NSString stringWithFormat:@"%@%@?jumpToken=%@&orderId=%@&isAppOrder=1",kTDNet_host,k_Creation_model_detail,[XPLocalStorage readUserToken],model.productId];
    
    XPCreateShowViewController *vc = [[XPCreateShowViewController alloc] init];
    vc.urlString = urlStr;
    vc.titleStr = @"打印";
    [self.navigationController pushViewController:vc animated:YES];
    
}

//物流跳转
- (void)showDeliverWithModel:(XPShopOrderModel *)model {
    
    if (model.deliveryStatus.intValue == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"暂无物流信息" ToView:k_keyWindow];
    }else if (model.deliveryStatus.intValue == 2 && model.packageStatus.intValue == 0){
        NSArray *packArray = model.mallOrderPackageListVOS;
        if (packArray.count > 0) {
            NSDictionary *dict = packArray.firstObject;
            XPLogisticsInfoController *vc = [XPLogisticsInfoController new];
            vc.orderId = dict[@"deliverySn"];
            vc.tplCode = dict[@"deliveryCode"];
            vc.phone = dict[@"receiverPhone"];
            vc.expressCompany = dict[@"deliveryCompany"];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (model.deliveryCode && model.deliveryCode.length > 0) {
            XPLogisticsInfoController *vc = [XPLogisticsInfoController new];
            vc.orderId = model.deliverySn;
            vc.tplCode = model.deliveryCode;
            vc.phone = model.receiverPhone;
            vc.orderStatus = [NSString stringWithFormat:@"%@",model.status];
            vc.expressCompany = model.deliveryCompany;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [[XPShowTipsTool shareInstance] showMsg:@"暂无物流信息" ToView:k_keyWindow];
        }
    }else{
        XPPackageDetailController *vc = [XPPackageDetailController new];
        vc.defaultIndex = 0;
        vc.orderId = model.productId;
        [self.navigationController pushViewController:vc animated:YES];
    }

}
//评价跳转
- (void)showCommentWithModel:(XPShopOrderModel *)model {
    XPShopProductCommentController *vc = [XPShopProductCommentController new];
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];
}

//再次购买
- (void)buyAgainWithModel:(XPShopOrderModel *)model {
    
    NSArray *productArray = model.mallOrderItemListVOS;
    
    NSMutableArray *tempArray = [NSMutableArray array];
    
    for (int i = 0; i< productArray.count; i++) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict addEntriesFromDictionary:productArray[i]];
        [dict setObject:dict[@"id"] forKey:@"orderItemId"];
        [dict setObject:dict[@"productQuantity"] forKey:@"quantity"];
        [tempArray addObject:dict];
    }
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"mallCartItemInsertJson"] = [XPCommonTool convertToJsonData:tempArray.copy];
    [self productsJoinToShopCarWithParaM:paraM];
}

#pragma mark -- 网络请求
- (void)getListData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_order_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(5);
    paraM[@"status"] = [self getTypeParaM];
    paraM[@"productName"] = self.keyStr;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPShopOrderModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            [self getCellHeightArray];
            [self.tableView reloadData];
            
            if (self.dataArray.count == 0) {
                [self showEmptyView:YES];
            }else {
                [self showEmptyView:NO];
            }
        }else {
            [self showEmptyView:YES];
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)headRefresh{
    self.page = 1;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_order_list];
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(5);
    paraM[@"status"] = [self getTypeParaM];
    paraM[@"productName"] = self.keyStr;
    
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPShopOrderModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = [modelArray mutableCopy];
            [self getCellHeightArray];
            [self.tableView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_order_list];
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(5);
    paraM[@"status"] = [self getTypeParaM];
    paraM[@"productName"] = self.keyStr;
    
    [[XPRequestManager shareInstance] commonPostWithURL:urlStr Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            
            if (dataArray.count == 0) {
                self.page --;
            }else {
                NSArray *modelArray = [XPShopOrderModel mj_objectArrayWithKeyValuesArray:dataArray];
                [self.dataArray addObjectsFromArray:modelArray];
                [self getCellHeightArray];
                [self.tableView reloadData];
            }
        }else {
            self.page --;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//删除订单
- (void)orderDeleteWithModel:(XPShopOrderModel *)model IndexPath:(NSIndexPath *)indexPath{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderDelte];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = model.productId;
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             [[XPShowTipsTool shareInstance] showMsg:@"删除订单成功" ToView:self.view];
             
             [self.dataArray removeObject:model];
             
             if (self.dataArray.count == 0) {
                 [self.tableView reloadData];
                 [self showEmptyView:YES];
             } else {
                 [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                 [self showEmptyView:NO];
              }
             
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
}
//取消订单
- (void)orderCancelWithModel:(XPShopOrderModel *)model {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderCancel];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = model.productId;
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             
             [self.dataArray removeObject:model];
             
             if (self.dataArray.count == 0) {
                 [self viewWillAppear:YES];
             }else {
                 [self.tableView reloadData];
             }
             [[XPShowTipsTool shareInstance] showMsg:@"取消订单成功" ToView:self.view];
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
}

//订单确认收货
- (void)orderConfirmlWithModel:(XPShopOrderModel *)model {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderconfirm];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = model.productId;
    [self startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [self stopLoadingGif];
         if (data.isSucess) {
             
             [self.dataArray removeObject:model];
             
             if (self.dataArray.count == 0) {
                 [self viewWillAppear:YES];
             }else {
                 [self.tableView reloadData];
             }
             
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
         }
     }];
}

//余额支付
-(void)orderAccountPayWithModel:(XPShopOrderModel *)model{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_AccountPay_shop_order];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = model.productId;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:@"支付成功!" ToView:k_keyWindow];
            self.currentIndex = 3;
            self.topView.defaultSeletIndex = 2;
            [self getListData];
            }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
}

//三方支付支付
- (void)orderPayWithModel:(XPShopOrderModel *)model PayType:(NSInteger)payType accountAmout:(NSString *)accountStr{

    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_sandPay_shop_order];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"orderId"] = model.productId;
    paraM[@"accountAmount"] = accountStr;
    paraM[@"payType"] = @(payType);
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            if(payType == 1){
                [[XPPaymentManager shareInstance] showAlipayWithDic:data.userData viewController:self];
                self.isPayShow = YES;
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if (payType == 2) {//微信支付
                self.isPayShow = YES;
                [[XPPaymentManager shareInstance] showWeChatPayWithDic:data.userData viewController:self];
                self.sendyPayOrder = data.userData[@"orderNo"];
            }else if (payType == 11){
                if ([data.userData isKindOfClass:[NSDictionary class]]  && data.userData[@"fastPayUrl"]) {
                    self.isPayShow = YES;
                    self.sendyPayOrder = data.userData[@"orderNo"];
                    [[XPPaymentManager shareInstance] showPayViewWithUrl:data.userData[@"fastPayUrl"] viewController:self];
                }else{
                    [[XPShowTipsTool shareInstance] showMsg:@"服务器内部错误,请稍后重试" ToView:k_keyWindow];
                }
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
}


- (void)getUserMoneyInfoWithModel:(XPShopOrderModel *)model{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_money_Info];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            model.fundsAmount = data.userData[@"funds"];
            
            [self showPayViewWithModel:model];
    
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}



- (void)showPayViewWithModel:(XPShopOrderModel *)model {
    
    self.payCurrentModel = model;
    
    XPPaymentView *payView = [[XPPaymentView alloc] initWithFrame:self.view.bounds];
    self.payView = payView;
    [self.view addSubview:payView];
    
    [payView viewWithNeedMoney:model.payAmount accountMoney:model.fundsAmount type:XPPaymentViewTypeBuy];
    
    XPWeakSelf
    [payView setNextBlock:^(NSInteger status, NSString * _Nonnull accountAmount) {
        if (status == 10) {//余额直接支付
            [weakSelf orderAccountPayWithModel:model];
        }else{
            [weakSelf orderPayWithModel:model PayType:status accountAmout:accountAmount];
        }
    }];
}

//多商品加入购物车
- (void)productsJoinToShopCarWithParaM:(NSDictionary *)paraM {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_carListAdd];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess){
            [[XPShowTipsTool shareInstance] showMsg:@"成功加入购物车!" ToView:k_keyWindow];
        }else{
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
    
}
@end
