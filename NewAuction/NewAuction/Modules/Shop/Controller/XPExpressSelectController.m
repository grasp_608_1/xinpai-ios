//
//  XPExpressSelectController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/29.
//

#import "XPExpressSelectController.h"
#import "XPEmptyView.h"
#import "XPDefaultLabelCell.h"

@interface XPExpressSelectController ()<UITableViewDelegate,UITableViewDataSource>
//数据源
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) XPEmptyView *emptyView;

@end

@implementation XPExpressSelectController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupUI];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getData];
}
- (void)setupUI {
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBottomHeight)];
    [self.view addSubview:self.tableView];
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无数据"];
    [self.view addSubview:self.emptyView];
}
-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"快递公司选择";
    [self.view addSubview:self.navBarView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPDefaultLabelCell *cell = [XPDefaultLabelCell cellWithTableView:tableView];
    NSDictionary *dict = self.dataArray[indexPath.row];
    [cell cellWithTitle:dict[@"name"]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.expressBlock) {
        self.expressBlock(self.dataArray[indexPath.row]);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_product_express_list];
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonGetWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
             
            self.dataArray = data.userData;
            
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)canShowEmptyView {
    if (self.dataArray.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}
@end
