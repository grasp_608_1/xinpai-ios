//
//  XPShopProductCommentController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/25.
//

#import "XPShopProductCommentController.h"
#import "XPHitButton.h"
#import "XPShopOrdeCommentItemView.h"
#import "XPShopOrderCommentModel.h"

@interface XPShopProductCommentController ()

@property (nonatomic, strong) UIScrollView *mainView;

//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) UIButton *rightButton;

@end

@implementation XPShopProductCommentController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    self.dataArray = [NSMutableArray array];
    
    [self setupUI];
}

- (void)setupUI {
    
    self.mainView = [[UIScrollView alloc] init];
    self.mainView.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - 48 -self.safeBottom);
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self.view addSubview:self.mainView];
    
    self.dataArray = [XPShopOrderCommentModel mj_objectArrayWithKeyValuesArray:self.model.mallOrderItemListVOS];
    
    CGFloat imageWH = (self.mainView.mj_w - 24)/3;
    CGFloat itemH = 412 - 124 + imageWH;
    CGFloat margin = 0;
    CGFloat itemW = self.mainView.mj_w;
    
    CGFloat bottom = 0;
    for (int i = 0; i<self.dataArray.count; i++) {
        XPShopOrderCommentModel *model = self.dataArray[i];
        
        XPShopOrdeCommentItemView *itemView = [[XPShopOrdeCommentItemView alloc] initWithFrame:CGRectMake(margin, 12 + (itemH + 12)*i, itemW, itemH)];
        [self.mainView addSubview:itemView];
        
        [itemView viewWithModel:model];
        [itemView setChangeBlock:^{
            [self checkButtonStatus];
        }];
        bottom = MaxY(itemView) + 12;
    }
    self.mainView.contentSize = CGSizeMake(self.mainView.mj_w, bottom);
    
    
    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,self.view.mj_h - 48 - kBottomHeight, self.view.mj_w, 48 + kBottomHeight);
    bottomView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:bottomView];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.titleLabel.font = kFontRegular(14);
    rightButton.frame = CGRectMake(bottomView.mj_w - 112,4,100,40);
    rightButton.layer.cornerRadius = 20;
    rightButton.layer.masksToBounds = YES;
    [rightButton setTitle:@"提交评价" forState:UIControlStateNormal];
    [rightButton setBackgroundColor:UIColorFromRGB(0xDEDEDE)];
    rightButton.userInteractionEnabled = NO;
    [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(sumbitComment:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:rightButton];
    self.rightButton = rightButton;
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"评价";
    self.navBarView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self.view addSubview:self.navBarView];
}


- (void)sumbitComment:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    [self getData];
    
}


- (void)checkButtonStatus{

    BOOL canOption = YES;
    
    for (int i = 0; i<self.dataArray.count; i++) {
        XPShopOrderCommentModel *model = self.dataArray[i];
        
        if (!model.star || model.star <= 0 || !model.textStr) {
            canOption = NO;
            break;
        }
        NSString *tempStr = [model.textStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        if(tempStr.length == 0){
            canOption = NO;
            break;
        }
    }
    
    if (canOption) {
        [self.rightButton setTitle:@"提交评价" forState:UIControlStateNormal];
        [self.rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        [self.rightButton setBackgroundColor:UIColorFromRGB(0x6E6BFF)];
//        [self.rightButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :self.rightButton colors:
//                                         @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
//                                           (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
        self.rightButton.userInteractionEnabled = YES;
    }else {
        [self.rightButton setTitle:@"提交评价" forState:UIControlStateNormal];
        [self.rightButton setBackgroundColor:UIColorFromRGB(0xDEDEDE)];
        self.rightButton.userInteractionEnabled = NO;
    }
    
}


#pragma mark -- 网络请求

- (void)getData {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderComment];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    
    paraM[@"mallProductCommentInsertJson"] = [XPCommonTool convertToJsonData:[self getCommentParaM]];
    
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
           
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (NSArray *)getCommentParaM {
    
    NSMutableArray *array = self.model.mallOrderItemListVOS.mutableCopy;
    for (int i = 0; i<array.count; i++) {
        XPShopOrderCommentModel *model = self.dataArray[i];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict addEntriesFromDictionary:array[i]];
        if (model.imagesUrlsString.length > 0) {
            [dict setObject:model.imagesUrlsString forKey:@"pics"];            
        }
        [dict setObject:model.star forKey:@"star"];
        [dict setObject:model.textStr forKey:@"content"];
        [dict setObject:dict[@"id"] forKey:@"orderItemId"];
        [array replaceObjectAtIndex:i withObject:dict];
    }
    return array.copy;
}

@end
