//
//  XPShopProductDetailController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/25.
//

#import "XPShopProductDetailController.h"
#import "XPProductImageView.h"
#import "XPTakeGoodsController.h"

@interface XPShopProductDetailController ()

@property (nonatomic, strong) UIImageView *topImageView;

@property (nonatomic, strong) UIScrollView *mainView;
//市场价
@property (nonatomic, strong) UILabel *marketPriceLabel;
//提货价
@property (nonatomic, strong) UILabel *pickPriceLabel;

@property (nonatomic, strong) UILabel *totalNumLabel;

@property (nonatomic, strong) UILabel *ptitleLabel;

@property (nonatomic, strong) UILabel *salesNum;

@property (nonatomic, strong) UILabel *perLabel;

@property (nonatomic, strong) UILabel *introLabel;

@property (nonatomic, strong) XPProductImageView *longImageView;

@end

@implementation XPShopProductDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupViews];
    [self getData];
    
}
-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.navBarView];
    self.navBarView.titleLabel.text = @"商品详情";
    
}

- (void)setupViews {
    UIScrollView *mainview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - 48 - self.safeBottom)];
    mainview.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.mainView = mainview;
    [self.view addSubview:mainview];
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(12,SCREEN_HEIGHT - self.safeBottom - 44, SCREEN_WIDTH - 2 *12, 40);
    [saveButton setTitle:@"提货" forState:UIControlStateNormal];
    saveButton.titleLabel.font = kFontMedium(16);
    [saveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    saveButton.layer.cornerRadius = 20;
    saveButton.clipsToBounds = YES;
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Rectangle_58"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveButton];
    
    
    self.topImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH)];
    [self.mainView addSubview:self.topImageView];
    
    UIView *sepView = [[UIView alloc] initWithFrame:CGRectMake(0, MaxY(self.topImageView), SCREEN_WIDTH, 1)];
    sepView.backgroundColor = UIColor.whiteColor;
    [self.mainView addSubview:sepView];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, MaxY(self.topImageView) + 1, SCREEN_WIDTH, 62)];
    imageView1.image = [UIImage imageNamed:@"Group_59"];
    [self.mainView addSubview:imageView1];
    
    UILabel *labe1 = [UILabel new];
    labe1.font = kFontMedium(14);
    labe1.textColor = UIColorFromRGB(0x3A3C41);
    labe1.frame = CGRectMake(24, 5, imageView1.mj_w - 48, 24);
    self.marketPriceLabel = labe1;
    [imageView1 addSubview:labe1];
    
    UILabel *labe2 = [UILabel new];
    labe2.font = kFontMedium(14);
    labe2.textColor = UIColor.whiteColor;
    labe2.frame = CGRectMake(24, MaxY(labe1), imageView1.mj_w *2/3.,30);
    self.pickPriceLabel = labe2;
    [imageView1 addSubview:labe2];
    
    UILabel *labe3 = [UILabel new];
    labe3.font = kFontMedium(14);
    labe3.textColor = UIColorFromRGB(0x3A3C41);
    labe3.frame = CGRectMake(imageView1.mj_w/2., 34, imageView1.mj_w/2 - 24, 24);
    labe3.text = @"";
    labe3.textAlignment = NSTextAlignmentRight;
    self.totalNumLabel= labe3;
    [imageView1 addSubview:labe3];
    
    UIView *detailView = [UIView new];
    detailView.backgroundColor = UIColor.whiteColor;
    detailView.frame = CGRectMake(0, MaxY(imageView1), SCREEN_WIDTH, 102);
    [mainview addSubview:detailView];
    
    UILabel *labe4 = [UILabel new];
    labe4.font = kFontMedium(16);
    labe4.textColor = UIColorFromRGB(0x3A3C41);
    labe4.frame = CGRectMake(24, 0, detailView.mj_w - 24*2, 48);
    labe4.numberOfLines = NO;
    self.ptitleLabel= labe4;
    [detailView addSubview:labe4];
    
    UILabel *label5 = [UILabel new];
    label5.font = kFontRegular(16);
    label5.textColor = UIColorFromRGB(0x8A8A8A);
    label5.frame = CGRectMake(24, MaxY(labe4), detailView.mj_w - 48, 18);
    self.salesNum = label5;
    [detailView addSubview:label5];
    
    
    UILabel *label6 = [UILabel new];
    label6.font = kFontRegular(16);
    label6.textColor = UIColorFromRGB(0x8A8A8A);
    label6.frame = CGRectMake(24, MaxY(label5) + 6, detailView.mj_w - 48, 18);
    self.perLabel = label6;
    [detailView addSubview:label6];
    
    UILabel *labe7 = [UILabel new];
    labe7.font = kFontMedium(16);
    labe7.textColor = UIColorFromRGB(0x3A3C41);
    labe7.frame = CGRectMake(0, MaxY(detailView) + 12, mainview.mj_w, 54);
    labe7.text = @"商品介绍";
    labe7.textAlignment = NSTextAlignmentCenter;
    labe7.backgroundColor = UIColor.whiteColor;
    self.introLabel = labe7;
    [self.mainView addSubview:labe7];

    XPProductImageView *longImageView = [[XPProductImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0)];
    longImageView.frame = CGRectMake(0, MaxY(labe7), SCREEN_WIDTH, SCREEN_WIDTH);
    self.longImageView = longImageView;
    longImageView.backgroundColor = UIColor.whiteColor;
    [self.mainView addSubview:longImageView];
    
//    CGFloat imageH = [self.longImageView configPicturesWithImageArray:self.model.imageList Title:@""];
    self.longImageView.frame = CGRectMake(0, MaxY(labe7), SCREEN_WIDTH, 20);
    self.mainView.contentSize = CGSizeMake(SCREEN_WIDTH, MaxY(self.longImageView) + 12);
    
    [self.topImageView sd_setImageWithURL:[NSURL URLWithString:self.model.pic] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];

    self.totalNumLabel.text = [NSString stringWithFormat:@"库存: %@",self.model.stock];
    
    self.ptitleLabel.text = self.model.productName;
    self.salesNum.text = [NSString stringWithFormat:@"销量  %@",self.model.sales];
    self.perLabel.text = [NSString stringWithFormat:@"规格  %@",self.model.spec];
    
    
    NSString *marketPriceStr = [XPMoneyFormatTool formatMoneyWithNum:self.model.priceMarket];
    NSString *pickPriceStr = [XPMoneyFormatTool formatMoneyWithNum:self.model.priceSale];
    
    NSString *marketTargetStr = [NSString stringWithFormat:@"市场价: ￥%@",marketPriceStr];
    NSString *pickTargetStr = [NSString stringWithFormat:@"提货价: ￥%@",pickPriceStr];
    
    NSMutableAttributedString *marketAttr = [[NSMutableAttributedString alloc] initWithString:marketTargetStr];
    [marketAttr addAttributes:@{NSFontAttributeName:kFontMedium(16),
       NSStrikethroughStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(marketTargetStr.length - marketPriceStr.length,marketPriceStr.length)];
    
    NSMutableAttributedString *pickAttr = [[NSMutableAttributedString alloc] initWithString:pickTargetStr];
    [pickAttr addAttributes:@{NSFontAttributeName:kFontMedium(24)} range:NSMakeRange(pickTargetStr.length - pickPriceStr.length,pickPriceStr.length)];
    
    self.marketPriceLabel.attributedText = marketAttr;
    self.pickPriceLabel.attributedText = pickAttr;
    
}

- (void)saveButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (![[XPVerifyTool shareInstance] checkLogin]) {
        [[XPVerifyTool shareInstance] userShouldLoginFirst];
    }else {
        XPTakeGoodsController *vc = [XPTakeGoodsController new];
        vc.goodsNum = self.model.productId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)getData {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_product_images];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = self.model.productId;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            CGFloat imageH = [self.longImageView configPicturesWithImageArray:data.userData Title:@""];
            self.longImageView.frame = CGRectMake(0, MaxY(self.introLabel), SCREEN_WIDTH, imageH);
            self.mainView.contentSize = CGSizeMake(SCREEN_WIDTH, MaxY(self.longImageView) + 12);
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
-(void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
