//
//  XPShopStatusDetailController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import "XPBaseViewController.h"
#import "XPShopOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopStatusDetailController : XPBaseViewController

@property (nonatomic, strong) XPShopOrderModel *model;

@property (nonatomic, copy) void(^cancelOrderBlock)(void);

@end

NS_ASSUME_NONNULL_END
