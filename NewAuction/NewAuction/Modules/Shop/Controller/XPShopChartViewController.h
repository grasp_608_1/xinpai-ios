//
//  XPShopChartViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/17.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopChartViewController : XPBaseViewController

@property (nonatomic, strong) NSNumber *chartId;

@end

NS_ASSUME_NONNULL_END
