//
//  XPShopMainController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/4.
//

#import "XPShopMainController.h"
#import "XPShopingController.h"
#import "XPShopCommendCollectionViewCell.h"
#import "XPShopProductDetailController.h"
#import "XPShopModel.h"
#import "XPSearchDefaultView.h"
#import "XPShopTopView.h"
#import "XPShopCategoryController.h"
#import "XPShopGoodsDetailController.h"
#import "XPShopingController.h"
#import "XPShopCarController.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "CHTCollectionViewWaterfallHeader.h"
#import "CHTCollectionViewWaterfallFooter.h"


#define XPSHOPMAIN_HEADER @"XPShopMainHeaderID"
#define XPSHOPMAIN_FOOTER @"XPShopMainFooterID"
#define XPSHOPMAIN_CELLID @"XPShopMainController"

@interface XPShopMainController ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>
@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) XPShopTopView *topView;
//当前页码
@property (nonatomic, assign) NSInteger page;

@property (nonatomic, assign) bool isFirstRequest;

@property (nonatomic, assign) CGFloat topH;
//cell高度
@property (nonatomic, strong) NSArray *heightArray;
@end

@implementation XPShopMainController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.page = 1;
    [self setupViews];
    self.dataArray = [NSMutableArray array];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(headerRefresh) name:Notification_shopList_fresh object:nil];
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    self.navBarView.leftButton.hidden = YES;
    [self.view addSubview:self.navBarView];
    
    XPSearchDefaultView *searchView = [[XPSearchDefaultView alloc] initWithFrame:CGRectMake(20, self.safeTop, SCREEN_WIDTH - 56 - 20, 40)];
    searchView.backgroundColor = RGBACOLOR(110, 107,255, 0.1);
    [searchView addTarget:self action:@selector(searchButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.navBarView addSubview:searchView];
    
    [self.navBarView.rightButton setImage:[UIImage imageNamed:@"xp_shop_car"] forState:UIControlStateNormal];
    self.navBarView.rightButton.frame = CGRectMake(self.view.mj_w - 20 - 24, self.navBarView.mj_h - 36, 24, 24);

    [self.navBarView.rightButton addTarget:self action:@selector(topRightButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)setupViews{
    
    CHTCollectionViewWaterfallLayout *flowLayout = [[CHTCollectionViewWaterfallLayout alloc] init];
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 12, 0, 12);
    flowLayout.headerHeight = 48;
    flowLayout.footerHeight = 0;
    flowLayout.minimumColumnSpacing = 12;
    flowLayout.minimumInteritemSpacing = 12;
    
    CGRect rect = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - kTabBarHeight);
    self.collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
    self.collectionView.showsVerticalScrollIndicator = NO;
    [self.collectionView registerClass:[XPShopCommendCollectionViewCell class] forCellWithReuseIdentifier:XPSHOPMAIN_CELLID];
    [self.collectionView registerClass:[CHTCollectionViewWaterfallHeader class]
        forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader
               withReuseIdentifier:XPSHOPMAIN_HEADER];
    [self.collectionView registerClass:[CHTCollectionViewWaterfallFooter class]
        forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter
               withReuseIdentifier:XPSHOPMAIN_FOOTER];

    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = UIColorFromRGB(0xF8F8FF);
//    self.collectionView.bounces = NO;
    self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:self.collectionView];
    
    XPWeakSelf
    self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [weakSelf headerRefresh];
    }];
    self.topView = [[XPShopTopView alloc] initWithFrame:CGRectMake(0, 0, self.collectionView.mj_w, 0)];
    [self.collectionView addSubview:self.topView];
   
    [self topCallBack];

}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *height = self.heightArray[indexPath.row];
    
    return CGSizeMake((SCREEN_WIDTH - 36)/2., height.floatValue);
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableView = nil;
    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
      reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                        withReuseIdentifier:XPSHOPMAIN_HEADER
                                                               forIndexPath:indexPath];
        
        CHTCollectionViewWaterfallHeader *header = (CHTCollectionViewWaterfallHeader *)reusableView;
        header.titleImageView.image = [UIImage imageNamed:@"xp_product_comment"];
        
    }
    else if ([kind isEqualToString:CHTCollectionElementKindSectionFooter]) {
      reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                        withReuseIdentifier:XPSHOPMAIN_FOOTER
                                                               forIndexPath:indexPath];
    }

    return reusableView;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    XPShopCommendCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:XPSHOPMAIN_CELLID forIndexPath:indexPath];
    XPShopModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    
    cell.backgroundColor = UIColor.whiteColor;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    XPShopModel *model = self.dataArray[indexPath.row];
    
    XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
    vc.hidesBottomBarWhenPushed = YES;
    vc.productSkuId = model.productSkuId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getData];
    if (self.dataArray.count == 0) {
        self.page = 1;
        [self getCurrentListData];
    }
}

- (void)getData {
    NSString *url;
    if (APPTYPE == 0) {
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_main_data];
    }else  if (APPTYPE == 1){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kCreation_main_data];
    }
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            CGFloat topH = [self.topView viewWithData:data.userData];
            self.topH = topH;
            self.topView.frame = CGRectMake(0, -topH, self.collectionView.mj_w, topH);
            self.collectionView.contentInset = UIEdgeInsetsMake(topH, 0, 0, 0);
            [self.collectionView reloadData];
            
            if (!self.isFirstRequest) {
                self.isFirstRequest = !self.isFirstRequest;
                self.collectionView.contentOffset = CGPointMake(0, -topH);
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

- (void)getCurrentListData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_data_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(10);
    paraM[@"sortType"] = @(1);
    paraM[@"pageType"] = @(21);

    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        if (data.isSucess) {
            self.dataArray = [XPShopModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            [self getCellHeightArray];
            [self.collectionView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

- (void)headerRefresh {
        
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_data_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(10);
    paraM[@"sortType"] = @(1);
    paraM[@"pageType"] = @(21);
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        [self.collectionView.mj_header endRefreshing];
        if (data.isSucess) {
            self.dataArray = [XPShopModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            [self getCellHeightArray];
            [self.collectionView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    
    self.page ++;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_data_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSString stringWithFormat:@"%zd",self.page];
    paraM[@"pageSize"] = @(10);
    paraM[@"sortType"] = @(1);
    paraM[@"pageType"] = @(21);
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.collectionView.mj_footer endRefreshing];
        if (data.isSucess) {
            
            NSArray *modelArray = [XPShopModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            if (modelArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self getCellHeightArray];
                [self.collectionView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

//获取cell高度
- (void)getCellHeightArray{
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPShopCommendCollectionViewCell *cell = XPShopCommendCollectionViewCell.new;
    for (int i = 0; i<self.dataArray.count; i++) {
        XPShopModel *model = self.dataArray[i];
        CGFloat height = [cell cellWithModel:model];
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    self.heightArray = tempArray.copy;
}

#pragma mark -- top click
- (void)searchButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPShopingController *vc = [XPShopingController new];
    vc.keyStr = @"";
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)topRightButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (![[XPVerifyTool shareInstance] checkLogin]) {
        [[XPVerifyTool shareInstance] userShouldLoginFirst];
        return;
    }
    XPShopCarController *vc = [XPShopCarController new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)topCallBack {
    XPWeakSelf
    [self.topView setMoreBlock:^(NSDictionary * _Nonnull dic) {
        XPShopCategoryController *vc = [XPShopCategoryController new];
        vc.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    [self.topView setBannerSelectBlock:^(NSDictionary * _Nonnull dic) {
        [XPRoutetool entryWithDic:dic];
    }];
    
    [self.topView setItemSelectBlock:^(NSDictionary * _Nonnull dic) {
        XPShopingController *vc = [XPShopingController new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.categoryIdTwo = dic[@"id"];
        vc.enterType = 6;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    [self.topView setCommendItemBlock:^(NSDictionary * _Nonnull dic) {
        XPShopingController *vc = [XPShopingController new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.subjectId = dic[@"id"];
        vc.enterType = 2;
        vc.noteLongTitle = dic[@"noteLong"];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];

    [self.topView setActivityItemBlock:^(NSDictionary * _Nonnull dic) {
        XPShopingController *vc = [XPShopingController new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.activityId = dic[@"id"];
        vc.enterType = 4;
        vc.noteLongTitle = dic[@"noteLong"];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
}

@end
