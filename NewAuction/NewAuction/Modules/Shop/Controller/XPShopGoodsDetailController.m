//
//  XPShopGoodsDetailController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/9.
//

#import "XPShopGoodsDetailController.h"
#import "XPUserAddressController.h"
#import "XPProductImageView.h"
#import "XPLunboView.h"
#import "XPShopProductMainView.h"
#import "XPShopCreationProductView.h"
#import "XPShopProductSpecView.h"
#import "XPShopCommentView.h"
#import "XPHitButton.h"
#import "XPShopDetailBottomView.h"
#import "XPShopCouponPopView.h"
#import "XPShopSpecPopView.h"
#import "XPShopCommentPopView.h"
#import "XPShopAddressPopView.h"
#import "XPAdressModel.h"
#import "XPShopProductDetailModel.h"
#import "XPShopOrderConfirmController.h"
#import "XPShopOrderConfirmModel.h"
#import "XPBaseWebViewViewController.h"
#import "XPShopCarController.h"
#import "XPShopActivityPopView.h"
#import "XPShopingController.h"
#import "XPTakeGoodsController.h"
#import "XPCreationAuthorController.h"
#import "XPShopChartViewController.h"
#import "UMCommon/MobClick.h"
#import "XPShopTeamView.h"
#import "XPPreSaleStatusView.h"
#import "XPShopChartView.h"
#import "XPShopCreationAuthorView.h"
#import <WebKit/WebKit.h>

@interface XPShopGoodsDetailController ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIScrollView *mainView;
//轮播图
@property (nonatomic, strong) XPLunboView *topView;
//模型加载
@property (nonatomic, strong) WKWebView *modelWebView;
//造物作者区
@property (nonatomic, strong) XPShopCreationAuthorView *authorView;
//价格区
@property (nonatomic, strong) XPShopProductMainView *priceView;
//价格区
@property (nonatomic, strong) XPShopCreationProductView *createPriceView;
//规格地址区
@property (nonatomic, strong) XPShopProductSpecView *specView;
//排行榜
@property (nonatomic, strong) XPShopChartView *chartView;
//拼团
@property (nonatomic, strong) XPShopTeamView *teamView;
//预售
@property (nonatomic, strong) XPPreSaleStatusView *preSaleStatusView;
//评论区
@property (nonatomic, strong) XPShopCommentView *commentView;
//图文详情区
@property (nonatomic, strong) XPProductImageView *longImageView;
//底部导航栏
@property (nonatomic, strong) XPShopDetailBottomView *bottomView;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//返回按钮
@property (nonatomic, strong) XPHitButton *backButton;
//购物车按钮
@property (nonatomic, strong) XPHitButton *shopCarButton;
//规格弹窗
@property (nonatomic, strong) XPShopSpecPopView *specPopView;
//地址弹窗
@property (nonatomic, strong) XPShopAddressPopView *addressPopView;
//活动弹窗
@property (nonatomic, strong) XPShopActivityPopView *activityPopView;
//邮费弹窗
@property (nonatomic, strong) XPShopActivityPopView *transportPopView;
//当期选中的地址model
@property (nonatomic, strong) XPAdressModel *addressModel;
@property (nonatomic, strong) XPShopProductDetailModel *detailModel;
//是否展示造物逻辑
@property (nonatomic, strong) NSNumber *isShow;
//优惠券modelArray
@property (nonatomic, copy) NSArray *couponArray;
//轮播array
@property (nonatomic, copy) NSArray *lunboArray;
//拼团参数
@property (nonatomic, copy) NSDictionary *groupDic;

@end

@implementation XPShopGoodsDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self setupViews];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearDefaultAddress:) name:Notification_user_choose_adress object:nil];
}


-(void)reloadUI{
    self.bottomView.hidden = NO;
    
    CGFloat bottom;
    if (self.detailModel.modelStatus.intValue == 1) {
        
        self.topView.hidden = YES;
        self.modelWebView.hidden = NO;
        NSDictionary *shortInfo = self.detailModel.productSkuShortInfoVO;
        
        [self.modelWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:TO_STR(shortInfo[@"modelUrl"])]]];
        bottom = MaxY(self.modelWebView);
    }else {
        self.topView.hidden = NO;
        self.modelWebView.hidden = YES;
        [self getLunboArrayWithProductSkuId:self.productSkuId];
        [self.topView setStaticLuboArray:self.lunboArray];
        
        bottom = MaxY(self.topView);
    }
  
    if (self.detailModel.modelStatus.intValue == 1) {
        
        [self.authorView viewWithDataModel:self.detailModel];
        self.authorView.frame = CGRectMake(0, bottom + 12, self.mainView.mj_w, 72);
        bottom = MaxY(self.authorView);
        
        self.authorView.hidden = NO;
        self.priceView.hidden = YES;
        self.createPriceView.hidden = NO;
        CGFloat priceH = [self.createPriceView viewWithDataModel:self.detailModel];
        self.createPriceView.frame = CGRectMake(0, bottom + 12, self.mainView.mj_w, priceH);
        bottom = MaxY(self.createPriceView);
    }else{
        self.authorView.hidden = YES;
        self.priceView.hidden = NO;
        self.createPriceView.hidden = YES;
        CGFloat priceH = [self.priceView viewWithDataModel:self.detailModel];
        self.priceView.frame = CGRectMake(0, bottom + 12, self.mainView.mj_w, priceH);
        bottom = MaxY(self.priceView);
    }
   
    //是否有排行榜
    if(self.detailModel.productChartsVO.allKeys.count > 0){
        [self.chartView viewWithDic:self.detailModel.productChartsVO];
        self.chartView.hidden = NO;
        self.chartView.frame = CGRectMake(12, bottom + 12, self.mainView.mj_w - 24, 48);
        
        bottom = MaxY(self.chartView);
    }else {
        self.chartView.hidden = YES;
    }

    if (self.detailModel.presaleStatus.intValue == 1) {//预售
        [self.preSaleStatusView viewWithDetailDic:self.detailModel.productSkuShortInfoVO[@"skuPrepareVO"]];
        self.preSaleStatusView.hidden = NO;
        self.preSaleStatusView.frame = CGRectMake(12, bottom + 12, self.mainView.mj_w -24 , 80);
        bottom = MaxY(self.preSaleStatusView);
    }else if (self.detailModel.groupStatus.intValue == 1){
        NSDictionary *skuGroupVO = self.detailModel.productSkuShortInfoVO[@"skuGroupVO"];
        NSArray *skuActivityGroupVOs = skuGroupVO[@"skuActivityGroupVOs"];
        if (skuActivityGroupVOs.count > 0) {
            CGFloat teamH = [self.teamView viewWithDataModel:self.detailModel];
            self.teamView.frame = CGRectMake(12, bottom + 12, self.mainView.mj_w - 24, teamH);
            self.teamView.hidden = NO;
            bottom = MaxY(self.teamView);
        }
    }else{
        self.preSaleStatusView.hidden = YES;
        self.teamView.hidden = YES;
        [self.teamView clear];
    }
    
    CGFloat specH = [self.specView viewWithDataModel:self.detailModel];
    self.specView.frame = CGRectMake(0, bottom + 12, self.mainView.mj_w, specH);
    
    bottom = MaxY(self.specView);
    
    if (self.detailModel.productCommentVO.allKeys.count == 0) {
        self.commentView.hidden = YES;
    }else{
        self.commentView.hidden = NO;
        CGFloat commentH = [self.commentView viewWithDataModel:self.detailModel];
        self.commentView.frame = CGRectMake(0, bottom + 12, self.mainView.mj_w, commentH);
        bottom = MaxY(self.commentView);
    }
    
    CGFloat imageH = [self.longImageView configPicturesWithImageArray:self.detailModel.productImagesVOs Title:@"商品详情"];
    if (self.detailModel.productImagesVOs.count == 0) {
        self.longImageView.hidden = YES;
        self.mainView.contentSize = CGSizeMake(self.mainView.mj_w, bottom + 12);
    }else {
        self.longImageView.hidden = NO;
        self.longImageView.frame = CGRectMake(0, bottom + 12, SCREEN_WIDTH, imageH);
        self.mainView.contentSize = CGSizeMake(self.mainView.mj_w, MaxY(self.longImageView) + 12);
    }
    
    if (self.detailModel.isFavorite.intValue == 1) {
        self.bottomView.hasCollected = YES;
    }else {
        self.bottomView.hasCollected = NO;
    }
    
    if (self.detailModel.modelStatus.intValue == 0) {//非造物商品
        self.shopCarButton.frame = CGRectMake( SCREEN_WIDTH - 20 - 27, 17 + self.safeTop, 27, 28);
        [self.shopCarButton setImage:[UIImage imageNamed:@"xp_shop_car_bg_gray"] forState:UIControlStateNormal];
        self.shopCarButton.backgroundColor = UIColor.clearColor;
        self.shopCarButton.layer.cornerRadius = 0;
        [self.shopCarButton setTitle:@"" forState:UIControlStateNormal];
        
    }else {//造物商品
        
        NSString *likeNumStr = [NSString stringWithFormat:@" %@",self.detailModel.productSkuShortInfoVO[@"worksSkuVO"][@"likeNum"]];
        CGFloat likeW = [likeNumStr sizeWithAttributes:@{NSFontAttributeName:kFontRegular(14)}].width;
        self.shopCarButton.frame = CGRectMake( SCREEN_WIDTH - 24 - likeW - 38 , self.topView.mj_h - 28 - 24, likeW + 28 + 10, 28);
        [self.shopCarButton setImage:[UIImage imageNamed:@"xp_circle_like_white"] forState:UIControlStateNormal];
        self.shopCarButton.backgroundColor = RGBACOLOR(0, 0, 0, 0.5);
        self.shopCarButton.titleLabel.font = kFontRegular(14);
        [self.shopCarButton setTitle:likeNumStr forState:UIControlStateNormal];
        [self.shopCarButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        self.shopCarButton.layer.cornerRadius = 14;
    }
    [self.bottomView configUIWithModel:self.detailModel isShowCreation:self.isShow.intValue];
    
    
}

- (void)setupViews {
    UIScrollView *mainview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 48 - self.safeBottom)];
    mainview.backgroundColor = UIColorFromRGB(0xF2F2F2);
    mainview.showsVerticalScrollIndicator = NO;
    mainview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    mainview.bounces = NO;
    self.mainView = mainview;
    [self.view addSubview:mainview];
    
    
    self.topView = [[XPLunboView alloc] initWithFrame:CGRectMake(0, 0, self.view.mj_w, self.view.mj_w)];
    self.topView.forbidScroll = YES;
    self.topView.cornerRadius = 0;
    [self.mainView addSubview:self.topView];
    
    self.modelWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.mj_w, self.view.mj_w)];
    self.modelWebView.tag = 999;
    [self.mainView addSubview:self.modelWebView];

    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [self.modelWebView addGestureRecognizer:panGestureRecognizer];
    
    
    self.authorView = [[XPShopCreationAuthorView alloc] initWithFrame:CGRectMake(0, 0, self.mainView.mj_w, 0)];
    self.authorView.layer.masksToBounds = YES;
    [self.mainView addSubview:self.authorView];
    
    self.priceView = [[XPShopProductMainView alloc] initWithFrame:CGRectMake(0, MaxY(self.topView) + 12, self.mainView.mj_w, 0)];
    [self.mainView addSubview:self.priceView];
    
    self.createPriceView = [[XPShopCreationProductView alloc] initWithFrame:CGRectMake(0, MaxY(self.topView) + 12, self.mainView.mj_w, 0)];
    [self.mainView addSubview:self.createPriceView];
    
    self.chartView = [[XPShopChartView alloc] initWithFrame:CGRectMake(0, MaxY(self.topView) + 12, self.mainView.mj_w - 24, 0)];
    self.chartView.hidden = YES;
    [self.mainView addSubview:self.chartView];
    
    self.preSaleStatusView = [[XPPreSaleStatusView alloc] initWithFrame:CGRectMake(12, MaxY(self.priceView) + 12, self.mainView.mj_w -24 , 60)];
    self.preSaleStatusView.hidden = YES;
    [self.mainView addSubview:self.preSaleStatusView];
    
    self.specView = [[XPShopProductSpecView alloc] initWithFrame:CGRectMake(0, MaxY(self.teamView) + 12, self.mainView.mj_w, 0)];
    [self.mainView addSubview:self.specView];
    self.commentView = [[XPShopCommentView alloc] initWithFrame:CGRectMake(0, MaxY(self.specView) + 12, self.mainView.mj_w, 140)];
    [self.mainView addSubview:self.commentView];
    
    XPProductImageView *longImageView = [[XPProductImageView alloc] initWithFrame:CGRectMake(0, MaxY(self.commentView) + 12, SCREEN_WIDTH, 0)];
    self.longImageView = longImageView;
    [self.mainView addSubview:longImageView];
    
    self.backButton = [[XPHitButton alloc] init];
    self.backButton.frame = CGRectMake(20, 17 + self.safeTop, 27, 27);
    self.backButton.hitRect = 50;
    [self.backButton setImage:[UIImage imageNamed:@"xp_nav_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.backButton];
    
    self.shopCarButton = [[XPHitButton alloc] init];
    self.shopCarButton.hitRect = 50;
    self.shopCarButton.titleLabel.font = kFontRegular(16);
    [self.shopCarButton addTarget:self action:@selector(shopCarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.shopCarButton];
    
    XPShopDetailBottomView *bottom = [[XPShopDetailBottomView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - self.safeBottom - 48, SCREEN_WIDTH, 48 + self.safeBottom)];
    bottom.backgroundColor = UIColor.whiteColor;
    bottom.hidden = YES;
    self.bottomView = bottom;
    [self.view addSubview:bottom];
    
    [self callBack];
}

//按钮回调
- (void)callBack {
    
    XPWeakSelf
    [self.authorView setTapBlock:^{
        
        if (![[XPVerifyTool shareInstance] checkLogin]) {
            [[XPVerifyTool shareInstance] userShouldLoginFirst];
            return;
        }
        
        XPCreationAuthorController *vc = [XPCreationAuthorController new];
        vc.authorUserId = weakSelf.detailModel.makerId;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    [self.priceView setDiscountBlock:^{
        XPShopCouponPopView *popView = [[XPShopCouponPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [k_keyWindow addSubview:popView];
        [popView showWithDataArray:weakSelf.couponArray];
    }];
    
    [self.createPriceView setDiscountBlock:^{
        XPShopCouponPopView *popView = [[XPShopCouponPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [k_keyWindow addSubview:popView];
        [popView showWithDataArray:weakSelf.couponArray];
    }];
    
    [self.chartView setTapBlock:^(NSDictionary * _Nonnull dic) {
        XPShopChartViewController *vc = [XPShopChartViewController new];
        vc.chartId = dic[@"id"];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    [self.specView setSpecBlock:^(NSInteger index) {
        if (index == 0) {//规格点击
            XPShopSpecPopView *popView = [[XPShopSpecPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            weakSelf.specPopView = popView;
            [k_keyWindow addSubview:popView];
            [popView showWithModel:weakSelf.detailModel ViewType:XPProductSpecPopTypeNormal];
            
            [popView setCancelBlock:^(XPShopSpecModel * _Nonnull model) {
                if (self.productSkuId.integerValue != model.productSkuId.integerValue) {
                    weakSelf.productSkuId = model.productSkuId;
                    [weakSelf clearTeamView];
                    [weakSelf getData];
                }
            }];
            
            [popView setBottomBlock:^(NSInteger index, NSInteger countNum, XPShopSpecModel * _Nonnull model) {
                [weakSelf.specView viewWithDataModel:weakSelf.detailModel];
                weakSelf.detailModel.productSkuId = model.productSkuId;
                if (index == 0) {//加入购物车
                    [weakSelf addToMallCarWithCount:countNum];
                }else {//立即购买
                    [weakSelf jumpOrderWithCount:countNum JumpType:0];
                }
            }];
            
        }else if (index == 1){//地址点击
            
            if (![[XPVerifyTool shareInstance] checkLogin]) {
                [[XPVerifyTool shareInstance] userShouldLoginFirst];
                return;
            }
            
            XPShopAddressPopView *popView = [[XPShopAddressPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            weakSelf.addressPopView = popView;
            [k_keyWindow addSubview:popView];
            
            [popView showPopViewWithController:weakSelf adressModel:weakSelf.specView.addressModel];
            
            [popView setAddressSelectBlock:^(XPAdressModel * _Nonnull model) {
                weakSelf.addressModel = model;
                weakSelf.specView.addressModel = model;
                [weakSelf.addressPopView removeFromSuperview];
            }];
            
            [popView setAddressChangeBlock:^{
                XPUserAddressController *vc = [XPUserAddressController new];
                [weakSelf.navigationController pushViewController:vc animated:YES];
                [weakSelf.addressPopView removeFromSuperview];
            }];
        }else if (index == 2){//活动
            XPShopActivityPopView *popView = [[XPShopActivityPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            weakSelf.activityPopView = popView;
            [k_keyWindow addSubview:popView];
            
            [popView viewWithModel:weakSelf.detailModel type:XPShopPopShowTypeActivity];
        
            [popView setItemTapBlock:^{
                XPShopingController *vc = [XPShopingController new];
                vc.enterType = 4;
                vc.noteLongTitle = weakSelf.detailModel.activityNoteLong;
                vc.activityId = weakSelf.detailModel.activityId;
                [weakSelf.navigationController pushViewController:vc animated:YES];
                [weakSelf.activityPopView removeFromSuperview];
            }];
            
            
        }else if(index == 3){
            XPShopActivityPopView *popView = [[XPShopActivityPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            weakSelf.transportPopView = popView;
            [k_keyWindow addSubview:popView];
            
            [popView viewWithModel:weakSelf.detailModel type:XPShopPopShowTypeTransport];
        
        }
        
    }];
    [self.commentView setCommentBlock:^{
        XPShopCommentPopView *popView = [[XPShopCommentPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [k_keyWindow addSubview:popView];
        [popView showWithModel:weakSelf.detailModel];
    }];
    
    [self.addressPopView setAddressSelectBlock:^(XPAdressModel * _Nonnull model) {
        weakSelf.specView.addressModel = model;
        [weakSelf.addressPopView removeFromSuperview];
    }];
    [self.addressPopView setAddressChangeBlock:^{
        XPUserAddressController *vc = [XPUserAddressController new];
        [weakSelf.navigationController pushViewController:vc animated:YES];
        [weakSelf.addressPopView removeFromSuperview];
    }];
    
    [self.bottomView setItemClickedBlock:^(NSInteger index) {
        
        if (![[XPVerifyTool shareInstance] checkLogin]) {
            [[XPVerifyTool shareInstance] userShouldLoginFirst];
            return;
        }
        
        if(index == 0){//收藏
            if (self.detailModel.isFavorite.intValue == 0) {
                [weakSelf addFavoriteProduct];
            }else {
                [weakSelf deleteFavoriteProduct];
            }
        }else if (index == 1){//客服
            XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
            vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,k_user_help];
            vc.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:vc animated:YES];
            
        }else if (index == 2){//购物车
            [weakSelf shopCarButtonClicked:UIButton.new];
        }
        else if (index == 3){//加入购物车
            [weakSelf addToMallCarWithCount:1];
        }
        else if (index == 4){//立即购买
            [weakSelf orderComfirmWithCount:1 JumpType:0];
        }else if (index == 5){//提货
            
            //弹规格选择框
            XPShopSpecPopView *popView = [[XPShopSpecPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            weakSelf.specPopView = popView;
            [k_keyWindow addSubview:popView];
            [popView showWithModel:weakSelf.detailModel ViewType:XPProductSpecPopTypeSure];
            [popView hiddenCountView:YES];
            [popView setBottomBlock:^(NSInteger index, NSInteger countNum, XPShopSpecModel * _Nonnull model) {
                [weakSelf.specView viewWithDataModel:weakSelf.detailModel];
                weakSelf.detailModel.productSkuId = model.productSkuId;
               
                XPTakeGoodsController *vc = [XPTakeGoodsController new];
                vc.goodsNum = weakSelf.detailModel.productId;
                vc.productSkuId = weakSelf.detailModel.productSkuId;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }];
        }else if (index == 6){//定金
//            [weakSelf orderComfirmWithCount:1 JumpType:4];
            [weakSelf jumpOrderWithCount:1 JumpType:4];
        }else if(index == 7){//拼团
//            [weakSelf orderComfirmWithCount:1 JumpType:3];
            [weakSelf jumpOrderWithCount:1 JumpType:3];
        }
    }];
    
}
#pragma mark -- 地址清空处理
- (void)clearDefaultAddress:(NSNotification *)noti{
    
    XPAdressModel *model = [noti object];
    if (self.addressModel.productId && [model.productId isEqualToNumber:self.addressModel.productId]) {
        
        self.specView.addressModel = [XPAdressModel new];
    }
    
}
#pragma mark --- 网络请求
- (void)getData {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_product_detail];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = self.productSkuId;
    paraM[@"userAddressId"] = self.addressModel.productId;
    if (self.voiceSearchStatus.intValue == 1) {
        paraM[@"voiceSearchStatus"] = @(1);
    }
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.isShow = data.isShow;
            self.detailModel = [XPShopProductDetailModel mj_objectWithKeyValues:data.userData];
            if (!self.addressModel) {
                self.addressModel = [XPAdressModel mj_objectWithKeyValues:self.detailModel.userAddressVO];
            }
            self.couponArray = [XPCouponModel mj_objectArrayWithKeyValuesArray:self.detailModel.userCouponVOs];
            [self reloadUI];
            
            [self addUMSKUPoints];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
//添加到购物车
- (void)addToMallCarWithCount:(NSInteger) quantity{
    if (self.addressModel.area.length == 0 ||[self.addressModel.area isEqualToString:@"null"]) {
        [[XPShowTipsTool shareInstance] showMsg:@"请先添加收货地址!" ToView:k_keyWindow];
        return;
    }
    
    //弹规格选择框
    XPWeakSelf
    XPShopSpecPopView *popView = [[XPShopSpecPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    weakSelf.specPopView = popView;
    [k_keyWindow addSubview:popView];
    [popView showWithModel:weakSelf.detailModel ViewType:XPProductSpecPopTypeSure];
   
    [popView setCancelBlock:^(XPShopSpecModel * _Nonnull model) {
        if (self.productSkuId.integerValue != model.productSkuId.integerValue) {
            weakSelf.productSkuId = model.productSkuId;
            [weakSelf clearTeamView];
            [weakSelf getData];
        }
    }];
    [popView setBottomBlock:^(NSInteger index, NSInteger countNum, XPShopSpecModel * _Nonnull model) {
        [weakSelf.specView viewWithDataModel:weakSelf.detailModel];
        weakSelf.detailModel.productSkuId = model.productSkuId;
        [weakSelf addToMallCarRequestWithCount:countNum];
    }];
    
}

-(void)addToMallCarRequestWithCount:(NSInteger) quantity{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_carAdd];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"productId"] = self.detailModel.productId;
    paraM[@"productSkuId"] = self.detailModel.productSkuId;
    paraM[@"quantity"] = @(quantity);
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
        
            [[XPShowTipsTool shareInstance] showMsg:@"加入购物车成功!" ToView:self.view];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)orderComfirmWithCount:(NSInteger) quantity JumpType:(NSInteger)type{
    
    //弹规格选择框
    XPWeakSelf
    XPShopSpecPopView *popView = [[XPShopSpecPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    weakSelf.specPopView = popView;
    [k_keyWindow addSubview:popView];
    [popView showWithModel:weakSelf.detailModel ViewType:XPProductSpecPopTypeSure];
    
    [popView setCancelBlock:^(XPShopSpecModel * _Nonnull model) {
        if (self.productSkuId.integerValue != model.productSkuId.integerValue) {
            weakSelf.productSkuId = model.productSkuId;
            [weakSelf clearTeamView];
            [weakSelf getData];
        }
    }];
    
    [popView setBottomBlock:^(NSInteger index, NSInteger countNum, XPShopSpecModel * _Nonnull model) {
        [weakSelf.specView viewWithDataModel:weakSelf.detailModel];
        weakSelf.detailModel.productSkuId = model.productSkuId;
        [weakSelf jumpOrderWithCount:countNum JumpType:type];
    }];
}

//0 单商品提交  1 购物车,多商品提交  2 拼团 3 开团 4 预售 5 积分商品
- (void)jumpOrderWithCount:(NSInteger) quantity JumpType:(NSInteger)type{
    
    if (self.addressModel.area.length == 0 ||[self.addressModel.area isEqualToString:@"null"]) {
        [[XPShowTipsTool shareInstance] showMsg:@"请先添加收货地址!" ToView:k_keyWindow];
        return;
    }
    
    NSDictionary *productSkuShortInfoVO = self.detailModel.productSkuShortInfoVO;
    if (productSkuShortInfoVO[@"pointStatus"] && [productSkuShortInfoVO[@"pointStatus"] intValue] == 1) {
        type = 5;
    }
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"userAddressId"] = self.addressModel.productId;
    paraM[@"productSkuId"] = self.detailModel.productSkuId;
    paraM[@"productQuantity"] = @(quantity);
    if (self.voiceSearchStatus.intValue == 1) {
        paraM[@"voiceSearchStatus"] = @(1);
    }
    
    NSString *url;
    if (type == 0 || type == 1 || type == 5) {
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderConfirmBuy];
    }else if(type == 2){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderConfirmGroupBuy];
        paraM[@"groupId"] = self.groupDic[@"id"];
    }else if (type == 3){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderConfirmGroupBuy];
        paraM[@"groupId"] = @(0);
    }else if (type == 4){
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderConfirmPresaleBuy];
    }
    
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
        
            XPShopOrderConfirmModel *model = [XPShopOrderConfirmModel mj_objectWithKeyValues:data.userData];
            XPShopOrderConfirmController *vc = [XPShopOrderConfirmController new];
            vc.entryType = type;
            vc.groupDic = self.groupDic;
            model.singleQuality = quantity;
            if (self.voiceSearchStatus.intValue == 1) {
                vc.voiceSearchStatus = @(1);
            }
            vc.model = model;
            [self.navigationController pushViewController:vc animated:YES];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)shopCarButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (![[XPVerifyTool shareInstance] checkLogin]) {
        [[XPVerifyTool shareInstance] userShouldLoginFirst];
        return;
    }
    
    if (self.detailModel.modelStatus.intValue == 1) {
        [self addLikeProduct];
    }else {
        XPShopCarController *vc = [XPShopCarController new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
//点赞
- (void)addLikeProduct {
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kVERIFY_Get_shopLike_add,self.detailModel.productSkuShortInfoVO[@"worksId"]];

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:@"点赞成功" ToView:self.view];
            [self getData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
        
    }];
}

//添加收藏
- (void)addFavoriteProduct {
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kVERIFY_Get_shopFav_add,self.detailModel.productSkuId];

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.detailModel.isFavorite = @(1);
            self.bottomView.hasCollected = YES;
            [[XPShowTipsTool shareInstance] showMsg:@"收藏成功" ToView:self.view];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
        
    }];
}

//取消收藏
- (void)deleteFavoriteProduct {
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kVERIFY_host,kVERIFY_Get_shopFav_delete,self.detailModel.productSkuId];

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.detailModel.isFavorite = @(0);
            self.bottomView.hasCollected = NO;
            [[XPShowTipsTool shareInstance] showMsg:@"已取消收藏" ToView:self.view];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)getLunboArrayWithProductSkuId:(NSNumber *)skuNum{
    NSMutableArray *tempArray = [NSMutableArray array];

    for (int i = 0; i < self.detailModel.productSkuShortInfoVOs.count; i++) {
        NSDictionary *dict = self.detailModel.productSkuShortInfoVOs[i];
        if ([skuNum isEqual:dict[@"productSkuId"]]) {
             NSArray *array = dict[@"productImagesRotationVOs"];
            [tempArray addObjectsFromArray:array];
            [tempArray insertObject:dict atIndex:0];
            break;
        }
    }
    self.lunboArray = tempArray;
}

#pragma mark --- gesture delegate
// 处理拖动手势,防止webview和scrollView冲突
- (void)handlePanGesture:(UIPanGestureRecognizer *)gestureRecognizer {
    // 子控件的拖动操作
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        // 禁用 UIScrollView 滚动
        self.mainView.scrollEnabled = NO;
    } else if (gestureRecognizer.state == UIGestureRecognizerStateEnded || gestureRecognizer.state == UIGestureRecognizerStateCancelled) {
        // 恢复 UIScrollView 滚动
        self.mainView.scrollEnabled = YES;
    }
}

#pragma UM Points
- (void)addUMSKUPoints{
    //埋点
    NSString *phone = [XPLocalStorage readUserUserPhone];
    [MobClick event:@"xp_shop_detail_skuId" label:[NSString stringWithFormat:@"%@_%@_%@",phone.length > 0 ? phone:@"0",self.detailModel.productId,self.detailModel.productSkuId]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getData];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear: animated];
    [self clearTeamView];
}

-(void)clearTeamView {
    [self.teamView clear];
    [self.teamView removeFromSuperview];
    self.teamView = nil;
}

-(XPShopTeamView *)teamView{
    if (_teamView == nil) {
      _teamView = [[XPShopTeamView alloc] initWithFrame:CGRectMake(12, MaxY(self.priceView) + 12, self.mainView.mj_w -24, 0)];
        _teamView.hidden = YES;
        [self.mainView addSubview:self.teamView];
        XPWeakSelf
        [self.teamView setJoinBlock:^(NSDictionary * _Nonnull dic) {
            weakSelf.groupDic = dic;
            [weakSelf jumpOrderWithCount:1 JumpType:2];
        }];
    }
    return _teamView;
}



@end
