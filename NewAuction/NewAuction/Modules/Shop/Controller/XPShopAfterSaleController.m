//
//  XPShopAfterSaleController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/20.
//

#import "XPShopAfterSaleController.h"
#import "XPShopOrderPruductView.h"
#import "XPLimitInputTextView.h"
#import "XPImageAddView.h"
#import "IQKeyboardManager.h"

@interface XPShopAfterSaleController ()

@property (nonatomic, strong) UIScrollView *mainView;

@property (nonatomic, strong) XPShopOrderPruductView *item;
//退款原因选择
@property (nonatomic, strong) UIView *reasonView;
//退款原因
@property (nonatomic, strong) UILabel *typeLabel;
//问题描述
@property (nonatomic, strong) UIView *commentView;
//内容
@property (nonatomic, strong) XPLimitInputTextView *textView;
//添加图片
@property (nonatomic, strong) XPImageAddView *imageAddView;

@property (nonatomic, strong) NSString *addPics;

@end

@implementation XPShopAfterSaleController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.addPics = @"";
    [self setupViews];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 32;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 0;
}
-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.navBarView];
    
    if (self.entryType == 1) {
        self.navBarView.titleLabel.text = @"仅退款";
    }else if (self.entryType == 2){
        self.navBarView.titleLabel.text = @"退货退款";
    }
    
}

- (void)setupViews {
    self.mainView =  [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTopHeight + 12, SCREEN_WIDTH,SCREEN_HEIGHT - kTopHeight - 12 - 48 - kBottomHeight)];
    self.mainView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.mainView];
    
    XPShopOrderPruductView *item = [[XPShopOrderPruductView alloc] initWithFrame:CGRectMake(12, 0, self.mainView.mj_w - 24, 112)];
    item.layer.cornerRadius = 8;
    [self.mainView addSubview:item];
    CGFloat itemH = [item viewWithModel:self.model viewType:XPOrderProductListTypeSubTitle showStatus:NO];
    self.item = item;
    item.frame = CGRectMake(12, 0, self.mainView.mj_w - 24, itemH);
    
    UIButton *reasonView = [[UIButton alloc] initWithFrame:CGRectMake(12, MaxY(self.item) + 12, self.mainView.mj_w - 24, 56)];
    reasonView.backgroundColor = [UIColor whiteColor];
    reasonView.layer.cornerRadius = 8;
    [reasonView addTarget:self action:@selector(topButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.reasonView = reasonView;
    [self.mainView addSubview:reasonView];
    
    UILabel *typeDescabel = [UILabel new];
    typeDescabel.font = kFontRegular(16);
    typeDescabel.textColor = app_font_color;
    typeDescabel.text = @"退款原因";
    typeDescabel.frame = CGRectMake(16, 0, 150, reasonView.mj_h);
    [reasonView addSubview:typeDescabel];
    
    UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(reasonView.mj_w - 16 - 24, 16, 24, 24)];
    arrow.image = [UIImage imageNamed:@"xp_setting_arrow"];
    [reasonView addSubview:arrow];
    
    UILabel *typeLabel = [UILabel new];
    typeLabel.font = kFontRegular(14);
    typeLabel.textColor = UIColorFromRGB(0x8A8A8A);
    typeLabel.textAlignment = NSTextAlignmentRight;
    self.typeLabel = typeLabel;
    typeLabel.frame = CGRectMake(reasonView.mj_w - 40 - 155, 0, 150, reasonView.mj_h);
    [reasonView addSubview:typeLabel];
    
    self.commentView = [[UIView alloc]initWithFrame:CGRectMake(12, MaxY(self.reasonView) + 12, self.view.mj_w - 24, 300)];
    self.commentView.backgroundColor = UIColor.whiteColor;
    self.commentView.layer.cornerRadius = 8;
    [self.mainView addSubview:self.commentView];
    
    UILabel *commentTitleLabel = [UILabel new];
    commentTitleLabel.font = kFontMedium(14);
    commentTitleLabel.textColor = app_font_color;
    commentTitleLabel.frame = CGRectMake(12, 12, 200, 24);
    commentTitleLabel.text = @"补充描述和凭证";
    [self.commentView addSubview:commentTitleLabel];
    
    self.textView = [[XPLimitInputTextView alloc] initWithFrame:CGRectMake(12,48, self.commentView.mj_w - 24, 120)];
    [self.textView configWithMaxCount:200 textViewPlaceholder:@"请输入您的问题描述"];
    [self.commentView addSubview:self.textView];
    
    self.imageAddView = [[XPImageAddView alloc] initWithFrame:CGRectMake(24, MaxY(self.textView) + 12, self.view.mj_w - 48, 101)];
    self.imageAddView.type = 1;
    [self.commentView addSubview:self.imageAddView];
    
    UIButton *backTypeButton = [[UIButton alloc] initWithFrame:CGRectMake(12, MaxY(self.commentView) + 12, self.mainView.mj_w - 24, 56)];
    backTypeButton.backgroundColor = [UIColor whiteColor];
    backTypeButton.layer.cornerRadius = 8;
    [self.mainView addSubview:backTypeButton];
    
    UILabel *backTypeDescabel = [UILabel new];
    backTypeDescabel.font = kFontRegular(16);
    backTypeDescabel.textColor = app_font_color;
    backTypeDescabel.text = @"退款方式";
    backTypeDescabel.frame = CGRectMake(16, 0, 150, backTypeButton.mj_h);
    [backTypeButton addSubview:backTypeDescabel];

    UILabel *backTypeLabel = [UILabel new];
    backTypeLabel.font = kFontRegular(14);
    backTypeLabel.textColor = app_font_color;
    backTypeLabel.textAlignment = NSTextAlignmentRight;
    backTypeLabel.text = @"原支付方式返回";
    backTypeLabel.frame = CGRectMake(155 + 16, 0, backTypeButton.mj_w - 155 - 16 - 12, reasonView.mj_h);
    [backTypeButton addSubview:backTypeLabel];
    
    self.mainView.contentSize = CGSizeMake(self.mainView.mj_w, MaxY(backTypeButton) + 12);
    
    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,self.view.mj_h - 48 - kBottomHeight, self.view.mj_w, 48 + kBottomHeight);
    bottomView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:bottomView];
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton setTitle:@"提交申请" forState:UIControlStateNormal];
    [nextButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    
    nextButton.frame = CGRectMake(24,4,bottomView.mj_w - 48,40);
    nextButton.layer.cornerRadius = 20;
    nextButton.layer.masksToBounds = YES;
    [nextButton addTarget:self action:@selector(nextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:nextButton];
    [nextButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :nextButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    XPWeakSelf
    [self.imageAddView setChangeBlock:^(NSArray * _Nonnull imagesArray) {
        if (imagesArray.count == 0) {
            weakSelf.addPics = @"";
        }else {
            weakSelf.addPics = [imagesArray componentsJoinedByString:@","];
        }
    }];
    
}

- (void)topButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:nil
                                                                    message:nil
                                                             preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSArray *array = @[@"不想要了",@"买多了/买错了",@"质量问题",@"商品发货慢",@"商品发错货",@"商品与描述不符",@"与商家协商一致"];
    for (int i = 0; i< array.count; i++) {
        [alertC addAction:[UIAlertAction actionWithTitle:array[i]                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            self.typeLabel.text = array[i];
        }]];
    }
    [alertC addAction:[UIAlertAction actionWithTitle:@"取消"                                                               style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    
    [self presentViewController:alertC animated:YES completion:nil];
    
}

- (void)nextButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.typeLabel.text.length == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"请选择您的退款原因" ToView:k_keyWindow];
    }else if ([XPCommonTool isEmpty:self.textView.contentView.text]){
        [[XPShowTipsTool shareInstance] showMsg:@"请输入您的问题描述" ToView:k_keyWindow];
    }else {
       //提交
        [self postUserAdvice];
    }
}


//提交服务器反馈意见
- (void)postUserAdvice{
    
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"orderId"] = self.model.productId;
    paraM[@"reason"] = self.typeLabel.text;
    paraM[@"description"] = self.textView.contentView.text;
    paraM[@"type"] = [self getAfterType];
    if (self.addPics.length > 0) {
        paraM[@"pics"] = self.addPics;
    }
    
    
    NSString *url;
    if (self.model.status.intValue == 2) {//待发货
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderAfterSale_single];
//        paraM[@"orderItemId"] = [self getOrderItemId];
    }else {
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_mall_orderAfterSale];
        paraM[@"orderItemIds"] = [self getOrderItemId];
    }
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
           
            [[XPShowTipsTool shareInstance] showLongDuringMsg:@"提交成功!" ToView:k_keyWindow];
            [self.navigationController popViewControllerAnimated:YES];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (NSString *)getAfterType {
    NSString *typeStr = @"999";
    if (self.entryType == 1) {
        if (self.model.status.intValue == 2) {
            typeStr = @"1";//整单退
        }else {
            typeStr = @"2";//仅退款
        }
    }else if (self.entryType == 2){
        typeStr = @"3";//退款退货
    }
    return typeStr;
}

- (NSString *)getOrderItemId {
    NSMutableArray *tempArray = [NSMutableArray array];
    for (int i = 0; i< self.model.mallOrderItemListVOS.count ; i++) {
        NSString *idStr = self.model.mallOrderItemListVOS[i][@"id"];
        [tempArray addObject:idStr];
    }
    return [tempArray componentsJoinedByString:@","];
}

@end
