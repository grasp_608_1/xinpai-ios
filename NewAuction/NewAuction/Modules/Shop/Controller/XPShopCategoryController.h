//
//  XPShopCategoryController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/7.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCategoryController : XPBaseViewController

@property (nonatomic, copy) NSString *keywords;
@end

NS_ASSUME_NONNULL_END
