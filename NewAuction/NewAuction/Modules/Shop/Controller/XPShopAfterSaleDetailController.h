//
//  XPShopAfterSaleDetailController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/16.
//

#import "XPBaseViewController.h"
#import "XPShopAfterSaleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopAfterSaleDetailController : XPBaseViewController

@property (nonatomic, copy)   NSString *afterSaleId;

@property (nonatomic, copy) void(^refreshBlock)(void);



@end

NS_ASSUME_NONNULL_END
