//
//  XPShopingController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import "XPShopingController.h"
#import "XPShopCommendCollectionViewCell.h"
#import "XPFilterView.h"
#import "XPProductPopView.h"
#import "XPShopGoodsDetailController.h"
#import "XPShopModel.h"
#import "XPEmptyView.h"
#import "XPSearchDefaultView.h"
#import "XPSlideItemView.h"

#import "CHTCollectionViewWaterfallLayout.h"
#import "CHTCollectionViewWaterfallHeader.h"
#import "CHTCollectionViewWaterfallFooter.h"


#define XPSHOP_HEADER @"XPShopHeaderID"
#define XPSHOP_FOOTER @"XPShopFooterID"
#define XPSHOP_CELLID @"XPShopController"

@interface XPShopingController ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>

@property (nonatomic ,strong) XPFilterView *topView;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) XPProductPopView *popView;
//筛选位置
@property (nonatomic, assign) NSInteger tabIndex;
//分类
@property (nonatomic, strong) NSMutableArray *nameArray;
@property (nonatomic, strong) NSMutableArray *idArray;

@property (nonatomic, strong) XPEmptyView *emptyView;
//当前页码
@property (nonatomic, assign) NSInteger page;
//是否为升序 默认降序 由 优->一般  箭头朝下
@property (nonatomic, assign) BOOL isAsc;
//cell高度
@property (nonatomic, strong) NSArray *heightArray;

@property (nonatomic, strong) XPSlideItemView *selectItemView;

@end

@implementation XPShopingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.dataArray = [NSMutableArray array];
    self.page = 1;
    self.tabIndex = 1;
    if (self.keyStr == nil) {
        self.keyStr = @"";
    }
//    if (self.enterType != 0 || self.enterType != 1 || self.enterType != 6 || self.enterType != 7 ) {
//        self.categoryId = @"0";
//    }
    [self setupViews];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(headerRefresh) name:Notification_shopList_fresh object:nil];
    
    if (self.enterType == 2 ||self.enterType == 3 || self.enterType == 4 || self.enterType == 7) {
        self.categoryId = @"0";
        [self getCurrentListData];
    }
    
}

-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    XPSearchDefaultView *searchView = [[XPSearchDefaultView alloc] initWithFrame:CGRectMake(52, self.safeTop, SCREEN_WIDTH - 52 - 24, 40)];
    searchView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    searchView.searchIcon.image = [UIImage imageNamed:@"xp_search_black"];
    searchView.canSearch = YES;
    searchView.defaultSearchStr = self.keyStr;
    [self.navBarView addSubview:searchView];
    
    [self.view addSubview:self.navBarView];
    
    XPWeakSelf;
    [searchView setSearchBlock:^(NSString * _Nonnull keyStr) {
        [weakSelf.view endEditing:YES];
        
        weakSelf.dataArray = @[].mutableCopy;
        [weakSelf.collectionView reloadData];
        weakSelf.keyStr = keyStr;
        weakSelf.tabIndex = 0;
        weakSelf.page = 1;
        
        [weakSelf getCurrentListData];
        if (weakSelf.enterType == 0 || self.enterType == 2|| weakSelf.enterType == 7) {
            [weakSelf.topView resetUI];
        }
        
    }];
    
    [searchView setSearchStateBlock:^(BOOL state) {
        if (state) {
            weakSelf.collectionView.hidden = YES;
            weakSelf.topView.hidden = YES;
            weakSelf.emptyView.hidden = YES;
        }else{
            
            weakSelf.topView.hidden = NO;
            if (weakSelf.dataArray.count > 0) {
                weakSelf.collectionView.hidden = NO;
                weakSelf.emptyView.hidden = YES;
            }else {
                weakSelf.collectionView.hidden = YES;
                weakSelf.emptyView.hidden = NO;
            }
        }
    }];
    
}
-(void)setupViews{
    
    CHTCollectionViewWaterfallLayout *flowLayout = [[CHTCollectionViewWaterfallLayout alloc] init];
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 12, self.safeBottom, 12);
    if (self.enterType == 0  || self.enterType == 1||self.enterType == 2|| self.enterType == 5 || self.enterType == 6 || self.enterType == 7) {
        flowLayout.headerHeight = 12;
    }else{
        flowLayout.headerHeight = 48;
    }
    
    flowLayout.footerHeight = 0;
    flowLayout.minimumColumnSpacing = 12;
    flowLayout.minimumInteritemSpacing = 12;
    
    CGRect rect;
    if (self.enterType == 0 || self.enterType == 2 || self.enterType == 7) {
        self.topView = [[XPFilterView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.view.mj_w, 40)];
        self.topView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:self.topView];
        rect = CGRectMake(0, kTopHeight + 40, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    }else if (self.enterType == 1 || self.enterType == 5|| self.enterType == 6){
        self.selectItemView = [[XPSlideItemView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, 40)];
        self.selectItemView.backgroundColor = UIColor.whiteColor;
        [self.view addSubview:self.selectItemView];
        rect = CGRectMake(0, kTopHeight + 40, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    }else {
        rect = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight - 40);
    }
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
    [self.collectionView registerClass:[XPShopCommendCollectionViewCell class] forCellWithReuseIdentifier:XPSHOP_CELLID];
    [self.collectionView registerClass:[CHTCollectionViewWaterfallHeader class]
        forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader
               withReuseIdentifier:XPSHOP_HEADER];
    [self.collectionView registerClass:[CHTCollectionViewWaterfallFooter class]
        forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter
               withReuseIdentifier:XPSHOP_FOOTER];


    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = UIColorFromRGB(0xF8F8FF);
    self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:self.collectionView];
    
    XPWeakSelf
    self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.collectionView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_empty_favorite"] Description:@"未搜索到相应商品~"];
    [self.view addSubview:self.emptyView];
    
    XPProductPopView *popView = [[XPProductPopView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.view.mj_w, SCREEN_HEIGHT - kTopHeight)];
    self.popView = popView;
    [self.view addSubview:popView];
  
    [self.topView setTapBlock:^(NSInteger index,UIButton *sender) {
        
        if (index == 1 || index == 2) {
            [weakSelf adjustPriceButton];
            weakSelf.page = 1;
            weakSelf.isAsc = NO;
            if (weakSelf.tabIndex != index) {
                weakSelf.tabIndex = index;
                [weakSelf getCurrentListData];
            }
        }else if(index == 3){
            weakSelf.page = 1;
            
            weakSelf.tabIndex = index;
            if (sender.isSelected) {
                weakSelf.isAsc = YES;
            }else {
                weakSelf.isAsc = NO;
            }
            
            [weakSelf getCurrentListData];
        }else if (index == 4){
            [weakSelf.popView showToPoint:CGPointMake(weakSelf.view.mj_w - 6 - 120, 40) titleArray:weakSelf.nameArray];
        }
        
    }];
    
    [self.popView setSelectBlock:^(NSInteger index, NSString * _Nonnull content) {
        
        [weakSelf updateFliterButtonWithContent:content];
        [weakSelf.topView delayUpdateFliterUI];
        weakSelf.categoryId = weakSelf.idArray[index];
        weakSelf.page = 1;
        [weakSelf getCurrentListData];
    }];
    
    [self.selectItemView setTapBlock:^(NSInteger index) {
        weakSelf.categoryId = weakSelf.idArray[index -1];
        weakSelf.page = 1;
        [weakSelf getCurrentListData];
        
    }];
    
}
- (void)adjustPriceButton {
    UIButton *priceBtn = self.topView.buttonArray[2];
    priceBtn.selected = NO;
}

- (void)updateFliterButtonWithContent:(NSString *)content{
    UIButton *categroyBtn = self.topView.buttonArray[3];
    [categroyBtn setTitle:content forState:UIControlStateNormal];
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *height = self.heightArray[indexPath.row];
    return CGSizeMake((SCREEN_WIDTH - 36)/2., height.floatValue);
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    XPShopCommendCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:XPSHOP_CELLID forIndexPath:indexPath];
    XPShopModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    
    cell.backgroundColor = UIColor.whiteColor;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    XPShopModel *model = self.dataArray[indexPath.row];
    
    XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
    vc.hidesBottomBarWhenPushed = YES;
    vc.productSkuId = model.productSkuId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableView = nil;
    //返回段头段尾视图
    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
      reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                        withReuseIdentifier:XPSHOP_HEADER
                                                               forIndexPath:indexPath];
        
        CHTCollectionViewWaterfallHeader *header = (CHTCollectionViewWaterfallHeader *)reusableView;
//        header.titleImageView.image = [UIImage imageNamed:@"xp_product_comment"];
        if (self.enterType == 0 || self.enterType == 2|| self.enterType == 7) {
            header.titleLabel.text = @"";
        }else {
            header.titleLabel.text = self.noteLongTitle;
        }
    }
    else if ([kind isEqualToString:CHTCollectionElementKindSectionFooter]) {
      reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                        withReuseIdentifier:XPSHOP_FOOTER
                                                               forIndexPath:indexPath];
    }

    return reusableView;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.idArray.count == 0 && (self.enterType == 0 || self.enterType == 1 || self.enterType == 2|| self.enterType == 5 || self.enterType == 6 || self.enterType == 7) ) {
        [self getCategory];
    }
}

//分类数据
- (void)getCategory {
    NSString *url;
    if (self.enterType == 5 || self.enterType == 6) {
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_sort_one];
    }else{
        url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_shop_sortOne_list];
    }
    
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            
            NSInteger index = 0;
            if (self.enterType == 1 || self.enterType == 0 || self.enterType == 2|| self.enterType == 7) {
                [self getTitleArray:data.userData];
                [self.selectItemView configWithtitleArray:self.nameArray];
            }else if (self.enterType == 5){
                
                NSArray *childArray = data.userData;
                
                NSArray *twoArray = [NSArray array];
                
                for (int i = 0; i< childArray.count; i++) {
                    NSArray *childTwoArray = childArray[i][@"children"];
                    if (childTwoArray.count > 0) {
                        for (int j = 0; j< childTwoArray.count; j++) {
                            NSArray *threeArray = childTwoArray[j][@"children"];
                            if (threeArray.count == 0) {
                                continue;
                            }else {
                                for (int k = 0; k< threeArray.count; k++) {
                                    NSDictionary *threeDict = threeArray[k];
                                    if ([threeDict[@"id"] isEqualToString:self.categoryIdThree]) {
                                        index = k;
                                        twoArray = threeArray;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                
                [self getTitleArray:twoArray];
                [self.selectItemView configWithtitleArray:self.nameArray];
                if (self.idArray.count > 0) {
                    self.categoryId = self.idArray[index];
                    self.selectItemView.defaultSeletIndex = index;
                }
                
            }else if(self.enterType == 6) {
                
                NSArray *childArray = data.userData;
                
                NSArray *twoArray = [NSArray array];
                
                for (int i = 0; i< childArray.count; i++) {
                    NSArray *childTwoArray = childArray[i][@"children"];
                    if (childTwoArray.count > 0) {
                        for (int j = 0; j< childTwoArray.count; j++) {
                            NSDictionary *tempDict = childTwoArray[j];
                            if ([tempDict[@"id"] isEqualToString:self.categoryIdTwo] ) {
                                index = j;
                                twoArray = childTwoArray;
                                break;
                            }
                        }
                    }
                }
                
                [self getTitleArray:twoArray];
                [self.selectItemView configWithtitleArray:self.nameArray];
                
                if (self.idArray.count > 0) {
                    self.categoryId = self.idArray[index];
                    self.selectItemView.defaultSeletIndex = index;
                }
            }
            
            [self getCurrentListData];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}



- (void)getCurrentListData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_data_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSString stringWithFormat:@"%zd",self.page];
    paraM[@"pageSize"] = @(10);
    paraM[@"keywords"] = self.keyStr;
    paraM[@"pageType"] = @(20);
//    paraM[@"categoryId"] = self.categoryId;

    if (self.enterType == 1 || self.enterType == 2|| self.enterType == 7) {
        if (self.tabIndex == 1) {//综合
            paraM[@"sortType"] = @(1);
        }else if (self.tabIndex == 2){//销量
            paraM[@"sortType"] = @(2);
        }else if(self.tabIndex == 3){//价格
            paraM[@"sortType"] = self.isAsc ? @(3):@(4);
        }
        
        if (self.enterType == 7) {
            paraM[@"modelStatus"] = @(1);
        }
        
    }
    
    if (self.enterType == 0 || self.enterType == 1 || self.enterType == 2 ||self.enterType == 5|| self.enterType == 6 || self.enterType == 7) {
        paraM[@"categoryId"] = self.categoryId;
    }
    if (self.enterType == 2) {
        paraM[@"subjectId"] = self.subjectId;
    }
    if (self.enterType == 3) {
        paraM[@"couponId"] = self.couponId;
    }
    if (self.enterType == 4) {
        paraM[@"activityId"] = self.activityId;
    }
       
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            self.dataArray = [XPShopModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            if (self.dataArray.count > 0) {
                self.collectionView.hidden = NO;
                self.emptyView.hidden = YES;
            }else {
                self.collectionView.hidden = YES;
                self.emptyView.hidden = NO;
            }
            [self getCellHeightArray];
            [self.collectionView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

- (void)headerRefresh {
    self.page = 1;
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_data_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSString stringWithFormat:@"%zd",self.page];
    paraM[@"pageSize"] = @(10);
    paraM[@"keywords"] = self.keyStr;
    paraM[@"pageType"] = @(20);
    paraM[@"categoryId"] = self.categoryId;
    if (self.enterType == 3) {
        paraM[@"couponId"] = self.couponId;
    }
    if (self.enterType == 4) {
        paraM[@"activityId"] = self.activityId;
    }
    
    if (self.enterType == 2) {
        paraM[@"subjectId"] = self.subjectId;
    }
    
    if (self.enterType == 1 || self.enterType == 7) {
        if (self.tabIndex == 1) {//综合
            paraM[@"sortType"] = @(1);
        }else if (self.tabIndex == 2){//销量
            paraM[@"sortType"] = @(2);
        }else if(self.tabIndex == 3){//价格
            paraM[@"sortType"] = self.isAsc ? @(3):@(4);
        }
        
        if (self.enterType == 7) {
            paraM[@"modelStatus"] = @(1);
        }
    }
    
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.collectionView.mj_header endRefreshing];
        if (data.isSucess) {
            
            [self.dataArray removeAllObjects];
            self.dataArray = [XPShopModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            
            if (self.dataArray.count > 0) {
                self.collectionView.hidden = NO;
                self.emptyView.hidden = YES;
            }else {
                self.collectionView.hidden = YES;
                self.emptyView.hidden = NO;
            }
            [self getCellHeightArray];
            [self.collectionView reloadData];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

- (void)footerRefresh {
    
    self.page ++;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_data_list];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSString stringWithFormat:@"%zd",self.page];
    paraM[@"pageSize"] = @(10);
    paraM[@"keywords"] = self.keyStr;
    paraM[@"pageType"] = @(20);
    paraM[@"categoryId"] = self.categoryId;
    if (self.enterType == 3) {
        paraM[@"couponId"] = self.couponId;
    }
    if (self.enterType == 4) {
        paraM[@"activityId"] = self.activityId;
    }
    
    if (self.enterType == 2) {
        paraM[@"subjectId"] = self.subjectId;
    }
    
    if (self.enterType == 1|| self.enterType == 7) {
        if (self.tabIndex == 1) {//综合
            paraM[@"sortType"] = @(1);
        }else if (self.tabIndex == 2){//销量
            paraM[@"sortType"] = @(2);
        }else if(self.tabIndex == 3){//价格
            paraM[@"sortType"] = self.isAsc ? @(3):@(4);
        }
        if (self.enterType == 7) {
            paraM[@"modelStatus"] = @(1);
        }
    }
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.collectionView.mj_footer endRefreshing];
        if (data.isSucess) {
            
            NSArray *modelArray = [XPShopModel mj_objectArrayWithKeyValuesArray:data.userData[@"records"]];
            if (modelArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self getCellHeightArray];
                [self.collectionView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
     
}

//获取cell高度
- (void)getCellHeightArray{
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPShopCommendCollectionViewCell *cell = XPShopCommendCollectionViewCell.new;
    for (int i = 0; i<self.dataArray.count; i++) {
        XPShopModel *model = self.dataArray[i];
        CGFloat height = [cell cellWithModel:model];
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    
    self.heightArray = tempArray.copy;
}
- (void)getTitleArray:(NSArray *)array {
    self.nameArray = [NSMutableArray array];
    self.idArray = [NSMutableArray array];
    
    for (int i = 0; i<array.count; i++) {
        NSDictionary *dic = array[i];
        [self.nameArray addObject:dic[@"name"]];
        [self.idArray addObject:dic[@"id"]];
    }
    
    if (self.enterType == 0 || self.enterType == 1 || self.enterType == 2|| self.enterType == 7) {
        [self.nameArray insertObject:@"全部" atIndex:0];
        [self.idArray insertObject:@(0) atIndex:0];
    }
    
}
@end
