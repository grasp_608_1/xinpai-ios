//
//  XPShopingController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopingController : XPBaseViewController

@property (nonatomic, copy) NSString *keyStr;
//0 默认,搜索全部 1 一级分类下搜索 2 专题下搜索 3 优惠券 4 活动 
//5 二级分类下定位某个三级别分类 6 1级分类下定位某个二级别分类
// 7 造物商品
@property (nonatomic, assign) NSInteger enterType;
//专题id
@property (nonatomic, strong) NSNumber *subjectId;
//分类id
@property (nonatomic, copy) NSString *categoryId;
//长标签
@property (nonatomic, copy) NSString *noteLongTitle;
//优惠券
@property (nonatomic, strong) NSNumber *couponId;
//活动Id
@property (nonatomic, strong) NSNumber *activityId;
//二级分类ID
@property (nonatomic, copy) NSString *categoryIdTwo;
//当前三级分类
@property (nonatomic, copy) NSString *categoryIdThree;

@end

NS_ASSUME_NONNULL_END
