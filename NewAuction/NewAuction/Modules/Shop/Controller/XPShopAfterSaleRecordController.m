//
//  XPShopAfterSaleRecordController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/20.
//

#import "XPShopAfterSaleRecordController.h"
#import "XPEmptyView.h"
#import "XPShopProductAfterSaleCell.h"
#import "XPShopAfterSaleModel.h"
#import "XPShopAfterSaleDetailController.h"
#import "XPBaseWebViewViewController.h"
#import "XPAfterSaleMoneyPopView.h"

@interface XPShopAfterSaleRecordController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) XPEmptyView *emptyView;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
//cell高度
@property (nonatomic, strong) NSArray *heightArray;
//当前页码
@property (nonatomic, assign) NSInteger page;
//金额
@property (nonatomic, strong) XPAfterSaleMoneyPopView *popView;

@end

@implementation XPShopAfterSaleRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    self.dataArray = [NSMutableArray array];
    self.page = 1;
    [self setupUI];
    
    if (self.enterType == 0) {
        [self getData];
    }else {
        [self getSingleData];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}


- (void)setupUI {
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBottomHeight)];
    [self.view addSubview:self.tableView];
    
    CGFloat emptyW = self.view.bounds.size.width - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 195 - 20 + self.safeTop, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无售后记录"];
    [self.view addSubview:self.emptyView];
    
    
    if (self.enterType != 1) {
        XPWeakSelf
        self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
        self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            weakSelf.page = 1;
            [weakSelf headerRefresh];
        }];
    }
    
    
}


-(void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.titleLabel.text = @"售后记录";
    
    [self.navBarView.rightButton setImage:[UIImage imageNamed:@"xp_custom_service"] forState:UIControlStateNormal];
    [self.navBarView.rightButton addTarget:self action:@selector(topRightButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navBarView.rightButton.frame = CGRectMake(self.view.mj_w - 20 - 24, self.navBarView.mj_h - 36, 24, 24);
    
    [self.view addSubview:self.navBarView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPShopProductAfterSaleCell *cell = [XPShopProductAfterSaleCell cellWithTableView:tableView];
    XPShopAfterSaleModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    cell.backgroundColor = UIColorFromRGB(0xF2F2F2);
    XPWeakSelf
    cell.buttonBlock = ^(NSInteger tag) {
        [weakSelf buttonActionWithTag:tag model:model indexPath:indexPath];
    };
    
    return cell;
}

- (void)buttonActionWithTag:(NSInteger) tag model:(XPShopAfterSaleModel *)model indexPath:(NSIndexPath *)indexPath{
    
    NSInteger status = model.status.intValue;
    if (status == 3) {//拒绝
        if (tag == 0) {
            [self showSeverice];
        }else if(tag == 1){
            //再次申请
            [self reTryWithModel:model];
            
        }else if (tag == 2){
           //删除
            [self deleteAfterSaleWithModel:model indexPath:indexPath];
        }
        
    }else if (status == 6){//已完成
        if (tag == 0) {
            [self showSeverice];
        }else if(tag == 1){
            //钱款去向
            [self showPopWithModel:model];
        }else if (tag == 2){
            [self deleteAfterSaleWithModel:model indexPath:indexPath];
        }
    }else {
        if (tag == 0) {
            [self showSeverice];
        }
//        else if(tag == 1){
//            [self deleteAfterSaleWithModel:model];
//        }
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = self.heightArray[indexPath.row];
    return height.floatValue;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    XPShopAfterSaleDetailController *vc = [XPShopAfterSaleDetailController new];
    XPShopAfterSaleModel *model = self.dataArray[indexPath.row];
    vc.afterSaleId = [NSString stringWithFormat:@"%@",model.afterSaleId];
    XPWeakSelf
    vc.refreshBlock = ^{
        [weakSelf headerRefresh];
    };
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_order_afterSaleRecord];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(10);
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPShopAfterSaleModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            
            [self getCellHeightArray];
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)getSingleData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_order_afterSaleSingle];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] =  self.afterSaleId;

    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
    
            NSArray *modelArray = [XPShopAfterSaleModel mj_objectArrayWithKeyValuesArray:dataArray];
            
            self.dataArray = modelArray.mutableCopy;
            
            [self getCellHeightArray];
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)getCellHeightArray{
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPShopProductAfterSaleCell *cell = XPShopProductAfterSaleCell.new;
    for (int i = 0; i<self.dataArray.count; i++) {
        XPShopAfterSaleModel *model = self.dataArray[i];
        CGFloat height = [cell cellWithModel:model];
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    self.heightArray = tempArray.copy;
}


- (void)headerRefresh{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_order_afterSaleRecord];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(10);
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPShopAfterSaleModel mj_objectArrayWithKeyValuesArray:dataArray];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:modelArray];
            [self getCellHeightArray];
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_order_afterSaleRecord];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(10);
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData[@"records"];
            NSArray *modelArray = [XPShopAfterSaleModel mj_objectArrayWithKeyValuesArray:dataArray];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self getCellHeightArray];
                [self.tableView reloadData];
            }
            
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
    
}

- (void)canShowEmptyView {
    if (self.dataArray.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}

#pragma mark --cell button action
//客服
- (void)showSeverice{
    [self topRightButtonClicked:[UIButton new]];
}

//左上角客服
- (void)topRightButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
    vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,k_user_help];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
//删除订单
- (void)deleteAfterSaleWithModel:(XPShopAfterSaleModel *)model indexPath:(NSIndexPath *)indexPath{
    XPWeakSelf
    [self.xpAlertView configWithTitle:@"确定删除售后记录吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
        
    } rightAction:^(NSString * _Nullable extra) {
        [weakSelf sureToDeleteAfterSaleWithModel:model indexPath:indexPath];
    } alertType:XPAlertTypeNormal];
    
}
- (void)sureToDeleteAfterSaleWithModel:(XPShopAfterSaleModel *)model indexPath:(NSIndexPath *)indexPath{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_order_afterSaleRecord_delete];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = model.afterSaleId;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [self.dataArray removeObject:model];
            
            if (self.dataArray.count == 0) {
                [self.tableView reloadData];
            } else {
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
             }
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}


//重新申请
- (void)reTryWithModel:(XPShopAfterSaleModel *)model {
    XPWeakSelf
    [self.xpAlertView configWithTitle:@"确定重新发起售后申请吗?" desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
    } rightAction:^(NSString * _Nullable extra) {
        [weakSelf sureToReTryWithModel:model];
    } alertType:XPAlertTypeNormal];
    
}

- (void)sureToReTryWithModel:(XPShopAfterSaleModel *)model{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_order_afterSaleRecord_retry];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = model.afterSaleId;
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        if (data.isSucess) {
            [[XPShowTipsTool shareInstance] showMsg:@"申请成功!" ToView:self.view];
            [self refreshCellWithModel:model];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

- (void)refreshCellWithModel:(XPShopAfterSaleModel *)model{
    __block NSInteger index = 0;
    
    [self.dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        XPShopAfterSaleModel *tempModel = obj;
        if ([model.afterSaleId isEqual:tempModel.afterSaleId]) {
            index = idx;
            *stop = YES;
        }
    }];
    
    model.status = @(1);
    [self.tableView reloadData];
    
}

//钱款去向按钮点击
- (void)showPopWithModel:(XPShopAfterSaleModel *)model {
    
    XPAfterSaleMoneyPopView *popView = [[XPAfterSaleMoneyPopView alloc] initWithFrame:k_keyWindow.bounds];
    self.popView = popView;
    [k_keyWindow addSubview:popView];
    [popView viewWithModel:model];
}

@end
