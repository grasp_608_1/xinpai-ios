//
//  XPExtraContentView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPExtraContentView : UIView
//点击完成回调
@property (nonatomic, copy) void(^finishBlock)(NSString *content);
//取消回调
@property (nonatomic, copy) void(^cancelBlock)(void);
// can edit  default is no
@property (nonatomic, assign) BOOL canEdit;

@property (nonatomic, assign) BOOL canInputEmoji;

-(void)showWithContent:(NSString *)contentStr Title:(NSString *)titleStr;

@property (nonatomic, copy) NSString *inputPlaceHolder;

@property (nonatomic, assign) NSInteger maxWords;

@end

NS_ASSUME_NONNULL_END
