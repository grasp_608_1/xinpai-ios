//
//  XPAfterSaleInfoView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/17.
//

#import <UIKit/UIKit.h>
#import "XPShopAfterSaleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPAfterSaleInfoView : UIView

- (CGFloat)viewWithModel:(XPShopAfterSaleModel *)model;

@end

NS_ASSUME_NONNULL_END
