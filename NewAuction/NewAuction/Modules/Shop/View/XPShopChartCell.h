//
//  XPShopChartCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/18.
//

#import <UIKit/UIKit.h>
#import "XPShopModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopChartCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (void)cellWithModel:(XPShopModel *)model Index:(NSInteger)index;
@end

NS_ASSUME_NONNULL_END
