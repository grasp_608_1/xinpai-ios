//
//  XPOrderPayMoneyDetailPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/21.
//

#import "XPOrderPayMoneyDetailPopView.h"
#import "XPShopProductListView.h"

@interface XPOrderPayMoneyDetailPopView ()

@property (nonatomic, strong) XPShopProductListView *moneyListView;

@property (nonatomic, strong) XPShopProductListView *payTypeView;

@end

@implementation XPOrderPayMoneyDetailPopView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupViews];
    }
    return self;
}
- (void)setupViews {
    XPShopProductListView *moneyListView = [[XPShopProductListView alloc] initWithFrame:CGRectMake(0,0, self.contentView.mj_w, 0)];
    moneyListView.strokeValue = YES;
    self.moneyListView = moneyListView;
    [self.contentView addSubview:moneyListView];
    
    XPShopProductListView *payTypeView = [[XPShopProductListView alloc] initWithFrame:CGRectMake(0,0, self.contentView.mj_w, 0)];
    self.payTypeView = payTypeView;
    [self.contentView addSubview:payTypeView];
}

-(void)setModel:(XPShopOrderModel *)model {
    
    CGFloat top = 0;
    NSMutableArray *orderTitleDescArray = [NSMutableArray array];
    NSMutableArray *orderTitleArray = [NSMutableArray array];

    orderTitleDescArray = @[@"在线支付",@"余额支付",(model.status.intValue == 1 || model.status.intValue == 5) ?@"需付款": @"实付款"].mutableCopy;
    orderTitleArray = @[[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.payAmount]],
                        [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.accountAmount]],
                        [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.spendAllAmount]],].mutableCopy;
    
    if (model.status.intValue == 4 || model.status.intValue == 5 ||model.status.intValue == 6) {
        
        NSMutableArray *moneyTitleDescArray = [NSMutableArray array];
        NSMutableArray *moneyTitleArray = [NSMutableArray array];
        
        // moneyTitleDescArray= @[@"商品总价",@"运费",@"优惠券",@"活动",@"积分"].mutableCopy;
        moneyTitleDescArray= @[@"商品总价",@"运费",@"优惠券",@"活动"].mutableCopy;
        moneyTitleArray = @[[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.productSkuAmount]],
                            [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.freightAmount]],
                            [NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.couponAmount]],
                            [NSString stringWithFormat:@"减￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.activityAmount]],
                           // [NSString stringWithFormat:@"减￥%@",model.pointsAmount]
                           ].mutableCopy;
        
        CGFloat moneyH = [self.moneyListView viewWithKeyArray:moneyTitleDescArray ValueArray:moneyTitleArray];
        self.moneyListView.frame = CGRectMake(0,top, self.contentView.mj_w, moneyH);
        UILabel *label1 = self.moneyListView.rightArray[0];
        label1.textColor = app_font_color;
        label1.font = kFontRegular(14);
        
        UILabel *label2 = self.moneyListView.rightArray[1];
        label2.textColor = app_font_color;
        label2.font = kFontRegular(14);
        
        UILabel *label3 = self.moneyListView.rightArray[2];
        label3.textColor = UIColorFromRGB(0xFF4A4A);
        label3.font = kFontRegular(14);
        
        UILabel *label4 = self.moneyListView.rightArray[3];
        label4.textColor = UIColorFromRGB(0xFF4A4A);
        label4.font = kFontRegular(14);
        
    //    UILabel *label5 = self.moneyListView.rightArray[4];
    //    label5.textColor = UIColorFromRGB(0xFF4A4A);
    //    label5.font = kFontRegular(14);
        
        top = MaxY(self.moneyListView) + 12;
    }

    CGFloat typeH = [self.payTypeView viewWithKeyArray:orderTitleDescArray ValueArray:orderTitleArray];
    self.payTypeView.frame = CGRectMake(0,top, self.contentView.mj_w, typeH);
    UILabel *label11 = self.payTypeView.rightArray[0];
    label11.textColor = app_font_color;
    label11.font = kFontMedium(14);
    
    UILabel *label22 = self.payTypeView.rightArray[1];
    label22.textColor = app_font_color;
    label22.font = kFontMedium(14);
    
    UILabel *label33 = self.payTypeView.rightArray[2];
    label33.textColor = app_font_color;
    label33.font = kFontMedium(18);
    
    NSString *truePayStr = orderTitleArray.lastObject;
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:truePayStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(12)} range:NSMakeRange(0, 1)];
    label33.attributedText = attr;
    
    
    [self.payTypeView showTopLineAtIndex:2];
    
    self.contentView.contentSize = CGSizeMake(self.contentView.mj_w, MaxY(self.payTypeView));
    [self showPopWithTitle:@"金额明细" bottomTitle:@"确定"];
}

@end
