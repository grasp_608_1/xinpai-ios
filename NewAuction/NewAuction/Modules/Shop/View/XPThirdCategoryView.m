//
//  XPThirdCategoryView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/7.
//

#import "XPThirdCategoryView.h"

@interface XPThirdCategoryView ()

@property (nonatomic, copy) NSArray *dataArray;

@end

@implementation XPThirdCategoryView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
}

- (CGFloat)configWithtitle:(NSString *)titleStr dataArray:(NSArray *)dataArray{
    self.dataArray = dataArray;
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(16);
    titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel.text = titleStr;
    titleLabel.frame = CGRectMake(12 , 12, 200, 24);
    [self addSubview:titleLabel];
    CGFloat itemW = (self.mj_w - 21*2 - 12*2)/3;
    CGFloat itemH =  90*itemW/64.;
    
    CGFloat TopMargin = 44;
    CGFloat maxH = MaxY(titleLabel);
    
    for (int idx = 0; idx< dataArray.count; idx++) {
        NSDictionary *dic = dataArray[idx];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(12 +(itemW + 21) * (idx%3), TopMargin +  (itemH + 12)*(idx/3), itemW, itemH);
        [self addSubview:button];
        
        UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, itemW, itemW)];
        imageV.layer.cornerRadius = 8;
        imageV.layer.masksToBounds = YES;
        [button addSubview:imageV];
        
        UILabel *titleLabel = [UILabel new];
        titleLabel.font = kFontRegular(12);
        titleLabel.textColor = UIColorFromRGB(0x8A8A8A);
        titleLabel.frame = CGRectMake(0,MaxY(imageV), itemW, itemH - itemW);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [button addSubview:titleLabel];
        button.tag = idx;
        [button addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        maxH = MaxY(button);
        
        titleLabel.text = dic[@"name"];
        [imageV sd_setImageWithURL:[NSURL URLWithString:dic[@"pic"]]];
    }
    return maxH + 12;
}

-(void)itemButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    NSDictionary *dic = self.dataArray[sender.tag];
    if (dic &&self.itemSelectBlock) {
        self.itemSelectBlock(dic);
    }
}
@end
