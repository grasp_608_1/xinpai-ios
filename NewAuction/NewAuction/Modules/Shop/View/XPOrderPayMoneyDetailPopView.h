//
//  XPOrderPayMoneyDetailPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/21.
//

#import "XPDefaultContentPopView.h"
#import "XPShopOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPOrderPayMoneyDetailPopView : XPDefaultContentPopView

@property (nonatomic, strong) XPShopOrderModel *model;


@end

NS_ASSUME_NONNULL_END
