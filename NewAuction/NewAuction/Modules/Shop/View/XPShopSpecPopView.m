//
//  XPShopSpecPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/10.
//

#import "XPShopSpecPopView.h"
#import "XPHitButton.h"
#import "XPNoMenuTextField.h"

@interface XPShopSpecPopView ()

@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView *mainView;
//产品图片
@property (nonatomic, strong) UIImageView *pImageView;
//产品名称
@property (nonatomic, strong) UILabel *pTitleLabel;
//计算购买数量
@property (nonatomic, strong) UIView *countView;
@property (nonatomic, strong) UIButton *mButton;//减法
@property (nonatomic, strong) UIButton *addButton;//加法
@property (nonatomic, strong) UIButton *allButton;//全部
@property (nonatomic, strong) XPNoMenuTextField *numberTextField;//当前数量
//当前数量
@property (nonatomic, assign) NSInteger count;
//单个价格(销售价)
@property (nonatomic, assign) double price;
//提货价
@property (nonatomic, assign) double priceCost;
//最大数量
@property (nonatomic, assign) NSInteger max;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) NSMutableArray <UIView *>*sortViewArray;
//分类一
@property (nonatomic, assign) NSInteger currentIndex;
//全属性模型数组
@property (nonatomic, copy) NSArray *specTotalArray;
//选中属性
@property (nonatomic, copy) NSString *specStr;
@property (nonatomic, strong) NSNumber *productSkuId;

@property (nonatomic, strong) XPShopProductDetailModel *totalModel;
//当前规格model
@property (nonatomic, strong) XPShopSpecModel *currentSpecModel;
//确定按钮
@property (nonatomic, strong) UIButton *sureButton;
//加入购物车
@property (nonatomic, strong) UIButton *leftButton;
//立即购买
@property (nonatomic, strong) UIButton *rightButton;
//是否是积分商品
@property (nonatomic, assign) BOOL priceStatus;

@end

@implementation XPShopSpecPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    self.backgroundColor = UIColor.clearColor;
    self.count = 1;
    self.currentIndex = -1;
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    bgView.alpha = 0;
    self.bgView = bgView;
    [self addSubview:bgView];
    
    self.mainView = [UIView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h, SCREEN_WIDTH, 0.75 *self.mj_h);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(24, 24, 80, 80);
//    self.pImageView.layer.cornerRadius = 4;
//    self.pImageView.layer.masksToBounds = YES;
    [self.mainView addSubview:_pImageView];
    
    CGFloat LabelX = MaxX(self.pImageView) + 12;
    CGFloat LabelW = self.mainView.mj_w - LabelX  - 50;
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontMedium(14);
    self.pTitleLabel.textColor = app_font_color;
    self.pTitleLabel.frame = CGRectMake(LabelX, 28, LabelW, 32);
    [self.mainView addSubview:self.pTitleLabel];

    UIView *countView = [UIView new];
    countView.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel) + 22, 200, 24);
    self.countView = countView;
    [_mainView addSubview:countView];
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.backgroundColor = [UIColor whiteColor];
    [button1 addTarget:self action:@selector(mButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button1 setImage:[UIImage imageNamed:@"Group_99"] forState:UIControlStateNormal];
    button1.frame = CGRectMake(0, 0, 24, 24);
    self.mButton = button1;
    [countView addSubview:button1];
    
    self.numberTextField = [XPNoMenuTextField new];
    self.numberTextField.textColor = UIColorFromRGB(0x3A3C41);
    self.numberTextField.font = kFontMedium(16);
    self.numberTextField.frame = CGRectMake(MaxX(button1) + 4, 0, 28, 24);
    self.numberTextField.text = @"1";
    self.numberTextField.userInteractionEnabled = NO;
    self.numberTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.numberTextField.textAlignment = NSTextAlignmentCenter;
    self.numberTextField.layer.cornerRadius = 4;
    self.numberTextField.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [countView addSubview:self.numberTextField];
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.backgroundColor = [UIColor whiteColor];
    [button2 addTarget:self action:@selector(aButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button2 setImage:[UIImage imageNamed:@"Group_100"] forState:UIControlStateNormal];
    button2.frame = CGRectMake(MaxX(self.numberTextField) + 3, 0, 24, 24);
    self.addButton = button2;
    [countView addSubview:button2];
    
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.frame = CGRectMake(0, 128, self.mainView.mj_w, self.mainView.mj_h - 128 - 48 - 24);
    [self.mainView addSubview:self.scrollView];
    
    CGFloat bottomW = (self.mainView.mj_w - 24)/2.;
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.backgroundColor = RGBACOLOR(110, 107, 255, 0.15);
    leftButton.frame = CGRectMake(12, self.mainView.mj_h - 64, bottomW, 40);
    [XPCommonTool cornerRadius:leftButton andType:6 radius:20];
    [leftButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    leftButton.titleLabel.font = kFontMedium(14);
    [leftButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setTitle:@"加入购物车" forState:UIControlStateNormal];
    leftButton.tag = 0;
    self.leftButton = leftButton;
    [self.mainView addSubview:leftButton];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
    rightButton.frame = CGRectMake(self.mainView.mj_w/2., self.mainView.mj_h - 64, bottomW, 40);
    [XPCommonTool cornerRadius:rightButton andType:4 radius:20];
    [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    rightButton.titleLabel.font = kFontMedium(14);
    [rightButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setTitle:@"立即购买" forState:UIControlStateNormal];
    rightButton.tag = 1;
    self.rightButton = rightButton;
    [self.mainView addSubview:rightButton];
    
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sureButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
    sureButton.frame = CGRectMake(24, self.mainView.mj_h - 64, self.mainView.mj_w - 48, 40);
    sureButton.layer.cornerRadius = 20;
    [sureButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    sureButton.titleLabel.font = kFontMedium(14);
    [sureButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [sureButton setTitle:@"更改规格" forState:UIControlStateNormal];
    sureButton.tag = 2;
    self.sureButton = sureButton;
    [self.mainView addSubview:sureButton];
    
}


- (void)showWithModel:(XPShopProductDetailModel *)model ViewType:(XPProductSpecPopType)type {
    self.totalModel = model;
    self.max = 200;
    if (type == XPProductSpecPopTypeNormal) {
        self.sureButton.hidden = YES;
        
    }else {
        self.leftButton.hidden = YES;
        self.rightButton.hidden = YES;
        
        if (type == XPProductSpecPopTypeChange) {
            [self.sureButton setTitle:@"更改规格" forState:UIControlStateNormal];
        }else if (type == XPProductSpecPopTypeSure){
            [self.sureButton setTitle:@"确定" forState:UIControlStateNormal];
        }
        for (int i = 0; i< model.productSkuShortInfoVOs.count; i++) {
            NSDictionary *dict = model.productSkuShortInfoVOs[i];
            if ([dict[@"productSkuId"] isEqual:model.productSkuId]) {
                model.productSkuShortInfoVO = dict;
                break;
            }
        }
    }

    
    self.specStr = model.productSkuShortInfoVO[@"attributeValues"];
    self.productSkuId = model.productSkuId;
    NSDictionary *productSkuShortInfoVO = model.productSkuShortInfoVO;
    if (productSkuShortInfoVO[@"pointStatus"] && [productSkuShortInfoVO[@"pointStatus"] intValue] == 1) {
        self.priceStatus = YES;
    }else{
        self.priceStatus = NO;
    }
    
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    self.specTotalArray = [XPShopSpecModel mj_objectArrayWithKeyValuesArray:model.productSkuShortInfoVOs];
    
    CGFloat maxY = 0;
    self.sortViewArray = [NSMutableArray array];
    
    NSData *jsonData = [model.attributeMergeJson dataUsingEncoding:NSUTF8StringEncoding];
            
    NSError *error;
    // 使用 NSJSONSerialization 将 NSData 对象转换为 NSDictionary 对象
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&error];
            
    // 检查是否出现错误
    if (!error) {
              
        NSArray *tempArray = jsonDict[@"attributeList"];
        for (int i = 0; i< tempArray.count; i++) {
            UIView *sortView = [[UIView alloc] initWithFrame:CGRectMake(12,maxY, self.scrollView.mj_w - 24, 0)];
            sortView.tag = i;
            [self.scrollView addSubview:sortView];
            [self.sortViewArray addObject:sortView];
            
            NSDictionary *tempDic = tempArray[i];
            
            CGFloat sortViewH = [self createButtonsWithTitleArray:tempDic[@"attributeValue"] defaultTag:0 ByView:sortView TopTitle:tempDic[@"attributeKey"]];
            sortView.frame = CGRectMake(12, maxY, self.scrollView.mj_w - 24,sortViewH);
            maxY = MaxY(sortView) + 12;
        }
        
        //设置默认选择的规格
        [self setDefaultSelectSpec];

        self.scrollView.contentSize = CGSizeMake(self.scrollView.mj_w, maxY+ 42);
        [UIView animateWithDuration:0.5 animations:^{
            self.bgView.alpha = 1;
            self.mainView.frame = CGRectMake(0, self.mj_h *0.25, SCREEN_WIDTH, 0.75 *self.mj_h);
        }];

        
   }
}

-(CGFloat)createButtonsWithTitleArray:(NSArray *)titleArray defaultTag:(NSInteger)tag ByView:(UIView *)view TopTitle:(NSString *)topTitleStr{
    
    CGFloat contentW = view.mj_w;
    CGFloat marginTop = 10;
    CGFloat marginLeft = 8;
    CGFloat currentX = 0;
    CGFloat currentY = 0;
    CGFloat btnH = 28;
    CGFloat bottomY = 0;
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(16);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(0, 0, 100, 24);
    titleLabel.text = topTitleStr;
    [view addSubview:titleLabel];
    
    currentY = MaxY(titleLabel) + 14;
    
    for (int i = 0; i<titleArray.count; i++) {
//        NSString *name = [[titleArray objectAtIndex:i] objectForKey:@"name"];
        NSString *name = titleArray[i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i + tag;
        button.layer.cornerRadius = 2;
        button.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [button setTitle:name forState:UIControlStateNormal];
        [button setTitleColor:app_font_color forState:UIControlStateNormal];
        button.titleLabel.font = kFontRegular(14);
        button.tag = i + tag;
        [button addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        CGFloat stringW = [name sizeWithAttributes:@{NSFontAttributeName:button.titleLabel.font}].width;
        
        CGFloat btnW = stringW + 16;
        
        //一个规格一行的情况
        if (btnW >=  contentW) {
            currentY = currentY + btnH + marginTop;
            button.frame = CGRectMake(0, currentY, contentW, btnH);
            
        }else{//正常情况
            
            if (currentX + btnW + marginLeft > contentW) {//换行
                currentX = 0;
                currentY = currentY + btnH + marginTop;
                button.frame = CGRectMake(currentX, currentY,btnW, btnH);
                currentX = MaxX(button);
            }else{
                CGFloat left = 0;
                if (currentX != 0) {
                    left = currentX + marginLeft;
                }
                button.frame = CGRectMake(left, currentY,btnW, btnH);
                currentX = MaxX(button);
            }
            
           
        }
        [view addSubview:button];
        
        if (i == titleArray.count) {
            bottomY = MaxY(button);
        }
        
    }
    return currentY + btnH;
}

- (void)itemButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    

    UIView *sortView = self.sortViewArray[sender.superview.tag];

    [self selectSpecWithButton:sender];
    
        for (UIView *view in sortView.subviews) {
            if ([view isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)view;
                if (btn.tag != sender.tag) {
                    [btn setTitleColor:app_font_color forState:UIControlStateNormal];
                    btn.backgroundColor = UIColorFromRGB(0xF2F2F2);
                }else {
                    [btn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
                    btn.backgroundColor = UIColorFromRGB(0x6E6BFF);
                }
                
            }
    }
    
}

- (void)selectSpecWithButton:(UIButton *)sender {
    NSInteger index = sender.superview.tag;
    
    NSMutableArray *array = [NSMutableArray array];
    [array addObjectsFromArray:[self.specStr componentsSeparatedByString:@","]];
    [array replaceObjectAtIndex:index withObject:sender.titleLabel.text];
    
    self.specStr = [array componentsJoinedByString:@","];
    [self selectSpecStringReloadUI];
   
}

- (void)selectSpecStringReloadUI {

    for (int i = 0; i<self.specTotalArray.count; i++) {
        XPShopSpecModel *model = self.specTotalArray[i];
        if ([self.specStr isEqualToString:model.attributeValues]) {
            self.currentSpecModel = model;
            [self.pImageView sd_setImageWithURL:[NSURL URLWithString:model.pic]];
            if (self.priceStatus) {
                self.price =  model.pricePoint.doubleValue * 1000;
            }else {
                self.price =  model.priceSale.doubleValue * 1000;
            }
            self.priceCost = model.priceCost.doubleValue * 1000;
            [self showTotalPrice];
            break;
        }
    }
}

- (void)setDefaultSelectSpec {
    
    NSArray *titleArray = [self.specStr componentsSeparatedByString:@","];
    for (int i= 0 ; i< titleArray.count; i++) {
        NSString *titleStr = titleArray[i];
        UIView *sortView = self.sortViewArray[i];
        
        for (UIView *view in sortView.subviews) {
            if ([view isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)view;
                if ([btn.titleLabel.text isEqualToString:titleStr]) {
                    [btn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
                    btn.backgroundColor = UIColorFromRGB(0x6E6BFF);
                }
            }
        }
    }
    
    [self selectSpecStringReloadUI];
}


- (void)bottomButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);

    [self reloadUI];
    if (self.bottomBlock) {
        self.bottomBlock(sender.tag,self.count,self.currentSpecModel);
    }
    [self removeFromSuperview];
}

- (void)cancelButtonClicked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    [self reloadUI];
    if (self.cancelBlock) {
        self.cancelBlock(self.currentSpecModel);
    }
    [self removeFromSuperview];
}

//更新已选属性
- (void)reloadUI{
    NSMutableDictionary *tempDic = [NSMutableDictionary dictionary];
    [tempDic addEntriesFromDictionary:self.totalModel.productSkuShortInfoVO];
    [tempDic setValue:self.specStr forKey:@"attributeValues"];
    [tempDic setValue:[self getProductId] forKey:@"productSkuId"];
    
    self.totalModel.productSkuShortInfoVO = tempDic.copy;
}

- (NSNumber *)getProductId {
    
    NSNumber *tempNum = @(0);
    for (int i = 0; i < self.specTotalArray.count; i++) {
        XPShopSpecModel *model = self.specTotalArray[i];
        if ([model.attributeValues isEqualToString:self.specStr]) {
            tempNum = model.productSkuId;
        }
    }
    return tempNum;
}

- (void)mButtonClicked:(UIButton *)sender {
    
    if (self.count <= 0) {
        self.numberTextField.text = @"0";
        self.count = 0;
    }else {
        self.count --;
        
        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.count];
    }
//    [self showTotalPrice];
    [self layoutCountView];
}
////当前最大可参拍数量为0
- (void)aButtonClicked:(UIButton *)sender {
    
    if (self.max == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"当前最大数量为0" ToView:k_keyWindow];
    }
    
    if (self.count >= self.max) {
        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.max];
        self.count = self.max;
    }else {
        self.count ++;
        
        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.count];
    }
//    [self showTotalPrice];
    [self layoutCountView];
}

- (void)layoutCountView {
    CGFloat numW = [self.numberTextField.text sizeWithAttributes:@{NSFontAttributeName:self.numberTextField.font}].width;
    if (numW <10) {
        numW = 10;
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.numberTextField.frame = CGRectMake(MaxX(self.mButton) + 4, 0, 20 + numW, 24);
        self.addButton.frame = CGRectMake(MaxX(self.numberTextField) + 4, 0, 24, 24);
        self.allButton.frame = CGRectMake(MaxX(self.addButton) + 17, 0, 30, 24);
    }];
}

- (void)showTotalPrice {
    
    double totalMoneyf = self.price * self.count;
    double totalCostf = self.priceCost *self.count;
    
    if (self.priceStatus) {
        NSString *totalMoneyStr = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithDouble:totalMoneyf/1000.]]];
    
        NSString *totalMoney = [NSString stringWithFormat:@"%@ 积分",totalMoneyStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalMoney];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(18),NSForegroundColorAttributeName:app_font_color_FF4A4A} range:NSMakeRange(0, totalMoney.length - 2)];
        
        self.pTitleLabel.attributedText = attr;
    }else {
        NSString *totalMoneyStr = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithDouble:totalMoneyf/1000.]]];
        NSString *totalCostMoneyStr = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithDouble:totalCostf/1000.]]];
        NSString *totalMoney = [NSString stringWithFormat:@"￥%@  提货价: ￥%@",totalMoneyStr,totalCostMoneyStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalMoney];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(28),NSForegroundColorAttributeName:app_font_color_FF4A4A} range:NSMakeRange(1, totalMoneyStr.length)];
        
        self.pTitleLabel.attributedText = attr;
    }
    

}

-(void)hiddenCountView:(BOOL)isHidden {
    self.countView.hidden = isHidden;
}

@end
