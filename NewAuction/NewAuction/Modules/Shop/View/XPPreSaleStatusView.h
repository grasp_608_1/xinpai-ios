//
//  XPPreSaleStatusView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPPreSaleStatusView : UIView

-(void)viewWithDetailDic:(NSDictionary *)dic;//详情页

-(void)viewWithDic:(NSDictionary *)dic;//列表

@end

NS_ASSUME_NONNULL_END
