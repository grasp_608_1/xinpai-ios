//
//  XPStarView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPStarView : UIView

@property (nonatomic, assign) NSInteger star;

@property (nonatomic, assign) BOOL canOption;

@property (nonatomic, copy) void(^changeBlock)(NSInteger count);

@end

NS_ASSUME_NONNULL_END
