//
//  XPShopItemView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopItemView : UIView
//数据源
@property (nonatomic, copy) NSArray *dataArray;
//点击回调
@property (nonatomic, copy) void(^itemSelectBlock)(NSDictionary *dic);

@end

NS_ASSUME_NONNULL_END
