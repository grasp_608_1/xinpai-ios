//
//  XPShopProductCommentCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/11.
//

#import "XPShopProductCommentCell.h"
#import "XPHitButton.h"
#import "XPStarView.h"

@interface XPShopProductCommentCell ()

@property (nonatomic, strong) UIView *mainView;
//用户头像
@property (nonatomic, strong) UIImageView  *iconImageView;
//用户昵称
@property (nonatomic, strong) UILabel *nameLabel;
//评价时间
@property (nonatomic, strong) UILabel *timeLabel;
//用户评价星星
@property (nonatomic, strong) XPStarView *starView;
//评语
@property (nonatomic, strong) UILabel *contenLabel;
//评价图片
@property (nonatomic, strong) UIView  *commentImageView;

@end

@implementation XPShopProductCommentCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"XPShopProductCommentCell";
    XPShopProductCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPShopProductCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorFromRGB(0xF2F2F2);
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12*2, 0);
    mainView.clipsToBounds = YES;
    mainView.layer.cornerRadius = 8;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.frame = CGRectMake(12, 12, 30, 30);
    self.iconImageView.layer.cornerRadius = 15;
    self.iconImageView.layer.masksToBounds = YES;
    [self.mainView addSubview:self.iconImageView];
    
    UILabel *nameLabel = [UILabel new];
    nameLabel.font = kFontRegular(14);
    nameLabel.textColor = app_font_color;
    nameLabel.frame = CGRectMake(50, 12,250, 16);
    self.nameLabel = nameLabel;
    [self.mainView addSubview:nameLabel];
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = kFontRegular(10);
    timeLabel.textColor = UIColorFromRGB(0xB8B8B8);
    timeLabel.frame = CGRectMake(50, 28,200, 14);
    self.timeLabel = timeLabel;
    [self.mainView addSubview:timeLabel];
    
    self.starView = [[XPStarView alloc] initWithFrame:CGRectMake(self.mainView.mj_w - 12 - 95, 20, 95, 16)];
    [self.mainView addSubview:self.starView];
    
    
    UILabel *contenLabel = [UILabel new];
    contenLabel.font = kFontRegular(14);
    contenLabel.textColor = app_font_color;
    contenLabel.frame = CGRectMake(12,54,self.mainView.mj_w - 24, 0);
    contenLabel.numberOfLines = NO;
    self.contenLabel = contenLabel;
    [self.mainView addSubview:contenLabel];
    
    self.commentImageView = [[UIView alloc] init];
    self.commentImageView.frame = CGRectMake(12,MaxY(self.contenLabel) + 12,self.mainView.mj_w - 24, 0);
    [self.mainView addSubview:self.commentImageView];
    
}
-(void)setCellType:(NSInteger)cellType {
    _cellType = cellType;
}

-(CGFloat)cellWithModel:(XPShopProductCommentModel *)model{
    
    if(self.cellType == 0){
        self.nameLabel.text = model.userNickname;
        self.timeLabel.text = model.createTime;
        self.starView.star = model.star.integerValue;
        self.contenLabel.text = model.content;
        [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.userHead] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
        
        CGFloat contentH = [self.contenLabel.text boundingRectWithSize:CGSizeMake(self.contenLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.contenLabel.font} context:nil].size.height;
        self.contenLabel.frame = CGRectMake(12,54,self.mainView.mj_w - 24, contentH);
    }else if (self.cellType == 1){
        
        self.starView.hidden = YES;
        self.contenLabel.text = model.content;
        
        NSArray *array = @[@"意见与建议",@"功能异常",@"系统卡顿",@"界面错位",@"其他"];
        self.nameLabel.text = (model.type.intValue >= array.count )?@"其他":array[model.type.intValue];
        self.nameLabel.frame = CGRectMake(12, 16,250, 24);
        self.nameLabel.font = kFontMedium(16);
        
        self.timeLabel.text = model.createTime;
        self.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
        self.timeLabel.frame = CGRectMake(self.mainView.mj_w - 12 - 250, 16, 250, 24);
        self.timeLabel.textAlignment = NSTextAlignmentRight;
        
        CGFloat contentH = [self.contenLabel.text boundingRectWithSize:CGSizeMake(self.contenLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.contenLabel.font} context:nil].size.height;
        self.contenLabel.frame = CGRectMake(12,56,self.mainView.mj_w - 24, contentH);
    }else if (self.cellType == 2){
        
        self.starView.hidden = YES;
        
        self.contenLabel.text = model.content;
        
        self.timeLabel.text = model.createTime;
        self.timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
        CGFloat timeW = [self.timeLabel.text sizeWithAttributes:@{NSFontAttributeName:self.timeLabel.font}].width;
        self.timeLabel.frame = CGRectMake(self.mainView.mj_w - 12 - timeW, 16, timeW, 24);
        self.timeLabel.textAlignment = NSTextAlignmentRight;
        
        self.nameLabel.text = model.title;
        self.nameLabel.font = kFontMedium(16);
        self.nameLabel.frame = CGRectMake(12, 16, self.mainView.mj_w - 24 - timeW - 5, 24);
        
        CGFloat contentH = [self.contenLabel.text boundingRectWithSize:CGSizeMake(self.contenLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.contenLabel.font} context:nil].size.height;
        self.contenLabel.frame = CGRectMake(12,56,self.mainView.mj_w - 24, contentH);
    }
    
    [self.commentImageView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSArray *array = [model.pics componentsSeparatedByString:@","];
    if (model.pics.length >0 && array.count > 0) {
        
        CGFloat margin = 12;
        CGFloat imageWH = (self.mainView.mj_w - 12 *(array.count + 1))/3;
        
        for (int i = 0; i<array.count; i++) {
            UIImageView *imageView = [UIImageView new];
            [imageView sd_setImageWithURL:[NSURL URLWithString:array[i]]];
            imageView.frame = CGRectMake(i* (imageWH + margin), 0, imageWH, imageWH);
            imageView.layer.cornerRadius = 8;
            imageView.layer.masksToBounds = YES;
            imageView.contentMode =  UIViewContentModeScaleAspectFit;
            [self.commentImageView addSubview:imageView];
        }
        self.commentImageView.frame = CGRectMake(12,MaxY(self.contenLabel) + 12,self.mainView.mj_w - 24, imageWH);
        self.mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12*2, MaxY(self.commentImageView) + 12);
    }else{
        
        self.commentImageView.frame = CGRectMake(12,MaxY(self.contenLabel) + 12,self.mainView.mj_w - 24, 0);
        self.mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12*2, MaxY(self.contenLabel) + 20 );
        
    }
    
    return MaxY(self.mainView) + 12;
}


@end
