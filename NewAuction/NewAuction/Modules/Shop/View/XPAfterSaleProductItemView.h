//
//  XPAfterSaleProductView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/16.
//

#import <UIKit/UIKit.h>
#import "XPShopOrderModel.h"
#import "XPShopOrderProductListModel.h"
#import "XPHitButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPAfterSaleProductItemView : UIView
//选择按钮
@property (nonatomic, strong) XPHitButton *selectedButton;

@property (nonatomic, strong) XPShopOrderProductListModel *model;
//是否显示商品状态
@property (nonatomic, assign) bool showStatus;
//1 显示商品状态
@property (nonatomic, assign) NSInteger status;
//是否可选择
@property (nonatomic, assign) bool canSelect;

@property (nonatomic, copy) void(^selectBlock)(XPShopOrderProductListModel *model,bool select);
@end

NS_ASSUME_NONNULL_END
