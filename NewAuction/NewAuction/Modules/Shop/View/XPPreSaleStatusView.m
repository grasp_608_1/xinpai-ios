//
//  XPPreSaleStatusView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/16.
//

#import "XPPreSaleStatusView.h"
#import "XPShopPreSaleModel.h"

@interface  XPPreSaleStatusView()

@property (nonatomic, strong) UIImageView *topImageView;
@property (nonatomic, strong) UILabel *topLabel;

@property (nonatomic, strong) UIImageView *downImageView;
@property (nonatomic, strong) UILabel *downLabel;
@end

@implementation XPPreSaleStatusView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.backgroundColor = RGBACOLOR(110, 107, 255, 0.03);
    
    self.topImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 12, 9, 17)];
    [self addSubview:self.topImageView];
    
    self.topLabel = [UILabel new];
    self.topLabel.font = kFontRegular(14);
    self.topLabel.textColor = app_font_color_8A;
    self.topLabel.frame = CGRectMake(25, 4, self.mj_w - 25, 24);
    [self addSubview:self.topLabel];

    self.downImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 32, 9, 17)];
    [self addSubview:self.downImageView];
    
    self.downLabel = [UILabel new];
    self.downLabel.font = kFontRegular(14);
    self.downLabel.textColor = app_font_color_8A;
    self.downLabel.frame = CGRectMake(25, 32, self.mj_w - 25, 24);
    [self addSubview:self.downLabel];
    
    self.topImageView.image = [UIImage imageNamed:@"group_gray_down"];
    self.downImageView.image = [UIImage imageNamed:@"group_gray_up"];
    
}

-(void)viewWithDetailDic:(NSDictionary *)dic {
    
    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    
    XPShopPreSaleModel *model = [XPShopPreSaleModel mj_objectWithKeyValues:dic];
    //预售-支付状态 0 未支付 1 已付定金未支付尾款(到达尾款支付时间) 2 已付尾款 10 已付定金未支付尾款(未到达尾款支付时间)
    long long nowStamp = [self getLocalNowStampStr].longLongValue;
    long long presaleDepositStartTime  = [self getTimeStampWithTime:model.presaleDepositStartTime dataFormate:@"YYYY-MM-dd HH:mm:ss"].longLongValue;
    long long presaleDepositEndTime  = [self getTimeStampWithTime:model.presaleDepositEndTime dataFormate:@"YYYY-MM-dd HH:mm:ss"].longLongValue;
    
    long long presaleBalanceStartTime  = [self getTimeStampWithTime:model.presaleBalanceStartTime dataFormate:@"YYYY-MM-dd HH:mm:ss"].longLongValue;
    long long presaleBalanceEndTime  = [self getTimeStampWithTime:model.presaleBalanceEndTime dataFormate:@"YYYY-MM-dd HH:mm:ss"].longLongValue;
    
    if ((nowStamp > presaleDepositStartTime) && (nowStamp < presaleDepositEndTime)) {
        self.topLabel.textColor = UIColorFromRGB(0x6E6BFF);
        self.downLabel.textColor = app_font_color;
        
        self.topImageView.image = [UIImage imageNamed:@"group_blue_down"];
        self.downImageView.image = [UIImage imageNamed:@"group_gray_up"];
    }else if ((nowStamp > presaleBalanceStartTime) && (nowStamp < presaleBalanceEndTime)) {
        self.topLabel.textColor = app_font_color;
        self.downLabel.textColor = UIColorFromRGB(0x6E6BFF);
        
        self.topImageView.image = [UIImage imageNamed:@"group_gray_down"];
        self.downImageView.image = [UIImage imageNamed:@"group_blue_up"];
    }else {
         self.topLabel.textColor = app_font_color;
         self.downLabel.textColor = app_font_color;
         
         self.topImageView.image = [UIImage imageNamed:@"group_gray_down"];
         self.downImageView.image = [UIImage imageNamed:@"group_gray_up"];
        
     }
    
    NSString *preStr = [NSString stringWithFormat:@"付定金 %@截止",[self formateTimeWithTimeString:model.presaleDepositEndTime]];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:preStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(16)} range:NSMakeRange(0, 3)];
    self.topLabel.attributedText = attr;
   
    NSString *remainStr = [NSString stringWithFormat:@"付尾款 %@开始",[self formateTimeWithTimeString:model.presaleBalanceStartTime]];
    NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc] initWithString:remainStr];
    [attr2 addAttributes:@{NSFontAttributeName:kFontMedium(16)} range:NSMakeRange(0, 3)];
    self.downLabel.attributedText = attr2;
    
    self.topLabel.frame = CGRectMake(29, 12, self.mj_w - 29, 24);
    self.topImageView.frame = CGRectMake(12, 20, 9, 17);
    self.downLabel.frame = CGRectMake(29, 44, self.mj_w - 29, 24);
    self.downImageView.frame = CGRectMake(12, 42, 9, 17);
}

-(void)viewWithDic:(NSDictionary *)dic {
    
    XPShopPreSaleModel *model = [XPShopPreSaleModel mj_objectWithKeyValues:dic];

    //预售-支付状态 0 未支付 1 已付定金未支付尾款(到达尾款支付时间) 2 已付尾款 10 已付定金未支付尾款(未到达尾款支付时间)
    if (model.presalePayStatus.intValue == 0) {
        self.topLabel.textColor = UIColorFromRGB(0x6E6BFF);
        self.downLabel.textColor = app_font_color;
        
        self.topLabel.text = [NSString stringWithFormat:@"%@前支付定金",[self formateTimeWithTimeString:model.presaleDepositEndTime]];
        self.topImageView.image = [UIImage imageNamed:@"group_blue_down"];
       
        self.downLabel.text = [NSString stringWithFormat:@"%@开始支付尾款",[self formateTimeWithTimeString:model.presaleBalanceStartTime]];
        self.downImageView.image = [UIImage imageNamed:@"group_gray_up"];
    }else if (model.presalePayStatus.intValue == 1) {
        self.topLabel.textColor = app_font_color;
        self.downLabel.textColor = UIColorFromRGB(0x6E6BFF);
        
        self.topLabel.text = @"定金已支付";
        self.topImageView.image = [UIImage imageNamed:@"group_gray_down"];
       
        self.downLabel.text = @"等待买家支付尾款";
        self.downImageView.image = [UIImage imageNamed:@"group_blue_up"];
    }else if (model.presalePayStatus.intValue == 2) {
        self.topLabel.textColor = app_font_color;
        self.downLabel.textColor = UIColorFromRGB(0x6E6BFF);
        
        self.topLabel.text = @"定金已支付";
        self.topImageView.image = [UIImage imageNamed:@"group_gray_down"];
       
        self.downLabel.text = @"尾款已支付";
        self.downImageView.image = [UIImage imageNamed:@"group_blue_up"];
    }else if (model.presalePayStatus.intValue == 10) {
        
        self.topLabel.textColor = app_font_color;
        self.downLabel.textColor = UIColorFromRGB(0x6E6BFF);
        
        self.topLabel.text = @"定金已支付";
        self.topImageView.image = [UIImage imageNamed:@"group_gray_down"];
       
        self.downLabel.text = [NSString stringWithFormat:@"%@开始支付尾款",[self formateTimeWithTimeString:model.presaleBalanceStartTime]];
        self.downImageView.image = [UIImage imageNamed:@"group_blue_up"];
    }
    
    self.topLabel.frame = CGRectMake(25, 4, self.mj_w - 25, 24);
    self.topImageView.frame = CGRectMake(8, 12, 9, 17);
    self.downLabel.frame = CGRectMake(25, 32, self.mj_w - 25, 24);
    self.downImageView.frame = CGRectMake(8, 32, 9, 17);
    
    
}

#pragma mark -- 时间转换
- (NSString *)getTimeStampWithTime:(NSString *)time dataFormate:(NSString *)dataFormateStr {
    
    if (![time isKindOfClass:[NSString class]] || time.length == 0) {
        return @"0";
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:dataFormateStr]; //设定时间的格式
    NSDate *tempDate = [dateFormatter dateFromString:time];//将字符串转换为时间对象
    NSString *timeStr = [NSString stringWithFormat:@"%ld", (long)[tempDate timeIntervalSince1970]*1000];
    return timeStr;
}

- (NSString *)getLocalNowStampStr {
    NSDate *tempDate = [NSDate now];//将字符串转换为时间对象
    NSString *timeStr = [NSString stringWithFormat:@"%ld", (long)[tempDate timeIntervalSince1970]*1000];
    return timeStr;
}
- (NSString *)formateTimeWithTimeString:(NSString *)timeStr {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:timeStr];
    [dateFormatter setDateFormat:@"MM-dd HH:mm"];
    
    NSString *targetDateString = [dateFormatter stringFromDate:date];
   
    return targetDateString;
}
@end
