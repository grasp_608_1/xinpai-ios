//
//  XPShopProductMainView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/9.
//

#import <UIKit/UIKit.h>
#import "XPShopProductDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopProductMainView : UIView
//优惠券
@property (nonatomic, copy) void(^discountBlock)(void);

- (CGFloat)viewWithDataModel:(XPShopProductDetailModel *)model;


@end

NS_ASSUME_NONNULL_END
