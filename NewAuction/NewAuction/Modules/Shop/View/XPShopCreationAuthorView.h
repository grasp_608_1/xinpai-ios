//
//  XPShopCreationAuthorView.h
//  NewAuction
//
//  Created by Grasp_L on 2025/1/3.
//

#import <UIKit/UIKit.h>
#import "XPShopProductDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCreationAuthorView : UIView

@property (nonatomic, copy) void(^tapBlock)(void);

- (void)viewWithDataModel:(XPShopProductDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
