//
//  XPShopTopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/5.
//

#import "XPShopTopView.h"
#import "XPLunboView.h"
#import "XPShopSortItemView.h"
#import "XPShopItemView.h"
#import "XPShopCommandView.h"

#define shopBannerHeight  156*SCREEN_WIDTH/375.

@interface XPShopTopView ()<XPLunboViewDelegate>

@property (nonatomic, strong) UIView *mainView;
//轮播图
@property (nonatomic, strong) XPLunboView *topView;
//一级分类
@property (nonatomic, strong) XPShopSortItemView *selectItemView;
//二级分类
@property (nonatomic, strong) XPShopItemView *itemView;
//推荐位
@property (nonatomic, strong) UIView *commendView;
//专题位
@property (nonatomic, strong) UIView *activityView;
////左侧推荐
//@property (nonatomic, strong) XPShopCommandView *leftView;
////右侧推荐
//@property (nonatomic, strong) XPShopCommandView *rightView;

//数据源
@property (nonatomic, copy) NSDictionary *mainDict;
//轮播图Array
@property (nonatomic, copy) NSArray *bannerArray;
//分类Array
@property (nonatomic, copy) NSArray *sortArray;
//当前分类选中位置
@property (nonatomic, assign) NSInteger currentIndex;

@end

@implementation XPShopTopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    self.currentIndex = 1;
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(0, 0, self.mj_w, 0);
    mainView.clipsToBounds = YES;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    self.topView = [[XPLunboView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, shopBannerHeight)];
//    self.topView.layer.shadowColor = RGBACOLOR(64, 61, 78, 0.1).CGColor;
//    self.topView.layer.shadowOffset = CGSizeMake(0, 4);
//    self.topView.layer.shadowOpacity = 1;
//    self.topView.layer.shadowRadius = 4;
    self.topView.cornerRadius = 0;
    self.topView.lunDelegate = self;
    [self.mainView addSubview:self.topView];
    
    XPShopSortItemView *selectItemView = [[XPShopSortItemView alloc] initWithFrame:CGRectMake(16, MaxY(self.topView) + 12, SCREEN_WIDTH - 16 - 70, 24)];
    [self.mainView addSubview:selectItemView];
    self.selectItemView = selectItemView;
    
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = CGRectMake(SCREEN_WIDTH - 70, selectItemView.mj_y, 70, 24);
    [moreButton addTarget:self action:@selector(moreButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:moreButton];
    
    UIImageView *sepView = [[UIImageView alloc] initWithFrame:CGRectMake(4, 2, 1, 20)];
    sepView.image = [UIImage imageNamed:@"Rectangle_252"];
    [moreButton addSubview:sepView];
    
    UIImageView *moreIcon = [[UIImageView alloc] initWithFrame:CGRectMake(9,4, 18, 18)];
    moreIcon.image = [UIImage imageNamed:@"xp_categroy_list"];
    [moreButton addSubview:moreIcon];
    
    UILabel *moreTitleLabel = [UILabel new];
    moreTitleLabel.font = kFontRegular(14);
    moreTitleLabel.textColor = app_font_color;
    moreTitleLabel.text = @"分类";
    moreTitleLabel.frame = CGRectMake(29, 0, 40, moreButton.mj_h);
    [moreButton addSubview:moreTitleLabel];
    
    self.itemView = [[XPShopItemView alloc] initWithFrame:CGRectMake(0, MaxY(selectItemView), self.mj_w, 88)];
    [self.mainView addSubview:self.itemView];
    
    CGFloat commendH = 120*SCREEN_WIDTH/375.;
    UIView *commendView = [UIView new];
    commendView.frame = CGRectMake(12, MaxY(self.itemView) + 12, SCREEN_WIDTH - 12*2,commendH);
    self.commendView = commendView;
    [self.mainView addSubview:commendView];
    
    UIView *activityView = [UIView new];
    activityView.frame = CGRectMake(12, MaxY(self.commendView) + 12, SCREEN_WIDTH - 12*2,commendH);
    self.activityView = activityView;
    [self.mainView addSubview:activityView];
    
}

- (CGFloat)viewWithData:(NSDictionary *)dataDict{
    
    self.mainDict = dataDict;
    CGFloat bottom = 0;
    NSArray *banerArray = dataDict[@"mallRotationVOs"];
    self.bannerArray = banerArray;
    [self.topView setLunBoArray:banerArray];
    
    NSArray *sortArray = dataDict[@"mallCategoryOneTwoVOs"];
    self.sortArray = sortArray;
    [self.selectItemView configWithtitleArray:sortArray];
    if (self.sortArray.count == 0) {
        return 0;
    }
    NSInteger index = (self.currentIndex - 1) < sortArray.count ? self.currentIndex - 1 : 0;
    [self.itemView setDataArray:self.sortArray[index][@"children"]];
    self.selectItemView.defaultSeletIndex = index;

    bottom = MaxY(self.itemView);
    
    NSArray *array = dataDict[@"mallSubjectContainProductVOs"];
    [self.commendView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat commendH = 120*SCREEN_WIDTH/375.;
    XPWeakSelf
    for (int i = 0; i<array.count; i++) {
        XPShopCommandView *commentItemView = [[XPShopCommandView alloc] initWithFrame:CGRectMake(((self.commendView.mj_w - 12)/2. + 12)* (i%2), (i/2)*(commendH + 12), (self.commendView.mj_w - 12)/2., commendH)];
        commentItemView.dataDict = array[i];
        [self.commendView addSubview:commentItemView];
        [commentItemView setTopTapBlock:^(NSDictionary * _Nonnull dic) {
            if (weakSelf.commendItemBlock) {
                weakSelf.commendItemBlock(dic);
            }
        }];
        
        if (i == array.count - 1) {
            self.commendView.frame = CGRectMake(12, bottom + 12, SCREEN_WIDTH - 12*2,MaxY(commentItemView) + 12);
            bottom = MaxY(self.commendView);
        }
    }
    
    NSArray *activityArray = dataDict[@"mallActivityContainProductVOs"];
    [self.activityView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat activityH = 120*SCREEN_WIDTH/375.;
    for (int j = 0; j<activityArray.count; j++) {
        XPShopCommandView *commentItemView = [[XPShopCommandView alloc] initWithFrame:CGRectMake(((self.activityView.mj_w - 12)/2. + 12)* (j%2), (j/2)*(activityH + 12), (self.activityView.mj_w - 12)/2., activityH)];
        commentItemView.dataDict = activityArray[j];
        [self.activityView addSubview:commentItemView];
        [commentItemView setTopTapBlock:^(NSDictionary * _Nonnull dic) {
            if (weakSelf.activityItemBlock) {
                weakSelf.activityItemBlock(dic);
            }
        }];
        [commentItemView setItemTapBlock:^(NSDictionary * _Nonnull dic) {
            if (weakSelf.activityItemBlock) {
                weakSelf.activityItemBlock(dic);
            }
        }];
        
        if (j == array.count - 1) {
            self.activityView.frame = CGRectMake(12, bottom + 12, SCREEN_WIDTH - 12*2,MaxY(commentItemView) + 12);
            bottom = MaxY(self.activityView);
        }
    }
    
    [self callBack];
    
    self.mainView.frame = CGRectMake(0,0, self.mj_w, bottom);
    
    return bottom;
}

- (void)callBack {
    
    XPWeakSelf
    [self.selectItemView setTapBlock:^(NSInteger index) {
        weakSelf.currentIndex = index ;
        //网络请求
        [weakSelf.itemView setDataArray:weakSelf.sortArray[index -1][@"children"]];
    }];
    
    [self.itemView setItemSelectBlock:self.itemSelectBlock];
    
}


#pragma mark  -- banner delegate
-(void)tapImageClick:(NSDictionary *)dic{
    if (self.bannerSelectBlock) {
        self.bannerSelectBlock(dic);
    }
}

//button Clicked
- (void)moreButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.moreBlock) {
        self.moreBlock(@{});
    }
}

@end
