//
//  XPShopOrderProductCommentCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/25.
//

#import "XPShopOrdeCommentItemView.h"
#import "XPStarView.h"
#import "XPImageAddView.h"

@interface XPShopOrdeCommentItemView ()

@property (nonatomic, strong) UIView *mainView;
//商品图标
@property (nonatomic, strong) UIImageView *pImageView;
//产品名称
@property (nonatomic, strong) UILabel *pTitleLabel;
//评价时间
@property (nonatomic, strong) UILabel *specLabel;

@property (nonatomic, strong) UILabel *starDescLabel;
//用户评价星星
@property (nonatomic, strong) XPStarView *starView;
//添加图片
@property (nonatomic, strong) XPImageAddView *imageAddView;

@property (nonatomic, strong) XPShopOrderCommentModel *model;
@end


@implementation XPShopOrdeCommentItemView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

-(void)setupView{
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 0, self.mj_w - 12*2, self.mj_h);
    mainView.clipsToBounds = YES;
    mainView.layer.cornerRadius = 8;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(12, 14, 80, 80);
    self.pImageView.layer.cornerRadius = 4;
    self.pImageView.layer.masksToBounds = YES;
    [self.mainView addSubview:_pImageView];
    
    CGFloat LabelX = MaxX(self.pImageView) + 12;
    CGFloat LabelW = self.mainView.mj_w - 140;
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontMedium(14);
    self.pTitleLabel.textColor = app_font_color;
    self.pTitleLabel.textAlignment = NO;
    self.pTitleLabel.frame = CGRectMake(LabelX, 12, LabelW, 48);
    [self.mainView addSubview:self.pTitleLabel];
    
    UILabel *specLabel = [UILabel new];
    specLabel.font = kFontRegular(12);
    specLabel.textColor = UIColorFromRGB(0xB8B8B8);
    specLabel.frame = CGRectMake(LabelX, 70,self.mainView.mj_w - 24 - 70 - LabelX, 24);
    self.specLabel = specLabel;
    [self.mainView addSubview:specLabel];
    
    UILabel *starDescLabel = [UILabel new];
    starDescLabel.font = kFontRegular(14);
    starDescLabel.textColor = app_font_color;
    starDescLabel.frame = CGRectMake(12, MaxY(self.pImageView) + 20,60, 24);
    starDescLabel.text = @"商品评价";
    [self.mainView addSubview:starDescLabel];
    
    self.starView = [[XPStarView alloc] initWithFrame:CGRectMake(84, MaxY(self.pImageView) + 20, 300, 27)];
    self.starView.canOption = YES;
    [self.mainView addSubview:self.starView];
    
    
    self.textView = [[XPLimitInputTextView alloc] initWithFrame:CGRectMake(12, MaxY(self.starView) + 20, self.mainView.mj_w - 24, 120)];
    [self.textView configWithMaxCount:200 textViewPlaceholder:@"请输入您的评价"];
    XPWeakSelf
    [self.textView setChangeBlock:^{
        weakSelf.model.textStr = weakSelf.textView.contentView.text;
        if (weakSelf.changeBlock) {
            weakSelf.changeBlock();
        }
    }];
    [self.mainView addSubview:self.textView];
    
    self.imageAddView = [[XPImageAddView alloc] initWithFrame: CGRectMake(12,MaxY(self.textView) + 12,self.mainView.mj_w - 24, 100)];
    self.imageAddView.type = 2;
    [self.mainView addSubview:self.imageAddView];
    [self callBack];
    
}

- (void)callBack {
    
    XPWeakSelf
    [self.imageAddView setChangeBlock:^(NSArray * _Nonnull imagesArray) {
       
        if (imagesArray.count == 0) {
            weakSelf.model.imagesUrlsString = @"";
        }else {
            weakSelf.model.imagesUrlsString = [imagesArray componentsJoinedByString:@","];
        }
    }];
    
    [self.starView setChangeBlock:^(NSInteger count) {
        weakSelf.model.star = [NSNumber numberWithInteger:count];
        if (weakSelf.changeBlock) {
            weakSelf.changeBlock();
        }
    }];
}


-(void)viewWithModel:(XPShopOrderCommentModel *)model {
    self.model = model;
    self.pTitleLabel.text = model.productName;
    self.specLabel.text = model.attributeValues;
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    self.starView.star = 0;
}
@end
