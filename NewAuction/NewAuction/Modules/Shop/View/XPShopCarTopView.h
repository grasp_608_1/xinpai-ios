//
//  XPShopCarTopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCarTopView : UIView

//是否显示左侧选择按钮 默认显示
@property (nonatomic, assign) BOOL showSelectItem;
//是否可侧滑删除 default no
@property (nonatomic, assign) BOOL canSlideOption;
//数量加1
@property (nonatomic, copy) void(^addBlock)(NSInteger index,NSInteger count);
//数量减1
@property (nonatomic, copy) void(^minusBlock)(NSInteger index,NSInteger count);
//重选规格
@property (nonatomic, copy) void(^specBlock)(NSInteger index);
//选中//取消 当前商品
@property (nonatomic, copy) void(^selectBlock)(NSInteger index,bool isChoose);
//删除当前商品
@property (nonatomic, copy) void(^deleteBlock)(NSInteger index);
//item 点击
@property (nonatomic, copy) void(^itemSelect)(id modelObj);
//活动按钮点击
@property (nonatomic, copy) void(^activityBlcok)(NSInteger index);
//顶部活动数据源
@property (nonatomic, copy) NSArray *activityArray;
//商品array
@property (nonatomic, strong) NSMutableArray *itemsArray;
//购物车数据源
- (CGFloat)viewWithDataArray:(NSArray *)dataArray;

@end

NS_ASSUME_NONNULL_END
