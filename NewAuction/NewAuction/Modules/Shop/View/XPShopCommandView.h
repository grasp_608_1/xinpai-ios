//
//  XPShopCommandView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCommandView : UIView

@property (nonatomic, copy) void(^itemTapBlock)(NSDictionary *dic);

@property (nonatomic, copy) void(^topTapBlock)(NSDictionary *dic);

@property (nonatomic, copy) NSDictionary *dataDict;

@end

NS_ASSUME_NONNULL_END
