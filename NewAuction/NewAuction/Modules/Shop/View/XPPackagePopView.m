//
//  XPPackagePopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/2.
//

#import "XPPackagePopView.h"
#import "XPHitButton.h"

@interface XPPackagePopView ()

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIScrollView *mainView;
@property (nonatomic, strong) UIScrollView *contentView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//小标题
@property (nonatomic, strong) UILabel *titleDescLabel;

@end

@implementation XPPackagePopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    bgView.alpha = 0;
    self.bgView = bgView;
    [self addSubview:bgView];
    
    self.mainView = [UIScrollView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h, SCREEN_WIDTH, 0.8 *self.mj_h);
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(16);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(12, 6,self.mj_w, 26);
    titleLabel.text = @"包裹列表";
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    UILabel *titleDescLabel = [UILabel new];
    titleDescLabel.font = kFontRegular(14);
    titleDescLabel.textColor = app_font_color;
    titleDescLabel.frame = CGRectMake(12, MaxY(self.titleLabel),self.mj_w, 20);
    self.titleDescLabel = titleDescLabel;
    [self.mainView addSubview:titleDescLabel];
    
    UIScrollView *contentView =  [[UIScrollView alloc] initWithFrame:CGRectMake(0, 52, self.mainView.mj_w , self.mainView.mj_h)];
    self.contentView = contentView;
    [self.mainView addSubview:contentView];
    
}

- (void)viewWithArray:(NSArray *)array{
    
    [self getTopStatus:array];
    
    CGFloat top = 0;
    
    for (int i = 0; i< array.count; i++) {
        
        NSDictionary *dic = array[i];
        
        UIView *itemView =[UIView new];
        itemView.frame = CGRectMake(12, top + 12, self.mainView.mj_w - 24, 172);
        itemView.layer.cornerRadius = 8;
        itemView.backgroundColor = UIColor.whiteColor;
        [self.contentView addSubview:itemView];

        UILabel *titleLabel = [UILabel new];
        titleLabel.font = kFontRegular(14);
        titleLabel.textColor = app_font_color_8A;
        titleLabel.frame = CGRectMake(12, 0,self.mj_w, 52);
        [itemView addSubview:titleLabel];
        
        NSString *name;
        NSNumber *deliveryStatus = dic[@"deliveryStatus"];
        if (deliveryStatus.intValue == 1) {
            name = [NSString stringWithFormat:@"包裹%d",i + 1];
        }else {
            name = @"待发货商品";
        }
        
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ 共%@件",name,dic[@"productTotalNum"]]];
        [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color,NSFontAttributeName:kFontMedium(16)} range:NSMakeRange(0, name.length)];
        titleLabel.attributedText = attr;
        
        
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 48, self.mainView.mj_w, 60)];
        [itemView addSubview:scrollView];
        
        NSArray *listArray = dic[@"mallOrderPackageItemVOS"];
        CGFloat labelLeft = 12;
        for (int j = 0; j<listArray.count; j++) {
            NSDictionary *tempDic = listArray[j];
            UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(labelLeft + 4, 0, 60, 60)];
            [iconImageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(tempDic[@"productPic"])] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
            iconImageView.layer.cornerRadius = 4;
            iconImageView.layer.masksToBounds = YES;
            [scrollView addSubview:iconImageView];
            
            NSString *labelStr = [NSString stringWithFormat:@"X%@",tempDic[@"productNum"]];
            UILabel *numLabel = [UILabel new];
            numLabel.backgroundColor = RGBACOLOR(58, 60, 65, 0.7);
            numLabel.font = kFontRegular(10);
            numLabel.textColor = UIColor.whiteColor;
            numLabel.textAlignment = NSTextAlignmentCenter;
            numLabel.text = labelStr;
            [iconImageView addSubview:numLabel];
            
            CGFloat LabelW = [labelStr sizeWithAttributes:@{NSFontAttributeName:numLabel.font}].width;
            numLabel.frame = CGRectMake(iconImageView.mj_w - LabelW - 4, iconImageView.mj_h - 12, LabelW + 4, 12);
            [XPCommonTool cornerRadius:numLabel andType:7 radius:4];
            labelLeft = MaxX(iconImageView) + 8;
        }
        scrollView.contentSize = CGSizeMake(labelLeft, scrollView.mj_h);
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(12,  12 + MaxY(scrollView), itemView.mj_w - 24, 40);
        button.backgroundColor = UIColorFromRGB(0xFAFAFA);
        button.layer.cornerRadius = 4;
        button.titleLabel.font = kFontRegular(14);
        button.tag = i;
        [button addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [itemView addSubview:button];
        
        UIButton *arrowButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [arrowButton2 setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
        arrowButton2.frame = CGRectMake(button.mj_w - 8 -18, 8, 24, 24);
        arrowButton2.tag = i;
        [arrowButton2 addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [button addSubview:arrowButton2];
        
        UILabel *expressLabel = [UILabel new];
        expressLabel.textColor = UIColorFromRGB(0x8A8A8A);
        expressLabel.font = kFontRegular(14);
        expressLabel.frame = CGRectMake(8, 0, itemView.mj_w - 8 - 8 -18 , button.mj_h);
        [button addSubview:expressLabel];
        
        if (deliveryStatus.intValue == 1) {
            expressLabel.text = [NSString stringWithFormat:@"%@ %@",TO_STR(dic[@"deliveryCompany"]),TO_STR(dic[@"deliverySn"])];
        }else{
            expressLabel.text = @"商品准备中，请耐心等待";
        }
        top = MaxY(itemView);
        
    }
    self.contentView.contentSize = CGSizeMake(self.contentView.mj_w, top + 40);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bgView.alpha = 1;
        self.mainView.frame = CGRectMake(0, self.mj_h * 0.2, SCREEN_WIDTH, 0.8 *self.mj_h);
    }];
}

- (void)arrowButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.arrowBlock) {
        self.arrowBlock(sender.tag);
        [self removeFromSuperview];
    }
}

- (void)getTopStatus:(NSArray *)array {
    NSInteger deliverNum = 0;
    NSInteger signNum = 0;
    
    for (int i = 0; i<array.count; i++) {
        NSDictionary *dict = array[i];
        NSNumber *status = dict[@"deliveryStatus"];
        NSNumber *signStatus = dict[@"signStatus"];
        if (status.intValue == 1) {
            deliverNum++;
        }
        if (signStatus.intValue == 1) {
            signNum++;
        }
    }
    
    if(signNum == array.count && signNum >0){
        NSString *tempStr = @"全部包裹已签收";
        self.titleDescLabel.text = tempStr;
    }else if(signNum >0){
        NSString *tempStr = @"部分包裹已签收，剩余商品正在努力运输中";
        self.titleDescLabel.text = tempStr;
    }else if(deliverNum == array.count && signNum == 0){
        NSString *tempStr = @"您的货品正在运输中,请耐心等待";
        self.titleDescLabel.text = tempStr;
    }else if(deliverNum > 0){
        self.titleDescLabel.text = [NSString stringWithFormat:@"商家已发出%zd个包裹，剩余商品将尽快发货",deliverNum];
    }else{
        self.titleDescLabel.text = @"待发货";
    }
    
}

- (void)cancelButtonClicked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    
    [self removeFromSuperview];
}
    
@end
