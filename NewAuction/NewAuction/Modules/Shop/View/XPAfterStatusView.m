//
//  XPAfterStatusView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/16.
//

#import "XPAfterStatusView.h"

@interface XPAfterStatusView ()
//标题栏
@property (nonatomic, strong) UILabel *titleLabel;
//钱款去向
@property (nonatomic, strong) UILabel *moneyLabel;

@property (nonatomic, strong) UIButton *button;

@end

@implementation XPAfterStatusView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
 
    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = kFontMedium(16);
    self.titleLabel.textColor = app_font_color;
    self.titleLabel.frame = CGRectMake(12,16,self.mj_w - 24,24);
    [self addSubview:self.titleLabel];
    
    self.moneyLabel = [UILabel new];
    self.moneyLabel.font = kFontMedium(16);
    self.moneyLabel.textColor = app_font_color;
    self.moneyLabel.frame = CGRectMake(12,16,self.mj_w - 24,24);
    self.moneyLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.moneyLabel];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.layer.cornerRadius = 18;
    button.layer.borderColor = UIColorFromRGB(0xB8B8B8).CGColor;
    button.layer.borderWidth = 1;
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:app_font_color forState:UIControlStateNormal];
    button.titleLabel.font = kFontRegular(14);
    button.frame = CGRectMake(12, 56, self.mj_w - 24, 36);
    self.button = button;
    [self addSubview:button];
    
    
}

- (CGFloat)viewWithModel:(XPShopAfterSaleModel *)model {
    CGFloat bottom = 0;
    ///// 售后状态 0 未提交 1 待审核(仅退款通过->5;退款退货通过->2) 2 审核成功|待归仓  3 已拒绝 4 商品回寄中 5 退款处理 中 6 已完成
    self.moneyLabel.hidden = YES;
    self.titleLabel.textColor = app_font_color;
    
    int status = model.status.intValue;
    if (status == 1) {
        self.button.hidden = NO;
        NSString *titleStr = [self getOrderStatusStr:model.status.integerValue type:model.status.integerValue];
        NSString *descStr = [self getTypeStr:model.status.integerValue];
        
        NSString *totalStr = [NSString stringWithFormat:@"%@ %@",titleStr,descStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSFontAttributeName:kFontRegular(12),NSForegroundColorAttributeName:UIColorFromRGB(0x8A8A8A)} range:NSMakeRange(totalStr.length - descStr.length, descStr.length)];
        self.titleLabel.attributedText = attr;
        [self.button setTitle:@"查看审核进度" forState:UIControlStateNormal];
        bottom = 108;
    }else if (status == 2 ||status == 3 ||status == 4){
        self.button.hidden = YES;
        NSString *titleStr = [self getOrderStatusStr:model.status.intValue type:model.type.integerValue];
        NSString *descStr = [self getTypeStr:model.status.integerValue];
        
        NSString *totalStr = [NSString stringWithFormat:@"%@ %@",titleStr,descStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSFontAttributeName:kFontRegular(12),NSForegroundColorAttributeName:UIColorFromRGB(0x8A8A8A)} range:NSMakeRange(totalStr.length - descStr.length, descStr.length)];
        self.titleLabel.attributedText = attr;
        [self.button setTitle:@"查看审核进度" forState:UIControlStateNormal];
        bottom = 56;
    }else if (status == 5){
        self.button.hidden = YES;
        NSString *titleStr = [self getOrderStatusStr:model.status.intValue type:model.type.integerValue];
        NSString *descStr = [self getTypeStr:model.status.integerValue];
        
        NSString *totalStr = [NSString stringWithFormat:@"%@ %@",titleStr,descStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSFontAttributeName:kFontRegular(12),NSForegroundColorAttributeName:UIColorFromRGB(0x8A8A8A)} range:NSMakeRange(totalStr.length - descStr.length, descStr.length)];
        self.titleLabel.attributedText = attr;
        [self.button setTitle:@"查看审核进度" forState:UIControlStateNormal];
        bottom = 56;
    }else if (status == 6){
        self.button.hidden = NO;
        self.moneyLabel.hidden = NO;
        NSString *titleStr = [self getOrderStatusStr:model.status.intValue type:model.type.integerValue];
        NSString *descStr = model.createTime;
        
        self.titleLabel.textColor = UIColorFromRGB(0xFF4A4A);
        
        NSString *totalStr = [NSString stringWithFormat:@"%@ %@",titleStr,descStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSFontAttributeName:kFontRegular(12),NSForegroundColorAttributeName:UIColorFromRGB(0x8A8A8A)} range:NSMakeRange(totalStr.length - descStr.length, descStr.length)];
        self.titleLabel.attributedText = attr;
    
        NSString *moneyDescStr = @"已退款";
        NSString *moneyStr = [XPMoneyFormatTool formatMoneyWithNum:model.returnSumAmount] ;
        NSString *totalMoneyStr = [NSString stringWithFormat:@"%@￥%@",moneyDescStr,moneyStr];
        
        NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc] initWithString:totalMoneyStr];
        [attr2 addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(totalMoneyStr.length - moneyStr.length - 1, moneyStr.length + 1)];
        self.moneyLabel.attributedText = attr2;
        
        [self.button setTitle:@"查看钱款去向" forState:UIControlStateNormal];
    
        bottom = 108;
    }
    return bottom;
}

- (void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.tapBlock) {
        self.tapBlock();
    }
}

//订单退货状态
- (NSString *)getOrderStatusStr:(NSInteger) status type:(NSInteger)type{
    NSString *str = @"";
    ///// 售后状态 0 未提交 1 待审核(仅退款通过->5;退款退货通过->2) 2 审核成功|待归仓  3 已拒绝 4 商品回寄中 5 退款处理 中 6 已完成
    switch (status) {
        case 0:
            str = @"未提交";
            break;
        case 1:
            str = @"待审核";
            break;
        case 2:
            str = @"等待寄回商品";
            break;
        case 3:
            str = @"商家已拒绝退货申请";
            break;
        case 4:
            str = @"等待商家收货";
            break;
        case 5:
            if (type == 3) {
                str = @"商家已收货等待退款";
            }else {
                str = @"等待退款";
            }
            break;
        case 6:
            str = @"退款成功";
            break;
        default:
            str = @"售后处理中";
            break;
    }
    return str;
    
}


//售后类型
- (NSString *)getTypeStr:(NSInteger) type {
    NSString *str = @"";
    ///// 售后状态 0 未提交 1 待审核(仅退款通过->5;退款退货通过->2) 2 审核成功|待归仓  3 已拒绝 4 商品回寄中 5 退款处理 中 6 已完成
    switch (type) {

        case 1:
            str = @"商家正在审核，请耐心等待";
            break;
        case 2:
            str = @"审核成功请填写寄件信息";
            break;
        case 3:
            str = @"联系商家";
            break;
        case 4:
            str = @"商品正在运输中";
            break;
        case 5:
            str = @"商家退款中";
            break;
        default:
            str = @"售后处理中";
            break;
    }
    return str;
    
}

@end
