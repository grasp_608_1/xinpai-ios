//
//  XPDefaultContentPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/21.
//

#import "XPDefaultContentPopView.h"
#import "XPHitButton.h"

@interface XPDefaultContentPopView ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *mainView;
//底部下一步按钮
@property (nonatomic, strong) UIButton *nextButton;

@end


@implementation XPDefaultContentPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    bgView.alpha = 0;
    self.bgView = bgView;
    [self addSubview:bgView];
    
    self.mainView = [UIScrollView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h, SCREEN_WIDTH, 0.7 *self.mj_h - 48 - kBottomHeight);
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(18);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(0, 0,self.mj_w, 52);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    UIScrollView *contentView =  [[UIScrollView alloc] initWithFrame:CGRectMake(0, 52, self.mainView.mj_w, self.mainView.mj_h - 52)];
    self.contentView = contentView;
    [self.mainView addSubview:contentView];
    
    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,self.mj_h - 48 - kBottomHeight, self.mj_w, 48 + kBottomHeight);
    bottomView.backgroundColor = UIColor.whiteColor;
    [self addSubview:bottomView];
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton setTitle:@"我知道了" forState:UIControlStateNormal];
    [nextButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    nextButton.frame = CGRectMake(24,4,bottomView.mj_w - 48,40);
    nextButton.layer.cornerRadius = 20;
    nextButton.layer.masksToBounds = YES;
    [nextButton addTarget:self action:@selector(nextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.nextButton = nextButton;
    [bottomView addSubview:nextButton];
    [nextButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :nextButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
}

- (void)showPopWithTitle:(NSString *)titleStr bottomTitle:(NSString *)bottomStr {
   
    self.titleLabel.text= titleStr;
    [self.nextButton setTitle:bottomStr forState:UIControlStateNormal];
    
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bgView.alpha = 1;
        self.mainView.frame = CGRectMake(0, self.mj_h * 0.3, SCREEN_WIDTH, 0.7 *self.mj_h - 48 - kBottomHeight);
    }];
    
}


- (void)cancelButtonClicked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    
    [self removeFromSuperview];
}

- (void)nextButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.finishBlock) {
        self.finishBlock();
    }else {
        [self removeFromSuperview];
    }
}
@end
