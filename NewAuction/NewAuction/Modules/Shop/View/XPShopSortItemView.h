//
//  XPShopgSortItemView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ActionBlock)(NSInteger index);

@interface XPShopSortItemView : UIScrollView

@property (nonatomic, assign) CGFloat itemSpace;

@property (nonatomic, assign) NSUInteger defaultSeletIndex;

@property (nonatomic, copy) ActionBlock tapBlock;
- (CGFloat)configWithtitleArray:(NSArray *)titleArray;

@end

NS_ASSUME_NONNULL_END
