//
//  XPShopCommendCollectionViewCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/21.
//

#import "XPShopCommendCollectionViewCell.h"


@interface XPShopCommendCollectionViewCell ()

//商品图片
@property (nonatomic, strong) UIImageView *imageView;
//3d图标
@property (nonatomic, strong) UIImageView *signImageView;

//商品名称
@property (nonatomic, strong) UILabel *titleLabel;
//标签
@property (nonatomic, strong) UIScrollView *signView;
//商品购买价格
@property (nonatomic, strong) UILabel *priceSaleLabel;
//商品提货价格
@property (nonatomic, strong) UILabel *priceCostLabel;

@end

@implementation XPShopCommendCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = UIColorFromRGB(0xF7F4FF);
        self.layer.cornerRadius = 4;
        self.clipsToBounds = YES;
        [self  setupViews];
        
    }
    
    return self;
}

-(void)setupViews
{
    CGFloat margin = 8;
    CGFloat cellW = self.bounds.size.width;
    self.imageView = [[UIImageView alloc] init];
    self.imageView.frame = CGRectMake(0, 0,cellW ,cellW);
    self.imageView.image = [UIImage imageNamed:@"xp_pd_default"];
    [self addSubview:self.imageView];
    
    self.signImageView = [[UIImageView alloc] init];
    self.signImageView.frame = CGRectMake(cellW - 45,0,45 ,30);
    self.signImageView.image = [UIImage imageNamed:@"xp_sign_3d"];
    self.signImageView.hidden = YES;
    [self.imageView addSubview:self.signImageView];
    
    
    self.titleLabel = [UILabel new];
    self.titleLabel.frame = CGRectMake(margin, MaxY(self.imageView) + margin, cellW - 2 * margin, 20);
    self.titleLabel.font = kFontMedium(14);
    self.titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.titleLabel.lineBreakMode  = NSLineBreakByTruncatingTail;
    self.titleLabel.numberOfLines = 2;
    [self addSubview:self.titleLabel];
    
    self.priceSaleLabel = [UILabel new];
    self.priceSaleLabel.frame = CGRectMake(margin,MaxY(self.titleLabel) + 4, cellW - 2 * margin, 20);
    self.priceSaleLabel.font = kFontRegular(16);
    self.priceSaleLabel.textColor = UIColorFromRGB(0xFF4A4A);
    [self addSubview:self.priceSaleLabel];
    
    self.priceCostLabel = [UILabel new];
    self.priceCostLabel.frame = CGRectMake(margin,MaxY(self.priceSaleLabel) + 4, cellW - 2 * margin, 20);
    self.priceCostLabel.font = kFontRegular(10);
    self.priceCostLabel.textColor = app_font_color_8A;
    [self addSubview:self.priceCostLabel];
    
    self.signView = [[UIScrollView alloc] init];
    self.signView.bounces = NO;
    self.signView.showsHorizontalScrollIndicator = NO;
    self.signView.frame = CGRectMake(12, MaxY(self.priceCostLabel) + 4,self.mj_w - 12,16);
    [self addSubview:self.signView];

}

- (CGFloat)cellWithModel:(XPShopModel *)model {

    CGFloat margin = 8;
    CGFloat imageWH = (SCREEN_WIDTH - 36)/2.;
    self.imageView.frame = CGRectMake(0, 0, imageWH, imageWH);
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] init];
    if (model.presaleStatus.intValue == 1) {//预售
        NSTextAttachment *attch = [[NSTextAttachment alloc] init];
        attch.image = [UIImage imageNamed:@"xp_circle_preSale"];
        attch.bounds = CGRectMake(0, -2, 32,16);
        NSAttributedString *signAttr = [NSAttributedString attributedStringWithAttachment:attch];
        [attr appendAttributedString:signAttr];
    }else if (model.groupStatus.intValue == 1) {//拼团
        NSTextAttachment *attch = [[NSTextAttachment alloc] init];
        attch.image = [UIImage imageNamed:@"xp_circle_team"];
        attch.bounds = CGRectMake(0, -2, 32,16);
        NSAttributedString *signAttr = [NSAttributedString attributedStringWithAttachment:attch];
        [attr appendAttributedString:signAttr];
    }
    NSAttributedString *titleStr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",model.skuName]];
    [attr appendAttributedString:titleStr];
    
    CGFloat titleH = [XPCommonTool calculateHeightForAttributedString:attr withWidth:imageWH - 2 * margin];

    self.titleLabel.frame = CGRectMake(margin, MaxY(self.imageView) + margin, imageWH - 2 * margin, titleH > 40 ? 40:titleH);
    self.titleLabel.attributedText = attr;
    [self.titleLabel sizeToFit];
    
    self.priceSaleLabel.frame = CGRectMake(margin,MaxY(self.titleLabel) + 4, imageWH - 2 * margin, 20);
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.pic]] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    if (model.modelStatus.intValue == 1) {
        self.signImageView.hidden = NO;
        NSString *priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceSale]];
        NSMutableAttributedString *attr9 = [[NSMutableAttributedString alloc] initWithString:priceStr];
        [attr9 addAttributes:@{NSFontAttributeName:kFontMedium(10)} range:NSMakeRange(0,1)];
        self.priceSaleLabel.attributedText = attr9;
        self.priceCostLabel.hidden = YES;

        return MaxY(self.priceSaleLabel) + 8;
    }else if (model.pointStatus.intValue == 1){
        self.signImageView.hidden = YES;
        self.priceCostLabel.hidden = YES;

        NSString *priceStr = [NSString stringWithFormat:@"%@ 积分",model.pricePoint];
        
        NSMutableAttributedString *attr10 = [[NSMutableAttributedString alloc] initWithString:priceStr];
        [attr10 addAttributes:@{NSFontAttributeName:kFontMedium(12)} range:NSMakeRange(priceStr.length - 2,2)];
        self.priceSaleLabel.attributedText = attr10;
        
        return MaxY(self.priceSaleLabel) + 8;
        
    }else {
        self.priceCostLabel.hidden = NO;
        self.signImageView.hidden = YES;
    }
    
    NSString *priceSaleStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceSale]];
    if (model.sale.intValue >= 10) {
        NSString *saleNumStr = [NSString stringWithFormat:@"销量 %@",model.sale];
       NSString *priceStr = [NSString stringWithFormat:@"%@ %@",priceSaleStr,saleNumStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
        [attr addAttributes:@{NSFontAttributeName:kFontRegular(12)} range:NSMakeRange(0,1)];
        [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color_8A,NSFontAttributeName:kFontRegular(12)} range:NSMakeRange(priceStr.length - saleNumStr.length, saleNumStr.length)];
        self.priceSaleLabel.attributedText = attr;
        
    }else {
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceSaleStr];
        [attr addAttributes:@{NSFontAttributeName:kFontRegular(12)} range:NSMakeRange(0,1)];
        self.priceSaleLabel.attributedText = attr;
    }
    
    NSString *priceCostStr = [NSString stringWithFormat:@"提货价:￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceCost]];
    self.priceCostLabel.text = priceCostStr;
    
    self.priceCostLabel.frame = CGRectMake(margin,MaxY(self.priceSaleLabel) + 4, imageWH - 2 * margin, 20);
    
    
    [self.signView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    NSMutableArray *titleArray = [NSMutableArray array];
    if (model.activityNote.length > 0) {
        [titleArray addObject:model.activityNote];
    }
//    if (model.freightNote.length > 0) {
//        [titleArray addObject:model.freightNote];
//    }
//    
    if (titleArray.count == 0) {
        return MaxY(self.priceCostLabel) + 8;
    }
    
    CGFloat left = 0;
    for (int i = 0; i<titleArray.count; i++) {
        NSString *str = titleArray[i];
        UILabel *label = [UILabel new];
        label.text = str;
        label.font = kFontMedium(10);
        if (i == 0 && model.activityNote.length > 0) {
            label.textColor = UIColorFromRGB(0xF55B19);
        }else {
            label.textColor = UIColorFromRGB(0xFF3F6D);
        }
        [self.signView addSubview:label];
        CGFloat labelW = [str sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
        label.frame = CGRectMake(left,0,labelW,self.signView.mj_h);
        left = MaxX(label) + 4;
    }
    self.signView.frame = CGRectMake(12, MaxY(self.priceCostLabel) + 4,self.mj_w -12,16);
    return MaxY(self.signView) + 8;
}
@end
