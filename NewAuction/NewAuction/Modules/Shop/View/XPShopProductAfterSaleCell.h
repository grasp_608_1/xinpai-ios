//
//  XPShopProductAfterSaleCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/20.
//

#import <UIKit/UIKit.h>
#import "XPShopAfterSaleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopProductAfterSaleCell : UITableViewCell

//右侧 0 中间1 左侧 1
@property (nonatomic, copy) void(^buttonBlock)(NSInteger tag);

//@property (nonatomic, strong) XPShopAfterSaleModel *model;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (CGFloat)cellWithModel:(XPShopAfterSaleModel *)model;



@end

NS_ASSUME_NONNULL_END
