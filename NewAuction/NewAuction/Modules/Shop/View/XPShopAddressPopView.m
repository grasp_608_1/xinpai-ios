//
//  XPShopAddressPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/11.
//

#import "XPShopAddressPopView.h"
#import "XPHitButton.h"
#import "XPEmptyView.h"
#import "XPUserAddressCell.h"

@interface XPShopAddressPopView ()<UITableViewDelegate,UITableViewDataSource>
//父view的控制器
@property (nonatomic, strong) XPBaseViewController *controller;
//默认地址
@property (nonatomic, strong) XPAdressModel *currentModel;

@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView *mainView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//列表
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) XPEmptyView *emptyView;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation XPShopAddressPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    bgView.alpha = 0;
    self.bgView = bgView;
    [self addSubview:bgView];
    
    self.mainView = [UIView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h, SCREEN_WIDTH, 0.85 *self.mj_h);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(18);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(0, 16,self.mj_w, 24);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"配送至";
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    CGFloat emptyW = self.mj_w - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 150, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"您还没有收货地址"];
    [self.mainView addSubview:self.emptyView];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, 56, self.mainView.mj_w , self.mainView.mj_h -56 - 72);
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainView addSubview:self.tableView];
    
    CGFloat bottomW = (self.mainView.mj_w - 24)/2.;
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.backgroundColor = RGBACOLOR(110, 107, 255, 0.15);
    leftButton.frame = CGRectMake(12, self.mainView.mj_h - 64, bottomW, 40);
    [XPCommonTool cornerRadius:leftButton andType:6 radius:20];
    [leftButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    leftButton.titleLabel.font = kFontMedium(14);
    [leftButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setTitle:@"选择其他地址" forState:UIControlStateNormal];
    leftButton.tag = 0;
    [self.mainView addSubview:leftButton];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
    rightButton.frame = CGRectMake(self.mainView.mj_w/2., self.mainView.mj_h - 64, bottomW, 40);
    [XPCommonTool cornerRadius:rightButton andType:4 radius:20];
    [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    rightButton.titleLabel.font = kFontMedium(14);
    [rightButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setTitle:@"确定" forState:UIControlStateNormal];
    rightButton.tag = 1;
    [self.mainView addSubview:rightButton];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPUserAddressCell *cell = [XPUserAddressCell cellWithTableView:tableView];
    cell.cellType = 1;
    cell.rightArrow.hidden = NO;
    XPAdressModel *model = self.dataArray[indexPath.row];
    
    [cell cellWithDic:model];
    //只有一行时
    if (indexPath.row == 0) {
            [XPCommonTool cornerRadius:cell.mainView andType:1 radius:4];
        }else if (indexPath.row == self.dataArray.count - 1) {
            [XPCommonTool cornerRadius:cell.mainView andType:2 radius:4];
        }else {
            
        }
        if (indexPath.row == self.dataArray.count - 1) {
            cell.downLine.hidden = YES;
        }else {
            cell.downLine.hidden = NO;
        }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 86;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    XPAdressModel *model = self.dataArray[indexPath.row];
    self.currentModel = model;
    [self selectAddress];
    [self.tableView reloadData];
}


- (void)cancelButtonClicked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    
    [self removeFromSuperview];
}


- (void)showPopViewWithController:(XPBaseViewController *)controller
                            adressModel:(XPAdressModel *)model {
    self.controller = controller;
    self.currentModel = model;
    
    [self getData];
    [UIView animateWithDuration:0.5 animations:^{
        self.bgView.alpha = 1;
        self.mainView.frame = CGRectMake(0, self.mj_h *0.15, SCREEN_WIDTH, 0.85 *self.mj_h);
    }];
}


- (void)bottomButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (sender.tag == 0) {//选择其他地址
        if (self.addressChangeBlock) {
            self.addressChangeBlock();
        }
    }else {//确定
        if (self.addressSelectBlock) {
            self.addressSelectBlock(self.currentModel);
        }
    }
}

- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kUser_adress_list];

    [self.controller startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.controller stopLoadingGif];
        if (data.isSucess) {
            NSArray *modelArray = [XPAdressModel mj_objectArrayWithKeyValuesArray:data.userData];
            
            if (modelArray.count > 0) {
                self.dataArray = modelArray.mutableCopy;
                [self selectAddress];
                [self.tableView reloadData];
                self.tableView.hidden = NO;
                self.emptyView.hidden = YES;
            }else {
                self.tableView.hidden = YES;
                self.emptyView.hidden = NO;
            }
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
}

- (void)selectAddress{
    
    for (int i = 0; i<self.dataArray.count; i++) {
        XPAdressModel *model = self.dataArray[i];
        if (model.productId.intValue == self.currentModel.productId.intValue) {
            model.isSelected = @"1";
        }else {
            model.isSelected = @"0";
        }
    }
}


@end
