//
//  XPShopCollectionViewCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/25.
//

#import <UIKit/UIKit.h>
#import "XPShopModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCollectionViewCell : UICollectionViewCell

- (CGFloat)viewWithModel:(XPShopModel *)model;

@end

NS_ASSUME_NONNULL_END
