//
//  XPShopDetailBottomView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/10.
//

#import <UIKit/UIKit.h>
#import "XPShopProductDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopDetailBottomView : UIView

@property (nonatomic, copy) void(^itemClickedBlock)(NSInteger index);

@property (nonatomic, assign) BOOL hasCollected;

- (void)configUIWithModel:(XPShopProductDetailModel *)model isShowCreation:(BOOL)isShow;

@end

NS_ASSUME_NONNULL_END
