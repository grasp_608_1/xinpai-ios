//
//  XPShopCouponCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/10.
//

#import "XPShopCouponCell.h"

@interface XPShopCouponCell ()
//背景图
@property (nonatomic, strong) UIImageView *bgImageView;
//可用金额
@property (nonatomic, strong) UILabel *moneyLabel;
//标志
@property (nonatomic, strong) UILabel *signLabel;
//满减条件
@property (nonatomic, strong) UILabel *avaliableLabel;
//满减条件2
@property (nonatomic, strong) UILabel *limitedLabel;
//使用限期
@property (nonatomic, strong) UILabel *timeLabel;

@end


@implementation XPShopCouponCell
+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPShopCouponCell";
    XPShopCouponCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPShopCouponCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    UIImageView *bgImageView = [UIImageView new];
    bgImageView.frame = CGRectMake(16, 0, SCREEN_WIDTH - 32, 100);
    self.bgImageView = bgImageView;
    [self.contentView addSubview:bgImageView];
    
    UIButton *useButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    useButton.backgroundColor = UIColorFromRGB(0xF5E5E5);
    useButton.layer.cornerRadius = 12;
    useButton.layer.borderColor = UIColorFromRGB(0xFF4A4A).CGColor;
    useButton.layer.borderWidth = 0.5;
    useButton.layer.masksToBounds = YES;
    [useButton setTitleColor:UIColorFromRGB(0xFF4A4A) forState:UIControlStateNormal];
    useButton.frame = CGRectMake(self.bgImageView.mj_w - 24 - 32 , 42, 64, 24);
    useButton.titleLabel.font = kFontMedium(12);
    [useButton addTarget:self action:@selector(useButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.useButton = useButton;
    [self.contentView addSubview:useButton];
    
    
    UILabel *moneyLabel = [UILabel new];
    moneyLabel.font = kFontMedium(28);
    moneyLabel.textColor = UIColorFromRGB(0xFF4A4A);
    moneyLabel.frame = CGRectMake(12, 27,self.bgImageView.mj_w * 0.3, 32);
    moneyLabel.textAlignment = NSTextAlignmentCenter;
    self.moneyLabel = moneyLabel;
    [self.contentView addSubview:moneyLabel];
    
    UILabel *signLabel = [UILabel new];
    signLabel.font = kFontRegular(10);
    signLabel.textColor = UIColorFromRGB(0xDC6A3A);
    signLabel.frame = CGRectMake(12, 27,self.bgImageView.mj_w * 0.3, 32);
    signLabel.textAlignment = NSTextAlignmentCenter;
    signLabel.layer.borderColor = UIColorFromRGB(0xDC6A3A).CGColor;
    signLabel.layer.borderWidth = 0.5;
    signLabel.layer.cornerRadius = 2;
    signLabel.layer.masksToBounds = YES;
    self.signLabel = signLabel;
    [self.contentView addSubview:signLabel];
    
    
    UILabel *avaliableLabel = [UILabel new];
    avaliableLabel.font = kFontMedium(16);
    avaliableLabel.textColor = UIColorFromRGB(0xFF4A4A);
    avaliableLabel.frame = CGRectMake(self.bgImageView.mj_w * 0.36 + 12, 20,self.bgImageView.mj_w * 0.64 - 14, 20);
    self.avaliableLabel = avaliableLabel;
    [self.contentView addSubview:avaliableLabel];
    
    
    UILabel *limitedLabel = [UILabel new];
    limitedLabel.font = kFontRegular(12);
    limitedLabel.textColor = UIColorFromRGB(0x8A8A8A);
    limitedLabel.frame = CGRectMake(self.bgImageView.mj_w * 0.36 + 12, 44,self.bgImageView.mj_w * 0.64 - 14 - 12 - 64, 16);
    self.limitedLabel = limitedLabel;
    [self.contentView addSubview:limitedLabel];
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = kFontRegular(12);
    timeLabel.textColor = UIColorFromRGB(0x8A8A8A);
    timeLabel.frame = CGRectMake(self.bgImageView.mj_w * 0.36 + 12, 64,self.bgImageView.mj_w * 0.64 - 14, 16);
    self.timeLabel = timeLabel;
    [self.contentView addSubview:timeLabel];
    
}

- (void)setModel:(XPCouponModel *)model {
    
    _model = model;
    
    self.signLabel.text = model.note;
    CGFloat signW = [self.signLabel.text sizeWithAttributes:@{NSFontAttributeName:self.signLabel.font}].width;
    self.signLabel.frame = CGRectMake(self.moneyLabel.center.x - signW/2. - 2, 59,signW + 4, 14);
    
    NSString *priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.amount]];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(16)} range:NSMakeRange(0, 1)];
    
    self.moneyLabel.attributedText = attr;
    self.avaliableLabel.text = model.name;
    self.limitedLabel.text = model.remark;
    self.timeLabel.text = [NSString stringWithFormat:@"%@-%@",model.startTime,model.endTime];
    
    if (model.receiveStatus.intValue == 0) {
        [self.useButton setTitle:@"立即领取" forState:UIControlStateNormal];
        self.bgImageView.image = [UIImage imageNamed:@"xp_coupon_normal"];
    }else if(model.receiveStatus.intValue == 1){
        [self.useButton setTitle:@"去使用" forState:UIControlStateNormal];
        self.bgImageView.image = [UIImage imageNamed:@"xp_coupon_received"];
    }
    else if(model.receiveStatus.intValue == 2){
        [self.useButton setTitle:@"已使用" forState:UIControlStateNormal];
        self.bgImageView.image = [UIImage imageNamed:@"xp_coupon_received"];
    }
    else if(model.receiveStatus.intValue == 3){
        [self.useButton setTitle:@"已过期" forState:UIControlStateNormal];
        self.bgImageView.image = [UIImage imageNamed:@"xp_coupon_received"];
    }else{
        
    }
    
    if (self.cellType == 2 || self.cellType == 3) {
        self.contentView.alpha = 0.5;
        self.userInteractionEnabled = NO;
    }else {
        self.contentView.alpha = 1;
        self.userInteractionEnabled = YES;
    }
    
}

- (void)useButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
//    if (self.model.receiveStatus.intValue == 1) {
//        [[XPShowTipsTool shareInstance] showMsg:@"已领取优惠券,请前往使用" ToView:k_keyWindow];
//        return;
//    }
    if (self.receiveBlock) {
        self.receiveBlock();
    }
}

@end
