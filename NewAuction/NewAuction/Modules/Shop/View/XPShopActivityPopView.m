//
//  XPShopActivityPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/15.
//

#import "XPShopActivityPopView.h"
#import "XPHitButton.h"

@interface XPShopActivityPopView ()

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIScrollView *mainView;
@property (nonatomic, strong) UIScrollView *contentView;

@property (nonatomic, strong) UIView *itemView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//内容小标题
@property (nonatomic, strong) UILabel *contentTitleLabel;
//主要内容描述
@property (nonatomic, strong) UILabel *contentLabel;
//Jiantou
@property (nonatomic, strong) XPHitButton *arrowButton4;

@property (nonatomic, assign) XPShopPopShowType type;

@property (nonatomic, strong) XPShopProductDetailModel *model;

@end

@implementation XPShopActivityPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    bgView.alpha = 0;
    self.bgView = bgView;
    [self addSubview:bgView];
    
    self.mainView = [UIScrollView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h, SCREEN_WIDTH, 0.7 *self.mj_h - 48 - kBottomHeight);
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(18);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(0, 0,self.mj_w, 52);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    UIScrollView *contentView =  [[UIScrollView alloc] initWithFrame:CGRectMake(0, 52, self.mainView.mj_w , self.mainView.mj_h - 52)];
    self.contentView = contentView;
    [self.mainView addSubview:contentView];
    
    self.itemView = [UIView new];
    self.itemView.backgroundColor = UIColor.whiteColor;
    self.itemView.layer.cornerRadius = 8;
    self.itemView.layer.masksToBounds = YES;
    self.itemView.frame = CGRectMake(12, 0, self.contentView.mj_w - 24,0);
    [self.contentView addSubview:self.itemView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.itemView addGestureRecognizer:tap];
    
    
    
    UILabel *contentTitleLabel = [UILabel new];
    contentTitleLabel.font = kFontMedium(14);
    contentTitleLabel.textColor = UIColorFromRGB(0xFF3F6D);
    contentTitleLabel.frame = CGRectMake(12, 16,self.contentView.mj_w - 24, 24);
    self.contentTitleLabel = contentTitleLabel;
    [self.itemView addSubview:contentTitleLabel];
    
    UILabel *contentLabel = [UILabel new];
    contentLabel.font = kFontRegular(14);
    contentLabel.textColor = UIColorFromRGB(0x8A8A8A);
    contentLabel.frame = CGRectMake(12, 16,self.contentView.mj_w - 40, 24);
    contentLabel.numberOfLines = NO;
    self.contentLabel = contentLabel;
    [self.itemView addSubview:contentLabel];
    
    XPHitButton *arrowButton4 = [[XPHitButton alloc] init];
    arrowButton4.hitRect = 25;
    arrowButton4.userInteractionEnabled = NO;
    [arrowButton4 setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    arrowButton4.tag = 2;
    arrowButton4.frame = CGRectMake(self.itemView.mj_w - 8 - 18, 15, 18, 18);
    self.arrowButton4 = arrowButton4;
    [self.itemView addSubview:arrowButton4];
    

    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,self.mj_h - 48 - kBottomHeight, self.mj_w, 48 + kBottomHeight);
    bottomView.backgroundColor = UIColor.whiteColor;
    [self addSubview:bottomView];
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton setTitle:@"我知道了" forState:UIControlStateNormal];
    [nextButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    nextButton.frame = CGRectMake(24,4,bottomView.mj_w - 48,40);
    nextButton.layer.cornerRadius = 20;
    nextButton.layer.masksToBounds = YES;
    [nextButton addTarget:self action:@selector(nextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:nextButton];
    [nextButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :nextButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
}

-(void)viewWithModel:(XPShopProductDetailModel *)model type:(XPShopPopShowType)type{
    
    
    _type = type;
    _model = model;
    NSString *desc;
    if (type == XPShopPopShowTypeActivity || type == XPShopPopShowTypeActivities) {
        self.titleLabel.text = @"活动";
        desc = model.activityRemark;
        self.contentTitleLabel.text = model.activityNote;
        self.contentTitleLabel.textColor = UIColorFromRGB(0xFF3F6D);
    }else {
        self.titleLabel.text = @"运费";
        desc = model.freightRemark;
        self.contentTitleLabel.text = model.freightNote;
        self.contentTitleLabel.textColor = app_font_color;
    }
    
    CGFloat descW = 0;
    if (type == XPShopPopShowTypeActivity) {
        descW = self.itemView.mj_w - 40;
        self.arrowButton4.hidden = NO;
        CGFloat descH = [desc boundingRectWithSize:CGSizeMake(descW, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.contentLabel.font} context:nil].size.height;
        
        self.contentLabel.text = desc;
        self.itemView.frame = CGRectMake(12, 0, self.contentView.mj_w - 24, descH + 52 + 12);
        self.contentTitleLabel.frame = CGRectMake(12, 16,self.itemView.mj_w - 24, 24);
        self.contentLabel.frame = CGRectMake(12, 52,descW, descH);
        self.arrowButton4.frame = CGRectMake(self.itemView.mj_w - 8 - 18, self.itemView.mj_h/2 - 9, 18, 18);
        
        self.contentView.contentSize = CGSizeMake(self.contentView.mj_w, MaxY(self.itemView) + 12);
    }else if(type == XPShopPopShowTypeTransport) {
        self.arrowButton4.hidden = YES;
        descW = self.itemView.mj_w - 24;
        CGFloat descH = [desc boundingRectWithSize:CGSizeMake(descW, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.contentLabel.font} context:nil].size.height;
        
        self.contentLabel.text = desc;
        self.itemView.frame = CGRectMake(12, 0, self.contentView.mj_w - 24, descH + 52 + 12);
        self.contentTitleLabel.frame = CGRectMake(12, 16,self.itemView.mj_w - 24, 24);
        self.contentLabel.frame = CGRectMake(12, 52,descW, descH);
        self.arrowButton4.frame = CGRectMake(self.itemView.mj_w - 8 - 18, self.itemView.mj_h/2 - 9, 18, 18);
        
        self.contentView.contentSize = CGSizeMake(self.contentView.mj_w, MaxY(self.itemView) + 12);
    }else if(type == XPShopPopShowTypeActivities){
        [self.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        CGFloat bottom = 0;
        for (int i = 0; i< model.orderActivityVOs.count; i++) {
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.backgroundColor = UIColor.whiteColor;
            button.layer.cornerRadius = 8;
            button.layer.masksToBounds = YES;
            button.frame = CGRectMake(12, 0, self.contentView.mj_w - 24,0);
            button.tag = i;
            [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:button];
            
            UILabel *contentTitleLabel = [UILabel new];
            contentTitleLabel.font = kFontMedium(14);
            contentTitleLabel.textColor = UIColorFromRGB(0xFF3F6D);
            [button addSubview:contentTitleLabel];
            
            UILabel *contentLabel = [UILabel new];
            contentLabel.font = kFontRegular(14);
            contentLabel.textColor = UIColorFromRGB(0x8A8A8A);
            contentLabel.frame = CGRectMake(12, 16,button.mj_w - 40, 24);
            contentLabel.numberOfLines = NO;
            [button addSubview:contentLabel];
            
            XPHitButton *arrowButton4 = [[XPHitButton alloc] init];
            arrowButton4.userInteractionEnabled = NO;
            [arrowButton4 setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
            arrowButton4.frame = CGRectMake(button.mj_w - 8 - 18, 15, 18, 18);
            [button addSubview:arrowButton4];
            
            descW = button.mj_w - 24;
            
            NSDictionary *dic = model.orderActivityVOs[i];
            
            contentTitleLabel.text = TO_STR(dic[@"note"]);
            contentTitleLabel.frame = CGRectMake(12, 16,button.mj_w - 40, 24);
            
            contentLabel.text = TO_STR(dic[@"remark"]);
            
            CGFloat descH = [contentLabel.text boundingRectWithSize:CGSizeMake(descW, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:contentLabel.font} context:nil].size.height;
            contentLabel.frame = CGRectMake(12, 52,descW, descH);
            
            button.frame = CGRectMake(12, bottom, self.contentView.mj_w - 24, descH + 52 + 12);
            
            arrowButton4.frame = CGRectMake(button.mj_w - 8 - 18, button.mj_h/2 - 9, 18, 18);
            
            bottom = MaxY(button) + 12;
        }
        self.contentView.contentSize = CGSizeMake(self.contentView.mj_w, bottom + 12);
        
        
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bgView.alpha = 1;
        self.mainView.frame = CGRectMake(0, self.mj_h * 0.3, SCREEN_WIDTH, 0.7 *self.mj_h - 48 - kBottomHeight);
    }];
    

}

- (void)cancelButtonClicked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    
    [self removeFromSuperview];
}
- (void)nextButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self removeFromSuperview];
}

-(void)tap:(UITapGestureRecognizer *)tap {
    buttonCanUseAfterOneSec(tap.view);
    if (self.type == XPShopPopShowTypeActivity) {
        if (self.itemTapBlock) {
            self.itemTapBlock();
        }        
    }
}
- (void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.tapBlock) {
        NSDictionary *dic = self.model.orderActivityVOs[sender.tag];
        self.tapBlock(dic);
    }
    
}


@end

