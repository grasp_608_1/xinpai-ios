//
//  XPShopOrderPruductView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import <UIKit/UIKit.h>
#import "XPShopOrderModel.h"

typedef NS_ENUM(NSUInteger, XPOrderProductListType) {
    XPOrderProductListTypeNone = 0,//单样式,不显示金额
    XPOrderProductListTypeSingle = 1 ,//仅显示线上支持金额
    XPOrderProductListTypeSubTitle = 2,//显示现金支付和账户余额支付
    XPOrderProductListTypeCanSelect = 3,//在XPOrderProductListTypeNone 样式基础上显示可选按钮
};

NS_ASSUME_NONNULL_BEGIN

@interface XPShopOrderPruductView : UIView

@property (nonatomic, copy) void (^selectedBlock)(void);

@property (nonatomic, strong) NSMutableArray *itemsArray;

@property (nonatomic, strong) NSMutableArray *itemsSelectedArray;
//是否显示商品参加的活动和优惠券活动 1 参与优惠活动 0 显示此商品所有的优惠活动
@property (nonatomic, assign) bool showUsedActivityAndCoupon;

- (CGFloat)viewWithModel:(XPShopOrderModel *)model
                viewType:(XPOrderProductListType) type 
              showStatus:(BOOL)isShowStatus;

@end

NS_ASSUME_NONNULL_END
