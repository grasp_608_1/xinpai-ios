//
//  XPShopCouponPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/10.
//

#import <UIKit/UIKit.h>
#import "XPCouponModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCouponPopView : UIView

@property (nonatomic, copy) void(^finishBlock)(XPCouponModel *model);
//1 可点击cell,选中回调
@property (nonatomic, assign) NSInteger canSelected;

- (void)showWithDataArray:(NSArray *)mainArray;

@end

NS_ASSUME_NONNULL_END
