//
//  XPShopActivityPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/15.
//

#import <UIKit/UIKit.h>
#import "XPShopProductDetailModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, XPShopPopShowType) {
    XPShopPopShowTypeActivity,
    XPShopPopShowTypeTransport,
    XPShopPopShowTypeActivities,
};


@interface XPShopActivityPopView : UIView

@property (nonatomic, copy) void(^itemTapBlock)(void);

@property (nonatomic, copy) void (^tapBlock)(NSDictionary *dic);

-(void)viewWithModel:(XPShopProductDetailModel *)model type:(XPShopPopShowType)type;

@end

NS_ASSUME_NONNULL_END
