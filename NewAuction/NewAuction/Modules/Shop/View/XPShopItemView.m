//
//  XPShopItemView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/5.
//

#import "XPShopItemView.h"

@interface XPShopItemView ()

@property (nonatomic, copy) NSArray *mainArray;

@property (nonatomic, strong) UIScrollView *mainView;

@end

@implementation XPShopItemView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
    
    self.mainView = [[UIScrollView alloc] initWithFrame:self.bounds];
    self.mainView.showsHorizontalScrollIndicator = NO;
    [self addSubview:self.mainView];
    
}

-(void)setDataArray:(NSArray *)dataArray {
    self.mainArray = dataArray;
    [self.mainView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat itemW = self.mj_w /5.;
    CGFloat itemH =  74;
    CGFloat TopMargin = 12;
    
    CGFloat right = 0;
    for (int idx = 0; idx< dataArray.count; idx++) {
        NSDictionary *dic = dataArray[idx];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(itemW * idx, TopMargin, itemW, itemH);
        [self.mainView addSubview:button];
        
        UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(button.mj_w/2 - 24, 0, 48, 48)];
        [imageV sd_setImageWithURL:[NSURL URLWithString:dic[@"pic"]]];
        imageV.layer.cornerRadius = 4;
        imageV.layer.masksToBounds = YES;
        [button addSubview:imageV];
        
        UILabel *titleLabel = [UILabel new];
        titleLabel.font = kFontRegular(14);
        titleLabel.textColor = UIColorFromRGB(0x3A3C41);
        titleLabel.frame = CGRectMake(0, itemH - 24, itemW, 24);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = dic[@"name"];
        [button addSubview:titleLabel];
        button.tag = idx;
        [button addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        right = MaxX(button);
    }
    self.mainView.contentSize = CGSizeMake(right + 12, self.mainView.mj_h);
}

-(void)itemButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    NSDictionary *dic = self.mainArray[sender.tag];
    if (self.itemSelectBlock && dic) {
        self.itemSelectBlock(dic);
    }
}
@end
