//
//  XPProductDeclareView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/20.
//

#import "XPProductDeclareView.h"

@interface XPProductDeclareView ()

@property (nonatomic, strong) UIView *mainView;

//标题
@property (nonatomic, strong) UILabel *titleLabel;
//评语
@property (nonatomic, strong) UILabel *contenLabel;
//评价图片
@property (nonatomic, strong) UIView  *commentImageView;

@end

@implementation XPProductDeclareView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12*2, 0);
    mainView.clipsToBounds = YES;
    mainView.layer.cornerRadius = 8;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(14);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(12, 12,self.mainView.mj_w - 24, 24);
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    UILabel *contenLabel = [UILabel new];
    contenLabel.font = kFontRegular(14);
    contenLabel.textColor = app_font_color;
    contenLabel.frame = CGRectMake(12,54,self.mainView.mj_w - 24, 0);
    contenLabel.numberOfLines = NO;
    self.contenLabel = contenLabel;
    [self.mainView addSubview:contenLabel];
    
    self.commentImageView = [[UIView alloc] init];
    self.commentImageView.frame = CGRectMake(12,MaxY(self.contenLabel) + 12,self.mainView.mj_w - 24, 0);
    [self.mainView addSubview:self.commentImageView];
    
}

- (CGFloat)viewWithImageArray:(NSArray *)imageArray content:(NSString *)contentStr TitleStr:(NSString *)titleStr{
    self.contenLabel.text = contentStr;
    CGFloat contentH = [self.contenLabel.text boundingRectWithSize:CGSizeMake(self.contenLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.contenLabel.font} context:nil].size.height;
    if (titleStr == nil || titleStr.length == 0) {
        self.titleLabel.hidden = YES;
        self.contenLabel.frame = CGRectMake(12,16,self.mainView.mj_w - 24, contentH);
    }else{
        self.titleLabel.hidden = NO;
        self.titleLabel.text = titleStr;
        self.contenLabel.frame = CGRectMake(12,MaxY(self.titleLabel) + 12,self.mainView.mj_w - 24, contentH);
        
    }
    [self.commentImageView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSArray *array = imageArray;
    CGFloat margin = 12;
    CGFloat imageWH = (self.mainView.mj_w - 12 *(array.count + 1))/3;
    for (int i = 0; i<array.count; i++) {
        UIImageView *imageView = [UIImageView new];
        imageView.frame = CGRectMake(i* (imageWH + margin), 0, imageWH, imageWH);
        [imageView sd_setImageWithURL:[NSURL URLWithString:array[i]]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.layer.cornerRadius = 4;
        imageView.layer.masksToBounds = YES;
        [self.commentImageView addSubview:imageView];
    }
    
    if (array.count > 0) {
        self.commentImageView.frame = CGRectMake(12,MaxY(self.contenLabel) + 12,self.mainView.mj_w - 24, imageWH);
        self.mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12*2, MaxY(self.commentImageView) + 12);
    }else{
        
        self.commentImageView.frame = CGRectMake(12,MaxY(self.contenLabel) + 12,self.mainView.mj_w - 24, 0);
        self.mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12*2, MaxY(self.contenLabel) + 12 );
        
    }
    return MaxY(self.mainView) + 12;
    
}
@end
