//
//  XPShopCouponCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/10.
//

#import <UIKit/UIKit.h>
#import "XPCouponModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCouponCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@property (nonatomic, strong) XPCouponModel *model;

@property (nonatomic, copy) void(^receiveBlock)(void);
//领取/使用
@property (nonatomic, strong) UIButton *useButton;
// 0 待使用 1 已使用 3 已过期
@property ( nonatomic, assign) NSInteger cellType;

@end

NS_ASSUME_NONNULL_END
