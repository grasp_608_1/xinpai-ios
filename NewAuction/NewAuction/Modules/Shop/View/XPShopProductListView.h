//
//  XPShopProductListView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopProductListView : UIView
//左侧标题是否加粗
@property (nonatomic, assign) bool strokeTitle;
//右侧标题是否加粗
@property (nonatomic, assign) bool strokeValue;
//右侧按钮
@property (nonatomic, strong) NSMutableArray *rightArray;
//左侧按钮
@property (nonatomic, strong) NSMutableArray *leftArray;
@property (nonatomic, copy) void(^arrowBlock)(NSInteger index);
//左右样式
- (CGFloat)viewWithKeyArray:(NSArray *)keyArray ValueArray:(NSArray *)valueArray;
//左右下 样式
- (CGFloat)viewWithKeyArray:(NSArray *)keyArray ValueArray:(NSArray *)valueArray descArray:(NSArray *)descArray;
//左右样式 底部增加两个lable 栏目汇总信息
- (CGFloat)viewWithKeyArray:(NSArray *)keyArray ValueArray:(NSArray *)valueArray bottomInfo:(NSDictionary *) extraInfo;
//包含一个头部标题 可以展开 折叠 fold = yes 折叠 no 展开
- (CGFloat)viewWithKeyArray:(NSArray *)keyArray ValueArray:(NSArray *)valueArray  topTitle:(NSString *)titleStr fold:(bool)fold foldHander:(void(^)(bool fold))foldHandler;
/// 带标签的样式
/// - Parameters:
///   - index: 显示到index 行
///   - type: // 1 活动 2 优惠券
- (void)showCouponViewAtIndex:(NSInteger)index type:(NSInteger)type signArray:(NSArray *)signArray;
//设置第几行可以复制
- (void)canPastAtIndex:(NSInteger)index;
//设置第几行可以设置顶部分割线
- (void)showTopLineAtIndex:(NSInteger )index;
//设置右侧箭头
- (void)showRightArrowAtIndex:(NSInteger) index;
//设置右侧？
- (void)showRightQuestionAtIndex:(NSInteger) index;
@end

NS_ASSUME_NONNULL_END
