//
//  XPThirdCategoryView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPThirdCategoryView : UIView

@property (nonatomic, copy) void(^itemSelectBlock)(NSDictionary *dic);

- (CGFloat)configWithtitle:(NSString *)titleStr dataArray:(NSArray *)dataArray;


@end

NS_ASSUME_NONNULL_END
