//
//  XPShopTeamItemView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/16.
//

#import "XPShopTeamItemView.h"
#import "NSTimer+JKBlocks.h"

@interface XPShopTeamItemView ()

@property (nonatomic, strong) UILabel *timeLabel;
//计时器
@property (nonatomic, strong) NSTimer * oneMinTimer;

@property (nonatomic, copy) NSDictionary *mainDic;

@end

@implementation XPShopTeamItemView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
}

-(void)viewWithDic:(NSDictionary *)dic {
    self.mainDic = dic;
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSArray *iconArray = dic[@"skuActivityGroupUserVOs"];
    CGFloat left = 0;
    CGFloat imageWH = 28;
    NSInteger maxCount = iconArray.count >3 ? 3:iconArray.count;
    for (int i = 0; i<maxCount; i++) {
        UIImageView *imageView = [UIImageView new];
        [imageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(iconArray[i][@"userHead"])] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
        imageView.frame = CGRectMake(12 + 24*i,0,imageWH, imageWH);
        imageView.layer.cornerRadius = 14;
        imageView.layer.masksToBounds = YES;
        imageView.tag = i;
        [self addSubview:imageView];
        
        if (i == maxCount - 1) {
            left = MaxX(imageView);
        }
    }
    
    UILabel *nameLabel = [UILabel new];
    nameLabel.font = kFontRegular(14);
    nameLabel.textColor = app_font_color;
    [self addSubview:nameLabel];
    
    if (iconArray.count > 0) {
        NSDictionary *userDic = iconArray.firstObject;
        nameLabel.text = userDic[@"userNickname"];
    }
    
    CGFloat labelW = [nameLabel.text sizeWithAttributes:@{NSFontAttributeName:nameLabel.font}].width;
    nameLabel.frame = CGRectMake(left + 4, 0, labelW, self.mj_h);
    
    UIButton *joinButton = [UIButton buttonWithType:UIButtonTypeCustom];
    joinButton.frame = CGRectMake(self.mj_w - 12 - 68, 2, 68, 24);
    joinButton.backgroundColor = UIColorFromRGB(0xFF3EB2);
    joinButton.layer.cornerRadius = 12;
    [joinButton setTitle:@"即刻参团" forState:UIControlStateNormal];
    [joinButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    joinButton.titleLabel.font = kFontRegular(14);
    [joinButton addTarget:self action:@selector(joinButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:joinButton];
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = kFontRegular(14);
    timeLabel.textColor = app_font_color;
    timeLabel.tag = 99;
    self.timeLabel = timeLabel;
    [self addSubview:timeLabel];
    
    timeLabel.text = @"剩余 00:00:00";
    CGFloat timeW = [timeLabel.text sizeWithAttributes:@{NSFontAttributeName:nameLabel.font}].width;
    timeLabel.frame = CGRectMake(self.mj_w - 83 - timeW, 0, timeW, self.mj_h);
    
    NSNumber *countNum = dic[@"remainingTimeLong"];
    [self countDownWithTimer:countNum.integerValue];
    
}

- (void)joinButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.joinBlock) {
        self.joinBlock(self.mainDic);
    }
}

#pragma mark -- 倒计时
-(void)countDownWithTimer:(NSInteger)time{
    
    XPWeakSelf
    __block NSInteger i = time;
        //一分钟倒计时
    self.oneMinTimer = [NSTimer jk_timerWithTimeInterval:1 block:^{
        if (i> 0) {
            weakSelf.timeLabel.text = [NSString stringWithFormat:@"%@",[weakSelf getTimeWithSecond:i]];
            i--;
        }else {
            //计时器停止
            [weakSelf.oneMinTimer invalidate];
            weakSelf.oneMinTimer = nil;
            //block
            weakSelf.timeLabel.text = @"剩余 00:00:00";
        }
    } repeats:YES];
    
    [[NSRunLoop mainRunLoop]addTimer:self.oneMinTimer forMode:NSRunLoopCommonModes];
    [self.oneMinTimer fire];
}

- (NSString *)getTimeWithSecond:(NSInteger )seconds{
    NSString *hour = [NSString stringWithFormat:@"%02ld",seconds/3600];
    NSString *min = [NSString stringWithFormat:@"%02ld",(seconds%3600)/60];
    NSString *sec = [NSString stringWithFormat:@"%02ld",seconds%60];
    return [NSString stringWithFormat:@"剩余%@:%@:%@",hour,min,sec];
}

@end
