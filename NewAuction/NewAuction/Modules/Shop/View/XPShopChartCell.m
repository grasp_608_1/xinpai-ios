//
//  XPShopChartCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/18.
//

#import "XPShopChartCell.h"

@interface XPShopChartCell ()

@property (nonatomic, strong) UIView *mainView;
//排行图
@property (nonatomic, strong) UIImageView *numImageView;
//产品图片
@property (nonatomic, strong) UIImageView *pImageView;
//产品名称
@property (nonatomic, strong) UILabel *pTitleLabel;
//标签
@property (nonatomic, strong) UIScrollView *signView;
//价格
@property (nonatomic, strong) UILabel *label1;
//右侧按钮
@property (nonatomic, strong) UIButton *rightButton;

@end

@implementation XPShopChartCell

/// cell 初始化方法
+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPShopChartCell";
    XPShopChartCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPShopChartCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColor.clearColor;
        [self setupViews];
    }
    return self;
}

-(void)setupViews {
    
    self.mainView = [UIView new];
    self.mainView.layer.cornerRadius = 4;
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(12, 16, SCREEN_WIDTH - 12 * 2, 140);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:self.mainView];

    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(0, 0, 140, 140);
    [self.mainView addSubview:_pImageView];
    
    self.numImageView = [[UIImageView alloc] init];
    self.numImageView.frame = CGRectMake(20, 14, 32, 29);
    [self addSubview:self.numImageView];
    
    CGFloat LabelX = MaxX(self.pImageView) + 8;
    CGFloat LabelW = self.mainView.frame.size.width - LabelX  - 8;
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontBold(16);
    self.pTitleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.pTitleLabel.frame = CGRectMake(LabelX, 15, LabelW, 24);
    self.pTitleLabel.numberOfLines = 2;
    [self.mainView addSubview:_pTitleLabel];
    
    self.signView = [[UIScrollView alloc] init];
    self.signView.frame = CGRectMake(12,0,self.mj_w -12,16);
    [self addSubview:self.signView];
    
    self.label1 = [UILabel new];
    self.label1.textColor = UIColorFromRGB(0xD90003);
    self.label1.font = kFontRegular(18);
    [self.mainView addSubview:self.label1];
   
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.backgroundColor = UIColorFromRGB(0xEDCA99);
    self.rightButton.frame = CGRectMake(self.mainView.bounds.size.width - 8 - 68, 140 - 24 - 8, 68, 24);
    [self.rightButton setTitle:@"立即购买" forState:UIControlStateNormal];
    [self.rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    self.rightButton.titleLabel.font = kFontMedium(14);
    self.rightButton.layer.cornerRadius = 12;
    self.rightButton.layer.masksToBounds = YES;
    [self.mainView addSubview:self.rightButton];
    
    [self.rightButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :self.rightButton colors:
                                     @[(__bridge id)UIColorFromRGB(0xEDCA99).CGColor,
                                       (__bridge id)UIColorFromRGB(0xB17B59).CGColor]] atIndex:0];
    
}

- (void)cellWithModel:(XPShopModel *)model Index:(NSInteger)index{
    CGFloat top = 0;
    CGFloat LabelX = MaxX(self.pImageView) + 8;
    CGFloat LabelW = self.mainView.frame.size.width - LabelX  - 8;
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] init];
    if (model.presaleStatus.intValue == 1) {//预售
        NSTextAttachment *attch = [[NSTextAttachment alloc] init];
        attch.image = [UIImage imageNamed:@"xp_circle_preSale"];
        attch.bounds = CGRectMake(0, -2, 32,16);
        NSAttributedString *signAttr = [NSAttributedString attributedStringWithAttachment:attch];
        [attr appendAttributedString:signAttr];
    }else if (model.groupStatus.intValue == 1) {//拼团
        NSTextAttachment *attch = [[NSTextAttachment alloc] init];
        attch.image = [UIImage imageNamed:@"xp_circle_team"];
        attch.bounds = CGRectMake(0, -2, 32,16);
        NSAttributedString *signAttr = [NSAttributedString attributedStringWithAttachment:attch];
        [attr appendAttributedString:signAttr];
    }
    NSAttributedString *titleStr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",model.skuName]];
    [attr appendAttributedString:titleStr];
    
    CGFloat titleH = [XPCommonTool calculateHeightForAttributedString:attr withWidth:LabelW];
    self.pTitleLabel.frame = CGRectMake(LabelX, 8, LabelW, titleH > 40 ? 40:titleH);
    self.pTitleLabel.attributedText = attr;
    [self.pTitleLabel sizeToFit];
    
    top = MaxY(self.pTitleLabel);
    
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];

    self.numImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"xp_score_chart_%zd",index]];
    
    
    [self.signView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSMutableArray *titleArray = [NSMutableArray array];
    if (model.activityNote.length > 0) {
        [titleArray addObject:model.activityNote];
    }
//    if (model.freightNote.length > 0) {
//        [titleArray addObject:model.freightNote];
//    }
    
    CGFloat left = 0;
    for (int i = 0; i<titleArray.count; i++) {
        NSString *str = titleArray[i];
        UILabel *label = [UILabel new];
        label.text = str;
        label.font = kFontMedium(10);
        if (i == 0) {
            label.textColor = UIColorFromRGB(0xF55B19);
        }else {
            label.textColor = UIColorFromRGB(0xFF3F6D);
        }
        [self.signView addSubview:label];
        CGFloat labelW = [str sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
        label.frame = CGRectMake(left,0,labelW,self.signView.mj_h);
        left = MaxX(label) + 4;
    }
    
    if (titleArray.count > 0) {
        top = MaxY(self.signView);
        self.signView.frame = CGRectMake(LabelX,top + 8,LabelW,16);
    }
    self.label1.frame = CGRectMake(LabelX, top + 12, LabelW, 24);
    self.label1.text = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceSale]];

}
@end
