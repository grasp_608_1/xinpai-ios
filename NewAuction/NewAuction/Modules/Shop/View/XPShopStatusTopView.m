//
//  XPShopStatusTopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import "XPShopStatusTopView.h"
#import "NSTimer+JKBlocks.h"
#import "XPShopPreSaleModel.h"

@interface XPShopStatusTopView ()
//小汽车🚗
@property (nonatomic, strong) UIImageView *carLogo;
//闹钟⏰
@property (nonatomic, strong) UIImageView *clockLogo;
//状态view
@property (nonatomic, strong) UILabel *statusLabel;
//右侧箭头→
@property (nonatomic, strong) UIImageView *arrowImageView;
//状态描述
@property (nonatomic, strong) UILabel *statusTipLabel;

//计时器
@property (nonatomic,strong) NSTimer * oneMinTimer;
//1 待付款  2 已完成  3 已取消  4 待收货
@property (nonatomic, assign) NSInteger type;

@property (nonatomic, copy) void(^completeBlock)(void);

@end

@implementation XPShopStatusTopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    UIImageView *carLogo = [UIImageView new];
    carLogo = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, 48, 48)];
    carLogo.image = [UIImage imageNamed:@"xp_car"];
    carLogo.hidden = YES;
    self.carLogo = carLogo;
    [self addSubview:carLogo];
    
    UIImageView *clockLogo = [UIImageView new];
    clockLogo = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, 48, 48)];
    clockLogo.image = [UIImage imageNamed:@"xp_clock_gray"];
    clockLogo.hidden = YES;
    self.clockLogo = clockLogo;
    [self addSubview:clockLogo];
    
    
    UILabel *statusLabel = [UILabel new];
    statusLabel.textColor = UIColorFromRGB(0x3A3C41);
    statusLabel.font = kFontMedium(16);
    statusLabel.frame = CGRectMake(MaxX(carLogo) + 12, 12, self.mj_w - MaxX(carLogo) - 12 - 12 - 24 - 5, 24);
    self.statusLabel = statusLabel;
    [self addSubview:statusLabel];
    
    UILabel *statusTipLabel = [UILabel new];
    statusTipLabel.textColor = UIColorFromRGB(0x8A8A8A);
    statusTipLabel.font = kFontRegular(14);
    self.statusTipLabel = statusTipLabel;
    [self addSubview:statusTipLabel];
    
    UIImageView *arrowImageView = [UIImageView new];
    arrowImageView.image = [UIImage imageNamed:@"xp_setting_arrow"];
    arrowImageView.frame = CGRectMake(self.mj_w - 12 -24, 24, 24, 24);
    self.arrowImageView = arrowImageView;
    [self addSubview:arrowImageView];
    
    
}

- (void)viewWithShopOrderModel:(XPShopOrderModel *)model countDownBlock:(nullable void (^)(void))completeBlock {
    self.completeBlock = completeBlock;
    NSInteger status = model.status.integerValue;
    self.type = status;
    NSDictionary *dict = model.appKuaiDi100FindOrderDTO;
    
    [self.oneMinTimer invalidate];
    self.oneMinTimer = nil;
    ////订单状态 0 默认 1 待付款 2 已支付|待发货 3 待收货|已发货 4 已完成 5 已取消 6 已关闭
    if (status == 1) {//待付款
        if (model.presaleStatus.intValue == 1) {//预售
            self.carLogo.hidden = YES;
            self.arrowImageView.hidden = YES;
            self.clockLogo.hidden = YES;
            self.statusLabel.frame = CGRectMake(0 , 12, self.mj_w, 24);
            self.clockLogo.frame = CGRectMake(self.statusLabel.mj_x - 30, 14, 20, 20);
            self.statusTipLabel.frame = CGRectMake(0, MaxY(self.statusLabel) + 2, self.mj_w, 24);
            self.statusTipLabel.textAlignment = NSTextAlignmentCenter;
            self.statusLabel.textAlignment = NSTextAlignmentCenter;
            
            ///预售-支付状态 0 未支付 1 已付定金未支付尾款(到达尾款支付时间) 2 已付尾款 10 已付定金未支付尾款(未到达尾款支付时间)
            if (model.presalePayStatus.intValue == 0) {
                self.statusLabel.text = @"等待支付定金";
                self.statusTipLabel.text = @"商品已为您准备好,请及时付款";
                //暂时不使用倒计时功能
//                [self countDownWithTime:10];
            }else if (model.presalePayStatus.intValue == 1) {
                self.statusLabel.text = @"待支付尾款";
                self.statusTipLabel.text = @"等待买家支付尾款";
            }else if (model.presalePayStatus.intValue == 10) {
                self.statusLabel.text = @"定金已支付";
                self.statusTipLabel.text = [NSString stringWithFormat:@"%@ 开始支付尾款",model.presaleBalanceStartTime];
            }
            
        }else{
            self.carLogo.hidden = YES;
            self.arrowImageView.hidden = YES;
            self.clockLogo.hidden = NO;
            self.statusLabel.text = @"等待付款";
            self.statusTipLabel.text = @"商品已为您准备好,请及时付款";
            self.statusLabel.frame = CGRectMake((self.mj_w - 96)/2 + 30 , 12, 66, 24);
            self.clockLogo.frame = CGRectMake(self.statusLabel.mj_x - 30, 14, 20, 20);
            self.statusTipLabel.frame = CGRectMake(0, MaxY(self.statusLabel) + 2, self.mj_w, 24);
    //        [self countDownWithTime:timeInterval];
            self.statusTipLabel.textAlignment = NSTextAlignmentCenter;
            self.statusLabel.textAlignment = NSTextAlignmentLeft;
        }
    
    }else if(status == 4 ){//已完成
        self.carLogo.hidden = YES;
        self.arrowImageView.hidden = NO;
        self.clockLogo.hidden = YES;

        self.statusLabel.text = dict[@"stateText"];
        self.statusTipLabel.text = dict[@"stateDesc"];
        self.statusLabel.frame = CGRectMake(12, 12, self.mj_w - 12 - 24 - 5, 24);
        self.statusTipLabel.frame = CGRectMake(self.statusLabel.mj_x, MaxY(self.statusLabel) + 2, self.self.statusLabel.mj_w, 24);
        self.statusTipLabel.textAlignment = NSTextAlignmentLeft;
        self.statusLabel.textAlignment = NSTextAlignmentLeft;
    }else if(status == 5 ||status == 6){//已取消
        self.carLogo.hidden = YES;
        self.arrowImageView.hidden = NO;
        self.clockLogo.hidden = YES;
        if (status == 5) {
            self.statusLabel.text = @"已取消";
            self.statusTipLabel.text = @"取消原因: 手动取消该订单";
        }else {
            self.statusLabel.text = @"已关闭";
            self.statusTipLabel.text = @"关闭原因: 订单售后处理完成";
        }
        
        self.statusLabel.frame = CGRectMake( 12, 12, self.mj_w - 12 - 24 - 5, 24);
        
        self.statusTipLabel.frame = CGRectMake(self.statusLabel.mj_x, MaxY(self.statusLabel) + 2, self.self.statusLabel.mj_w, 24);
        self.statusTipLabel.textAlignment = NSTextAlignmentLeft;
        self.statusLabel.textAlignment = NSTextAlignmentLeft;
        
    }else if(status == 2 || status == 3){//待收货
        self.carLogo.hidden = YES;
        self.arrowImageView.hidden = NO;
        self.clockLogo.hidden = YES;

        self.statusLabel.text = dict[@"stateText"];
        self.statusTipLabel.text = dict[@"stateDesc"];
        self.statusLabel.frame = CGRectMake(12, 12, self.mj_w - 12 - 24 - 5, 24);
        self.statusTipLabel.frame = CGRectMake(self.statusLabel.mj_x, MaxY(self.statusLabel) + 2, self.self.statusLabel.mj_w, 24);
        self.statusTipLabel.textAlignment = NSTextAlignmentLeft;
        self.statusLabel.textAlignment = NSTextAlignmentLeft;
    }
}

#pragma mark -- 倒计时
-(void)countDownWithTime:(NSTimeInterval)time{
    
    if (time<= 0) {
        return;
    }
    XPWeakSelf
    __block int i = time;
    
    self.oneMinTimer = [NSTimer jk_timerWithTimeInterval:1 block:^{
        if (i >= 0) {
            
            self.statusTipLabel.text = [NSString stringWithFormat:@"%@后订单关闭",[self getTimeWithSecond:i]];
            i--;
        } else{
            [weakSelf.oneMinTimer invalidate];
            weakSelf.oneMinTimer = nil;
            //取消状态
            self.type = 1;
            if(self.completeBlock){
                self.completeBlock();
            }
        }
    } repeats:YES];
    [[NSRunLoop mainRunLoop]addTimer:self.oneMinTimer forMode:NSRunLoopCommonModes];
    [self.oneMinTimer fire];
}

- (NSString *)getTimeWithSecond:(NSInteger )seconds{
    NSString *hour = [NSString stringWithFormat:@"%02ld",seconds/3600];
    NSString *min = [NSString stringWithFormat:@"%02ld",(seconds%3600)/60];
    NSString *sec = [NSString stringWithFormat:@"%02ld",seconds%60];
    return [NSString stringWithFormat:@"%@时%@分%@秒",hour,min,sec];
}

@end
