//
//  XPPreSaleStatusDetailView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/17.
//

#import "XPPreSaleStatusDetailView.h"
#import "XPDoubleLabelView.h"
#import "XPShopPreSaleModel.h"

@interface  XPPreSaleStatusDetailView()
//定金
@property (nonatomic, strong) UIImageView *topImageView;
@property (nonatomic, strong) UILabel *topLabel;
//定金金额
@property (nonatomic, strong) XPDoubleLabelView *topDescView;
//尾款
@property (nonatomic, strong) UIImageView *downImageView;
@property (nonatomic, strong) UILabel *downLabel;

@property (nonatomic, strong) UIView *remainMoneyView;
//尾款金额
@property (nonatomic, strong) XPDoubleLabelView *downDescView;
//尾款描述
@property (nonatomic, strong) UILabel *descLabel;

@end

@implementation XPPreSaleStatusDetailView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    self.clipsToBounds = YES;
    
    self.topImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 12, 9, 17)];
    [self addSubview:self.topImageView];
    
    self.topLabel = [UILabel new];
    self.topLabel.font = kFontRegular(14);
    self.topLabel.textColor = app_font_color_8A;
    self.topLabel.frame = CGRectMake(25, 4, self.mj_w - 25, 24);
    [self addSubview:self.topLabel];
    
    self.topDescView = [[XPDoubleLabelView alloc] initWithFrame:CGRectMake(30, 40, self.mj_w - 30 - 12, 40)];
    self.topDescView.leftLabel.font = kFontRegular(14);
    self.topDescView.righLabel.font = kFontMedium(16);
    self.topDescView.righLabel.textColor = app_font_color;
    self.topDescView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.topDescView.layer.cornerRadius = 4;
    [self addSubview:self.topDescView];
    
    
    self.downImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 92, 9, 17)];
    [self addSubview:self.downImageView];
    
    self.downLabel = [UILabel new];
    self.downLabel.font = kFontRegular(14);
    self.downLabel.textColor = app_font_color_8A;
    self.downLabel.frame = CGRectMake(25, 92, self.mj_w - 25, 24);
    [self addSubview:self.downLabel];
    
    self.remainMoneyView = [[UIView alloc] initWithFrame:CGRectMake(30, MaxY(self.downLabel) + 8, self.mj_w - 30 - 12, 0)];
    self.remainMoneyView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.remainMoneyView.clipsToBounds = YES;
    self.remainMoneyView.layer.cornerRadius = 4;
    [self addSubview:self.remainMoneyView];
    
    self.downDescView = [[XPDoubleLabelView alloc] initWithFrame:CGRectMake(0, 8, self.remainMoneyView.mj_w, 24)];
    self.downDescView.leftLabel.font = kFontRegular(14);
    self.downDescView.righLabel.font = kFontRegular(12);
    [self.remainMoneyView addSubview:self.downDescView];
    
    self.descLabel = [UILabel new];
    self.descLabel.font = kFontRegular(14);
    self.descLabel.textColor = app_font_color_8A;
    self.descLabel.numberOfLines = NO;
    [self.remainMoneyView addSubview:self.descLabel];
    
}

-(CGFloat)viewWithDic:(NSDictionary *)dic {

    XPShopPreSaleModel *model = [XPShopPreSaleModel mj_objectWithKeyValues:dic];

    //预售-支付状态 0 未支付 1 已付定金未支付尾款(到达尾款支付时间) 2 已付尾款 10 已付定金未支付尾款(未到达尾款支付时间)
    if (model.presalePayStatus.intValue == 0) {
        self.topLabel.textColor = UIColorFromRGB(0x6E6BFF);
        self.downLabel.textColor = app_font_color;
        self.topLabel.text = @"定金  等待买家付款";
        
        self.topImageView.image = [UIImage imageNamed:@"group_blue_down"];
        self.downImageView.image = [UIImage imageNamed:@"group_gray_up"];
        self.downLabel.text = [NSString stringWithFormat:@"尾款 %@开始支付尾款",model.presaleBalanceStartTime];
    }else if (model.presalePayStatus.intValue == 1){
        self.topLabel.textColor = app_font_color;
        self.downLabel.textColor = UIColorFromRGB(0x6E6BFF);
        self.topLabel.text = @"定金  已支付";
        
        self.topImageView.image = [UIImage imageNamed:@"group_gray_down"];
        self.downImageView.image = [UIImage imageNamed:@"group_blue_up"];
        self.downLabel.text = [NSString stringWithFormat:@"尾款 等待买家付款"];
    }else if (model.presalePayStatus.intValue == 2){
        self.topLabel.textColor = app_font_color        ;
        self.downLabel.textColor = UIColorFromRGB(0x6E6BFF);
        self.topLabel.text = @"定金  已支付";
        
        self.topImageView.image = [UIImage imageNamed:@"group_gray_down"];
        self.downImageView.image = [UIImage imageNamed:@"group_blue_up"];
        self.downLabel.text = @"尾款 已支付";
    }else if (model.presalePayStatus.intValue == 10){
        self.topLabel.textColor = app_font_color        ;
        self.downLabel.textColor = UIColorFromRGB(0x6E6BFF);
        self.topLabel.text = @"定金  已支付";
        
        self.topImageView.image = [UIImage imageNamed:@"group_gray_down"];
        self.downImageView.image = [UIImage imageNamed:@"group_blue_up"];
        self.downLabel.text = [NSString stringWithFormat:@"尾款 %@开始支付尾款",model.presaleBalanceStartTime];
    }
    
    self.topDescView.leftStr = @"商品定金";
    self.topDescView.rightStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.presaleDepositPrice]];
    self.downDescView.leftStr = @"商品尾款";
    NSNumber *remainMoneyNum = model.presaleBalancePrice;
    
    NSString *moneyStr = [XPMoneyFormatTool formatMoneyWithNum:remainMoneyNum];
    NSString *totalStr = [NSString stringWithFormat:@"￥%@",moneyStr];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:app_font_color} range:NSMakeRange(totalStr.length - moneyStr.length, moneyStr.length)];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(12),NSForegroundColorAttributeName:app_font_color} range:NSMakeRange(0, 1)];
    self.downDescView.righLabel.attributedText = attr;
    
    self.descLabel.text = model.presaleBalanceDescribe;
    CGFloat descH = [self.descLabel.text  boundingRectWithSize:CGSizeMake(self.remainMoneyView.mj_w - 24, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.descLabel.font} context:nil].size.height;
    self.descLabel.frame = CGRectMake(12, MaxY(self.downDescView) + 12, self.remainMoneyView.mj_w - 24, descH);
    self.remainMoneyView.frame = CGRectMake(30, MaxY(self.downLabel) + 8, self.mj_w - 30 - 12, MaxY(self.descLabel) + 12);
    
    return MaxY(self.remainMoneyView) + 12;
}

@end
