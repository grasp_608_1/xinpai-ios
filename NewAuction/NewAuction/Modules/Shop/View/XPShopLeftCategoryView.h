//
//  XPShopLeftCategoryView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^ActionBlock)(NSInteger index);

@interface XPShopLeftCategoryView : UIScrollView

@property (nonatomic, copy) ActionBlock tapBlock;

- (void)configWithtitleArray:(NSArray *)titleArray;

@end

NS_ASSUME_NONNULL_END
