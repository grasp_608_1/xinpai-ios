//
//  XPShopRightCategoryView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/7.
//

#import "XPShopRightCategoryView.h"
#import "XPThirdCategoryView.h"

@implementation XPShopRightCategoryView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
}

- (void)setDataArray:(NSArray *)dataArray{
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat bottom = 0;
    for (int i= 0; i<dataArray.count; i++) {
        
        NSDictionary *dic = dataArray[i];
        XPThirdCategoryView *itemView = [[XPThirdCategoryView alloc] initWithFrame:CGRectMake(0, bottom, self.mj_w, 0)];
        CGFloat itemH  = [itemView configWithtitle:dic[@"name"] dataArray:dic[@"children"]];
        itemView.frame = CGRectMake(0, bottom, self.mj_w, itemH);
        [self addSubview:itemView];
        bottom = MaxY(itemView) + 12;
        
        [itemView setItemSelectBlock:self.itemSelectBlock];
        
    }
    self.contentSize = CGSizeMake(self.mj_w, bottom + 12);
}


@end
