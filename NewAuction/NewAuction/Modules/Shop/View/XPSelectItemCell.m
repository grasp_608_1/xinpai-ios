//
//  XPSelectItemCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/22.
//

#import "XPSelectItemCell.h"

@interface XPSelectItemCell ()

@property (nonatomic, strong) UILabel *scoreLabel;


@end

@implementation XPSelectItemCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPSelectItemCell";
    XPSelectItemCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPSelectItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    self.scoreLabel = [UILabel new];
    self.scoreLabel.font = kFontMedium(14);
    self.scoreLabel.textColor  = app_font_color;
    self.scoreLabel.frame = CGRectMake(24, 0, SCREEN_WIDTH - 24* 2 - 20, 40);
    [self.contentView addSubview:self.scoreLabel];
    
    self.dotButton = [[XPHitButton alloc] init];
    [self.dotButton setImage:[UIImage imageNamed:@"xp_xbox_unselect"] forState:UIControlStateNormal];
    [self.dotButton setImage:[UIImage imageNamed:@"xp_xbox_selected"] forState:UIControlStateSelected];
    self.dotButton.hitRect = 40;
    self.dotButton.frame = CGRectMake(MaxX(self.scoreLabel), 10,20, 20);
    [self.dotButton addTarget:self action:@selector(dotButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.dotButton];
    
}
//数据源
-(void)setModel:(XPShopOrderConfirmModel *)model {
    _model = model;
    NSString *scoreStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.pointsAmount]];
    NSString *totalStr = [NSString stringWithFormat:@"抵扣%@最多可使用%@积分",scoreStr,model.pointsUse];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
    [attr addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(2, scoreStr.length)];
    
    self.scoreLabel.attributedText = attr;
    if (model.scoreSelected) {
        self.dotButton.selected = YES;
    }else {
        self.dotButton.selected = NO;
    }
    
}

-(void)dotButtonClicked:(UIButton *)sender {
    self.dotButton.selected = !self.dotButton.selected;
    self.model.scoreSelected = self.dotButton.selected;
    
}

@end
