//
//  XPInputTransView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/17.
//

#import <UIKit/UIKit.h>
#import "XPShopAfterSaleModel.h"
#import "SZTextView.h"
#import "XPHitButton.h"
#import "XPLogisticsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPInputTransView : UIView

//快递公司
@property (nonatomic, strong) UITextField *companyTextField;
//快递单号
@property (nonatomic, strong) UITextField *orderTextField;
//寄出地址
@property (nonatomic, strong) SZTextView *locationTextView;
//寄到公司的地址
@property (nonatomic, strong) UILabel *companyLocationLabel;
//物流
@property (nonatomic, strong) XPLogisticsModel *logisticsModel;


@property (nonatomic, copy) void(^arrowBlock)(void);

@property (nonatomic, copy) void(^submitBlock)(XPLogisticsModel *model,NSString *addressStr,NSString *expressSn);

- (CGFloat)viewWithModel:(XPShopAfterSaleModel *)model;

@end

NS_ASSUME_NONNULL_END
