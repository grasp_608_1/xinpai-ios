//
//  XPShopTeamPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/16.
//

#import "XPShopTeamPopView.h"
#import "XPHitButton.h"
#import "XPShopTeamItemView.h"
#import "XPShopGroupModel.h"

@interface XPShopTeamPopView ()

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIScrollView *mainView;
@property (nonatomic, strong) UIScrollView *contentView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//小标题
@property (nonatomic, strong) UILabel *titleDescLabel;
@property (nonatomic, strong) NSMutableArray *teamArray;
@end

@implementation XPShopTeamPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    bgView.alpha = 0;
    self.bgView = bgView;
    [self addSubview:bgView];
    
    self.mainView = [UIScrollView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h, SCREEN_WIDTH, 0.8 *self.mj_h);
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontRegular(14);
    titleLabel.textColor = app_font_color_8A;
    titleLabel.frame = CGRectMake(12, 8,self.mj_w, 24);
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    UILabel *titleDescLabel = [UILabel new];
    titleDescLabel.font = kFontRegular(14);
    titleDescLabel.textColor = UIColorFromRGB(0xFF3F6D);
    titleDescLabel.frame = CGRectMake(12, MaxY(self.titleLabel),self.mj_w, 24);
    self.titleDescLabel = titleDescLabel;
    [self.mainView addSubview:titleDescLabel];
    
    UIScrollView *contentView =  [[UIScrollView alloc] initWithFrame:CGRectMake(12, 52, self.mainView.mj_w - 24, self.mainView.mj_h)];
    contentView.backgroundColor = UIColor.whiteColor;
    contentView.layer.cornerRadius = 8;
    self.contentView = contentView;
    [self.mainView addSubview:contentView];
    
}

- (void)viewWithDataModel:(XPShopProductDetailModel *)model{
    
    XPShopGroupModel *groupModel = [XPShopGroupModel mj_objectWithKeyValues:model.productSkuShortInfoVO[@"skuGroupVO"]];
    
    self.teamArray = groupModel.skuActivityGroupVOs.mutableCopy;
    
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:@"可参与的拼团 仅显示100个可参与的拼单"];
    [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color,NSFontAttributeName:kFontMedium(16)} range:NSMakeRange(0, 6)];
    self.titleLabel.attributedText = attr;
    self.titleDescLabel.text = [NSString stringWithFormat:@"%@人正在抢，现在参与即可抢到",groupModel.groupPeopleNumberTotal];
    XPWeakSelf
    CGFloat top = 12;
    for (int i = 0; i < self.teamArray.count; i++) {
        NSDictionary *tempDic = self.teamArray[i];
        XPShopTeamItemView *item = [[XPShopTeamItemView alloc] initWithFrame:CGRectMake(0, top + 12, self.contentView.mj_w, 28)];
        [self.contentView addSubview:item];
        [item viewWithDic:tempDic];
        top = MaxY(item);
        
        [item setJoinBlock:^(NSDictionary * _Nonnull dic) {
            
            [UIView animateWithDuration:0.2 animations:^{
                self.alpha = 0;
            }completion:^(BOOL finished) {
                if (weakSelf.arrowBlock) {
                    weakSelf.arrowBlock(dic);
                }
            }];
            
        }];
        
    }
    
    CGFloat contentMaxH = self.mainView.mj_h - 64 - 12;
    if (top > contentMaxH) {
        self.contentView.frame = CGRectMake(12, 64, self.mainView.mj_w -24 , contentMaxH + 12);
        self.contentView.contentSize = CGSizeMake(self.contentView.mj_w, top + 12);
    }else {
        self.contentView.frame = CGRectMake(12, 64, self.mainView.mj_w - 24 , top + 12);
        self.contentView.contentSize = CGSizeMake(self.contentView.mj_w, top + 12);
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bgView.alpha = 1;
        self.mainView.frame = CGRectMake(0, self.mj_h * 0.2, SCREEN_WIDTH, 0.8 *self.mj_h);
    }];
}

- (void)cancelButtonClicked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    
    [self removeFromSuperview];
}
@end
