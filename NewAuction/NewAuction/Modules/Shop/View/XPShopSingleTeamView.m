//
//  XPShopSingleTeamView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/13.
//

#import "XPShopSingleTeamView.h"
#import "NSTimer+JKBlocks.h"
#import "XPWeakTimer.h"
#import "XPSingleTeamPopView.h"

@interface XPShopSingleTeamView ()
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//分割线
@property (nonatomic, strong) UIView *sepLine;

@property (nonatomic, strong) UIView *mainView;
//倒计时
@property (nonatomic, strong) UILabel *timeLabel;
//计时器
@property (nonatomic,strong) XPWeakTimer * oneMinTimer;
@property (nonatomic, assign) NSInteger pastTime;
@property (nonatomic, assign) NSInteger remainTime;
@property (nonatomic, copy) NSDictionary *mainDic;

@end


@implementation XPShopSingleTeamView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    self.clipsToBounds = YES;
    self.oneMinTimer = [XPWeakTimer shareTimer];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.textColor = app_font_color;
    self.titleLabel.font = kFontMedium(14);
    self.titleLabel.frame = CGRectMake(12, 8, self.mj_w - 12, 24);
    [self addSubview:self.titleLabel];
    
    UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.mj_w - 30, 8, 24, 24)];
    arrowImageView.image = [UIImage imageNamed:@"Frame_7"];
    arrowImageView.userInteractionEnabled = YES;
    [self addSubview:arrowImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [arrowImageView addGestureRecognizer:tap];
    
    self.sepLine = [[UIView alloc] init];
    self.sepLine.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.sepLine.frame = CGRectMake(0, 40, self.mj_w, 1);
    [self addSubview:self.sepLine];
    
    self.mainView = [UIView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, 40, self.mj_w, 56);
    [self addSubview:self.mainView];
    
}

-(void)viewWithDic:(NSDictionary *)dic Type:(XPShopTeamType)type{
    
    [self.mainView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    _mainDic = dic;
    NSArray *skuTeamArray = dic[@"skuActivityGroupVOs"];
    NSDictionary *firstTeamDic = skuTeamArray.firstObject;
    
    NSArray *leaderArray = skuTeamArray.firstObject[@"skuActivityGroupUserVOs"];
    NSDictionary *leaderDic = leaderArray.firstObject;
    
    NSNumber *totalNum = firstTeamDic[@"groupPeopleNumber"];
    NSNumber *currentNum = firstTeamDic[@"groupPresentNumber"];
    NSNumber *groupStatus = firstTeamDic[@"status"];
    
    NSString *needStr = [NSString stringWithFormat:@"还需%d人即可成团",totalNum.intValue - currentNum.intValue];
    NSString *totalStr = [NSString stringWithFormat:@"%@的团 %@",leaderDic[@"userNickname"],needStr];
    
    if (type == XPShopTeamTypeNormal || groupStatus.intValue == 1) {
        
        self.titleLabel.text = [NSString stringWithFormat:@"%@的团",leaderDic[@"userNickname"]];
    }else if (type == XPShopTeamTypeWait || type == XPShopTeamTypeConfirm ||type == XPShopTeamTypeCreate){
        
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color_8A,NSFontAttributeName:kFontRegular(14)} range:NSMakeRange(totalStr.length - needStr.length, needStr.length)];
        self.titleLabel.attributedText = attr;
    }
    
    CGFloat left = 0;
    CGFloat imageWH = 28;
    NSInteger maxCount = leaderArray.count >3 ? 3:leaderArray.count;
    
    for (int i = 0 ; i<maxCount; i++) {
        UIImageView *imageView = [UIImageView new];
        [imageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(leaderArray[i][@"userHead"])] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
        imageView.frame = CGRectMake(12 + 24*i,12,imageWH, imageWH);
        imageView.layer.cornerRadius = 14;
        imageView.layer.masksToBounds = YES;
        imageView.tag = i;
        [self.mainView addSubview:imageView];
        
        if (i == maxCount - 1) {
            left = MaxX(imageView);
        }
    }
    
    NSMutableArray *tempArray = [NSMutableArray array];
    for (int j = 0; j<(leaderArray.count > 3 ? 3 : leaderArray.count); j++) {
        NSDictionary *dic = leaderArray[j];
        [tempArray addObject:dic[@"userNickname"]];
    }
    
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.font = kFontRegular(14);
    nameLabel.textColor = app_font_color_8A;
    [self.mainView addSubview:nameLabel];
    nameLabel.frame = CGRectMake(left + 4, 12, self.mj_w - 12 - left - 4, 28);
    nameLabel.text = [tempArray componentsJoinedByString:@""];
    
    if (type == XPShopTeamTypeWait) {
        UILabel *timeLabel = [UILabel new];
        timeLabel.font = kFontRegular(14);
        timeLabel.textColor = app_font_color;
        timeLabel.tag = 99;
        self.timeLabel = timeLabel;
        [self.mainView addSubview:timeLabel];
        
        NSNumber *remainTimeNun = firstTeamDic[@"remainingTimeLong"];
        NSInteger remainTime = remainTimeNun.integerValue;
        
        timeLabel.text = [self getTimeWithSecond:remainTime];
        CGFloat timeW = [timeLabel.text sizeWithAttributes:@{NSFontAttributeName:nameLabel.font}].width;
        timeLabel.frame = CGRectMake(self.mainView.mj_w - 12 - timeW, 12, timeW, 28);
        if (remainTimeNun < 0) {
            timeLabel.hidden = YES;
        }
        [self countDownWithRemainTime:remainTime];
    }

}

-(void)tap:(UITapGestureRecognizer *)ges{
    buttonCanUseAfterOneSec(ges.view);
    XPSingleTeamPopView *popView = [[XPSingleTeamPopView alloc] initWithFrame:k_keyWindow.bounds];
    [k_keyWindow addSubview:popView];
    [popView viewViewDic:self.mainDic];
}

#pragma mark -- 倒计时
-(void)countDownWithRemainTime:(NSInteger)remainTime{
    self.remainTime = remainTime;
    [self.oneMinTimer startTimerWithTime:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
}
- (void)countDown{
    self.remainTime--;
    if (self.remainTime < 0) {
        [self.oneMinTimer stopTimer];
        if (self.countDownBlock) {
            self.countDownBlock();
        }
    }else{
        self.timeLabel.text = [self getTimeWithSecond:self.remainTime];
        CGFloat timeW = [self.timeLabel.text sizeWithAttributes:@{NSFontAttributeName:self.timeLabel.font}].width;
        self.timeLabel.frame = CGRectMake(self.mainView.mj_w - 12 - timeW, 12, timeW, 28);
    }
}

- (NSString *)getTimeWithSecond:(NSInteger )seconds{
    if (seconds< 0) {
        return @"剩余00:00:00";
    }
    
    NSString *hour = [NSString stringWithFormat:@"%02ld",seconds/3600];
    NSString *min = [NSString stringWithFormat:@"%02ld",(seconds%3600)/60];
    NSString *sec = [NSString stringWithFormat:@"%02ld",seconds%60];
    return [NSString stringWithFormat:@"剩余%@:%@:%@",hour,min,sec];
}





-(void)dealloc {
    [self.oneMinTimer stopTimer];
    
    NSLog(@"**************************");
}

@end
