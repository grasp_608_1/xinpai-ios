//
//  XPPackageView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/29.
//

#import "XPPackageView.h"

@interface XPPackageView ()

@property (nonatomic, strong) UILabel *topLabel;

@property (nonatomic, strong) UIButton *rightButton;

@property (nonatomic, strong) UIScrollView *mainView;

@property (nonatomic, copy) NSArray *buttonArray;

@end

@implementation XPPackageView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    
    UILabel *topLabel = [UILabel new];
    topLabel.font = kFontRegular(14);
    topLabel.textColor = app_font_color_8A;
    topLabel.frame = CGRectMake(12, 12, self.mj_w - 24, 24);
    self.topLabel = topLabel;
    [self addSubview:topLabel];
    
    UIImageView *rightImageView  = [[UIImageView alloc] initWithFrame:CGRectMake(self.mj_w - 56, 40, 56, 72)];
    rightImageView.image = [UIImage imageNamed:@"xp_package_all"];
    rightImageView.userInteractionEnabled = YES;
    
    [self addSubview:rightImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [rightImageView addGestureRecognizer:tap];
    
    
    self.mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 40,self.mj_w - 48, 72)];
    self.mainView.showsHorizontalScrollIndicator = NO;
    [self addSubview:self.mainView];
    
    
}

- (void)viewWithArray:(NSArray *)array{
    
    [self getTopStatus:array];
    
    [self.mainView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat left = 0;
    NSMutableArray *tempButtonArray = [NSMutableArray array];
    for (int i= 0 ; i<array.count; i++) {
        NSDictionary *dict = array[i];
        
        UIButton *packageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        packageButton.frame = CGRectMake(12 + left, 8, 0, 56);
        packageButton.layer.cornerRadius = 4;
        packageButton.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
        packageButton.layer.borderWidth = 1;
        packageButton.tag = i;
        [packageButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.mainView addSubview:packageButton];
        [tempButtonArray addObject:packageButton];
        
        UILabel *nameLabel = [UILabel new];
        nameLabel.font = kFontMedium(16);
        nameLabel.textColor = app_font_color;
        [packageButton addSubview:nameLabel];
        
        UILabel *countLabel = [UILabel new];
        countLabel.font = kFontRegular(14);
        countLabel.textColor = app_font_color_8A;
        [packageButton addSubview:countLabel];
        NSArray *listArray = dict[@"mallOrderPackageItemVOS"];
        
        NSString *name;
        NSNumber *deliveryStatus = dict[@"deliveryStatus"];
        if (deliveryStatus.intValue == 1) {
            name = [NSString stringWithFormat:@"包裹%d",i + 1];
        }else {
            name = @"待发货商品";
        }
        
        NSString *countStr = [NSString stringWithFormat:@"共%@件",dict[@"productTotalNum"]];
        CGFloat nameW = [name sizeWithAttributes:@{NSFontAttributeName:nameLabel.font}].width;
        CGFloat countW = [countStr sizeWithAttributes:@{NSFontAttributeName:countLabel.font}].width;
        
        nameLabel.text = name;
        nameLabel.frame = CGRectMake(8, 4, nameW, 24);
        countLabel.text = countStr;
        countLabel.frame = CGRectMake(8, MaxY(nameLabel), countW, 24);
        
        CGFloat labelLeft = nameW > countW ? MaxX(nameLabel) : MaxX(countLabel);
        
        for (int j = 0; j<listArray.count ; j++) {
            NSDictionary *tempDic = listArray[j];
            UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(labelLeft + 4, 8, 40, 40)];
            [iconImageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(tempDic[@"productPic"])] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
            iconImageView.layer.cornerRadius = 4;
            iconImageView.layer.masksToBounds = YES;
            [packageButton addSubview:iconImageView];
            
            NSString *labelStr = [NSString stringWithFormat:@"x%@",tempDic[@"productNum"]];
            UILabel *numLabel = [UILabel new];
            numLabel.backgroundColor = RGBACOLOR(58, 60, 65, 0.7);
            numLabel.font = kFontRegular(10);
            numLabel.textColor = UIColor.whiteColor;
            numLabel.textAlignment = NSTextAlignmentCenter;
            
            [iconImageView addSubview:numLabel];
            
            CGFloat LabelW = [labelStr sizeWithAttributes:@{NSFontAttributeName:numLabel.font}].width;
            numLabel.frame = CGRectMake(iconImageView.mj_w - LabelW - 4, iconImageView.mj_h - 12, LabelW + 4, 12);
            numLabel.text = labelStr;
            [XPCommonTool cornerRadius:numLabel andType:7 radius:4];
            
            labelLeft = MaxX(iconImageView);
        }
        packageButton.frame = CGRectMake(12 + left, 8, labelLeft + 8, 56);
        
        left = MaxX(packageButton);
    }
    
    self.buttonArray = tempButtonArray.copy;
    self.mainView.contentSize = CGSizeMake(left,72);
}

- (void)getTopStatus:(NSArray *)array {
    
    NSInteger deliverNum = 0;
    NSInteger signNum = 0;
    
    for (int i = 0; i<array.count; i++) {
        NSDictionary *dict = array[i];
        NSNumber *status = dict[@"deliveryStatus"];
        NSNumber *signStatus = dict[@"signStatus"];
        if (status.intValue == 1) {
            deliverNum++;
        }
        if (signStatus.intValue == 1) {
            signNum++;
        }
    }
    
    if (!self.canSelect) {
        NSMutableAttributedString *attr;
        if(signNum == array.count && signNum >0){
            
            NSString *tempStr = @"全部包裹已签收";
            attr = [[NSMutableAttributedString alloc] initWithString:tempStr];
            [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color,NSFontAttributeName:kFontMedium(16)} range:NSMakeRange(0, tempStr.length)];
        }else if(signNum >0){
            NSString *tempStr = @"订单已拆成多个包裹 部分包裹已签收";
            attr = [[NSMutableAttributedString alloc] initWithString:tempStr];
            [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color,NSFontAttributeName:kFontMedium(16)} range:NSMakeRange(0, 9)];
        }else if(deliverNum > 0){
            attr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"订单已拆成多个包裹 已发货%zd个包裹",deliverNum]];
            [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color,NSFontAttributeName:kFontMedium(16)} range:NSMakeRange(0, 9)];
        }else{
            NSString *tempStr = @"待发货";
            attr = [[NSMutableAttributedString alloc] initWithString:tempStr];
            [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color,NSFontAttributeName:kFontMedium(16)} range:NSMakeRange(0, tempStr.length)];
        }
        self.topLabel.attributedText = attr;
    }else {
        if(signNum == array.count && signNum >0){
            NSString *tempStr = @"全部包裹已签收";
            self.topLabel.text = tempStr;
        }else if(signNum >0){
            NSString *tempStr = @"部分包裹已签收，剩余商品正在努力运输中";
            self.topLabel.text = tempStr;
        }else if(deliverNum == array.count && signNum == 0){
            NSString *tempStr = @"您的货品正在运输中,请耐心等待";
            self.topLabel.text = tempStr;
        }else if(deliverNum > 0){
            self.topLabel.text = [NSString stringWithFormat:@"商家已发出%zd个包裹，剩余商品将尽快发货",deliverNum];
        }else{
            self.topLabel.text = @"待发货";
        }
    }
    
}

-(void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (!_canSelect) {
        return;
    }
    
    [self.mainView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.tag == sender.tag) {
            obj.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
        }else{
            obj.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
        }
    }];
    
    if (self.itemBlock) {
        self.itemBlock(sender.tag);
    }
}

- (void)tap:(UITapGestureRecognizer *)sender{
    buttonCanUseAfterOneSec(sender.view);
    
    if (self.rightBlock) {
        self.rightBlock();
    }
    
}

-(void)setCanSelect:(bool)canSelect{
    _canSelect = canSelect;
}

-(void)setDefaultSeletIndex:(NSUInteger)defaultSeletIndex{
    @try {
        
        if (self.buttonArray.count == 0) {
            return;
        }
        UIButton *sender = self.buttonArray[defaultSeletIndex];
        
        for (UIView *btn  in self.mainView.subviews) {
            
            if ([btn isKindOfClass:[UIButton class]]) {
                UIButton *button = (UIButton *)btn;
                if (sender.tag == button.tag) {
                    button.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
                    
                    [self scrollToCenterByButton:button animation:YES];
                }else {
                    button.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
                }
            }
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}

-(void)scrollToCenterByButton:(UIButton *)sender animation:(BOOL)animation{
    
        CGFloat offsetX = sender.mj_x - self.mainView.mj_w/2 + sender.mj_w/2.;

        if (sender.mj_x<self.mj_w*2/3.) {
            if (animation) {
                [UIView animateWithDuration:0.2 animations:^{
                    [self.mainView setContentOffset:CGPointMake(0, 0)];
                }];
            }else{
                [self.mainView setContentOffset:CGPointMake(0, 0)];
            }
        }else if (self.mainView.contentSize.width - sender.mj_x - self.mj_w*2/3 < 0){
            if (animation) {
                [UIView animateWithDuration:0.2 animations:^{
                    [self.mainView setContentOffset:CGPointMake(self.mainView.contentSize.width - self.mainView.mj_w, 0)];
                }];
            }else{
                [self.mainView setContentOffset:CGPointMake(self.mainView.contentSize.width - self.mainView.mj_w, 0)];
            }
        }else{
            if (animation) {
                [UIView animateWithDuration:0.2 animations:^{
                    [self.mainView setContentOffset:CGPointMake(offsetX, 0)];
                }];
            }else{
                [self.mainView setContentOffset:CGPointMake(offsetX, 0)];
            }
        }
}


@end
