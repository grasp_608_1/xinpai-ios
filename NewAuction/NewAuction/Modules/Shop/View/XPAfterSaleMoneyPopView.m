//
//  XPAfterSaleMoneyPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/18.
//

#import "XPAfterSaleMoneyPopView.h"
#import "XPHitButton.h"
#import "XPShopProductListView.h"

@interface XPAfterSaleMoneyPopView ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UIScrollView *contentView;
//付款信息
@property (nonatomic, strong) XPShopProductListView *moneyListView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation XPAfterSaleMoneyPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    bgView.alpha = 0;
    self.bgView = bgView;
    [self addSubview:bgView];
    
    self.mainView = [UIScrollView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h, SCREEN_WIDTH, 0.7 *self.mj_h - 48 - kBottomHeight);
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(18);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(0, 16,self.mj_w, 24);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    UIScrollView *contentView =  [[UIScrollView alloc] initWithFrame:CGRectMake(0, 52, self.mainView.mj_w, self.mainView.mj_h - 52)];
    self.contentView = contentView;
    [self.mainView addSubview:contentView];
    
    XPShopProductListView *moneyListView = [[XPShopProductListView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.mj_w, 0)];
    moneyListView.strokeTitle = YES;
    self.moneyListView = moneyListView;
    [self.contentView addSubview:moneyListView];
    
    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,self.mj_h - 48 - kBottomHeight, self.mj_w, 48 + kBottomHeight);
    bottomView.backgroundColor = UIColor.whiteColor;
    [self addSubview:bottomView];
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton setTitle:@"我知道了" forState:UIControlStateNormal];
    [nextButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    nextButton.frame = CGRectMake(24,4,bottomView.mj_w - 48,40);
    nextButton.layer.cornerRadius = 20;
    nextButton.layer.masksToBounds = YES;
    [nextButton addTarget:self action:@selector(nextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:nextButton];
    [nextButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :nextButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
}

-(void)viewWithModel:(XPShopAfterSaleModel *)model {
    
    self.titleLabel.text = @"钱款去向";
    
//    NSArray *titleArray = @[@"退款总金额",@"退款金额",@"退回账户",@"返回积分"];
    NSArray *titleArray;
    
    if (model.status.intValue == 6) {
        titleArray = @[@"退款总金额",@"退款金额",@"退回账户"];
    }else{
        titleArray = @[@"预计退款总金额",@"预计退款金额",@"预计退回账户"];
    }
    
    NSArray *moneyArray = @[
        [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.returnSumAmount]],
        [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.returnAmount]],
        [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.returnFunds]],
//        [NSString stringWithFormat:@"￥%@",model.returnPoints]
    ];
    CGFloat height = [self.moneyListView viewWithKeyArray:titleArray ValueArray:moneyArray];
    self.moneyListView.frame = CGRectMake(0, 0, self.contentView.mj_w , 0);
    self.contentView.contentSize = CGSizeMake(self.moneyListView.mj_w, height);
    
    UILabel *titleLabel = self.moneyListView.leftArray.firstObject;
    titleLabel.font = kFontMedium(18);
    titleLabel.textColor = UIColorFromRGB(0xFF4A4A);
    titleLabel.frame =  CGRectMake(12,10, 100, 36);
    
    UILabel *totalMoneyLabel = self.moneyListView.rightArray.firstObject;
    totalMoneyLabel.font = kFontMedium(14);
    totalMoneyLabel.textColor = UIColorFromRGB(0xFF4A4A);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bgView.alpha = 1;
        self.mainView.frame = CGRectMake(0, self.mj_h * 0.3, SCREEN_WIDTH, 0.7 *self.mj_h - 48 - kBottomHeight);
    }];
    
}
- (void)listViewWithModel:(XPShopAfterSaleModel *)model{
    
    self.titleLabel.text = @"审核进度";

    NSMutableArray  *titleArray = [NSMutableArray array];
    NSMutableArray *moneyArray = [NSMutableArray array];
    NSMutableArray *descArray = [NSMutableArray array];
    
    for (int i = 0; i<model.mallOrderAfterLogVOList.count; i++) {
        NSDictionary *dict = model.mallOrderAfterLogVOList[i];
        [titleArray addObject:dict[@"logTypeName"]];
        [moneyArray addObject:dict[@"createTime"]];
        [descArray addObject:dict[@"logTypeDescription"]];
    }
    
    CGFloat height = [self.moneyListView viewWithKeyArray:titleArray ValueArray:moneyArray descArray:descArray];
    self.moneyListView.frame = CGRectMake(0, 0, self.contentView.mj_w , 0);
    self.contentView.contentSize = CGSizeMake(self.moneyListView.mj_w, height);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bgView.alpha = 1;
        self.mainView.frame = CGRectMake(0, self.mj_h * 0.3, SCREEN_WIDTH, 0.7 *self.mj_h - 48 - kBottomHeight);
    }];
}



- (void)cancelButtonClicked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    
    [self removeFromSuperview];
}

- (void)nextButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self removeFromSuperview];
}


@end
