//
//  XPShopChartView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/17.
//

#import "XPShopChartView.h"

@interface XPShopChartView ()
@property (nonatomic, strong) UIButton *labelButton;

@property (nonatomic, strong) UIImageView *arrowImageView;

@property (nonatomic, copy) NSDictionary *dic;

@end

@implementation XPShopChartView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    
    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    
    UIButton *labelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [labelButton setTitleColor:UIColorFromRGB(0xAC855F) forState:UIControlStateNormal];
    labelButton.titleLabel.font = kFontMedium(14);
    labelButton.layer.cornerRadius = 4;
    labelButton.layer.masksToBounds = YES;
    labelButton.frame = CGRectMake(8, 8, self.mj_w - 16, self.mj_h - 16);
    labelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [labelButton addTarget:self action:@selector(tap:) forControlEvents:UIControlEventTouchUpInside];
    self.labelButton = labelButton;
    [self addSubview:labelButton];
    
    [labelButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :labelButton colors:
                                     @[(__bridge id)UIColorFromRGB(0xFFFBF3).CGColor,
                                       (__bridge id)UIColorFromRGB(0xFFF4E7).CGColor]] atIndex:0];
    
    
    UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.labelButton.mj_w - 18, 7, 18, 18)];
    arrowImageView.image = [UIImage imageNamed:@"Frame_7"];
    self.arrowImageView = arrowImageView;
    [labelButton addSubview:arrowImageView];
    
    
}

-(void)viewWithDic:(NSDictionary *)dic{
    _dic = dic;
    [self.labelButton setTitle:[NSString stringWithFormat:@" 入选%@  %@",dic[@"name"],dic[@"ranking"]] forState:UIControlStateNormal];

}

-(void)tap:(UIButton *)sender{
    buttonCanUseAfterOneSec(sender);
    
    if (self.tapBlock) {
        self.tapBlock(_dic);
    }
    
    
}


@end
