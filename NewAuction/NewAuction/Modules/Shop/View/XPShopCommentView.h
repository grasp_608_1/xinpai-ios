//
//  XPShopCommentView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/9.
//

#import <UIKit/UIKit.h>
#import "XPShopProductDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCommentView : UIView

//选规格
@property (nonatomic, copy) void(^commentBlock)(void);

- (CGFloat)viewWithDataModel:(XPShopProductDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
