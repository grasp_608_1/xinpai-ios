//
//  XPProductDeclareView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPProductDeclareView : UIView

/// - Parameters:
///   - imageArray: 图片数组
///   - contentStr: 详情
///   - titleStr: 标题
- (CGFloat)viewWithImageArray:(NSArray *)imageArray content:(NSString *)contentStr TitleStr:(NSString *)titleStr;
@end

NS_ASSUME_NONNULL_END
