//
//  XPSingleTeamPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPSingleTeamPopView : UIView

-(void)viewViewDic:(NSDictionary *)dic;

@end

NS_ASSUME_NONNULL_END
