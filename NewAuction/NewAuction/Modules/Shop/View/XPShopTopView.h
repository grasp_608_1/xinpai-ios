//
//  XPShopTopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopTopView : UIView
//轮播图点击
@property (nonatomic, copy) void(^bannerSelectBlock)(NSDictionary *dic);
//区域切换
@property (nonatomic, copy) void(^itemSelectBlock)(NSDictionary *dic);
//更多
@property (nonatomic, copy) void(^moreBlock)(NSDictionary *dic);
//活动专区
@property (nonatomic, copy) void(^commendItemBlock)(NSDictionary *dic);

@property (nonatomic, copy) void(^activityItemBlock)(NSDictionary *dic);

//数据源
- (CGFloat)viewWithData:(NSDictionary *)dataDict;
@end

NS_ASSUME_NONNULL_END
