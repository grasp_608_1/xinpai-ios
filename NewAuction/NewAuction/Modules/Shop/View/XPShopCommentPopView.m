//
//  XPShopCommentPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/11.
//

#import "XPShopCommentPopView.h"
#import "XPHitButton.h"
#import "XPEmptyView.h"
#import "XPShopProductCommentCell.h"
#import "XPShopProductCommentModel.h"

@interface XPShopCommentPopView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *mainView;
//产品图片
@property (nonatomic, strong) UIImageView *pImageView;
//产品名称
@property (nonatomic, strong) UILabel *pTitleLabel;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//评价时间
@property (nonatomic, strong) UILabel *specLabel;
//好评率
@property (nonatomic, strong) UILabel *commentRateLabel;
//列表
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) XPEmptyView *emptyView;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;

//当前页码
@property (nonatomic, assign) NSInteger page;

@property (nonatomic, strong) XPShopProductDetailModel *detailModel;
//cell高度
@property (nonatomic, strong) NSArray *heightArray;

@end

@implementation XPShopCommentPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {

    self.backgroundColor = UIColor.clearColor;
    self.page = 1;
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    bgView.alpha = 0;
    self.bgView = bgView;
    [self addSubview:bgView];
    
    self.mainView = [UIView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h, SCREEN_WIDTH, 0.85 *self.mj_h);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(18);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(0, 16,self.mj_w, 24);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"评论";
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, 56, self.mainView.mj_w, self.mainView.mj_h - 56);
    self.tableView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainView addSubview:self.tableView];
    self.tableView.tableHeaderView = [self createTableViewHeaderView];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.mj_w, IS_IPHONEX?34:0)];
    
//    XPWeakSelf
//    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        weakSelf.page = 1;
//        [weakSelf headerRefresh];
//    }];
    
    CGFloat emptyW = self.mj_w - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 150, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:[UIImage imageNamed:@"xp_default_empty"] Description:@"暂无评论"];
    [self.mainView addSubview:self.emptyView];
    
}

- (UIView *)createTableViewHeaderView {
    UIView *header = [UIView new];
    header.frame = CGRectMake(0, 0, self.mj_w, 104);
    
    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(24, 12, 80, 80);
    self.pImageView.layer.cornerRadius = 4;
    self.pImageView.layer.masksToBounds = YES;
    [header addSubview:_pImageView];
    
    CGFloat LabelX = MaxX(self.pImageView) + 12;
    CGFloat LabelW = self.mainView.mj_w - 140;
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontMedium(14);
    self.pTitleLabel.textColor = app_font_color;
    self.pTitleLabel.textAlignment = NO;
    self.pTitleLabel.frame = CGRectMake(LabelX, 12, LabelW, 48);
    [header addSubview:self.pTitleLabel];
    
    UILabel *specLabel = [UILabel new];
    specLabel.font = kFontRegular(14);
    specLabel.textColor = UIColorFromRGB(0x8A8A8A);
    specLabel.frame = CGRectMake(LabelX, 68,self.mainView.mj_w - 24 - 70 - LabelX, 14);
    self.specLabel = specLabel;
    [header addSubview:specLabel];
    
    UILabel *commentRateLabel = [UILabel new];
    commentRateLabel.font = kFontRegular(12);
    commentRateLabel.textColor = UIColorFromRGB(0x8A8A8A);
    commentRateLabel.textAlignment = NSTextAlignmentRight;
    commentRateLabel.frame = CGRectMake(self.mainView.mj_w - 24 - 70, 68,70, 24);
    self.commentRateLabel = commentRateLabel;
    [header addSubview:commentRateLabel];
    
    return header;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPShopProductCommentCell *cell = [XPShopProductCommentCell cellWithTableView:tableView];
    XPShopProductCommentModel *model = self.dataArray[indexPath.row];
    [cell cellWithModel:model];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *cellHeight = self.heightArray[indexPath.row];
    return cellHeight.floatValue;
}

- (void)cancelButtonClicked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    
    [self removeFromSuperview];
}

- (void)showWithModel:(XPShopProductDetailModel *)model {
    _detailModel = model;

    [self getData];
    NSDictionary *productDict = model.productSkuShortInfoVO;
    
    self.pTitleLabel.text = productDict[@"productName"];
    self.specLabel.text = productDict[@"attributeValues"];
    self.commentRateLabel.text = [NSString stringWithFormat:@"好评率%@%%",model.commentRate];
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:productDict[@"pic"]]];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bgView.alpha = 1;
        self.mainView.frame = CGRectMake(0, self.mj_h * 0.15, SCREEN_WIDTH, 0.85 *self.mj_h);
    }];
}

- (void)getData {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_product_Comment];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"id"] = self.detailModel.productSkuId;
    paraM[@"pageNo"] = @(1);
    
    XPBaseViewController *controller = (XPBaseViewController *)[XPCommonTool getCurrentViewController];
    
    [controller startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [controller stopLoadingGif];
        if (data.isSucess) {
            self.dataArray = [XPShopProductCommentModel mj_objectArrayWithKeyValuesArray:data.userData];
            [self canShowEmptyView];
            [self getCellHeightArray];
            [self.tableView reloadData];
            
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
    
}

- (void)headerRefresh{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_product_Comment];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = @(1);
    paraM[@"pageSize"] = @(20);
    paraM[@"id"] = self.detailModel.productSkuId;

    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_header endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData;
            NSArray *modelArray = [XPShopProductCommentModel mj_objectArrayWithKeyValuesArray:dataArray];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:modelArray];
            [self getCellHeightArray];
            [self.tableView reloadData];
            
            [self canShowEmptyView];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
}

- (void)footerRefresh {
    self.page ++;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_product_Comment];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"pageNo"] = [NSNumber numberWithInteger:self.page];
    paraM[@"pageSize"] = @(20);
    paraM[@"id"] = self.detailModel.productSkuId;
    
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.tableView.mj_footer endRefreshing];
        if (data.isSucess) {
            NSArray *dataArray = data.userData;
            NSArray *modelArray = [XPShopProductCommentModel mj_objectArrayWithKeyValuesArray:dataArray];
            if (dataArray.count == 0) {
                self.page--;
            }else {
                [self.dataArray addObjectsFromArray:modelArray];
                [self getCellHeightArray];
                [self.tableView reloadData];
            }
        }else {
            self.page--;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
        }
    }];
    
}
//获取cellheight
- (void)getCellHeightArray{
    
    NSMutableArray *tempArray = [NSMutableArray array];
    XPShopProductCommentCell *cell = XPShopProductCommentCell.new;
    for (int i = 0; i<self.dataArray.count; i++) {
        XPShopProductCommentModel *model = self.dataArray[i];
        cell.cellType = 1;
        CGFloat height = [cell cellWithModel:model];
        [tempArray addObject:[NSNumber numberWithFloat:height]];
    }
    self.heightArray = tempArray.copy;
}

- (void)canShowEmptyView {
    if (self.dataArray.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}

@end
