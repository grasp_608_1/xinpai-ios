//
//  XPShopCarTopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/13.
//

#import "XPShopCarTopView.h"
#import "XPShopCarItemView.h"
#import "XPCubeSlideItemView.h"

@interface XPShopCarTopView ()
//顶部活动标签
@property (nonatomic, strong) XPCubeSlideItemView *selectItemView;
//空白显示
@property (nonatomic, strong) UILabel *emptyLabel;

@end

@implementation XPShopCarTopView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    XPCubeSlideItemView *selectItemView = [[XPCubeSlideItemView alloc] initWithFrame:CGRectMake(0, 0, self.mj_w, 60)];
    selectItemView.backgroundColor = UIColor.clearColor;
    self.selectItemView = selectItemView;
    
    self.emptyLabel = [UILabel new];
    self.emptyLabel.font = kFontRegular(14);
    self.emptyLabel.textAlignment = NSTextAlignmentCenter;
    self.emptyLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.emptyLabel.text = @"您还没有加购商品,去逛逛";
    self.emptyLabel.frame = CGRectMake(0, 60, self.mj_w, 120);
    
    XPWeakSelf
    [self.selectItemView setTapBlock:^(NSInteger index) {
        if (weakSelf.activityBlcok) {
            weakSelf.activityBlcok(index);
        }
    }];
    
}
-(void)setShowSelectItem:(BOOL)showSelectItem{
    _showSelectItem = showSelectItem;
    if (!showSelectItem) {
        self.backgroundColor = UIColor.whiteColor;
        self.layer.cornerRadius = 8;
        self.layer.masksToBounds = YES;
    }
}

- (void)setActivityArray:(NSArray *)activityArray {
    [self.selectItemView configWithtitleArray:activityArray];
}


- (CGFloat)viewWithDataArray:(NSArray *)dataArray {
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self addSubview:self.selectItemView];
    
    CGFloat bottom = 0;
    if (self.showSelectItem) {
        bottom = 60;
    }else {
        bottom = 0;
    }
    
    self.itemsArray = [NSMutableArray array];
    for (int i = 0; i<dataArray.count; i++) {
        
        CGFloat itemH = 0;
        
        XPShopCarItemView *itemView = [[XPShopCarItemView alloc] initWithFrame:CGRectMake(0, bottom, self.mj_w, itemH)];
        itemView.tag = i;
        [self addSubview:itemView];
        itemView.showSelectItem = self.showSelectItem;
        itemView.canSlideOption = self.canSlideOption;
        if (self.showSelectItem) {
            CGFloat height = [itemView viewWithModel:[dataArray objectAtIndex:i]];
            itemView.frame = CGRectMake(0, bottom, self.mj_w, height);
        }else{
            CGFloat height = [itemView viewWithOtherModel:[dataArray objectAtIndex:i]];
            itemView.frame = CGRectMake(0, bottom, self.mj_w, height);
        }
        
        XPWeakSelf

        [itemView setSpecBlock:^{
            if (weakSelf.specBlock) {
                weakSelf.specBlock(i);
            }
        }];
        [itemView setAddBlock:^(NSInteger num) {
            if (weakSelf.addBlock) {
                weakSelf.addBlock(i, num);
            }
        }];
        [itemView setMinusBlock:^(NSInteger num) {
            if (weakSelf.minusBlock) {
                weakSelf.minusBlock(i, num);
            }
        }];
        [itemView setSelectBlock:^(bool isChoose) {
            if (weakSelf.selectBlock) {
                weakSelf.selectBlock(i,isChoose);
            }
        }];
        [itemView setDeleteBlock:^{
            if (weakSelf.deleteBlock) {
                weakSelf.deleteBlock(i);
            }
        }];
        [itemView setItemSelect:self.itemSelect];
        
        bottom = MaxY(itemView) + 12;
        [self.itemsArray addObject:itemView];
    }
    
    if (dataArray.count == 0) {
        [self addSubview:self.emptyLabel];
        bottom =  MaxY(self.emptyLabel);
    }

    return bottom;
}


@end
