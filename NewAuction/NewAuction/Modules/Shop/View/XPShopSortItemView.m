//
//  XPShopgSortItemView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/5.
//

#import "XPShopSortItemView.h"

@interface XPShopSortItemView ()

@property (nonatomic, copy) NSArray *titleArray;

@property (nonatomic, strong) UIImageView *downLineView;

@property (nonatomic, assign)NSInteger lastIndex;

@property (nonatomic, copy) NSArray *buttonArray;
@end

@implementation XPShopSortItemView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.showsHorizontalScrollIndicator = NO;
    self.bounces = YES;
    self.lastIndex = -1;
}

-(CGFloat)configWithtitleArray:(NSArray *)titleArray {
    self.titleArray = titleArray;
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
   
    //下划线
    UIImageView *downLineView = [[UIImageView alloc] init];
    downLineView.image = [UIImage imageNamed:@"xp_line_left_pink"];
    [self addSubview:downLineView];
    self.downLineView = downLineView;
    
    CGFloat maxRight = 0.0;
    CGFloat margin = self.itemSpace == 0? 5 : self.itemSpace;
    CGFloat btnH = self.mj_h;
    NSMutableArray *tempButtonArray = [NSMutableArray array];
    for (int i = 0; i<titleArray.count; i++) {
        NSString *titleStr = titleArray[i][@"name"];
        CGFloat btnW = [titleStr sizeWithAttributes:@{NSFontAttributeName:kFontMedium(16)}].width;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:titleStr forState:UIControlStateNormal];
        [self addSubview:btn];
        
        btn.frame = CGRectMake(margin + maxRight, 0, btnW, btnH);
        btn.tag = i + 1;
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            CGFloat lineY = MaxY(btn) -7;
            self.downLineView.frame = CGRectMake(btn.center.x - btnW/2, lineY, 34, 7);
            [btn setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontMedium(16);
        }else {
            [btn setTitleColor:app_font_color forState:UIControlStateNormal];
            btn.titleLabel.font = kFontRegular(14);
        }
        [tempButtonArray addObject:btn];
//        if (i == self.titleArray.count - 1) {
            maxRight = MaxX(btn) ;
//        }
    }
    self.buttonArray = tempButtonArray.copy;
    maxRight = maxRight + margin;
    //
    self.contentSize = CGSizeMake(maxRight<self.bounds.size.width ?self.bounds.size.width:maxRight, self.bounds.size.height);
    
    return maxRight;
}

- (void)btnClicked:(UIButton *)sender {
    
    if (sender.tag == _lastIndex) {
        return;
    }
    _lastIndex = sender.tag;
    
    for (UIView *btn  in self.subviews) {
        
        if ([btn isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)btn;
            if (sender.tag == button.tag) {
                [button setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
                button.titleLabel.font = kFontMedium(16);
                
                CGFloat lineY = MaxY(sender) - 7;
                self.downLineView.frame = CGRectMake(btn.center.x - btn.mj_w/2, lineY, 34, 7);
            }else {
                [button setTitleColor:app_font_color forState:UIControlStateNormal];
                button.titleLabel.font = kFontRegular(14);
            }
        }
    }
    
    if (self.tapBlock) {
        self.tapBlock(sender.tag);
    }
    
}

-(void)setDefaultSeletIndex:(NSUInteger)defaultSeletIndex{
    @try {
        
        if (self.buttonArray.count == 0) {
            return;
        }
        UIButton *sender = self.buttonArray[defaultSeletIndex];
        _lastIndex = sender.tag;
        
        for (UIView *btn  in self.subviews) {
            
            if ([btn isKindOfClass:[UIButton class]]) {
                UIButton *button = (UIButton *)btn;
                if (sender.tag == button.tag) {
                    [button setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
                    button.titleLabel.font = kFontMedium(16);
                    
                    CGFloat lineY = MaxY(sender) - 7;
                    self.downLineView.frame = CGRectMake(btn.center.x - btn.mj_w/2, lineY, 34, 7);
                    
                }else {
                    [button setTitleColor:app_font_color forState:UIControlStateNormal];
                    button.titleLabel.font = kFontRegular(14);
                }
            }
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}

@end
