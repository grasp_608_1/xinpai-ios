//
//  XPShopProductMainView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/9.
//

#import "XPShopProductMainView.h"
#import "XPHitButton.h"

@interface XPShopProductMainView ()

@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UIImageView *productStyleImageView;
//销售价
@property (nonatomic, strong) UILabel *priceSaleLabel;
//价格显示区
@property (nonatomic, strong) UILabel *priceCostLabel;
//销售数量
@property (nonatomic, strong) UILabel *salesNum;
//优惠券
@property (nonatomic, strong) UILabel *disCountDesLabel;
//优惠券列表
@property (nonatomic, strong) UIScrollView *discountView;
//产品名称
@property (nonatomic, strong) UILabel *titleLabel;
//标签
@property (nonatomic, strong) UIScrollView *signView;
@end

@implementation XPShopProductMainView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 12, self.mj_w - 12*2, 0);
    mainView.clipsToBounds = YES;
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.layer.cornerRadius = 8;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    self.productStyleImageView = [[UIImageView alloc] init];
    self.productStyleImageView.frame = CGRectMake(12, 24, 32, 16);
    self.productStyleImageView.layer.masksToBounds = YES;
    self.productStyleImageView.hidden = YES;
    [self.mainView addSubview:self.productStyleImageView];
    
    UILabel *priceSaleLabel = [UILabel new];
    priceSaleLabel.font = kFontMedium(26);
    priceSaleLabel.textColor = UIColorFromRGB(0xFF4A4A);
    priceSaleLabel.frame = CGRectMake(12, 12,mainView.mj_w - 24 - 80, 30);
    self.priceSaleLabel = priceSaleLabel;
    [self.mainView addSubview:priceSaleLabel];
    
    UILabel *priceCostLabel = [UILabel new];
    priceCostLabel.font = kFontRegular(14);
    priceCostLabel.textColor = app_font_color;
    priceCostLabel.frame = CGRectMake(12, MaxY(priceSaleLabel) + 4,mainView.mj_w - 24 - 80, 24);
    self.priceCostLabel = priceCostLabel;
    [self.mainView addSubview:priceCostLabel];
    
    UILabel *salesNum = [UILabel new];
    salesNum.font = kFontRegular(14);
    salesNum.textColor = UIColorFromRGB(0x8A8A8A);
    salesNum.textAlignment = NSTextAlignmentRight;
    salesNum.frame = CGRectMake(MaxX(self.priceSaleLabel), 18, 68, 18);
    self.salesNum = salesNum;
    [self.mainView addSubview:salesNum];
    
    UILabel *disCountDesLabel = [UILabel new];
    disCountDesLabel.font = kFontRegular(14);
    disCountDesLabel.textColor = app_font_color;
    disCountDesLabel.textAlignment = NSTextAlignmentRight;
    disCountDesLabel.frame = CGRectMake(12, MaxY(self.priceCostLabel) + 8, 42, 24);
    disCountDesLabel.text = @"优惠券";
    self.disCountDesLabel = disCountDesLabel;
    [self.mainView addSubview:disCountDesLabel];
    
    XPHitButton *arrowButton = [[XPHitButton alloc] init];
    arrowButton.hitRect = 30;
    [arrowButton addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [arrowButton setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    arrowButton.frame = CGRectMake(self.mainView.mj_w - 8 -18, 81, 18, 18);
    [self.mainView addSubview:arrowButton];
    
    self.discountView = [[UIScrollView alloc] initWithFrame:CGRectMake(62, 78, self.mainView.mj_w - 88 , 24)];
    self.discountView.showsHorizontalScrollIndicator = NO;
    [self.mainView addSubview:self.discountView];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(16);
    titleLabel.textColor = app_font_color;
    titleLabel.numberOfLines = NO;
    titleLabel.frame = CGRectMake(12, MaxY(self.discountView) + 12, self.mainView.mj_w - 24, 0);
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    self.signView = [[UIScrollView alloc] initWithFrame:CGRectMake(62, MaxY(self.titleLabel) + 12, self.mainView.mj_w - 24 , 24)];
    self.signView.showsHorizontalScrollIndicator = NO;
    [self.mainView addSubview:self.signView];
}


- (CGFloat)viewWithDataModel:(XPShopProductDetailModel *)model{
    //销量
    NSString *salesStr  = [NSString stringWithFormat:@"销量 %@ 库存 %@",model.sale,[self getStockWithModel:model]];
    CGFloat saleW = [salesStr sizeWithAttributes:@{NSFontAttributeName:self.salesNum.font}].width;
    self.salesNum.text = salesStr;
    self.salesNum.frame = CGRectMake(self.mainView.mj_w - saleW - 12 - 2,18, saleW, 18);

    NSDictionary *productSkuShortInfoVO = model.productSkuShortInfoVO;
    
    
    if (model.presaleStatus.intValue == 1) {
        //价格
        NSDictionary *productInfo = model.productSkuShortInfoVO[@"skuPrepareVO"];
        
        NSString *priceSaleStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:productInfo[@"presalePrice"]]];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceSaleStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(0, 1)];
        self.priceSaleLabel.attributedText = attr;
        
        self.productStyleImageView.hidden = NO;
        self.productStyleImageView.image = [UIImage imageNamed:@"xp_circle_preSale"];
        self.priceSaleLabel.frame = CGRectMake(50, 12,self.mainView.mj_w - 12 - 80 - 50, 30);
    }else if (model.groupStatus.intValue == 1) {
        NSDictionary *productInfo = model.productSkuShortInfoVO[@"skuGroupVO"];
        
        NSString *priceSaleStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:productInfo[@"groupPrice"]]];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceSaleStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(0, 1)];
        self.priceSaleLabel.attributedText = attr;
        
        self.productStyleImageView.image = [UIImage imageNamed:@"xp_circle_team"];
        self.productStyleImageView.hidden = NO;
        self.priceSaleLabel.frame = CGRectMake(50, 12,self.mainView.mj_w, 30);
    }else if (productSkuShortInfoVO[@"pointStatus"] && [productSkuShortInfoVO[@"pointStatus"] intValue] == 1) {
        
        NSString *priceSaleStr = [NSString stringWithFormat:@"%@ 积分",productSkuShortInfoVO[@"pricePoint"]];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceSaleStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(18)} range:NSMakeRange(priceSaleStr.length - 2, 2)];
        self.priceSaleLabel.attributedText = attr;
        
        self.productStyleImageView.hidden = YES;
        self.priceSaleLabel.frame = CGRectMake(12, 12,self.mainView.mj_w, 30);
    }else {
        //价格
        NSDictionary *productInfo = model.productSkuShortInfoVO;
        NSString *priceSaleStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:productInfo[@"priceSale"]]];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceSaleStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(0, 1)];
        self.priceSaleLabel.attributedText = attr;
        self.productStyleImageView.hidden = YES;
        self.priceSaleLabel.frame = CGRectMake(12, 12,self.mainView.mj_w, 30);
    }
    //价格
    NSDictionary *productInfo = model.productSkuShortInfoVO;
    NSString *priceCostStr = [NSString stringWithFormat:@"提货价:￥%@",[XPMoneyFormatTool formatMoneyWithNum:productInfo[@"priceCost"]]];
    self.priceCostLabel.text = priceCostStr;
    
    [self.discountView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (productSkuShortInfoVO[@"pointStatus"] && [productSkuShortInfoVO[@"pointStatus"] intValue] == 1) {
        self.priceCostLabel.frame = CGRectMake(12, MaxY(self.priceSaleLabel) + 4,self.mainView.mj_w - 24 - 80, 0);
        self.priceCostLabel.hidden = YES;
    }else{
        self.priceCostLabel.hidden = NO;
        self.priceCostLabel.frame = CGRectMake(12, MaxY(self.priceSaleLabel) + 4,self.mainView.mj_w - 24 - 80, 24);
    }
    
    NSArray *discountArray = model.userCouponVOs;
    CGFloat discountX = 0;
    for (int i = 0 ; i< discountArray.count; i++) {
        NSDictionary *couponDic = discountArray[i];
        UILabel *label= [UILabel new];
        label.font = kFontMedium(12);
        label.textColor = UIColorFromRGB(0x6E6BFF);
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = UIColorFromRGB(0xEEEEFF);
        label.layer.cornerRadius = 4;
        label.layer.masksToBounds = YES;
        [self.discountView addSubview:label];
        
        NSString *title = couponDic[@"note"];
        CGFloat discountW = [title sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
        label.frame = CGRectMake(discountX, 0, discountW + 8, self.discountView.mj_h);
        
        label.text = title;
        discountX = MaxX(label) + 8;
        
    }
    self.discountView.contentSize = CGSizeMake(discountX, self.discountView.mj_h);
    
    NSString *titleStr = model.productSkuShortInfoVO[@"productName"];
    
    CGFloat titleH = [titleStr boundingRectWithSize:CGSizeMake(self.titleLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.titleLabel.font} context:nil].size.height;
    self.titleLabel.frame = CGRectMake(12, MaxY(self.discountView) + 12, self.mainView.mj_w - 24, titleH + 6);
    self.titleLabel.text = titleStr;
    
    
    NSString *signStr = model.productSkuShortInfoVO[@"subTitle"];
    
    if (!signStr || signStr.length == 0) {
        self.mainView.frame = CGRectMake(12, 0, self.mj_w - 12*2, MaxY(self.titleLabel) + 12);
    }else {
        NSArray *signArray = [signStr componentsSeparatedByString:@","];
        CGFloat signX = 0;
        for (int i = 0 ; i< signArray.count; i++) {
            
            UILabel *label= [UILabel new];
            label.font = kFontMedium(12);
            label.textColor = UIColorFromRGB(0x8A8A8A );
            label.textAlignment = NSTextAlignmentCenter;
            label.backgroundColor = UIColorFromRGB(0xF2F2F2);
            label.layer.cornerRadius = 4;
            label.layer.masksToBounds = YES;
            [self.signView addSubview:label];
            
            NSString *title = signArray[i];
            CGFloat signW = [title sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
            label.frame = CGRectMake(signX, 0, signW + 8, self.signView.mj_h);
            
            label.text = title;
            signX = MaxX(label) + 8;
            
        }
        self.signView.frame = CGRectMake(12, MaxY(self.titleLabel) + 12, self.mainView.mj_w - 24 , 24);
        self.signView.contentSize = CGSizeMake(signX, self.discountView.mj_h);
        self.mainView.frame = CGRectMake(12, 0, self.mj_w - 12*2, MaxY(self.signView) + 12);
    }
    
    return MaxY(self.mainView);
    
}

- (NSNumber *)getStockWithModel:(XPShopProductDetailModel *)model {
    for (int i = 0 ; i< model.productSkuShortInfoVOs.count; i++) {
        NSDictionary *dict = model.productSkuShortInfoVOs[i];
        
        if ([dict objectForKey:@"productSkuId"] && [[dict objectForKey:@"productSkuId"] isEqualToNumber:model.productSkuId]) {
            return dict[@"stock"];
        }
    }
    return 0;
}


- (void)arrowButtonClicked:(UIButton *)sender{
    buttonCanUseAfterOneSec(sender);
    if (self.discountBlock) {
        self.discountBlock();
    }
}

@end
