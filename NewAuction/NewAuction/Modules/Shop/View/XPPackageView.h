//
//  XPPackageView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/29.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPPackageView : UIView
//按钮是否可选中
@property (nonatomic, assign) bool canSelect;

//默认选中
@property (nonatomic, assign) NSUInteger defaultSeletIndex;

@property (nonatomic, copy) void(^rightBlock)(void);

@property (nonatomic, copy) void(^itemBlock)(NSInteger tag);

- (void)viewWithArray:(NSArray *)array;



@end

NS_ASSUME_NONNULL_END
