//
//  XPAfterSaleProductView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/16.
//

#import <UIKit/UIKit.h>
#import "XPShopOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPAfterSaleProductView : UIView

//四周间距 default edgeInset
@property (nonatomic, assign) UIEdgeInsets edgeInset;
//是否显示分割线 default is NO
@property (nonatomic, assign) bool showSepView;
//是否显示选择框
@property (nonatomic, assign) bool showSelectButton;
//数据源
- (CGFloat)viewWithModel:(XPShopOrderModel *)model;
//是否显示商品参加的活动和优惠券活动 1 参与优惠活动 0 显示此商品所有的优惠活动
@property (nonatomic, assign) bool showUsedActivityAndCoupon;
//申请售后数据源
- (CGFloat)viewWithAfterSaleApplyModel:(XPShopOrderModel *)model;
//当前选中的商品Array
@property (nonatomic, strong) NSMutableArray *selectArray;



@end

NS_ASSUME_NONNULL_END
