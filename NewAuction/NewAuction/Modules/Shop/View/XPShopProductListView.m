//
//  XPShopProductListView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import "XPShopProductListView.h"
#import "XPHitButton.h"

@interface XPShopProductListView ()

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UIButton *foldButton;

@property (nonatomic, assign) CGFloat topMagin;

@property (nonatomic, assign) bool isFold;

@property (nonatomic, assign) CGFloat mainH;

@property (nonatomic, copy) void(^foldHander)(bool isFold);

@end

@implementation XPShopProductListView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    
    
}

- (CGFloat)viewWithKeyArray:(NSArray *)keyArray ValueArray:(NSArray *)valueArray {
    self.topMagin = 10;
    [self showKeyArray:keyArray ValueArray:valueArray];
    
    return self.mainView.mj_h;
}
- (CGFloat)viewWithKeyArray:(NSArray *)keyArray ValueArray:(NSArray *)valueArray bottomInfo:(NSDictionary *) extraInfo{
    [self showKeyArray:keyArray ValueArray:valueArray];
    
    UIView *sepView = [[UIView alloc] init];
    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    sepView.frame = CGRectMake(12, self.mainView.mj_h, self.mainView.mj_w - 24, 1);
    [self.mainView addSubview:sepView];
    
    UILabel *totalDiscountLabel = [UILabel new];
    totalDiscountLabel.textColor = UIColorFromRGB(0xFF4A4A);
    totalDiscountLabel.font = kFontRegular(12);
    totalDiscountLabel.frame = CGRectMake(12, MaxY(sepView) + 12, self.mainView.mj_w - 24, 16);
    totalDiscountLabel.textAlignment = NSTextAlignmentRight;
    [self.mainView addSubview:totalDiscountLabel];
    
    UILabel *totalMoneyLabel = [UILabel new];
    totalMoneyLabel.textColor = app_font_color;
    totalMoneyLabel.font = kFontRegular(14);
    totalMoneyLabel.frame = CGRectMake(12, MaxY(totalDiscountLabel), self.mainView.mj_w - 24, 24);
    totalMoneyLabel.textAlignment = NSTextAlignmentRight;
    [self.mainView addSubview:totalMoneyLabel];
    NSNumber *reduceAllAmount = extraInfo[@"reduceAllAmount"];
    
    NSNumber *spendAllAmount = extraInfo[@"spendAllAmount"];
    NSString *priceStr = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:spendAllAmount]];
    NSString *totalStr;
    NSNumber *status = extraInfo[@"status"];
    //////订单状态 0 默认 1 待付款 2 已支付|待发货 3 待收货|已发货 4 已完成 5 已取消 6 已关闭 888 积分兑换
    if(status.intValue == 1 || status.intValue == 5 ){
        totalDiscountLabel.text = [NSString stringWithFormat:@"共减￥%@",[XPMoneyFormatTool formatMoneyWithNum:reduceAllAmount]];
        totalStr = [NSString stringWithFormat:@"需付款￥%@",priceStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(totalStr.length - 1 - priceStr.length, priceStr.length + 1)];
        
        totalMoneyLabel.attributedText = attr;
    }else if(status.intValue == 2 ||status.intValue == 3 ||status.intValue == 4){
        totalDiscountLabel.text = [NSString stringWithFormat:@"共减￥%@",[XPMoneyFormatTool formatMoneyWithNum:reduceAllAmount]];
        totalStr = [NSString stringWithFormat:@"实付款￥%@",priceStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(totalStr.length - 1 - priceStr.length, priceStr.length + 1)];
        
        totalMoneyLabel.attributedText = attr;
    }else if(status.intValue == 888){//积分兑换
        totalDiscountLabel.text = @"";
        totalStr = [NSString stringWithFormat:@"合计: %@积分",priceStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(totalStr.length - 2 - priceStr.length, priceStr.length)];
        
        totalMoneyLabel.attributedText = attr;
    }else{
        totalDiscountLabel.text = [NSString stringWithFormat:@"共减￥%@",[XPMoneyFormatTool formatMoneyWithNum:reduceAllAmount]];
        totalStr = [NSString stringWithFormat:@"合计￥%@",priceStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:totalStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(totalStr.length - 1 - priceStr.length, priceStr.length + 1)];
        
        totalMoneyLabel.attributedText = attr;
    }
    
    self.mainView.frame = CGRectMake(12, 0, self.mj_w - 12 * 2, MaxY(totalMoneyLabel) + 16);
    
    return self.mainView.mj_h;
}
- (CGFloat)viewWithKeyArray:(NSArray *)keyArray ValueArray:(NSArray *)valueArray  topTitle:(NSString *)titleStr fold:(bool)fold foldHander:(void(^)(bool fold))foldHandler{

    self.isFold = fold;
    self.foldHander = foldHandler;
    self.topMagin = 51;
    
    [self showKeyArray:keyArray ValueArray:valueArray];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.textColor = app_font_color;
    titleLabel.font = kFontRegular(16);
    titleLabel.frame = CGRectMake(12, 12, self.mainView.mj_w - 24, 24);
    titleLabel.text = titleStr;
    [self.mainView addSubview:titleLabel];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(self.mainView.mj_w - 12 - 24 - 28 - 5 , 12, 24 + 28 + 5, 24);
    [btn addTarget:self action:@selector(foldButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.foldButton = btn;
    [self.mainView addSubview:btn];
    
    [btn setImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    [btn setTitleColor:app_font_color_8A forState:UIControlStateNormal];
    btn.titleLabel.font = kFontRegular(14);
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -65)];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -24, 0, 0)];
    
    if (self.isFold) {
        [self.foldButton setTitle:@"展开" forState:UIControlStateNormal];
        self.foldButton.imageView.transform = CGAffineTransformMakeRotation(M_PI_2);
    }else {
        [self.foldButton setTitle:@"收起" forState:UIControlStateNormal];
        self.foldButton.imageView.transform = CGAffineTransformMakeRotation(-M_PI_2);
    }
    
    
    return self.mainView.mj_h;
}

- (void)showKeyArray:(NSArray *)keyArray ValueArray:(NSArray *)valueArray{
    NSAssert(keyArray.count == valueArray.count, @"必须相等");
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    self.mainView = [UIView new];
    self.mainView.layer.cornerRadius = 4;
    self.mainView.backgroundColor = UIColor.whiteColor;
    self.mainView.clipsToBounds = YES;
    [self addSubview:self.mainView];
    CGFloat mainHeight = self.isFold ? 48 : self.topMagin + 10 + keyArray.count *34;
    self.mainView.frame = CGRectMake(12, 0, self.mj_w - 12 * 2, mainHeight);
    
    self.mainH = self.mainView.mj_h;
    
    self.rightArray = [NSMutableArray array];
    self.leftArray = [NSMutableArray array];
    for (int i = 0; i<keyArray.count; i++) {
        UILabel *leftView = [UILabel new];
        leftView.text = keyArray[i];
        if (self.strokeTitle) {
            leftView.textColor = app_font_color;
        }else {
            leftView.textColor = UIColorFromRGB(0x8A8A8A);
        }
        
        leftView.font = kFontRegular(14);
        CGFloat leftW = [leftView.text sizeWithAttributes:@{NSFontAttributeName:leftView.font}].width;
        leftView.frame = CGRectMake(12,self.topMagin + i *(10 + 24), leftW, 36);
        [self.mainView addSubview:leftView];
        
        UILabel *rightLabel = [UILabel new];
        rightLabel.text = valueArray[i];
        rightLabel.textColor = UIColorFromRGB(0x8A8A8A);
        rightLabel.font = kFontRegular(14);
        if (self.strokeValue) {
            rightLabel.font = kFontMedium(14);
        }else {
            rightLabel.font = kFontRegular(14);
        }
        rightLabel.frame = CGRectMake(leftW + 12 + 5,self.topMagin + i *(10 + 24), self.mainView.mj_w - leftW - 5 - 12 - 12, 36);
        rightLabel.tag = i;
        rightLabel.textAlignment = NSTextAlignmentRight;
        [self.mainView addSubview:rightLabel];
        
        [self.rightArray addObject:rightLabel];
        [self.leftArray addObject:leftView];
        
    }
    
}


- (CGFloat)viewWithKeyArray:(NSArray *)keyArray ValueArray:(NSArray *)valueArray descArray:(NSArray *)descArray{
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSAssert(keyArray.count == valueArray.count && keyArray.count == descArray.count, @"三个必须相等");
    
    self.mainView = [UIView new];
    self.mainView.layer.cornerRadius = 8;
    self.mainView.backgroundColor = UIColor.whiteColor;
    [self addSubview:self.mainView];
    self.mainView.frame = CGRectMake(12, 0, self.mj_w - 12 * 2, 20 + keyArray.count *65);
    
    self.rightArray = [NSMutableArray array];
    self.leftArray = [NSMutableArray array];
    for (int i = 0; i<keyArray.count; i++) {
        UILabel *leftView = [UILabel new];
        leftView.text = keyArray[i];
        if (self.strokeTitle) {
            leftView.textColor = app_font_color;
        }else {
            leftView.textColor = UIColorFromRGB(0x8A8A8A);
        }
        leftView.font = kFontRegular(14);
        leftView.frame = CGRectMake(12,16 + i * 65, 56, 24);
        [self.mainView addSubview:leftView];
        
        UILabel *rightLabel = [UILabel new];
        rightLabel.text = valueArray[i];
        rightLabel.textColor = UIColorFromRGB(0xB8B8B8);
        if (self.strokeValue) {
            rightLabel.font = kFontMedium(14);
        }else {
            rightLabel.font = kFontRegular(14);
        }
        
        rightLabel.frame = CGRectMake(70,16 + i * 65, self.mainView.mj_w - 70 - 12, 24);
        rightLabel.tag = i;
        rightLabel.textAlignment = NSTextAlignmentRight;
        [self.mainView addSubview:rightLabel];
        
        UILabel *bottomLabel = [UILabel new];
        bottomLabel.text = descArray[i];
        bottomLabel.textColor = UIColorFromRGB(0x8A8A8A);
        bottomLabel.font = kFontRegular(14);
        bottomLabel.frame = CGRectMake(12,MaxY(leftView), self.mainView.mj_w  - 12, 24);
        bottomLabel.tag = i;
        [self.mainView addSubview:bottomLabel];
        
        [self.rightArray addObject:rightLabel];
        [self.leftArray addObject:leftView];
        
        if (i != keyArray.count - 1) {
            
            UIView *sepView = [UIView new];
            sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
            sepView.frame = CGRectMake(12, MaxY(bottomLabel) + 12, self.mainView.mj_w - 12, 1);
            [self.mainView addSubview:sepView];
        }
    }
    
    return self.mainView.mj_h;
}

// 1 活动 2 优惠券
- (void)showCouponViewAtIndex:(NSInteger)index type:(NSInteger)type signArray:(NSArray *)signArray{
    
    UILabel *leftLabel = [self.leftArray objectAtIndex:index];
    UILabel *righLabel = [self.rightArray objectAtIndex:index];
    CGFloat leftW = [leftLabel.text sizeWithAttributes:@{NSFontAttributeName:leftLabel.font}].width;
    CGFloat rightW = [righLabel.text sizeWithAttributes:@{NSFontAttributeName:righLabel.font}].width;
    
    UIScrollView * signView = [[UIScrollView alloc] init];
    signView.bounces = NO;
    signView.showsHorizontalScrollIndicator = NO;
    signView.frame = CGRectMake(leftW + 30, leftLabel.mj_y,self.mainView.mj_w - leftW - 30 - rightW - 12,leftLabel.mj_h);
    
    [self addSubview:signView];
    
    NSMutableArray *titleArray = [NSMutableArray array];
    for (int i = 0; i<signArray.count ;i++) {
        NSDictionary *dict = signArray[i];
        [titleArray addObject:TO_STR(dict[@"note"])];
    }
    
    CGFloat left = 0;
    for (int i = 0; i<titleArray.count; i++) {
        NSString *str = titleArray[i];
        UILabel *label = [UILabel new];
        label.text = str;
        label.font = kFontMedium(10);
        
        if (type == 1) {
            label.textColor = UIColorFromRGB(0xF55B19);
        }else if(type == 2) {
            label.textColor = UIColorFromRGB(0xFF3F6D);
        }
        
        [signView addSubview:label];
        CGFloat labelW = [str sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
        label.frame = CGRectMake(left,0,labelW,signView.mj_h);
        left = MaxX(label) + 4;
    }
    signView.contentSize = CGSizeMake(left,signView.mj_h);
    
}

- (void)canPastAtIndex:(NSInteger)index {
    UILabel *label = self.rightArray[index];
    label.frame = CGRectMake(label.mj_x, label.mj_y, label.mj_w - 40, label.mj_h);
    
    XPHitButton *pastPostButton = [[XPHitButton alloc] init];
    [pastPostButton setTitle:@"复制" forState:UIControlStateNormal];
    pastPostButton.titleLabel.font = kFontRegular(14);
    [pastPostButton setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
    pastPostButton.frame = CGRectMake(self.mainView.mj_w - 12 - 28,self.topMagin + 6 + 36 *index,28,24);
    [pastPostButton addTarget:self action:@selector(pasteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    pastPostButton.tag = index;
    [self.mainView addSubview:pastPostButton];
    
    UIView *sepView = [UIView new];
    sepView.frame = CGRectMake(self.mainView.mj_w - 12 - 28 - 7,self.topMagin + 10 + 36 *index, 1, 16);
    sepView.backgroundColor = UIColorFromRGB(0xDEDEDE);
    [self.mainView addSubview: sepView];
   
}

- (void)showTopLineAtIndex:(NSInteger )index {
    UILabel *label = self.rightArray[index];
    
    UIView *sepView = [UIView new];
    sepView.frame = CGRectMake(12,label.mj_y, self.mainView.mj_w - 24, 1);
    sepView.backgroundColor = UIColorFromRGB(0xDEDEDE);
    [self.mainView addSubview: sepView];
}

- (void)showRightArrowAtIndex:(NSInteger) index {
    UILabel *label = self.rightArray[index];
    label.frame = CGRectMake(label.mj_x, label.mj_y, label.mj_w - 24 , label.mj_h);
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(self.mainView.mj_w - 24 - 12,label.mj_y , 24, label.mj_h);
    [btn setImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = index;
    self.foldButton = btn;
    [self.mainView addSubview:btn];
    
}

- (void)showRightQuestionAtIndex:(NSInteger) index {
    UILabel *label = self.rightArray[index];
    label.frame = CGRectMake(label.mj_x, label.mj_y, label.mj_w - 24 , label.mj_h);
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(self.mainView.mj_w - 24 - 12,label.mj_y , 24, label.mj_h);
    [btn setImage:[UIImage imageNamed:@"xp_need_help"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = index;
    self.foldButton = btn;
    [self.mainView addSubview:btn];
    
}

-(void)setStrokeTitle:(_Bool)strokeTitle {
    _strokeTitle = strokeTitle;
}

- (void)pasteButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    UILabel *label = self.rightArray[sender.tag];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = label.text;
    [[XPShowTipsTool shareInstance] showMsg:@"复制成功!" ToView:k_keyWindow];
}
- (void)foldButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.isFold) {
        self.mainView.frame = CGRectMake(12, 0, self.mj_w - 12 * 2, 48);
    }else {
        self.mainView.frame = CGRectMake(12, 0, self.mj_w - 12 * 2, self.mainH);
    }
    
    if(self.foldHander){
        self.foldHander(self.isFold);
    }
    
}

- (void)arrowButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.arrowBlock) {
        self.arrowBlock(sender.tag);
    }
}

@end
