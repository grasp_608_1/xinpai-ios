//
//  XPShopCommentPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/11.
//

#import <UIKit/UIKit.h>
#import "XPShopProductDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCommentPopView : UIView

- (void)showWithModel:(XPShopProductDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
