//
//  XPShopCarItemView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/13.
//

#import "XPShopCarItemView.h"
#import "XPHitButton.h"

static CGFloat swipOffset = 80;

@interface XPShopCarItemView ()
//内容View
@property (nonatomic, strong) UIView *mainView;
//可点击区域
@property (nonatomic, strong) UIButton *touchArea;
//删除View
@property (nonatomic, strong) UIButton *deleteButton;
//选择按钮
@property (nonatomic, strong) UIButton *selectedButton;
//商品图
@property (nonatomic, strong) UIImageView *iconImageView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//规格
@property (nonatomic, strong) UIButton *specButton;
@property (nonatomic, strong) UILabel *specLabel;
@property (nonatomic, strong) UIImageView *specArrowIcon;
//价格
@property (nonatomic, strong) UILabel *priceLabel;
//商品状态栏
@property (nonatomic, strong) UILabel *productStatusLabel;
//重新选择商品
@property (nonatomic, strong) UIButton *reSelectButton;
//加减计算View
@property (nonatomic, strong) UIView *countView;
//加减计算bg轮廓
@property (nonatomic, strong) UIView *countBgView;

@property (nonatomic, strong) XPHitButton *mButton;//减法
@property (nonatomic, strong) XPHitButton *addButton;//加法
//当前数量
@property (nonatomic, assign) NSInteger count;
//单个价格
@property (nonatomic, assign) CGFloat price;
//最大数量
@property (nonatomic, assign) NSInteger max;
//标签
@property (nonatomic, strong) UIScrollView *signView;

@property (nonatomic) id modelObj;

@end

@implementation XPShopCarItemView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.mainView.backgroundColor = RandColor;
    
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
    
    UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.mj_w, self.mj_h)];
    self.mainView = mainView;
    [self addSubview:mainView];
    
    UIButton *touchArea = [UIButton buttonWithType:UIButtonTypeCustom];
    touchArea.frame = CGRectMake(40, 0, self.mainView.mj_w - 40, self.mainView.mj_h);
    [touchArea addTarget:self action:@selector(touchAreaClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.touchArea = touchArea;
    [self.mainView addSubview:touchArea];
    
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteButton.backgroundColor = UIColorFromRGB(0xFF4A4A);
    deleteButton.frame = CGRectMake(self.mainView.mj_w, 0, swipOffset, self.mj_h);
    self.deleteButton = deleteButton;
    [self addSubview:deleteButton];
    
    [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    self.deleteButton.titleLabel.font = kFontMedium(18);
    [self.deleteButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(delegateButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    XPHitButton *selectButton = [[XPHitButton alloc] init];
    selectButton.frame = CGRectMake(12, 50, 20, 20);
    [selectButton setBackgroundImage:[UIImage imageNamed:@"xp_xbox_unselect"] forState:UIControlStateNormal];
    [selectButton setBackgroundImage:[UIImage imageNamed:@"xp_xbox_selected"] forState:UIControlStateSelected];
    selectButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [selectButton addTarget:self action:@selector(selectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:selectButton];
    selectButton.hitRect = 40;
    self.selectedButton = selectButton;
    selectButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.frame = CGRectMake(40, 12,88 ,88);
    self.iconImageView.layer.cornerRadius = 8;
    self.iconImageView.clipsToBounds = YES;
    [self.mainView addSubview:self.iconImageView];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 14, self.mj_w - MaxX(self.iconImageView) - 8 - 12, 24);
    self.titleLabel.font = kFontMedium(14);
    self.titleLabel.textColor = app_font_color;
    [self.mainView addSubview:self.titleLabel];
    
    self.specButton = [UIButton new];
    self.specButton.frame = CGRectMake(self.titleLabel.mj_x, MaxY(self.titleLabel) + 4, self.mj_w - MaxX(self.iconImageView) - 8 - 12, 24);
    [self.specButton addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    self.specButton.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.specButton.layer.cornerRadius = 4;
    [self.mainView addSubview:self.specButton];
    
    self.specLabel = [UILabel new];
    self.specLabel.frame = CGRectMake(0, 0, self.specButton.mj_w, 24);
    self.specLabel.font = kFontRegular(12);
    self.specLabel.textColor = UIColorFromRGB(0x8A8A8A);
    [self.specButton addSubview:self.specLabel];
    
    self.specArrowIcon = [[UIImageView alloc] init];
    self.specArrowIcon.frame = CGRectMake(self.specButton.mj_w - 24,0,24 ,24);
    self.specArrowIcon.image = [UIImage imageNamed:@"Frame_7"];
    self.specArrowIcon.transform = CGAffineTransformMakeRotation(M_PI_2);
    [self.specButton addSubview:self.specArrowIcon];
    
    self.priceLabel = [UILabel new];
    self.priceLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 82, self.mj_w - MaxX(self.iconImageView) - 8 - 12 - 100 , 30);
    self.priceLabel.font = kFontMedium(20);
    self.priceLabel.textColor = UIColorFromRGB(0xFF4A4A);
    [self.mainView addSubview:self.priceLabel];
    
    self.signView = [[UIScrollView alloc] init];
    self.signView.hidden = YES;
    [self.mainView addSubview:self.signView];
    
    
    UIView *countView = [UIView new];
    countView.frame = CGRectMake(self.mj_w - 100, 89, 100, 24);
    self.countView = countView;
    [self.mainView addSubview:countView];
    
    UIView *countBgView = [UIView new];
    countBgView.frame = self.countView.bounds;
    countBgView.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
    countBgView.layer.borderWidth = 0.5;
    countBgView.layer.cornerRadius = 4;
    countBgView.layer.masksToBounds = YES;
    self.countBgView = countBgView;
    [countView addSubview:countBgView];
    
    
    XPHitButton *button1 = [XPHitButton new];
    [button1 addTarget:self action:@selector(mButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button1 setTitle:@"-" forState:UIControlStateNormal];
    button1.hitRect  = 30;
    self.mButton = button1;
    [countView addSubview:button1];
    
    self.numberTextField = [XPNoMenuTextField new];
    self.numberTextField.textColor = UIColorFromRGB(0x3A3C41);
    self.numberTextField.font = kFontMedium(16);
    self.numberTextField.text = @"1";
    self.numberTextField.userInteractionEnabled = NO;
    self.numberTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.numberTextField.textAlignment = NSTextAlignmentCenter;
    self.numberTextField.layer.borderWidth = 0.5;
    self.numberTextField.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
    self.numberTextField.adjustsFontSizeToFitWidth = YES;
    [countView addSubview:self.numberTextField];
    
    XPHitButton *button2 = [XPHitButton new];
    [button2 addTarget:self action:@selector(aButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button2 setTitle:@"+" forState:UIControlStateNormal];
    button2.titleLabel.font = kFontMedium(14);
    button2.hitRect = 30;
    self.addButton = button2;
    [countView addSubview:button2];
    
    self.productStatusLabel = [UILabel new];
    self.productStatusLabel.frame = CGRectMake(40, MaxY(self.iconImageView) + 12, self.specButton.mj_w, 24);
    self.productStatusLabel.font = kFontRegular(14);
    self.productStatusLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.productStatusLabel.text = @"请重新选择商品规格";
    self.productStatusLabel.hidden = YES;
    [self.mainView addSubview:self.productStatusLabel];
    
    self.reSelectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.reSelectButton.hidden = YES;
    [self.reSelectButton setTitle:@"重选" forState:UIControlStateNormal];
    self.reSelectButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    self.reSelectButton.layer.borderWidth = 1;
    self.reSelectButton.layer.cornerRadius = 12;
    self.reSelectButton.titleLabel.font = kFontRegular(12);
    [self.reSelectButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    [self.reSelectButton addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.reSelectButton];
    
    
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftSwipe:)];
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    [self addGestureRecognizer:leftSwipe];
    
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightSwipe:)];
    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self addGestureRecognizer:rightSwipe];
    
    CGFloat numW = 30;
    
    self.countView.frame = CGRectMake(self.mj_w - (numW + 32 + 12) - 6, 82, numW + 32 + 12, 24 + 12);
    self.countBgView.frame = CGRectMake(6, 6, numW + 32, 24);
    self.addButton.frame = CGRectMake(MaxX(self.countBgView) - 16, self.countBgView.mj_y, 16, 24);
    self.mButton.frame = CGRectMake(self.countBgView.mj_x, self.countBgView.mj_y, 16, 24);
    self.numberTextField.frame = CGRectMake(MaxX(self.mButton), self.countBgView.mj_y,numW - 1, 24);
}

-(CGFloat)viewWithOtherModel:(XPShopProductBuyModel *)otherModel{
    
    CGFloat bottom = 0;
    _modelObj = otherModel;
    self.max = 200;
    self.count = otherModel.productQuantity.integerValue >= self.max ? self.max : otherModel.productQuantity.integerValue;
    self.numberTextField.text = [NSString stringWithFormat:@"%zd", self.count];
    if (self.count <= 1) {
        [self.mButton setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
    }else {
        [self.mButton setTitleColor:app_font_color forState:UIControlStateNormal];
    }
    
    if (self.count >= self.max) {
        [self.addButton setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
    }else{
        [self.addButton setTitleColor:app_font_color forState:UIControlStateNormal];
    }

    self.titleLabel.text = otherModel.productName;
    self.specLabel.text = otherModel.attributeValues;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:otherModel.pic] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    NSString *priceStr;
    if (otherModel.pointStatus.intValue == 1) {//积分兑换商品
        priceStr = [NSString stringWithFormat:@"%@ 积分",[XPMoneyFormatTool formatMoneyWithNum:otherModel.pricePoint]];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(12)} range:NSMakeRange(0, 1)];
        self.priceLabel.font = kFontMedium(16);
        self.priceLabel.textColor = app_font_color;
        self.priceLabel.text = priceStr;
    }else {
        priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:otherModel.priceSale]];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(12)} range:NSMakeRange(0, 1)];
        self.priceLabel.font = kFontMedium(16);
        self.priceLabel.textColor = app_font_color;
        self.priceLabel.attributedText = attr;
    }
    
    self.selectedButton.hidden = YES;
    self.specArrowIcon.hidden = YES;
    self.specButton.userInteractionEnabled = NO;

    self.iconImageView.frame = CGRectMake(12, 16,68 ,68);
    
    CGFloat priceW = [priceStr sizeWithAttributes:@{NSFontAttributeName:kFontMedium(16)}].width;
    self.priceLabel.frame = CGRectMake(self.mainView.mj_w - 12 - priceW, 16, priceW , 20);
    self.titleLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 16, self.priceLabel.mj_x - MaxX(self.iconImageView)  - 10, 20);
    
    self.specButton.frame = CGRectMake(self.titleLabel.mj_x, MaxY(self.titleLabel) + 4, self.mj_w - MaxX(self.iconImageView) - 12 - 8, 20);
    self.specLabel.frame = CGRectMake(0, 0, self.specButton.mj_w, 20);

    [self showSignViewWithStatus:3];
    
    [self layoutCountView];
    bottom = MaxY(self.iconImageView);
    self.mainView.frame = CGRectMake(0, 0, self.mainView.mj_w, bottom);
    self.touchArea.frame = CGRectMake(40, 0, self.mainView.mj_w - 40, self.mainView.mj_h);
    
    return bottom;
}
-(void)setModel:(XPShopCarItemModel *)model {

    _modelObj = model;
    _model = model;
    self.max = 200;
    self.count = model.quantity.integerValue >= self.max ? self.max : model.quantity.integerValue;
    self.numberTextField.text = [NSString stringWithFormat:@"%zd", self.count];
    if (self.count <= 1) {
        [self.mButton setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
    }else {
        [self.mButton setTitleColor:app_font_color forState:UIControlStateNormal];
    }
    
    if (self.count >= self.max) {
        [self.addButton setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
    }else{
        [self.addButton setTitleColor:app_font_color forState:UIControlStateNormal];
    }
    
    self.titleLabel.text = model.name;
    self.specLabel.text = model.attributeValues;
    self.selectedButton.selected = model.isChoose;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    if (model.isShow.intValue == 0) {
        self.productStatusLabel.text = @"商品已下架请重新选择";
    }else if (model.stock.intValue == 0){
        self.productStatusLabel.text = @"商品库存不足重新选择";
    }
    
    NSString *priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.price]];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(0, 1)];
    self.priceLabel.attributedText = attr;
    
    if (self.showSelectItem) {//显示
        self.selectedButton.hidden = NO;
        self.iconImageView.frame = CGRectMake(40, 12,88 ,88);
        self.titleLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 14, self.mj_w - MaxX(self.iconImageView) - 8 - 12, 24);
        self.specButton.frame = CGRectMake(self.titleLabel.mj_x, MaxY(self.titleLabel) + 4, self.mj_w - MaxX(self.iconImageView) - 8 - 12, 24);
        self.specLabel.frame = CGRectMake(0, 0, self.specButton.mj_w, 24);
        self.priceLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 82, self.mj_w - MaxX(self.iconImageView) - 8 - 12 - 100 , 30);
        self.specArrowIcon.frame = CGRectMake(self.specButton.mj_w - 24,0,24 ,24);
        self.priceLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 82, self.mj_w - MaxX(self.iconImageView) - 8 - 12 - 100 , 30);
        [self layoutCountView];
        
    }else{//不显示
        self.selectedButton.hidden = YES;
        self.iconImageView.frame = CGRectMake(12, 20,88 ,88);
        self.titleLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 22, self.mj_w - MaxX(self.iconImageView) - 8 - 12, 24);
        self.specButton.frame = CGRectMake(self.titleLabel.mj_x, MaxY(self.titleLabel) + 4, self.mj_w - MaxX(self.iconImageView) - 8 - 12, 24);
        self.specLabel.frame = CGRectMake(0, 0, self.specButton.mj_w, 24);
        self.priceLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 82, self.mj_w - MaxX(self.iconImageView) - 8 - 12 - 100 , 30);
        self.specArrowIcon.frame = CGRectMake(self.specButton.mj_w - 24,0,24 ,24);
        self.priceLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 96, self.mj_w - MaxX(self.iconImageView) - 8 - 12 - 100 , 24);
        [self layoutCountView];
    }
}

- (void)layoutCountView {
    CGFloat numW = [self.numberTextField.text sizeWithAttributes:@{NSFontAttributeName:self.numberTextField.font}].width + 24;
    if (!self.showSelectItem) {
        self.countView.frame = CGRectMake(self.mj_w - (numW + 32 + 12) - 6, 54, numW + 32 + 12, 24 + 12);
    }else {
        self.countView.frame = CGRectMake(self.mj_w - (numW + 32 + 12) - 6, self.priceLabel.mj_y - 4, numW + 32 + 12, 24 + 12);
    }
    
    self.countBgView.frame = CGRectMake(6, 6, numW + 32, 24);
    self.addButton.frame = CGRectMake(MaxX(self.countBgView) - 16, self.countBgView.mj_y, 16, 24);
    self.mButton.frame = CGRectMake(self.countBgView.mj_x, self.countBgView.mj_y, 16, 24);
    self.numberTextField.frame = CGRectMake(MaxX(self.mButton), self.countBgView.mj_y,numW - 1, 24);

}

-(void)setShowSelectItem:(BOOL)showSelectItem {
    _showSelectItem = showSelectItem;
    
}

- (CGFloat)viewWithModel:(XPShopCarItemModel *)model{
    
    _model = model;
    _modelObj = model;
    
    CGFloat bottom = 0;
    
    self.max = 200;
    if (self.showSelectItem) {
        self.count = model.quantity.integerValue >= self.max ? self.max : model.quantity.integerValue;
    }else{
        self.count = model.productQuantity.integerValue >= self.max ? self.max : model.productQuantity.integerValue;
    }
    
    self.numberTextField.text = [NSString stringWithFormat:@"%zd", self.count];
    if (self.count <= 1) {
        [self.mButton setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
    }else {
        [self.mButton setTitleColor:app_font_color forState:UIControlStateNormal];
    }
    
    if (self.count >= self.max) {
        [self.addButton setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
    }else{
        [self.addButton setTitleColor:app_font_color forState:UIControlStateNormal];
    }
    self.titleLabel.text = model.name;
    self.specLabel.text = model.attributeValues;
    self.selectedButton.selected = model.isChoose;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    NSString *priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.price]];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(0, 1)];
    self.priceLabel.attributedText = attr;
    
    if (self.showSelectItem) {//显示
        self.selectedButton.hidden = NO;
        self.specButton.userInteractionEnabled = YES;
        
        self.iconImageView.frame = CGRectMake(40, 12,88 ,88);
        self.titleLabel.frame = CGRectMake(MaxX(self.iconImageView) + 12, 12, self.mj_w - MaxX(self.iconImageView) - 12 - 12, 20);
        //不可点击状态
        if (model.isShow.intValue != 1 || model.stock.intValue == 0) {//库存不足
            self.priceLabel.hidden = YES;
            self.specButton.hidden = YES;
            self.productStatusLabel.hidden = NO;
            self.reSelectButton.hidden = NO;
            self.selectedButton.selected = NO;
            
            [self.selectedButton setBackgroundImage:[UIImage imageNamed:@"xp_xbox_forbid"] forState:UIControlStateNormal];
            self.titleLabel.textColor = app_font_color_8A;
            
            self.productStatusLabel.text = @"请重新选择商品规格";
            self.selectedButton.userInteractionEnabled = NO;
    
            [self showSignViewWithStatus:0];
            
            self.productStatusLabel.frame = CGRectMake(MaxX(self.iconImageView) + 12, 64, self.mj_w - 40 - 12, 24);
            self.reSelectButton.frame = CGRectMake(self.mainView.mj_w - 12 - 52, 64, 52, 24);
            self.countView.hidden = YES;
            bottom = MaxY(self.iconImageView) + 12;
            self.mainView.frame = CGRectMake(0, 0, self.mj_w, bottom);
            
        }else {// 可以点击,可修改状态
            self.priceLabel.hidden = NO;
            self.specButton.hidden = NO;
            self.reSelectButton.hidden = YES;
            self.productStatusLabel.hidden = YES;
            
            self.titleLabel.textColor = app_font_color;
            [self.selectedButton setBackgroundImage:[UIImage imageNamed:@"xp_xbox_unselect"] forState:UIControlStateNormal];
            
            self.countView.hidden = NO;
            self.selectedButton.userInteractionEnabled = YES;
            self.specButton.frame = CGRectMake(self.titleLabel.mj_x, MaxY(self.titleLabel) + 4, self.mj_w - MaxX(self.iconImageView) - 12 - 12, 24);
            self.specLabel.frame = CGRectMake(0, 0, self.specButton.mj_w - 24, 24);
            
            self.specArrowIcon.frame = CGRectMake(self.specButton.mj_w - 24,0,24 ,24);
            
            [self showSignViewWithStatus:1];
            
            if (model.skuActivityCouponNoteVOs.count == 0) {
                self.priceLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 68, self.mj_w - MaxX(self.iconImageView) - 8 - 12 - 100 , 30);
            }else {
                self.priceLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 92, self.mj_w - MaxX(self.iconImageView) - 8 - 12 - 100 , 30);
            }
            
            [self layoutCountView];
            
            bottom = model.skuActivityCouponNoteVOs.count == 0 ? MaxY(self.iconImageView) + 12: MaxY(self.iconImageView) + 28;
            self.mainView.frame = CGRectMake(0, 0, self.mj_w, bottom);
            self.touchArea.frame = CGRectMake(40, 0, self.mainView.mj_w - 40, self.mainView.mj_h);
        }
        
        
    }else{//不显示
        self.selectedButton.hidden = YES;
        self.specArrowIcon.hidden = YES;
        self.specButton.userInteractionEnabled = NO;
        
        CGFloat priceW = [priceStr sizeWithAttributes:@{NSFontAttributeName:self.priceLabel.font}].width;
        self.priceLabel.frame = CGRectMake(self.mainView.mj_w - 12 - priceW, self.iconImageView.mj_y, priceW , 20);
        
        self.iconImageView.frame = CGRectMake(12, 16,68 ,68);
        self.titleLabel.frame = CGRectMake(MaxX(self.iconImageView) + 8, 16, self.priceLabel.mj_x - MaxX(self.iconImageView)  - 10, 20);
        self.specButton.frame = CGRectMake(self.titleLabel.mj_x, MaxY(self.titleLabel) + 4, self.mj_w - MaxX(self.iconImageView) - 12 - 8, 20);
        self.specLabel.frame = CGRectMake(0, 0, self.specButton.mj_w, 20);

        [self showSignViewWithStatus:3];
        
        [self layoutCountView];
        bottom = MaxY(self.iconImageView) + 12;
        self.mainView.frame = CGRectMake(0, 0, self.mj_w, bottom);
        self.touchArea.frame = CGRectMake(40, 0, self.mainView.mj_w - 40, self.mainView.mj_h);
    }
    
    return bottom;
}

// 1 可选择状态 2 不可选择状态 3 带选择数量的标签
- (void)showSignViewWithStatus:(NSInteger) status{
    XPShopProductBuyModel *buyModel = [XPShopProductBuyModel new];
    if (self.model == nil) {
        buyModel = (XPShopProductBuyModel *)self.modelObj;
    }
    
    NSArray *titleArray = self.model != nil ? self.model.skuActivityCouponNoteVOs : buyModel.skuActivityCouponNoteVOs;
    if (titleArray.count == 0) {
        self.signView.hidden = YES;
    }else{
        self.signView.hidden = NO;
        if (status == 3) {
            self.signView.frame = CGRectMake(MaxX(self.iconImageView) + 8,64,self.mainView.mj_w - 70 - 12 - MaxX(self.iconImageView) - 8, 16);
        }else {
            self.signView.frame = CGRectMake(12 + MaxX(self.iconImageView),MaxY(self.titleLabel) + (status == 1 ? 28 : 4),self.mainView.mj_w - 144,16);
        }
        
        [self.signView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        CGFloat left = 0;
        for (int i = 0; i<titleArray.count; i++) {
            NSDictionary *dict = titleArray[i];
            UILabel *label = [UILabel new];
            label.text = TO_STR(dict[@"note"]);
            label.font = kFontMedium(10);
            NSNumber *type = dict[@"type"];
            if (type.intValue == 2) {
                label.textColor = UIColorFromRGB(0xF55B19);
            }else if (type.intValue == 1){
                label.textColor = UIColorFromRGB(0xFF3F6D);
            }
            [self.signView addSubview:label];
            CGFloat labelW = [label.text sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
            label.frame = CGRectMake(left,0,labelW,self.signView.mj_h);
            left = MaxX(label) + 4;
        }
        self.signView.contentSize = CGSizeMake(left, self.signView.mj_h);
    }
}

- (void)mButtonClicked:(UIButton *)sender {
    if (self.count <= 1) {
        self.numberTextField.text = @"1";
        self.count = 1;
    }else {
        self.count --;
        if (self.minusBlock) {
            self.minusBlock(self.count);
        }
//        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.count];
    }
    [self showTotalPrice];
    [self layoutCountView];
}
////当前最大可参拍数量为0
- (void)aButtonClicked:(UIButton *)sender {
    
    if (self.max == 0) {
        [[XPShowTipsTool shareInstance] showMsg:@"当前最大数量为0" ToView:k_keyWindow];
    }
    
    if (self.count >= self.max) {
        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.max];
        self.count = self.max;
    }else {
        self.count ++;
//        self.numberTextField.text = [NSString stringWithFormat:@"%zd",self.count];
        if (self.addBlock) {
            self.addBlock(self.count);
        }
    }
    [self showTotalPrice];
    [self layoutCountView];
}
- (void)showTotalPrice {
    
}

- (void)arrowButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.specBlock) {
        self.specBlock();
    }
}

- (void)selectButtonClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.selectBlock) {
        self.selectBlock(sender.selected);
    }
}
- (void)delegateButtonClicked:(UIButton  *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.deleteBlock) {
        self.deleteBlock();
    }
}


// 左滑手势处理方法
- (void)handleLeftSwipe:(UISwipeGestureRecognizer *)gesture {
    NSLog(@" left swipe!");
    if (!self.canSlideOption) {
        return;
    }
    self.touchArea.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.5 animations:^{
        self.mainView.frame = CGRectMake(-swipOffset, 0, self.mj_w, self.mj_h);
        self.deleteButton.frame = CGRectMake(self.mainView.mj_w - swipOffset, 0, swipOffset, self.mj_h);
    }];
}
// 右滑手势处理方法
- (void)handleRightSwipe:(UISwipeGestureRecognizer *)gesture {
    NSLog(@" right swipe!");
    if (!self.canSlideOption) {
        return;
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.mainView.frame = CGRectMake(0, 0, self.mj_w, self.mj_h);
        self.deleteButton.frame = CGRectMake(self.mainView.mj_w, 0, swipOffset, self.mj_h);
    } completion:^(BOOL finished) {
        self.touchArea.userInteractionEnabled = YES;
    }];
}
//点击区域
- (void)touchAreaClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.itemSelect) {
        self.itemSelect(self.modelObj);
    }
}

@end
