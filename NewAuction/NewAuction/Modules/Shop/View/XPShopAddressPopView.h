//
//  XPShopAddressPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/11.
//

#import <UIKit/UIKit.h>
#import "XPAdressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopAddressPopView : UIView

@property (nonatomic, copy) void (^addressSelectBlock)(XPAdressModel *model);
@property (nonatomic, copy) void (^addressChangeBlock)(void);

- (void)showPopViewWithController:(XPBaseViewController *)controller 
                    adressModel:(XPAdressModel *)XPAdressModel;
@end

NS_ASSUME_NONNULL_END
