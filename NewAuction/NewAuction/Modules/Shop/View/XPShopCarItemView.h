//
//  XPShopCarItemView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/13.
//

#import <UIKit/UIKit.h>
#import "XPShopCarItemModel.h"
#import "XPShopProductBuyModel.h"
#import "XPNoMenuTextField.h"


NS_ASSUME_NONNULL_BEGIN

@interface XPShopCarItemView : UIView
//是否显示左侧选择按钮 默认显示
@property (nonatomic, assign) BOOL showSelectItem;
//是否可侧滑删除 default no
@property (nonatomic, assign) BOOL canSlideOption;

//数量加1
@property (nonatomic, copy) void(^addBlock)(NSInteger num);
//数量减1
@property (nonatomic, copy) void(^minusBlock)(NSInteger num);
//重选规格
@property (nonatomic, copy) void(^specBlock)(void);
//重选规格
@property (nonatomic, copy) void(^deleteBlock)(void);
//选中//取消 当前商品
@property (nonatomic, copy) void(^selectBlock)(bool isChoose);
//item 点击
@property (nonatomic, copy) void(^itemSelect)(id modelObj);
//购物车model
@property (nonatomic, strong) XPShopCarItemModel *model;

@property (nonatomic, strong) XPNoMenuTextField *numberTextField;//当前数量
//添加购买商品Model
-(CGFloat)viewWithOtherModel:(XPShopProductBuyModel *)otherModel;

- (CGFloat)viewWithModel:(XPShopCarItemModel *)model;

@end

NS_ASSUME_NONNULL_END
