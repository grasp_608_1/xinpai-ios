//
//  XPShopCreationProductView.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/3.
//

#import "XPShopCreationProductView.h"
#import "XPHitButton.h"
#import "XPColorsSignView.h"

@interface XPShopCreationProductView ()

@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UIImageView *productStyleImageView;
//销售价
@property (nonatomic, strong) UILabel *priceSaleLabel;
//优惠券
@property (nonatomic, strong) UILabel *disCountDesLabel;
//优惠券列表
@property (nonatomic, strong) UIScrollView *discountView;
//产品名称
@property (nonatomic, strong) UILabel *titleLabel;
//标签
@property (nonatomic, strong) UIScrollView *signView;
//多色标签
@property (nonatomic, strong) XPColorsSignView *colorSignView;

//商品购买提示
@property (nonatomic, strong) UILabel *descLabel;
//热度
@property (nonatomic, strong) UIView *hotView;
//🔥
@property (nonatomic, strong) UIImageView *hotIcon;
//热度数量
@property (nonatomic, strong) UILabel *hotNumLabel;
@end

@implementation XPShopCreationProductView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 12, self.mj_w - 12*2, 0);
    mainView.clipsToBounds = YES;
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.layer.cornerRadius = 8;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    self.productStyleImageView = [[UIImageView alloc] init];
    self.productStyleImageView.frame = CGRectMake(12, 24, 32, 16);
    self.productStyleImageView.layer.masksToBounds = YES;
    self.productStyleImageView.hidden = YES;
    [self.mainView addSubview:self.productStyleImageView];
    
    UILabel *priceSaleLabel = [UILabel new];
    priceSaleLabel.font = kFontRegular(12);
    priceSaleLabel.textColor = app_font_color_8A;
    priceSaleLabel.frame = CGRectMake(12, 12,mainView.mj_w - 24 - 80, 30);
    self.priceSaleLabel = priceSaleLabel;
    [self.mainView addSubview:priceSaleLabel];
    
    UILabel *disCountDesLabel = [UILabel new];
    disCountDesLabel.font = kFontRegular(14);
    disCountDesLabel.textColor = app_font_color;
    disCountDesLabel.textAlignment = NSTextAlignmentRight;
    disCountDesLabel.frame = CGRectMake(12, MaxY(self.priceSaleLabel) + 8, 42, 24);
    disCountDesLabel.text = @"优惠券";
    self.disCountDesLabel = disCountDesLabel;
    [self.mainView addSubview:disCountDesLabel];
    
    XPHitButton *arrowButton = [[XPHitButton alloc] init];
    arrowButton.hitRect = 30;
    [arrowButton addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [arrowButton setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    arrowButton.frame = CGRectMake(self.mainView.mj_w - 8 -18, MaxY(self.priceSaleLabel) + 8, 18, 18);
    [self.mainView addSubview:arrowButton];
    
    self.discountView = [[UIScrollView alloc] initWithFrame:CGRectMake(62, 78, self.mainView.mj_w - 88 , 24)];
    self.discountView.showsHorizontalScrollIndicator = NO;
    [self.mainView addSubview:self.discountView];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(16);
    titleLabel.textColor = app_font_color;
    titleLabel.numberOfLines = NO;
    titleLabel.frame = CGRectMake(12, MaxY(self.discountView) + 12, self.mainView.mj_w - 24, 0);
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    self.signView = [[UIScrollView alloc] initWithFrame:CGRectMake(62, MaxY(self.titleLabel) + 12, self.mainView.mj_w - 24 , 24)];
    self.signView.showsHorizontalScrollIndicator = NO;
    [self.mainView addSubview:self.signView];
    
    self.colorSignView = [XPColorsSignView new];
    [self.mainView addSubview:self.colorSignView];
    
    UILabel *descLabel = [UILabel new];
    descLabel.font = kFontRegular(14);
    descLabel.textColor = app_font_color_8A;
    descLabel.numberOfLines = NO;
    descLabel.frame = CGRectMake(12, MaxY(self.signView) + 12, self.mainView.mj_w - 24, 0);
    self.descLabel = descLabel;
    [self.mainView addSubview:descLabel];
    
    self.hotView = [UIView new];
    self.hotView.frame = CGRectMake(0, 0, 0, 20);
    self.hotView.layer.cornerRadius = 2;
    [self.mainView addSubview:self.hotView];
    
    self.hotIcon = [[UIImageView alloc] init];
    self.hotIcon.frame = CGRectMake(0, 0, 20, 20);
    self.hotIcon.image = [UIImage imageNamed:@"xp_circle_fire"];
    [self.hotView addSubview:self.hotIcon];
    
    UILabel *hotNumLabel = [UILabel new];
    hotNumLabel.font = kFontRegular(12);
    hotNumLabel.textColor = app_font_color;
    hotNumLabel.frame = CGRectMake(17, 0,100, 20);
    self.hotNumLabel = hotNumLabel;
    [self.hotView addSubview:hotNumLabel];
}


- (CGFloat)viewWithDataModel:(XPShopProductDetailModel *)model{
    
    CGFloat top = 0;
    //销量
    NSString *salesStr  = [NSString stringWithFormat:@" 销量 %@",model.sale];
    NSDictionary *workSkuDic = model.productSkuShortInfoVO[@"worksSkuVO"];
    
    NSString *hotNum = [NSString stringWithFormat:@"%@",workSkuDic[@"popularity"]];
    CGFloat hotW = [hotNum sizeWithAttributes:@{NSFontAttributeName:self.hotNumLabel.font,}].width;
    self.hotNumLabel.frame = CGRectMake(20, 0, hotW, 20);
    self.hotNumLabel.text = hotNum;
    self.hotView.frame = CGRectMake(self.mainView.mj_w - 4 - 20 - 12 - hotW, 24, 24 + hotW, 20);
    
    if (model.presaleStatus.intValue == 1) {
        //价格
        NSDictionary *productInfo = model.productSkuShortInfoVO[@"skuPrepareVO"];
        
        NSString *priceSaleStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:productInfo[@"presalePrice"]]];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceSaleStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(0, 1)];
        self.priceSaleLabel.attributedText = attr;
        
        self.productStyleImageView.hidden = NO;
        self.productStyleImageView.image = [UIImage imageNamed:@"xp_circle_preSale"];
        self.priceSaleLabel.frame = CGRectMake(50, 12,self.mainView.mj_w - 12 - 80 - 50, 30);
    }else if (model.groupStatus.intValue == 1) {
        NSDictionary *productInfo = model.productSkuShortInfoVO[@"skuGroupVO"];
        
        NSString *priceSaleStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:productInfo[@"groupPrice"]]];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceSaleStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(0, 1)];
        self.priceSaleLabel.attributedText = attr;
        
        self.productStyleImageView.image = [UIImage imageNamed:@"xp_circle_team"];
        self.productStyleImageView.hidden = NO;
        self.priceSaleLabel.frame = CGRectMake(50, 12,self.mainView.mj_w, 30);
    }else {
        //价格
        NSDictionary *productInfo = model.productSkuShortInfoVO;
        NSString *priceSaleStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:productInfo[@"priceSale"]]];
        NSString *priceMarketStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:productInfo[@"priceMarket"]]];
        
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@",priceSaleStr,priceMarketStr,salesStr]];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(26)} range:NSMakeRange(1, priceSaleStr.length -1)];
        [attr addAttributes:@{NSForegroundColorAttributeName:app_font_color_FF4A4A} range:NSMakeRange(0, priceSaleStr.length)];
        [attr addAttributes:@{NSStrikethroughStyleAttributeName: @(NSUnderlineStyleSingle),
                              NSUnderlineColorAttributeName:app_font_color_8A} range:NSMakeRange(priceSaleStr.length, priceMarketStr.length)];
        
        self.priceSaleLabel.attributedText = attr;
        self.productStyleImageView.hidden = YES;
        self.priceSaleLabel.frame = CGRectMake(12, 12,self.mainView.mj_w, 30);
    }
   
    top = MaxY(self.priceSaleLabel);
    [self.discountView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSArray *discountArray = model.userCouponVOs;
    CGFloat discountX = 0;
    for (int i = 0 ; i< discountArray.count; i++) {
        NSDictionary *couponDic = discountArray[i];
        UILabel *label= [UILabel new];
        label.font = kFontMedium(12);
        label.textColor = UIColorFromRGB(0x6E6BFF);
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = UIColorFromRGB(0xEEEEFF);
        label.layer.cornerRadius = 4;
        label.layer.masksToBounds = YES;
        [self.discountView addSubview:label];
        
        NSString *title = couponDic[@"note"];
        CGFloat discountW = [title sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
        label.frame = CGRectMake(discountX, 0, discountW + 8, self.discountView.mj_h);
        
        label.text = title;
        discountX = MaxX(label) + 8;
        
    }
    self.discountView.contentSize = CGSizeMake(discountX, self.discountView.mj_h);
    self.discountView.frame = CGRectMake(62, top + 12, self.mainView.mj_w - 88 , 24);
    
    top = MaxY(self.discountView);
    
    NSString *titleStr = model.productSkuShortInfoVO[@"productName"];
    
    CGFloat titleH = [titleStr boundingRectWithSize:CGSizeMake(self.titleLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.titleLabel.font} context:nil].size.height;
    self.titleLabel.frame = CGRectMake(12, top + 4, self.mainView.mj_w - 24, titleH + 6);
    self.titleLabel.text = titleStr;
    
    top = MaxY(self.titleLabel);
    NSArray *colorsArray = model.productSkuShortInfoVO[@"worksSkuVO"][@"worksSkuLabelVos"];
    
    if (colorsArray.count == 0) {
        self.colorSignView.hidden = YES;
    }else {
        self.colorSignView.hidden = NO;
        self.colorSignView.frame = CGRectMake(12, top + 6, self.mainView.mj_w - 24 , 24);
        [self.colorSignView viewWithArray:colorsArray];
        
        top = MaxY(self.colorSignView);
        
    }
    NSString *orderInfo = model.productSkuShortInfoVO[@"recentOrderInfo"];
    if (orderInfo && orderInfo.length > 0) {
        self.descLabel.text = orderInfo;
        self.descLabel.frame = CGRectMake(12, top + 6, self.mainView.mj_w - 24, 24);
        top = MaxY(self.descLabel);
    }
    
    self.mainView.frame = CGRectMake(12, 0, self.mj_w - 12*2, top + 12);
    
    return MaxY(self.mainView);
    
}
//获取库存
- (void)arrowButtonClicked:(UIButton *)sender{
    buttonCanUseAfterOneSec(sender);
    if (self.discountBlock) {
        self.discountBlock();
    }
}

@end
