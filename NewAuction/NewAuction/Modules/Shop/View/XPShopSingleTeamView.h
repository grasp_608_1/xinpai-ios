//
//  XPShopSingleTeamView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/13.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, XPShopTeamType) {
    XPShopTeamTypeNormal,
    XPShopTeamTypeWait,//未完成,含倒计时
    XPShopTeamTypeConfirm,//加团,待成团,不含倒计时
    XPShopTeamTypeCreate//建团
};

NS_ASSUME_NONNULL_BEGIN

@interface XPShopSingleTeamView : UIView

@property (nonatomic, copy) void(^countDownBlock)(void);

@property (nonatomic, copy) void(^arrowBlock)(void);

-(void)viewWithDic:(NSDictionary *)dic Type:(XPShopTeamType)type;

@end

NS_ASSUME_NONNULL_END
