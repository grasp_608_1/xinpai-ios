//
//  XPShopProductSpecView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/9.
//

#import "XPShopProductSpecView.h"
#import "XPAdressModel.h"
#import "XPHitButton.h"

@interface XPShopProductSpecView ()

@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UILabel *selectDesLabel;
//已选择规格
@property (nonatomic, strong) UILabel *specLabel;
@property (nonatomic, strong) UILabel *sendDesLabel;
//当前地址
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UIButton *arrowButton2;

//活动
@property (nonatomic, strong) UILabel *promoteLabel;
@property (nonatomic, strong) UILabel *promoteDesLabel;
@property (nonatomic, strong) UIButton *arrowButton3;

//运费
@property (nonatomic, strong) UILabel *transLabel;
@property (nonatomic, strong) UILabel *transDesLabel;
@property (nonatomic, strong) UIButton *arrowButton4;


@end

@implementation XPShopProductSpecView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    
    self.layer.cornerRadius = 8;
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 0, self.mj_w - 12*2, 0);
    mainView.clipsToBounds = YES;
    mainView.layer.cornerRadius = 8;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    UILabel *selectDesLabel = [UILabel new];
    selectDesLabel.font = kFontRegular(14);
    selectDesLabel.textColor = UIColorFromRGB(0x8A8A8A);
    selectDesLabel.frame = CGRectMake(12, 12,28, 24);
    selectDesLabel.text = @"已选";
    self.selectDesLabel = selectDesLabel;
    [self.mainView addSubview:selectDesLabel];
    
    UILabel *specLabel = [UILabel new];
    specLabel.font = kFontRegular(14);
    specLabel.textColor = app_font_color;
    specLabel.frame = CGRectMake(52, 12, self.mainView.mj_w - 86, 24);
    specLabel.numberOfLines = NO;
    self.specLabel = specLabel;
    [self.mainView addSubview:specLabel];
    
    XPHitButton *arrowButton1 = [[XPHitButton alloc] init];
    arrowButton1.hitRect = 25;
    [arrowButton1 addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [arrowButton1 setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    arrowButton1.tag = 0;
    arrowButton1.frame = CGRectMake(self.mainView.mj_w - 8 - 18, 15, 18, 18);
    [self.mainView addSubview:arrowButton1];
    
    UILabel *sendDesLabel = [UILabel new];
    sendDesLabel.font = kFontRegular(14);
    sendDesLabel.textColor = UIColorFromRGB(0x8A8A8A);
    sendDesLabel.frame = CGRectMake(12, 50,28, 24);
    sendDesLabel.text = @"送至";
    self.sendDesLabel = sendDesLabel;
    [self.mainView addSubview:sendDesLabel];
    
    XPHitButton *arrowButton2 = [[XPHitButton alloc] init];
    arrowButton2.hitRect = 25;
    [arrowButton2 addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [arrowButton2 setImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    arrowButton2.frame = CGRectMake(self.mainView.mj_w - 8 - 18, 15, 18, 18);
    arrowButton2.tag = 1;
    self.arrowButton2 = arrowButton2;
    [self.mainView addSubview:arrowButton2];
    
    UILabel *addressLabel = [UILabel new];
    addressLabel.font = kFontRegular(14);
    addressLabel.textColor = app_font_color;
    addressLabel.frame = CGRectMake(self.mainView.mj_w - 8 - 24, 15, 24, 24);
    addressLabel.text = @"暂未设置地址";
    self.addressLabel = addressLabel;
    [self.mainView addSubview:addressLabel];
    
    //活动
    UILabel *promoteDescLabel = [UILabel new];
    promoteDescLabel.font = kFontRegular(14);
    promoteDescLabel.textColor = UIColorFromRGB(0x8A8A8A);
    promoteDescLabel.frame = CGRectMake(12, 12,28, 24);
    promoteDescLabel.text = @"活动";
    self.promoteDesLabel = promoteDescLabel;
    [self.mainView addSubview:promoteDescLabel];
    
    UILabel *promoteLabel = [UILabel new];
    promoteLabel.font = kFontRegular(14);
    promoteLabel.textColor = app_font_color;
    promoteLabel.frame = CGRectMake(52, 12, self.mainView.mj_w - 86, 24);
    promoteLabel.numberOfLines = NO;
    self.promoteLabel = promoteLabel;
    [self.mainView addSubview:promoteLabel];
    
    XPHitButton *arrowButton3 = [[XPHitButton alloc] init];
    arrowButton3.hitRect = 25;
    [arrowButton3 addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [arrowButton3 setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    arrowButton3.tag = 2;
    arrowButton3.frame = CGRectMake(self.mainView.mj_w - 8 - 18, 15, 18, 18);
    self.arrowButton3 = arrowButton3;
    [self.mainView addSubview:arrowButton3];
    
    //运费
    UILabel *transDesLabel = [UILabel new];
    transDesLabel.font = kFontRegular(14);
    transDesLabel.textColor = UIColorFromRGB(0x8A8A8A);
    transDesLabel.frame = CGRectMake(12, 12,28, 24);
    transDesLabel.text = @"运费";
    self.transDesLabel = transDesLabel;
    [self.mainView addSubview:transDesLabel];
    
    UILabel *transLabel = [UILabel new];
    transLabel.font = kFontRegular(14);
    transLabel.textColor = app_font_color;
    transLabel.frame = CGRectMake(52, 12, self.mainView.mj_w - 86, 24);
    transLabel.numberOfLines = NO;
    self.transLabel = transLabel;
    [self.mainView addSubview:transLabel];
    
    XPHitButton *arrowButton4 = [[XPHitButton alloc] init];
    arrowButton4.hitRect = 25;
    [arrowButton4 addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [arrowButton4 setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    arrowButton4.tag = 3;
    arrowButton4.frame = CGRectMake(self.mainView.mj_w - 8 - 18, 15, 18, 18);
    self.arrowButton4 = arrowButton4;
    [self.mainView addSubview:arrowButton4];
    
}

- (CGFloat)viewWithDataModel:(XPShopProductDetailModel *)model{
    
    CGFloat bottom;
    
    NSString *specString  = model.productSkuShortInfoVO[@"attributeValues"];

    CGFloat specH = [specString boundingRectWithSize:CGSizeMake(self.specLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.specLabel.font} context:nil].size.height;
    
    XPAdressModel *addressModel = [XPAdressModel mj_objectWithKeyValues:model.userAddressVO];
    
    if (self.addressModel.area.length == 0) {
        self.addressModel = addressModel;
    }
    
    self.specLabel.text = specString;
    self.specLabel.frame = CGRectMake(52, 12, self.mainView.mj_w - 86, specH + 2);
    self.sendDesLabel.frame = CGRectMake(12, MaxY(self.specLabel) + 12,28, 24);
    self.addressLabel.frame = CGRectMake(52, self.sendDesLabel.mj_y, self.mainView.mj_w - 86, 24);
    self.arrowButton2.frame = CGRectMake(self.mainView.mj_w - 8 - 24, self.sendDesLabel.mj_y, 24, 24);
    
    bottom = MaxY(self.addressLabel);
    
    if (model.activityNote.length >  0) {
        self.promoteLabel.hidden = NO;
        self.promoteDesLabel.hidden = NO;
        self.arrowButton4.hidden = NO;
        self.promoteDesLabel.frame = CGRectMake(12, MaxY(self.addressLabel) + 12, 28, 24);
        self.promoteLabel.frame = CGRectMake(52, self.promoteDesLabel.mj_y, self.mainView.mj_w - 86, 24);
        self.arrowButton3.frame = CGRectMake(self.mainView.mj_w - 8 - 24, self.promoteDesLabel.mj_y + 3, 18, 18);
        bottom = MaxY(self.promoteLabel);
        
        NSString *promoteSignStr = model.activityNote;
        NSString *promoteDescStr = model.activityRemark;
        NSString *promoteStr = [NSString stringWithFormat:@"%@ %@",promoteSignStr,promoteDescStr];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:promoteStr];
        [attr addAttributes:@{NSFontAttributeName:kFontMedium(14),NSForegroundColorAttributeName:UIColorFromRGB(0xFF3F6D)} range:NSMakeRange(0, promoteSignStr.length)];
        
        self.promoteLabel.attributedText = attr;
        bottom = MaxY(self.promoteDesLabel);
    }else {
        self.promoteLabel.hidden = YES;
        self.promoteDesLabel.hidden = YES;
        self.arrowButton3.hidden = YES;
        
    }
    
    if (model.freightNote.length > 0){
        self.transDesLabel.hidden = NO;
        self.transLabel.hidden = NO;
        self.arrowButton4.hidden = NO;
        
        self.transDesLabel.frame = CGRectMake(12, bottom + 12, 28, 24);
        self.transLabel.frame = CGRectMake(52, self.transDesLabel.mj_y, self.mainView.mj_w - 86, 24);
        self.arrowButton4.frame = CGRectMake(self.mainView.mj_w - 8 - 24, self.transDesLabel.mj_y + 3, 18, 18);
        bottom = MaxY(self.transLabel);
        self.transLabel.text = model.freightNote;
        
        bottom = MaxY(self.transLabel);
    }else {
        self.transDesLabel.hidden = YES;
        self.transLabel.hidden = YES;
        self.arrowButton4.hidden = YES;
    }

    self.mainView.frame = CGRectMake(12, 0, self.mj_w - 12*2, bottom + 12);
    
    return MaxY(self.mainView);
    
}

-(void)setAddressModel:(XPAdressModel *)addressModel {
    _addressModel = addressModel;
    self.addressLabel.text = addressModel.area.length>0 ? [NSString stringWithFormat:@"%@%@",[addressModel.area stringByReplacingOccurrencesOfString:@"," withString:@""],addressModel.address] : @"暂未设置地址";
}


- (void)arrowButtonClicked:(UIButton *)sender{
    buttonCanUseAfterOneSec(sender);
    if (self.specBlock) {
        self.specBlock(sender.tag);
    }
}


@end
