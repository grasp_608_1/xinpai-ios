//
//  XPShopOrderPruductView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import "XPShopOrderPruductView.h"
#import "XPAfterSaleProductItemView.h"
#import "XPShopOrderProductListModel.h"

@interface XPShopOrderPruductView ()

@property (nonatomic, strong) UIView *mainView;
//账户资金
@property (nonatomic, strong) UILabel *accountLabel;
//现金支付
@property (nonatomic, strong) UILabel *payLabel;

@end

@implementation XPShopOrderPruductView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    self.backgroundColor = UIColor.whiteColor;
    self.mainView = [UIView new];
    self.mainView.frame = CGRectMake(0, 0, self.mj_w, 0);
    [self addSubview:self.mainView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.mainView addGestureRecognizer:tap];
    
    self.payLabel = [UILabel new];
    self.payLabel.font = kFontMedium(14);
    self.payLabel.textColor = app_font_color;
    self.payLabel.textAlignment = NSTextAlignmentRight;
    self.payLabel.numberOfLines = YES;
    [self.mainView addSubview:self.payLabel];
    
    self.accountLabel = [UILabel new];
    self.accountLabel.font = kFontMedium(14);
    self.accountLabel.textColor = app_font_color;
    self.accountLabel.textAlignment = NSTextAlignmentRight;
    self.accountLabel.numberOfLines = YES;
    [self.mainView addSubview:self.accountLabel];
    
}

- (CGFloat)viewWithModel:(XPShopOrderModel *)model 
                viewType:(XPOrderProductListType) type
              showStatus:(BOOL)isShowStatus {
    
    [self.mainView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.itemsArray = [NSMutableArray array];
    self.itemsSelectedArray = [NSMutableArray array];
    
    CGFloat bottom = 0;
    NSArray *array = [XPShopOrderProductListModel mj_objectArrayWithKeyValuesArray:model.mallOrderItemListVOS];
    
    
    if (type == XPOrderProductListTypeCanSelect) {
        
        NSMutableArray *tempArray = [NSMutableArray array];
        for (int i = 0; i< array.count; i++) {
            XPShopOrderProductListModel *listModel = array[i];
            if (listModel.afterStatus.intValue == 0) {
                [tempArray addObject:listModel];
            }
        }
        array = tempArray.copy;
    }
    
    
    for (int i = 0; i<array.count; i++) {
        XPAfterSaleProductItemView *view = [[XPAfterSaleProductItemView alloc]initWithFrame:CGRectMake(12, 12 + 80*i, self.mj_w - 24, 68)];
        [self.mainView addSubview:view];
        XPShopOrderProductListModel *tempModel = array[i];
        
        if (self.showUsedActivityAndCoupon) {
            NSMutableArray *skuActivityCouponNoteVOsArray = [NSMutableArray array];
            if (tempModel.activityNote.length > 0) {
                [skuActivityCouponNoteVOsArray addObject:@{@"type":@(1),@"note":tempModel.activityNote}];
            }
            if (tempModel.couponNote.length > 0) {
                [skuActivityCouponNoteVOsArray addObject:@{@"type":@(2),@"note":tempModel.couponNote}];
            }
            tempModel.skuActivityCouponNoteVOs = skuActivityCouponNoteVOsArray;
        }
        
        if (type == XPOrderProductListTypeCanSelect) {
            view.canSelect = YES;
        }else{
            view.canSelect = NO;
        }
        
        view.selectedButton.tag = i;
        view.model = tempModel;
        [self.itemsArray addObject:view];
        bottom = MaxY(view);
    }
    
    if (type == XPOrderProductListTypeNone || type == XPOrderProductListTypeCanSelect) {
        self.mainView.frame = CGRectMake(0, 0, self.mj_w, 80 *array.count + 12);
        
    }else if (type == XPOrderProductListTypeSingle){
        [self.mainView addSubview:self.payLabel];
        if (model.pointStatus.intValue == 1) {
          
            NSString *spendAllAmountStr = [XPMoneyFormatTool formatMoneyWithNum:model.pricePoint];
            NSString *priceStr = [NSString stringWithFormat:@"实付:%@积分",spendAllAmountStr];

            self.payLabel.frame = CGRectMake(0, bottom + 16, self.mainView.mj_w - 12, 20);
            self.mainView.frame = CGRectMake(0, 0, self.mj_w, 100 *array.count + 48);
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(3, spendAllAmountStr.length)];
            
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(13),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(priceStr.length - 2,2 )];
            self.payLabel.attributedText = attr;
            
            self.mainView.frame = CGRectMake(0, 0, self.mj_w, 80 *array.count + 48);
        }else {
            NSString *payAmoutStr = [XPMoneyFormatTool formatMoneyWithNum:model.payAmount];
            NSString *spendAllAmountStr = [XPMoneyFormatTool formatMoneyWithNum:model.spendAllAmount];
            NSString *priceStr ;
            if (model.status.intValue == 1 || model.status.intValue == 5) {
                priceStr = [NSString stringWithFormat:@"待支付:￥%@",payAmoutStr];
            }else{
                priceStr = [NSString stringWithFormat:@"实付款:￥%@",spendAllAmountStr];
            }
            self.payLabel.frame = CGRectMake(0, bottom + 16, self.mainView.mj_w - 12, 20);
            self.mainView.frame = CGRectMake(0, 0, self.mj_w, 100 *array.count + 48);
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(12),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(4, 1)];
            
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(5, priceStr.length - 5)];
            self.payLabel.attributedText = attr;
            
            self.mainView.frame = CGRectMake(0, 0, self.mj_w, 80 *array.count + 48);
        }
        
        
    }else{//多行展示
        [self.mainView addSubview:self.accountLabel];
        [self.mainView addSubview:self.payLabel];
        
        CGFloat totalpayAmount = 0;
        CGFloat totalAccountAmount = 0;
        for (int i = 0 ; i<model.mallOrderItemListVOS.count; i++) {
            NSDictionary *dict = model.mallOrderItemListVOS[i];
            NSNumber *payAmount = dict[@"payAmount"];
            NSNumber *accountAmount = dict[@"accountAmount"];
            totalpayAmount = totalpayAmount + [payAmount floatValue] * 1000;
            totalAccountAmount = totalAccountAmount + [accountAmount floatValue] * 1000;
        }
        
        if (totalpayAmount == 0 && totalAccountAmount != 0) {
            NSString *totalAmountStr = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:totalAccountAmount/1000.]]];
            NSString *priceStr = [NSString stringWithFormat:@"账户支付:￥%@",totalAmountStr];
            self.accountLabel.frame = CGRectMake(0, bottom + 16, self.mainView.mj_w - 12, 20);
            self.mainView.frame = CGRectMake(0, 0, self.mj_w, 80 *array.count + 48);
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(12),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(5, 1)];
            
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(priceStr.length - totalAmountStr.length, totalAmountStr.length)];
            self.accountLabel.attributedText = attr;
            
        }else if (totalAccountAmount == 0 && totalpayAmount != 0){
            NSString *totalPayAmoutStr = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:totalpayAmount/1000.]]];
            NSString *priceStr = [NSString stringWithFormat:@"现金支付:￥%@",totalPayAmoutStr];
            self.payLabel.frame = CGRectMake(0, bottom + 16, self.mainView.mj_w - 12, 20);
            self.mainView.frame = CGRectMake(0, 0, self.mj_w, 80 *array.count + 48);
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(12),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(5, 1)];
            
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(priceStr.length - totalPayAmoutStr.length, totalPayAmoutStr.length)];
            self.payLabel.attributedText = attr;
        }else {
            NSString *totalAmountStr = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:totalAccountAmount/1000.]]];
            NSString *priceStr1 = [NSString stringWithFormat:@"账户支付:￥%@",totalAmountStr];
            NSMutableAttributedString *attr1 = [[NSMutableAttributedString alloc] initWithString:priceStr1];
            [attr1 addAttributes:@{NSFontAttributeName:kFontMedium(12),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(5, 1)];
            [attr1 addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(priceStr1.length - totalAmountStr.length, totalAmountStr.length)];
            self.accountLabel.attributedText = attr1;
            
            NSString *totalPayAmoutStr = [NSString stringWithFormat:@"%@",[XPMoneyFormatTool formatMoneyWithNum:[NSNumber numberWithFloat:totalpayAmount/1000.]]];
            NSString *priceStr2 = [NSString stringWithFormat:@"现金支付:￥%@",totalPayAmoutStr];
            NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc] initWithString:priceStr2];
            [attr2 addAttributes:@{NSFontAttributeName:kFontMedium(12),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(5, 1)];
            [attr2 addAttributes:@{NSFontAttributeName:kFontMedium(16),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(priceStr2.length - totalPayAmoutStr.length, totalPayAmoutStr.length)];
            self.payLabel.attributedText = attr2;
            
            self.payLabel.frame = CGRectMake(0, bottom + 16, self.mainView.mj_w - 12, 20);
            self.accountLabel.frame = CGRectMake(0, MaxY(self.payLabel), self.mainView.mj_w - 12, 20);
            self.mainView.frame = CGRectMake(0, 0, self.mj_w, 80 *array.count + 48 + 20);
        }
    }
    return MaxY(self.mainView);
}

- (void)tap:(UITapGestureRecognizer *)ges{
    buttonCanUseAfterOneSec(ges.view);
    
    if (self.selectedBlock) {
        self.selectedBlock();
    }
    
}

@end
