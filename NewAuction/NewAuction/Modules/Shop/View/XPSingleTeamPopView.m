//
//  XPSingleTeamPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/16.
//

#import "XPSingleTeamPopView.h"

@interface XPSingleTeamPopView ()

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *desLabel;

@property (nonatomic, strong) UIScrollView *userScrollView;

@property (nonatomic, strong) UIButton *sureButton;
@end

@implementation XPSingleTeamPopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {

    UIView *bgView = [[UIView alloc] initWithFrame:self.bounds];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    [self addSubview:bgView];
    
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.layer.cornerRadius = 20;
    mainView.layer.masksToBounds = YES;
    mainView.frame = CGRectMake(40,self.mj_h/2 - 200 , self.mj_w - 80, 332);
    self.mainView = mainView;
    [self addSubview:mainView];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel.font = kFontMedium(16);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.frame = CGRectMake(12, 12, mainView.mj_w - 24, 24);
    self.titleLabel = titleLabel;
    [mainView addSubview:titleLabel];
    
    UILabel *desLabel = [[UILabel alloc] init];
    desLabel.textColor = UIColorFromRGB(0x8A8A8A);
    desLabel.font = kFontRegular(12);
    desLabel.textAlignment = NSTextAlignmentCenter;
    desLabel.frame = CGRectMake(12, MaxY(self.titleLabel), mainView.mj_w - 24 , 20);
    desLabel.numberOfLines = NO;
    self.desLabel = desLabel;
    [mainView addSubview:desLabel];
    
    UIView *sepView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, mainView.mj_w, 1)];
    sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [mainView addSubview:sepView];
    
    UIScrollView *userScrollView = [UIScrollView new];
    userScrollView.frame = CGRectMake(0, 65, mainView.mj_w, 212);
    userScrollView.showsVerticalScrollIndicator = NO;
    self.userScrollView = userScrollView;
    [mainView addSubview:userScrollView];
    
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureButton setTitle:@"确定" forState:UIControlStateNormal];
    sureButton.titleLabel.font = kFontMedium(14);
    [sureButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    sureButton.layer.cornerRadius = 16;
    sureButton.clipsToBounds = YES;
    sureButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
    sureButton.frame = CGRectMake(57, self.mainView.mj_h - 44, self.mainView.mj_w - 114, 32);
    [sureButton addTarget:self action:@selector(sureButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.sureButton = sureButton;
    [self.mainView addSubview:sureButton];
    [sureButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :sureButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];

    self.alpha = 0;
    
}

-(void)viewViewDic:(NSDictionary *)dic{
    
    NSArray *skuTeamArray = dic[@"skuActivityGroupVOs"];
    NSDictionary *firstTeamDic = skuTeamArray.firstObject;
    
    NSArray *leaderArray = skuTeamArray.firstObject[@"skuActivityGroupUserVOs"];
    NSDictionary *leaderDic = leaderArray.firstObject;

    self.titleLabel.text = [NSString stringWithFormat:@"%@的拼单",leaderDic[@"userNickname"]];
    NSNumber *groupStatus = firstTeamDic[@"status"];
    if (groupStatus.intValue == 0) {
        NSNumber *totalNum = firstTeamDic[@"groupPeopleNumber"];
        NSNumber *currentNum = firstTeamDic[@"groupPresentNumber"];
        
        self.desLabel.text = [NSString stringWithFormat:@"还需%d人即可成团",totalNum.intValue - currentNum.intValue];
    }else{
        self.desLabel.text = [NSString stringWithFormat:@"拼团时间%@",firstTeamDic[@"groupTime"]];
    }
    
    CGFloat top = 8;
    for (int i = 0; i < leaderArray.count; i++) {
        CGFloat labelLeft = 16;
            
        NSDictionary *tempDic = leaderArray[i];
        
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(labelLeft,top, 36, 36)];
        [iconImageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(tempDic[@"userHead"])] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
        iconImageView.layer.cornerRadius = 18;
        iconImageView.layer.masksToBounds = YES;
        [self.userScrollView addSubview:iconImageView];
        
        UILabel *nameLabel = [UILabel new];
        nameLabel.font = kFontRegular(14);
        nameLabel.textColor = app_font_color;
        nameLabel.frame = CGRectMake(MaxX(iconImageView) + 4, top, self.userScrollView.mj_w - MaxX(iconImageView)- 4 , 36);
        nameLabel.text = tempDic[@"userNickname"];
        [self.userScrollView addSubview:nameLabel];

        if (i == 0) {
            UILabel *teamLabel = [UILabel new];
            teamLabel.textColor = UIColor.whiteColor;
            teamLabel.font = kFontRegular(14);
            teamLabel.backgroundColor = UIColorFromRGB(0xFF5787);
            teamLabel.frame = CGRectMake(self.userScrollView.mj_w - 16 - 40, top + 6, 40 , 24);
            teamLabel.textAlignment = NSTextAlignmentCenter;
            teamLabel.layer.cornerRadius = 12;
            teamLabel.layer.masksToBounds = YES;
            teamLabel.text = @"团长";
            [self.userScrollView addSubview:teamLabel];
        }
        
        top = MaxY(iconImageView) + 8;
    }
    self.userScrollView.contentSize = CGSizeMake(self.mainView.mj_w, top);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1;
    }];
}

- (void)sureButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender)
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
        
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
@end
