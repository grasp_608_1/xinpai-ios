//
//  XPUserScorePopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/22.
//

#import "XPDefaultContentPopView.h"
#import "XPShopOrderConfirmModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPUserScorePopView : XPDefaultContentPopView

@property (nonatomic, strong) XPShopOrderConfirmModel *model;

@end

NS_ASSUME_NONNULL_END
