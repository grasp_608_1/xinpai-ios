//
//  XPShopOrderStatusCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import "XPShopOrderStatusCell.h"
#import "XPShopOrderPruductView.h"
#import "XPPreSaleStatusView.h"
#import "XPShopPreSaleModel.h"

@interface XPShopOrderStatusCell ()

@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) XPShopOrderPruductView *item;
//预售金额
@property (nonatomic, strong) UILabel *preMoneyLabel;
//预售状态
@property (nonatomic, strong)XPPreSaleStatusView *preSaleStatusView;
//左侧按钮
@property (nonatomic, strong) UIButton *leftButton;
//中间按钮
@property (nonatomic, strong) UIButton *centerButton;
//右侧按钮
@property (nonatomic, strong) UIButton *rightButton;
//车
@property (nonatomic, strong) UIImageView *cardImageView;
//物流状态
@property (nonatomic, strong) UILabel *statusLabel;
@end

@implementation XPShopOrderStatusCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPShopOrderStatusCell";
    XPShopOrderStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPShopOrderStatusCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    self.mainView = [UIView new];
    self.mainView.layer.cornerRadius = 4;
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(12, 12, SCREEN_WIDTH - 12 * 2, 0);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:self.mainView];

    XPShopOrderPruductView *item = [[XPShopOrderPruductView alloc] initWithFrame:CGRectMake(-12, 0, self.mainView.mj_w, 200)];
    item.showUsedActivityAndCoupon = YES;
    self.item = item;
    [self.mainView addSubview:item];
    
    self.cardImageView = [[UIImageView alloc] init];
    self.cardImageView.frame = CGRectMake(12, MaxY(self.item) + 28, 24, 24);
    [self.mainView addSubview:self.cardImageView];
    
    self.statusLabel = [UILabel new];
    self.statusLabel.font = kFontMedium(14);
    self.statusLabel.textColor = app_font_color;
    self.statusLabel.frame = CGRectMake(MaxX(self.cardImageView) + 7, self.cardImageView.mj_y, 80, 24);
    [self.mainView addSubview:self.statusLabel];
    
    self.preSaleStatusView = [[XPPreSaleStatusView alloc] initWithFrame:CGRectMake(92, MaxY(self.item) + 8, self.mainView.mj_w - 92 - 12 , 60)];
    self.preSaleStatusView.hidden = YES;
    [self.mainView addSubview:self.preSaleStatusView];
    
    self.preMoneyLabel = [UILabel new];
    self.preMoneyLabel.font = kFontRegular(14);
    self.preMoneyLabel.textColor = app_font_color;
    self.preMoneyLabel.textAlignment = NSTextAlignmentRight;
    self.preMoneyLabel.frame = CGRectMake(0, MaxY(self.preSaleStatusView) + 12, self.mainView.mj_w - 12, 24);
    self.preMoneyLabel.hidden = YES;
    [self.mainView addSubview:self.preMoneyLabel];
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.frame = CGRectMake(self.mainView.mj_w - 12 - 80, self.mainView.frame.size.height - 12 - 32, 80, 32);
    self.rightButton.titleLabel.font = kFontRegular(14);
    self.rightButton.layer.borderWidth = 1;
    self.rightButton.layer.cornerRadius = 16;
    self.rightButton.layer.masksToBounds = YES;
    [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    self.rightButton.tag = 0;
    [self.rightButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.rightButton];
    
    self.centerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.centerButton.frame = CGRectMake(self.mainView.mj_w - 92*2, self.mainView.frame.size.height - 12 - 32, 80, 32);
    self.centerButton.titleLabel.font = kFontRegular(14);
    self.centerButton.layer.borderWidth = 1;
    self.centerButton.layer.cornerRadius = 16;
    self.centerButton.layer.masksToBounds = YES;
    [self.centerButton setTitleColor:app_font_color forState:UIControlStateNormal];
    self.centerButton.layer.borderColor = UIColorFromRGB(0x8A8A8A).CGColor;
    self.centerButton.tag = 1;
    [self.centerButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.centerButton];
    
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.frame = CGRectMake(self.mainView.mj_w - 92*3, self.rightButton.mj_y, 80, 32);
    self.leftButton.titleLabel.font = kFontRegular(14);
    self.leftButton.layer.borderWidth = 1;
    self.leftButton.layer.cornerRadius = 16;
    self.leftButton.layer.masksToBounds = YES;
    [self.leftButton setTitleColor:app_font_color forState:UIControlStateNormal];
    self.leftButton.layer.borderColor = UIColorFromRGB(0x8A8A8A).CGColor;
    self.leftButton.tag = 2;
    [self.leftButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.leftButton];
    
    XPWeakSelf
    [self.item setSelectedBlock:^{
        if (weakSelf.tapBlock) {
            weakSelf.tapBlock();
        }
    }];
}

- (CGFloat)cellWithModel:(XPShopOrderModel *)model {

    CGFloat top = 0;

    if (model.presaleStatus.intValue == 1) {
        self.preSaleStatusView.hidden = NO;
        self.preMoneyLabel.hidden = NO;
        
        CGFloat productH = [self.item viewWithModel:model viewType:XPOrderProductListTypeNone showStatus:NO];
        self.item.frame = CGRectMake(-12, 0, self.mainView.mj_w, productH);
        
        NSMutableDictionary *tempOrderPrepareVO = [NSMutableDictionary dictionary];
        [tempOrderPrepareVO setObject:model.presaleDepositAmount forKey:@"presaleDepositPrice"];
        [tempOrderPrepareVO setObject:model.presaleBalanceAmount forKey:@"presaleBalancePrice"];
        [tempOrderPrepareVO setObject:model.presaleAmount forKey:@"presalePrice"];
        [tempOrderPrepareVO setObject:model.presaleBalanceStartTime forKey:@"presaleBalanceStartTime"];
        [tempOrderPrepareVO setObject:model.presaleDepositEndTime forKey:@"presaleDepositEndTime"];
        [tempOrderPrepareVO setObject:model.presaleBalanceStartTime forKey:@"presaleBalanceStartTime"];
        [tempOrderPrepareVO setObject:model.presalePayStatus forKey:@"presalePayStatus"];
        [self.preSaleStatusView viewWithDic:tempOrderPrepareVO];
        
        self.preSaleStatusView.frame = CGRectMake(92, MaxY(self.item), self.mainView.mj_w - 92 - 12 , 60);
        
        self.preMoneyLabel.frame = CGRectMake(0, MaxY(self.preSaleStatusView) + 16, self.mainView.mj_w - 12, 24);
        //预售-支付状态 0 未支付 1 已付定金未支付尾款(到达尾款支付时间) 2 已付尾款 10 已付定金未支付尾款(未到达尾款支付时间)
        NSString *money;
        NSString *moneyDesc;
        
        if (model.presalePayStatus.integerValue == 0) {
            money = [XPMoneyFormatTool formatMoneyWithNum:model.presaleDepositAmount];
            moneyDesc = [NSString stringWithFormat:@" 定金待付款:￥%@",money];
        }else if (model.presalePayStatus.integerValue == 1){
            money = [XPMoneyFormatTool formatMoneyWithNum:model.presaleBalanceAmount];
            moneyDesc = [NSString stringWithFormat:@" 需付尾款:￥%@",money];
        }else if (model.presalePayStatus.integerValue == 2){
            money = [XPMoneyFormatTool formatMoneyWithNum:model.presaleBalanceAmount];
            moneyDesc = [NSString stringWithFormat:@" 实付尾款:￥%@",money];
        }else if (model.presalePayStatus.integerValue == 10){
            money = [XPMoneyFormatTool formatMoneyWithNum:model.presaleDepositAmount];
            moneyDesc = [NSString stringWithFormat:@" 定金已付款:￥%@",money];
        }
        
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:moneyDesc];
        [attr addAttributes:@{NSFontAttributeName:kFontRegular(10),NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A)} range:NSMakeRange(moneyDesc.length - money.length - 1, 1)];
        [attr addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xFF4A4A),NSFontAttributeName:kFontMedium(14)} range:NSMakeRange(moneyDesc.length - money.length, money.length)];
        self.preMoneyLabel.attributedText = attr;
        
        top = MaxY(self.preMoneyLabel) + 16;
        
    }else{
        CGFloat productH = [self.item viewWithModel:model viewType:XPOrderProductListTypeSingle showStatus:NO];
        self.item.frame = CGRectMake(-12, 0, self.mainView.mj_w, productH);
        
        top = MaxY(self.item);
        self.preSaleStatusView.hidden = YES;
        self.preMoneyLabel.hidden = YES;
    }
        
    //订单状态 0 默认 1 待付款 2 已支付|待发货 3 待收货|已发货 4 已完成 5 已取消 6 已关闭
    NSInteger status = model.status.integerValue;
    if (status == 1) {
        
        if (model.presaleStatus.intValue == 1 && model.presalePayStatus.intValue == 10) {
            self.rightButton.hidden = YES;
            self.centerButton.hidden = YES;
            self.leftButton.hidden  = YES;
            self.cardImageView.hidden = NO;
            self.statusLabel.hidden = NO;
            
            self.statusLabel.text = @"等待付款";
            [self.rightButton setTitle:@"立即支付" forState:UIControlStateNormal];
            [self.centerButton setTitle:@"取消订单" forState:UIControlStateNormal];
            self.cardImageView.image = [UIImage imageNamed:@"xp_clock_gray"];
            self.cardImageView.frame = CGRectMake(12, top + 9, 18, 18);
            self.statusLabel.frame = CGRectMake(MaxX(self.cardImageView) + 7, top + 7, 80, 20);
            
            self.mainView.frame = CGRectMake(12,12, SCREEN_WIDTH - 12 * 2,MaxY(self.preMoneyLabel) + 16);
            
            return MaxY(self.mainView);
            
        }else {
            self.rightButton.hidden = NO;
            self.centerButton.hidden = NO;
            self.leftButton.hidden  = YES;
            self.cardImageView.hidden = NO;
            self.statusLabel.hidden = NO;
            
            self.statusLabel.text = @"等待付款";
            [self.rightButton setTitle:@"立即支付" forState:UIControlStateNormal];
            [self.centerButton setTitle:@"取消订单" forState:UIControlStateNormal];
            self.cardImageView.image = [UIImage imageNamed:@"xp_clock_gray"];
            self.cardImageView.frame = CGRectMake(12, top + 9, 18, 18);
            self.statusLabel.frame = CGRectMake(MaxX(self.cardImageView) + 7, top + 7, 80, 20);
        }   
    }else if(status == 2) {
        self.rightButton.hidden = NO;
        self.centerButton.hidden = NO;
        self.leftButton.hidden  = YES;
        self.cardImageView.hidden = NO;
        self.statusLabel.hidden = NO;
        if (model.modelStatus.intValue == 1) {
            [self.centerButton setTitle:@"去打印" forState:UIControlStateNormal];
        }else{
            [self.centerButton setTitle:@"物流信息" forState:UIControlStateNormal];
        }
        [self.rightButton setTitle:@"确认收货" forState:UIControlStateNormal];
        self.cardImageView.image  = [UIImage imageNamed:@"xp_car_head_left"];
        self.statusLabel.text = status == 2 ? @"待发货" : @"已发货";
        self.cardImageView.frame = CGRectMake(12, top + 9, 24, 24);
        self.statusLabel.frame = CGRectMake(MaxX(self.cardImageView) + 7, top + 7, 80, 24);
        
    }else if (status == 3){
        self.rightButton.hidden = NO;
        self.centerButton.hidden = NO;
        self.leftButton.hidden  = YES;
        self.cardImageView.hidden = NO;
        self.statusLabel.hidden = NO;
        
        [self.rightButton setTitle:@"确认收货" forState:UIControlStateNormal];
        [self.centerButton setTitle:@"物流信息" forState:UIControlStateNormal];
        self.cardImageView.image  = [UIImage imageNamed:@"xp_car_head_left"];
        self.statusLabel.text = status == 2 ? @"待发货" : @"已发货";
        self.cardImageView.frame = CGRectMake(12, top + 9, 24, 24);
        self.statusLabel.frame = CGRectMake(MaxX(self.cardImageView) + 7, top + 7, 80, 24);
    }else if (status == 4) {
        self.rightButton.hidden = NO;
        self.centerButton.hidden = NO;
        self.cardImageView.hidden = YES;
        self.statusLabel.hidden = NO;
        
        [self.rightButton setTitle:@"再次购买" forState:UIControlStateNormal];
        [self.centerButton setTitle:@"物流信息" forState:UIControlStateNormal];
        if ([self getCommentStatusWithModel:model]) {
            self.leftButton.hidden = NO;
            [self.leftButton setTitle:@"已评价" forState:UIControlStateNormal];
            [self.leftButton setTitleColor:UIColorFromRGB(0xB8B8B8) forState:UIControlStateNormal];
            self.leftButton.layer.borderColor = UIColorFromRGB(0xB8B8B8).CGColor;
            self.leftButton.userInteractionEnabled = NO;
        }else {
            self.leftButton.hidden = NO;
            [self.leftButton setTitle:@"待评价" forState:UIControlStateNormal];
            [self.leftButton setTitleColor:app_font_color forState:UIControlStateNormal];
            self.leftButton.layer.borderColor = UIColorFromRGB(0x8A8A8A).CGColor;
            self.leftButton.userInteractionEnabled = YES;
        }
        self.statusLabel.text = @"完成";
        self.statusLabel.frame = CGRectMake(12, top + 7, 80, 24);
    }else if (status == 5 || status == 6) {
        self.rightButton.hidden = NO;
        self.centerButton.hidden = NO;
        self.leftButton.hidden  = YES;
        self.cardImageView.hidden = YES;
        self.statusLabel.hidden = NO;
        
        [self.rightButton setTitle:@"再次购买" forState:UIControlStateNormal];
        [self.centerButton setTitle:@"删除订单" forState:UIControlStateNormal];
        if (status == 5) {
            self.statusLabel.text = @"已取消";
        }else {
            self.statusLabel.text = @"已关闭";
        }
        
        self.statusLabel.frame = CGRectMake(12, top + 7, 80, 24);
    }
    
    self.rightButton.frame = CGRectMake(self.mainView.mj_w - 12 - 80, top, 80, 32);
    self.centerButton.frame = CGRectMake(self.mainView.mj_w - 92*2, top, 80, 32);
    self.leftButton.frame = CGRectMake(self.mainView.mj_w - 92*3, top, 80, 32);
    
    self.mainView.frame = CGRectMake(12, 12, SCREEN_WIDTH - 12 * 2,MaxY(self.rightButton) + 16 );
    return MaxY(self.mainView);
    
}

- (BOOL)getCommentStatusWithModel:(XPShopOrderModel *)model{
    
    NSArray *productList = model.mallOrderItemListVOS;
    
    bool hasCommented = YES;
    for (int i = 0; i<productList.count; i++) {
        NSDictionary *dict = productList[i];
        NSNumber *stats = dict[@"productCommentStatus"];
        if (stats.intValue == 0) {
            hasCommented = NO;
            break;
        }
    }
    return hasCommented;
}


- (void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.buttonBlock) {
        self.buttonBlock(sender.tag);
    }
}

@end
