//
//  XPShopTeamPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/16.
//

#import <UIKit/UIKit.h>
#import "XPShopProductDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopTeamPopView : UIView

@property (nonatomic, copy) void(^arrowBlock)(NSDictionary *dic);

- (void)viewWithDataModel:(XPShopProductDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
