//
//  XPShopCreationProductView.h
//  NewAuction
//
//  Created by Grasp_L on 2025/1/3.
//

#import <UIKit/UIKit.h>
#import "XPShopProductDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCreationProductView : UIView
//优惠券
@property (nonatomic, copy) void(^discountBlock)(void);

- (CGFloat)viewWithDataModel:(XPShopProductDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
