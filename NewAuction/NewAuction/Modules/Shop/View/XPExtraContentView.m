//
//  XPExtraContentView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/18.
//

#import "XPExtraContentView.h"
#import "XPHitButton.h"
#import "XPLimitInputTextView.h"

@interface XPExtraContentView ()

@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView *mainView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) XPLimitInputTextView *textView;

@end

@implementation XPExtraContentView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    bgView.alpha = 0;
    self.bgView = bgView;
    [self addSubview:bgView];
    
    self.mainView = [UIView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h, SCREEN_WIDTH, 0.7 *self.mj_h);
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(18);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(0, 16,self.mj_w, 24);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    self.textView = [[XPLimitInputTextView alloc] initWithFrame:CGRectMake(12, MaxY(self.titleLabel) + 20, self.mj_w - 24, 180)];
    [self.textView configWithMaxCount:200 textViewPlaceholder: @"选填,请先和商家协商一致,付款后商家可见。"];
    [self.mainView addSubview:self.textView];
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(24, self.mainView.mj_h - 68, self.mainView.mj_w - 48, 40);
    [closeButton setTitle:@"确定" forState:UIControlStateNormal];
    closeButton.layer.cornerRadius = 20;
    closeButton.layer.masksToBounds = YES;
    closeButton.titleLabel.font = kFontMedium(16);
    [closeButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    closeButton.tag = 1;
    [closeButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:closeButton];
    
    [closeButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :closeButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
}


- (void)cancelButtonClicked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    if (sender.tag == 1) {//确定
        if (self.finishBlock) {
            self.finishBlock(self.textView.contentView.text);
        }
        
    }else {//关闭
        if (self.cancelBlock) {
            self.cancelBlock();
        }
        [self removeFromSuperview];
    }
    
    
}
-(void)showWithContent:(NSString *)contentStr Title:(NSString *)titleStr{
        
    self.textView.contentView.text = contentStr;
    self.titleLabel.text = titleStr;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bgView.alpha = 1;
        self.mainView.frame = CGRectMake(0, self.mj_h * 0.3, SCREEN_WIDTH, 0.7 *self.mj_h);
    }];
    
    
}

-(void)setCanEdit:(BOOL)canEdit {
    if (canEdit) {
        self.textView.contentView.editable = YES;
        self.textView.numLabel.hidden = NO;
    }else {
        self.textView.contentView.editable = NO;
        self.textView.numLabel.hidden = YES;
    }
}

-(void)setInputPlaceHolder:(NSString *)inputPlaceHolder{
    self.textView.contentView.placeholder = inputPlaceHolder;
}

-(void)setMaxWords:(NSInteger)maxWords {
    self.textView.maxWords = maxWords;
}

-(void)setCanInputEmoji:(BOOL)canInputEmoji {
    self.textView.canInputEmoji = canInputEmoji;
}

@end
