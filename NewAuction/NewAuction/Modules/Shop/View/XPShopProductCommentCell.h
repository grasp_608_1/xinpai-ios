//
//  XPShopProductCommentCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/11.
//

#import <UIKit/UIKit.h>
#import "XPShopProductCommentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopProductCommentCell : UITableViewCell
//0 详情页UI样式   1 反馈列表cell样式 2 站内信
@property (nonatomic, assign) NSInteger cellType;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

-(CGFloat)cellWithModel:(XPShopProductCommentModel *)model;

@end

NS_ASSUME_NONNULL_END
