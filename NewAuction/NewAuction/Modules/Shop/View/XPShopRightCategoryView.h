//
//  XPShopRightCategoryView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopRightCategoryView : UIScrollView

@property (nonatomic, copy) void(^itemSelectBlock)(NSDictionary *dic);

@property (nonatomic, strong) NSArray *dataArray;
//-(void)configWithArray:(NSArray *)array;

@end

NS_ASSUME_NONNULL_END
