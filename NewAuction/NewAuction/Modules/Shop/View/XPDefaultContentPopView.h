//
//  XPDefaultContentPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPDefaultContentPopView : UIView
//显示内容的View
@property (nonatomic, strong) UIScrollView *contentView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, copy) void(^finishBlock)(void);
/// 弹窗
/// - Parameters:
///   - titleStr: 顶部标题
///   - bottomStr: 底部标题
- (void)showPopWithTitle:(NSString *)titleStr bottomTitle:(NSString *)bottomStr;

@end

NS_ASSUME_NONNULL_END
