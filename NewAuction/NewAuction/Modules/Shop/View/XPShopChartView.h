//
//  XPShopChartView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShopChartView : UIView

@property (nonatomic, copy) void(^tapBlock)(NSDictionary *dic);

-(void)viewWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
