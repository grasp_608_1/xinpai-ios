//
//  XPInputTransView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/17.
//

#import "XPInputTransView.h"

@interface XPInputTransView ()<UITextViewDelegate,UITextFieldDelegate>

@property (nonatomic, strong) UIButton *sureButton;

@end

@implementation XPInputTransView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}


- (void)setupView {
    
    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
    
    CGFloat leftMargin = 12;
    CGFloat inputW = self.mj_w - 24 - 67;
    CGFloat inputH = 32;
    CGFloat labelW = 67;
    CGFloat inputX = 67 + 12;
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(leftMargin,16, self.mj_w - 12, 24);
    titleLabel.font = kFontMedium(16);
    titleLabel.textColor = app_font_color;
    titleLabel.text = @"寄回商品";
    [self addSubview:titleLabel];
    
    UILabel *methodLabel = [UILabel new];
    methodLabel.frame = CGRectMake(0,16, self.mj_w - 12, 24);
    methodLabel.font = kFontRegular(14);
    methodLabel.textColor = app_font_color;
    methodLabel.text = @"寄回方式:自行邮寄";
    methodLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:methodLabel];
    
    UILabel *label1 = [UILabel new];
    label1.frame = CGRectMake(leftMargin, 52, labelW, inputH);
    label1.font = kFontRegular(14);
    label1.textColor = UIColorFromRGB(0x8A8A8A);
    label1.text = @"快递公司";
    [self addSubview:label1];
    
    UITextField *companyTextField = [[UITextField alloc] initWithFrame:CGRectMake(inputX, label1.mj_y,inputW,inputH)];
    companyTextField.font = kFontRegular(14);
    companyTextField.textColor = UIColorFromRGB(0x3A3C41);
    companyTextField.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [self addSubview:companyTextField];
    companyTextField.userInteractionEnabled = NO;
    companyTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, 32)];
    companyTextField.leftViewMode = UITextFieldViewModeAlways;
    companyTextField.text = @"快递公司名称";
    companyTextField.layer.cornerRadius = 4;
    self.companyTextField = companyTextField;
    
    XPHitButton *arrowButton4 = [[XPHitButton alloc] init];
    arrowButton4.hitRect = 40;
    [arrowButton4 addTarget:self action:@selector(arrowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [arrowButton4 setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    arrowButton4.tag = 3;
    arrowButton4.frame = CGRectMake(self.mj_w - 16 - 18, 59, 18, 18);
    [self addSubview:arrowButton4];
    
    UILabel *label2 = [UILabel new];
    label2.frame = CGRectMake(leftMargin, MaxY(self.companyTextField) + 12, labelW, inputH);
    label2.font = kFontRegular(14);
    label2.textColor = UIColorFromRGB(0x8A8A8A);
    label2.text = @"快递单号";
    [self addSubview:label2];
    
    UITextField *orderTextField = [[UITextField alloc] initWithFrame:CGRectMake(inputX, label2.mj_y,inputW,inputH)];
    orderTextField.font = kFontRegular(14);
    orderTextField.textColor = UIColorFromRGB(0x3A3C41);
    orderTextField.backgroundColor = UIColorFromRGB(0xF2F2F2);
    orderTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, 32)];
    orderTextField.leftViewMode = UITextFieldViewModeAlways;
    orderTextField.placeholder = @"请输入快递单号";
    orderTextField.layer.cornerRadius = 4;
    orderTextField.keyboardType = UIKeyboardTypeURL;
    orderTextField.delegate = self;
    self.orderTextField = orderTextField;
    [self addSubview:orderTextField];
    
    UILabel *label3 = [UILabel new];
    label3.frame = CGRectMake(leftMargin, MaxY(self.orderTextField) + 12, labelW, inputH);
    label3.font = kFontRegular(14);
    label3.textColor = UIColorFromRGB(0x8A8A8A);
    label3.layer.cornerRadius = 4;
    label3.text = @"寄出地址";
    [self addSubview:label3];
    
    SZTextView *locationTextView = [[SZTextView alloc] initWithFrame:CGRectMake(inputX, label3.mj_y,inputW,inputH * 3)];
    locationTextView.font = kFontRegular(14);
    locationTextView.textColor = UIColorFromRGB(0x3A3C41);
    locationTextView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    locationTextView.placeholder = @"请输入寄出地址";
    locationTextView.layer.cornerRadius = 4;
    locationTextView.delegate = self;
    self.locationTextView = locationTextView;
    [self addSubview:locationTextView];
    
    UILabel *label4 = [UILabel new];
    label4.frame = CGRectMake(leftMargin, MaxY(self.locationTextView) + 12, labelW, inputH);
    label4.font = kFontRegular(14);
    label4.textColor = UIColorFromRGB(0x8A8A8A);
    label4.text = @"邮寄地址";
    [self addSubview:label4];
    
    UILabel *companyLocationLabel = [UILabel new];
    companyLocationLabel.frame = CGRectMake(inputX,MaxY(self.locationTextView) + 12, labelW, 0);
    companyLocationLabel.font = kFontRegular(14);
    companyLocationLabel.textColor = UIColorFromRGB(0x3A3C41);
    companyLocationLabel.numberOfLines = NO;
    self.companyLocationLabel = companyLocationLabel;
    [self addSubview:companyLocationLabel];
    
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sureButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
    sureButton.frame = CGRectMake(self.mj_w - 12 - 80, MaxY(self.companyLocationLabel) + 16, 80, 32);
    sureButton.layer.cornerRadius = 16;
    [sureButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    sureButton.titleLabel.font = kFontMedium(14);
    [sureButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [sureButton setTitle:@"确认提交" forState:UIControlStateNormal];
    self.sureButton = sureButton;
    [self addSubview:sureButton];
    
}

- (CGFloat)viewWithModel:(XPShopAfterSaleModel *)model {
    
    CGFloat inputW = self.mj_w - 24 - 67;
    CGFloat inputX = 67 + 12;
    
    self.companyLocationLabel.text = model.mailingAddress;
    CGFloat labelH = [self.companyLocationLabel.text boundingRectWithSize:CGSizeMake(inputW, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.companyLocationLabel.font} context:nil].size.height;
    self.companyLocationLabel.frame = CGRectMake(inputX,MaxY(self.locationTextView) + 12, inputW, labelH + 10);
    self.sureButton.frame = CGRectMake(self.mj_w - 12 - 80, MaxY(self.companyLocationLabel) + 16, 80, 32);
    
    return MaxY(self.sureButton) + 16;
    
}

- (void)arrowButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.arrowBlock) {
        self.arrowBlock();
    }
}

-(void)setLogisticsModel:(XPLogisticsModel *)logisticsModel {
    _logisticsModel = logisticsModel;
    self.companyTextField.text = logisticsModel.deliveryCompany;
    
}

- (void)bottomButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if ([self.companyTextField.text isEqualToString:@"快递公司名称"]) {
        
        [[XPShowTipsTool shareInstance] showMsg:@"请选择快递公司" ToView:k_keyWindow];
        
    }else if (self.orderTextField.text.length == 0 || [self.orderTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
        
        [[XPShowTipsTool shareInstance] showMsg:@"请输入快递单号" ToView:k_keyWindow];
        
    }else if (self.locationTextView.text.length == 0 || [self.locationTextView.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0){
        
        [[XPShowTipsTool shareInstance] showMsg:@"请输入邮寄地址" ToView:k_keyWindow];
        
    } else {
        if (self.submitBlock) {
            self.submitBlock(self.logisticsModel, self.locationTextView.text,self.orderTextField.text);
        }
    }
    
}

#pragma mark --- textView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([XPSwiftTool.new containsEmoji:text]) {
        return NO;
    }else {
        return YES;
    }
}

#pragma mark --- textfield Delegate
//限制只能输入金额
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ([XPSwiftTool.new containsEmoji:string]) {
        return NO;
    }else {
        return YES;
    }
    
}
@end
