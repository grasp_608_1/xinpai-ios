//
//  XPAfterSaleProductView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/16.
//

#import "XPAfterSaleProductView.h"
#import "XPAfterSaleProductItemView.h"

@implementation XPAfterSaleProductView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.edgeInset = UIEdgeInsetsZero;
}
-(void)setEdgeInset:(UIEdgeInsets)edgeInset {
    _edgeInset = edgeInset;
}
-(void)setShowSepView:(_Bool)showSepView {
    _showSepView = showSepView;
}


- (CGFloat)viewWithModel:(XPShopOrderModel *)model {
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.selectArray = [NSMutableArray array];
    
    NSArray *array = [XPShopOrderProductListModel mj_objectArrayWithKeyValuesArray:model.mallOrderItemListVOS];
    CGFloat viewH = 68;
    CGFloat contentH = viewH + self.edgeInset.top + self.edgeInset.bottom;
    for (int i = 0; i<array.count; i++) {
        
        XPAfterSaleProductItemView *view = [[XPAfterSaleProductItemView alloc]initWithFrame:CGRectMake(self.edgeInset.left, contentH * i + self.edgeInset.top , self.mj_w - self.edgeInset.left - self.edgeInset.right, viewH)];
        [self addSubview:view];
        
        XPShopOrderProductListModel *tempModel = array[i];
        
        if (self.showUsedActivityAndCoupon) {
            NSMutableArray *skuActivityCouponNoteVOsArray = [NSMutableArray array];
            if (tempModel.activityNote.length > 0) {
                [skuActivityCouponNoteVOsArray addObject:@{@"type":@(1),@"note":tempModel.activityNote}];
            }
            if (tempModel.couponNote.length > 0) {
                [skuActivityCouponNoteVOsArray addObject:@{@"type":@(2),@"note":tempModel.couponNote}];
            }
            tempModel.skuActivityCouponNoteVOs = skuActivityCouponNoteVOsArray;
        }
        
        view.selectedButton.tag = i;
        if (self.showSelectButton) {
            view.canSelect = YES;
        }else {
            view.canSelect = NO;
        }
        view.model = tempModel;
        
        if (i != 0 && self.showSepView) {
            UIView *sepView = [UIView new];
            sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
            sepView.frame = CGRectMake(self.edgeInset.left,contentH * i, self.mj_w - self.edgeInset.left - self.edgeInset.right, 1);
            [self addSubview:sepView];
        }
        XPWeakSelf
        [view setSelectBlock:^(XPShopOrderProductListModel *model, _Bool select) {
            if (select) {
                [weakSelf.selectArray addObject:model];
            }else {
                [weakSelf.selectArray removeObject:model];
            }
        }];
    }
    return array.count * contentH;
    
}

- (CGFloat)viewWithAfterSaleApplyModel:(XPShopOrderModel *)model {
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.selectArray = [NSMutableArray array];

    NSMutableArray *tempArray = [NSMutableArray array];
    for (int i = 0; i< model.mallOrderItemListVOS.count; i++) {
        NSDictionary *dic = model.mallOrderItemListVOS[i];
        if ([[dic objectForKey:@"afterStatus"] isEqualToNumber:@(0)]) {
            [tempArray addObject:dic];
        }
    }
    
    NSArray *array = [XPShopOrderProductListModel mj_objectArrayWithKeyValuesArray:tempArray.copy];
    CGFloat viewH = 68;
    CGFloat contentH = viewH + self.edgeInset.top + self.edgeInset.bottom;
    for (int i = 0; i<array.count; i++) {
        
        XPAfterSaleProductItemView *view = [[XPAfterSaleProductItemView alloc]initWithFrame:CGRectMake(self.edgeInset.left, contentH * i + self.edgeInset.top , self.mj_w - self.edgeInset.left - self.edgeInset.right, viewH)];
        view.status = model.status.intValue;
        [self addSubview:view];
        
        XPShopOrderProductListModel *tempModel = array[i];
        view.selectedButton.tag = i;
        if (self.showSelectButton) {
            view.canSelect = YES;
        }else {
            view.canSelect = NO;
        }
        view.model = tempModel;
        
        if (i != 0 && self.showSepView) {
            UIView *sepView = [UIView new];
            sepView.backgroundColor = UIColorFromRGB(0xF2F2F2);
            sepView.frame = CGRectMake(self.edgeInset.left,contentH * i, self.mj_w - self.edgeInset.left - self.edgeInset.right, 1);
            [self addSubview:sepView];
        }
        XPWeakSelf
        [view setSelectBlock:^(XPShopOrderProductListModel *model, _Bool select) {
            if (select) {
                [weakSelf.selectArray addObject:model];
            }else {
                [weakSelf.selectArray removeObject:model];
            }
        }];
    }
    return array.count * contentH;
    
}


-(void)setShowSelectButton:(_Bool)showSelectButton {
    _showSelectButton = showSelectButton;
}

@end
