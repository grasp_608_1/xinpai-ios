//
//  XPShopTeamView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/12.
//

#import <UIKit/UIKit.h>
#import "XPShopProductDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopTeamView : UIView
//加入回调
@property (nonatomic, copy) void(^joinBlock)(NSDictionary *dic);
//拼团倒计时回调
@property (nonatomic, copy) void(^countDownBlock)(void);
//数据源
- (CGFloat)viewWithDataModel:(XPShopProductDetailModel *)model;
//清空
- (void)clear;
@end

NS_ASSUME_NONNULL_END
