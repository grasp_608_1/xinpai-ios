//
//  XPSelectItemCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/22.
//

#import <UIKit/UIKit.h>
#import "XPShopOrderConfirmModel.h"
#import "XPHitButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPSelectItemCell : UITableViewCell
//选中按钮
@property (nonatomic, strong) XPHitButton *dotButton;

@property (nonatomic, strong) XPShopOrderConfirmModel *model;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
