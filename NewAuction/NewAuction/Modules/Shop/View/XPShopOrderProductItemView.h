//
//  XPShopOrderProductItemView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import <UIKit/UIKit.h>
#import "XPShopOrderProductListModel.h"
#import "XPHitButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopOrderProductItemView : UIView
//选择按钮
@property (nonatomic, strong) XPHitButton *selectedButton;

//是否显示商品状态
@property (nonatomic, assign) bool showStatus;
//是否可选择
@property (nonatomic, assign) bool canSelect;

@property (nonatomic, copy) void(^selectBlock)(NSInteger index,bool select);
//1 显示商品状态
@property (nonatomic, assign) NSInteger status;

@property (nonatomic, strong) XPShopOrderProductListModel *model;
@end

NS_ASSUME_NONNULL_END
