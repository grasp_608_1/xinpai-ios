//
//  XPShopDetailBottomView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/10.
//

#import "XPShopDetailBottomView.h"

@interface XPShopDetailBottomView ()

@property (nonatomic, strong) UIImageView *starImageView;

@property (nonatomic, strong) UILabel *starLabel;
@end

@implementation XPShopDetailBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
}

-(void)configUIWithModel:(XPShopProductDetailModel *)model isShowCreation:(BOOL)isShow{
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSArray *dataArray = @[
    @{
        @"image":@"xp_shopProduct_uncollect",
        @"title":@"收藏"
    },@{
        @"image":@"xp_custom_service",
        @"title":@"客服"
    },
    @{
        @"image":@"xp_shop_car",
        @"title":@"购物车"
    },
    ];
    
    CGFloat itemW = 30;
    CGFloat itemH = 40;
    //造物商品 拼团 预售
    for (int idx = 0; idx< ((model.modelStatus.intValue == 1 || model.groupStatus.intValue == 1 || model.presaleStatus.intValue == 1) ? 2 : 1); idx++) {
        NSDictionary *dic = dataArray[idx];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(26 + (itemW + 18) * idx,4, itemW, itemH);
        [self addSubview:button];
        
        UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(2, 3, 24, 24)];
        imageV.image = [UIImage imageNamed:dic[@"image"]];
        [button addSubview:imageV];
        
        UILabel *titleLabel = [UILabel new];
        titleLabel.font = kFontMedium(10);
        titleLabel.textColor = app_font_color;
        titleLabel.frame = CGRectMake(0, 26, button.mj_w, 14);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = dic[@"title"];
        [button addSubview:titleLabel];
        button.tag = idx;
        [button addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
        if (idx == 0) {
            self.starLabel = titleLabel;
            self.starImageView = imageV;
        }
    }
    
    UIButton *carButton = [UIButton buttonWithType:UIButtonTypeCustom];
    carButton.frame = CGRectMake(SCREEN_WIDTH - 200 - 100, 4, 100, 40);
    carButton.layer.cornerRadius = 20;
    carButton.backgroundColor = RGBACOLOR(110, 107, 255, 0.15);
    [carButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    carButton.titleLabel.font = kFontMedium(14);
    carButton.tag = 3;
    [carButton setTitle:@"加入购物车" forState:UIControlStateNormal];
    [carButton addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:carButton];
    
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [buyButton setTitle:@"立即购买" forState:UIControlStateNormal];
    [buyButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    buyButton.titleLabel.font = kFontMedium(14);
    buyButton.layer.cornerRadius = 20;
    buyButton.layer.masksToBounds = YES;
    buyButton.frame = CGRectMake(SCREEN_WIDTH - 91 - 100, 4, 100, 40);
    buyButton.tag = 4;
    [buyButton addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:buyButton];
    [buyButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :buyButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    UIButton *takeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [takeButton setTitle:@"提货" forState:UIControlStateNormal];
    [takeButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    takeButton.titleLabel.font = kFontMedium(14);
    takeButton.layer.cornerRadius = 20;
    takeButton.layer.masksToBounds = YES;
    takeButton.frame = CGRectMake(SCREEN_WIDTH - 75 - 12, 4, 75, 40);
    takeButton.tag = 5;
    [takeButton addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:takeButton];
    [takeButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :takeButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    NSDictionary *productInfo = model.productSkuShortInfoVO;
    
    UIButton *preMoneyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [preMoneyButton setTitle:[NSString stringWithFormat:@"定金￥%@",[XPMoneyFormatTool formatMoneyWithNum:productInfo[@"skuPrepareVO"][@"presaleDepositPrice"]]] forState:UIControlStateNormal];
    [preMoneyButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    preMoneyButton.titleLabel.font = kFontMedium(14);
    preMoneyButton.layer.cornerRadius = 20;
    preMoneyButton.layer.masksToBounds = YES;
    preMoneyButton.frame = CGRectMake(SCREEN_WIDTH - 100 - 12, 4, 100, 40);
    preMoneyButton.tag = 6;
    [preMoneyButton addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:preMoneyButton];
    [preMoneyButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :preMoneyButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    NSString *directStr;
    if (model.groupStatus.intValue == 1) {
        directStr = @"单独购买";
    }else if (model.presaleStatus.intValue == 1){
        directStr = @"购买现货";
    }
    
    
    UIButton *directBuyButton = [self getButtonWithtop:[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:productInfo[@"priceSale"]]] Desc:directStr];
    directBuyButton.backgroundColor = UIColorFromRGB(0xB7B5FF);
    directBuyButton.frame = CGRectMake(SCREEN_WIDTH - 120 - 100, 4, 100, 40);
    directBuyButton.tag = 4;//与购买一样
    [self addSubview:directBuyButton];
    
    UIButton *teamButton = [self getButtonWithtop:[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:productInfo[@"skuGroupVO"][@"groupPrice"]]] Desc:@"拼团专享"];
    teamButton.frame = CGRectMake(SCREEN_WIDTH - 100 - 12, 4, 100, 40);
    teamButton.tag = 7;//团购
    [self addSubview:teamButton];
    [teamButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :teamButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
    if (model.modelStatus.intValue == 1 || model.pointStatus.intValue == 1) {//造物 积分兑换商品
        takeButton.hidden = YES;
        carButton.hidden = YES;
        directBuyButton.hidden = YES;
        teamButton.hidden = YES;
        buyButton.hidden = NO;
        preMoneyButton.hidden = YES;
        buyButton.frame = CGRectMake(SCREEN_WIDTH - 12 - 100, 4, 100, 40);
    }else if (model.groupStatus.intValue == 1){//拼团
        takeButton.hidden = YES;
        carButton.hidden = YES;
        directBuyButton.hidden = NO;
        teamButton.hidden = NO;
        buyButton.hidden = YES;
        preMoneyButton.hidden = YES;
        buyButton.frame = CGRectMake(SCREEN_WIDTH - 12 - 100, 4, 100, 40);
    }else if(model.presaleStatus.intValue == 1){//预售
        takeButton.hidden = YES;
        carButton.hidden = YES;
        directBuyButton.hidden = NO;
        preMoneyButton.hidden = NO;
        teamButton.hidden = YES;
        buyButton.hidden = YES;
        buyButton.frame = CGRectMake(SCREEN_WIDTH - 12 - 100, 4, 100, 40);
    }else {//正常商品
        directBuyButton.hidden = YES;
        teamButton.hidden = YES;
        preMoneyButton.hidden = YES;
        takeButton.hidden = NO;
        carButton.hidden = NO;
        buyButton.hidden = NO;
        buyButton.frame = CGRectMake(SCREEN_WIDTH - 91 - 100, 4, 100, 40);
    }
    if (APPTYPE == 1) {
        takeButton.hidden = YES;
        buyButton.frame = CGRectMake(SCREEN_WIDTH - 12 - 100, 4, 100, 40);
        carButton.frame = CGRectMake(SCREEN_WIDTH - 100 - 12 - 12 - 100, 4, 100, 40);
    }
}


- (UIButton *)getButtonWithtop:(NSString *)priceStr Desc:(NSString *)descString {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.layer.cornerRadius = 20;
    button.layer.masksToBounds = YES;
    button.frame = CGRectMake(0, 4, 100, 40);
    [button addTarget:self action:@selector(itemButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontRegular(16);
    titleLabel.textColor = UIColor.whiteColor;
    titleLabel.text = priceStr;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.frame = CGRectMake(0, 2, button.mj_w, 18);
    [button addSubview:titleLabel];
    
    UILabel *descLabel = [UILabel new];
    descLabel.font = kFontRegular(16);
    descLabel.textColor = UIColor.whiteColor;
    descLabel.text = descString;
    descLabel.textAlignment = NSTextAlignmentCenter;
    descLabel.frame = CGRectMake(0, 20, button.mj_w, 18);
    [button addSubview:descLabel];
    
    return button;
}


- (void)itemButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.itemClickedBlock) {
        self.itemClickedBlock(sender.tag);
    }
}

-(void)setHasCollected:(BOOL)hasCollected {
    _hasCollected = hasCollected;
    if (hasCollected) {
        self.starImageView.image = [UIImage imageNamed:@"xp_shopProduct_collected"];
        self.starLabel.text = @"已收藏";
    }else{
        self.starImageView.image = [UIImage imageNamed:@"xp_shopProduct_uncollect"];
        self.starLabel.text = @"收藏";
    }
}
@end
