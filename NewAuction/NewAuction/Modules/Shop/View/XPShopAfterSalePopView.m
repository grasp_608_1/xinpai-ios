//
//  XPShopAfterSalePopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import "XPShopAfterSalePopView.h"
#import "XPHitButton.h"
#import "XPAfterSaleProductView.h"

@interface XPShopAfterSalePopView ()

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UIScrollView *contentView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *topView;
//商品列表
@property (nonatomic, strong) XPAfterSaleProductView *item;

@property (nonatomic, strong) NSMutableArray *buttonsArray;

@property (nonatomic, strong) XPShopOrderModel *orderMode;

@end

@implementation XPShopAfterSalePopView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    self.buttonsArray  = [NSMutableArray array];
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    bgView.alpha = 0;
    self.bgView = bgView;
    [self addSubview:bgView];
    
    self.mainView = [UIScrollView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h, SCREEN_WIDTH, 0.7 *self.mj_h - 48 - kBottomHeight);
    self.mainView.backgroundColor = UIColorFromRGB(0xF2F2F2);
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(18);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(0, 16,self.mj_w, 24);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"选择售后类型";
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    NSArray *downArray = @[@"仅退款",@"退货退款",@"换货"];
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(12, 56, self.mj_w - 24, 68)];
    topView.backgroundColor = UIColor.whiteColor;
    topView.layer.cornerRadius = 8;
    self.topView = topView;
    [self.mainView addSubview:topView];
    for (int j = 0; j< downArray.count; j++) {
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightButton setTitle:downArray[j] forState:UIControlStateNormal];
        [rightButton setTitleColor:app_font_color forState:UIControlStateNormal];
        [rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateSelected];
        rightButton.frame = CGRectMake(12 + 112*j,16,100,40);
        rightButton.layer.cornerRadius = 20;
        rightButton.layer.borderWidth = 1;
        rightButton.layer.borderColor = UIColorFromRGB(0xB8B8B8).CGColor;
        rightButton.layer.masksToBounds = YES;
        rightButton.titleLabel.font = kFontRegular(14);
        rightButton.tag = j;
        [rightButton addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [topView addSubview:rightButton];
        [self.buttonsArray addObject:rightButton];
    }
    
    UIScrollView *contentView =  [[UIScrollView alloc] initWithFrame:CGRectMake(0, MaxY(topView) + 12, self.mainView.mj_w, self.mainView.mj_h - MaxY(topView) - 12 )];
    self.contentView = contentView;
    [self.mainView addSubview:contentView];
    
    XPAfterSaleProductView *item = [[XPAfterSaleProductView alloc] initWithFrame:CGRectMake(12, MaxY(topView) + 12, self.mainView.mj_w - 24, 112)];
    item.showUsedActivityAndCoupon = YES;
    item.edgeInset = UIEdgeInsetsMake(12, 12, 0, 12);
    item.backgroundColor = UIColor.whiteColor;
    self.item = item;
    item.layer.cornerRadius = 8;
    item.layer.masksToBounds = YES;
    [contentView addSubview:item];

    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,self.mj_h - 48 - kBottomHeight, self.mj_w, 48 + kBottomHeight);
    bottomView.backgroundColor = UIColor.whiteColor;
    [self addSubview:bottomView];
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [nextButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    
    nextButton.frame = CGRectMake(24,4,bottomView.mj_w - 48,40);
    nextButton.layer.cornerRadius = 20;
    nextButton.layer.masksToBounds = YES;
    [nextButton addTarget:self action:@selector(nextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:nextButton];
    [nextButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :nextButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
    
}

-(void)viewWithModel:(XPShopOrderModel *)model{
    
    self.orderMode = model;
    int status = model.status.intValue;
    if (status == 2) {
        UIButton *button = self.buttonsArray[1];
        button.userInteractionEnabled = NO;
        [button setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
        button.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
        
        UIButton *button2 = self.buttonsArray[2];
        button2.userInteractionEnabled = NO;
        [button2 setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
        button2.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
    }else {
        UIButton *button2 = self.buttonsArray[2];
        button2.userInteractionEnabled = NO;
        [button2 setTitleColor:UIColorFromRGB(0xDEDEDE) forState:UIControlStateNormal];
        button2.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
    }
    
    self.item.showSelectButton = YES;
//    if (model.status.intValue > 2) {
//        self.item.showSelectButton = YES;
//    }else {
//        self.item.showSelectButton = NO;
//    }
    
    CGFloat itemH = [self.item viewWithAfterSaleApplyModel:model];
    self.item.frame = CGRectMake(12, 0, self.mainView.mj_w - 24, itemH + 12);
    self.contentView.contentSize = CGSizeMake(self.mainView.mj_w, self.item.mj_h);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bgView.alpha = 1;
        self.mainView.frame = CGRectMake(0, self.mj_h * 0.3, SCREEN_WIDTH, 0.7 *self.mj_h - 48 - kBottomHeight);
    }];

}

- (void)cancelButtonClicked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    
    [self removeFromSuperview];
}

- (void)bottomButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    for (UIView *view in self.topView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            if (btn.tag == 2) {
                continue;
            }
            if (sender.tag == view.tag) {
                btn.layer.borderColor =  UIColorFromRGB(0x6E6BFF).CGColor;
                btn.selected = YES;
            }else {
                btn.layer.borderColor =  UIColorFromRGB(0xB8B8B8).CGColor;
                btn.selected = NO;
            }
        }
    }
}

- (void)nextButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    NSInteger index = -1;
    
    for (UIView *view in self.topView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            if (btn.selected == YES) {
                index = btn.tag;
                break;
            }
        }
    }
    
    if (index == -1) {
        [[XPShowTipsTool shareInstance] showMsg:@"请先选择售后类型" ToView:k_keyWindow];
        
    }else {
        
        if (self.orderMode.status.intValue > 1) {
            
            if (self.finishBlock) {
                self.finishBlock(index,self.item.selectArray);
            }
        }else {
            if (self.finishBlock) {
                self.finishBlock(index,nil);
            }
        }
        
        
    }
    
}

@end
