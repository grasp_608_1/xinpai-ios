//
//  XPShopLeftCategoryView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/7.
//

#import "XPShopLeftCategoryView.h"

@interface XPShopLeftCategoryView ()

@property (nonatomic, copy) NSArray *titleArray;

@property (nonatomic, strong) UIImageView *downLineView;

@property (nonatomic, assign)NSInteger lastIndex;

@end

@implementation XPShopLeftCategoryView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.showsVerticalScrollIndicator = NO;
    self.bounces = YES;
    self.lastIndex = 1;
}

-(void)configWithtitleArray:(NSArray *)titleArray {
    self.titleArray = titleArray;
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
   
    //下划线
    UIImageView *downLineView = [[UIImageView alloc] init];
    downLineView.image = [UIImage imageNamed:@"xp_line_left_pink"];
    [self addSubview:downLineView];
    self.downLineView = downLineView;
    
    CGFloat maxBottom = 0.0;
    CGFloat margin = 0;
    CGFloat btnH = 52;
    CGFloat btnW = self.mj_w;
    for (int i = 0; i<titleArray.count; i++) {
        NSString *titleStr =TO_STR(titleArray[i][@"name"]);
        
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:titleStr forState:UIControlStateNormal];
        [self addSubview:btn];
        
        btn.frame = CGRectMake(0, i*btnH, btnW, btnH);
        btn.tag = i + 1;
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            self.downLineView.frame = [self getLineFrameWithButton:btn];
            [btn setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontMedium(16);
        }else {
            [btn setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontRegular(14);
        }
        maxBottom = MaxY(btn) ;
    }
    
    maxBottom = maxBottom + margin;
    self.contentSize = CGSizeMake(self.mj_w,maxBottom);
    
    
}

- (void)btnClicked:(UIButton *)sender {
    
    if (sender.tag == _lastIndex) {
        return;
    }
    _lastIndex = sender.tag;
    
    for (UIView *btn  in self.subviews) {
        
        if ([btn isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)btn;
            if (sender.tag == button.tag) {
                [button setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
                button.titleLabel.font = kFontMedium(16);
                self.downLineView.frame = [self getLineFrameWithButton:button];
            }else {
                [button setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
                button.titleLabel.font = kFontRegular(14);
            }
        }
    }
    
    if (self.tapBlock) {
        self.tapBlock(sender.tag);
    }
    
}

//计算下划线的frame
- (CGRect)getLineFrameWithButton:(UIButton *)btn {
    CGSize titleSize = [btn.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:btn.titleLabel.font}];
    
    CGFloat lineX = btn.center.x - titleSize.width/2;
    CGFloat lineY = btn.center.y + titleSize.height/2 - 7;
    
    return CGRectMake(lineX, lineY, 34, 7);
}

@end
