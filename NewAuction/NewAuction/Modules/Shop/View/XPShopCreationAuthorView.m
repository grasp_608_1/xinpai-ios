//
//  XPShopCreationAuthorView.m
//  NewAuction
//
//  Created by Grasp_L on 2025/1/3.
//

#import "XPShopCreationAuthorView.h"

@interface XPShopCreationAuthorView ()

@property (nonatomic, strong) UIView *mainView;
//用户头像
@property (nonatomic, strong) UIImageView *userIcon;
//用户名称
@property (nonatomic, strong) UILabel *userNickNameLabel;
//用户名称
@property (nonatomic, strong) UILabel *descLabel;
//徽章
@property (nonatomic, strong) UIView *badgeView;
@end

@implementation XPShopCreationAuthorView


-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 0, self.mj_w - 12*2, 72);
    mainView.clipsToBounds = YES;
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.layer.cornerRadius = 8;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.mainView addGestureRecognizer:tap];
    
    
    self.userIcon = [UIImageView new];
    self.userIcon.layer.cornerRadius = 20;
    self.userIcon.layer.masksToBounds = YES;
    [self.mainView addSubview:self.userIcon];
    
    self.userNickNameLabel = [UILabel new];
    self.userNickNameLabel.font = kFontMedium(16);
    self.userNickNameLabel.textColor = app_font_color;
    [self.mainView addSubview:self.userNickNameLabel];
    
    self.descLabel = [UILabel new];
    self.descLabel.font = kFontMedium(12);
    self.descLabel.textColor = app_font_color_8A;
    [self.mainView addSubview:self.descLabel];
    
    self.badgeView = [UIView new];
    [self.mainView addSubview:self.badgeView];
    
}

-(void)viewWithDataModel:(XPShopProductDetailModel *)model{
    
    NSDictionary *workDic = model.productSkuShortInfoVO[@"worksSkuVO"];
    
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:@"userHead"] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    self.userIcon.frame = CGRectMake(12, 16, 40, 40);
    
    NSString *userName = [NSString stringWithFormat:@"作者: %@",workDic[@"userNickname"]];
    self.userNickNameLabel.text = userName;
    CGFloat nameW = [userName sizeWithAttributes:@{NSFontAttributeName:self.userNickNameLabel.font}].width;
    
    self.userNickNameLabel.frame = CGRectMake(MaxX(self.userIcon) + 4, self.userIcon.frame.origin.y, nameW, 22);
    
    self.descLabel.text = TO_STR(workDic[@"userIntroduce"]);
    self.descLabel.frame = CGRectMake(MaxX(self.userIcon) + 4, MaxY(self.userNickNameLabel) + 4, self.mainView.mj_w - 68, 14);
    
    NSArray *titleArray = [NSArray array];
    NSString *badgePics = workDic[@"badgePics"];
    CGFloat imageWH = 16;
    if (badgePics.length > 0) {
        titleArray = [badgePics componentsSeparatedByString:@","];
    }
    self.badgeView.frame = CGRectMake(MaxX(self.userNickNameLabel) + 4, self.userIcon.frame.origin.y + 3, imageWH * titleArray.count, imageWH);
    [self.badgeView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (titleArray.count > 0) {
       
        for (int i = 0; i< titleArray.count; i++) {
            UIImageView *iconImageView = [[UIImageView alloc] init];
            iconImageView.frame = CGRectMake(imageWH * i, 0, imageWH, imageWH);
            [iconImageView sd_setImageWithURL:[NSURL URLWithString:titleArray[i]] placeholderImage:[UIImage imageNamed:@""]];
            [self.badgeView addSubview:iconImageView];
        }
    }
}

-(void)tap:(UITapGestureRecognizer *)ges {
    buttonCanUseAfterOneSec(ges.view);
    if (self.tapBlock) {
        self.tapBlock();
    }
}

@end
