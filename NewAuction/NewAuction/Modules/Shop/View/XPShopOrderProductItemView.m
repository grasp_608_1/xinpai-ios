//
//  XPShopOrderProductItemView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import "XPShopOrderProductItemView.h"
#import "XPShopProductListView.h"
#import "XPShopOrderProductItemView.h"

@interface XPShopOrderProductItemView ()
@property (nonatomic, strong) UIView *mainView;
//产品图片
@property (nonatomic, strong) UIImageView *pImageView;
//产品名称
@property (nonatomic, strong) UILabel *pTitleLabel;
//列表详情
@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;
@property (nonatomic, strong) UILabel *label3;
//商品状态标签
@property (nonatomic, strong) UILabel *statusLabel;
@end

@implementation XPShopOrderProductItemView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.mainView = [UIView new];
    self.mainView.layer.cornerRadius = 4;
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, 0, self.mj_w, 112);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [self addSubview:self.mainView];

    XPHitButton *selectButton = [[XPHitButton alloc] init];
    selectButton.frame = CGRectMake(12, 50, 20, 20);
    [selectButton setBackgroundImage:[UIImage imageNamed:@"xp_xbox_unselect"] forState:UIControlStateNormal];
    [selectButton setBackgroundImage:[UIImage imageNamed:@"xp_xbox_selected"] forState:UIControlStateSelected];
    selectButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [selectButton addTarget:self action:@selector(selectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:selectButton];
    selectButton.hitRect = 40;
    selectButton.hidden = YES;
    self.selectedButton = selectButton;
    selectButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(12, 16, 80, 80);
    self.pImageView.layer.cornerRadius = 4;
    self.pImageView.layer.masksToBounds = YES;
    [self.mainView addSubview:self.pImageView];
    
    self.statusLabel = [UILabel new];
    self.statusLabel.font = kFontRegular(12);
    self.statusLabel.textColor = UIColor.whiteColor;
    self.statusLabel.backgroundColor = UIColorFromRGB(0x666A71);
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    self.statusLabel.frame = CGRectMake(0, self.pImageView.mj_h - 20, self.pImageView.mj_w, 20);
    [self.pImageView addSubview:self.statusLabel];
    
    CGFloat LabelX = MaxX(self.pImageView) + 12;
    CGFloat LabelW = self.mainView.mj_w - LabelX  - 16;
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontMedium(14);
    self.pTitleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.pTitleLabel.frame = CGRectMake(LabelX, 16, LabelW, 24);
    [self.mainView addSubview:_pTitleLabel];
    
    self.label1 = [UILabel new];
    self.label1.font = kFontRegular(14);
    self.label1.textColor = UIColorFromRGB(0x8A8A8A);
    self.label1.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel), LabelW, 24);
    [self.mainView addSubview:self.label1];
    
    self.label2 = [UILabel new];
    self.label2.font = kFontMedium(14);
    self.label2.textColor = app_font_color;
    self.label2.frame = CGRectMake(LabelX, MaxY(self.label1)+ 4, LabelW, 20);
    [self.mainView addSubview:self.label2];
    
    self.label3 = [UILabel new];
    self.label3.font = kFontRegular(14);
    self.label3.textColor = UIColorFromRGB(0x8A8A8A);
    self.label3.frame = self.label2.frame;
    self.label3.textAlignment = NSTextAlignmentRight;
    [self.mainView addSubview:self.label3];
}

- (void)setModel:(XPShopOrderProductListModel *)model{
    
    if (self.canSelect) {
        self.selectedButton.hidden = NO;
        
        self.pImageView.frame = CGRectMake(44, 16, 68, 68);
        
        CGFloat LabelX = MaxX(self.pImageView) + 12;
        CGFloat LabelW = self.mainView.mj_w - LabelX  - 16;
        self.pTitleLabel.frame = CGRectMake(LabelX, 16, LabelW, 24);
        self.statusLabel.frame = CGRectMake(0, self.pImageView.mj_h - 20, self.pImageView.mj_w, 20);
        self.pTitleLabel.frame = CGRectMake(LabelX, 16, LabelW, 24);
        self.label1.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel), LabelW, 24);
        self.label2.frame = CGRectMake(LabelX, MaxY(self.label1)+ 4, LabelW, 20);
        self.label3.frame = self.label2.frame;
    }else {
        self.selectedButton.hidden = YES;
    }
    
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    self.pTitleLabel.text = model.productName;
    self.label1.text = model.attributeValues;
    self.label2.text = [NSString stringWithFormat:@"单价: ￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.productSkuAmount]];
    self.label3.text = [NSString stringWithFormat:@"共%@件",model.productQuantity];
    
    if (self.showStatus == YES) {
        self.statusLabel.hidden = NO;
        self.statusLabel.text = [self getStatus:self.status];
    }else{
        self.statusLabel.hidden = YES;
    }
}

-(void)setCanSelect:(_Bool)canSelect {
    _canSelect = canSelect;
}

- (void)selectButtonClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.selectBlock) {
        self.selectBlock(sender.tag, sender.selected);
    }
}

- (NSString *)getStatus:(NSInteger)status {
    NSString *str = @"";
    //////订单状态 0 默认 1 待付款 2 已支付|待发货 3 待收货|已发货 4 已完成 5 已取消 6 已关闭
    switch (status) {
        case 1:
            str = @"待付款";
            break;
        case 2:
            str = @"待发货";
            break;
        case 3:
            str = @"待收货";
            break;
        case 4:
            str = @"已完成";
            break;
        case 5:
            str = @"已取消";
            break;
        case 6:
            str = @"已关闭";
            break;
            
        default:
            str = @"未知";
            break;
    }
    return str;
}


@end
