//
//  XPShopStatusTopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import <UIKit/UIKit.h>
#import "XPShopOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopStatusTopView : UIButton

/// <#Description#>
/// - Parameters:
///   - status: //1 待付款  2 已完成  3 已取消  4 待收货
///   - timeInterval: 代付款时 1 的倒计时,其他状态传0
- (void)viewWithShopOrderModel:(XPShopOrderModel *)model countDownBlock:( nullable void(^)( void))completeBlock;

@end

NS_ASSUME_NONNULL_END
