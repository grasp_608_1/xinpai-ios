//
//  XPShopCollectionViewCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/25.
//

#import "XPShopCollectionViewCell.h"

@interface XPShopCollectionViewCell ()
//商品图片
@property (nonatomic, strong) UIImageView *imageView;
//3d图标
@property (nonatomic, strong) UIImageView *signImageView;
//商品名称
@property (nonatomic, strong) UILabel *titleLabel;
//商品购买价格
@property (nonatomic, strong) UILabel *priceSaleLabel;
//商品提货价格
@property (nonatomic, strong) UILabel *priceCostLabel;
//好评率
@property (nonatomic, strong) UILabel *rateLabel;
//评价
@property (nonatomic, strong) UILabel *commentLabel;
//标签
@property (nonatomic, strong) UIScrollView *signView;

@end

@implementation XPShopCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = UIColorFromRGB(0xF7F4FF);
        self.layer.cornerRadius = 4;
        self.clipsToBounds = YES;
        [self  setupViews];
        
    }
    
    return self;
}

-(void)setupViews
{
    CGFloat margin = 8;
    CGFloat cellW = self.bounds.size.width;
    self.imageView = [[UIImageView alloc] init];
    self.imageView.frame = CGRectMake(0, 0,cellW ,cellW);
    self.imageView.image = [UIImage imageNamed:@"xp_pd_default"];
    [self addSubview:self.imageView];
    
    self.signImageView = [[UIImageView alloc] init];
    self.signImageView.frame = CGRectMake(cellW - 45,0,45 ,30);
    self.signImageView.image = [UIImage imageNamed:@"xp_sign_3d"];
    self.signImageView.hidden = YES;
    [self.imageView addSubview:self.signImageView];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.frame = CGRectMake(margin, MaxY(self.imageView) + margin, cellW - 2 * margin, 20);
    self.titleLabel.font = kFontMedium(14);
    self.titleLabel.numberOfLines = NO;
    self.titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    [self addSubview:self.titleLabel];
    
    self.priceSaleLabel = [UILabel new];
    self.priceSaleLabel.frame = CGRectMake(margin,MaxY(self.titleLabel) + 4, cellW - 2 * margin, 20);
    self.priceSaleLabel.font = kFontRegular(16);
    self.priceSaleLabel.textColor = UIColorFromRGB(0xFF4A4A);
    [self addSubview:self.priceSaleLabel];
    
    self.priceCostLabel = [UILabel new];
    self.priceCostLabel.frame = CGRectMake(margin,MaxY(self.priceSaleLabel) + 4, cellW - 2 * margin, 20);
    self.priceCostLabel.font = kFontRegular(10);
    self.priceCostLabel.textColor = app_font_color_8A;
    [self addSubview:self.priceCostLabel];
    
    self.rateLabel = [UILabel new];
    self.rateLabel.frame = CGRectMake(margin,MaxY(self.priceCostLabel) + 4, cellW - 2 * margin, 16);
    self.rateLabel.font = kFontRegular(12);
    self.rateLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.rateLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.rateLabel];
    self.rateLabel.hidden = YES;
    
    self.commentLabel = [UILabel new];
    self.commentLabel.frame = self.rateLabel.frame;
    self.commentLabel.font = kFontRegular(12);
    self.commentLabel.textColor = UIColorFromRGB(0x8A8A8A);
    [self addSubview:self.commentLabel];
    self.commentLabel.hidden = YES;
    
    self.signView = [[UIScrollView alloc] init];
    self.signView.frame = CGRectMake(12, MaxY(self.priceCostLabel) + 4,self.mj_w -12,16);
    [self addSubview:self.signView];
    
}

- (CGFloat)viewWithModel:(XPShopModel *)model{
    
    CGFloat margin = 8;
    CGFloat imageWH = (SCREEN_WIDTH - 36)/2.;
    self.imageView.frame = CGRectMake(0, 0, imageWH, imageWH);
    
    CGFloat titleH = [model.skuName boundingRectWithSize:CGSizeMake(imageWH - 2 * margin, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.titleLabel.font} context:nil].size.height;

    self.titleLabel.frame = CGRectMake(margin, MaxY(self.imageView) + margin, imageWH - 2 * margin, titleH > 40 ? 40:titleH);
    self.titleLabel.text = model.skuName;
    
    self.priceSaleLabel.frame = CGRectMake(margin,MaxY(self.titleLabel) + 4, imageWH - 2 * margin, 20);
   
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.pic]] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    if (model.modelStatus.intValue == 1) {
        self.signImageView.hidden = NO;
        NSString *priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceSale]];
        NSMutableAttributedString *attr9 = [[NSMutableAttributedString alloc] initWithString:priceStr];
        [attr9 addAttributes:@{NSFontAttributeName:kFontMedium(10)} range:NSMakeRange(0,1)];
        self.priceSaleLabel.attributedText = attr9;
        self.priceCostLabel.hidden = YES;
        
        return MaxY(self.priceSaleLabel) + 8;
    }else {
        self.priceCostLabel.hidden = NO;
        self.signImageView.hidden = YES;
    }
    
    NSString *priceSale = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceSale]];
    
    NSString *priceCostStr = [NSString stringWithFormat:@"提货价:￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.priceCost]];
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceSale];
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(10)} range:NSMakeRange(0,1)];
   
    self.priceSaleLabel.attributedText = attr;
    
    self.priceCostLabel.text = priceCostStr;
    self.priceCostLabel.frame = CGRectMake(margin,MaxY(self.priceSaleLabel) + 4, imageWH - 2 * margin, 20);
    
    [self.signView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSMutableArray *titleArray = [NSMutableArray array];
    if (model.activityNote.length > 0) {
        [titleArray addObject:model.activityNote];
    }
//    if (model.freightNote.length > 0) {
//        [titleArray addObject:model.freightNote];
//    }
    if (titleArray.count == 0) {
        return MaxY(self.priceCostLabel) + 8;
    }
    
    CGFloat left = 0;
    for (int i = 0; i<titleArray.count; i++) {
        NSString *str = titleArray[i];
        UILabel *label = [UILabel new];
        label.text = str;
        label.font = kFontMedium(10);
        if (i == 0) {
            label.textColor = UIColorFromRGB(0xF55B19);
        }else {
            label.textColor = UIColorFromRGB(0xFF3F6D);
        }
        [self.signView addSubview:label];
        CGFloat labelW = [str sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
        label.frame = CGRectMake(left,0,labelW,self.signView.mj_h);
        left = MaxX(label) + 4;
    }
    
    self.signView.frame = CGRectMake(12, MaxY(self.priceCostLabel) + 4,self.mj_w -12,16);
    return MaxY(self.signView) + 8;
    
}

@end

