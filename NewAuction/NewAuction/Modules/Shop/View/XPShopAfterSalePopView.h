//
//  XPShopAfterSalePopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import <UIKit/UIKit.h>
#import "XPShopOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopAfterSalePopView : UIView

@property (nonatomic, copy) void(^finishBlock)(NSInteger index,NSArray *productModelArray);

-(void)viewWithModel:(XPShopOrderModel *)model;
@end

NS_ASSUME_NONNULL_END
