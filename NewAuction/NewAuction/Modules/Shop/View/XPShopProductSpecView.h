//
//  XPShopProductSpecView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/9.
//

#import <UIKit/UIKit.h>
#import "XPAdressModel.h"
#import "XPShopProductDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopProductSpecView : UIView
//选规格
@property (nonatomic, copy) void(^specBlock)(NSInteger index);


- (CGFloat)viewWithDataModel:(XPShopProductDetailModel *)model;

@property (nonatomic, strong) XPAdressModel *addressModel;

@end

NS_ASSUME_NONNULL_END
