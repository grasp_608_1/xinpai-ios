//
//  XPAfterSaleMoneyPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/18.
//

#import <UIKit/UIKit.h>
#import "XPShopAfterSaleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPAfterSaleMoneyPopView : UIView

@property (nonatomic, copy) void(^finishBlock)(NSInteger index);

-(void)viewWithModel:(XPShopAfterSaleModel *)model;

- (void)listViewWithModel:(XPShopAfterSaleModel *)model;

@end

NS_ASSUME_NONNULL_END
