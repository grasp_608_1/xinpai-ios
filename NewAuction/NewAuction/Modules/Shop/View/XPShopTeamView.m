//
//  XPShopTeamView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/12.
//

#import "XPShopTeamView.h"
#import "XPWeakTimer.h"
#import "UIButton+parameter.h"
#import "XPShopTeamPopView.h"
#import "XPShopGroupModel.h"

@interface XPShopTeamView ()
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//分割线
@property (nonatomic, strong) UIView *sepLine;

@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UIView *view1;
@property (nonatomic, strong) UIView *view2;
@property (nonatomic, strong) UIView *view3;

@property (nonatomic, strong) NSMutableArray *teamArray;
//顶部当前显示
@property (nonatomic, assign) NSInteger currentIndex;
//计时器
@property (nonatomic,strong) XPWeakTimer * oneMinTimer;

@property (nonatomic, assign) NSInteger pastTime;

@property (nonatomic, strong)XPShopProductDetailModel *detailModel;

@property (nonatomic, strong) NSMutableArray *viewsArray;

@end

@implementation XPShopTeamView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {

    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    self.clipsToBounds = YES;
    _oneMinTimer = [XPWeakTimer shareTimer];
    self.viewsArray = [NSMutableArray array];
    
    self.pastTime = 0;
    self.teamArray = [NSMutableArray array];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.textColor = app_font_color;
    self.titleLabel.font = kFontMedium(14);
    self.titleLabel.frame = CGRectMake(12, 8, self.mj_w - 12, 24);
    self.titleLabel.userInteractionEnabled = YES;
    [self addSubview:self.titleLabel];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.titleLabel addGestureRecognizer:tap];
    
    
    self.sepLine = [[UIView alloc] init];
    self.sepLine.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.sepLine.frame = CGRectMake(0, 40, self.mj_w, 1);
    [self addSubview:self.sepLine];
    
    self.mainView = [UIView new];
    self.mainView.clipsToBounds = YES;
    [self addSubview:self.mainView];
}

- (CGFloat)viewWithDataModel:(XPShopProductDetailModel *)model {
    [self clear];
    
    [self.mainView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _detailModel = model;
    XPShopGroupModel *groupModel = [XPShopGroupModel mj_objectWithKeyValues:model.productSkuShortInfoVO[@"skuGroupVO"]];
    
    self.titleLabel.text = [NSString stringWithFormat:@"%@人正在抢，现在参与即可抢到",groupModel.groupPeopleNumberTotal];
    self.teamArray = groupModel.skuActivityGroupVOs.mutableCopy;

    if (self.teamArray.count == 1) {//只有一个拼团
        self.mainView.frame = CGRectMake(0, 52, self.mj_w, 40 + 12);
        
        UIView *view = [self createTeamWithDic:self.teamArray.firstObject];
        view.frame = CGRectMake(0, 12, self.mainView.mj_w, 28);
        self.view1 = view;
        [self.mainView addSubview:view];
        
        
    }else if (self.teamArray.count == 2){//只有两个拼团
        self.mainView.frame = CGRectMake(0, 52, self.mj_w, 92);
        
        UIView *view = [self createTeamWithDic:self.teamArray[0]];
        view.frame = CGRectMake(0, 12, self.mainView.mj_w, 28);
        self.view1 = view;
        [self.mainView addSubview:view];
        
        UIView *view2 = [self createTeamWithDic:self.teamArray[1]];
        view2.frame = CGRectMake(0, 52, self.mainView.mj_w, 28);
        self.view2 = view2;
        [self.mainView addSubview:view2];
        
    }else{//两个以上的拼团
        
        if (self.teamArray.count < 3) {
            return 0;
        }
        
        self.mainView.frame = CGRectMake(0, 52, self.mj_w, 92);
        UIView *view = [self createTeamWithDic:self.teamArray[0]];
        view.frame = CGRectMake(0, 12, self.mainView.mj_w, 28);
        [self.mainView addSubview:view];
        
        UIView *view2 = [self createTeamWithDic:self.teamArray[1]];
        view2.frame = CGRectMake(0, 52, self.mainView.mj_w, 28);
        
        [self.mainView addSubview:view2];
        
        UIView *view3 = [self createTeamWithDic:self.teamArray[2]];
        view3.frame = CGRectMake(0, 92, self.mainView.mj_w, 28);
        
        [self.mainView addSubview:view3];
        
        [self.viewsArray addObject:view];
        [self.viewsArray addObject:view2];
        [self.viewsArray addObject:view3];
        

    }
    [self startTimer];
    return MaxY(self.mainView);
}



#pragma mark -- 倒计时
-(void)startTimer{
    
    self.pastTime = 0;
    [self.oneMinTimer startTimerWithTime:1 target:self selector:@selector(countTime) userInfo:nil repeats:YES];
}
-(void)countTime{
    
    self.pastTime++;
    
    if (self.pastTime%3 == 0 && self.teamArray.count > 2) {
        [UIView animateWithDuration:0.35 animations:^{
            
            UIView *view1 = self.viewsArray[0];
            UIView *view2 = self.viewsArray[1];
            UIView *view3 = self.viewsArray[2];
            
            view1.frame = CGRectMake(0, -28, self.mainView.mj_w, 28);
            view2.frame = CGRectMake(0, 12, self.mainView.mj_w, 28);
            view3.frame = CGRectMake(0, 52, self.mainView.mj_w, 28);
            
        }completion:^(BOOL finished) {
            
            id obj = self.teamArray.firstObject;
            [self.teamArray removeObjectAtIndex:0];
            [self.teamArray addObject:obj];
            
            UIView *view = [self createTeamWithDic:self.teamArray[0]];
            view.frame = CGRectMake(0, 92, self.mainView.mj_w, 28);
            [self.mainView addSubview:view];
            UIView *dissmissView = [self.viewsArray objectAtIndex:0];
            [self.viewsArray removeObjectAtIndex:0];
            [dissmissView removeFromSuperview];
            [self.viewsArray addObject:view];
            
        }];
    }
    //刷新计时器
    [self freshUI];
}



- (void)freshUI{
    
    if (self.teamArray.count == 1) {
        UILabel *timeLabel1 = [self.view1 viewWithTag:99];
        NSNumber *remainTimeNun = self.teamArray[0][@"remainingTimeLong"];
        NSInteger remainTime = remainTimeNun.integerValue - self.pastTime;
        timeLabel1.text = [self getTimeWithSecond:remainTime];
    }else if(self.teamArray.count == 2){
        UILabel *timeLabel1 = [self.view1 viewWithTag:99];
        NSNumber *remainTimeNun = self.teamArray[0][@"remainingTimeLong"];
        NSInteger remainTime = remainTimeNun.integerValue - self.pastTime;
        timeLabel1.text = [self getTimeWithSecond:remainTime];
        
        UILabel *timeLabel2 = [self.view2 viewWithTag:99];
        NSNumber *remainTimeNun2 = self.teamArray[1][@"remainingTimeLong"];
        NSInteger remainTime2 = remainTimeNun2.integerValue - self.pastTime;
        timeLabel2.text = [self getTimeWithSecond:remainTime2];
    }else {
        if (self.teamArray.count < 3 ) {
            return;
        }
        
        for (int j = 0; j<3; j++) {
            UIView *view1 = self.viewsArray[j];
            UILabel *timeLabel1 = [view1 viewWithTag:99];
            UIButton *joinButton = [view1 viewWithTag:88];
            NSNumber *remainTimeNun = joinButton.attachDic[@"remainingTimeLong"];
            NSInteger remainTime = remainTimeNun.integerValue - self.pastTime;
            timeLabel1.text = [self getTimeWithSecond:remainTime];
        }
    }
}

-(UIView *)createTeamWithDic:(NSDictionary *)dic{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.mainView.mj_w, 28)];
    NSArray *iconArray = dic[@"skuActivityGroupUserVOs"];
    CGFloat left = 0;
    CGFloat imageWH = 28;
    NSInteger maxCount = iconArray.count >3 ? 3:iconArray.count;
    for (int i = 0; i<maxCount; i++) {
        UIImageView *imageView = [UIImageView new];
        [imageView sd_setImageWithURL:[NSURL URLWithString:TO_STR(iconArray[i][@"userHead"])] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
        imageView.frame = CGRectMake(12 + 24*i,0,imageWH, imageWH);
        imageView.layer.cornerRadius = 14;
        imageView.layer.masksToBounds = YES;
        imageView.tag = i;
        [view addSubview:imageView];
        
        if (i == maxCount - 1) {
            left = MaxX(imageView);
        }
    }
    
    UILabel *nameLabel = [UILabel new];
    nameLabel.font = kFontRegular(14);
    nameLabel.textColor = app_font_color;
    nameLabel.tag = 77;
    [view addSubview:nameLabel];
    if (iconArray.count > 0) {
        NSDictionary *userDic = iconArray.firstObject;
        nameLabel.text = userDic[@"userNickname"];
    }

    UIButton *joinButton = [UIButton buttonWithType:UIButtonTypeCustom];
    joinButton.frame = CGRectMake(self.mainView.mj_w - 12 - 68, 2, 68, 24);
    joinButton.backgroundColor = UIColorFromRGB(0xFF3EB2);
    joinButton.layer.cornerRadius = 12;
    [joinButton setTitle:@"即刻参团" forState:UIControlStateNormal];
    [joinButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    joinButton.titleLabel.font = kFontRegular(14);
    joinButton.tag = 88;
    [joinButton addTarget:self action:@selector(joinButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:joinButton];
    joinButton.attachDic = dic;
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = kFontRegular(14);
    timeLabel.textColor = app_font_color;
    timeLabel.textAlignment = NSTextAlignmentRight;
    timeLabel.tag = 99;
    
    [view addSubview:timeLabel];
    
    NSNumber *remainTimeNun = dic[@"remainingTimeLong"];
    NSInteger remainTime = remainTimeNun.integerValue - self.pastTime;
    timeLabel.text =[self getTimeWithSecond:remainTime];
    CGFloat timeW = [timeLabel.text sizeWithAttributes:@{NSFontAttributeName:nameLabel.font}].width;
    timeLabel.frame = CGRectMake(self.mainView.mj_w - 83 - timeW - 30, 0, timeW + 30, view.mj_h);
    
    nameLabel.frame = CGRectMake(left + 4, 0, timeLabel.mj_x - left - 4 + 30, view.mj_h);
 
    return view;
}

-(void)joinButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.joinBlock) {
        self.joinBlock(sender.attachDic);
    }
}

-(void)tap:(UITapGestureRecognizer *)ges {
    buttonCanUseAfterOneSec(ges.view);
    XPWeakSelf
    XPShopTeamPopView *popView = [[XPShopTeamPopView alloc] initWithFrame:k_keyWindow.bounds];
    [k_keyWindow addSubview:popView];
    
    [popView viewWithDataModel:self.detailModel];
    
    [popView setArrowBlock:^(NSDictionary * _Nonnull dic) {
        if (weakSelf.joinBlock) {
            weakSelf.joinBlock(dic);
        }
    }];
}

- (NSString *)getTimeWithSecond:(NSInteger )seconds{
    
    if (seconds<= 0) {
        return @"剩余00:00:00";
    }
    
    NSString *hour = [NSString stringWithFormat:@"%02ld",seconds/3600];
    NSString *min = [NSString stringWithFormat:@"%02ld",(seconds%3600)/60];
    NSString *sec = [NSString stringWithFormat:@"%02ld",seconds%60];
    return [NSString stringWithFormat:@"剩余%@:%@:%@",hour,min,sec];
}

- (void)clear{
    self.pastTime = 0;
    [self.oneMinTimer stopTimer];
}
@end
