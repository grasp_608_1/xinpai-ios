//
//  XPAfterSaleInfoView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/17.
//

#import "XPAfterSaleInfoView.h"
#import "XPHitButton.h"
#import "XPAfterSaleProductView.h"
#import "XPShopProductListView.h"

@interface XPAfterSaleInfoView ()

@property (nonatomic, strong) UIView *mainView;
//退货信息标题
@property (nonatomic, strong) UILabel *titleLabel;
//商品
@property (nonatomic, strong) XPAfterSaleProductView *item;
//评价时间
@property (nonatomic, strong) UILabel *timeLabel;
//评语
@property (nonatomic, strong) UILabel *contenLabel;
//评价图片
@property (nonatomic, strong) UIView  *commentImageView;
//付款信息
@property (nonatomic, strong) XPShopProductListView *moneyListView;
//分割线
@property (nonatomic, strong) UIView *sepView1;
@property (nonatomic, strong) UIView *sepView2;

@end

@implementation XPAfterSaleInfoView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12*2, 0);
    mainView.clipsToBounds = YES;
    mainView.layer.cornerRadius = 8;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontRegular(16);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(12, 16,self.mainView.mj_w, 24);
    titleLabel.text = @"退款信息";
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    XPAfterSaleProductView *item = [[XPAfterSaleProductView alloc] initWithFrame:CGRectMake(12, MaxY(self.titleLabel) + 12, self.mainView.mj_w -24, 68)];
    item.edgeInset = UIEdgeInsetsMake(0, 0, 12, 0);
    self.item = item;
    [self.mainView addSubview:item];
    
    UILabel *contenLabel = [UILabel new];
    contenLabel.font = kFontRegular(14);
    contenLabel.textColor = app_font_color;
    contenLabel.frame = CGRectMake(12,54,self.mainView.mj_w - 24, 0);
    contenLabel.numberOfLines = NO;
    self.contenLabel = contenLabel;
    [self.mainView addSubview:contenLabel];
    
    self.commentImageView = [[UIView alloc] init];
    self.commentImageView.frame = CGRectMake(12,MaxY(self.contenLabel) + 12,self.mainView.mj_w - 24, 0);
    [self.mainView addSubview:self.commentImageView];
    
    XPShopProductListView *moneyListView = [[XPShopProductListView alloc] initWithFrame:CGRectMake(0, MaxY(self.commentImageView) + 12, self.mainView.mj_w + 24, 0)];
    moneyListView.strokeTitle = YES;
    self.moneyListView = moneyListView;
    [self.mainView addSubview:moneyListView];
    
    //分割线1
    UIView *sepView1 = [UIView new];
    sepView1.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.sepView1 = sepView1;
    [self.mainView addSubview:sepView1];
    
    //分割线2
    UIView *sepView2 = [UIView new];
    sepView2.backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.sepView2 = sepView2;
    [self.mainView addSubview:sepView2];
}

- (CGFloat)viewWithModel:(XPShopAfterSaleModel *)model {
    
    XPShopOrderModel *orderModel = [XPShopOrderModel new];
    orderModel.mallOrderItemListVOS = model.mallOrderItemListVOS;
    //商品
    CGFloat itemH = [self.item viewWithModel:orderModel];
    self.item.frame = CGRectMake(12, MaxY(self.titleLabel) + 12, self.mainView.mj_w - 24, itemH);
    
    self.sepView1.frame = CGRectMake(12, MaxY(self.item), self.mainView.mj_w - 24, 1);
    
    self.contenLabel.text = model.descStr;
    //售后内容
    CGFloat contentH = [self.contenLabel.text boundingRectWithSize:CGSizeMake(self.contenLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.contenLabel.font} context:nil].size.height;
    self.contenLabel.frame = CGRectMake(12,MaxY(self.item) + 24,self.mainView.mj_w - 24, contentH);
    
    CGFloat bottom = MaxY(self.contenLabel);
    
    //售后上传的图片
    [self.commentImageView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSArray *array = [model.pics componentsSeparatedByString:@","];
    if (model.pics.length >0 && array.count > 0) {
        
        CGFloat margin = 12;
        CGFloat imageWH = (self.mainView.mj_w - 12 *(array.count + 1))/3;
        
        for (int i = 0; i<array.count; i++) {
            UIImageView *imageView = [UIImageView new];
            [imageView sd_setImageWithURL:[NSURL URLWithString:array[i]]];
            imageView.frame = CGRectMake(i* (imageWH + margin), 0, imageWH, imageWH);
            imageView.layer.cornerRadius = 8;
            imageView.layer.masksToBounds = YES;
            imageView.contentMode =  UIViewContentModeScaleAspectFit;
            [self.commentImageView addSubview:imageView];
        }
        self.commentImageView.frame = CGRectMake(12,bottom + 12,self.mainView.mj_w - 24, imageWH);
        bottom = MaxY(self.commentImageView);
    }else{
        self.commentImageView.frame = CGRectMake(12,MaxY(self.contenLabel) + 12,self.mainView.mj_w - 24,0);
    }
    
    self.sepView2.frame = CGRectMake(12, bottom + 12, self.mainView.mj_w - 24, 1);
    
    NSArray *titleDescArray = @[@"售后类型",@"申请时间"];
    NSArray *contentArray = @[[self getTypeStr:model.type.intValue],
                              [NSString stringWithFormat:@"%@",model.createTime]];
    CGFloat moneyH = [self.moneyListView viewWithKeyArray:titleDescArray ValueArray:contentArray];
    self.moneyListView.frame = CGRectMake(-12, bottom + 24, self.mainView.mj_w + 24, moneyH);
    
    self.mainView.frame = CGRectMake(12, 0, SCREEN_WIDTH - 12*2, MaxY(self.moneyListView) + 16);
    
    return MaxY(self.mainView);
    
}

//售后类型
- (NSString *)getTypeStr:(NSInteger) type {
    NSString *str = @"";
    switch (type) {
        case 1:
            str = @" 退款";
            break;
        case 2:
            str = @" 退款";
            break;
        case 3:
            str = @"退货退款";
            break;
        default:
            str = @"未知类型";
            break;
    }
    return str;
    
}

@end
