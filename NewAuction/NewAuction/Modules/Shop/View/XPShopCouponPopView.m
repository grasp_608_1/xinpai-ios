//
//  XPShopCouponPopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/10.
//

#import "XPShopCouponPopView.h"
#import "XPHitButton.h"
#import "XPEmptyView.h"
#import "XPShopCouponCell.h"

@interface XPShopCouponPopView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *mainView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//列表
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) XPEmptyView *emptyView;

//数据源
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation XPShopCouponPopView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {

    self.backgroundColor = UIColor.clearColor;
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    bgView.frame = self.bounds;
    bgView.alpha = 0;
    self.bgView = bgView;
    [self addSubview:bgView];
    
    self.mainView = [UIView new];
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, self.mj_h, SCREEN_WIDTH, 0.75 *self.mj_h);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [XPCommonTool cornerRadius:self.mainView andType:1 radius:16];
    [self addSubview:self.mainView];
    
    XPHitButton *cancelButton = [[XPHitButton alloc] init];
    cancelButton.hitRect = 40;
    cancelButton.frame = CGRectMake(self.mj_w - 24 - 12, 12, 24, 24);
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"xp_window_close"] forState:UIControlStateNormal];
    [self.mainView addSubview:cancelButton];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFontMedium(18);
    titleLabel.textColor = app_font_color;
    titleLabel.frame = CGRectMake(0, 16,self.mj_w, 24);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"优惠券";
    self.titleLabel = titleLabel;
    [self.mainView addSubview:titleLabel];
    
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = CGRectMake(0, 56, self.mainView.mj_w, self.mainView.mj_h - 56 - 72);
    self.tableView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainView addSubview:self.tableView];
    
    CGFloat emptyW = self.mj_w - 2 *62;
    self.emptyView = [[XPEmptyView alloc] initWithFrame:CGRectMake(62, 150, emptyW,emptyW + 27 )];
    self.emptyView.hidden = NO;
    self.tableView.hidden = YES;
    [self.emptyView configWithImage:nil Description:@"暂无可领取优惠券"];
    [self.mainView addSubview:self.emptyView];
    
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(24, self.mainView.mj_h - 68, self.mainView.mj_w - 48, 40);
    [closeButton setTitle:@"关闭" forState:UIControlStateNormal];
    closeButton.layer.cornerRadius = 20;
    closeButton.layer.masksToBounds = YES;
    closeButton.titleLabel.font = kFontMedium(16);
    [closeButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:closeButton];
    
    [closeButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :closeButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPShopCouponCell *cell = [XPShopCouponCell cellWithTableView:tableView];
    XPCouponModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    if (self.canSelected) {
        cell.useButton.hidden = YES;
    }else{
        cell.useButton.hidden = NO;
    }
    
    XPWeakSelf
    [cell setReceiveBlock:^{
        [weakSelf receiveCouponWithcouponModel:model indexPath:indexPath];
    }];
    
    return cell;
}

- (void)receiveCouponWithcouponModel:(XPCouponModel *)model indexPath:(NSIndexPath *)indexPath{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kShop_coupon_receive];
     NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"couponId"] = model.couponId;
    XPBaseViewController *controller = (XPBaseViewController *)[XPCommonTool getCurrentViewController];
    [controller startLoadingGif];
     [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
         [controller stopLoadingGif];
         if (data.isSucess) {
             XPCouponModel *tempModel = self.dataArray[indexPath.row];
             tempModel.receiveStatus = @(1);
             [self.tableView reloadData];
         }else {
             [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
         }
     }];
}


-(void)setCanSelected:(NSInteger)canSelected {
    _canSelected = canSelected;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.canSelected == 0) {
        return;
    }else if (self.canSelected == 1){
        
        XPCouponModel *model = self.dataArray[indexPath.row];
        if (self.finishBlock) {
            self.finishBlock(model);
            [self removeFromSuperview];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 112;
}

- (void)cancelButtonClicked:(UIButton *)sender {
    
    buttonCanUseAfterOneSec(sender);
    
    [self removeFromSuperview];
}

- (void)showWithDataArray:(NSArray *)mainArray{
    
    
    if (mainArray.count == 0) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
    }else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
    self.dataArray = mainArray.mutableCopy;
    [self.tableView reloadData];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bgView.alpha = 1;
        self.mainView.frame = CGRectMake(0,self.mj_h *0.25, self.mj_w, self.mj_h * 0.75);
    }];
    
}

@end
