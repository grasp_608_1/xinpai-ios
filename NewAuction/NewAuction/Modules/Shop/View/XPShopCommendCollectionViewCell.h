//
//  XPShopCommendCollectionViewCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/21.
//

#import <UIKit/UIKit.h>
#import "XPShopModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopCommendCollectionViewCell : UICollectionViewCell

- (CGFloat)cellWithModel:(XPShopModel *)model;

@end

NS_ASSUME_NONNULL_END
