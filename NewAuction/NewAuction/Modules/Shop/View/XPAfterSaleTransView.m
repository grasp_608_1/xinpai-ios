//
//  XPAfterSaleTransView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/16.
//

#import "XPAfterSaleTransView.h"
#import "XPHitButton.h"

@interface XPAfterSaleTransView ()

@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UILabel *transLabel;

@end

@implementation XPAfterSaleTransView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    self.backgroundColor = UIColor.whiteColor;
    self.layer.cornerRadius = 8;
    
    self.descLabel = [UILabel new];
    self.descLabel.font = kFontMedium(16);
    self.descLabel.textColor = app_font_color;
    self.descLabel.frame = CGRectMake(12,0,self.mj_w - 24,self.mj_h);
    [self addSubview:self.descLabel];
    
    self.transLabel = [UILabel new];
    self.transLabel.font = kFontRegular(14);
    self.transLabel.textColor = app_font_color;
    self.transLabel.frame = CGRectMake(12,0,self.mj_w - 24 - 18 - 6,self.mj_h);
    self.transLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.transLabel];
    
    XPHitButton *arrowButton4 = [[XPHitButton alloc] init];
    [arrowButton4 setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    arrowButton4.tag = 3;
    arrowButton4.frame = CGRectMake(self.mj_w - 18 - 6 - 12, 20, 18, 18);
    [self addSubview:arrowButton4];
    
}

- (void)viewWithModel:(XPShopAfterSaleModel *)model {
    NSInteger status = model.status.intValue;
    self.descLabel.text = [self getOrderStatusStr:status];
    self.transLabel.text = @"查看物流";
}

//订单退货状态
- (NSString *)getOrderStatusStr:(NSInteger) status {
    NSString *str = @"";
    ///// 售后状态 0 未提交 1 待审核(仅退款通过->5;退款退货通过->2) 2 审核成功|待归仓  3 已拒绝 4 商品回寄中 5 退款处理 中 6 已完成
    switch (status) {
        case 0:
            str = @"未提交";
            break;
        case 1:
            str = @"待审核";
            break;
        case 2:
            str = @"审核成功,待归仓";
            break;
        case 3:
            str = @"已拒绝";
            break;
        case 4:
            str = @"寄回商品正在运输中";
            break;
        case 5:
            str = @"商品已签收";
            break;
        case 6:
            str = @"已完成";
            break;
        default:
            str = @"售后处理中";
            break;
    }
    return str;
    
}
@end
