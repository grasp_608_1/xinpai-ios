//
//  XPShopCommandView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/5.
//

#import "XPShopCommandView.h"

@interface XPShopCommandView ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *iconbgImageView;
@property (nonatomic, strong) UILabel *desLabel;

@property (nonatomic, copy) NSDictionary *mainDict;
@property (nonatomic, copy) NSArray *itemArray;
@end


@implementation XPShopCommandView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    self.layer.cornerRadius = 6;
    self.layer.masksToBounds = YES;
    [self.layer addSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0.36, 0.04) 
                                                              toPoint:CGPointMake(0.5, 1)
                                                             Location:@[@(0), @(0.3f)] :self
                                                               colors:@[(__bridge id)UIColorFromRGB(0xECE3FF).CGColor,(__bridge id)UIColorFromRGB(0xFFFFFF).CGColor]]];
}

- (void)setDataDict:(NSDictionary *)dataDict{
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    self.mainDict = dataDict;
    
    self.iconbgImageView = [UIImageView new];
    self.iconbgImageView.frame = CGRectMake(12, 2, 60, 24);
//    self.iconbgImageView.backgroundColor = RandColor;
    [self.iconbgImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",dataDict[@"pic"]]]];
    self.iconbgImageView.userInteractionEnabled = YES;
    [self addSubview:self.iconbgImageView];
    
    UITapGestureRecognizer *sortTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sortTap:)];
    [self.iconbgImageView addGestureRecognizer:sortTap];
    
    CGFloat topMargin = 28;
    CGFloat bottomMargin = 24;
    CGFloat imageH = self.mj_h - topMargin - bottomMargin;
    
    NSArray *itemArray = dataDict[@"mallSubjectProductVOs"];
    self.itemArray = itemArray;
    for (int i = 0; i < itemArray.count; i++) {
        UIImageView *imageView = [UIImageView new];
        imageView.layer.cornerRadius = 4;
        imageView.layer.masksToBounds = YES;
        imageView.userInteractionEnabled = YES;
        imageView.frame = CGRectMake(self.mj_w/4. - imageH/2. + self.mj_w * i/2 , topMargin, imageH, imageH);
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        [imageView addGestureRecognizer:tap];
        imageView.tag = i;
        [self addSubview:imageView];
        
        UILabel *titleLabel = [UILabel new];
        titleLabel.font = kFontMedium(12);
        titleLabel.textColor = UIColorFromRGB(0xFF4A4A);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.frame = CGRectMake(self.mj_w/2 *i, self.mj_h - bottomMargin, self.mj_w/2., bottomMargin);
        [self addSubview:titleLabel];
        
        NSDictionary *dict = itemArray[i];
        [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",dict[@"productPicSubject"]]]];
        NSNumber *pointStatus = dict[@"pointStatus"];
        if (pointStatus && dict[@"pricePoint"] && pointStatus.intValue == 1) {
            
            NSString *priceStr = [NSString stringWithFormat:@"%@积分",[XPMoneyFormatTool formatMoneyWithNum:dict[@"pricePoint"]]];
            
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(9)} range:NSMakeRange(priceStr.length - 2, 2)];
            titleLabel.attributedText = attr;
        }else {
            NSString *priceStr = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:dict[@"priceSaleSubject"]]];
            
            NSArray *dotArray = [priceStr componentsSeparatedByString:@"."];
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
            [attr addAttributes:@{NSFontAttributeName:kFontMedium(9)} range:NSMakeRange(0, 1)];
            
            if (dotArray.count >1) {
                NSString *dotStr = dotArray.lastObject;
                [attr addAttributes:@{NSFontAttributeName:kFontMedium(9)} range:NSMakeRange(priceStr.length - dotStr.length, dotStr.length)];
            }
            
            titleLabel.attributedText = attr;
        }
        
    }
}

- (void)tap:(UITapGestureRecognizer *)sender{
    buttonCanUseAfterOneSec(sender.view);
//    NSDictionary *dict = self.itemArray[sender.view.tag];
//    if (self.itemTapBlock && dict) {
//        self.itemTapBlock(dict);
//    }
    if (self.topTapBlock && self.mainDict) {
        self.topTapBlock(self.mainDict);
    }
    
}

- (void)sortTap:(UITapGestureRecognizer *)sender{
    buttonCanUseAfterOneSec(sender.view);
    if (self.topTapBlock && self.mainDict) {
        self.topTapBlock(self.mainDict);
    }
    
}


@end
