//
//  XPShopOrderProductCommentCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/25.
//

#import <UIKit/UIKit.h>
#import "XPShopOrderCommentModel.h"
#import "XPLimitInputTextView.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopOrdeCommentItemView : UIView

@property (nonatomic, copy) void(^changeBlock)(void);

@property (nonatomic, copy) void(^textViewBlock)(void);

-(void)viewWithModel:(XPShopOrderCommentModel *)model;
//评语
@property (nonatomic, strong) XPLimitInputTextView *textView;

@end

NS_ASSUME_NONNULL_END
