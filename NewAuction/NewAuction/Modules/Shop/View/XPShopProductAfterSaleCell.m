//
//  XPShopProductAfterSaleCell.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/20.
//

#import "XPShopProductAfterSaleCell.h"
#import "XPShopOrderModel.h"
#import "XPAfterSaleProductView.h"

@interface XPShopProductAfterSaleCell ()

@property (nonatomic, strong) UIView *mainView;
//售后状态
@property (nonatomic, strong) UILabel *pTitleLabel;
//商品
@property (nonatomic, strong) XPAfterSaleProductView *item;
//售后状态View
@property (nonatomic, strong) UIView *statusView;
//售后类型描述
@property (nonatomic, strong) UILabel *statusLabel;
//退款原因
@property (nonatomic, strong) UILabel *resultLabel;
//左侧按钮
@property (nonatomic, strong) UIButton *leftButton;
//中间按钮
@property (nonatomic, strong) UIButton *centerButton;
//右侧按钮
@property (nonatomic, strong) UIButton *rightButton;

@end

@implementation XPShopProductAfterSaleCell


+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"XPShopProductAfterSaleCell";
    XPShopProductAfterSaleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[XPShopProductAfterSaleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    self.mainView = [UIView new];
    self.mainView.layer.cornerRadius = 4;
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(12, 12, SCREEN_WIDTH - 12 * 2, 201);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:self.mainView];
        
    XPAfterSaleProductView *item = [[XPAfterSaleProductView alloc] initWithFrame:CGRectMake(12, 16, self.mainView.mj_w - 24, 68)];
    item.edgeInset = UIEdgeInsetsMake(0, 0, 12, 0);
    self.item = item;
    [self.mainView addSubview:item];
    
    self.statusView = [UIView new];
    self.statusView.backgroundColor = UIColorFromRGB(0xFAFAFA);
    self.statusView.frame = CGRectMake(12, MaxY(self.item) + 18, self.mainView.mj_w - 24, 43);
    [self.mainView addSubview:self.statusView];
    
    self.statusLabel = [UILabel new];
    self.statusLabel.font = kFontMedium(14);
    self.statusLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.statusLabel.frame = CGRectMake(12, 0, self.statusView.mj_w - 12, self.statusView.mj_h);
    [self.statusView addSubview:self.statusLabel];

    self.resultLabel = [UILabel new];
    self.resultLabel.font = kFontRegular(14);
    self.resultLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.resultLabel.frame = CGRectMake(0, 0, self.statusView.mj_w - 12, self.statusView.mj_h);
    self.resultLabel.textAlignment = NSTextAlignmentRight;
    [self.statusView addSubview:self.resultLabel];
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
   
    self.rightButton.titleLabel.font = kFontRegular(14);
    self.rightButton.layer.borderWidth = 1;
    self.rightButton.layer.cornerRadius = 16;
    self.rightButton.layer.masksToBounds = YES;
    [self.rightButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    self.rightButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    self.rightButton.tag = 0;
    [self.rightButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.rightButton];
    
    self.centerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.centerButton.titleLabel.font = kFontRegular(14);
    self.centerButton.layer.borderWidth = 1;
    self.centerButton.layer.cornerRadius = 16;
    self.centerButton.layer.masksToBounds = YES;
    [self.centerButton setTitleColor:app_font_color forState:UIControlStateNormal];
    self.centerButton.layer.borderColor = UIColorFromRGB(0x8A8A8A).CGColor;
    self.centerButton.tag = 1;
    [self.centerButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.centerButton];
    
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.titleLabel.font = kFontRegular(14);
    self.leftButton.layer.borderWidth = 1;
    self.leftButton.layer.cornerRadius = 16;
    self.leftButton.layer.masksToBounds = YES;
    [self.leftButton setTitleColor:app_font_color forState:UIControlStateNormal];
    self.leftButton.layer.borderColor = UIColorFromRGB(0x8A8A8A).CGColor;
    self.leftButton.tag = 2;
    [self.leftButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.leftButton];
    
}

-(CGFloat)cellWithModel:(XPShopAfterSaleModel *)model {
    
    XPShopOrderModel *orderModel = [XPShopOrderModel new];
    orderModel.mallOrderItemListVOS = model.mallOrderItemListVOS;
    orderModel.payAmount = model.payAmount;
    orderModel.accountAmount = model.accountAmount;
    
    CGFloat itemH = [self.item viewWithModel:orderModel];
    self.item.frame = CGRectMake(12, 16, self.mainView.mj_w - 24, itemH);
    
    ///售后状态 0 未提交 1 待审核(仅退款通过->5;退款退货通过->2) 2 审核成功|待归仓  3 已拒绝 4 商品回寄中 5 退款处理 中 6 已完成
    NSInteger status = model.status.integerValue;
    
    self.statusLabel.text = [self getOrderStatusStr:status];
    self.resultLabel.text = [self getTypeStr:model.type.intValue];
    self.statusView.frame = CGRectMake(12, MaxY(self.item) + 6, self.mainView.mj_w - 24, 43);
    
    self.rightButton.frame = CGRectMake(self.mainView.mj_w - 12 - 80, MaxY(self.statusView) + 12, 80, 32);
    self.centerButton.frame = CGRectMake(self.mainView.mj_w - 92*2, self.rightButton.mj_y, 80, 32);
    self.leftButton.frame = CGRectMake(self.mainView.mj_w - 92*3, self.rightButton.mj_y, 80, 32);
    
    self.statusLabel.textColor = app_font_color;
    if (status == 1 || status == 2 || status == 4 || status == 5) {
        self.rightButton.hidden = NO;
        self.centerButton.hidden = YES;
        self.leftButton.hidden  = YES;
        
        [self.rightButton setTitle:@"联系商家" forState:UIControlStateNormal];
        
    }else if(status == 3) {
        self.rightButton.hidden = NO;
        self.centerButton.hidden = NO;
        self.leftButton.hidden  = NO;
        
        [self.leftButton setTitle:@"删除记录" forState:UIControlStateNormal];
        [self.rightButton setTitle:@"联系商家" forState:UIControlStateNormal];
        [self.centerButton setTitle:@"再次申请" forState:UIControlStateNormal];
        
    }else if ( status == 6) {
        self.rightButton.hidden = NO;
        self.centerButton.hidden = NO;
        self.leftButton.hidden  = NO;
        
        self.statusLabel.textColor = UIColorFromRGB(0x6E6BFF);
        [self.rightButton setTitle:@"联系商家" forState:UIControlStateNormal];
        [self.centerButton setTitle:@"钱款去向" forState:UIControlStateNormal];
        [self.leftButton setTitle:@"删除记录" forState:UIControlStateNormal];
    }
    self.mainView.frame = CGRectMake(12, 12, SCREEN_WIDTH - 12 * 2, MaxY(self.rightButton) + 12);
    return  self.mainView.mj_h + 12;
}

- (void)buttonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.buttonBlock) {
        self.buttonBlock(sender.tag);
    }
}

//订单退货状态
- (NSString *)getOrderStatusStr:(NSInteger) status {
    NSString *str = @"";
    ///// 售后状态 0 未提交 1 待审核(仅退款通过->5;退款退货通过->2) 2 审核成功|待归仓  3 已拒绝 4 商品回寄中 5 退款处理 中 6 已完成
    switch (status) {
        case 0:
            str = @"未提交";
            break;
        case 1:
            str = @"待审核";
            break;
        case 2:
            str = @"等待寄回商品";
            break;
        case 3:
            str = @"已拒绝";
            break;
        case 4:
            str = @"商品回寄中";
            break;
        case 5:
            str = @"退款处理中";
            break;
        case 6:
            str = @"退款成功";
            break;
        default:
            str = @"售后处理中";
            break;
    }
    return str;
    
}
//售后类型
/// 申请类型：0 默认 1 仅退款；2 退货退款
- (NSString *)getTypeStr:(NSInteger) type {
    NSString *str = @"";
    switch (type) {
        case 1:
            str = @"退款";
            break;
        case 2:
            str = @"退款";
            break;
        case 3:
            str = @"退货退款";
            break;
        
        default:
            str = @"未知类型";
            break;
    }
    return str;
    
}

@end
