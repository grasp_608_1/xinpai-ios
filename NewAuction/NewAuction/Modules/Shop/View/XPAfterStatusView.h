//
//  XPAfterStatusView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/16.
//

#import <UIKit/UIKit.h>
#import "XPShopAfterSaleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPAfterStatusView : UIView

@property (nonatomic, copy) void(^tapBlock)(void);

- (CGFloat)viewWithModel:(XPShopAfterSaleModel *)model;

@end

NS_ASSUME_NONNULL_END
