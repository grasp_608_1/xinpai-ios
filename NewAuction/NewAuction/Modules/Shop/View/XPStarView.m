//
//  XPStarView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/9.
//

#import "XPStarView.h"

@implementation XPStarView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    self.userInteractionEnabled = NO;
}

- (void)setStar:(NSInteger)star {
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    for (int i = 0; i < 5; i++) {
        UIImageView *starView = [[UIImageView alloc] initWithFrame:CGRectMake(i * (self.mj_h + 4), 0, self.mj_h, self.mj_h)];
        starView.tag = i;
        [self addSubview:starView];
        
        starView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        [starView addGestureRecognizer:tap];
        
        if (i < star) {
            if (self.canOption) {
                starView.image = [UIImage imageNamed:@"xp_comment_star_collected"];
            }else {
                starView.image = [UIImage imageNamed:@"xp_star_selected_blue"];
            }
        } else {
            starView.image = [UIImage imageNamed:@"xp_star_uncollect"];
        }
    }
}

- (void)tap:(UITapGestureRecognizer *)tap {
    buttonCanUseAfterOneSec(tap.view);
    self.star = tap.view.tag + 1;
    if (self.changeBlock) {
        self.changeBlock(tap.view.tag + 1);
    }
}

- (void)setCanOption:(BOOL)canOption {
    _canOption = canOption;
    if (canOption) {
        self.userInteractionEnabled = YES;
    }else {
        self.userInteractionEnabled = NO;
    }
}


@end
