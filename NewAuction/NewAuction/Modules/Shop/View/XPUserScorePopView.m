//
//  XPUserScorePopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/22.
//

#import "XPUserScorePopView.h"
#import "XPSelectItemCell.h"

@interface XPUserScorePopView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UILabel *scoreLabel;

@property (nonatomic, strong) UIButton *selectButton;

@property (nonatomic, strong) UITableView *tableView;


@end

@implementation XPUserScorePopView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupViews];
    }
    return self;
}
- (void)setupViews {
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.frame = self.contentView.bounds;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.contentView addSubview:self.tableView];
    
}

-(void)setModel:(XPShopOrderConfirmModel *)model {
    _model = model;
    self.titleLabel.backgroundColor = UIColor.whiteColor;
    [self showPopWithTitle:@"" bottomTitle:@"确定"];
    
    NSString *titleStr = [NSString stringWithFormat:@"     积分 (剩余:%@)",model.currentPoints];
    self.titleLabel.font = kFontRegular(14);
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:titleStr];
    [titleAttr addAttributes:@{NSFontAttributeName:kFontMedium(16)} range:NSMakeRange(0, 2)];
    self.titleLabel.attributedText = titleAttr;

    [self.tableView reloadData];

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XPSelectItemCell *cell = [XPSelectItemCell cellWithTableView:tableView];
    cell.model = self.model;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}


@end
