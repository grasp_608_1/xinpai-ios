//
//  XPShopSpecPopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/10.
//

#import <Foundation/Foundation.h>
#import "XPShopProductDetailModel.h"
#import "XPShopSpecModel.h"

typedef NS_ENUM(NSUInteger, XPProductSpecPopType) {
    XPProductSpecPopTypeNormal,//加入购物车 确定购买
    XPProductSpecPopTypeChange, //更改规格
    XPProductSpecPopTypeSure,//确定
};

NS_ASSUME_NONNULL_BEGIN

@interface XPShopSpecPopView : UIView

@property (nonatomic, copy) void(^cancelBlock)(XPShopSpecModel *model);

@property (nonatomic, copy) void(^bottomBlock)(NSInteger index,NSInteger countNum,XPShopSpecModel *model);

- (void)showWithModel:(XPShopProductDetailModel *)model ViewType:(XPProductSpecPopType)type;
//是否显示数量View  1 隐藏  0 显示 default 0
- (void)hiddenCountView:(BOOL)isHidden;

@end

NS_ASSUME_NONNULL_END
