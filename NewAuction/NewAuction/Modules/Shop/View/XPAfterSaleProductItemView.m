//
//  XPAfterSaleProductView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/16.
//

#import "XPAfterSaleProductItemView.h"

@interface XPAfterSaleProductItemView ()

@property (nonatomic, strong) UIView *mainView;
//产品图片
@property (nonatomic, strong) UIImageView *pImageView;
//商品状态标签
@property (nonatomic, strong) UILabel *statusLabel;
//商品属性
@property (nonatomic, strong) UIImageView *attrImageView;
//产品名称
@property (nonatomic, strong) UILabel *pTitleLabel;
//价格
@property (nonatomic, strong) UILabel *priceLabel;
//规格
@property (nonatomic, strong) UILabel *specLabel;
//标签
@property (nonatomic, strong) UIScrollView *signView;
//数量
@property (nonatomic, strong) UILabel *numLabel;

@end


@implementation XPAfterSaleProductItemView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.mainView = [UIView new];
    self.mainView.layer.cornerRadius = 4;
    self.mainView.clipsToBounds = YES;
    self.mainView.frame = CGRectMake(0, 0, self.mj_w, 68);
    self.mainView.backgroundColor = UIColor.whiteColor;
    [self addSubview:self.mainView];

    XPHitButton *selectButton = [[XPHitButton alloc] init];
    selectButton.frame = CGRectMake(12, 24, 20, 20);
    [selectButton setBackgroundImage:[UIImage imageNamed:@"xp_xbox_unselect"] forState:UIControlStateNormal];
    [selectButton setBackgroundImage:[UIImage imageNamed:@"xp_xbox_selected"] forState:UIControlStateSelected];
    selectButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [selectButton addTarget:self action:@selector(selectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:selectButton];
    selectButton.hitRect = 40;
    selectButton.hidden = YES;
    self.selectedButton = selectButton;
    selectButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.pImageView = [[UIImageView alloc] init];
    self.pImageView.frame = CGRectMake(0, 0, 68, 68);
    self.pImageView.layer.cornerRadius = 4;
    self.pImageView.layer.masksToBounds = YES;
    [self.mainView addSubview:self.pImageView];
    
    self.statusLabel = [UILabel new];
    self.statusLabel.font = kFontRegular(12);
    self.statusLabel.textColor = UIColor.whiteColor;
    self.statusLabel.backgroundColor = UIColorFromRGB(0x666A71);
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    self.statusLabel.frame = CGRectMake(0, self.pImageView.mj_h - 20, self.pImageView.mj_w, 20);
    self.statusLabel.hidden = YES;
    [self.pImageView addSubview:self.statusLabel];
    
    CGFloat LabelX = MaxX(self.pImageView) + 8;
    CGFloat LabelW = self.mainView.mj_w - LabelX  - 16;
    
    self.attrImageView = [[UIImageView alloc] init];
    self.attrImageView.frame = CGRectMake(LabelX, 0, 32, 16);
    self.attrImageView.layer.cornerRadius = 4;
    self.attrImageView.layer.masksToBounds = YES;
    self.attrImageView.hidden = YES;
    [self.mainView addSubview:self.attrImageView];
    
    self.pTitleLabel = [UILabel new];
    self.pTitleLabel.font = kFontMedium(14);
    self.pTitleLabel.textColor = UIColorFromRGB(0x3A3C41);
    [self.mainView addSubview:_pTitleLabel];
    
    self.priceLabel = [UILabel new];
    self.priceLabel.font = kFontMedium(14);
    self.priceLabel.textColor = app_font_color;
    [self.mainView addSubview:self.priceLabel];
    
    self.specLabel = [UILabel new];
    self.specLabel.font = kFontMedium(14);
    self.specLabel.textColor = app_font_color;
    self.specLabel.frame = CGRectMake(LabelX, MaxY(self.priceLabel)+ 4, LabelW, 20);
    [self.mainView addSubview:self.specLabel];
    
    self.numLabel = [UILabel new];
    self.numLabel.font = kFontRegular(14);
    self.numLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.numLabel.textAlignment = NSTextAlignmentRight;
    [self.mainView addSubview:self.numLabel];
    
    self.signView = [[UIScrollView alloc] init];
    self.signView.bounces = NO;
    self.signView.showsHorizontalScrollIndicator = NO;
    [self addSubview:self.signView];
}

- (void)setModel:(XPShopOrderProductListModel *)model{
    _model = model;
    
    if (_canSelect) {
        self.selectedButton.hidden = NO;
        self.pImageView.frame = CGRectMake(44, 0, 68, 68);
    }else {
        self.selectedButton.hidden = YES;
        self.pImageView.frame = CGRectMake(0, 0, 68, 68);
    }
    
    CGFloat LabelX = MaxX(self.pImageView) + 8;
    
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"xp_pd_default"]];
    
    if (self.showStatus == YES) {
        self.statusLabel.hidden = NO;
        self.statusLabel.text = [self getStatus:self.status];
    }else{
        self.statusLabel.hidden = YES;
    }
    
    NSString *price;
    if (model.pointStatus.intValue == 1) {
        price = [NSString stringWithFormat:@"%@积分",[XPMoneyFormatTool formatMoneyWithNum:model.pricePoint]];
    }else {
        price = [NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:model.productSkuAmount]];
    }
    
    CGFloat priceW = [price sizeWithAttributes:@{NSFontAttributeName:self.priceLabel.font}].width;
    self.priceLabel.text = price;
    self.priceLabel.frame = CGRectMake(self.mainView.mj_w  - priceW , 0, priceW, 20);
    
    self.pTitleLabel.text = model.productName;
    
    if (model.groupStatus.intValue == 1) {
        self.attrImageView.hidden = NO;
        self.attrImageView.frame = CGRectMake(LabelX, 2, 32, 16);
        self.attrImageView.image = [UIImage imageNamed:@"xp_circle_team"];
        self.pTitleLabel.frame = CGRectMake(MaxX(self.pImageView) + 46, 0, self.mainView.mj_w - LabelX - 34 - priceW - 12 - 6, 20);
    }else if (model.presaleStatus.intValue == 1){
        self.attrImageView.hidden = NO;
        self.attrImageView.frame = CGRectMake(LabelX, 2, 32, 16);
        self.attrImageView.image = [UIImage imageNamed:@"xp_circle_preSale"];
        self.pTitleLabel.frame = CGRectMake(MaxX(self.pImageView) + 46, 0, self.mainView.mj_w - LabelX - 34 - priceW - 12 - 6, 20);
    }else{
        self.attrImageView.hidden = YES;
        self.pTitleLabel.frame = CGRectMake(LabelX, 0, self.mainView.mj_w - LabelX - priceW - 12 - 6, 20);
    }
    
    self.specLabel.text = model.attributeValues;
    self.specLabel.frame = CGRectMake(LabelX, MaxY(self.pTitleLabel) + 4,self.mainView.mj_w - LabelX- 12 , 20);
    
    NSString *numStr = [NSString stringWithFormat:@"共%@件",model.productQuantity];;
    CGFloat numW = [numStr sizeWithAttributes:@{NSFontAttributeName: self.numLabel.font}].width;
    self.numLabel.text = numStr;
    self.numLabel.frame = CGRectMake(self.mainView.mj_w - numW , MaxY(self.specLabel) + 4, numW, 20);
    
    [self.signView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.signView.frame = CGRectMake(LabelX, self.numLabel.mj_y, self.numLabel.mj_x - LabelX- 12, 20);
    
//    NSArray *titleArray = [NSArray array];
//    NSMutableArray *tempArray = [NSMutableArray array];
    
//    if (model.activityNote.length > 0) {
//        [tempArray addObject:model.activityNote];
//    }
//    
//    if (model.skuActivityCouponNoteVOs.count > 0) {
//        
//        for (int i = 0; i< model.skuActivityCouponNoteVOs.count; i++) {
//            NSDictionary *tempDic = model.skuActivityCouponNoteVOs[i];
//            [tempArray addObject:tempDic[@"note"]];
//        }
//    }
//    titleArray = tempArray.copy;
    
    if (model.skuActivityCouponNoteVOs.count == 0) {
        self.signView.hidden = YES;
    }else {
        self.signView.hidden = NO;
        [self.signView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        CGFloat left = 0;
        for (int i = 0; i<model.skuActivityCouponNoteVOs.count; i++) {
            NSDictionary *dic = model.skuActivityCouponNoteVOs[i];
            NSString *str = TO_STR(dic[@"note"]);
            NSNumber *type = dic[@"type"];
            UILabel *label = [UILabel new];
            label.text = TO_STR(str);
            label.font = kFontMedium(10);
            if (type.intValue == 2) {
                label.textColor = UIColorFromRGB(0xF55B19);
            }else {
                label.textColor = UIColorFromRGB(0xFF3F6D);
            }
            [self.signView addSubview:label];
            CGFloat labelW = [TO_STR(str) sizeWithAttributes:@{NSFontAttributeName:label.font}].width;
            label.frame = CGRectMake(left,0,labelW,self.signView.mj_h);
            left = MaxX(label) + 4;
        }
        self.signView.contentSize = CGSizeMake(left,self.signView.mj_h);
    }
}

-(void)setCanSelect:(_Bool)canSelect {
    _canSelect = canSelect;
}

- (void)selectButtonClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.selectBlock) {
        self.selectBlock(self.model, sender.selected);
    }
}

- (NSString *)getStatus:(NSInteger)status {
    NSString *str = @"";
    //////订单状态 0 默认 1 待付款 2 已支付|待发货 3 待收货|已发货 4 已完成 5 已取消 6 已关闭
    switch (status) {
        case 1:
            str = @"待付款";
            break;
        case 2:
            str = @"待发货";
            break;
        case 3:
            str = @"待收货";
            break;
        case 4:
            str = @"已完成";
            break;
        case 5:
            str = @"已取消";
            break;
        case 6:
            str = @"已关闭";
            break;
            
        default:
            str = @"未知";
            break;
    }
    return str;
}

@end
