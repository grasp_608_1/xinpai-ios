//
//  XPShopOrderStatusCell.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/19.
//

#import <UIKit/UIKit.h>
#import "XPShopOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPShopOrderStatusCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (CGFloat)cellWithModel:(XPShopOrderModel *)model;

//全部
@property (nonatomic, assign) NSInteger cellType;

//右侧 0 中间1 左侧 1
@property (nonatomic, copy) void(^buttonBlock)(NSInteger tag);
//商品点击
@property (nonatomic, copy) void (^tapBlock)(void);

@end

NS_ASSUME_NONNULL_END
