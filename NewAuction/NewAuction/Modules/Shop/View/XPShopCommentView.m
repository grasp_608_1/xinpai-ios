//
//  XPShopCommentView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/9.
//

#import "XPShopCommentView.h"
#import "XPHitButton.h"
#import "XPStarView.h"
#import "XPShopProductCommentModel.h"

@interface XPShopCommentView ()

@property (nonatomic, strong) UIView *mainView;
//评价数量
@property (nonatomic, strong) UILabel *commentNumLabel;

@property (nonatomic, strong) XPHitButton *arrowButton;
//好评率
@property (nonatomic, strong) UILabel *commentRateLabel;
//用户头像
@property (nonatomic, strong) UIImageView  *iconImageView;
//用户昵称
@property (nonatomic, strong) UILabel *nameLabel;
//用户评价星星
@property (nonatomic, strong) XPStarView *starView;
//评语
@property (nonatomic, strong) UILabel *contenLabel;
//评价图片
@property (nonatomic, strong) UIImageView  *commentImageView;
@end

@implementation XPShopCommentView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    
    UIView *mainView = [UIView new];
    mainView.frame = CGRectMake(12, 0, self.mj_w - 12*2, 0);
    mainView.clipsToBounds = YES;
    mainView.layer.cornerRadius = 8;
    mainView.backgroundColor = UIColor.whiteColor;
    self.mainView = mainView;
    [self addSubview:mainView];
    
    UILabel *commentNumLabel = [UILabel new];
    commentNumLabel.font = kFontMedium(16);
    commentNumLabel.textColor = app_font_color;
    commentNumLabel.frame = CGRectMake(12, 12,200, 24);
    self.commentNumLabel = commentNumLabel;
    [self.mainView addSubview:commentNumLabel];
    
    UILabel *commentRateLabel = [UILabel new];
    commentRateLabel.font = kFontRegular(12);
    commentRateLabel.textColor = UIColorFromRGB(0x8A8A8A);
    commentRateLabel.textAlignment = NSTextAlignmentRight;
    commentRateLabel.frame = CGRectMake(self.mainView.mj_w - 24 - 200 - 12, 12,200, 24);
    self.commentRateLabel = commentRateLabel;
    [self.mainView addSubview:commentRateLabel];
    
    self.arrowButton = [[XPHitButton alloc] init];
    self.arrowButton.frame = CGRectMake(self.mainView.mj_w - 24 - 12, 16, 18, 18);
    self.arrowButton.hitRect = 36;
    [self.arrowButton setBackgroundImage:[UIImage imageNamed:@"Frame_7"] forState:UIControlStateNormal];
    [self.arrowButton addTarget:self action:@selector(arrowButtonButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.arrowButton];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.frame = CGRectMake(12, 48, 30, 30);
    self.iconImageView.layer.cornerRadius = 15;
    self.iconImageView.clipsToBounds = YES;
    [self.mainView addSubview:self.iconImageView];
    
    UILabel *nameLabel = [UILabel new];
    nameLabel.font = kFontRegular(12);
    nameLabel.textColor = app_font_color;
    nameLabel.frame = CGRectMake(50, 48,250, 16);
    self.nameLabel = nameLabel;
    [self.mainView addSubview:nameLabel];
    
    self.starView = [[XPStarView alloc] initWithFrame:CGRectMake(50, 70, 70, 10)];
    [self.mainView addSubview:self.starView];
    
    
    self.commentImageView = [[UIImageView alloc] init];
    self.commentImageView.frame = CGRectMake(self.mainView.mj_w - 80 - 12,48, 80, 80);
    self.commentImageView.layer.cornerRadius = 8;
    self.commentImageView.layer.masksToBounds = YES;
    [self.mainView addSubview:self.commentImageView];
    
    UILabel *contenLabel = [UILabel new];
    contenLabel.font = kFontRegular(14);
    contenLabel.textColor = app_font_color;
    contenLabel.frame = CGRectMake(12, 86,self.commentImageView.mj_x - 12, 40);
    contenLabel.numberOfLines = NO;
    self.contenLabel = contenLabel;
    [self.mainView addSubview:contenLabel];
    
}


- (CGFloat)viewWithDataModel:(XPShopProductDetailModel *)model {
    
    XPShopProductCommentModel *comentModel = [XPShopProductCommentModel mj_objectWithKeyValues:model.productCommentVO];
    self.commentNumLabel.text = [NSString stringWithFormat:@"评价（%@）",model.commentNum];
    self.commentRateLabel.text = [NSString stringWithFormat:@"好评率%@%%",model.commentRate];
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:comentModel.userHead] placeholderImage:[UIImage imageNamed:@"xp_user_icon"]];
    self.nameLabel.text = comentModel.userNickname;
    self.starView.star = [comentModel.star intValue];
    self.contenLabel.text = comentModel.content;
    
    NSString *commentImagesString = comentModel.pics;
    NSArray *urlArray = [commentImagesString componentsSeparatedByString:@","];
    
    [self.commentImageView sd_setImageWithURL:[NSURL URLWithString:urlArray.firstObject]];
    
    self.mainView.frame = CGRectMake(12, 0, self.mj_w - 12*2, 140);
    
    return 140;
}


- (void)arrowButtonButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    if (self.commentBlock) {
        self.commentBlock();
    }
}

@end
