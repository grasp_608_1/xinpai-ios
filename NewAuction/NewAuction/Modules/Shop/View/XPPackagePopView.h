//
//  XPPackagePopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPPackagePopView : UIView

@property (nonatomic, copy) void(^arrowBlock)(NSInteger tag);

- (void)viewWithArray:(NSArray *)array;

@end

NS_ASSUME_NONNULL_END
