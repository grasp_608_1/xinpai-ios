//
//  XPLoginChoseView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import "XPLoginChooseView.h"

@interface XPLoginChooseView ()
//底部横线
@property (nonatomic, strong) UIImageView *lineView;
//文字显示
@property (nonatomic, strong) UILabel *contenLabel;

@end

@implementation XPLoginChooseView

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView 
{
    self.contenLabel = [UILabel new];
    self.contenLabel.textAlignment = NSTextAlignmentCenter;
    self.contenLabel.frame = self.bounds;
    
    
    self.lineView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"xp_line_blue"]];
    self.lineView.frame = CGRectMake(self.bounds.size.width/2. - 53/2., self.bounds.size.height - 2 -8, 53, 8);
    self.lineView.hidden = YES;
    
    [self addSubview:self.lineView];
    [self addSubview:self.contenLabel];
}

- (void)setTitleStr:(NSString *)titleStr {
    self.contenLabel.text = titleStr;
}
-(void)setIsChoosed:(BOOL)isChoosed {
    if (isChoosed) {
            self.contenLabel.textColor = app_font_color;
            self.contenLabel.font = kFontBold(18);
            self.lineView.hidden = NO;
    }else {
        self.contenLabel.textColor = UIColorFromRGB(0x8A8A8A);
        self.contenLabel.font = kFontMedium(16);
        self.lineView.hidden = YES;
    }
}

@end
