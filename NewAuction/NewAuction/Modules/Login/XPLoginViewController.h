//
//  XPLoginViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import "XPBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPLoginViewController : XPBaseViewController

@property (nonatomic, assign) NSInteger modelType;

@end

NS_ASSUME_NONNULL_END
