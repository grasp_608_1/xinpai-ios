//
//  XPLoginChoseView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPLoginChooseView : UIButton
//选中状态
@property (nonatomic, assign) BOOL isChoosed;
//标题显示
@property (nonatomic, copy) NSString *titleStr;

@end

NS_ASSUME_NONNULL_END
