//
//  XPLoginViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import "XPLoginViewController.h"
#import "XPLoginChooseView.h"
#import "XPBaseWebViewViewController.h"
#import "NSTimer+JKBlocks.h"
#import "XPHitButton.h"
#import "XPUserPaySecrectController.h"
#import "XPNoMenuTextField.h"
#import "XPAlertView.h"

@interface XPLoginViewController ()<UITextViewDelegate,UITextFieldDelegate>
//账户登录按钮
@property (nonatomic, strong) XPLoginChooseView *codeTitleView;
//免密登录按钮
@property (nonatomic, strong) XPLoginChooseView *secretTitleView;

//手机号登录
@property (nonatomic, strong) UIView *phoneView;
@property (nonatomic, strong) UITextField *phoneNumTextField;

//验证码
@property (nonatomic, strong) UIView  *codeView;
@property (nonatomic, strong) UITextField *codeTextField;
//验证码点击按钮
@property (nonatomic, strong) UIButton *codeButton;
//计时器
@property(nonatomic,strong)NSTimer * oneMinTimer;

//密码登录
@property (nonatomic, strong) UIView *userSecretView;
@property (nonatomic, strong) XPNoMenuTextField *secrectTextField;
@property (nonatomic, strong) UIButton *eyesButton;

@property (nonatomic, assign) BOOL currentSelectStatus;

//隐私协议政策选择
@property (nonatomic, strong) XPHitButton *dotButton;

@property (nonatomic, strong) UITextView *policyView;

@property (nonatomic, copy) NSString *codeUUid;

@property (nonatomic, strong) UIButton *forgetButton;

//@property (nonatomic, strong) XPAlertView *alert;


@end

@implementation XPLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = UIColor.whiteColor;
    [self seupUI];
    
//    XPAlertView *alert = [[XPAlertView alloc]initWithFrame:self.view.bounds];
//    self.alert = alert;
//    [self.view addSubview:alert];
    
}

- (void)seupUI {
    UIImageView *backImageView = [[UIImageView alloc]init];
    backImageView.frame = self.view.bounds;
    backImageView.image = [UIImage imageNamed:@"xp_login_banner"];
    [self.view addSubview:backImageView];
    
    //顶部标题/banner模块
    UIImageView *topBanner = [[UIImageView alloc]init];
    topBanner.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.safeTop + 145);
    [self.view addSubview:topBanner];
    
    UILabel *topLabel = [[UILabel alloc] init];
    topLabel.numberOfLines = NO;
    topLabel.font = kFontBold(18);
    topLabel.textColor = app_font_color;
    topLabel.frame = CGRectMake(40, self.safeTop + 40, SCREEN_WIDTH - 40, 64);
    [topBanner addSubview:topLabel];
    
    if (APPTYPE == 0) {
        topLabel.text = @"您好,\n欢迎登录新拍探讨";
    }else if (APPTYPE == 1){
        topLabel.text = @"您好,\n欢迎登录语音造物";
    }
    
    
    
    //选择标题模块
    self.codeTitleView = [[XPLoginChooseView alloc] initWithFrame:CGRectMake(41, CGRectGetMaxY(topBanner.frame), 75, 24)];
    self.codeTitleView.titleStr = @"短信登录";
    [self.codeTitleView addTarget:self action:@selector(codeTitleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.codeTitleView.isChoosed = YES;
    
    self.secretTitleView = [[XPLoginChooseView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.codeTitleView.frame) + 30, CGRectGetMaxY(topBanner.frame), 75, 24)];
    self.secretTitleView.titleStr = @"账号登录";
    [self.secretTitleView addTarget:self action:@selector(secretTitleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.secretTitleView.isChoosed = NO;
    
    [self.view addSubview:self.codeTitleView];
    [self.view addSubview:self.secretTitleView];
   
    //手机号登录
    CGFloat originX = 28;
    CGRect phoneFrame =  CGRectMake(originX, CGRectGetMaxY(self.codeTitleView.frame) + 20, SCREEN_WIDTH - originX *2, 50);
    self.phoneNumTextField = [[UITextField alloc] init];
    self.phoneNumTextField.delegate = self;
    self.phoneView = [self configInputViewWithframe:phoneFrame frontImage:[UIImage imageNamed:@"user_account"] textField:self.phoneNumTextField placeHolder:@"请输入手机号"];
    self.phoneNumTextField.keyboardType =  UIKeyboardTypeNumberPad;
    [self.view addSubview:self.phoneView];
    
    //验证码登录
    CGRect codeRect = CGRectMake(originX, MaxY(self.phoneView) + 20, self.phoneView.bounds.size.width, self.phoneView.bounds.size.height);
    self.codeTextField = [[UITextField alloc] init];
    self.codeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.codeTextField.delegate = self;
    self.codeView = [self configInputViewWithframe:codeRect frontImage:[UIImage imageNamed:@"Shield_safe"] textField:self.codeTextField placeHolder:@"请输入验证码"];
    self.codeTextField.keyboardType = UIKeyboardTypeURL;
    if (@available(iOS 12.0, *)) {
        self.codeTextField.textContentType = UITextContentTypeOneTimeCode;
    }
    [self.view addSubview:self.codeView];
    
    //验证码
    self.codeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.codeButton.frame = CGRectMake(self.codeView.bounds.size.width - 100, 0, 100, self.codeView.bounds.size.height);
    [self.codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.codeButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    self.codeButton.titleLabel.font = kFontRegular(16);
    [self.codeButton addTarget:self action:@selector(codeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.codeView addSubview:self.codeButton];
   //验证码前边的的那条线
    CGRect codelineRect = CGRectMake(self.codeButton.frame.origin.x - 1, 10, 1, self.codeView.frame.size.height - 10 -8);
    UIView *codeLineView = [[UIView alloc] initWithFrame:codelineRect];
    codeLineView.backgroundColor = UIColorFromRGB(0xDEDEDE);
    [self.codeView addSubview:codeLineView];
    //重置手机验证码Frame
    CGFloat codeW = codelineRect.origin.x - 50 -13;
    self.codeTextField.frame = CGRectMake(self.codeTextField.frame.origin.x, self.codeTextField.frame.origin.y, codeW, self.codeTextField.frame.size.height);
    
    self.secrectTextField = [[XPNoMenuTextField alloc] init];
    self.userSecretView = [self configInputViewWithframe:codeRect frontImage:[UIImage imageNamed:@"secret_lock"] textField:self.secrectTextField placeHolder:@"请输入密码"];
    self.secrectTextField.keyboardType = UIKeyboardTypeURL;
    self.userSecretView.hidden = YES;
    [self.view addSubview:self.userSecretView];
    
    self.eyesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.eyesButton.frame = CGRectMake(self.userSecretView.frame.size.width - 13 - 24 , 13, 24, 24);
    [self.eyesButton setImage:[UIImage imageNamed:@"preview_close"] forState:UIControlStateNormal];
    [self.eyesButton setImage:[UIImage imageNamed:@"preview_open"] forState:UIControlStateSelected];
    [self.eyesButton addTarget:self action:@selector(eyesButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.userSecretView addSubview:self.eyesButton];
    
    self.secrectTextField.frame = CGRectMake(self.secrectTextField.frame.origin.x, self.secrectTextField.frame.origin.y, self.eyesButton.frame.origin.x - 50 -10, self.secrectTextField.frame.size.height);
    self.secrectTextField.secureTextEntry = YES;
    
    
    self.forgetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.forgetButton setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
    self.forgetButton.titleLabel.font = kFontRegular(14);
    [self.forgetButton addTarget:self action:@selector(forgetButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.forgetButton setTitle:@"忘记密码" forState:UIControlStateNormal];
    self.forgetButton.hidden = YES;
    self.forgetButton.frame = CGRectMake(MaxX(self.userSecretView) - 60 , MaxY(self.userSecretView) + 10, 56, 24);
//    self.forgetButton.backgroundColor = RandColor;
    [self.view addSubview:self.forgetButton];
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    loginButton.frame = CGRectMake(28,SCREEN_HEIGHT - (185 + self.safeBottom), SCREEN_WIDTH - 2 *28, 50);
    [loginButton setTitle:@"注册 / 登录" forState:UIControlStateNormal];
    loginButton.titleLabel.font = kFontMedium(16);
    [loginButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    loginButton.layer.cornerRadius = 6;
    loginButton.clipsToBounds = YES;
    [loginButton setBackgroundImage:[UIImage imageNamed:@"xp_login_btn"] forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(loginButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginButton];
    
    NSString *policy = @"我已阅读并同意服务协议和隐私政策";
    
    CGFloat policyW = [policy sizeWithAttributes:@{NSFontAttributeName:kFontRegular(14)}].width;
    
    self.dotButton = [[XPHitButton alloc] init];
    self.dotButton.hitRect = 40;
    [self.dotButton setImage:[UIImage imageNamed:@"xp_dot_unselected"] forState:UIControlStateNormal];
    [self.dotButton setImage:[UIImage imageNamed:@"xp_dot_selected"] forState:UIControlStateSelected];
    [self.dotButton addTarget:self action:@selector(dotButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.dotButton];
    
    self.policyView = [[UITextView alloc] init];
    CGFloat policyOriginX = (SCREEN_WIDTH - policyW - 14 - 5)/2.;
    self.policyView.frame = CGRectMake(policyOriginX + 14 + 5, SCREEN_HEIGHT - self.safeBottom - 40, policyW, 24);
    self.policyView.editable = NO;
    self.policyView.scrollEnabled = NO;
    self.policyView.textContainerInset = UIEdgeInsetsZero;
    self.policyView.textColor = UIColorFromRGB(0x8A8A8A);
    self.policyView.font = kFontRegular(14);
    self.policyView.textAlignment = NSTextAlignmentCenter;
    self.policyView.backgroundColor = UIColor.clearColor;
    [self.view addSubview:self.policyView];
    
    self.dotButton.frame = CGRectMake(policyOriginX + 5, self.policyView.frame.origin.y, 14, 14);
    
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:policy];
    [attributedString addAttribute:NSLinkAttributeName
                             value: [NSString stringWithFormat:@"DIY:%@",[NSString stringWithFormat:@"DIY:%@",[NSString stringWithFormat:@"%@%@",kNet_host,kUser_Agreement]]]
                             range:[[attributedString string] rangeOfString:@"服务协议"]];
    [attributedString addAttribute:NSLinkAttributeName
                             value:[NSString stringWithFormat:@"DIY:%@",[NSString stringWithFormat:@"%@%@",kNet_host,kUser_Policy]]
                             range:[[attributedString string] rangeOfString:@"隐私政策"]];

    
    [attributedString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x8A8A8A) range:NSMakeRange(0, policy.length)];
    
    self.policyView.attributedText = attributedString;
    self.policyView.linkTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x6E6BFF)};

    self.policyView.delegate = self;
    self.policyView.editable = NO;
}

- (UIView *)configInputViewWithframe:(CGRect)frame frontImage:(UIImage *)image textField:(UITextField *)textField placeHolder:(NSString *)placeholder {
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.layer.cornerRadius = 6.0;
    view.layer.borderColor = UIColorFromRGB(0xDEDEDE).CGColor;
    view.layer.borderWidth = 1.0;
    
    UIImageView *phoneImageView = [[UIImageView alloc] initWithFrame:CGRectMake(13, 13, 24, 24)];
    phoneImageView.image = image;
    [view addSubview:phoneImageView];
    
    textField.frame = CGRectMake(MaxX(phoneImageView) + 13 , 0, view.bounds.size.width - MaxX(phoneImageView) - 13 - 13, view.bounds.size.height);
    textField.placeholder = placeholder;
    [view addSubview:textField];
    textField.font = [UIFont systemFontOfSize:16];
    return view;
}

//用户密码显示与隐藏
- (void)eyesButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    sender.selected = !sender.selected;
    self.secrectTextField.secureTextEntry = !self.secrectTextField.secureTextEntry;
}

- (void)dotButtonClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
}

//免密登录
- (void)codeTitleButtonClicked:(UIButton *)sender {
    
    if (!self.currentSelectStatus) {
        return;
    }
    self.currentSelectStatus = !self.currentSelectStatus;
    self.codeTitleView.isChoosed = YES;
    self.secretTitleView.isChoosed = NO;
    
    self.codeView.hidden = NO;
    self.userSecretView.hidden = YES;
    self.phoneNumTextField.placeholder = @"请输入手机号";
    self.codeTextField.text = @"";
    self.phoneNumTextField.text = @"";
    self.forgetButton.hidden = YES;
}

//账号登录选择方式
- (void)secretTitleButtonClicked:(UIButton *)sender {
    
    if (self.currentSelectStatus) {
        return;
    }
    self.currentSelectStatus = !self.currentSelectStatus;
    
    self.codeTitleView.isChoosed = NO;
    self.secretTitleView.isChoosed = YES;
    
    self.codeView.hidden = YES;
    self.userSecretView.hidden = NO;
    self.forgetButton.hidden = NO;
    self.phoneNumTextField.placeholder = @"请输入账号";
    self.phoneNumTextField.text = @"";
    self.secrectTextField.text = @"";
    
//    self.phoneNumTextField.text = @"15530052093";
//    self.secrectTextField.text = @"P!ssw0rd!";
}

- (void)loginButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    
    if (self.currentSelectStatus == NO) {//手机号登录
        
        NSString *codeStr = [[XPVerifyTool shareInstance] verifyCode:self.codeTextField.text];
        if(self.phoneNumTextField.text.length == 0){
            [[XPShowTipsTool shareInstance] showMsg:@"请输入手机号！" ToView:self.view];
            return;
        }else if (![self validVerifyPredicate:phonePredicate TargetString:self.phoneNumTextField.text]) {
            [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的手机号！" ToView:self.view];
            return;
        }else if (codeStr){
            [[XPShowTipsTool shareInstance] showMsg:codeStr ToView:self.view];
            return;
        }
        else {
            
            if (!self.dotButton.selected) {
                [[XPShowTipsTool shareInstance] showMsg:@"请阅读并勾选服务协议!" ToView:self.view];
                return;
            }
            
           //手机号和验证码验证通过
            NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
            paraM[@"phone"] = self.phoneNumTextField.text;
            paraM[@"code"] = self.codeTextField.text;
            paraM[@"uuid"] = self.codeUUid;
            [self postToSeverWithDIc:paraM];
            
        }
    }else {//账号登录
        if(self.phoneNumTextField.text.length == 0){
            [[XPShowTipsTool shareInstance] showMsg:@"请输入手机号！" ToView:self.view];
            return;
        }else if(![self validVerifyPredicate:phonePredicate TargetString:self.phoneNumTextField.text]) {
            [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的手机号！" ToView:self.view];
            return;
        }else if (self.secrectTextField.text.length == 0){
            [[XPShowTipsTool shareInstance] showMsg:@"请输入密码！" ToView:self.view];
            return;
        }
        else if (self.secrectTextField.text.length < 8 || self.secrectTextField.text.length >20) {//![self validVerifyPredicate:@"/^(?=.[A-Za-z])(?=.\d)(?=.[_-!%])[A-Za-z\d_-!%]{8,20}$/" TargetString:self.secrectTextField.text]
            [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的密码格式！" ToView:self.view];
            return;
        }else if (![self validVerifyPredicate:@"^(?=.*[A-Za-z])(?=.*\\d)(?=.*[-!%])[A-Za-z\\d-!%]{8,20}$" TargetString:self.secrectTextField.text]){
            [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的密码格式！" ToView:self.view];
        }else {
            if (!self.dotButton.selected) {
                [[XPShowTipsTool shareInstance] showMsg:@"请阅读并勾选服务协议!" ToView:self.view];
                return;
            }
            //手机号和密码验证通过
            NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
            paraM[@"phone"] = self.phoneNumTextField.text;
            paraM[@"password"] = self.secrectTextField.text;
            [self postToSeverWithDIc:paraM];
        }
    }
}

- (void)postToSeverWithDIc:(NSMutableDictionary *)paraM{
    NSString *urlString;
    if (self.currentSelectStatus == 0) {
      urlString = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_LOGIN];
    }else {
        urlString = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Password_login];
    }
    
    [self startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:urlString Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self stopLoadingGif];
        NSLog(@"------- %@",data);
        if (data.isSucess) {
       //记录当前用户信息
            XPLocalStorage *storage = [XPLocalStorage new];
            [storage writeUserToken:data.userData[@"token"]];
            [storage writeUserCertified:data.userData[@"isCertified"]];
            [storage writeUserType:data.userData[@"userType"]];
            [storage writeUserName:data.userData[@"name"]];
            [storage writeUserIconUrl:data.userData[@"userIcon"]];
            [storage writeUserNickName:data.userData[@"nickname"]];
            [storage writeUserPhone:data.userData[@"phone"]];
            [storage writeUserHomeStatus:data.userData[@"homeStatus"]];
            [storage writeUserSubInventCode:data.userData[@"supInvCode"]];
            
            NSString *idNumber = data.userData[@"idCard"];
            [storage writeUserIdNumber:idNumber];
            if (self.modelType == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:Notification_user_login_success object:nil];
                
            }
            [self goBack];
        }else if(data.code.intValue == 1){
            XPWeakSelf
            self.xpAlertView.autoDissmiss = NO;
            [self.xpAlertView configWithTitle:@"填写邀请码" desCriptionStr:@"" placeHolderStr:@"请填写邀请码" leftAction:^(NSString * _Nullable extra) {
                self.xpAlertView.autoDissmiss = YES;
                
            } rightAction:^(NSString * _Nullable extra) {
                if (extra.length == 0) {
                    [[XPShowTipsTool shareInstance] showMsg:@"请输入邀请码" ToView:k_keyWindow];
                    
                }else if(extra.length != 6){
                    [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的邀请码" ToView:k_keyWindow];
                     
                }else {
                    
                    paraM[@"supInvCode"] = extra;
                    [weakSelf postToSeverWithDIc:paraM];
                    weakSelf.xpAlertView.autoDissmiss = YES;
                    [weakSelf.xpAlertView dismiss];
                }
                
            } alertType:XPAlertTypeInput];
            
        }else{
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}

//发送验证码
-(void)codeButtonClicked:(UIButton *)sender {
    [self.view endEditing:YES];

    if (![self validVerifyPredicate:phonePredicate TargetString:self.phoneNumTextField.text]) {
        [[XPShowTipsTool shareInstance] showMsg:@"请输入正确的手机号！" ToView:self.view];
        return;
    }else {
        NSString *codeUrlString = [NSString stringWithFormat:@"%@%@",kVERIFY_host,kVERIFY_Get_Code];
        NSDictionary *paraM = @{@"phoneNum":self.phoneNumTextField.text,@"type":@1};
        [self startLoadingGif];
        [[XPRequestManager shareInstance] commonPostWithURL:codeUrlString Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
            [self stopLoadingGif];
            if (data.isSucess) {
                self.codeUUid = data.userData;
                [self countDown];
                [self.codeTextField becomeFirstResponder];
                [[XPShowTipsTool shareInstance]showMsg:@"验证码发送成功!" ToView:self.view];
            }else {
            [[XPShowTipsTool shareInstance]showMsg:data.msg ToView:self.view];
            }
        }];
    }
    
}

//正则
-(BOOL)validVerifyPredicate:(NSString *) pString TargetString:(NSString *)targerString
{
   NSPredicate *num = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pString];
   return [num evaluateWithObject:targerString];
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
#pragma mark --- textView Delegate
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {

    if (([[URL scheme] isEqualToString:@"DIY"])) {
        NSString *pushUrl = [URL.absoluteString stringByReplacingOccurrencesOfString:@"DIY:" withString:@""];
        XPBaseWebViewViewController *vc = [[XPBaseWebViewViewController alloc] init];
        vc.urlString = pushUrl;
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    return YES;
}

#pragma mark --- textField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.codeTextField) {
        // 获取当前文本框中的文本，包括用户输入的新字符
        NSString *currentText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        // 如果当前文本长度超过6位，则不允许输入更多字符
        if (currentText.length > 6) {
            return NO;
        }
    }else if (textField == self.phoneNumTextField){
        NSString *currentText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        // 如果当前文本长度超过6位，则不允许输入更多字符
        if (currentText.length > 11) {
            return NO;
        }
    }
    // 允许输入
    return YES;
}

- (void)goBack {;
    [self dismissModalViewControllerAnimated:YES];
}


- (void)forgetButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    XPUserPaySecrectController *vc = [XPUserPaySecrectController new];
    vc.enterType = 1;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark -- 倒计时
-(void)countDown{
    
    XPWeakSelf
    __block int i = 60;
        //一分钟倒计时
    self.oneMinTimer = [NSTimer jk_timerWithTimeInterval:1 block:^{
        if (i >= 0) {
            [weakSelf.codeButton setTitle:[NSString stringWithFormat:@" %d秒 ",i] forState:0];
            i--;
            weakSelf.codeButton.enabled = NO;
            weakSelf.codeButton.alpha = 0.5;
        } else{
            [weakSelf.oneMinTimer invalidate];
            weakSelf.oneMinTimer = nil;
            [ weakSelf.codeButton setTitle:@"获取验证码" forState:0];
            weakSelf.codeButton.enabled = YES;
            weakSelf.codeButton.alpha = 1.0;
        }
    } repeats:YES];
    [[NSRunLoop mainRunLoop]addTimer:self.oneMinTimer forMode:NSRunLoopCommonModes];
    [self.oneMinTimer fire];
}

@end
