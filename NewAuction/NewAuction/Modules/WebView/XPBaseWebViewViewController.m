//
//  XPBaseWebViewViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import "XPBaseWebViewViewController.h"
#import <WebKit/WebKit.h>

@interface XPBaseWebViewViewController ()

@property (nonatomic, strong) WKWebView *webView;

@end

@implementation XPBaseWebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupViews];
    // 添加进度监听
    [self.webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
    
}
- (void)setupNavBarView {
    self.navBarView = [[XPNavTopView alloc] init];
    [self.navBarView.leftButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.navBarView];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadUrl:_urlString];
}

- (void)setupViews {
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, kTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT - kTopHeight)];
    _webView.backgroundColor = UIColor.lightGrayColor;
    
    [self.view addSubview:self.webView];
}

- (void)loadUrl:(NSString *)urlString {
    NSLog(@"urlString -------- >%@",urlString);
    
    if (self.webViewType == 0) {
        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    }else if (self.webViewType == 1){
        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:urlString]]];
    }
}


#pragma mark KVO 实现方法
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"title"]) {
        
        if (object == self.webView) {
            
            self.navBarView.titleLabel.text = self.webView.title;

        } else {
            
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    }
}


- (void)backButtonClicked {
    
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
