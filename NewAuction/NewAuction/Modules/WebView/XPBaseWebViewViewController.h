//
//  XPBaseWebViewViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPBaseWebViewViewController : UIViewController
//0 默认 网页  1 本地html
@property (nonatomic, assign) NSInteger webViewType;

@property (nonatomic, strong) NSString *urlString;

@end

NS_ASSUME_NONNULL_END
