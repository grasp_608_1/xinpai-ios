//
//  XPNavigationController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "XPNavigationController.h"

@interface XPNavigationController ()

@end

@implementation XPNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.hidden = YES;
}

@end
