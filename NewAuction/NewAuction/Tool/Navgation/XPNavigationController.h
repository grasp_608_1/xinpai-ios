//
//  XPNavigationController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPNavigationController : UINavigationController

@end

NS_ASSUME_NONNULL_END
