//
//  XPSpeechRecognitionManager.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/25.
//

#import "XPSpeechRecognitionManager.h"
#import "LVRecordTool.h"

@interface XPSpeechRecognitionManager () <SFSpeechRecognizerDelegate, AVAudioRecorderDelegate>

@property (nonatomic, strong) SFSpeechRecognizer *speechRecognizer;
@property (nonatomic, strong) SFSpeechAudioBufferRecognitionRequest *recognitionRequest;
@property (nonatomic, strong) SFSpeechRecognitionTask *recognitionTask;
@property (nonatomic, strong) AVAudioEngine *audioEngine;

@end

@implementation XPSpeechRecognitionManager


-(void)startRecognizeLocalAudioCompletion:(void (^)(NSString * _Nullable resultStr,NSInteger code))completion{
    [self startREcognizeWithAudioPath:[[LVRecordTool sharedRecordTool] getAudioPath] Completion:completion];
}

- (void)startREcognizeWithAudioPath:(NSString *)filePath Completion:(void (^)(NSString * _Nullable resultStr,NSInteger code))completion{
    
    NSLocale *local =[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    SFSpeechRecognizer *localRecognizer =[[SFSpeechRecognizer alloc] initWithLocale:local];
    NSURL *url = [NSURL fileURLWithPath:filePath];
    if (!url) return;
    SFSpeechURLRecognitionRequest *res =[[SFSpeechURLRecognitionRequest alloc] initWithURL:url];
    [localRecognizer recognitionTaskWithRequest:res resultHandler:^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error) {

        if (result.isFinal) {
            
            if (!error) {
                NSLog(@"success---- %@",result.bestTranscription.formattedString);
                if (completion) {
                    completion(result.bestTranscription.formattedString,0);
                }
                
            }else{
                
                if (completion) {
                    completion(@"识别失败",1);
                }
            }
        }else if (result == nil){
            //检测不到语音
            if (completion) {
                completion(@"您好像没有说话",1);
            }
        }
    }];
}

@end
