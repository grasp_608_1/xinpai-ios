//
//  XPSpeechRecognitionManager.h
//  NewAuction
//
//  Created by Grasp_L on 2024/12/25.
//

#import <Foundation/Foundation.h>
#import <Speech/Speech.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface XPSpeechRecognitionManager : NSObject
//开始识别本地固定地址的语音
-(void)startRecognizeLocalAudioCompletion:(void (^)(NSString * _Nullable resultStr,NSInteger code))completion;
//开始识别指定的沙盒地址的语音
- (void)startREcognizeWithAudioPath:(NSString *)filePath Completion:(void (^)(NSString * _Nullable resultStr,NSInteger code))completion;

@end

NS_ASSUME_NONNULL_END
