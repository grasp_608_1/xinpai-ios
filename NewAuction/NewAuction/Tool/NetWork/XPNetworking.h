//
//  XPNetworking.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class XPNetworking;
typedef  void (^SuccessBlock)(id responseObject);
typedef  void (^FailureBlock)(NSString * error, NSInteger code);

@interface XPNetworking : NSObject
/**
 *  获取单例对象
 */
+ (instancetype)shareInstance;
//post 请求接口
+ (void)xpPostWithURL:(NSString *)url Params:(id)params success:(SuccessBlock)success failure:(FailureBlock)failure;
//get 请求接口
+ (void)xpGetWithURL:(NSString *)url Params:(id)params success:(SuccessBlock)success failure:(FailureBlock)failure;
// 取消当前
- (void)cancelNetworking;

@end

NS_ASSUME_NONNULL_END
