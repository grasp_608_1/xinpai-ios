//
//  XPNetworkConfiguration.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#ifndef XPNetworkConfiguration_h
#define XPNetworkConfiguration_h

//服务器地址
#if 1
#define kNet_host              @"https://www.new-auction.com"
#else
//#define kNet_host            @"http://192.168.31.105:8082"
//#define kNet_host            @"https://2d76dc9b.r29.cpolar.top"
//#define kNet_host            @"http://192.168.1.50:8082"
#define kNet_host              @"http://120.78.139.171"
#endif
#define kTDNet_host            @"https://tdprinter.iotbroad.com"
#define kVERIFY_host         [NSString stringWithFormat:@"%@%@",kNet_host,@"/api-app"]

//更新接口
#define k_app_update @"/systemctl/getForceStatus"
//登陆(手机号,验证码)
#define kVERIFY_LOGIN @"/xp-user/autoLoginByPhone"
//账户密码登录
#define kVERIFY_Password_login @"/xp-user/login"
//注销用户
#define kVERIFY_user_destory @"/xp-user/cancel"
//获取短信验证码
#define kVERIFY_Get_Code @"/xp-user/getSmsCode"
//添加用户邀请码
#define kVERIFY_user_UpdateInventCode @"/xp-user/updateSupInvCode"
//分类 列表
#define kVERIFY_Get_sort_list @"/category/list"
//商城一级分类
#define kVERIFY_shop_sortOne_list @"/category/mall/getCategoryOne"
//商城分类树(根据id返回相应)
#define kShop_sort_one @"/category/mall/getCategoryOneTwoThree"
// 分类下类目
#define kVERIFY_Get_sort_product @"/category/auction/byCategoryId"
//添加身份信息表
#define kVERIFY_User_id_verify @"/xp-user/add"

#define kVERIFY_User_get_info @"/xp-user-info/get"
//修改用户头像
#define kVERIFY_User_update_icon @"/xp-user-info/updateHead"

#define kVERIFY_User_login_password  @"/xp-user/initUserPassward"
//设置支付密码
#define kVERIFY_User_pay_password @"/xp-user-info/updatePayPassward"
//更新头像
#define kVERIFY_User_update_nickname @"/xp-user-info/updateNickName"
#define kVERIFY_User_update_userinfo @"/xp-user-info/updateUserBasicInfo"
//我的站内信
#define kVERIFY_User_notice @"/xp-user/getNoticeList"
//我的收藏
#define kVERIFY_Get_fav_list @"/favorite/data"
//取消收藏 直接拼id
#define kVERIFY_Get_fav_delete @"/favorite/cancel/favorite/"
#define kVERIFY_Get_shopFav_delete @"/favorite/cancel/favoriteSku/"
//用户反馈列表
#define kVERIFY_Get_feedback_list @"/mall-user-feedback/pageList"
#define kVERIFY_submit_feedback @"/mall-user-feedback/insert"
//我的粉丝
#define kVERIFY_Get_my_fans @"/xp-user-info/list/fans"
//造物作者粉丝列表
#define kVERIFY_Creation_fans_list @"/product/model/toFollowUserList"
//造物作者的关注
#define kVERIFY_Creation_Attention_list @"/product/model/fromFollowUserList"
//造物打印记录
#define kVERIFY_Creation_Print_Recoard  @"/user-device/getPrintRecordByUser"
//获取打印机列表
#define kVERIFY_Creation_Printer_list @"/user-device/getListByUserId"
//添加打印机
#define kVERIFY_Creation_Printer_Add @"/user-device/addUserDevice"

//添加收藏(拍品)
#define kVERIFY_Get_fav_add @"/favorite/save/"
//添加收藏(商品)
#define kVERIFY_Get_shopFav_add @"/favorite/saveSku/"
//商品作品点赞
#define kVERIFY_Get_shopLike_add @"/product/worksLikeCreate/"
//取消委拍
#define kVERIFY_delegate_delete_aution @"/entrust/cancel/entrust"
//取消can拍
#define kVERIFY_join_delete_aution @"/xp-part-auction/cancelPartAuction"
//获取可参拍数量
#define kVERIFY_Get_joinNum  @"/xp-part-auction/countBy"
//获取可委拍数量
#define kVERIFY_Get_DelegateNum  @"/entrust/countBy/"
//参拍
#define kVERIFY_join_product @"/xp-part-auction/partAuction"
//参拍申请补差价结果查询
#define kVERIFY_join_extraPay_query @"/xp-part-auction/partResultQuery"
//参拍补差价确认结果
#define kVERIFY_join_extraPay_comfirm @"/xp-part-auction/partResultConfirm"
//参拍补差价确认结果查询
#define kVERIFY_join_extraPay_comfirmQuery @"/xp-part-auction/partResultConfirmQuery"
//利润结算
#define kVERIFY_money_count @"/xp-settle/profitSettlement"
//委拍
#define kVERIFY_delegate_product @"/entrust/auction"
//商品分享
#define kVERIFY_product_share @"/share/address"

#if APPTYPE == 0
//用户服务协议
#define kUser_Agreement @"/h5/prompt/userAuthorizationNotice.html"
//用户隐私
#define kUser_Policy    @"/h5/prompt/userPrivacy.html"
//退换货政策
#define k_goodsExchange_policy @"/h5/prompt/returnPolicy.html"
#elif APPTYPE == 1
//用户服务协议
#define kUser_Agreement @"/h5/prompt/userAgreement.html"
//用户隐私
#define kUser_Policy    @"/h5/prompt/privacy.html"
//退换货政策
#define k_goodsExchange_policy @"/h5/prompt/refundPolicy.html"
#else

#endif

//拍卖提货
#define kUser_Aution_goods @"/h5/prompt/auctionRules.html"
//限额提示
#define kBank_description @"/h5/quickPayLimitTable.html"
//营业资质
#define kCompany_authen @"/h5/credentials.html"
//动态页
#define k_update_page @"/h5/quillRichText.html?id="
//客服中心
#define k_user_help @"/h5/prompt/customer.html"
//造物,造字等详情页
#define k_Creation_model_detail @"/webzw/#/pages/models/modelDetail"

//首页详情
#define kHome_data @"/home/data"
//首页2.0
#define kHome_main_data @"/home/mall/homeAuction"

//商城1.0首页
#define kShop_data @"/xp-mall/list"
//2.0新拍商城首页
#define kShop_main_data @"/home/mall/homeMall"
//2.0造物商城首页
#define kCreation_main_data @"/home/mall/homeMallCreation"
//推荐页
#define kShop_data_list @"/product/mall/productSkuList"
//商品详情页
#define kShop_product_detail @"/product/mall/productSkuInfo"
//商品评论列表
#define kShop_product_Comment @"/mall-product-comment/productCommentList"

#define kShop_mall_orderConfirmPresaleBuy @"/order/mall/orderConfirmPresaleBuy"//定金订单确认
#define kShop_mall_orderConfirmGroupBuy @"/order/mall/orderConfirmGroupBuy"//拼团订单确认
#define kShop_mall_orderConfirmBuy @"/order/mall/orderConfirmBuy"//订单确认(单商品)
#define kShop_mall_carOrderConfirmBuy @"/order/mall/orderConfirmCart"//订单确认(购物车,多商品)
#define kShop_mall_carDelteItem @"/mall-cart/deleteById"//购物车删除某个商品
#define kShop_mall_carChangeItem @"/mall-cart/updateQuantity"//改变购物车某个商品的数量
#define kShop_mall_selectAllSku @"/product/getProductSkuListByProductIdInfo"//根据skuid查询商品下所有sku

#define kShop_mall_orderSubmitPresaleBuy @"/order/mall/orderSubmitPresaleBuy" //订单购买预售定金
#define kShop_mall_orderSubmitGroupBuy @"/order/mall/orderSubmitGroupBuy"//订单购买拼团
#define kShop_mall_orderSubmitBuy @"/order/mall/orderSubmitBuy"//订单购买(单商品)
#define kShop_mall_carOrderSubmitBuy @"/order/mall/orderSubmitCart"//订单购买(购物车,多商品)
#define kShop_mall_carAdd @"/mall-cart/insert"//加入购物车(但商品)
#define kShop_mall_carListAdd @"/mall-cart/insertList"//加入购物车(多商品)
#define kShop_mall_carList @"/mall-cart/pageList"//购物车商品列表
#define kShop_mall_carActivity @"/mall-cart/activityList"//购物车活动列表
#define kShop_mall_orderDetail @"/order/detail" //订单详情
#define kShop_mall_orderDelte @"/order/deleteOrder"//删除订单
#define kShop_mall_orderCancel @"/order/offOrder" //取消订单
#define kShop_mall_orderconfirm @"/order/confirmOrder"//确认收货
//#define kShop_mall_orderAfterSale @"/mall-order-after/insert"//提交售后(2.0 已废弃)
#define kShop_mall_orderAfterSale @"/mall-order-after/batchInsert"//多商品提交售后
#define kShop_mall_orderAfterSale_single @"/mall-order-after/insertTwo"//单商品提交售后
#define kShop_mall_creation_author @"/product/model/getMakerUserVO"//造物-作者个人中心
#define kShop_mall_creation_attention_someOne @"/product/model/saveFollow"//关注某人
#define kShop_mall_creation_disAttention_someOne @"/product/model/cancelFollow"//取关某人
//#define kShop_mall_orderComment @"/order/mall/orderConfirmCart"//提交评论
#define kShop_mall_orderComment @"/mall-product-comment/insertProductCommentList"//提交多条评论

//售后列表
#define kShop_order_afterSaleRecord @"/mall-order-after/pageList"
//删除某条售后记录
#define kShop_order_afterSaleRecord_delete @"/mall-order-after/delete"
#define kShop_order_afterSaleRecord_retry @"/mall-order-after/resubmitOrderAfter"
//单订单商品
#define kShop_order_afterSaleSingle @"/mall-order-after/selectById"
//售后商品寄回-商品归仓
#define kShop_order_afterSaleExpress @"/mall-order-after/productToWareHouse"
#define kShop_product_images @"/xp-mall/product/image"
//优惠券领取
#define kShop_coupon_receive @"/mall-coupon/couponReceive"
//优惠券待使用
#define kShop_coupon_unused   @"/xp-user-info/couponsPendingUse"
//优惠券已使用
#define kShop_coupon_used     @"/xp-user-info/couponsUse"
//优惠券已过期
#define kShop_coupon_expired  @"/xp-user-info/couponsExpired"
//商城订单列表
#define kShop_order_list      @"/order/pageList"
//商品排行榜
#define kShop_Chart_list @"/product/mall/getBillboardProductSkuList/"
//排行榜分类
#define kShop_Chart_category @"/product/mall/getBillboardList/"


//3d 造物首页
#define kCreate_main_data @"/home/mall/homePrinter"
#define kCreate_command_product @"/product/mall/productSkuNote"

//圈子列表
#define kCircle_Page_list @"/circle/circlePageList"
#define kCircle_top @"/circle/circleLabelInfo/"
//圈子文章详情
#define kCircle_Page_detail @"/circle/circleInfo/"
//发布圈子
#define kCircle_publish @"/circle/circleRelease"
//圈子标签列表
#define kCircle_sign_list @"/circle/circleLabelList"
//我的圈子标签设置
#define kCircle_sign_Set @"/circle/circleLabelSet"
//圈子评论
#define kCircle_comment_submit @"/circle/circleCommentCreate"
//删除圈子某条评论
#define kCircle_comment_delete @"/circle/circleCommentDel/"
//圈子点赞列表
#define kCircle_like_list @"/circle/circleLikeList"
//点赞某条圈子
#define kCircle_like @"/circle/circleLikeCreate/"
//圈子点赞列表
#define kCircle_comment_like @"/circle/circleLikeList"
//圈子点赞评论列表
#define kCircle_comment_list @"/circle/circleCommentList"
//圈子点赞评论列表
#define kCircle_comment_like_list @"/circle/circleCommentLikeList"
//转发圈子
#define kCircle_share @"/circle/circleShare/"

//创客首页
#define kMaker_home_data @"/home/homeMaker"
//加入创客
#define kMaker_apply @"/maker/addMaker"
//创客作品列表
#define kMaker_list @"/product/model/getWorksSkuVOList"
//我的创客列表
#define kMaker_list_my @"/product/model/getMakerUserVOList"
//我的徽章
#define kMaker_badge_my @"/maker/selectBadgeByUser"
//我的作品申请售卖
#define kMaker_work_apply @"/tdWorks/applySell"
//创客佣金金额信息
#define kMaker_money_info @"/xp-settle/commissionExchangeInfo"
//创客佣金转现金
#define kMaker_money_exchange @"/payment/withdrawal/cash/commission"
//创客佣金提现明细
#define kMaker_money_exchange_detail @"/payment/pageListWithdrawalCommissionCash"

//商品详情页
#define kProduct_detail @"/auctionItem/detail"
//仓库委拍
#define kProduct_store_delegate @"/entrust/warehouse/entrust"
#define kProduct_store_list @"/entrust/page"//委拍列表查询
#define kProduct_store_wait_send @"/wait/pickgoods"
#define kProduct_store_pick_detail @"/xp-mall/productPickOrderDetail"
//当前委托-参拍 已废弃
#define kProduct_my_delegte_list @"xp-part-auction/partAuctionList"
//当前委托new
#define kProduct_my_delegte_list_new @"/xp-part-auction/partAuctionList"
//我的仓库参拍列表 (已废弃)
#define kProduct_store_auction_list  @"/xp-part-auction/listWarehouseByPage"
//我的仓库参拍列表 new
#define kProduct_store_auction_list_new  @"/xp-part-auction/partAuctionList"
#define k_xp_news @"/news/pageList"//动态

//银行卡OCR
#define kUpload_card_url @"/xp-user/ocrBankCard"
#define kBank_Card_query @"/xp-user/ocrBankCard/asyncResult"
//支付卡列表
#define kBank_card_list @"/payment/pageList/payment/card"
//添加支付卡
#define kBank_card_add  @"/payment/add/payment/card"
//添加结算卡
#define kBank_profit_card_add  @"/payment/add/settlement/card"
//删除支付卡(拼ID)
#define kBank_card_delete @"/payment/remove/payment/card"

//OCr 身份证
#define kUser_id_front_ocr @"/xp-user/ocrIdCardFace"
//ocr 身份证正面结果轮询
#define kUser_id_front_ocrQuery @"/xp-user/ocrIdCardFace/asyncResult"
//ocr 身份证背面
#define kUser_id_back_ocr @"/xp-user/ocrIdCardBack"
//ocr 身份证国徽面结果轮询
#define kUser_id_back_ocrQuery @"/xp-user/ocrIdCardBack/asyncResult"

//人脸OCR
#define kUser_id_face_ocr @"/xp-user/faceIdentify"
#define kUser_id_face_ocr_query @"/xp-user/faceIdentify"

//结算卡列表
#define kBank_result_list @"/payment/pageList/settlement/card"
//添加支付卡
#define kBank_result_add  @"/payment/add/settlement/card"
#define kBank_result_Confirm @"/payment/add/payment/card/confirm"
#define kBank_result_Confirm_query @"/payment/add/payment/card/confirm/query"

//货款支付结果
#define kBank_recharge_Confirm @"/payment/recharge/confirm"
//货款支付结果查询
#define kBank_recharge_Confirm_query @"/payment/recharge/confirm/query"

//补差价
#define kPayExtra_recharge_Confirm @"/xp-mall/pickResultConfirm"

#define kPayExtra_recharge_Query @"/xp-mall/pickResultConfirmQuery"

//删除支付卡(拼ID)
#define kBank_result_delete @"/payment/remove/settlement/card"

//盈利转余额明细
#define kDetail_profit_exchange @"/xp-settle/profitBalanceExchangeDetail"
//收益转余额明细
#define kDetail_income_exchange @"/xp-settle/incomeBalanceExchangeDetail"
//盈利明细
#define kDetail_profit @"/xp-settle/profitDetails"
//收益明细
#define kDetail_income @"/xp-settle/incomeDetails"
//委拍明细
#define kDetail_aution_delegate @"/entrust/pageListDetail"
//全部账单
#define kDetail_all @"/xp-settle/billList"
//充值明细
#define kDetail_recharge_detail @"/payment/pageListRechargeNew"
//提现明细
#define kDetail_cash_detail @"/payment/pageListWithdrawalCashNew"


#define kDetail_pay_in @"/payment/pageListRecharge"
//我的积分
#define kDetail_user_score @"/xp-user/getPointsInfo"
#define kUser_score_submit @"/xp-user-info/submitUserPointSport"

//我的收货地址列表
#define kUser_adress_list @"/xp-user-address/get"
//新增
#define kUser_adress_add @"/xp-user-address/save"
//编辑
#define kUser_adress_edit @"/xp-user-address/update"
//删除
#define kUser_adress_delete @"/xp-user-address/delete"

//app分享页保存
#define kUser_share_url @"/xp-user-info/appShare"

//盈利兑换
#define k_monney_income_info @"/xp-settle/profitBalanceExchangeInfo"
//盈利兑换 收益转余额
#define k_monney_income_exchange @"/xp-settle/incomeBalanceExchange"
//余额兑换-盈利转余额
#define k_monney_profit_exchange @"/xp-settle/profitBalanceExchange"
//余额兑换 收益兑换
#define k_monney_profit_info @"/xp-settle/incomeBalanceExchangeInfo"

//提货详情
#define k_pick_good_detail @"/xp-mall/productPickDetail"
//提货
#define k_pick_goods @"/xp-mall/productPick"
//提货+ 支付结果轮训
#define k_pick_goods_query @"/xp-mall/pickResultQuery"
//我的仓库,已提货列表
#define k_pick_goods_list @"/xp-mall/pickCompleted"
//货款管理资金信息
#define k_money_Info @"/xp-user-info/highestPoint"
//个人充值信息 get
#define k_reCharge_Info @"/xp-user/getXpUserAmountWeights"
//货款转出
#define k_money_to_cash @"/payment/withdrawal/cash"
//体现明细
#define k_money_to_cash_detail @"/payment/pageListWithdrawalCash"

//货款支付订单确认
#define k_money_in_account @"/payment/recharge"
#define k_sandPay_recharge @"/sandPay/recharge"
//货款支付订单结果轮询
#define k_money_in_account_querey @"/payment/recharge/query"
//银行卡汇付解绑轮训
#define k_bank_delete_query @"/payment/remove/payment/card/query"

#define k_sandPay_shop_order @"/order/mall/orderPayment"//商城(三方)支付
#define k_AccountPay_shop_order @"/order/mall/orderPayUseFunds"//商城(余额)支付
#define k_sandPay_recharge_query     @"/sandPay/recharge/query"//充值轮训
#define k_sandPay_updateOrderStatus     @"/order/mall/updateOrderPayStatus"//更新商城订单状态
#define k_sandPay_rechargePart_query @"/sandPay/recharge/part/query"//参拍补差价轮训
#define k_sandPay_rechargePick_query @"/sandPay/recharge/pick/query"//提货补差价轮训
#define k_sandPay_shopOrder_query @"/sandPay/recharge/mallOrder/query"// 商城支付补差价轮训

#define k_bank_company_rechargeInfo @"/systemctl/getCorporateAccountConfig"//获取对公打款接口
#define k_bank_company_rechargeSubmit @"/sandPay/recharge-corporate"//提交对公打款凭证
//
#define k_upload_file @"/xp-user-info/upload"//文件上传到服务器
#define k_product_express @"/dim/express100/findOrder"//快递100 查询
#define k_product_express_list @"/dim/express100" //支持的物流公司
#define k_product_package_detail @"/order/packageDetail"//包裹详情
#define k_user_preferenceHome @"/xp-user/setHomeStatus"//偏好设置

#endif /* XPNetworkConfiguration_h */
