//
//  XPRequestManager.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import "XPRequestManager.h"
#import "XPNetworking.h"
#import "XPNetworkStatusClass.h"
#import "XPRequestResultDescribe.h"

@implementation XPRequestManager

static XPRequestManager * shareInstance;
+ (instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstance = [[self alloc] init];
    });
    return shareInstance;
}

#pragma mark -- 修改之前直接使用STNetworking的放法修改为现在的方法
- (void)commonPostWithURL:(NSString *)url Params:(id)params requestResultBlock:(RequestResultBlock)block
{
//    //判断当前是否有网络
//    if(![XPNetworkStatusClass isHaveNetwork]){
//        XPRequestResult *tempRes = [[XPRequestResult alloc]init];
//        tempRes.isSucess = NO;
//        tempRes.msg = httpMsg_noNetWork;
//        tempRes.code = @999;
//        block(tempRes);
//        return;
//    }
    XPRequestResult *result = [[XPRequestResult alloc]init];
    [XPNetworking xpPostWithURL:url  Params:params success:^(id responseObject) {
        [self resolveSuccessResultResponseObject:responseObject requestResultBlock:block result:result];
    } failure:^(NSString *error, NSInteger code) {
        [self resolveFailureResultWithErrorMsg:error Code:code requestResultBlock:block result:result];
    }];
}

- (void)commonGetWithURL:(NSString *)url Params:(id)params requestResultBlock:(RequestResultBlock)block
{
    //判断当前是否有网络
    if(![XPNetworkStatusClass isHaveNetwork]){
        XPRequestResult *tempRes = [[XPRequestResult alloc]init];
        tempRes.isSucess = NO;
        tempRes.msg = httpMsg_noNetWork;
        tempRes.code = @999;
        block(tempRes);
        return;
    }
    XPRequestResult *result = [[XPRequestResult alloc]init];
    [XPNetworking xpGetWithURL:url  Params:params success:^(id responseObject) {
        [self resolveSuccessResultResponseObject:responseObject requestResultBlock:block result:result];
    } failure:^(NSString *error, NSInteger code) {
        [self resolveFailureResultWithErrorMsg:error Code:code requestResultBlock:block result:result];
    }];
}


//服务器成功后返回数据处理
- (void)resolveSuccessResultResponseObject:(id)responseObject requestResultBlock:(RequestResultBlock)block result:(XPRequestResult *)result{
    result.isShow = @(0);
    NSDictionary *dicTemp = responseObject;
    if (dicTemp) {
        
        if ([dicTemp.allKeys containsObject:@"status"]&&[[dicTemp objectForKey:@"status"] isEqualToString:@"succeeded"] ) {
            if([dicTemp.allKeys containsObject:@"code"]&&[[dicTemp objectForKey:@"code"] intValue]==0){
                int  code = [[dicTemp objectForKey:@"code"] intValue];
                NSNumber *codeNum = [NSNumber numberWithInt:code];
                result.code = codeNum;
                result.isSucess = YES;
                result.isShow = dicTemp[@"isShow"];
                result.userData = [dicTemp objectForKey:@"data"];
                NSString *msg = [responseObject objectForKey:@"msg"];
                result.msg = msg;
                block(result);
            }
            
        }else if([dicTemp.allKeys containsObject:@"status"]&&([[dicTemp objectForKey:@"status"] isEqualToString:@"pending"] || [[dicTemp objectForKey:@"status"] isEqualToString:@"failed"])){
            
            //先处理登录token 失效
            if([dicTemp.allKeys containsObject:@"code"]&&[[dicTemp objectForKey:@"code"] intValue] == httpRequestResult_Login_timeOut){
                
                XPBaseViewController *vc = (XPBaseViewController *)[XPCommonTool getCurrentViewController];
                [vc.xpAlertView showAlertWithTitle:dicTemp[@"msg"] actionBlock:^(NSString * _Nullable extra) {
                    [[XPVerifyTool shareInstance] userShouldLoginFirst];
                }];
                
            }else if([dicTemp.allKeys containsObject:@"code"]&&[[dicTemp objectForKey:@"code"] intValue] == 10000){//pengding状态也需要用到 userdata
                int  code = [[dicTemp objectForKey:@"code"] intValue];
                NSNumber *codeNum = [NSNumber numberWithInt:code];
                result.code = codeNum;
                result.isSucess = NO;
                result.userData = [dicTemp objectForKey:@"data"];
                NSString *msg = [responseObject objectForKey:@"msg"];
                result.msg = msg;
            }
            else if([dicTemp.allKeys containsObject:@"code"]&&[[dicTemp objectForKey:@"code"] intValue]!=0){
                int  code = [[dicTemp objectForKey:@"code"] intValue];
                NSNumber *codeNum = [NSNumber numberWithInt:code];
                result.code = codeNum;
                result.isSucess = NO;
                NSString *msg = [responseObject objectForKey:@"msg"];
                result.msg = msg;
            }else{
                //不包含code字端
                result.code = @998;
                result.isSucess = NO;
                NSString *msg = [responseObject objectForKey:@"message"];
                result.msg = msg;
            }
            block(result);
        }else{
            NSString *msgString = httpMsg_noNetWork;
            if([dicTemp.allKeys containsObject:@"msg"]){
                msgString = [responseObject objectForKey:@"msg"];
            }else if([dicTemp.allKeys containsObject:@"message"]){
                msgString = [responseObject objectForKey:@"message"];
            }else{
                //nothing
            }
            result.isSucess = NO;
            result.msg = msgString;
            block(result);
        }
            
            
        }
       
}

- (void)resolveFailureResultWithErrorMsg:(NSString *)error Code:(NSInteger)code requestResultBlock:(RequestResultBlock)block result:(XPRequestResult *)result{
    result.isSucess = NO;
    result.msg = error;
    result.code = @(code);
    block(result);
}

@end
