//
//  XPOSSUploadTool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, XPMediaType) {
    XPMediaTypeImage,//圈子图片
    XPMediaTypeVideo,//圈子视频
    XPMediaTypeFeedback,//用户反馈
    XPMediaTypeUserHead,//用户头像
    XPMediaTypeShopAfterSale,//商品售后图片
    XPMediaTypeShopComment,//商品评价
    XPMediaTypeMoneyRecharge,//商品评价
    XPMediaTypeMakerFaceHand,//创客手持照片人脸照片
    XPMediaTypeScoreImage,//积分兑换图片上传
};

@interface XPOSSUploadTool : NSObject

+ (instancetype)shareInstance;

///上传本地文件
- (void)uploadFileWithMediaType:(XPMediaType)type filePath:(NSString *)filePath progress:(void(^)(CGFloat progress))progressBlock  completeBlock:(void(^)(BOOL success,NSString *remoteUrlStr))block;

//固定沙盒目录,使用前会清空文件夹下文件
- (NSString *)saveImageToSandbox:(UIImage *)image;

@end

NS_ASSUME_NONNULL_END
