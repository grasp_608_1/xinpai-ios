//
//  XPRequestResult.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPRequestResult : NSObject
@property (nonatomic, assign) BOOL isSucess; //获取数据是否成功
@property (nonatomic, strong) NSString *msg; //服务端返回的消息内容
@property (nonatomic, strong) NSNumber *code; //json返回的status的值
@property (nonatomic, strong) NSNumber *isShow; //控制造中新拍页面是否显示 0 不显示 1 显示
@property (nonatomic, strong) id userData; //数据模型

@end

NS_ASSUME_NONNULL_END
