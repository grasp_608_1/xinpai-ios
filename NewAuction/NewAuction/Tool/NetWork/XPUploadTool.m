//
//  XPUploadTool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/19.
//

#import "XPUploadTool.h"
#import "AFNetworking.h"

@implementation XPUploadTool

+ (void)uploadWithUrl:(NSString *)urlString fromData:(NSData *)bodyData
             progress:(void (^)(NSProgress *uploadProgress)) uploadProgressBlock
    completionHandler:(void (^)(NSURLResponse *response, id responseObject, NSError *error))completionHandler  {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager.requestSerializer setValue:[XPLocalStorage readUserToken] forHTTPHeaderField:@"token"];    NSMutableURLRequest *request = [manager.requestSerializer multipartFormRequestWithMethod:@"POST" URLString:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } error:nil];
    
    [manager uploadTaskWithRequest:request fromData:bodyData progress:uploadProgressBlock completionHandler:completionHandler];
    
}


@end
