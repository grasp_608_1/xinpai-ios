//
//  XPUploadTool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPUploadTool : NSObject
+ (void)uploadWithUrl:(NSString *)urlString fromData:(NSData *)bodyData
             progress:(void (^)(NSProgress *uploadProgress)) uploadProgressBlock
    completionHandler:(void (^)(NSURLResponse *response, id responseObject, NSError *error))completionHandler;
@end

NS_ASSUME_NONNULL_END
