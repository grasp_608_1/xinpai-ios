//
//  XPRequestManager.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import <Foundation/Foundation.h>
#import "XPRequestResult.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^RequestResultBlock)(XPRequestResult *data);
@interface XPRequestManager : NSObject

+ (instancetype)shareInstance;

#pragma mark -- 标准方法用这个
- (void)commonPostWithURL:(NSString *)url Params:(id)params requestResultBlock:(RequestResultBlock)block;
- (void)commonGetWithURL:(NSString *)url Params:(id)params requestResultBlock:(RequestResultBlock)block;

@end

NS_ASSUME_NONNULL_END
