//
//  XPNetworkStatusClass.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPNetworkStatusClass : NSObject
/*
 当前是否有网络
 */
+ (BOOL)isHaveNetwork;

@end

NS_ASSUME_NONNULL_END
