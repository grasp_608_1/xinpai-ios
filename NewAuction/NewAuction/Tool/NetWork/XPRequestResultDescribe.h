//
//  XPRequestResultDescribe.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#ifndef XPRequestResultDescribe_h
#define XPRequestResultDescribe_h

/*
 成功
 */
#define httpRequestResult_OK 0
/*
 登录超时
 */
#define httpRequestResult_Login_timeOut 403
#define httpRequestResult_Login_timeOutMsg @"登录超时,请重新登录"

/*
 无网络文案
 */
#define httpMsg_noNetWork @"无网络,请检查网络后重试"

/*
 网络超时
 */
#define httpMsg_timeout @"服务不可用,请稍后重试"

/*
 服务器返回格式错误
 */
#define httpResponse_severError_code 998
#define httpResponse_severError_Msg @"服务器返回错误,请联系管理员"

#endif /* XPRequestResultDescribe_h */
