//
//  XPOSSUploadTool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/18.
//

#import "XPOSSUploadTool.h"
#import <AliyunOSSiOS/OSSService.h>
#import <MBProgressHUD.h>


@implementation XPOSSUploadTool

static XPOSSUploadTool * shareInstance;
+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstance = [[self alloc] init];
    });
    return shareInstance;
}

- (void)requestForConfig
{
    
//获取oss 配置信息 accessKeyId  accessKeySecret  endpoint
    
}

///上传本地文件
- (void)uploadFileWithMediaType:(XPMediaType)type filePath:(NSString *)filePath progress:(void(^)(CGFloat progress))progressBlock  completeBlock:(void(^)(BOOL success,NSString *remoteUrlStr))block {
    
    NSString *accessKey = @"LTAI5tQbhbqxy3jhU6q5stZn";
    NSString *accessKeySecret = @"0tkEct5DDBQ0GRi4vpAG5haJjrk6Fw";
    NSString *endpoint = @"oss-cn-beijing.aliyuncs.com";
    
    NSString *ossPath = [self getOssPathWithMidiaType:type];
    
    //配置oss参数
    static NSInteger uploadLogCount = 0;
    uploadLogCount++;
    
    id<OSSCredentialProvider> credential = [[OSSStsTokenCredentialProvider alloc] initWithAccessKeyId:accessKey secretKeyId:accessKeySecret securityToken:@""];
    OSSClientConfiguration * conf = [OSSClientConfiguration new];
    conf.backgroundSesseionIdentifier = [NSString stringWithFormat:@"XP_LOG_%zd", uploadLogCount];
    conf.enableBackgroundTransmitService = YES;
    conf.maxRetryCount = 2;
    conf.maxConcurrentRequestCount = 5;
    conf.timeoutIntervalForRequest = 30;
    conf.timeoutIntervalForResource = 120;
    NSString *cachesDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    OSSClient *client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential clientConfiguration:conf];
    OSSTaskCompletionSource *tcs = [OSSTaskCompletionSource taskCompletionSource];
    
    NSString *videoPath = filePath;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:videoPath]) {
        dispatch_queue_t uploadQueue = dispatch_queue_create("com.aliyun.oss.log", DISPATCH_QUEUE_CONCURRENT);
        dispatch_async(uploadQueue, ^{
            OSSResumableUploadRequest *resumableUpload = [OSSResumableUploadRequest new];
            resumableUpload.bucketName =@"oss608";
            resumableUpload.objectKey = ossPath;
            resumableUpload.uploadingFileURL = [NSURL fileURLWithPath:videoPath];
            resumableUpload.partSize = 1024 * 1024;
            resumableUpload.recordDirectoryPath = cachesDir;// 设置分片信息的本地存储路径
            resumableUpload.deleteUploadIdOnCancelling = NO;//默认为yes
            resumableUpload.completeMetaHeader = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"value1", @"x-oss-meta-name1",@"public-read",@"x-oss-object-acl", nil];
            
            resumableUpload.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
                
                CGFloat currentProgress = (CGFloat)totalByteSent/(CGFloat)totalBytesExpectedToSend;
                if (progressBlock) {
                    progressBlock(currentProgress);
                }
                NSLog(@"progress: %lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
            };
            OSSTask * resumeTask = [client resumableUpload:resumableUpload];
            [resumeTask continueWithBlock:^id _Nullable(OSSTask * _Nonnull task) {
                if (!task.error) {
                    NSLog(@"upload object success");
                    block(YES,[NSString stringWithFormat:@"https://oss608.oss-cn-beijing.aliyuncs.com/%@",ossPath]);
                } else {
                    NSLog(@"upload object failed, error: %@" , task.error);
                    block(NO,@"");
                }
                return nil;
            }];
            
            [tcs.task waitUntilFinished];
        });
    }else{
        block(NO,@"文件目录不存在");
    }
}
//获取oss 上传地址
- (NSString *)getOssPathWithMidiaType:(XPMediaType)type {
    NSString *ossPath;
    if (type == XPMediaTypeImage) {
        ossPath = [self getUploadImagePath:@"upload_image" soureceType:nil];
    }else if (type == XPMediaTypeVideo) {
        ossPath = [self getUploadImagePath:@"upload_image" soureceType:@"mp4"];
    }else if (type == XPMediaTypeUserHead){
        ossPath = [self getUploadImagePath:@"head" soureceType:nil];
    }else if (type == XPMediaTypeFeedback){
        ossPath = [self getUploadImagePath:@"feedback" soureceType:nil];
    }else if (type == XPMediaTypeShopComment){
        ossPath = [self getUploadImagePath:@"evaluate" soureceType:nil];
    }else if (type == XPMediaTypeShopAfterSale){
        ossPath = [self getUploadImagePath:@"after_sale" soureceType:nil];
    }else if (type == XPMediaTypeMoneyRecharge){
        ossPath = [self getUploadImagePath:@"public_transfer" soureceType:nil];
    }else if (type == XPMediaTypeMakerFaceHand){
        ossPath = [self getUploadImagePath:@"auth_hand" soureceType:nil];
    }else if(type == XPMediaTypeScoreImage){
        ossPath = [self getUploadImagePath:@"integral_keep" soureceType:nil];
    }
    return ossPath;
}

- (NSString *)getUploadImagePath:(NSString *)fileName soureceType:(NSString *)sourceType{
    
    NSString *userPhone = [XPLocalStorage readUserUserPhone];
    NSString *appName;
    if (APPTYPE == 0) {
        appName = @"xinpai";
    }else if (APPTYPE == 1){
        appName = @"zaowu";
    }
    if (sourceType == nil || sourceType.length == 0) {
        sourceType = @"png";
    }
    
    return [NSString stringWithFormat:@"%@/%@/%@/ios_%@.%@",appName,fileName,userPhone,[self currentDateWithFormatter:@"YYMMddHHmmsss"],sourceType];
}


- (NSString *)currentDateWithFormatter:(NSString *)str {
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:str];
    NSString *time = [formatter stringFromDate:date];
    return time;
}

//图片写入本地
- (NSString *)saveImageToSandbox:(UIImage *)image {
    // 1. 将 UIImage 转换为 NSData (保存为 PNG 格式)
    NSData *imageData = UIImagePNGRepresentation(image);
    
    if (imageData == nil) {
        NSLog(@"图片转换失败");
        return nil;
    }
    // 2. 获取沙盒 Documents 目录路径
    NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    // 3. 创建保存文件的路径 (使用文件名来保存图片)
    NSString *filePath = [documentDirectory stringByAppendingPathComponent:@"saved_image.png"];
    NSError *err;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        BOOL remove = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&err];
        if (remove) {
            NSLog(@"已存在,成功移除图片");
        } else {
            NSLog(@"移除失败: %@", err.localizedDescription);
        }
    }
    // 4. 将图片数据写入文件
    NSError *error;
    BOOL success = [imageData writeToFile:filePath options:NSDataWritingAtomic error:&error];
    
    if (success) {
        NSLog(@"图片已成功保存到路径: %@", filePath);
        return filePath;
    } else {
        NSLog(@"保存图片失败: %@", error.localizedDescription);
        return nil;
    }
    
}

@end
