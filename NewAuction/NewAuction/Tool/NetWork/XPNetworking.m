//
//  XPNetworking.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "XPNetworking.h"
#import "AFHTTPSessionManager.h"
#import "XPRequestResultDescribe.h"
#import "XPNetworkStatusClass.h"

#define XPLog(FORMAT, ...) {\
NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];\
[dateFormatter setDateStyle:NSDateFormatterMediumStyle];\
[dateFormatter setTimeStyle:NSDateFormatterShortStyle];\
NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];\
[dateFormatter setTimeZone:timeZone];\
[dateFormatter setDateFormat:@"HH:mm:ss.SSSSSSZ"];\
NSString *str = [dateFormatter stringFromDate:[NSDate date]];\
fprintf(stderr,"\nTIME：%s %s\n",[str UTF8String],[[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);\
}


@interface XPNetworking ()<NSURLSessionTaskDelegate>
{
    AFHTTPSessionManager *_manager;
}

@property(nonatomic,strong)NSURLSessionTask * myTask;

@end

@implementation XPNetworking

#pragma mark - singleton
static XPNetworking *shareInstance;
+ (instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstance = [[self alloc] init];
    });
    return shareInstance;
}
+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstance = [super allocWithZone:zone];
    });
    return shareInstance;
}

#pragma mark - life cycle
- (instancetype)init
{
    self = [super init];
    if (self) {
        if (!_manager) {
            _manager = [AFHTTPSessionManager manager];
        }
    }
    return self;
}

//头部信息集中处理
+ (void)addHeadInfomationWithRequest:(NSMutableURLRequest *)request
{
    // 设置token
    NSString * token = [XPLocalStorage readUserToken];
    [request setValue:token forHTTPHeaderField:@"token"];
    [request setValue:@"iOS" forHTTPHeaderField:@"X-Source"];
    if (APPTYPE == 0) {
        [request setValue:@"xp" forHTTPHeaderField:@"X-App-Type"];
    }else if (APPTYPE == 1){
        [request setValue:@"zw" forHTTPHeaderField:@"X-App-Type"];
    }
    //
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString * Version_str = [NSString stringWithFormat:@"%@",app_Version];
    [request setValue:Version_str forHTTPHeaderField:@"X-Version"];
    //手机系统
    NSString *sysytemName = [NSString stringWithFormat:@"iOS%@",[[UIDevice currentDevice] systemVersion]];
    [request setValue:sysytemName forHTTPHeaderField:@"X-SystemVersion"];
//    //设备唯一号
//    [request setValue:[XPCommonTool deviceUUId] forHTTPHeaderField:@"X-DeviceNum"];
}

//post
+ (void)xpPostWithURL:(NSString *)url Params:(id)params success:(SuccessBlock)success failure:(FailureBlock)failure
{
    [self requestByMothod:@"POST" URL:url Params:params success:success failure:failure];
}
//get
+ (void)xpGetWithURL:(NSString *)url Params:(id)params success:(SuccessBlock)success failure:(FailureBlock)failure
{
    [self requestByMothod:@"GET" URL:url Params:params success:success failure:failure];
}

+ (void)requestByMothod:(NSString *)mehthod URL:(NSString *)url Params:(id)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    XPLog(@"%@\n%@",url,params);
    url =  [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"#%^{}\"[]|\\<> "].invertedSet];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:mehthod];
    [XPNetworking addHeadInfomationWithRequest:request];
    //设置本次请求数据格式
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"" forHTTPHeaderField:@"Accept-Encoding"];
    // 参数拼接
    if (![mehthod isEqualToString:@"GET"]) {
        NSData * bodyData;
        if ([NSJSONSerialization isValidJSONObject:params]) {
            NSError *error;
            NSData *bodyData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
            NSString * requestStr = [[NSString alloc] initWithData:bodyData encoding:NSUTF8StringEncoding];
            bodyData = [requestStr dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:[[NSMutableData alloc]initWithData:bodyData]];
            //设置本次请求体长度
            [request setValue:[NSString stringWithFormat:@"%ld",(unsigned long)bodyData.length] forHTTPHeaderField:@"Content-Length"];
        }
    }
    
    //设置alive模式
    [request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    //cookie
    request.timeoutInterval = 10;//网络等待时间
    NSURLSession * seeion = [NSURLSession sharedSession] ;
    NSURLSessionTask * myTask  = [seeion dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSHTTPURLResponse *httpUrlResponse = (NSHTTPURLResponse *)response;

        if(httpUrlResponse.statusCode==200&&data.length>0){//处理登录超时问题
            NSError *error;
            NSDictionary *dicTemp = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            XPLog(@"%@\n%@",url,[self transformDic:dicTemp]);
            
            if ([dicTemp.allKeys containsObject:@"status"] && [dicTemp.allKeys containsObject:@"code"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    success(dicTemp);
                });
            }else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    failure(httpResponse_severError_Msg,httpResponse_severError_code);
                });
            }
        }else{
            XPLog(@"%@\n%@\n%@",url,error,httpUrlResponse);
            //判断是否有网络
            dispatch_async(dispatch_get_main_queue(), ^{
                if(![XPNetworkStatusClass isHaveNetwork]){
                    failure(httpMsg_noNetWork, httpUrlResponse.statusCode);
                }else{
                    failure(httpMsg_timeout, httpUrlResponse.statusCode);
                }
            });
        }
    }];
    [myTask resume];
}


//取消当前
- (void)cancelNetworking
{
    [self.myTask cancel];
}

+ (NSString *)transformDic:(NSDictionary *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *string = [[dic description] stringByReplacingOccurrencesOfString:@"\\u" withString:@"\\U"];
    string           = [string stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    string           = [[@"\"" stringByAppendingString:string] stringByAppendingString:@"\""];
    NSData *data     = [string dataUsingEncoding:NSUTF8StringEncoding];
    string           = [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListImmutable format:NULL error:NULL];
    return string;
}

@end
