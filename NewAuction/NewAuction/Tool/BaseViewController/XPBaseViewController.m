//
//  XPBaseViewController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "XPBaseViewController.h"
#import "MBProgressHUD.h"

@interface XPBaseViewController ()
/// gif 视图
@property (nonatomic, strong) UIView *gifView;

@property (nonatomic, strong) UIView *progressView;
//进度条
@property (nonatomic, strong) MBBarProgressView *hudView;

@end

@implementation XPBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (UIModalPresentationStyle)modalPresentationStyle {
    return UIModalPresentationFullScreen;
}


-(void)startLoadingGif
{
    [self stopLoadingGif];
    _loadingView = [self showImsageMessag:@"加载中" toView:[UIApplication sharedApplication].keyWindow];
}

-(void)stopLoadingGif
{
    [_loadingView removeFromSuperview];
    _loadingView = nil;
}

-(void)showProgressView{
    
    if (self.progressView) {
        return;
    }
    
    UIView * contentView = [[UIView alloc]initWithFrame:k_keyWindow.bounds];
    [contentView setBackgroundColor:UIColor.clearColor];
    contentView.layer.cornerRadius = 10;
    self.progressView = contentView;
    [k_keyWindow addSubview:contentView];
    
    
    _hudView = [[MBBarProgressView alloc] initWithFrame:CGRectMake(contentView.mj_w/2. - 60, contentView.mj_h/2., 120, 10)];
    _hudView.progressColor = UIColor.redColor;
    [self.progressView addSubview:_hudView];
    
}
- (void)hideProgressView{
    [self.hudView removeFromSuperview];
    [self.progressView removeFromSuperview];
    self.progressView = nil;
    self.progressView = nil;
}


- (UIView *)showImsageMessag:(NSString *)message toView:(UIView *)view{
    int width = view.frame.size.width;
    int height = view.frame.size.height;
    
    UIView * contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    [contentView setBackgroundColor:UIColor.clearColor];
    contentView.layer.cornerRadius = 10;
    [view addSubview:contentView];

    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 80, 80)];
    imageView.center = contentView.center;
    NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:@"load_gif" withExtension:@"gif"];
    //将GIF图片转换成对应的图片源
       CGImageSourceRef gifSource = CGImageSourceCreateWithURL((CFURLRef) fileUrl, NULL);
       //获取其中图片源个数，即由多少帧图片组成
       size_t frameCount = CGImageSourceGetCount(gifSource);
       //定义数组存储拆分出来的图片
       NSMutableArray *frames = [[NSMutableArray alloc] init];
       for (size_t i = 0; i < frameCount; i++) {
           //从GIF图片中取出源图片
           CGImageRef imageRef = CGImageSourceCreateImageAtIndex(gifSource, i, NULL);
           //将图片源转换成UIimageView能使用的图片源
           UIImage *imageName = [UIImage imageWithCGImage:imageRef];
           //将图片加入数组中
           [frames addObject:imageName];
           CGImageRelease(imageRef);
       }
    imageView.animationImages = frames;
    //每次动画时长
    imageView.animationDuration = 1.0;
    [imageView startAnimating];
    [contentView addSubview:imageView];

    return contentView;
}

/// 加载gif文件动画
/// @param msg msg为gif下面显示的文字,如果,文字传控,则不显示文字,gif动画居中显示.
- (void)showGifWithMsg:(NSString *)msg {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.gifView = [self createGifWithMessage:msg];
        [[UIApplication sharedApplication].keyWindow addSubview:self.gifView];
    });
}
-(void)dismissGif {
    [self.gifView removeFromSuperview];
}

-(UIView *)createGifWithMessage:(NSString *)msg {
    
    CGRect rect = [UIScreen mainScreen].bounds;
    
    UIView *contenView = [[UIView alloc] initWithFrame:rect];
    contenView.backgroundColor = UIColor.whiteColor;
    UIImageView *imageView = [[UIImageView alloc] init];
    
    if (msg.length > 0) {
        imageView.frame = CGRectMake(rect.size.width/2. - 44, rect.size.height/2. - 22 - 19, 88, 44);
        UILabel *msgLabel = [[UILabel alloc] initWithFrame:CGRectMake(58, rect.size.height/2. + 19, rect.size.width- 116, 20)];
        msgLabel.font = [UIFont systemFontOfSize:16];
        msgLabel.textColor = UIColor.blackColor;
        msgLabel.text = msg;
        msgLabel.textAlignment = NSTextAlignmentCenter;
        [contenView addSubview:msgLabel];
    }else{
        imageView.frame = CGRectMake(rect.size.width/2. - 44, rect.size.height/2. - 22, 88, 44);
    }

    NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:@"load_gif" withExtension:@"gif"];
    //将GIF图片转换成对应的图片源
       CGImageSourceRef gifSource = CGImageSourceCreateWithURL((CFURLRef) fileUrl, NULL);
       //获取其中图片源个数，即由多少帧图片组成
       size_t frameCount = CGImageSourceGetCount(gifSource);
       //定义数组存储拆分出来的图片
       NSMutableArray *frames = [[NSMutableArray alloc] init];
       for (size_t i = 0; i < frameCount; i++) {
           //从GIF图片中取出源图片
           CGImageRef imageRef = CGImageSourceCreateImageAtIndex(gifSource, i, NULL);
           //将图片源转换成UIimageView能使用的图片源
           UIImage *imageName = [UIImage imageWithCGImage:imageRef];
           //将图片加入数组中
           [frames addObject:imageName];
           CGImageRelease(imageRef);
       }
    imageView.animationImages = frames;
    //每次动画时长
    imageView.animationDuration = 1.0;
    [imageView startAnimating];
    [contenView addSubview:imageView];

    return contenView;
}

-(void)setProgress:(CGFloat)progress {
    self.hudView.progress = progress;
}

- (XPAlertView *)xpAlertView {
    if (_xpAlertView == nil) {
        _xpAlertView = [[XPAlertView alloc] initWithFrame:k_keyWindow.bounds];
        _xpAlertView.autoDissmiss = YES;
        [k_keyWindow addSubview:_xpAlertView];
    }
    return _xpAlertView;
}
-(void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)dealloc{
    NSLog(@"%@ ########## %@",[self class],NSStringFromSelector(_cmd));
}
@end
