//
//  XPBaseViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import <UIKit/UIKit.h>
#import "XPAlertView.h"
#import "MBProgressHUD.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPBaseViewController : UIViewController
{
    UIView *_loadingView;
}

@property (nonatomic, strong)XPAlertView *xpAlertView;

-(void)showProgressView;
- (void)hideProgressView;
@property (nonatomic, assign) CGFloat progress;

//普通旋转加载loading
-(void)startLoadingGif;
-(void)stopLoadingGif;

/// 加载gif文件动画
/// @param msg msg为gif下面显示的文字,如果,文字传控,则不显示文字,gif动画居中显示.
- (void)showGifWithMsg:(NSString *)msg;
- (void)dismissGif;

-(void)goBack;

@end

NS_ASSUME_NONNULL_END
