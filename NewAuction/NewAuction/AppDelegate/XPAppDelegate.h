//
//  XPAppDelegate.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/6.
//

#import <UIKit/UIKit.h>

@interface XPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@property(nonatomic,assign)BOOL allowRotation;

@end

