//
//  AppDelegate.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/6.
//

#import "XPAppDelegate.h"
#import "XPTabBarController.h"
#import "IQKeyboardManager.h"
#import "XPLoginViewController.h"
#import "XPLocalStorage.h"
#import "XPNavigationController.h"
#import "XPUMShareTool.h"
#import "WXApi.h"
#import <AlipaySDK/AlipaySDK.h>

@interface XPAppDelegate ()<WXApiDelegate>

@end

@implementation XPAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    XPTabBarController *rootVC = [[XPTabBarController alloc] init];
    self.window.rootViewController = rootVC;
    [self.window makeKeyAndVisible];
    
    
    IQKeyboardManager.sharedManager.toolbarDoneBarButtonItemText = @"完成";
    
    [[XPUMShareTool shareInstance] configUSharePlatforms];
    
//    [WXApi registerApp:XP_wechat_appKey universalLink:XP_wechat_universalLink];
    
    if (@available(iOS 15.0, *)) {
        [UITableView appearance].sectionHeaderTopPadding = 0;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoginOut) name:Notification_user_login_out object:nil];
    
    return YES;
    
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(nullable UIWindow *)window {
    if (self.allowRotation == YES) {
        //横屏
        return UIInterfaceOrientationMaskLandscapeRight;
    } else {
        //竖屏
        return UIInterfaceOrientationMaskPortrait;
    }
}

-(BOOL)application:(UIApplication*)application continueUserActivity:(NSUserActivity*)userActivity restorationHandler:(void(^)(NSArray* __nullable restorableObjects))restorationHandler
{
    if(![[XPUMShareTool shareInstance] umHandleUniversalLink:userActivity]){//友盟时候要回调,不回调需要继续判断其他sdk
        // 其他SDK的回调
        
    }
    
    BOOL handled = [WXApi handleOpenUniversalLink:userActivity delegate:self];
    
    if ([userActivity.webpageURL.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:userActivity.webpageURL standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
        }];
    }
    
    return handled;
}

-(BOOL)application:(UIApplication*)application openURL:(NSURL *)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
//6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    
    BOOL result = [[XPUMShareTool shareInstance] umhandleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if(!result){
    }
    BOOL handled = [WXApi handleOpenURL:url delegate:self];
    return result;
}

-(BOOL)application:(UIApplication*)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id>*)options {
    
    BOOL result = [[XPUMShareTool shareInstance] umOpenURL:url options:options];
    if(!result){
        NSLog(@"失败----- ");
    }
    BOOL handled = [WXApi handleOpenURL:url delegate:self];
    
    return handled;
}

//微信成功后的回调
-(void)onResp:(BaseResp *)resp{
    if ([resp isKindOfClass:[PayResp class]]) {
        PayResp *response = (PayResp *)resp;
        if (response.errCode == WXSuccess) {
         

        } else {
            [[XPShowTipsTool shareInstance] showMsg:@"支付失败!" ToView:k_keyWindow];
        }
    }
}


- (void)userLoginOut {
    [XPLocalStorage cleanAllUserInfo];
}

@end
