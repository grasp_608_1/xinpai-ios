//
//  XPSlideItemView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ActionBlock)(NSInteger index);

@interface XPSlideItemView : UIScrollView

@property (nonatomic, assign) NSUInteger defaultSeletIndex;

@property (nonatomic, copy) ActionBlock tapBlock;
//需要再调用 configWithtitleArray 之后使用
@property (nonatomic, assign) BOOL hidenDownLine;

- (void)configWithtitleArray:(NSArray *)titleArray;


@end

NS_ASSUME_NONNULL_END
