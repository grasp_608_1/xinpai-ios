//
//  FormatTimerView.h
//  postgraduate
//
//  Created by Liu on 2021/3/25.
//  Copyright © 2021 学为贵. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FormatTimerView : UIView

@property (nonatomic, assign) NSInteger seconds;

@end

NS_ASSUME_NONNULL_END
