//
//  FormatTimerView.m
//  postgraduate
//
//  Created by Liu on 2021/3/25.
//  Copyright © 2021 学为贵. All rights reserved.
//

#import "FormatTimerView.h"

@interface FormatTimerView ()

@property (nonatomic, copy) NSString *hour;

@property (nonatomic, copy) NSString *min;

@property (nonatomic, copy) NSString *second;

@end

@implementation FormatTimerView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupViews];
    }
    return self;
}

-(void)setupViews{
    
    for (int i = 0; i<3; i++) {
        UILabel *label = [UILabel new];
        label.textColor = UIColor.whiteColor;
        label.backgroundColor = UIColorFromRGB(0x6E6BFF);
        label.font = kFontRegular(12);
        label.textAlignment = NSTextAlignmentCenter;
        label.frame = CGRectMake(i*27, 0,19, 16);
        label.tag = i+999;
        label.layer.cornerRadius = 2;
        label.clipsToBounds = YES;
        label.text = @"00";
        [self addSubview:label];
        
        if (i<2) {
            
            UILabel *signLabel = [UILabel new];
            signLabel.textAlignment = NSTextAlignmentCenter;
            signLabel.textColor = label.backgroundColor;
            signLabel.text = @":";
            signLabel.font = label.font;
            signLabel.frame = CGRectMake(19 + i*27, 0, 8, 16);
            [self addSubview:signLabel];
            
        }
        
    }
}

-(void)setSeconds:(NSInteger)seconds{
    
    [self formatTimerWithSeconds:seconds];
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
       
        if (obj.class == [UILabel class]) {
            UILabel *label = (UILabel *)obj;
            if (label.tag == 999) {
                label.text = _hour;
            }else if(label.tag == 1000){
                label.text = _min;
            }else if(label.tag == 1001){
                label.text = _second;
            }
        }
        
    }];
    
}

-(void)formatTimerWithSeconds:(NSInteger)seconds{
    
    _hour = [NSString stringWithFormat:@"%02ld",seconds/3600];
    _min = [NSString stringWithFormat:@"%02ld",(seconds%3600)/60];
    _second = [NSString stringWithFormat:@"%02ld",seconds%60];

}


@end
