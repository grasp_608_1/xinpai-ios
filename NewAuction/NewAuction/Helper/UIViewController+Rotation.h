//
//  UIViewController+Rotation.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Rotation)
/**
 强制横屏方法
 横屏(如果属性值为YES,仅允许屏幕向右旋转,否则仅允许竖屏)
 @param fullscreen 屏幕方向
 */
- (void)setNewOrientation:(BOOL)fullscreen;

@end

NS_ASSUME_NONNULL_END
