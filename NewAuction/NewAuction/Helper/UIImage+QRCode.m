//
//  UIImage+QRCode.m
//  NewAuction
//
//  Created by Grasp_L on 2025/2/7.
//

#import "UIImage+QRCode.h"
#import <CoreImage/CoreImage.h>


@implementation UIImage (QRCode)

+ (UIImage *)generateQRCodeFromString:(NSString *)string {
    // 将字符串转换为 NSData
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    // 使用 CIFilter 创建二维码
    CIFilter *qrCodeFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrCodeFilter setValue:data forKey:@"inputMessage"];
    
    // 获取生成的二维码图像
    CIImage *ciImage = qrCodeFilter.outputImage;
    
    // 将 CIImage 转换为 UIImage
    UIImage *qrCodeImage = [self createNonInterpolatedUIImageFromCIImage:ciImage withSize:200.0f];
    
    return qrCodeImage;
}

+ (UIImage *)createNonInterpolatedUIImageFromCIImage:(CIImage *)image withSize:(CGFloat)size {
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = size / CGRectGetWidth(extent);
    
    // 使用上下文将 CIImage 转换为 UIImage
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:image fromRect:extent];
    
    // 创建 UIImage
    UIImage *nonInterpolatedImage = [UIImage imageWithCGImage:cgImage scale:scale orientation:UIImageOrientationUp];
    
    CGImageRelease(cgImage);
    
    return nonInterpolatedImage;
}

@end
