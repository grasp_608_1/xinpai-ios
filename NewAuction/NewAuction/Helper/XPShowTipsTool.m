//
//  XPShowTipsTool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import "XPShowTipsTool.h"
#import "MBProgressHUD.h"

static NSTimeInterval normalHudDuring = 2.0;
static NSTimeInterval longHudDuring   = 3.0;

@implementation XPShowTipsTool

static XPShowTipsTool *instance;

+ (instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

/// 吐司
/// - Parameters:
///   - msg: 要展示的文字
///   - targetView: 吐司展示的父view
- (void)showMsg:(NSString *)msg ToView:(UIView *)targetView{
    [self showMsg:msg ToView:targetView During:normalHudDuring];
}

- (void)showLongDuringMsg:(NSString *)msg ToView:(UIView *)targetView{
    [self showMsg:msg ToView:targetView During:longHudDuring];
}

-(void)showMsg:(NSString *)msg ToView:(UIView *)targetView During:(CGFloat)during{
    if (targetView == nil) {
        targetView = k_keyWindow;
    }
    
    if (![msg isKindOfClass:[NSString class]]) {
        return;
    }else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:targetView animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.detailsLabel.text = msg;
        hud.detailsLabel.font = [UIFont fontWithName:@"Arial" size:16];
        hud.contentColor = UIColor.blackColor;
        // 隐藏时候从父控件中移除
    //    hud.removeFromSuperViewOnHide = YES;
    //    // YES代表需要蒙版效果
    //    hud.dimBackground = NO;
        //1 秒后消失
        [hud hideAnimated:YES afterDelay:during];
    }

}

@end
