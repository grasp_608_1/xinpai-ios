//
//  XPSelectItemView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ActionBlock)(NSInteger index);

@interface XPSelectItemView : UIView

@property (nonatomic, assign) NSUInteger defaultSeletIndex;

@property (nonatomic, copy) ActionBlock tapBlock;

- (void)configWithtitleArray:(NSArray *)titleArray;

- (void)hiddenLine;
- (void)showDownSepLine;

@end

NS_ASSUME_NONNULL_END
