//
//  UIViewController+iOS11.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import <UIKit/UIKit.h>
#import "XPNavTopView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (iOS11)
//刘海屏安全头部
@property (nonatomic, assign) float safeTop;
//刘海屏安全底部
@property (nonatomic, assign) float safeBottom;
//navBar
@property (nonatomic, strong) XPNavTopView *navBarView;

//导航栏
-(void)setupNavBarView;

@end

NS_ASSUME_NONNULL_END
