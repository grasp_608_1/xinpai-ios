//
//  XPLimitInputTextView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/10.
//

#import "XPLimitInputTextView.h"


@interface XPLimitInputTextView ()<UITextViewDelegate>
//可输入最大数量
@property (nonatomic, assign) NSInteger maxWordCount;


@end

@implementation XPLimitInputTextView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    
    self.layer.borderColor = UIColorFromRGB(0xF2F2F2).CGColor;
    self.layer.borderWidth = 1;
    self.layer.cornerRadius = 8;
    self.clipsToBounds = YES;
    self.backgroundColor = UIColor.whiteColor;
    
    self.contentView = [[SZTextView alloc] initWithFrame:CGRectMake(5,5, self.mj_w - 10, self.mj_h - 30)];
    self.contentView.font = kFontRegular(14);
    self.contentView.delegate = self;
    self.contentView.placeholder = @"请输入您的问题描述";
    [self addSubview:self.contentView];
    
    self.numLabel = [UILabel new];
    self.numLabel.textColor = UIColorFromRGB(0x8A8A8A);
    self.numLabel.textAlignment = NSTextAlignmentRight;
    self.numLabel.font = kFontRegular(14);
    self.numLabel.text = @"0 / 0";
    self.numLabel.frame =CGRectMake(0, self.mj_h - 30 , self.mj_w - 12, 30);
    [self addSubview:_numLabel];
    
}

- (void)configWithMaxCount:(NSInteger)maxCount textViewPlaceholder:(NSString *)placeholder {
    _maxWordCount = maxCount;
    self.contentView.placeholder = placeholder;
    self.numLabel.text = [NSString stringWithFormat:@"0 / %zd",maxCount];
    
}

#pragma mark --- textView Delegate
-(void)textViewDidChange:(UITextView *)textView{
    
    if (self.changeBlock) {
        self.changeBlock();
    }
    
    if (textView.text.length > 0) {
        
        if (self.contentView.text.length>self.maxWordCount) {
            self.contentView.text = [self.contentView.text substringToIndex:self.maxWordCount];
        }
        self.numLabel.text = [NSString stringWithFormat:@"%tu / %zd",self.contentView.text.length,self.maxWordCount];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([XPSwiftTool.new containsEmoji:text] && !self.canInputEmoji) {
        return NO;
    }else {
        return YES;
    }
}

-(void)setMaxWords:(NSInteger)maxWords {
    _maxWordCount = maxWords;
    self.numLabel.text = [NSString stringWithFormat:@"0 / %zd",maxWords];
}
@end
