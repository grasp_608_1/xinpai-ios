//
//  XPOrderDataTool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPOrderDataTool : NSObject
+ (NSString *)getStatusTitle:(NSString *)statusStr;
@end

NS_ASSUME_NONNULL_END
