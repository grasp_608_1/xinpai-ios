//
//  XPAlertView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/2.
//

#import "XPAlertView.h"

@interface XPAlertView ()<UITextFieldDelegate>

@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *desLabel;

@property (nonatomic, strong) UIButton *cancelButton;

@property (nonatomic, strong) UIButton *sureButton;

@property (nonatomic, assign) XPAlertType type;

@end

static  NSTimeInterval xpAlertduring = 0.5;

@implementation XPAlertView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    
    self.autoDissmiss = YES;

    UIView *bgView = [[UIView alloc] initWithFrame:self.bounds];
    bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    [self addSubview:bgView];
    
    UIView *mainView = [UIView new];
    mainView.backgroundColor = UIColor.whiteColor;
    mainView.layer.cornerRadius = 20;
    mainView.layer.masksToBounds = YES;
    mainView.frame = CGRectMake(40,self.mj_h/2 - 200 , self.mj_w - 80, 250);
    self.mainView = mainView;
    [self addSubview:mainView];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    titleLabel.font = kFontMedium(16);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.frame = CGRectMake(20, 20, mainView.mj_w - 40, 20);
    titleLabel.numberOfLines = NO;
    self.titleLabel = titleLabel;
    [mainView addSubview:titleLabel];
    
    UITextField *textField = [UITextField new];
    textField.backgroundColor = UIColorFromRGB(0xF2F2F2);
    textField.textColor = UIColorFromRGB(0x3A3C41);
    textField.frame = CGRectMake(20, 64, self.mainView.mj_w - 40, 45);
    textField.keyboardType = UIKeyboardTypeNumberPad;
    textField.layer.cornerRadius = 4;
    textField.font = kFontMedium(16);
    textField.leftView = [self createTextFieldLeftView];
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.delegate = self;
    self.textField = textField;
    [mainView addSubview:textField];
    
    UILabel *desLabel = [[UILabel alloc] init];
    desLabel.textColor = UIColorFromRGB(0x8A8A8A);
    desLabel.font = kFontRegular(14);
    desLabel.textAlignment = NSTextAlignmentCenter;
    desLabel.frame = CGRectMake(20, MaxY(self.textField) + 6, mainView.mj_w -40 , 24);
    desLabel.numberOfLines = NO;
    self.desLabel = desLabel;
    [mainView addSubview:desLabel];
    
    
    CGFloat buttonW = (self.mainView.mj_w - 28*2 -20)/2.;
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(28,MaxY(self.desLabel) + 30,buttonW, 40);
    [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    cancelButton.titleLabel.font = kFontMedium(14);
    [cancelButton setTitleColor:UIColorFromRGB(0x6E6BFF) forState:UIControlStateNormal];
    cancelButton.layer.cornerRadius = 17.5;
    cancelButton.layer.borderColor = UIColorFromRGB(0x6E6BFF).CGColor;
    cancelButton.layer.borderWidth = 1;
    cancelButton.clipsToBounds = YES;
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.cancelButton = cancelButton;
    [self.mainView addSubview:cancelButton];
    
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sureButton.frame = CGRectMake(MaxX(cancelButton) + 20,MaxY(self.desLabel) + 30,buttonW, 40);
    [sureButton setTitle:@"确定" forState:UIControlStateNormal];
    sureButton.titleLabel.font = kFontMedium(14);
    [sureButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    sureButton.layer.cornerRadius = 17.5;
    sureButton.clipsToBounds = YES;
    sureButton.backgroundColor = UIColorFromRGB(0x6E6BFF);
    [sureButton addTarget:self action:@selector(sureButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.sureButton = sureButton;
    [self.mainView addSubview:sureButton];
    self.alpha = 0;
    
}
- (void)configWithTitle:(NSString *)title desCriptionStr:(NSString *)desStr  placeHolderStr:(NSString *)placeholder{
    self.titleLabel.text = title;
    self.textField.placeholder = placeholder;
    
    CGFloat desH = [desStr boundingRectWithSize:CGSizeMake(self.desLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.desLabel.font} context:nil].size.height;
    self.desLabel.frame = CGRectMake(20, MaxY(self.textField) + 6, self.mainView.mj_w -40 , desH);
    
    self.desLabel.text = desStr;
    
    CGFloat buttonW = (self.mainView.mj_w - 28*2 -20)/2.;
    self.cancelButton.frame = CGRectMake(28,MaxY(self.desLabel) + 30,buttonW, 35);
    self.sureButton.frame = CGRectMake(MaxX(self.cancelButton) + 20,MaxY(self.desLabel) + 30,buttonW, 35);
    self.mainView.frame = CGRectMake(40,self.mj_h/2 - 180 , self.mj_w - 80, MaxY(self.cancelButton) + 20);
    
}

- (void)configWithTitle:(NSString *)title desCriptionStr:(NSString *)desStr  placeHolderStr:(NSString *)placeholder leftAction:(LeftBlock)leftBlock rightAction:(RightBlock)rightBlock alertType:(XPAlertType)type{
    [UIView animateWithDuration:xpAlertduring animations:^{
        self.alpha = 1;
    }];
    
    self.textField.text = @"";
    
    self.leftBlock = leftBlock;
    self.rightBlock = rightBlock;
    self.type = type;
    
    CGFloat buttonW = (self.mainView.mj_w - 28*2 -20)/2.;
    
    if (type == XPAlertTypeInput) {
        self.cancelButton.hidden = NO;
        self.textField.placeholder = placeholder;
        self.titleLabel.text = title;
        CGFloat desH = [desStr boundingRectWithSize:CGSizeMake(self.desLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.desLabel.font} context:nil].size.height;
        self.desLabel.frame = CGRectMake(20, MaxY(self.textField) + 6, self.mainView.mj_w -40 , desH);
        
        self.desLabel.text = desStr;
        
        if (desStr.length == 0) {
            self.cancelButton.frame = CGRectMake(28,MaxY(self.textField) + 30,buttonW, 35);
        }else {
            self.cancelButton.frame = CGRectMake(28,MaxY(self.desLabel) + 30,buttonW, 35);
        }
        
        
        self.sureButton.frame = CGRectMake(MaxX(self.cancelButton) + 20,self.cancelButton.mj_y,buttonW, 35);
        self.mainView.frame = CGRectMake(40,self.mj_h/2 - 180 , self.mj_w - 80, MaxY(self.cancelButton) + 20);
    }else if (type == XPAlertTypeNormal) {
        self.textField.hidden = YES;
        self.cancelButton.hidden = NO;
        self.titleLabel.text = title;
        CGFloat titleH = [title boundingRectWithSize:CGSizeMake(self.titleLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.titleLabel.font} context:nil].size.height;
        self.titleLabel.frame = CGRectMake(20, 40, self.mainView.mj_w - 40, titleH > 20 ? titleH : 20);
        self.cancelButton.frame = CGRectMake(28,MaxY(self.titleLabel) + 40,buttonW, 35);
        self.sureButton.frame = CGRectMake(MaxX(self.cancelButton) + 20,self.cancelButton.mj_y,buttonW, 35);
        self.mainView.frame = CGRectMake(40,self.mj_h/2 - 180 , self.mj_w - 80, MaxY(self.cancelButton) + 20);
    }else  if (type == XPAlertTypeSure){
        self.textField.hidden = YES;
        self.cancelButton.hidden = YES;
        self.titleLabel.text = title;
        CGFloat titleH = [title boundingRectWithSize:CGSizeMake(self.titleLabel.mj_w, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.titleLabel.font} context:nil].size.height;
        self.titleLabel.frame = CGRectMake(20, 40, self.mainView.mj_w - 40, titleH > 20 ? titleH : 20);
        self.sureButton.frame = CGRectMake(57,MaxY(self.titleLabel) + 30,self.mainView.mj_w - 57 *2, 32);
        self.mainView.frame = CGRectMake(40,self.mj_h/2 - 180 , self.mj_w - 80, MaxY(self.sureButton) + 20);
    }
    
}

- (void)showAlertWithTitle:(NSString *)titleStr actionBlock:(RightBlock)rightBlock{
    [self configWithTitle:titleStr desCriptionStr:@"" placeHolderStr:@"" leftAction:^(NSString * _Nullable extra) {
        
    } rightAction:rightBlock alertType:XPAlertTypeSure];
}

- (void)showCancelAlertWithTitle:(NSString *)titleStr leftAction:(LeftBlock)leftBlock rightAction:(RightBlock)rightBlock{
    [self configWithTitle:titleStr desCriptionStr:@"" placeHolderStr:@"" leftAction:leftBlock  rightAction:rightBlock alertType:XPAlertTypeNormal];
}

- (UIView *)createTextFieldLeftView {
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 0, 12, 40);
    return view;
}

- (void)cancelButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender)
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    if (self.leftBlock) {
        self.leftBlock(@"");
        [UIView animateWithDuration:xpAlertduring animations:^{
            self.alpha = 0;
        }];
    }
    
}
- (void)sureButtonClicked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender)
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    if (self.rightBlock) {
        if(self.type == XPAlertTypeInput){
            self.rightBlock(self.textField.text);
            if (self.autoDissmiss) {
                [UIView animateWithDuration:xpAlertduring animations:^{
                    self.alpha = 0;
                }];
            }
        }else if (self.type == XPAlertTypeNormal || self.type == XPAlertTypeSure){
            self.rightBlock(@"");
            if (self.autoDissmiss) {
                [UIView animateWithDuration:xpAlertduring animations:^{
                    self.alpha = 0;
                }];
            }
        }
    }
}

- (void)dismiss{
    [UIView animateWithDuration:xpAlertduring animations:^{
        self.alpha = 0;
    }];
    
}

#pragma mark --- textField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.textField) {
        // 获取当前文本框中的文本，包括用户输入的新字符
        NSString *currentText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        // 如果当前文本长度超过6位，则不允许输入更多字符
        if (currentText.length > 6) {
            return NO;
        }
    }
    return YES;
}
@end

