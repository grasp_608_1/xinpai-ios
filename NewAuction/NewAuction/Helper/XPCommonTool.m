//
//  XPCommonTool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "XPCommonTool.h"
#import "HXFKeychain.h"

@implementation XPCommonTool

+(NSString *)currentDeviceSystem
{
    return [[UIDevice currentDevice] systemVersion];
}

+ (NSString *)deviceUUId
{
    if([[HXFKeychain getUUID] length] == 0)
    {
        NSString *UUIDString = [[UIDevice currentDevice].identifierForVendor UUIDString];
        [HXFKeychain saveUUID:UUIDString];
    }
    return [HXFKeychain getUUID];
}


//前后去空格方法
+ (NSString *)spaceByTrimmingString:(NSString *)string
{
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

+ (BOOL)stringIsNull:(NSString *)string
{
    if([string isKindOfClass:[NSNull class]]||string.length==0||string==nil||[string isEqualToString:@"(null)"]){
        return YES;
    }else{
        return NO;
    }
}

+ (BOOL)isLargeScreeniPhone{
    if(@available(iOS 11.0,*)){
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        if(window.safeAreaInsets.bottom>0){
            return YES;
        }
        return NO;
    }else{
        return NO;
    }
}

//获取navbar的高度
+ (CGFloat)navBarHeight
{
    if([XPCommonTool isLargeScreeniPhone]){
        return 44 + [UIApplication sharedApplication].statusBarFrame.size.height;
    }else{
        return 64;
    }
}

//获取tabbar的高度
+ (CGFloat)tabbarHeight
{
    if([XPCommonTool isLargeScreeniPhone]){
        return 83;
    }else{
        return 49;
    }
}
+ (CGFloat)getStatusBarHight {
    float statusBarHeight = 0;
    if (@available(iOS 13.0, *)) {
        UIStatusBarManager *statusBarManager = [UIApplication sharedApplication].windows.firstObject.windowScene.statusBarManager;
        statusBarHeight = statusBarManager.statusBarFrame.size.height;
    }
    else {
        statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    }
    return statusBarHeight;
}

+ (UIViewController *)getCurrentViewController {
    UIViewController* currentViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    BOOL runLoopFind = YES;
    while (runLoopFind) {
        if (currentViewController.presentedViewController) {
            currentViewController = currentViewController.presentedViewController;
        } else if ([currentViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController* navigationController = (UINavigationController* )currentViewController;
            currentViewController = [navigationController.childViewControllers lastObject];
        } else if ([currentViewController isKindOfClass:[UITabBarController class]]) {
            UITabBarController* tabBarController = (UITabBarController* )currentViewController;
            currentViewController = tabBarController.selectedViewController;
        } else {
            NSUInteger childViewControllerCount = currentViewController.childViewControllers.count;
            if (childViewControllerCount > 0) {
                currentViewController = currentViewController.childViewControllers.lastObject;
                return currentViewController;
            } else {
                return currentViewController;
            }
        }
    }
    return currentViewController;
}

+ (UINavigationController *)currentNavigationController {
    return [self getCurrentViewController].navigationController;
}

#pragma mark -- 弹窗实现方法
+ (UIAlertController *)showAlertWithTitle:(NSString*)title
                   message:(NSString*)message
                  btnTitle:(NSString*)btnTitle
                  callBack:(void(^)(void))callBack
                    withVC:(UIViewController*)VC{
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alertC.view.tintColor = app_main_color;
    UIAlertAction *left = [UIAlertAction actionWithTitle:btnTitle                                                               style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        if (callBack) {
            callBack();
        }
    }];
    [alertC addAction:left];
    [VC presentViewController:alertC animated:YES completion:nil];
    return alertC;
}

+ (UIAlertController *)showAlertWithTitle:(NSString*)title
                   message:(NSString*)message
               btnTitleArr:(NSArray*)btnTitleArr
                  leftBack:(void(^)(void))leftBack
                 rightBack:(void(^)(void))rigthBack
                    withVC:(UIViewController*)VC{
    NSString *leftTitle;
    NSString *rightTitle;
    if (btnTitleArr.count < 2) {
        return nil;
    }
    leftTitle = [btnTitleArr objectAtIndex:0];
    rightTitle = [btnTitleArr objectAtIndex:1];
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alertC.view.tintColor = app_main_color;
    UIAlertAction *left = [UIAlertAction actionWithTitle:leftTitle                                                               style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        if (leftBack) {
            leftBack();
        }
    }];
    [alertC addAction:left];
    UIAlertAction *right = [UIAlertAction actionWithTitle:rightTitle                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if (rigthBack) {
            rigthBack();
        }
    }];
    [alertC addAction:right];
    [VC presentViewController:alertC animated:YES completion:nil];
    return alertC;
}

+(NSString *)convertToJsonData:(id)obj
{
    NSError * error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:obj options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString;
    if (!jsonData) {
        NSLog(@"%@",error);
        
    }else{
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    NSRange range = {0,jsonString.length}; //去掉字符串中的空格
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    NSRange range2 = {0,mutStr.length};
        //去掉字符串中的换行符
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    return mutStr;
}

//背景渐变色
+ (CAGradientLayer *)setGradualChangingFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint Location:(NSArray *)locations :(UIView *)view colors:(NSArray *)colorArray{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = view.bounds;
    //    @[(__bridge id)startColor.CGColor,(__bridge id)endColor.CGColor];
    gradientLayer.colors = colorArray;
    gradientLayer.startPoint = startPoint;
    gradientLayer.endPoint = endPoint;
    gradientLayer.locations = locations;
    return gradientLayer;
    
}

+ (void)cornerRadius:(UIView*)view andType:(NSInteger)type radius:(CGFloat)rad
{
    if (type == 1) {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(rad, rad)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = view.bounds;
        maskLayer.path = maskPath.CGPath;
        view.layer.mask = maskLayer;
    }else if (type == 2)
    {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(rad, rad)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = view.bounds;
        maskLayer.path = maskPath.CGPath;
        view.layer.mask = maskLayer;
    }else if (type == 3)
    {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(rad, rad)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = view.bounds;
        maskLayer.path = maskPath.CGPath;
        view.layer.mask = maskLayer;
    }else if (type == 4)
    {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(rad, rad)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = view.bounds;
        maskLayer.path = maskPath.CGPath;
        view.layer.mask = maskLayer;
    }else if (type == 5){
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(rad, rad)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = view.bounds;
        maskLayer.path = maskPath.CGPath;
        view.layer.mask = maskLayer;
    }else if (type == 6){
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerTopLeft cornerRadii:CGSizeMake(rad, rad)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = view.bounds;
        maskLayer.path = maskPath.CGPath;
        view.layer.mask = maskLayer;
    }else if (type == 7){
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerTopLeft cornerRadii:CGSizeMake(rad, rad)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = view.bounds;
        maskLayer.path = maskPath.CGPath;
        view.layer.mask = maskLayer;
    }
    else
    {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(0, 0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = view.bounds;
        maskLayer.path = maskPath.CGPath;
        view.layer.mask = maskLayer;
    }
    
}
//正则
+(BOOL)validVerifyPredicate:(NSString *) pString TargetString:(NSString *)targerString
{
   NSPredicate *num = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pString];
   return [num evaluateWithObject:targerString];
}

+ (NSData *)zipImage:(UIImage *)image{
    NSData *imagedata;
    if (image) {
        imagedata =UIImageJPEGRepresentation(image, 0.01f);
        if (imagedata.length>300 * 300) {
            if (imagedata.length>300 * 300) {//10M以及以上
                imagedata=UIImageJPEGRepresentation(image, 0.01);//压缩之后1M~
            }else if (imagedata.length>1024*1024){//5M~10M
                imagedata=UIImageJPEGRepresentation(image, 0.01);//压缩之后1M~2M
            }else if (imagedata.length>2048*1024){//2M~5M
                imagedata=UIImageJPEGRepresentation(image, 0.01);//压缩之后1M~2.5M
            }
        }
    }
    return imagedata;
}

+ (NSData *)zipImageWithMaxSize:(NSInteger)maxSizeInBytes image:(UIImage *)image {
    CGFloat compression = 1.0;
    NSData *imageData = UIImageJPEGRepresentation(image, compression);
    
    // 循环压缩，直到达到目标大小
    while (imageData.length > maxSizeInBytes && compression > 0.1) {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(image, compression);
    }
    return imageData;
}

+ (UIImage*)viewToImage:(UIView *)view
{
     UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
    
     CGContextRef ctx = UIGraphicsGetCurrentContext();
     [view.layer renderInContext:ctx];
     UIImage* tImage = UIGraphicsGetImageFromCurrentImageContext();
    
     UIGraphicsEndImageContext();
    
     return tImage;
}

+ (BOOL) isEmpty:(NSString *) str {
    if (!str) {
        return true;
    } else {
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *trimedString = [str stringByTrimmingCharactersInSet:set];
        if ([trimedString length] == 0) {
            return true;
        } else {
            return false;
        }
    }
}
//获取16进制颜色的方法
+ (UIColor *)colorWithHex:(NSString *)hexColor {
    hexColor = [hexColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([hexColor length] < 6) {
        return nil;
    }
    if ([hexColor hasPrefix:@"#"]) {
        hexColor = [hexColor substringFromIndex:1];
    }
    NSRange range;
    range.length = 2;
    range.location = 0;
    NSString *rs = [hexColor substringWithRange:range];
    range.location = 2;
    NSString *gs = [hexColor substringWithRange:range];
    range.location = 4;
    NSString *bs = [hexColor substringWithRange:range];
    unsigned int r, g, b, a;
    [[NSScanner scannerWithString:rs] scanHexInt:&r];
    [[NSScanner scannerWithString:gs] scanHexInt:&g];
    [[NSScanner scannerWithString:bs] scanHexInt:&b];
    if ([hexColor length] == 8) {
        range.location = 6;
        NSString *as = [hexColor substringWithRange:range];
        [[NSScanner scannerWithString:as] scanHexInt:&a];
    } else {
        a = 255;
    }
    return [UIColor colorWithRed:((float)r / 255.0f) green:((float)g / 255.0f) blue:((float)b / 255.0f) alpha:((float)a / 255.0f)];
}

+ (CGFloat)calculateHeightForAttributedString:(NSAttributedString *)attributedString withWidth:(CGFloat)width {
    // 设置最大宽度（给定的宽度）
    CGSize maxSize = CGSizeMake(width, CGFLOAT_MAX);
    
    // 使用 boundingRectWithSize 方法来计算所需的矩形区域
    CGRect textRect = [attributedString boundingRectWithSize:maxSize
                                                     options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                     context:nil];
    
    // 返回计算得到的高度，向上取整
    return ceil(textRect.size.height);
}

+ (BOOL)areDictionariesValuesEqual:(NSDictionary *)dict1 secondDict:(NSDictionary *)dict2 {
    // 首先判断两个字典的数量是否相同
    if (dict1.count != dict2.count) {
        return NO;
    }
    
    // 遍历第一个字典中的所有键
    for (id key in dict1) {
        id value1 = dict1[key];
        id value2 = dict2[key];
        
        // 比较对应键的值是否相等
        if (![value1 isEqual:value2]) {
            return NO;  // 如果有一个值不相等，返回NO
        }
    }
    
    // 所有值都相等
    return YES;
}
@end
