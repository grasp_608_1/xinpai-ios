//
//  XPUMShareTool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/8.
//

#import "XPUMShareTool.h"


static XPUMShareTool *instance;

@implementation XPUMShareTool



+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

-(void)configUSharePlatforms
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if (APPTYPE == 0) {
            [UMConfigure initWithAppkey:XP_UM_appKey channel:nil];
            [MobClick setAutoPageEnabled:YES];
            [UMSocialGlobal shareInstance].universalLinkDic =@{@(UMSocialPlatformType_WechatSession):XP_wechat_universalLink};
            
            [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:XP_wechat_appKey appSecret:XP_wechat_appSecret redirectURL:XP_wechat_universalLink];
        }else if (APPTYPE == 1){
            [UMConfigure initWithAppkey:ZW_UM_appKey channel:nil];
            [MobClick setAutoPageEnabled:YES];
            [UMSocialGlobal shareInstance].universalLinkDic =@{@(UMSocialPlatformType_WechatSession):ZW_wechat_universalLink};
            
            [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:ZW_wechat_appKey appSecret:ZW_wechat_appSecret redirectURL:ZW_wechat_universalLink];
        }
    });
}
//通用链接时候可以打开
-(BOOL)umHandleUniversalLink:(NSUserActivity *)userActivity{
    if ([[UMSocialManager defaultManager] handleUniversalLink:userActivity options:nil]) {
        return YES;
    }else {
        return NO;
    }
}
- (BOOL)umhandleOpenURL:url sourceApplication:sourceApplication annotation:annotation {
    BOOL result =[[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    return result;
}

- (BOOL)umOpenURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id>*)options{
    BOOL result =[[UMSocialManager defaultManager]  handleOpenURL:url options:options];
    return result;
}

- (void)shareWebUrlWithPlatform:(UMSocialPlatformType)platformType
                          title:(NSString *)title
                          descr:(NSString *)descr
                   webUrlString:(NSString *)urlString
          currentViewController:(id)currentViewController
                     completion:(UMSocialRequestCompletionHandler)completion{
    //创建分享消息对象
    UMSocialMessageObject*messageObject =[UMSocialMessageObject messageObject];
    //创建网页内容对象
    UMShareWebpageObject* shareObject = [UMShareWebpageObject shareObjectWithTitle:title
                                                                            descr:descr
                                                                        thumImage:[UIImage imageNamed:@"AppIcon"]];
    shareObject.webpageUrl = urlString;
    messageObject.shareObject = shareObject;
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:currentViewController completion:completion];
}


@end
