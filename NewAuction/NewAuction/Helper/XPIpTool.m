//
//  XPIpTool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import "XPIpTool.h"
#import <ifaddrs.h>
#import <arpa/inet.h>

@implementation XPIpTool


+ (NSString*)getRouterIP

{
    NSString *adress = @"error";
        struct ifaddrs *interfaces = NULL;
        struct ifaddrs *temp_addr = NULL;
        int success = 0;

        success = getifaddrs(&interfaces);
        if (success == 0) {
            temp_addr = interfaces; //将结构体复制给副本temp_addr
            while (temp_addr != NULL) {
              
                if (temp_addr->ifa_addr->sa_family == AF_INET) {
                    //check if interface is en0 which is the wifi conection on the iphone
                    if ([[NSString stringWithUTF8String:temp_addr ->ifa_name] isEqualToString:@"en0"]) {
                        adress = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];

                    }
                }
                temp_addr = temp_addr -> ifa_next;
            }
        }
        
        //Free memory
        //extern void freeifaddrs(struct ifaddrs *);
        freeifaddrs(interfaces);
        return (adress);
}

@end
