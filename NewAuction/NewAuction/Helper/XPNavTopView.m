//
//  XPNavTopView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "XPNavTopView.h"

@implementation XPNavTopView

- (instancetype)init
{
    if (self = [super init]) {
        self.backgroundColor = [UIColor colorWithRed:255 / 255.0 green:255 / 255.0 blue:255.0 / 255.0 alpha:1.0];
        if (@available(iOS 11.0, *)) {
            XPAppDelegate *app = (XPAppDelegate *)[UIApplication sharedApplication].delegate;
            self.frame = CGRectMake(0, 0, SCREEN_WIDTH, app.window.rootViewController.view.safeAreaInsets.top + 44);
            if (self.frame.size.height < 64) {
                self.frame = CGRectMake(0, 0, SCREEN_WIDTH, 20 + 44);
            }
        } else {
            self.frame = CGRectMake(0, 0, SCREEN_WIDTH, 20 + 44);
        }
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        [self createView];
    }
    return self;
}

- (void)createView
{
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.frame = CGRectMake(10, self.frame.size.height - 44, 44, 44);
    [self.leftButton setImage:[UIImage imageNamed:@"back_gray"] forState:UIControlStateNormal];
    [self addSubview:self.leftButton];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(68, self.frame.size.height - 44, self.frame.size.width - 136, 44)];
    self.titleLabel.textColor = UIColorFromRGB(0x3A3C41);
    self.titleLabel.font = kFontBold(18);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.frame = CGRectMake(self.frame.size.width - 10 - 60, self.frame.size.height - 44, 60, 44);
    self.rightButton.titleLabel.font = kFontRegular(14);
    [self.rightButton setTitleColor:[UIColor colorWithRed:0 / 255.0 green:0 / 255.0 blue:0 / 255.0 alpha:0.8] forState:UIControlStateNormal];
    self.rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self addSubview:self.rightButton];
}

@end
