//
//  XPMoneyFormatTool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/3.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPMoneyFormatTool : NSObject
//最多保留两位有效数字,四舍五入
+ (NSString *)formatMoneyWithNum:(NSNumber *)number;
//最多保留X位有效数字,四舍五入
+ (NSString *)formatMoneyWithNum:(NSNumber *)number maximumFractionDigits:(NSInteger)maximumFractionDigits;

@end

NS_ASSUME_NONNULL_END
