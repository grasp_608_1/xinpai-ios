//
//  XPVideoPlayerViewController.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/18.
//

#import <AVKit/AVKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPVideoPlayerViewController : AVPlayerViewController

@end

NS_ASSUME_NONNULL_END
