//
//  XPWeakTimer.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/26.
//

#import "XPWeakTimer.h"
#import "YYWeakProxy.h"

@implementation XPWeakTimer


+ (instancetype)shareTimer
{
    return [[XPWeakTimer alloc] init];
}

- (void)startTimerWithTime:(NSTimeInterval)time target:(id)aTarget selector:(SEL)aSelector userInfo:(nullable id)userInfo repeats:(BOOL)yesOrNo
{
    __weak typeof(self) weakSelf = self;
    self.isInvalidate = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (weakSelf.isInvalidate) {
            [weakSelf stopTimer];//
            weakSelf.timer = [NSTimer timerWithTimeInterval:time target:[YYWeakProxy proxyWithTarget:aTarget] selector:aSelector userInfo:userInfo repeats:yesOrNo];
            [[NSRunLoop currentRunLoop]addTimer:weakSelf.timer forMode:NSRunLoopCommonModes];
        }
    });
}

- (void)stopTimer
{
    self.isInvalidate = NO;
    if (self.timer) {
        if ([self.timer respondsToSelector:@selector(isValid)]){
            if ([self.timer isValid]){
                [self.timer invalidate];
                self.timer = nil;
            }
        }
    }
}
@end
