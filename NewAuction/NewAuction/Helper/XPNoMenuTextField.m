//
//  XPNoMenuTextField.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/10.
//

#import "XPNoMenuTextField.h"

@implementation XPNoMenuTextField

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    UIMenuController *menu = [UIMenuController sharedMenuController];
    if (menu) {
        [[UIMenuController sharedMenuController] hideMenu];
    }
    return NO;
}

@end
