//
//  XPSelectItemView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/11.
//

#import "XPSelectItemView.h"

@interface XPSelectItemView ()

@property (nonatomic, copy) NSArray *titleArray;

@property (nonatomic, strong) UIImageView *downLineView;

@property (nonatomic, strong) UIView *sepView;

@property (nonatomic, assign)NSInteger lastIndex;

@property (nonatomic, copy) NSArray *buttonArray;

@end

@implementation XPSelectItemView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
        self.backgroundColor = UIColor.whiteColor;
    }
    return self;
}
- (void)setupView {
    self.lastIndex = 1;
}

-(void)configWithtitleArray:(NSArray *)titleArray {
    self.titleArray = titleArray;
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
   
    CGFloat btnW = 80;
    CGFloat btnH = 30;
    CGFloat totalW = self.bounds.size.width;
    //下划线
    UIImageView *downLineView = [[UIImageView alloc] init];
    downLineView.image = [UIImage imageNamed:@"xp_line_blue"];
    [self addSubview:downLineView];
    self.downLineView = downLineView;
    
    NSMutableArray *tempButtonArray = [NSMutableArray array];
    for (int i = 0; i<titleArray.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:titleArray[i] forState:UIControlStateNormal];
        [self addSubview:btn];
        CGFloat margin = (totalW - titleArray.count * btnW)/(titleArray.count + 1);
        btn.frame = CGRectMake(margin + (margin + btnW) * i, 2, btnW, btnH);
        btn.tag = i + 1;
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            CGFloat lineX = btn.frame.origin.x;
            CGFloat lineY = MaxY(btn);
            CGFloat lineW = btn.frame.size.width;
            self.downLineView.frame = CGRectMake(lineX+ 14, lineY, lineW - 28, 8);
            [btn setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontMedium(16);
        }else {
            [btn setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontRegular(14);
        }
        [tempButtonArray addObject:btn];
        
    }
    
    self.buttonArray = tempButtonArray.copy;
    UIView *sepLineView = [[UIView alloc]init];
    sepLineView.backgroundColor = UIColorFromRGB(0xDEDEDE);
    sepLineView.frame = CGRectMake(16, 39, self.mj_w - 32, 1);
    self.sepView = sepLineView;
    sepLineView.hidden = YES;
    [self addSubview:sepLineView];
    
}

- (void)btnClicked:(UIButton *)sender {
    
    if (sender.tag == _lastIndex) {
        return;
    }
    _lastIndex = sender.tag;
    
    for (UIView *btn  in self.subviews) {
        
        if ([btn isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)btn;
            if (sender.tag == button.tag) {
                [button setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
                button.titleLabel.font = kFontMedium(16);
                
                CGFloat lineX = sender.frame.origin.x;
                CGFloat lineY = MaxY(sender) ;
                CGFloat lineW = sender.frame.size.width;
                
                self.downLineView.frame = CGRectMake(lineX+ 14, lineY, lineW - 28, 8);
            }else {
                [button setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
                button.titleLabel.font = kFontRegular(14);
            }
            
        }
    }
    
    if (self.tapBlock) {
        self.tapBlock(sender.tag);
    }
    
}

-(void)setDefaultSeletIndex:(NSUInteger)defaultSeletIndex{
    @try {
        
        UIButton *sender = self.buttonArray[defaultSeletIndex];
//        [self btnClicked:sender];
        
        if (sender.tag == _lastIndex) {
            return;
        }
        _lastIndex = sender.tag;
        
        for (UIView *btn  in self.subviews) {
            
            if ([btn isKindOfClass:[UIButton class]]) {
                UIButton *button = (UIButton *)btn;
                if (sender.tag == button.tag) {
                    [button setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
                    button.titleLabel.font = kFontMedium(16);
                    
                    CGFloat lineX = sender.frame.origin.x;
                    CGFloat lineY = MaxY(sender) ;
                    CGFloat lineW = sender.frame.size.width;
                    
                    self.downLineView.frame = CGRectMake(lineX+ 14, lineY, lineW - 28, 8);
                }else {
                    [button setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
                    button.titleLabel.font = kFontRegular(14);
                }
                
            }
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}

- (void)hiddenLine {
    self.downLineView.hidden = YES;
}
- (void)showDownSepLine {
    self.sepView.hidden = NO;
}
@end
