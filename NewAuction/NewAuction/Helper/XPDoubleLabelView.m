//
//  XPDoubleLabelView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/10/31.
//

#import "XPDoubleLabelView.h"

@interface XPDoubleLabelView ()


@end

@implementation XPDoubleLabelView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.leftLabel = [UILabel new];
    self.leftLabel.font = kFontMedium(12);
    self.leftLabel.textColor = app_font_color_8A;
    self.leftLabel.frame = CGRectMake(12, 0, self.mj_w - 12, self.mj_h);
    [self addSubview:self.leftLabel];
    
    self.righLabel = [UILabel new];
    self.righLabel.font = kFontMedium(12);
    self.righLabel.textColor = app_font_color_8A;
    self.righLabel.frame = CGRectMake(0, 0, self.mj_w - 12, self.mj_h);
    self.righLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.righLabel];
}
-(void)setLeftStr:(NSString *)leftStr {
    _leftStr = leftStr;
    self.leftLabel.text = leftStr;
}

-(void)setRightStr:(NSString *)rightStr {
    _rightStr = rightStr;
    self.righLabel.text = rightStr;
}


@end
