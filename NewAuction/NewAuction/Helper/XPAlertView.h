//
//  XPAlertView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/2.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, XPAlertType) {//弹窗样式枚举
    XPAlertTypeNormal = 0,// 取消 确认
    XPAlertTypeInput = 1,// 单输入框
    XPAlertTypeSure// 单确认按钮
};

typedef void(^LeftBlock)(NSString * _Nullable extra);
typedef void(^RightBlock)(NSString * _Nullable extra);

NS_ASSUME_NONNULL_BEGIN

@interface XPAlertView : UIView

@property (nonatomic, copy) LeftBlock leftBlock;

@property (nonatomic, copy) RightBlock rightBlock;
//点击取消或者确认后 自动消失
@property (nonatomic, assign) BOOL autoDissmiss;

- (void)dismiss;

@property (nonatomic, strong) UITextField *textField;

- (void)configWithTitle:(NSString *)title desCriptionStr:(NSString *)desStr  placeHolderStr:(NSString *)placeholder leftAction:(LeftBlock)leftBlock rightAction:(RightBlock)rightBlock alertType:(XPAlertType)type;

//单确认按钮弹窗
- (void)showAlertWithTitle:(NSString *)titleStr actionBlock:(RightBlock)rightBlock;
//取消 确认 按钮弹窗
- (void)showCancelAlertWithTitle:(NSString *)titleStr leftAction:(LeftBlock)leftBlock rightAction:(RightBlock)rightBlock;

@end

NS_ASSUME_NONNULL_END
