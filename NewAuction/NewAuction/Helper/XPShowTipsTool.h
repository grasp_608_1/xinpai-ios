//
//  XPShowTipsTool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPShowTipsTool : NSObject
+ (instancetype)shareInstance;

/// 吐司持续时间1s
/// - Parameters:
///   - msg: 要展示的文字
///   - targetView: 吐司展示的父view
- (void)showMsg:(NSString *)msg ToView:(UIView *)targetView;
/// 吐司 持续时间2s
/// - Parameters:
///   - msg: 要展示的文字
///   - targetView: 吐司展示的父view
- (void)showLongDuringMsg:(NSString *)msg ToView:(UIView *)targetView;
/// 吐司 持续时间2s
/// - Parameters:
///   - msg: 要展示的文字
///   - targetView: 吐司展示的父view
///     - during  ,自定义消失时间
-(void)showMsg:(NSString *)msg ToView:(UIView *)targetView During:(CGFloat)during;


@end

NS_ASSUME_NONNULL_END
