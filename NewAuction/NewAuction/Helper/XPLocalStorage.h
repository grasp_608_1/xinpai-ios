//
//  XPLocalStorage.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPLocalStorage : NSObject

//写入当前登录用户的本地token
- (void)writeUserToken:(NSString *)userToken;
//读取当前本地用户token
+ (NSString *)readUserToken;

-(void)writeUserCertified:(NSString *)certified;

+(NSString *)readUserCertified;

-(void)writeUserType:(NSString *)userType;

+(NSString *)readUserType;

-(void)writeUserIconUrl:(NSString *)url;

+(NSString *)readUserIconUrl;

-(void)writeUserName:(NSString *)name;

+(NSString *)readUserUserName;

-(void)writeUserNickName:(NSString *)name;

+(NSString *)readUserNickName;

-(void)writeUserPhone:(NSString *)phone;

+(NSString *)readUserUserPhone;
//清除用户记录
+ (void)cleanAllUserInfo;

//身份证号
-(void)writeUserIdNumber:(NSString *)number;

+(NSString *)readUserIdNumber;

//用户的邀请码
-(void)writeUserInventCode:(NSString *)code;
+(NSString *)readUserInventCode;

-(void)writeUserHomeStatus:(NSString *)status;
+(NSString *)readUserHomeStatus;
//用户邀请人的
-(void)writeUserSubInventCode:(NSString *)code;
+(NSString *)readUserSubInventCode;

@end

NS_ASSUME_NONNULL_END
