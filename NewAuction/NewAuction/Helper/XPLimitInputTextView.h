//
//  XPLimitInputTextView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/10.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"

NS_ASSUME_NONNULL_BEGIN

@interface XPLimitInputTextView : UIView
//内容
@property (nonatomic, strong) SZTextView *contentView;
//字数显示
@property (strong, nonatomic) UILabel *numLabel;

@property (nonatomic, assign) NSInteger maxWords;

@property (nonatomic, assign) bool canInputEmoji;

@property (nonatomic, copy) void(^changeBlock)(void);

- (void)configWithMaxCount:(NSInteger)maxCount textViewPlaceholder:(NSString *)placeholder;


@end

NS_ASSUME_NONNULL_END
