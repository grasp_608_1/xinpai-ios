//
//  XPCalendarEventManager.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPCalendarEventManager : NSObject
+ (instancetype)sharedInstance;

/**
 将App事件添加到系统日历提醒事项，实现闹铃提醒的功能

 @param title 事件标题
 @param location 事件位置
 @param startDate 开始时间
 @param endDate 结束时间
 @param allDay 是否全天
 @param offset 开始前多少秒提醒
 */
- (void)createEventCalendarTitle:(NSString *)title location:(NSString *)location startDate:(NSDate *)startDate endDate:(NSDate *)endDate allDay:(BOOL)allDay endTimeoffset:(NSTimeInterval)offset calendarId:(NSString *)cid;

//判断日历中时候已经有个这id
- (BOOL)checkCalendarIsExistWithId:(NSString *)cid;

@end

NS_ASSUME_NONNULL_END
