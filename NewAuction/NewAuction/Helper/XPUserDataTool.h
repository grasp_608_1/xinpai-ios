//
//  XPUserDataTool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/8/28.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPUserDataTool : NSObject

//根据用户角色获取用户角色名称
+(NSString *)getUserRole:(NSInteger)userType;

//根据用户角色获取用户角色图标
+(UIImage *)getUserRoleIcon:(NSInteger)userType;

//根据用户角色获取用户角色北京图标
+(UIImage *)getUserRoleBackgroundImage:(NSInteger)userType;
//个人中心背景图
+(UIImage *)getUserCenterBackgroundImage:(NSInteger)userType;
//设置页背景图
+(UIImage *)getUserSettingBackgroundImage:(NSInteger)userType;
@end

NS_ASSUME_NONNULL_END
