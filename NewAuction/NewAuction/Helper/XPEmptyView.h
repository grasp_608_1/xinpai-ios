//
//  XPEmptyView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPEmptyView : UIView

//宽度 给x 高度要给 x + 27
-(void)configWithImage:(UIImage * __nullable)image Description:(NSString *)description;

@end

NS_ASSUME_NONNULL_END
