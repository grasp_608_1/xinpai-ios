//
//  XPVerifyTool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import "XPVerifyTool.h"
#import "XPUserVerifyIdentyController.h"
#import "XPNavigationController.h"
#import "XPLoginViewController.h"
#import  "XPUserCenterViewController.h"
#import <Photos/PHPhotoLibrary.h>

@implementation XPVerifyTool

static XPVerifyTool *instance;

+ (instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (BOOL)checkLogin {
    NSString *token = [XPLocalStorage readUserToken];
    if (token.length == 0) {
        return NO;
    }else {
        return YES;
    }
}

- (void)userShouldLoginFirst{
    @try {
        UIViewController *currentNav = [XPCommonTool getCurrentViewController];
        XPLoginViewController *loginVC = [[XPLoginViewController alloc] init];
        XPNavigationController *nav = [[XPNavigationController alloc] initWithRootViewController:loginVC];
//        nav.hidesBottomBarWhenPushed = YES;
        if ([currentNav isKindOfClass:[XPUserCenterViewController class]]) {
            loginVC.modelType = 1;
        }else {
            loginVC.modelType = 0;
        }
        
        nav.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [currentNav presentModalViewController:nav animated:YES];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

- (void)userLoginFirstHandler{
    
    UIViewController *currentNav = [XPCommonTool getCurrentViewController];
    XPLoginViewController *loginVC = [[XPLoginViewController alloc] init];
    loginVC.modelType = 1;
    XPNavigationController *nav = [[XPNavigationController alloc] initWithRootViewController:loginVC];
    nav.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [currentNav presentModalViewController:nav animated:YES];
}

//实名认证
- (BOOL)checkUserAuthentication{
    NSString *isCerified = [XPLocalStorage readUserCertified];
    if (isCerified.length == 0 || [isCerified isEqualToString:@"0"]) {
        return NO;
    }else {
        return YES;
    }
}

//是否有邀请码
- (BOOL)checkUserInventCode {
    NSString *isCerified = [XPLocalStorage readUserSubInventCode];
    if (isCerified.length == 0 || [isCerified isEqualToString:@"0"]) {
        return NO;
    }else {
        return YES;
    }
}

- (void)userShouldAuthentication {
    
    @try {
        
        XPBaseViewController *vc = (XPBaseViewController *)[XPCommonTool getCurrentViewController];
        
        [vc.xpAlertView showCancelAlertWithTitle:@"请先进行实名认证" leftAction:^(NSString * _Nullable extra) {
        
        } rightAction:^(NSString * _Nullable extra) {
            
            XPUserVerifyIdentyController *idVC = [[XPUserVerifyIdentyController alloc] init];
            idVC.hidesBottomBarWhenPushed = YES;
            [vc.navigationController pushViewController:idVC animated:YES];
        }];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

//验证码逻辑判断
- (NSString *)verifyCode:(NSString *)codeStr{
    if (codeStr == nil || codeStr.length == 0) {
        return @"请输入验证码";
    }else if(codeStr.length != 6){
        return @"请输入6位验证码";
    }else if (![self validVerifyPredicate:codePredicate TargetString:codeStr]){
        return @"请输入6位数字验证码";
    }else {
        return nil;
    }
}


//正则
-(BOOL)validVerifyPredicate:(NSString *) pString TargetString:(NSString *)targerString
{
   NSPredicate *num = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pString];
   return [num evaluateWithObject:targerString];
}




- (void)phoneAlbumCompleteBlock:(cameraAccessBlock)accessBlock{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (status == PHAuthorizationStatusAuthorized) {
                
                if (accessBlock) {
                    accessBlock();
                }
                
            }else {
                UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示"
                                                                                message:@"相册未授权,请开启相册权限"
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *left = [UIAlertAction actionWithTitle:@"取消"                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                }];
                
                UIAlertAction *right = [UIAlertAction actionWithTitle:@"前往设置"                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    if ([[UIApplication sharedApplication] canOpenURL:url]) {
                        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                    }
                }];
                [alertC addAction:left];
                [alertC addAction:right];
                [[XPCommonTool getCurrentViewController] presentViewController:alertC animated:YES completion:nil];
            }
            
        });
    }];
}


//麦克风权限
- (NSInteger)checkMicrophonePermission {
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    
    NSInteger mStatus = 0;
    
    switch (status) {
        case AVAuthorizationStatusNotDetermined:
            
            // 用户尚未做出选择，可以请求权限
            [self requestMicrophonePermission];
            break;
        case AVAuthorizationStatusRestricted:
        case AVAuthorizationStatusDenied:
            mStatus = 1;
            // 权限被拒绝或受限
            NSLog(@"麦克风权限被拒绝");
            break;
        case AVAuthorizationStatusAuthorized:
           
            // 已授权
            NSLog(@"麦克风权限已授权");
            break;
        default:
            break;
    }
    return status;
}

//麦克风权限获取
- (void)requestMicrophonePermission {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    // 请求麦克风权限
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [audioSession requestRecordPermission:^(BOOL granted) {
        if (granted) {
            NSLog(@"麦克风权限已授予");
            // 执行需要麦克风权限的操作
        } else {
            NSLog(@"麦克风权限被拒绝");
            // 提示用户
            [[XPShowTipsTool shareInstance] showMsg:@"麦克风权限已关闭" ToView:k_keyWindow];
        }
    }];
}
@end
