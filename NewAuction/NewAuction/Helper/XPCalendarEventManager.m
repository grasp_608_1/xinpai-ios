//
//  XPCalendarEventManager.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/15.
//

#import "XPCalendarEventManager.h"
#import <EventKit/EventKit.h>
#import <UIKit/UIKit.h>

static XPCalendarEventManager *shareInstance = nil;

@interface XPCalendarEventManager()

@property (nonatomic, strong) EKEventStore *eventStore;

@end

@implementation XPCalendarEventManager

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        shareInstance = [[XPCalendarEventManager alloc] init];
        shareInstance.eventStore = [[EKEventStore alloc] init];

    });
    return shareInstance;
}

/**
 将App事件添加到系统日历提醒事项，实现闹铃提醒的功能
 
 @param title 事件标题
 @param location 事件位置
 @param startDate 开始时间
 @param endDate 结束时间
 @param allDay 是否全天
 @param offset 开始前多少秒提醒
 */
- (void)createEventCalendarTitle:(NSString *)title location:(NSString *)location startDate:(NSDate *)startDate endDate:(NSDate *)endDate allDay:(BOOL)allDay endTimeoffset:(NSTimeInterval)offset calendarId:(NSString *)cid{
    __weak typeof(self) weakSelf = self;
    
    
    if ([self.eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)])
    {
        [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (error)
                {
                    [strongSelf showAlert:@"添加失败，请稍后重试"];
                }else if (!granted){
                    [strongSelf showAlert:@"不允许使用日历,请在设置中允许此App使用日历"];
                }else{
                    
                    EKEvent *event  = [EKEvent eventWithEventStore:self.eventStore];
                    event.title     = title;
                    event.location = location;
                    event.startDate = startDate;
                    event.endDate   = endDate;
                    event.allDay = allDay;
                    
                    EKAlarm *elarm2 = [EKAlarm alarmWithRelativeOffset:-10];
                    [event addAlarm:elarm2];
                    
                    [event setCalendar:[strongSelf.eventStore defaultCalendarForNewEvents]];
                    
                    NSError *err;
                    [strongSelf.eventStore saveEvent:event span:EKSpanThisEvent error:&err];
                    if (!err) {
                        
                    [strongSelf showAlert:@"已添加到系统日历中"];
                    NSString *iden = event.eventIdentifier;
                          // 保存在沙盒，避免重复添加等其他判断
                    [[NSUserDefaults standardUserDefaults] setObject:iden forKey:cid];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    }else {
                        NSLog(@"error --->%@",err);
                    }
                    
                }
            });
        }];
    }
}


- (BOOL)checkCalendarIsExistWithId:(NSString *)cid {
    NSString *identifier = [[NSUserDefaults standardUserDefaults] objectForKey:cid];
    if (identifier && identifier.length >0) {
        return YES;
    }else {
        return NO;
    }
}

- (void)showAlert:(NSString *)message {

    XPBaseViewController *vc = (XPBaseViewController *)[XPCommonTool getCurrentViewController];
    [vc.xpAlertView showAlertWithTitle:message actionBlock:^(NSString * _Nullable extra) {
        
    }];
    
}

@end

