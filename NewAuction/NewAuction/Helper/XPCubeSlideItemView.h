//
//  XPCubeSlideItemView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ActionBlock)(NSInteger index);

@interface XPCubeSlideItemView : UIScrollView

@property (nonatomic, assign) NSUInteger defaultSeletIndex;

@property (nonatomic, copy) ActionBlock tapBlock;

- (void)configWithtitleArray:(NSArray *)titleArray;
@end

NS_ASSUME_NONNULL_END
