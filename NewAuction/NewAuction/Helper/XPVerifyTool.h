//
//  XPVerifyTool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^cameraAccessBlock)(void);

@interface XPVerifyTool : NSObject


+ (instancetype)shareInstance;

//no 没登录  yes 一登录
- (BOOL)checkLogin;
//去登录
- (void)userShouldLoginFirst;
//登录后发送通知
- (void)userLoginFirstHandler;

//是否实名认证 yes 已经实名认证  未认证
- (BOOL)checkUserAuthentication;
//前往认证
- (void)userShouldAuthentication;
//是否有邀请码
- (BOOL)checkUserInventCode;
/// 验证码逻辑判断 返回非空,则需要提示返回的信息, 返回nil则表示验证通过
/// - Parameter codeStr: 输入的验证码
- (NSString *)verifyCode:(NSString *)codeStr;

//手机相册权限
- (void)phoneAlbumCompleteBlock:(cameraAccessBlock)accessBlock;
//麦克风权限
- (NSInteger)checkMicrophonePermission;

@end

NS_ASSUME_NONNULL_END
