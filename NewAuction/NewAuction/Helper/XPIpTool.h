//
//  XPIpTool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPIpTool : NSObject
+ (NSString *)getRouterIP;
@end

NS_ASSUME_NONNULL_END
