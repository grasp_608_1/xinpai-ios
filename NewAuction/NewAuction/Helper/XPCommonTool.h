//
//  XPCommonTool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPCommonTool : NSObject
//设备唯一标识
+ (NSString *)deviceUUId;
//手机系统
+(NSString *)currentDeviceSystem;

//获取navbar的高度
+ (CGFloat)navBarHeight;

//获取tabbar的高度
+ (CGFloat)tabbarHeight;

//获取导航栏高度
+ (CGFloat)getStatusBarHight;

//字典转json串
+ (NSString *)convertToJsonData:(id)obj;

//前后去空格方法
+ (NSString *)spaceByTrimmingString:(NSString *)string;

//判断字符串是否为空
+ (BOOL)stringIsNull:(NSString *)string;

/// 获取当前控制器
+ (UIViewController *)getCurrentViewController;

/// 获取当前导航
+ (UINavigationController *)currentNavigationController;

/**
 *弹窗提示:一个按钮
 */
+ (UIAlertController *)showAlertWithTitle:(NSString*)title
                   message:(NSString*)message
                  btnTitle:(NSString*)btnTitle
                  callBack:(void(^)(void))callBack
                    withVC:(UIViewController*)VC;

/**
 *弹窗提示:两个按钮
 */
+ (UIAlertController *)showAlertWithTitle:(NSString*)title
                   message:(NSString*)message
               btnTitleArr:(NSArray*)btnTitleArr
                  leftBack:(void(^)(void))leftBack
                 rightBack:(void(^)(void))rigthBack
                    withVC:(UIViewController*)VC;

//颜色渐变,从左到右 (注意颜色需要桥接) (__bridge id)XXColor.CGColor
+ (CAGradientLayer *)setGradualChangingFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint Location:(NSArray *)locations :(UIView *)view colors:(NSArray *)colorArray;

/// 设置圆角位置
/// @param view 设置圆角的view
/// @param type 设置圆角的位置
/// @param rad 圆角大小
+ (void)cornerRadius:(UIView*)view andType:(NSInteger)type radius:(CGFloat)rad;

+ (NSData *)zipImage:(UIImage *)image;

+ (NSData *)zipImageWithMaxSize:(NSInteger)maxSizeInBytes image:(UIImage *)image;

//将view转成成image
+ (UIImage*)viewToImage:(UIView *)view;
//字符串是否为空
+ (BOOL) isEmpty:(NSString *) str;
//正则
+(BOOL)validVerifyPredicate:(NSString *) pString TargetString:(NSString *)targerString;
//十六进制颜色转换
+ (UIColor *)colorWithHex:(NSString *)hexColor;
//计算富文本高度
+ (CGFloat)calculateHeightForAttributedString:(NSAttributedString *)attributedString withWidth:(CGFloat)width;
+ (BOOL)areDictionariesValuesEqual:(NSDictionary *)dict1 secondDict:(NSDictionary *)dict2;
@end

NS_ASSUME_NONNULL_END
