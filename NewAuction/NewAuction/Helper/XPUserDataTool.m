//
//  XPUserDataTool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/8/28.
//

#import "XPUserDataTool.h"

@implementation XPUserDataTool

+(NSString *)getUserRole:(NSInteger)userType {
    
    NSString *userRole;
    
    switch (userType) {
        case 1:
            userRole = @"普通用户";
            break;
        case 2:
            userRole = @"VIP用户";
            break;
        case 3:
            userRole = @"团购团长";
            break;
            
        default:
            break;
    }
    return userRole;
}

+(UIImage *)getUserRoleIcon:(NSInteger)userType {
    
    UIImage *userRoleIcon;
    
    switch (userType) {
        case 1:
            userRoleIcon = [UIImage imageNamed:@"user_role_normal"];
            break;
        case 2:
            userRoleIcon = [UIImage imageNamed:@"user_role_vip"];
            break;
        case 3:
            userRoleIcon = [UIImage imageNamed:@"user_role_commander"];
            break;
            
        default:
            userRoleIcon = [UIImage imageNamed:@""];
            break;
    }
    return userRoleIcon;
}

+(UIImage *)getUserRoleBackgroundImage:(NSInteger)userType {
    
    UIImage *userRoleIcon;
    
    switch (userType) {
        case 1:
            userRoleIcon = [UIImage imageNamed:@"user_role_normal_updatebg"];
            break;
        case 2:
            userRoleIcon = [UIImage imageNamed:@"user_role_vip_updatebg"];
            break;
        case 3:
            userRoleIcon = [UIImage imageNamed:@"user_role_commander_updatebg"];
            break;
            
        default:
            userRoleIcon = [UIImage imageNamed:@""];
            break;
    }
    return userRoleIcon;
}

+(UIImage *)getUserCenterBackgroundImage:(NSInteger)userType {
    
    UIImage *userRoleIcon;
    
    
    switch (userType) {
        case 1:
            userRoleIcon = [UIImage imageNamed:@"user_role_normal_usercenterbg"];
            break;
        case 2:
            userRoleIcon = [UIImage imageNamed:@"user_role_vip_usercenterbg"];
            break;
        case 3:
            userRoleIcon = [UIImage imageNamed:@"user_role_commander_usercenterbg"];
            break;
            
        default:
            userRoleIcon = [UIImage imageNamed:@""];
            break;
    }
    return userRoleIcon;
}

+(UIImage *)getUserSettingBackgroundImage:(NSInteger)userType {
    
    UIImage *userRoleIcon;
    
    
    switch (userType) {
        case 1:
            userRoleIcon = [UIImage imageNamed:@"user_role_normal_settingbg"];
            break;
        case 2:
            userRoleIcon = [UIImage imageNamed:@"user_role_vip_settingbg"];
            break;
        case 3:
            userRoleIcon = [UIImage imageNamed:@"user_role_commander_settingbg"];
            break;
            
        default:
            userRoleIcon = [UIImage imageNamed:@""];
            break;
    }
    return userRoleIcon;
}


@end
