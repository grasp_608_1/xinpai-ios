//
//  XPUMShareTool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/7/8.
//

#import <Foundation/Foundation.h>
#import <UMCommon/UMCommon.h>
#import "UMCommon/MobClick.h"
#import<UShareUI/UShareUI.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPUMShareTool : NSObject

+ (instancetype)shareInstance;

//初始化友盟平台
- (void)configUSharePlatforms;

-(BOOL)umHandleUniversalLink:(NSUserActivity *)userActivity;

- (BOOL)umhandleOpenURL:url sourceApplication:sourceApplication annotation:annotation;

- (BOOL)umOpenURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id>*)options;
/// 分享网页
/// - Parameters:
///   - platformType: 分享平台
///   - title: 标题
///   - descr: 分享内容
///   - urlString: 分享网页的地址
///   - currentViewController: 当前控制器
///   - completion: 分享完成回调
- (void)shareWebUrlWithPlatform:(UMSocialPlatformType)platformType
                          title:(NSString *)title
                          descr:(NSString *)descr
                   webUrlString:(NSString *)urlString
          currentViewController:(id)currentViewController
                     completion:(UMSocialRequestCompletionHandler)completion;

@end

NS_ASSUME_NONNULL_END
