//
//  XPNavTopView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPNavTopView : UIView
//左侧按钮
@property (strong, nonatomic) UIButton *leftButton;
//标题栏
@property (strong, nonatomic) UILabel *titleLabel;
//右侧按键
@property (strong, nonatomic) UIButton *rightButton;


@end

NS_ASSUME_NONNULL_END
