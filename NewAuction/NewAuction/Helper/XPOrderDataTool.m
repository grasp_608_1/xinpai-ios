//
//  XPOrderDataTool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/29.
//

#import "XPOrderDataTool.h"

@implementation XPOrderDataTool


+ (NSString *)getStatusTitle:(NSString *)statusStr{
    
    NSInteger status = [statusStr integerValue];
    
    NSString *targetStr = @"";
    switch (status) {
        case 0:
            targetStr = @"待支付";
            break;
        case 1:
            targetStr = @"已支付";
            break;
        case 2:
            targetStr = @"支付失败";
            break;
        case 3:
            targetStr = @"已发货";
            break;
        case 4:
            targetStr = @"已收货";
            break;
        case 5:
            targetStr = @"待支付确认";
            break;
        case 6:
            targetStr = @"已确认";
            break;
        case 7:
            targetStr = @"待提货";
            break;
        case 8:
            targetStr = @"提货成功";
            break;
        case 9:
            targetStr = @"提货失败";
            break;
        default:
            targetStr = @"未知状态";
            break;
    }
    return targetStr;
}

@end
