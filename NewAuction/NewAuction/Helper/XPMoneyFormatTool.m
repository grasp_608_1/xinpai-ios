//
//  XPMoneyFormatTool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/7/3.
//

#import "XPMoneyFormatTool.h"

@implementation XPMoneyFormatTool


+ (NSString *)formatMoneyWithNum:(NSNumber *)number maximumFractionDigits:(NSInteger)maximumFractionDigits{
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterNoStyle;
    formatter.minimumFractionDigits = 0;
    formatter.maximumFractionDigits = maximumFractionDigits;
    formatter.roundingMode = NSNumberFormatterRoundHalfUp;//四舍五入
    NSString *formattedString = [formatter stringFromNumber:number];
    return formattedString;
}

+ (NSString *)formatMoneyWithNum:(NSNumber *)number{
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterNoStyle;
    formatter.minimumFractionDigits = 0;
    formatter.maximumFractionDigits = 2;
    formatter.roundingMode = NSNumberFormatterRoundHalfUp;//四舍五入
    NSString *formattedString = [formatter stringFromNumber:number];
    return formattedString;
}

+ (NSMutableAttributedString *)moneyFormat:(NSNumber *)count attrFont:(UIFont *)font{
    NSString *priceStr = [NSString stringWithFormat:@"￥%@",[self formatMoneyWithNum:count]];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:priceStr];
    [attr addAttributes:@{NSFontAttributeName:font} range:NSMakeRange(priceStr.length -1, 1)];
    return attr;
}

@end
