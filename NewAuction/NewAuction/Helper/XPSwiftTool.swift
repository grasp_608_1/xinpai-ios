//
//  XPSwiftTool.swift
//  NewAuction
//
//  Created by Grasp_L on 2024/11/7.
//

import Foundation
@objcMembers class XPSwiftTool:NSObject {
    static let shared = XPSwiftTool()
    
    // 检查字符串中是否包含 Emoji
        func containsEmoji(_ string: String) -> Bool {
            for scalar in string.unicodeScalars {
                if (scalar.value >= 0x1F600 && scalar.value <= 0x1F64F) ||//（面部表情符号😃）
                    (scalar.value >= 0x1F300 && scalar.value <= 0x1F5FF) ||
                    (scalar.value >= 0x1F680 && scalar.value <= 0x1F6FF) ||
                    (scalar.value >= 0x1F700 && scalar.value <= 0x1F77F) ||
                    (scalar.value >= 0x1F780 && scalar.value <= 0x1F7FF) ||
                    (scalar.value >= 0x1F800 && scalar.value <= 0x1F8FF) ||
                    (scalar.value >= 0x1F900 && scalar.value <= 0x1F9FF) ||
                    (scalar.value >= 0x1FA00 && scalar.value <= 0x1FA6F) ||
                    (scalar.value >= 0x1FA70 && scalar.value <= 0x1FAFF) {
                    return true
                }
                // 额外的 Unicode 区间，可以补充一些特定 Emoji 范围
                if ((scalar.value >= 0x2600 && scalar.value <= 0x26FF) || // Miscellaneous Symbols: ☀️, ⚡ 等
                    (scalar.value >= 0x1F200 && scalar.value <= 0x1F251) ||//🈲系列
                     (scalar.value >= 0x1F1E6 && scalar.value <= 0x1F1FF)//国旗 
                   )
                    {  
                    return true
                }
            }
            return false
        }
}
