//
//  UIViewController+iOS11.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "UIViewController+iOS11.h"
#import "NSObject+Swizzling.h"
#import <objc/runtime.h>

@implementation UIViewController (iOS11)

static char navbarViewKey;
static char safeTopKey;
static char safeBottomKey;

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[self class] swizzleSelector:@selector(viewDidLoad)
                 withSwizzledSelector:@selector(xp_viewDidLoad)];
        [[self class] swizzleSelector:@selector(viewWillAppear:)
                 withSwizzledSelector:@selector(xp_viewWillAppear:)];
    });
    
}

- (float)safeTop
{
    return [objc_getAssociatedObject(self, &safeTopKey) floatValue];
}

- (void)setSafeTop:(float)safeTop{
    objc_setAssociatedObject(self, &safeTopKey, [NSString stringWithFormat:@"%f",safeTop], OBJC_ASSOCIATION_COPY);
}

- (float)safeBottom
{
    return [objc_getAssociatedObject(self, &safeBottomKey) floatValue];
}

- (void)setSafeBottom:(float)safeBottom{
    objc_setAssociatedObject(self, &safeBottomKey, [NSString stringWithFormat:@"%f",safeBottom], OBJC_ASSOCIATION_COPY);
}

- (XPNavTopView *)navBarView
{
    return objc_getAssociatedObject(self, &navbarViewKey);
}

-(void)setNavBarView:(XPNavTopView *)navBarView
{
    objc_setAssociatedObject(self, &navbarViewKey, navBarView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)xp_viewDidLoad
{
    if (@available(iOS 11.0, *)) {
        self.safeBottom = self.tabBarController.view.safeAreaInsets.bottom;
        self.safeTop = self.tabBarController.view.safeAreaInsets.top;
        if (self.safeTop < 20) {
            self.safeTop = 20;
        }
    } else {
        self.safeBottom = 0;
        self.safeTop = 20;
    }
    [self setupNavBarView];
    [self xp_viewDidLoad];
    
}
- (void)xp_viewWillAppear:(BOOL)animated
{
    [self xp_viewWillAppear:animated];
    [self.view bringSubviewToFront:self.navBarView];
    
    if (self.navBarView) {
        if (@available(iOS 13, *)) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDarkContent];
        } else {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        }
    } else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
}

-(void)setupNavBarView
{
    
}

@end
