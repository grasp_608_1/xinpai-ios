//
//  UIViewController+Rotation.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/18.
//

#import "UIViewController+Rotation.h"
#import <objc/runtime.h>
#import "XPAppDelegate.h"

#define AtAppDelegate ((XPAppDelegate *)[UIApplication sharedApplication].delegate)

//定义常量 必须是C语言字符串
static char *FullScreenAllowRotationKey = "FullScreenAllowRotationKey";

@implementation UIViewController (Rotation)

void swizzleMethod(Class class, SEL originalSelector, SEL swizzledSelector){
    // the method might not exist in the class, but in its superclass
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    
    // class_addMethod will fail if original method already exists
    BOOL didAddMethod = class_addMethod(class, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod));
    
    // the method doesn’t exist and we just added one
    if (didAddMethod) {
        class_replaceMethod(class, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
    }else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}
/**
 是否允许横屏
 
 @return bool YES 允许 NO 不允许
 */
- (BOOL)shouldAutorotate1{
    BOOL flag = objc_getAssociatedObject(self, FullScreenAllowRotationKey);
    return flag;
}


/**
 屏幕方向
 
 @return 屏幕方向
 */
- (UIInterfaceOrientationMask)supportedInterfaceOrientations1{
    //get方法通过key获取对象
    BOOL flag = objc_getAssociatedObject(self, FullScreenAllowRotationKey);
    if (flag) {
        return UIInterfaceOrientationMaskLandscapeRight;
    }else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation1{
    BOOL flag = objc_getAssociatedObject(self, FullScreenAllowRotationKey);
    if (flag) {
        return UIInterfaceOrientationLandscapeRight;
    }else{
        return UIInterfaceOrientationPortrait;
    }
}
/**
 强制横屏方法
 @param fullscreen 屏幕方向
 */
- (void)setNewOrientation:(BOOL)fullscreen{
    AtAppDelegate.allowRotation = fullscreen;
     objc_setAssociatedObject(self, FullScreenAllowRotationKey,[NSNumber numberWithBool:fullscreen], OBJC_ASSOCIATION_ASSIGN);
    
    swizzleMethod([self class], @selector(shouldAutorotate), @selector(shouldAutorotate1));
    swizzleMethod([self class], @selector(supportedInterfaceOrientations), @selector(supportedInterfaceOrientations1));
    swizzleMethod([self class], @selector(preferredInterfaceOrientationForPresentation), @selector(preferredInterfaceOrientationForPresentation1));
    
    @autoreleasepool {
        if (fullscreen) {
            NSNumber *resetOrientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationUnknown];
            [[UIDevice currentDevice] setValue:resetOrientationTarget forKey:@"orientation"];
            NSNumber *orientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
            [[UIDevice currentDevice] setValue:orientationTarget forKey:@"orientation"];
        }else{
            NSNumber *resetOrientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationUnknown];
            [[UIDevice currentDevice] setValue:resetOrientationTarget forKey:@"orientation"];
            NSNumber *orientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
            [[UIDevice currentDevice] setValue:orientationTarget forKey:@"orientation"];
        }
    }
}

@end
