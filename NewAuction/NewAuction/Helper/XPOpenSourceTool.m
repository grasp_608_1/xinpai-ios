//
//  XPOpenSourceTool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/22.
//

#import "XPOpenSourceTool.h"
#import "XPVideoPlayerViewController.h"

@implementation XPOpenSourceTool

static XPOpenSourceTool *instance;

+ (instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)openVideoControllerWithUrl:(NSString *)urlStr{
    
    NSURL *url = [NSURL URLWithString:urlStr];
    
    AVPlayer *avplayer = [[AVPlayer alloc] initWithURL:url];
    
    XPVideoPlayerViewController *vc = [[XPVideoPlayerViewController alloc] init];
    vc.player = avplayer;
    [vc.player play];
    [[XPCommonTool getCurrentViewController] presentViewController:vc animated:YES completion:nil];
    
}

@end
