//
//  XPEmptyView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/12.
//

#import "XPEmptyView.h"

@interface XPEmptyView ()
@property (nonatomic, strong) UIImageView *banner;
@property (nonatomic, strong) UILabel *label;

@end

@implementation XPEmptyView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.banner = [[UIImageView alloc] init];
    self.banner.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.width);
    [self addSubview:self.banner];
    
    self.label = [[UILabel alloc] init];
    self.label.font = kFontRegular(14);
    self.label.textColor = UIColorFromRGB(0x8A8A8A);
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.frame = CGRectMake(0, self.bounds.size.height - 17, self.bounds.size.width, 17);
    [self addSubview:self.label];
    
}

-(void)configWithImage:(UIImage * __nullable)image Description:(NSString *)description{
    if (image == nil) {
        self.label.frame = self.bounds;
        self.label.text = description;
    }else {
        self.label.text = description;
        self.banner.image = image;
    }
}

@end
