//
//  UIImage+QRCode.h
//  NewAuction
//
//  Created by Grasp_L on 2025/2/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (QRCode)

+ (UIImage *)generateQRCodeFromString:(NSString *)string;

@end

NS_ASSUME_NONNULL_END
