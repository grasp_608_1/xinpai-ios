//
//  XPWeakTimer.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPWeakTimer : NSObject

@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, assign) BOOL isInvalidate;

+ (instancetype)shareTimer;

- (void)startTimerWithTime:(NSTimeInterval)time target:(id)aTarget selector:(SEL)aSelector userInfo:(nullable id)userInfo repeats:(BOOL)yesOrNo;

- (void)stopTimer;
@end

NS_ASSUME_NONNULL_END
