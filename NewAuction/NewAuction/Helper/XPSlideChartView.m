//
//  XPSlideChartView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/12/17.
//

#import "XPSlideChartView.h"

@interface XPSlideChartView ()

@property (nonatomic, copy) NSArray *titleArray;
//麦穗
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UIImageView *rightImageView;

@property (nonatomic, assign)NSInteger lastIndex;

@property (nonatomic, copy) NSArray *buttonArray;

@end

@implementation XPSlideChartView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.showsHorizontalScrollIndicator = NO;
    self.bounces = YES;
    self.lastIndex = 1;
}

-(void)configWithtitleArray:(NSArray *)titleArray {
    self.titleArray = titleArray;
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    UIImageView *leftImageView = [[UIImageView alloc] init];
    leftImageView.image = [UIImage imageNamed:@"xp_compact_left"];
    self.leftImageView = leftImageView;
    [self addSubview:leftImageView];
   
   UIImageView *rightImageView = [[UIImageView alloc] init];
   rightImageView.image = [UIImage imageNamed:@"xp_compact_right"];
   self.rightImageView = rightImageView;
   [self addSubview:rightImageView];
    
    CGFloat maxRight = 12;
    CGFloat margin = 15;
    CGFloat btnH = 30;
    NSMutableArray *tempButtonArray = [NSMutableArray array];
    
    for (int i = 0; i<titleArray.count; i++) {
        NSString *titleStr = titleArray[i];
        CGFloat btnW = [titleStr sizeWithAttributes:@{NSFontAttributeName:kFontMedium(16)}].width;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:titleStr forState:UIControlStateNormal];
        [self addSubview:btn];
        
        btn.frame = CGRectMake(margin + maxRight , 4, btnW, btnH);
        btn.tag = i + 1;
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            CGFloat lineY = MaxY(btn);
            [self showCompactWithButton:btn];
            [btn setTitleColor:UIColorFromRGB(0xFFBE74) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontMedium(16);
        }else {
            [btn setTitleColor:UIColorFromRGB(0x9D6936) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontRegular(14);
        }
        
        [tempButtonArray addObject:btn];
        
//        if (i == self.titleArray.count - 1) {
            maxRight = MaxX(btn) ;
//        }
    }
    self.buttonArray = tempButtonArray.copy;
    maxRight = maxRight + margin;
    //
    self.contentSize = CGSizeMake(maxRight<self.bounds.size.width ?self.bounds.size.width:maxRight, self.bounds.size.height);
    
    
}

- (void)showCompactWithButton:(UIButton *)btn {
    CGFloat btnW = btn.mj_w;
    CGFloat lineY = MaxY(btn);

    self.leftImageView.frame = CGRectMake(btn.mj_x - 15, 5, 20, btn.mj_h);
    self.rightImageView.frame = CGRectMake(MaxX(btn) - 5, 5, 20, btn.mj_h);
}

- (void)btnClicked:(UIButton *)sender {
    self.bounces = NO;
    
    if (sender.tag == _lastIndex) {
        return;
    }
    _lastIndex = sender.tag;
    
    for (UIView *btn  in self.subviews) {
        
        if ([btn isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)btn;
            if (sender.tag == button.tag) {
                [button setTitleColor:UIColorFromRGB(0xFFBE74) forState:UIControlStateNormal];
                button.titleLabel.font = kFontMedium(16);
                CGFloat lineY = MaxY(sender) ;
                [self showCompactWithButton:sender];
            }else {
                [button setTitleColor:UIColorFromRGB(0x9D6936) forState:UIControlStateNormal];
                button.titleLabel.font = kFontRegular(14);
            }
            
        }
    }
    
    if (self.tapBlock) {
        self.tapBlock(sender.tag);
    }
    
}


-(void)setDefaultSeletIndex:(NSUInteger)defaultSeletIndex{
    @try {
        
        if (self.buttonArray.count == 0) {
            return;
        }
        UIButton *sender = self.buttonArray[defaultSeletIndex];
        _lastIndex = sender.tag;
        
        for (UIView *btn  in self.subviews) {
            
            if ([btn isKindOfClass:[UIButton class]]) {
                UIButton *button = (UIButton *)btn;
                if (sender.tag == button.tag) {
                    [button setTitleColor:UIColorFromRGB(0xFFBE74) forState:UIControlStateNormal];
                    button.titleLabel.font = kFontMedium(16);
                    
                    CGFloat lineY = MaxY(sender) ;
                    [self showCompactWithButton:sender];
                    [self scrollToCenterByButton:sender animation:YES];
                    
                    
                }else {
                    [button setTitleColor:UIColorFromRGB(0x9D6936) forState:UIControlStateNormal];
                    button.titleLabel.font = kFontRegular(14);
                }
            }
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}

-(void)scrollToCenterByButton:(UIButton *)sender animation:(BOOL)animation{
    
        
        CGFloat offsetX = sender.mj_x - self.mj_w/2 + sender.mj_w/2.;

        if (sender.mj_x<self.mj_w*2/3.) {
            if (animation) {
                [UIView animateWithDuration:0.2 animations:^{
                    
                    [self setContentOffset:CGPointMake(0, 0)];
                    
                }];
            }else{
                [self setContentOffset:CGPointMake(0, 0)];
            }
            
            
        }else if (self.contentSize.width - sender.mj_x - self.mj_w*2/3 < 0){
            if (animation) {
                [UIView animateWithDuration:0.2 animations:^{
                    
                    [self setContentOffset:CGPointMake(self.contentSize.width - self.mj_w, 0)];
                }];
            }else{
                [self setContentOffset:CGPointMake(self.contentSize.width - self.mj_w, 0)];
            }
        }else{
            if (animation) {
                [UIView animateWithDuration:0.2 animations:^{
                    
                    [self setContentOffset:CGPointMake(offsetX, 0)];
                }];
            }else{
                [self setContentOffset:CGPointMake(offsetX, 0)];
            }
        }
}

@end
