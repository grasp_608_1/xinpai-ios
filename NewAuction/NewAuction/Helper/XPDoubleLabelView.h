//
//  XPDoubleLabelView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/10/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPDoubleLabelView : UIView

@property (nonatomic, copy) NSString *leftStr;

@property (nonatomic, copy) NSString *rightStr;

@property (nonatomic, strong) UILabel *leftLabel;

@property (nonatomic, strong) UILabel *righLabel;

@end

NS_ASSUME_NONNULL_END
