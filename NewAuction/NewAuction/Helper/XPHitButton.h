//
//  XPHitButton.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPHitButton : UIButton
//扩大点击区域直径为hitRect
@property (nonatomic, assign) CGFloat hitRect;
@end

NS_ASSUME_NONNULL_END
