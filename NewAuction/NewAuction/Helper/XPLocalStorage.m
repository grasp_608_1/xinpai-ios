//
//  XPLocalStorage.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "XPLocalStorage.h"

#define xp_user_token @"login_user_token"
#define xp_user_certified @"login_user_certified"
#define xp_user_type @"login_user_userType"
#define xp_user_icon_url @"login_user_icon_url"
#define xp_user_name @"login_user_userName"
#define xp_user_nickname @"xp_user_nickname"
#define xp_user_phone @"login_user_userPhone"
#define xp_user_idnumber @"login_user_useridnumber"
#define xp_user_inventCode @"login_user_inventCode"
#define xp_user_subInventCode @"login_user_subInventCode"
#define xp_user_homeStatus @"login_user_homeStatus"


@implementation XPLocalStorage

-(void)writeUserToken:(NSString *)userToken {
    [[NSUserDefaults standardUserDefaults] setObject:TO_STR(userToken) forKey:xp_user_token];
}

+(NSString *)readUserToken {
    return [[NSUserDefaults standardUserDefaults] objectForKey:xp_user_token];
}

-(void)writeUserCertified:(NSString *)certified {
    [[NSUserDefaults standardUserDefaults] setObject:TO_STR(certified) forKey:xp_user_certified];
}

+(NSString *)readUserCertified {
    return [[NSUserDefaults standardUserDefaults] objectForKey:xp_user_certified];
}

-(void)writeUserType:(NSString *)userType {
    [[NSUserDefaults standardUserDefaults] setObject:TO_STR(userType) forKey:xp_user_type];
}

+(NSString *)readUserType {
    return [[NSUserDefaults standardUserDefaults] objectForKey:xp_user_type];
}
-(void)writeUserIconUrl:(NSString *)url {
    [[NSUserDefaults standardUserDefaults] setObject:TO_STR(url) forKey:xp_user_icon_url];
}

+(NSString *)readUserIconUrl {
    return [[NSUserDefaults standardUserDefaults] objectForKey:xp_user_icon_url];
}

-(void)writeUserName:(NSString *)name {
    [[NSUserDefaults standardUserDefaults] setObject:TO_STR(name) forKey:xp_user_name];
}

+(NSString *)readUserUserName {
    return [[NSUserDefaults standardUserDefaults] objectForKey:xp_user_name];
}

-(void)writeUserNickName:(NSString *)name {
    [[NSUserDefaults standardUserDefaults] setObject:TO_STR(name) forKey:xp_user_nickname];
}

+(NSString *)readUserNickName {
    return [[NSUserDefaults standardUserDefaults] objectForKey:xp_user_nickname];
}


-(void)writeUserPhone:(NSString *)phone {
    [[NSUserDefaults standardUserDefaults] setObject:TO_STR(phone) forKey:xp_user_phone];
}

+(NSString *)readUserUserPhone {
    return [[NSUserDefaults standardUserDefaults] objectForKey:xp_user_phone];
}

-(void)writeUserIdNumber:(NSString *)number {
    [[NSUserDefaults standardUserDefaults] setObject:TO_STR(number) forKey:xp_user_idnumber];
}

+(NSString *)readUserIdNumber {
    return [[NSUserDefaults standardUserDefaults] objectForKey:xp_user_idnumber];
}

-(void)writeUserInventCode:(NSString *)code {
    [[NSUserDefaults standardUserDefaults] setObject:TO_STR(code) forKey:xp_user_inventCode];
}

+(NSString *)readUserInventCode {
    return [[NSUserDefaults standardUserDefaults] objectForKey:xp_user_inventCode];
}

-(void)writeUserSubInventCode:(NSString *)code {
    [[NSUserDefaults standardUserDefaults] setObject:TO_STR(code) forKey:xp_user_subInventCode];
}

+(NSString *)readUserSubInventCode {
    return [[NSUserDefaults standardUserDefaults] objectForKey:xp_user_subInventCode];
}

//邀请码
-(void)writeUserHomeStatus:(NSString *)status {
    [[NSUserDefaults standardUserDefaults] setObject:TO_STR(status) forKey:xp_user_homeStatus];
}
+(NSString *)readUserHomeStatus{
    return [[NSUserDefaults standardUserDefaults] objectForKey:xp_user_homeStatus];
}

+ (void)cleanAllUserInfo {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:xp_user_token];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:xp_user_certified];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:xp_user_type];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:xp_user_icon_url];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:xp_user_name];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:xp_user_nickname];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:xp_user_phone];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:xp_user_idnumber];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:xp_user_inventCode];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:xp_user_homeStatus];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:xp_user_subInventCode];
}
@end
