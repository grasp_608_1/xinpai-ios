//
//  XPSlideItemView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import "XPSlideItemView.h"


@interface XPSlideItemView ()

@property (nonatomic, copy) NSArray *titleArray;

@property (nonatomic, strong) UIImageView *downLineView;

@property (nonatomic, assign)NSInteger lastIndex;

@property (nonatomic, copy) NSArray *buttonArray;

@end

@implementation XPSlideItemView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.showsHorizontalScrollIndicator = NO;
    self.bounces = YES;
    self.lastIndex = 1;
}

-(void)configWithtitleArray:(NSArray *)titleArray {
    self.titleArray = titleArray;
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
   
    //下划线
    UIImageView *downLineView = [[UIImageView alloc] init];
    downLineView.image = [UIImage imageNamed:@"xp_line_blue"];
    [self addSubview:downLineView];
    self.downLineView = downLineView;
    
    CGFloat maxRight = 0.0;
    CGFloat margin = 15;
    CGFloat btnH = 30;
    NSMutableArray *tempButtonArray = [NSMutableArray array];
    
    for (int i = 0; i<titleArray.count; i++) {
        NSString *titleStr = titleArray[i];
        CGFloat btnW = [titleStr sizeWithAttributes:@{NSFontAttributeName:kFontMedium(16)}].width;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:titleStr forState:UIControlStateNormal];
        [self addSubview:btn];
        
        btn.frame = CGRectMake(margin + maxRight , 4, btnW, btnH);
        btn.tag = i + 1;
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            CGFloat lineX = btn.frame.origin.x;
            CGFloat lineY = MaxY(btn);
            CGFloat lineW = btn.frame.size.width;
            self.downLineView.frame = CGRectMake(btn.center.x - btnW/2, lineY, btn.mj_w, 8);
            [btn setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontMedium(16);
        }else {
            [btn setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
            btn.titleLabel.font = kFontRegular(14);
        }
        
        [tempButtonArray addObject:btn];
        
//        if (i == self.titleArray.count - 1) {
            maxRight = MaxX(btn) ;
//        }
    }
    self.buttonArray = tempButtonArray.copy;
    maxRight = maxRight + margin;
    //
    self.contentSize = CGSizeMake(maxRight<self.bounds.size.width ?self.bounds.size.width:maxRight, self.bounds.size.height);
    
    
}

- (void)btnClicked:(UIButton *)sender {
    self.bounces = NO;
    
    if (sender.tag == _lastIndex) {
        return;
    }
    _lastIndex = sender.tag;
    
    for (UIView *btn  in self.subviews) {
        
        if ([btn isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)btn;
            if (sender.tag == button.tag) {
                [button setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
                button.titleLabel.font = kFontMedium(16);
                CGFloat lineY = MaxY(sender) ;
                self.downLineView.frame = CGRectMake(btn.center.x - btn.mj_w/2, lineY, btn.mj_w, 8);
            }else {
                [button setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
                button.titleLabel.font = kFontRegular(14);
            }
            
        }
    }
    
    if (self.tapBlock) {
        self.tapBlock(sender.tag);
    }
    
}

-(void)setHidenDownLine:(BOOL)hidenDownLine {
    
    self.downLineView.hidden = hidenDownLine;
}

-(void)setDefaultSeletIndex:(NSUInteger)defaultSeletIndex{
    @try {
        
        if (self.buttonArray.count == 0) {
            return;
        }
        UIButton *sender = self.buttonArray[defaultSeletIndex];
        _lastIndex = sender.tag;
        
        for (UIView *btn  in self.subviews) {
            
            if ([btn isKindOfClass:[UIButton class]]) {
                UIButton *button = (UIButton *)btn;
                if (sender.tag == button.tag) {
                    [button setTitleColor:UIColorFromRGB(0x3A3C41) forState:UIControlStateNormal];
                    button.titleLabel.font = kFontMedium(16);
                    
                    CGFloat lineY = MaxY(sender) ;
                    self.downLineView.frame = CGRectMake(btn.center.x - btn.mj_w/2, lineY, btn.mj_w, 8);
                    [self scrollToCenterByButton:sender animation:YES];
                    
                    
                }else {
                    [button setTitleColor:UIColorFromRGB(0x8A8A8A) forState:UIControlStateNormal];
                    button.titleLabel.font = kFontRegular(14);
                }
            }
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}

-(void)scrollToCenterByButton:(UIButton *)sender animation:(BOOL)animation{
    
        
        CGFloat offsetX = sender.mj_x - self.mj_w/2 + sender.mj_w/2.;

        if (sender.mj_x<self.mj_w*2/3.) {
            if (animation) {
                [UIView animateWithDuration:0.2 animations:^{
                    
                    [self setContentOffset:CGPointMake(0, 0)];
                    
                }];
            }else{
                [self setContentOffset:CGPointMake(0, 0)];
            }
            
            
        }else if (self.contentSize.width - sender.mj_x - self.mj_w*2/3 < 0){
            if (animation) {
                [UIView animateWithDuration:0.2 animations:^{
                    
                    [self setContentOffset:CGPointMake(self.contentSize.width - self.mj_w, 0)];
                }];
            }else{
                [self setContentOffset:CGPointMake(self.contentSize.width - self.mj_w, 0)];
            }
        }else{
            if (animation) {
                [UIView animateWithDuration:0.2 animations:^{
                    
                    [self setContentOffset:CGPointMake(offsetX, 0)];
                }];
            }else{
                [self setContentOffset:CGPointMake(offsetX, 0)];
            }
        }
}
@end
