//
//  XPMd5Tool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/6/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPMd5Tool : NSObject
+(NSString *) md5: (NSString *) inPutText ;
@end

NS_ASSUME_NONNULL_END
