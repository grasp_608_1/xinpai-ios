//
//  XPHitButton.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/14.
//

#import "XPHitButton.h"

@implementation XPHitButton

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent*)event
{
    CGRect bounds = self.bounds;
    CGFloat widthDelta = MAX(self.hitRect, 0);
    CGFloat heightDelta = MAX(self.hitRect, 0);
    bounds = CGRectInset(bounds, -0.5 * widthDelta, -0.5 * heightDelta);
    return CGRectContainsPoint(bounds, point);
}


@end
