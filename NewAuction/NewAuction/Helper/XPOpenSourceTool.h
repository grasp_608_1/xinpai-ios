//
//  XPOpenSourceTool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPOpenSourceTool : NSObject
+ (instancetype)shareInstance;

- (void)openVideoControllerWithUrl:(NSString *)urlStr;

@end

NS_ASSUME_NONNULL_END
