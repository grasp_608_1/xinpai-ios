//
//  XPTabBarController.m
//  NewAuction
//
//  Created by Grasp_L on 2024/6/7.
//

#import "XPTabBarController.h"
#import "XPNavigationController.h"
#import "XPMainViewController.h"
#import "XPCreationViewController.h"
#import "XPUserCenterViewController.h"
#import "XPShopMainController.h"
#import "XPLocalStorage.h"
#import "XPCircleViewController.h"
#import "XPMakerViewController.h"


@interface XPTabBarController ()

@property (nonatomic, strong) XPMainViewController *mainVC;

@property (nonatomic, strong) XPCreationViewController *createVC;

@property (nonatomic, strong) XPUserCenterViewController *userCenterVC;

@property (nonatomic, strong) XPShopMainController *shopVC;

@property (nonatomic, strong) XPCircleViewController *circleVC;

@property (nonatomic, strong) XPMakerViewController *makerVC;

@property (nonatomic, assign) BOOL isFirstLanch;

@end

@implementation XPTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if (@available(iOS 13.0, *)) {
        UITabBarAppearance *appearance = [[UITabBarAppearance alloc] init];
        // 未选中时候标题颜色
        appearance.stackedLayoutAppearance.normal.titleTextAttributes = @{NSForegroundColorAttributeName : UIColorFromRGB(0x6E6BFF),NSFontAttributeName:kFontMedium(16)};
        // 选中时候标题的颜色
        appearance.stackedLayoutAppearance.selected.titleTextAttributes = @{NSForegroundColorAttributeName : app_main_color,NSFontAttributeName:kFontMedium(16)};
        self.tabBar.standardAppearance = appearance;
        
    } else {
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x6E6BFF), NSForegroundColorAttributeName,kFontMedium(16),NSFontAttributeName,nil] forState:UIControlStateNormal];
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:app_main_color,kFontMedium(16),NSFontAttributeName,nil] forState:UIControlStateSelected];

    }
//    
    [self addAllControllers];
    
    //默认打开第几个controller
    NSString *selectedStr = [XPLocalStorage readUserHomeStatus];
    self.selectedIndex = selectedStr.integerValue;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoginOut) name:Notification_user_login_out object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getVersion];
}

//添加所有tabbarVC
- (void)addAllControllers {
    
    self.mainVC = [[XPMainViewController alloc] init];
    self.shopVC = [[XPShopMainController alloc] init];
    self.createVC = [[XPCreationViewController alloc] init];
    self.userCenterVC = [[XPUserCenterViewController alloc] init];
    self.circleVC = [[XPCircleViewController alloc] init];
    self.makerVC = [[XPMakerViewController alloc] init];
    
    
    XPNavigationController *mainNav = [[XPNavigationController alloc] initWithRootViewController:self.mainVC];
    XPNavigationController *createNav = [[XPNavigationController alloc] initWithRootViewController:self.createVC];
    XPNavigationController *userNav = [[XPNavigationController alloc] initWithRootViewController:self.userCenterVC];
    XPNavigationController *shopNav = [[XPNavigationController alloc] initWithRootViewController:self.shopVC];
    XPNavigationController *circleVC = [[XPNavigationController alloc] initWithRootViewController:self.circleVC];
    XPNavigationController *makerVC = [[XPNavigationController alloc] initWithRootViewController:self.makerVC];
    
    [self setupWithControllers:mainNav Title:@"新拍" Image:[UIImage imageNamed:@"xp_home_normal"] andSelectedImage:[UIImage imageNamed:@"xp_home_selected"]];
    [self setupWithControllers:shopNav Title:@"商城" Image:[UIImage imageNamed:@"xp_shop_normal"] andSelectedImage:[UIImage imageNamed:@"xp_shop_selected"]];
    [self setupWithControllers:circleVC Title:@"圈子" Image:[UIImage imageNamed:@"xp_circle_normal"] andSelectedImage:[UIImage imageNamed:@"xp_circle_selected"]];
    [self setupWithControllers:createNav Title:@"造物" Image:[UIImage imageNamed:@"xp_3d_normal"] andSelectedImage:[UIImage imageNamed:@"xp_3d_selected"]];
    [self setupWithControllers:makerVC Title:@"创客" Image:[UIImage imageNamed:@"xp_maker_normal"] andSelectedImage:[UIImage imageNamed:@"xp_maker_selected"]];
    [self setupWithControllers:userNav Title:@"我的" Image:[UIImage imageNamed:@"xp_usercenter_normal"] andSelectedImage:[UIImage imageNamed:@"xp_usercenter_selected"]];
    if (APPTYPE == 0) {
        self.viewControllers = @[mainNav,shopNav,circleVC,makerVC,userNav];
    }else if (APPTYPE == 1){
        self.viewControllers = @[createNav,shopNav,circleVC,makerVC,userNav];
    }
    
}

-(void)setupWithControllers:(UIViewController *)viewController Title:(NSString *)titleName Image:(UIImage *)normalImage andSelectedImage:(UIImage *)selectedImage{

    viewController.tabBarItem.title = titleName;
    viewController.tabBarItem.image = [normalImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    viewController.tabBarItem.selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    viewController.tabBarItem.imageInsets = UIEdgeInsetsMake(-2, -2, -5, -2);
    
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    NSLog(@"item -- %zd",item.tag);
    [self getVersion];
}

- (void)userLoginOut {
    self.selectedIndex = 0;
}

#pragma mark --- 版本升级相关
- (void)getVersion{
    NSString *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_app_update];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    
    if (self.isFirstLanch) {
        return;
    }
    self.isFirstLanch = YES;
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        if (data.isSucess) {
            self.isFirstLanch = YES;
            NSDictionary *versionDic;
            if (APPTYPE == 0) {
                versionDic = data.userData[@"currentVersion"];
            }else if (APPTYPE == 1){
                versionDic = data.userData[@"currentVersionVoiceShopping"];
            }
            
            NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
            NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
            //强更最低版本
            NSString *lineForceVersion = versionDic[@"versionIos"];
            //服务器最新版本
            NSString *appLastVersion = versionDic[@"versionIosOnline"];
            if (!appLastVersion) {
                appLastVersion = versionDic[@"iOSonLineVersion"];
            }
            NSString *appNeedPop = versionDic[@"showPopupIos"];
            //是否需要强更
            if ([self isNeedUpdateVersionLocationString:app_Version remoteString:lineForceVersion]) {//是否要更新
               
                [self showForceUpdateAlertWithDic:versionDic];

            }else
                if ([self isNeedUpdateVersionLocationString:app_Version remoteString:appLastVersion]){//是否需要弹窗
                if (appNeedPop.intValue == 1) {
                    
                    NSArray *localVersions = [[NSUserDefaults standardUserDefaults] objectForKey:@"localVersions"];
                    if (!localVersions || ![localVersions containsObject:appLastVersion]) {
                        [self showNormalUpdateAlertWithDic:versionDic];
                        
                        if (!localVersions) {
                            [[NSUserDefaults standardUserDefaults] setObject:@[appLastVersion] forKey:@"localVersions"];
                        }else{
                            NSArray *tempArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"localVersions"];
                            NSMutableArray *mArray = [NSMutableArray array];
                            [mArray addObjectsFromArray:tempArray];
                            [mArray addObject: appLastVersion];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:mArray.copy forKey:@"localVersions"];
                        }
                    }
                }
            }
 
        }else {
            self.isFirstLanch = NO;
            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:self.view];
        }
    }];
}
//强制更
- (void)showForceUpdateAlertWithDic:(NSDictionary *)dic {
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示"
                                                                    message:dic[@"descIos"]
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *left = [UIAlertAction actionWithTitle:@"立即更新"                                                               style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self showForceUpdateAlertWithDic:dic];
        [self jumpToStore:dic];
    }];
    [alertC addAction:left];
    [self presentViewController:alertC animated:YES completion:nil];
}
//普通更
- (void)showNormalUpdateAlertWithDic:(NSDictionary *)dic {
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"版本提示"
                                                                    message:dic[@"descIos"]
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *left = [UIAlertAction actionWithTitle:@"跳过" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *right = [UIAlertAction actionWithTitle:@"立即更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self jumpToStore:dic];
    }];
    
    [alertC addAction:left];
    [alertC addAction:right];

    [self presentViewController:alertC animated:YES completion:nil];
}

- (void)jumpToStore:(NSDictionary *)dic {
    NSString *urlStr = dic[@"downloadAddressIos"];
    NSURL *url = [NSURL URLWithString:urlStr];
    [[UIApplication sharedApplication] openURL:url options:nil completionHandler:nil];
    
}

#pragma mark -- 版本号比较大小
//是否需要升级。返回yes升级。返回no不升级.
- (BOOL)isNeedUpdateVersionLocationString:(NSString *)localVersion remoteString:(NSString *)remoteVersion{
    //把版本号转换成数值
    NSArray *array1 = [localVersion componentsSeparatedByString:@"."];
    NSArray *array2 = [remoteVersion componentsSeparatedByString:@"."];
    if(array1.count!=3||array2.count!=3){
        return NO;
    }
    
    if(array2.count>=3){
        if([array2[0] intValue]>[array1[0] intValue]){
            return YES;
        }else if([array2[0] intValue]==[array1[0] intValue]){
            //==的情况比较第二个数字
            if([array2[1] intValue]>[array1[1] intValue]){
                return YES;
            }else if([array2[1] intValue]==[array1[1] intValue]){
                //==的情况比较第三个数字
                if([array2[2] intValue]>[array1[2] intValue]){
                    return YES;
                }else{
                    return NO;
                }
            }else{
                return NO;
            }
        }else{
            return NO;
        }
    }else{
        return NO;
    }
}


@end
