//
//  XPRoutetool.h
//  NewAuction
//
//  Created by Grasp_L on 2024/9/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPRoutetool : NSObject
+ (void)pushWithDic:(NSDictionary*)info;
+ (void)entryWithDic:(NSDictionary *)info;
@end

NS_ASSUME_NONNULL_END
