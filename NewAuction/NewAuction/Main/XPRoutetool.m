//
//  XPRoutetool.m
//  NewAuction
//
//  Created by Grasp_L on 2024/9/6.
//

#import "XPRoutetool.h"
#import "XPMyStoreHouseViewController.h"
#import "XPMyFavoriteViewController.h"
#import "XPMyBankCardController.h"
#import "XPMoneyManagerController.h"
#import "XPStoreOrderController.h"
#import "XPMyFansViewController.h"
#import "XPPickGoodsManagerController.h"
#import "XPUserAddressController.h"
#import "XPBaseWebViewViewController.h"
#import "XPProductUpdateViewController.h"
#import "XPFeedbackRecordController.h"
#import "XPShopAfterSaleRecordController.h"
#import "XPUserShopCouponController.h"
#import "XPShopOrderStatusController.h"
#import "XPCreationFansController.h"
#import "XPMakerMoneyTranferOutController.h"
#import "XPMakerBadgeController.h"
#import "XPMyMakerUploadController.h"
#import "XPPrintRecoardController.h"
#import "XPPrintListController.h"

#import "XPBaseWebViewViewController.h"
#import "XPProductDetailController.h"
#import "XPProductSortViewController.h"
#import "XPShopGoodsDetailController.h"
#import "XPShopCategoryController.h"
#import "XPTrendDetailViewController.h"
#import "XPCreateShowViewController.h"


@implementation XPRoutetool

+ (void)pushWithDic:(NSDictionary*)info;
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:info];
    
    NSString *type = [dic objectForKey:@"mid"];
    
    if (type) {//根据mid 跳转具体页面
        
        UIViewController *currentVC = [XPCommonTool getCurrentViewController];
        
        NSInteger mId = type.integerValue;
        if (mId == 1) {//分享,暂不处理,控制器处理
            
        }else if (mId == 2){//我的粉丝
            XPMyFansViewController *vc = [XPMyFansViewController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
            
        }else if (mId == 3){//我的收藏
            XPMyFavoriteViewController *vc = [XPMyFavoriteViewController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
            
        }else if (mId == 4){//支付卡
            XPMyBankCardController *vc = [XPMyBankCardController new];
            vc.enterType = 0;
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 5){//结算卡
            XPMyBankCardController *vc = [XPMyBankCardController new];
            vc.enterType = 1;
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }
        
        else if (mId == 7){//待支付
            XPShopOrderStatusController *vc = [XPShopOrderStatusController new];
            vc.entertype = 2;
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 8){//待发货
            XPShopOrderStatusController *vc = [XPShopOrderStatusController new];
            vc.entertype = 3;
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 9){//待评价
            XPShopOrderStatusController *vc = [XPShopOrderStatusController new];
            vc.entertype = 4;
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 10){//售后
            XPShopAfterSaleRecordController *vc = [XPShopAfterSaleRecordController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 11){//我的造物订单
            [[XPShowTipsTool shareInstance] showMsg:@"敬请期待!" ToView:k_keyWindow];
            return;
            XPBaseViewController *vc = [XPBaseViewController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 12){//我的造物打印记录
            XPPrintRecoardController *vc = [XPPrintRecoardController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 13){//我的设备
            XPPrintListController *vc = [XPPrintListController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 14){//优惠券
            XPUserShopCouponController *vc = [XPUserShopCouponController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 15){//我的地址
            XPUserAddressController *vc = [XPUserAddressController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 16){//用户反馈
            XPFeedbackRecordController *vc = [XPFeedbackRecordController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 17){//联系客服
            XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
            vc.urlString = [NSString stringWithFormat:@"%@%@",kNet_host,k_user_help];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 18){//资讯动态
            XPProductUpdateViewController *vc = [XPProductUpdateViewController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 19){//货款管理
            XPMoneyManagerController *vc = [XPMoneyManagerController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 20){//提货管理
            XPPickGoodsManagerController *vc = [XPPickGoodsManagerController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 21){//拍卖订单
            XPMyStoreHouseViewController *vc = [XPMyStoreHouseViewController new];
            vc.entertype = 1;
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 22){//拍卖订单
            XPTrendDetailViewController *vc = [XPTrendDetailViewController new];
            vc.enterType = 10;
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 23){//我的造物关注
            XPCreationFansController *vc = [XPCreationFansController new];
            vc.entryType = 3;
            vc.userId = @(0);
            vc.nickName = @"我的";
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if (mId == 24){//我的创客提现
            XPMakerMoneyTranferOutController *vc = [XPMakerMoneyTranferOutController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if(mId == 25){//我的创客徽章
            XPMakerBadgeController *vc = [XPMakerBadgeController new];
            vc.hidesBottomBarWhenPushed = YES;
            vc.userId = @(0);
            [currentVC.navigationController pushViewController:vc animated:YES];
        }else if(mId == 26){//我的创客徽章
            XPMyMakerUploadController *vc = [XPMyMakerUploadController new];
            vc.hidesBottomBarWhenPushed = YES;
            [currentVC.navigationController pushViewController:vc animated:YES];
        }
    }
}

+(void)entryWithDic:(NSDictionary *)info {
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:info];
    NSString *type = [dic objectForKey:@"displayLocation"];
    NSString *jumpType = [dic objectForKey:@"jumpType"];
    if (type) {//新拍
        UIViewController *currentVC = [XPCommonTool getCurrentViewController];
        
        if (type.intValue == 0) {//新拍
            if (jumpType.intValue == 0) {//跳转类型 0 url 1 单拍品 2 拍品分类 3 拍品 专题
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.hidesBottomBarWhenPushed = YES;
                vc.urlString = dic[@"url"];
                [currentVC.navigationController pushViewController:vc animated:YES];
            }else if (jumpType.intValue == 1){
                XPProductDetailController *vc = [XPProductDetailController new];
                vc.hidesBottomBarWhenPushed = YES;
                vc.auctionItemId = dic[@"auctionItemsId"];
                [currentVC.navigationController pushViewController:vc animated:YES];
            }else if (jumpType.intValue == 2){
                XPProductSortViewController *vc = [XPProductSortViewController new];
                vc.hidesBottomBarWhenPushed = YES;
                [currentVC.navigationController pushViewController:vc animated:YES];
            }else{
                
            }
        }else if (type.intValue == 1){//商城
            //跳转类型 0 url 1 单商品 2 商品分类 3 商品专题
            if (jumpType.intValue == 0) {//跳转类型 0 url 1 单拍品 2 拍品分类 3 拍品 专题
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.hidesBottomBarWhenPushed = YES;
                vc.urlString = dic[@"url"];
                [currentVC.navigationController pushViewController:vc animated:YES];
            }else if (jumpType.intValue == 1){
                XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
                vc.hidesBottomBarWhenPushed = YES;
                vc.productSkuId = dic[@"productSkuId"];
                [currentVC.navigationController pushViewController:vc animated:YES];
            }else if (jumpType.intValue == 2){
                XPShopCategoryController *vc = [XPShopCategoryController new];
                vc.hidesBottomBarWhenPushed = YES;
                vc.keywords = dic[@"keywords"];
                [currentVC.navigationController pushViewController:vc animated:YES];
            }
            
        }else if (type.intValue == 2){//造物
            //跳转类型 0 url 1 单商品 2 商品分类 3 商品专题
            if (jumpType.intValue == 0) {//跳转类型 0 url 1 单拍品 2 拍品分类 3 拍品 专题
                XPCreateShowViewController *vc = [XPCreateShowViewController new];
                vc.hidesBottomBarWhenPushed = YES;
                vc.urlString = dic[@"url"];
                vc.titleStr = dic[@"name"];
                [currentVC.navigationController pushViewController:vc animated:YES];
            }else if (jumpType.intValue == 1){
                XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
                vc.hidesBottomBarWhenPushed = YES;
                vc.productSkuId = dic[@"productSkuId"];
                [currentVC.navigationController pushViewController:vc animated:YES];
            }else {
                [[XPShowTipsTool shareInstance] showMsg:@"敬请期待!" ToView:k_keyWindow];
            }
        }else if (type.intValue == 3){//创客
            //跳转类型 0 url 1 单商品 
            if (jumpType.intValue == 0) {//跳转类型 0 url 1 单拍品 2 拍品分类 3 拍品 专题
                XPBaseWebViewViewController *vc = [XPBaseWebViewViewController new];
                vc.hidesBottomBarWhenPushed = YES;
                vc.urlString = dic[@"url"];
                [currentVC.navigationController pushViewController:vc animated:YES];
            }else if (jumpType.intValue == 1){
                XPShopGoodsDetailController *vc = [XPShopGoodsDetailController new];
                vc.hidesBottomBarWhenPushed = YES;
                vc.productSkuId = dic[@"productSkuId"];
                [currentVC.navigationController pushViewController:vc animated:YES];
            }else {
                [[XPShowTipsTool shareInstance] showMsg:@"敬请期待!" ToView:k_keyWindow];
            }
        }   
    }
}

@end
