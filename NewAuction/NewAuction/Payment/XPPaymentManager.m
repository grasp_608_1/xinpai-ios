//
//  XPPaymentManager.m
//  NewAuction
//
//  Created by Grasp_L on 2024/8/6.
//

#import "XPPaymentManager.h"
#import "yunQuick/YunController.h"
#import "WXApi.h"
#import <AlipaySDK/AlipaySDK.h>

//最大轮训次数
static NSInteger MaxRepeatCount = 3;

static XPPaymentManager *instance;

@interface XPPaymentManager ()

@property (nonatomic, copy) NSString *queryUrl;

@property (nonatomic, strong) XPBaseViewController *controller;

@property (nonatomic, copy) RequestResultBlock block;

@property (nonatomic, assign) NSInteger repeatCount;
//支付方式
@property (nonatomic, assign) SendayPayType payType;
//是否需要轮巡
@property (nonatomic, assign) BOOL isQuery;
@end

@implementation XPPaymentManager
+ (instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}


- (void)showPayViewWithUrl:(NSString *)url viewController:(XPBaseViewController *)controller {
    self.controller = controller;
    [[YunController shareInstance] oneKeyAccessWithUrl:url viewController:controller completeBlock:^(NSNumber * _Nullable code, id  _Nonnull responseObj) {
        
        NSLog(@"code --->%@  res%@",code,responseObj);
        
        if (responseObj[@"respDesc"] &&[responseObj[@"respDesc"] isKindOfClass:[NSString class]]) {
            [[XPShowTipsTool shareInstance] showMsg:responseObj[@"respDesc"] ToView:k_keyWindow];
        }else {
            [[XPShowTipsTool shareInstance] showMsg:@"拉起支付失败,请联系管理员" ToView:k_keyWindow];
        }
        
    }];
}


- (void)showWeChatPayWithDic:(NSDictionary *)dic viewController:(XPBaseViewController *)controller {
    PayReq *request = [[PayReq alloc] init];
    // 注册时分配的财付通商户号
    request.partnerId = [NSString stringWithFormat:@"%@", [dic objectForKey:@"partnerId"]];
    //
    request.prepayId = [NSString stringWithFormat:@"%@", [dic objectForKey:@"prepayId"]];
    // 订单详情
    request.package = [NSString stringWithFormat:@"%@", [dic objectForKey:@"packageType"]];
    // 32位内的随机串,防重发
    request.nonceStr = [NSString stringWithFormat:@"%@", [dic objectForKey:@"nonceStr"]];
    // 时间戳
    request.timeStamp = [[NSString stringWithFormat:@"%@", [dic objectForKey:@"timestamp"]] intValue];
    // 签名
    request.sign = [NSString stringWithFormat:@"%@", [dic objectForKey:@"sign"]];
    if (APPTYPE == 0) {
        [WXApi registerApp:XP_wechat_appKey universalLink:XP_wechat_universalLink];
    }else if(APPTYPE == 1){
        [WXApi registerApp:ZW_wechat_appKey universalLink:ZW_wechat_universalLink];
    }
    
    [WXApi sendReq:request completion:^(BOOL success) {
        
    }];
    
}

-(void)showAlipayWithDic:(NSDictionary *)dic viewController:(XPBaseViewController *)controller {
    NSString *scheme;
    if (APPTYPE == 0) {
        scheme = @"newauction";
    }else if(APPTYPE == 1){
        scheme = @"newcreation";
    }
    NSString *orderStr = dic[@"aliOrderInfo"];
    
    [[AlipaySDK defaultService] payOrder:orderStr fromScheme:scheme callback:^(NSDictionary *resultDic) {
        NSLog(@"reslut = %@",resultDic);
    }];
    
}

- (void)orderPayResultQueryWithType:(SendayPayType)type orderId:(NSString *)orderID  controller:(XPBaseViewController *)controller requestResultBlock:(RequestResultBlock)block shouldQuery:(BOOL)query{
    self.payType = type;
    self.block = block;
    self.isQuery = query;
    self.controller = controller;
    
    self.queryUrl = [self getPayQueryUrlWithType:type orderId:orderID];
    self.repeatCount = 0;
    [self orderQuery];
    
}
//商城订单拉起第三方支付关闭，更新商城订单状态
- (void)orderPayResultQueryWithType:(SendayPayType)type orderId:(NSString *)orderID   orderNo:(NSString *)orderNo controller:(XPBaseViewController *)controller requestResultBlock:(RequestResultBlock)block shouldQuery:(BOOL)query{
    self.payType = type;
    self.block = block;
    self.isQuery = query;
    self.controller = controller;
    
    self.queryUrl = [self getPayQueryUrlWithType:type orderId:orderID];
    self.repeatCount = 0;
    [self updateSeverOrderStatusWithOrderId:orderID orderNo:orderNo];
    
}

- (void)updateSeverOrderStatusWithOrderId:(NSString *)orderID orderNo:(NSString *)orderNo{

    NSString  *url = [NSString stringWithFormat:@"%@%@",kVERIFY_host,k_sandPay_updateOrderStatus];
    NSMutableDictionary *paraM = [NSMutableDictionary dictionary];
    paraM[@"orderId"] = orderNo ;
    paraM[@"payOrderId"] = orderID;
    
    
    [self.controller startLoadingGif];
    [[XPRequestManager shareInstance] commonPostWithURL:url Params:paraM requestResultBlock:^(XPRequestResult * _Nonnull data) {
        [self.controller stopLoadingGif];
        if (data.isSucess) {
            [self orderQuery];
        }else {
             if (self.block) {
                 self.block(data);
             }
        }
    }];
}

- (void)orderQuery{
    [self.controller startLoadingGif];
    [[XPRequestManager shareInstance] commonGetWithURL:self.queryUrl Params:@{} requestResultBlock:^(XPRequestResult * _Nonnull data) {
        if (data.isSucess) {
            [self.controller stopLoadingGif];
            if (self.block) {
                self.block(data);
            }
        }else if(data.code.intValue == 10000){//pengding
            if (self.repeatCount >= MaxRepeatCount || !self.isQuery) {
                [self.controller stopLoadingGif];
                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(orderQuery) object:nil];
                if (!self.isQuery) {
                    if (self.block) {
                        if (self.block) {
                            self.block(data);
                        }
                    }
                }
            }else{
                self.repeatCount ++;
                [self performSelector:@selector(orderQuery) withObject:nil afterDelay:2.0];
            }
         }else {
             [self.controller stopLoadingGif];
//            [[XPShowTipsTool shareInstance] showMsg:data.msg ToView:k_keyWindow];
             if (self.block) {
                 self.block(data);
             }
        }
    }];
}


- (NSString *)getPayQueryUrlWithType:(SendayPayType)type orderId:(NSString *)orderID {
    NSString *url;
    if (type == SendayPayRechargeNormal) {//充值
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,k_sandPay_recharge_query,orderID];
    }else if (type == SendayPayRechargePart) {//参拍补差价
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,k_sandPay_rechargePart_query,orderID];
    }else if (type == SendayPayRechargePick) {//提货补差价
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,k_sandPay_rechargePick_query,orderID];
    }else if (type == SendayPayShopOrder) {//商城商品购买
        url = [NSString stringWithFormat:@"%@%@/%@",kVERIFY_host,k_sandPay_shopOrder_query,orderID];
    }
    return url;
}

@end
