//
//  YunController.h
//  yunQuick
//
//  Created by Cz on 14.11.22.
//

#import <Foundation/Foundation.h>
#import <SafariServices/SafariServices.h>
#import <WebKit/WebKit.h>


/**
 000001    微信支付res    {@"tn":@"2333333333"}
 000002    银联支付res    {@"path":@"home/payPage/"}
 000003    转账res    authWay为01或者03 直接成功
 10000001    数据异常    jsonString 数据格式有误
 10000003    业务响应异常    responseCode不为"00000"或者"00000BCEAS"
 */
typedef void (^YunControllerResultBlock)(NSNumber* _Nullable code, id _Nonnull responseObj);

typedef enum : NSUInteger {
    PyTypeWeiXP = 0,                // 微信
    PyTypeZFBP = 1,                 // 支付宝
    PyTypeUnionP = 4,               // 云闪付
} ProductPyType;


NS_ASSUME_NONNULL_BEGIN

@interface YunController : NSObject

///// 是否展示导航条 默认 YES
//@property (nonatomic, assign) bool showNav;
///// 是否展示进度条 默认 YES,   注意：showNav为NO,则不展示
//@property (nonatomic, assign) bool showProgress;
///// 进度条颜色 默认蓝色，     注意：showNav为NO,则不展示
////@property (nonatomic, strong) UIColor * progressTintColor;

/// YES: 生产环境   NO: 测试环境    默认pord
@property (nonatomic, assign) BOOL testMode;

/// 自定义domain。
@property (nonatomic, copy) NSString *diyUrl;


+ (YunController *)shareInstance;

/// 当前SDK版本号  eg:1.0.0
- (void)sdkVersion;

/**
 *
 *  预下单
 *  业务请求处理
 *
 *  @param paramsString   商户后台加密加签的jsonString数据。
 *  @param viewController 承载vc
 *  @param completeBlock  结果回调（杉德返回的加密加签的数据）
 */
- (void)placeOrder:(NSString *)paramsString
    viewController:(UIViewController*)viewController
     completeBlock:(YunControllerResultBlock)completeBlock;

/**
 *
 *  开户/个人主页
 *  业务请求处理
 *
 *  @param paramsString   商户后台加密加签的jsonString数据。
 *  @param viewController 承载vc
 *  @param completeBlock  结果回调（杉德返回的加密加签的数据）
 */
- (void)memberQuery:(NSString *)paramsString
     viewController:(UIViewController*)viewController
      completeBlock:(YunControllerResultBlock)completeBlock;


/**
 *  @param paramsString   商户后台解密的jsonString数据,传入sdk做解析。
 *  @param viewController 承载vc
 *  @param completeBlock  结果回调
 */
- (void)oneKeyAccess:(NSString *)paramsString viewController:(UIViewController*)viewController completeBlock:(YunControllerResultBlock)completeBlock;

/**
 *  拉起业务
 *
 *  @param urlString      商户后台解密的url参数。
 *  @param viewController 承载vc
 *  @param completeBlock  结果回调
 */
- (void)oneKeyAccessWithUrl:(NSString *)urlString viewController:(UIViewController*)viewController completeBlock:(YunControllerResultBlock)completeBlock;


@end

NS_ASSUME_NONNULL_END
