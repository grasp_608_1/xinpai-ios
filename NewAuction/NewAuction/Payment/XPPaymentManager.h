//
//  XPPaymentManager.h
//  NewAuction
//
//  Created by Grasp_L on 2024/8/6.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    SendayPayRechargeNormal,//充值
    SendayPayRechargePart,//参拍补差价
    SendayPayRechargePick,//提货补差价
    SendayPayShopOrder,//商城商品购买
} SendayPayType;

NS_ASSUME_NONNULL_BEGIN

@interface XPPaymentManager : NSObject
+ (instancetype)shareInstance;

/// 支付拉起杉德支付
/// - Parameters:
///   - url: 支付的url地址
///   - controller: 当前页面控制器
- (void)showPayViewWithUrl:(NSString *)url  viewController:(XPBaseViewController *)controller;


/// 支付拉起微信支付
/// - Parameters:
///   - dic: 微信支付拉起所需参数
///   - controller: 当前页面控制器
- (void)showWeChatPayWithDic:(NSDictionary *)dic viewController:(XPBaseViewController *)controller;

/// 支付拉起支付宝支付
/// - Parameters:
///   - dic: 支付宝参数
///   - controller: 当前页面控制器
-(void)showAlipayWithDic:(NSDictionary *)dic viewController:(XPBaseViewController *)controller;

/// 支付结果查询
/// - Parameters:
///   - type: 支付类型
///   - orderID: 支付订单号
///   - controller: 当前页面控制前
///   - block: 结果回调
///   - query: 是否需要轮巡
- (void)orderPayResultQueryWithType:(SendayPayType)type orderId:(NSString *)orderID  controller:(XPBaseViewController *)controller requestResultBlock:(RequestResultBlock)block shouldQuery:(BOOL)query;

/// 支付结果查询 商城订单拉起第三方支付关闭，更新商城订单状态 轮训之前需要更新商城状态
/// - Parameters:
///   - type: 支付类型
///   - orderID: 支付订单号
///   - controller: 当前页面控制前
///   - block: 结果回调
///   - query: 是否需要轮巡
- (void)orderPayResultQueryWithType:(SendayPayType)type orderId:(NSString *)orderID   orderNo:(NSString *)orderNo controller:(XPBaseViewController *)controller requestResultBlock:(RequestResultBlock)block shouldQuery:(BOOL)query;

@end

NS_ASSUME_NONNULL_END
