//
//  XPPaymentView.h
//  NewAuction
//
//  Created by Grasp_L on 2024/11/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, XPPaymentViewType) {
    XPPaymentViewTypeTakeGoods,//提货补差价
    XPPaymentViewTypeJoin,//参拍补差价
    XPPaymentViewTypeBuy//商品购买
};


@interface XPPaymentView : UIView
//下一步回调 0 杉德支付 1 微信支付 2 账户直接支付(金额足够)
@property (nonatomic, copy) void(^nextBlock)(NSInteger status,NSString *accountAmount);

@property (nonatomic, copy) void(^accountBlock)(void);

/// 支付金额
/// - Parameters:
///   - money: 支付金额
///   - accountNum: 账户余额
///   - type: view类型
- (void)viewWithNeedMoney:(NSNumber *)money
             accountMoney:(NSNumber *)accountNum
                     type:(XPPaymentViewType)type;
//账户余额是否选中
- (BOOL)getAccountStatus;
@end

NS_ASSUME_NONNULL_END
