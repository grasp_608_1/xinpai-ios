//
//  XPPaymentView.m
//  NewAuction
//
//  Created by Grasp_L on 2024/11/27.
//

#import "XPPaymentView.h"
#import "XPHitButton.h"
#import "XPPayTypeListView.h"

@interface XPPaymentView ()<UITextFieldDelegate>
//返回
@property (nonatomic, strong) XPHitButton *backButton;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//金额
@property (nonatomic, strong) UIView *moneyView;
//需补差价
@property (nonatomic, strong) UILabel *needMoneyDescLabel;
//需补金额
@property (nonatomic, strong) UILabel *needMoneyLabel;
//账户资金
@property (nonatomic, strong) UIView *accountView;
@property (nonatomic, strong) XPPayTypeListView *accountListView;
//支付方式
@property (nonatomic, strong) UIView *payWayView;
@property (nonatomic, strong) XPPayTypeListView *payWayListView;
//当前选中的支付方式
@property (nonatomic, assign) NSInteger payIndex;

@property (nonatomic, copy) NSString *needMoney;

//三方支付
@property (nonatomic, strong) NSMutableArray *payTypeArray;

@property (nonatomic, strong) NSNumber *needTotalMoney;
@property (nonatomic, strong) NSNumber *accountMoney;

@property (nonatomic, assign) XPPaymentViewType payType;
@end

@implementation XPPaymentView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView{
    
    self.payIndex = -1;
    
    self.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    XPHitButton *backButton = [[XPHitButton alloc] init];
    backButton.hitRect = 40;;
    backButton.frame = CGRectMake(16, 16 + kStatusBarHeight, 24, 24);
    [backButton addTarget:self action:@selector(leftButtonClcked:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"back_gray"] forState:UIControlStateNormal];
    [self addSubview:backButton];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = kFontMedium(18);
    self.titleLabel.textColor = app_font_color;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(0, kStatusBarHeight,self.mj_w, 56);
    [self addSubview:self.titleLabel];
    
    self.moneyView = [[UIView alloc] initWithFrame:CGRectMake(12,MaxY(self.titleLabel) + 12, self.mj_w - 24, 100)];
    self.moneyView.backgroundColor = UIColor.whiteColor;
    self.moneyView.layer.cornerRadius = 8;
    [self addSubview:self.moneyView];
    
    self.needMoneyDescLabel = [UILabel new];
    self.needMoneyDescLabel.font = kFontRegular(14);
    self.needMoneyDescLabel.textColor = app_font_color;
    self.needMoneyDescLabel.frame = CGRectMake(24, 12,self.moneyView.mj_w - 24,24);
    [self.moneyView addSubview:self.needMoneyDescLabel];
    
    self.needMoneyLabel = [UILabel new];
    self.needMoneyLabel.font = kFontMedium(32);
    self.needMoneyLabel.textColor = UIColor.blackColor;
    self.needMoneyLabel.frame = CGRectMake(24, 44,self.moneyView.mj_w - 24,40);
    [self.moneyView addSubview:self.needMoneyLabel];
    
    self.accountView = [[UIView alloc] initWithFrame:CGRectMake(12, MaxY(self.moneyView) + 12, self.mj_w - 24, 72)];
    self.accountView.layer.cornerRadius = 12;
    self.accountView.backgroundColor = UIColor.whiteColor;
    [self addSubview:self.accountView];
    
    self.accountListView = [[XPPayTypeListView alloc]initWithFrame:CGRectMake(0, 0, self.accountView.mj_w, 72)];
    [self.accountView addSubview:self.accountListView];
   
    XPWeakSelf
    [self.accountListView setSelectedBlock:^(NSInteger tag) {
       
        [weakSelf accountSelected];
        [weakSelf showNeedMoneyWithMoney];
        
    }];
    
    self.payWayView = [[UIView alloc] initWithFrame:CGRectMake(12, MaxY(self.moneyView) + 12, self.mj_w - 24, 0)];
    self.payWayView.layer.cornerRadius = 12;
    self.payWayView.layer.masksToBounds = YES;
    self.payWayView.backgroundColor = UIColor.whiteColor;
    [self addSubview:self.payWayView];
    
    
    UILabel *paytypeLabel = [UILabel new];
    paytypeLabel.font = kFontRegular(14);
    paytypeLabel.textColor = UIColorFromRGB(0x3A3C41);
    paytypeLabel.frame = CGRectMake(24, 16, 100, 24);
    paytypeLabel.text = @"支付方式";
    [self.payWayView addSubview:paytypeLabel];
    
    UIButton *tableButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tableButton.frame = CGRectMake(self.payWayView.mj_w - 12 - 100 , 16, 100, 24);
    tableButton.titleLabel.font = kFontRegular(12);
    [tableButton setTitle:@"第三方快捷支付" forState:UIControlStateNormal];
    [tableButton setTitleColor:app_font_color_8A forState:UIControlStateNormal];
    [self.payWayView addSubview:tableButton];
    
    UIView *bottomView = [UIView new];
    bottomView.frame = CGRectMake(0,self.mj_h - 48 - kBottomHeight, self.mj_w, 48 + kBottomHeight);
    bottomView.backgroundColor = UIColor.whiteColor;
    [self addSubview:bottomView];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"下一步" forState:UIControlStateNormal];
    [rightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(24,4,self.mj_w - 48,40);
    rightButton.layer.cornerRadius = 20;
    rightButton.layer.masksToBounds = YES;
    [rightButton addTarget:self action:@selector(rightButton:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:rightButton];
    [rightButton.layer insertSublayer:[XPCommonTool setGradualChangingFromPoint:CGPointMake(0, 0.5) toPoint:CGPointMake(1, 0.5) Location:@[@(0), @(1.0f)] :rightButton colors:
                                     @[(__bridge id)UIColorFromRGB(0x7774FF).CGColor,
                                       (__bridge id)UIColorFromRGB(0x9766FF).CGColor]] atIndex:0];
   
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.35f];
    [animation setFillMode:kCAFillModeForwards];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromRight];
    [self.layer addAnimation:animation forKey:nil];
}

- (void)showNeedMoneyWithMoney{
    
    NSNumber *needNum = _needTotalMoney;
    
    bool status = [self getAccountStatus];
    double needCountF = _needTotalMoney.floatValue *1000;
    double accountCountF = _accountMoney.floatValue *1000;
    
    if (status == YES) {
        if (accountCountF >= needCountF) {
            needNum = @(0);
            [self clearThirdStatus];
            self.payWayView.userInteractionEnabled = NO;
        }else {
            CGFloat needF = needCountF - accountCountF;
            needNum = [NSNumber numberWithFloat:needF/1000.];
            self.payWayView.userInteractionEnabled = YES;
        }
    }else{
        self.payWayView.userInteractionEnabled = YES;
    }
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%@",[XPMoneyFormatTool formatMoneyWithNum:needNum]]];
    
    [attr addAttributes:@{NSFontAttributeName:kFontMedium(24)} range:NSMakeRange(0, 1)];
    
    self.needMoneyLabel.attributedText = attr;
    
}

- (void)viewWithNeedMoney:(NSNumber *)money 
             accountMoney:(NSNumber *)accountNum
                     type:(XPPaymentViewType)type {
    
    _needTotalMoney = money;
    _accountMoney = accountNum;
    _payType = type;
    
    [self showNeedMoneyWithMoney];
    
    if (type == XPPaymentViewTypeTakeGoods) {
        self.titleLabel.text = @"提货补差价";
        self.needMoneyDescLabel.text = @"需补差价";
        NSDictionary *accountDic = @{
            @"title":[NSString stringWithFormat:@"账户资金:￥%@",[XPMoneyFormatTool formatMoneyWithNum:accountNum]],
            @"desc" : @"可使用账户资金余额",
            @"pic":@"xp_payment_account"
        };
        
        [self.accountListView viewWithDict:accountDic showTips:NO showDesc:YES];
        
    }else if(type == XPPaymentViewTypeJoin){
        self.titleLabel.text = @"参拍补差价";
        self.needMoneyDescLabel.text = @"需补差价";
        self.accountListView.hidden = YES;
    }else if(type == XPPaymentViewTypeBuy){
        
        self.titleLabel.text = @"购买商品";
        self.needMoneyDescLabel.text = @"需支付";
        NSDictionary *accountDic = @{
            @"title":[NSString stringWithFormat:@"账户资金:￥%@",[XPMoneyFormatTool formatMoneyWithNum:accountNum]],
            @"desc" : @"可使用账户资金余额",
            @"pic":@"xp_payment_account"
        };
        if (APPTYPE == 1) {
            self.accountListView.hidden = YES;
        }else{
            [self.accountListView viewWithDict:accountDic showTips:NO showDesc:YES];
            
        }
    }
    
    [self.payWayView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[XPPayTypeListView class]]) {
            [obj removeFromSuperview];
        }
    }];
    
    NSArray *payArray;
    if (APPTYPE == 0) {
        
        if (type ==  XPPaymentViewTypeBuy) {
            payArray = @[
                @{
                    @"title":@"微信支付",
                    @"desc" : @"",
                    @"pic":@"xp_payment_wechat"
                },@{
                    @"title":@"支付宝支付",
                    @"desc" : @"",
                    @"pic":@"xp_payment_alipay"
                }
                
            ];
        }else{
            payArray = @[
                @{
                    @"title":@"杉德支付",
                    @"desc" : @"",
                    @"pic":@"xp_payment_tocompany"
                },@{
                    @"title":@"微信支付",
                    @"desc" : @"",
                    @"pic":@"xp_payment_wechat"
                },@{
                    @"title":@"支付宝支付",
                    @"desc" : @"",
                    @"pic":@"xp_payment_alipay"
                }
                
            ];
        }
    }else if (APPTYPE == 1){
        payArray = @[
            @{
                @"title":@"微信支付",
                @"desc" : @"",
                @"pic":@"xp_payment_wechat"
            }
        ];
    }
    
    bool isShowTipView = NO;
    for (int i = 0; i<payArray.count; i++) {
        NSDictionary *dict = payArray[i];
        XPPayTypeListView *payView = [[XPPayTypeListView alloc]initWithFrame:CGRectMake(0, 52 + 64 *i, self.payWayView.mj_w, 64)];
        payView.selectedButton.tag = i;
        [self.payWayView addSubview:payView];
        if (i == 0) {
            payView.sepView.hidden = NO;
        }else {
            payView.sepView.hidden = NO;
        }
        
        [payView viewWithDict:dict showTips:isShowTipView showDesc:NO];
        
        XPWeakSelf
        [payView setSelectedBlock:^(NSInteger tag) {
            [weakSelf selectedPayTypeTag:tag];
        }];
        
    }
    if (type == XPPaymentViewTypeJoin || type == XPPaymentViewTypeTakeGoods ||APPTYPE == 1) {
        self.payWayView.frame = CGRectMake(12, MaxY(self.moneyView) + 12, self.mj_w - 24, 56 + 64 *payArray.count);
    }else {
        self.payWayView.frame = CGRectMake(12, MaxY(self.accountView) + 12, self.mj_w - 24, 56 + 64 *payArray.count);
    }
}


- (void)selectedPayTypeTag:(NSInteger)tag{
    self.payIndex = tag;
    for (UIView *view in self.payWayView.subviews) {
        if ([view isKindOfClass:[XPPayTypeListView class]]) {
            XPPayTypeListView *tempView = (XPPayTypeListView *)view;
            if (tempView.selectedButton.tag == tag) {
                if (tempView.selectedButton.selected) {
                    self.payIndex = -1;
                }
                tempView.selectedButton.selected = !tempView.selectedButton.selected;
                
            }else {
                tempView.selectedButton.selected = NO;
            }
        }
    }
}


- (void)accountSelected{
    for (UIView *view in self.accountView.subviews) {
        if ([view isKindOfClass:[XPPayTypeListView class]]) {
            XPPayTypeListView *tempView = (XPPayTypeListView *)view;
                if (!tempView.selectedButton.selected) {
                    if (self.needMoney.floatValue > self.accountMoney.floatValue) {
                        self.payIndex = -1;
                        [self clearThirdStatus];
                    }
                }
                tempView.selectedButton.selected = !tempView.selectedButton.selected;
        }
    }
}




- (void)clearThirdStatus{
    self.payIndex = -1;
    for (UIView *view in self.payWayView.subviews) {
        if ([view isKindOfClass:[XPPayTypeListView class]]) {
            XPPayTypeListView *tempView = (XPPayTypeListView *)view;
            tempView.selectedButton.selected = NO;
        }
    }
}

- (BOOL)getAccountStatus {
    for (UIView *view in self.accountView.subviews) {
        if ([view isKindOfClass:[XPPayTypeListView class]]) {
            XPPayTypeListView *tempView = (XPPayTypeListView *)view;
            return tempView.selectedButton.selected;
        }
    }
    return NO;
}


-(void)leftButtonClcked:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
    [self dismiss];
    
//    [self performSelector:@selector(dismiss) withObject:nil afterDelay:0.35f];
    
}

- (void)dismiss{
    CGRect rect = self.frame;
    NSLog(@"_____ %@",NSStringFromCGRect(rect));
    [UIView animateWithDuration:0.35 animations:^{
        self.frame = CGRectMake(rect.size.width, rect.origin.x, rect.size.width, rect.size.height);
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


-(void)rightButton:(UIButton *)sender {
    buttonCanUseAfterOneSec(sender);
//    [self dismiss];
    
    bool status = [self getAccountStatus];
    double needCountF = _needTotalMoney.floatValue *1000;
    double accountCountF = _accountMoney.floatValue *1000;
    
    if (status == YES) {
        if (accountCountF >= needCountF) {
            if (self.nextBlock) {
                self.nextBlock(10,[XPMoneyFormatTool formatMoneyWithNum:_accountMoney]);
            }
            [self dismiss];
        }else {
            if (self.payIndex < 0) {
                [[XPShowTipsTool shareInstance] showMsg:@"请选择支付方式" ToView:k_keyWindow];
            }else {
                if (self.payType == XPPaymentViewTypeBuy) {
                     if(self.payIndex == 0){
                        if (self.nextBlock) {
                            self.nextBlock(2,[XPMoneyFormatTool formatMoneyWithNum:_accountMoney]);
                        }
                    }else if (self.payIndex == 1){
                        if (self.nextBlock) {
                            self.nextBlock(1,[XPMoneyFormatTool formatMoneyWithNum:_accountMoney]);
                        }
                    }
                }else{
                    if (self.payIndex == 0) {
                        if (self.nextBlock) {
                            self.nextBlock(11,[XPMoneyFormatTool formatMoneyWithNum:_accountMoney]);
                        }
                    }else if(self.payIndex == 1){
                        if (self.nextBlock) {
                            self.nextBlock(2,[XPMoneyFormatTool formatMoneyWithNum:_accountMoney]);
                        }
                    }else if (self.payIndex == 2){
                        if (self.nextBlock) {
                            self.nextBlock(1,[XPMoneyFormatTool formatMoneyWithNum:_accountMoney]);
                        }
                    }
                }
                [self dismiss];
            }
            
        }
    }else{//不使用账户支付
        if (self.payIndex < 0) {
            [[XPShowTipsTool shareInstance] showMsg:@"请选择支付方式" ToView:k_keyWindow];
        }else {
            
            if (self.payType == XPPaymentViewTypeBuy && APPTYPE == 0) {
                if(self.payIndex == 0){
                    if (self.nextBlock) {
                        self.nextBlock(2,@"0");
                    }
                }else if(self.payIndex == 1){
                    if (self.nextBlock) {
                        self.nextBlock(1,@"0");
                    }
                }
            }else{
                if (APPTYPE == 0) {//新拍
                    if (self.payIndex == 0) {
                        if (self.nextBlock) {
                            self.nextBlock(11,@"0");
                        }
                    }else if(self.payIndex == 1){
                        if (self.nextBlock) {
                            self.nextBlock(2,@"0");
                        }
                    }else if(self.payIndex == 2){
                        if (self.nextBlock) {
                            self.nextBlock(1,@"0");
                        }
                    }
                }else{//造物
                    if(self.payIndex == 0){
                        if (self.nextBlock) {
                            self.nextBlock(2,@"0");
                        }
                    }
                }
                
            }
            
            [self dismiss];
        }
        
    }
}

@end
